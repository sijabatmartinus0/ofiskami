<?php
	defined('USERNAME') OR define('USERNAME', 'admin'); 
	defined('PASSWORD') OR define('PASSWORD', 'Passw0rd'); 
	defined('LOGIN_URL') OR define('LOGIN_URL', 'http://www.kantormu.tk/admin/index.php?route=common/login/jxLogin');
	defined('SERVICE_URL') OR define('SERVICE_URL', 'http://www.kantormu.tk/admin/index.php?route=api/scheduler/service');
	
	try{
		$response = _cURL(LOGIN_URL,'POST',array('email'=>USERNAME, 'password'=>PASSWORD));
		if(!empty($response) && !empty($response['token'])){
			echo SERVICE_URL."&token=".$response['token'];
			$ch = curl_init();
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, SERVICE_URL."&token=".$response['token']);
			curl_setopt($ch, CURLOPT_HEADER, 0);

			// grab URL and pass it to the browser
			curl_exec($ch);

			// close cURL resource, and free up system resources
			curl_close($ch);
		}
	}catch(Exception $e){
		echo $e;
	}
	
	function _cURL($url,$method='GET',$data=array()){
		$result=array();
		try{
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => $method,
			  CURLOPT_POSTFIELDS => json_encode($data),
			  CURLOPT_HTTPHEADER => array(
				"content-type: application/json",
				"accept: application/json"
			  )
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			//echo $response;
			curl_close($curl);
			$result=json_decode($response, true);
		}catch(Exception $e){
			$result=array();
		}
		
		return $result;
	}
?>