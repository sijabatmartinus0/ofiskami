<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/html3/backofis/');
define('HTTP_CATALOG', 'http://localhost/html3/');
define('HTTP_CATALOG_LOCAL', 'http://localhost/');

// HTTPS
define('HTTPS_SERVER', 'https://localhost/html3/backofis/');
define('HTTPS_CATALOG', 'https://localhost/html3/');
define('HTTPS_CATALOG_LOCAL', 'https://localhost/');

// DIR
$appdir = dirname(dirname(__FILE__));
define('DIR_APPLICATION', $appdir . '/backofis/');
define('DIR_SYSTEM', $appdir . '/system/');
define('DIR_LANGUAGE', $appdir . '/backofis/language/');
define('DIR_TEMPLATE', $appdir . '/backofis/view/template/');
define('DIR_CONFIG', $appdir . '/system/config/');
define('DIR_IMAGE', $appdir . '/image/');
define('DIR_CACHE', $appdir . '/system/cache/');
define('DIR_DOWNLOAD', $appdir . '/system/download/');
define('DIR_UPLOAD', $appdir . '/system/upload/');
define('DIR_UPLOAD_PRODUCT', $appdir . '/system/upload_product/');
define('DIR_UPLOAD_CUSTOMER', $appdir . '/system/upload_customer/');
define('DIR_LOGS', $appdir . '/system/logs/');
define('DIR_MODIFICATION', $appdir . '/system/modification/');
define('DIR_LIBRARY', $appdir . '/system/library/');
define('DIR_CATALOG', $appdir . '/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ofiskita_new');
define('DB_PREFIX', 'oc_');
