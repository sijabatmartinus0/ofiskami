<?php
class ControllerUploadCustomer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('upload/customer');

		$this->load->model('upload/customer');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_upload'] = $this->language->get('entry_upload');
		$data['entry_overwrite'] = $this->language->get('entry_overwrite');
		$data['entry_progress'] = $this->language->get('entry_progress');
		$data['entry_choose_file'] = $this->language->get('entry_choose_file');
		$data['help_upload'] = $this->language->get('help_upload');
		$data['help_customer'] = $this->language->get('help_customer');

		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_clear'] = $this->language->get('button_clear');
		$data['button_continue'] = $this->language->get('button_continue');
		
		$data['text_category'] = $this->language->get('text_category');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_choose'] = $this->language->get('text_choose');
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('upload/customer', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['token'] = $this->session->data['token'];

		$directories = glob(DIR_UPLOAD_CUSTOMER . 'temp-*', GLOB_ONLYDIR);

		if ($directories) {
			$data['error_warning'] = $this->language->get('error_temporary');
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		$data['customers'] = $this->model_upload_customer->getCustomers();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('upload/customer.tpl', $data));
	}
	
	public function upload_file() {
		$this->load->language('upload/customer');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/download')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();
				
				$filetypes = array("xls", "xlsx");

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . md5(mt_rand());

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_UPLOAD_CUSTOMER . $file); //pake enkripsi penamaan file nya
			// move_uploaded_file($this->request->files['file']['tmp_name'], DIR_UPLOAD_CUSTOMER . $filename);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_success_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function uploadProcess(){
		// $this->validate(__FUNCTION__);
		$data = $this->request->post;
		
		$data['token'] = $this->session->data['token'];
		
		$json = array();
		$this->load->language('upload/customer');
		$this->load->model('upload/customer');
		
		if (empty($data['filename'])) {
			$json['errors']['error_common'][] = $this->language->get('error_upload'); 
		}else if(!empty($data['filename'])){
			/* validasi format file */
			$check_format = $this->model_upload_customer->validateExcel($data['filename']);
			if(!$check_format){
				$json['errors']['error_common'][] = $this->language->get('error_format');
			}
			
			/* validasi email */
			$check_email = $this->model_upload_customer->readExcel($data['filename']);
				
			for($x = 0; $x < count($check_email); $x++){
				if(!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $check_email[$x][0])){
					$json['errors']['error_common'][] = $this->language->get('error_email') . ($x+2) . '<br/>';
				}
			}
		}
		if (empty($data['customer_group'])) {
			$json['errors']['customer_group'] = $this->language->get('error_customer'); 
		}
		
		if (empty($json['errors']) && empty($json['error'])) {
			$row_data = $this->model_upload_customer->readExcel($data['filename']);
			
			for($x = 0; $x < count($row_data); $x++){
				$this->model_upload_customer->saveUpload(
					array(
						'customer_group' 		=> $data['customer_group'],
						'email' 				=> $row_data[$x][0]
					)
				);
			}
			$this->session->data['success'] = $this->language->get('text_success_save');
		}
		
		$this->response->setOutput(json_encode($json));
	}
}

?>