<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

class ControllerPaymentSgopayment extends  Controller {
	private $error = array();
	
	public function index(){
		
		$this->language->load('payment/sgopayment');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('payment/sgopayment');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('sgopayment', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['sgopayment_geo_zone_id'])) {
			$data['sgopayment_geo_zone_id'] = $this->request->post['sgopayment_geo_zone_id'];
		} else {
			$data['sgopayment_geo_zone_id'] = $this->config->get('sgopayment_geo_zone_id'); 
		} 
		
		if (isset($this->request->post['sgopayment_sort_order'])) {
			$data['sgopayment_sort_order'] = $this->request->post['sgopayment_sort_order'];
		} else {
			$data['sgopayment_sort_order'] = $this->config->get('sgopayment_sort_order');
		}
		
		if (isset($this->request->post['sgopayment_id'])) {
			$data['sgopayment_sort_order'] = $this->request->post['sgopayment_sort_order'];
		} else {
			$data['sgopayment_sort_order'] = $this->config->get('sgopayment_sort_order');
		}
		
		if (isset($this->request->post['sgopayment_password'])) {
			$data['sgopayment_sort_order'] = $this->request->post['sgopayment_sort_order'];
		} else {
			$data['sgopayment_sort_order'] = $this->config->get('sgopayment_sort_order');
		}
		
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		
		$data['entry_bank'] = $this->language->get('entry_bank');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_total'] = $this->language->get('entry_total');
		
		$data['entry_sgopayment_id'] = $this->language->get('entry_sgopayment_id');
		$data['entry_sgopayment_password'] = $this->language->get('entry_sgopayment_password');
		
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['help_total'] = $this->language->get('help_total');
		$data['help_sgopaymentid'] = $this->language->get('help_sgopaymentid');
		$data['help_sgopayment_password'] = $this->language->get('help_sgopayment_password');
		
		$data['text_edit'] = $this->language->get('text_edit');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		
		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
		
		foreach ($languages as $language) {
			if (isset($this->error['bank_' . $language['language_id']])) {
				$data['error_bank_' . $language['language_id']] = $this->error['bank_' . $language['language_id']];
			} else {
				$data['error_bank_' . $language['language_id']] = '';
			}
		}
		
		$data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_payment'),
				'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('payment/sgopayment', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		$data['action'] = $this->url->link('payment/sgopayment', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('localisation/language');
		
		foreach ($languages as $language) {
			if (isset($this->request->post['sgopayment_bank_' . $language['language_id']])) {
				$data['sgopayment_bank_' . $language['language_id']] = $this->request->post['sgopayment_bank_' . $language['language_id']];
			} else {
				$data['sgopayment_bank_' . $language['language_id']] = $this->config->get('sgopayment_bank_' . $language['language_id']);
			}
		}
		
		$data['languages'] = $languages;
		
		if (isset($this->request->post['sgopayment_total'])) {
			$data['sgopayment_total'] = $this->request->post['sgopayment_total'];
		} else {
			$data['sgopayment_total'] = $this->config->get('sgopayment_total');
		}
		
		if (isset($this->request->post['sgopayment_id'])) {
			$data['sgopayment_id'] = $this->request->post['sgopayment_id'];
		} else {
			$data['sgopayment_id'] = $this->config->get('sgopayment_id');
		}
		
		if (isset($this->request->post['sgopayment_password'])) {
			$data['sgopayment_password'] = $this->request->post['sgopayment_password'];
		} else {
			$data['sgopayment_password'] = $this->config->get('sgopayment_password');
		}
		
		
		if (isset($this->request->post['sgopayment_order_status_id'])) {
			$data['sgopayment_order_status_id'] = $this->request->post['sgopayment_order_status_id'];
		} else {
			$data['sgopayment_order_status_id'] = $this->config->get('sgopayment_order_status_id');
		}
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['sgopayment_geo_zone_id'])) {
			$data['sgopayment_geo_zone_id'] = $this->request->post['sgopayment_geo_zone_id'];
		} else {
			$data['sgopayment_geo_zone_id'] = $this->config->get('sgopayment_geo_zone_id');
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		

		if (isset($this->request->post['sgopayment_status'])) {
			$data['sgopayment_status'] = $this->request->post['sgopayment_status'];
		} else {
			$data['sgopayment_status'] = $this->config->get('sgopayment_status');
		}
		
		if (isset($this->request->post['sgopayment_sort_order'])) {
			$data['sgopayment_sort_order'] = $this->request->post['sgopayment_sort_order'];
		} else {
			$data['sgopayment_sort_order'] = $this->config->get('sgopayment_sort_order');
		}
		
		$this->template = 'payment/sgopayment.tpl';
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']=$this->load->controller('common/footer');
		$data['header']=$this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view($this->template, $data));
	}
	
	
	public function install() {
		
		$this->load->model('payment/sgopayment');
		$this->load->model('setting/setting');
		$this->model_payment_sgopayment->install();
		
	}
	
	public function uninstall() {
		/*
		$this->load->model('payment/sgopayment');
		$this->model_payment_sgopyamennt->uninstall();*/
	}
	
	public function validate(){
		if (!$this->user->hasPermission('modify', 'payment/sgopayment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (empty($this->request->post['sgopayment_id'])) {
			$this->error['warning'] = $this->language->get('error_payment_id');
		}
		
		if (empty($this->request->post['sgopayment_password'])) {
			$this->error['warning'] = $this->language->get('error_password');
		}
		
		
		return !$this->error;
		
	}
	
	
	public function paymentReport(){
		
	}
	
	
	public function inquirytrx(){
		
	}
}