<?php
class ControllerPickupPointRequest extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('pickuppoint/request');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('pickuppoint/request');

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_branch'])) {
			$filter_branch = $this->request->get['filter_branch'];
		} else {
			$filter_branch = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'mss.nickname';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_branch'])) {
			$url .= '&filter_branch=' . urlencode(html_entity_decode($this->request->get['filter_branch'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('pickuppoint/request', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['requests'] = array();

		$filter_data = array(
			'filter_branch'	  => $filter_branch,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$request_total = $this->model_pickuppoint_request->getTotalRequests($filter_data);

		$results = $this->model_pickuppoint_request->getRequests($filter_data);

		foreach ($results as $result) {
			$data['requests'][] = array(
				'request_id' => $result['request_id'],
				'nickname'   => $result['nickname'],
				'branch'     => $result['branch'],
				'date_added' => $result['date_added'],
				'approval'   => $this->url->link('pickuppoint/request/approval', 'token=' . $this->session->data['token'] . '&request_id=' . $result['request_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_id'] = $this->language->get('column_id');
		$data['column_nickname'] = $this->language->get('column_nickname');
		$data['column_branch'] = $this->language->get('column_branch');
		$data['column_date'] = $this->language->get('column_date');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_branch'] = $this->language->get('entry_branch');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_branch'])) {
			$url .= '&filter_branch=' . urlencode(html_entity_decode($this->request->get['filter_branch'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_nickname'] = $this->url->link('pickuppoint/request', 'token=' . $this->session->data['token'] . '&sort=mss.nickname' . $url, 'SSL');
		$data['sort_branch'] = $this->url->link('pickuppoint/request', 'token=' . $this->session->data['token'] . '&sort=ppb.pp_branch_name' . $url, 'SSL');
		$data['sort_date'] = $this->url->link('pickuppoint/request', 'token=' . $this->session->data['token'] . '&sort=mspr.date_added' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['filter_branch'])) {
			$url .= '&filter_branch=' . urlencode(html_entity_decode($this->request->get['filter_branch'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $request_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('pickuppoint/request', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($request_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($request_total - $this->config->get('config_limit_admin'))) ? $request_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $request_total, ceil($request_total / $this->config->get('config_limit_admin')));

		$data['filter_branch'] = $filter_branch;

		$this->load->model('pickuppoint/branch');

		$data['branches'] = $this->model_pickuppoint_branch->getBranchs();
		
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('pickuppoint/request_list.tpl', $data));
	}

}