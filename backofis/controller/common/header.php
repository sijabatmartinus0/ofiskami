<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		/*vqmod change*/
		$data = array_merge(isset($data) ? $data : array(), $this->load->language('multiseller/multiseller'));
		$lang = "view/javascript/multimerch/datatables/lang/" . $this->config->get('config_admin_language') . ".lng";
		$data['dt_language'] = file_exists(DIR_APPLICATION . $lang) ? "'$lang'" : "undefined";
		/*-------------------*/
		
		$data['title'] = $this->document->getTitle();

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$this->load->language('common/header');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_order'] = $this->language->get('text_order');
		$data['text_order_status'] = $this->language->get('text_order_status');
		$data['text_checkout_status'] = $this->language->get('text_checkout_status');
		$data['text_expired_status'] = $this->language->get('text_expired_status');
		$data['text_complete_status'] = $this->language->get('text_complete_status');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_online'] = $this->language->get('text_online');
		$data['text_seller'] = $this->language->get('text_seller');
		$data['text_seller_fault'] = $this->language->get('text_seller_fault');
		$data['text_approval'] = $this->language->get('text_approval');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_stock'] = $this->language->get('text_stock');
		$data['text_review'] = $this->language->get('text_review');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_front'] = $this->language->get('text_front');
		$data['text_help'] = $this->language->get('text_help');
		$data['text_homepage'] = $this->language->get('text_homepage');
		$data['text_documentation'] = $this->language->get('text_documentation');
		$data['text_support'] = $this->language->get('text_support');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_confirm_payment'] = $this->language->get('text_confirm_payment');
		$data['text_withdrawal'] = $this->language->get('text_withdrawal');

		if (!isset($this->request->get['token']) || !isset($this->session->data['token']) && ($this->request->get['token'] != $this->session->data['token'])) {
			$data['logged'] = '';

			$data['home'] = $this->url->link('common/dashboard', '', 'SSL');
		} else {
			$data['logged'] = true;

			$data['home'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL');
			$data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');

			// Orders
			$this->load->model('sale/order');
			
			/*
			// Processing Orders
			$data['order_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));
			$data['order_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_processing_status')), 'SSL');
			*/
			
			// Checkout Orders
			$data['checkout_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => 1));
			$data['checkout_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . 1, 'SSL');
			
			// Expired Orders
			$data['expired_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => 14));
			$data['expired_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . 14, 'SSL');
			
			// Complete Orders
			$data['complete_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));
			$data['complete_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_complete_status')), 'SSL');
			
			// Confirm Payment Orders
			$data['confirm_payment_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_confirm_payment_status'))));
			$data['confirm_payment_status'] = $this->url->link('sale/confirm_payment', 'token=' . $this->session->data['token'] , 'SSL');

			// Returns
			$this->load->model('sale/return');

			$return_total = $this->model_sale_return->getTotalReturns(
				array(
						'filter_return_status_id' 	=> $this->config->get('config_return_status_id'),
						'config_limit_return' 		=> $this->config->get('config_limit_return')
					)
			);

			$data['return_total'] = $return_total;

			$data['return'] = $this->url->link('sale/return', 'token=' . $this->session->data['token'], 'SSL');

			// Customers
			$this->load->model('report/customer');

			$data['online_total'] = $this->model_report_customer->getTotalCustomersOnline();

			$data['online'] = $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], 'SSL');

			$this->load->model('sale/customer');

			$customer_total = $this->model_sale_customer->getTotalCustomers(array('filter_approved' => false));

			$data['customer_total'] = $customer_total;
			$data['customer_approval'] = $this->url->link('sale/customer', 'token=' . $this->session->data['token'] . '&filter_approved=0', 'SSL');
			
			$withdrawal_request = $this->model_sale_order->getWithdrawals("customer", Payment::STATUS_UNPAID);
			$data['withdrawal'] = $withdrawal_request;
			$data['customer_withdrawal'] = $this->url->link('sale/customer_withdrawal', 'token=' . $this->session->data['token'] . '&filter_date_start=&filter_date_end=&filter_status=' . Payment::STATUS_UNPAID , 'SSL');
			
			// Products
			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProducts(array('filter_quantity' => 0));

			$data['product_total'] = $product_total;

			$data['product'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&filter_quantity=0', 'SSL');

			// Reviews
			$this->load->model('catalog/review');

			$review_total = $this->model_catalog_review->getTotalReviews(array('filter_status' => false));

			$data['review_total'] = $review_total;

			$data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&filter_status=0', 'SSL');

			// Affliate
			$this->load->model('marketing/affiliate');

			$affiliate_total = $this->model_marketing_affiliate->getTotalAffiliates(array('filter_approved' => false));

			$data['affiliate_total'] = $affiliate_total;
			$data['affiliate_approval'] = $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . '&filter_approved=1', 'SSL');
	
			// Seller

			$seller_fault_total = $this->MsLoader->MsSeller->getSellerFaultReachLimit();
			
			$data['seller_fault_total'] = $seller_fault_total;
			$data['seller_fault'] = $this->url->link('multiseller/seller', 'token=' . $this->session->data['token'], 'SSL');
			
			$payment_request = $this->model_sale_order->getWithdrawals("seller", Payment::STATUS_UNPAID);
			$data['payment'] = $payment_request;
			$data['seller_payment'] = $this->url->link('multiseller/payment', 'token=' . $this->session->data['token'] . '&filter_date_start=&filter_date_end=&filter_status=' . Payment::STATUS_UNPAID , 'SSL');
			
			$data['alerts'] = $data['order_status_total'] + $data['complete_status_total'] + $data['online_total'] + $return_total + $customer_total + $product_total + $review_total + $affiliate_total + $seller_fault_total;

			// Online Stores
			$data['stores'] = array();

			$data['stores'][] = array(
				'name' => $this->config->get('config_name'),
				'href' => HTTP_CATALOG
			);

			$this->load->model('setting/store');

			$results = $this->model_setting_store->getStores();

			foreach ($results as $result) {
				$data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}
		}

		return $this->load->view('common/header.tpl', $data);
	}
}