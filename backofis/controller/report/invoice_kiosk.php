<?php
class ControllerReportInvoiceKiosk extends Controller {
	const BATCH_PREFIX = "B03-";
	
	public function index() {
		$this->load->language('report/invoice_kiosk');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('report/invoice_kiosk');
		$this->load->model('pickuppoint/branch');
		
		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}
		
		if (isset($this->request->get['filter_kiosk'])) {
			$filter_kiosk = $this->request->get['filter_kiosk'];
		} else {
			$filter_kiosk = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}
		
		if (isset($this->request->get['filter_kiosk'])) {
			$url .= '&filter_kiosk=' . $this->request->get['filter_kiosk'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/invoice_kiosk', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['heading_title'] = $this->language->get('heading_title');
			
		$data['text_list'] = $this->language->get('text_billdue_list');
		$data['text_batch'] = $this->language->get('text_batch');
		$data['text_kiosk'] = $this->language->get('text_kiosk');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['column_batch'] = $this->language->get('column_batch');
		$data['column_kiosk'] = $this->language->get('column_kiosk');
		$data['column_invoice'] = $this->language->get('column_invoice');
		$data['column_action'] = $this->language->get('column_action');
		
		$data['button_print'] = $this->language->get('button_print');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_back'] = $this->language->get('button_back');
		
		$data['token'] = $this->session->data['token'];
		$data['print'] = $this->url->link('report/invoice_kiosk/invoice', '', 'SSL');
		
		$data['batchs'] = $this->model_report_invoice_kiosk->getBatchs();
		$data['kiosks'] = $this->model_pickuppoint_branch->getBranchs();

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['invoices'] = array();

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'filter_kiosk'	     => $filter_kiosk
		);

		$invoice_total = $this->model_report_invoice_kiosk->getTotalInvoice($filter_data);

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'filter_kiosk'	     => $filter_kiosk,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);
		
		$invoices = $this->model_report_invoice_kiosk->getInvoices($filter_data);

		foreach ($invoices as $invoice) {
			$data['invoices'][] = array(
				'sale_id' 		=> $invoice['sale_id'],
				'kiosk_id' 		=> $invoice['kiosk_id'],
				'batch_id' 		=> $invoice['batch_id'],
				'kiosk_name' 		=> $invoice['kiosk_name'],
				'invoice' 		=> $invoice['invoice']
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/invoice_vendor', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));

		$data['filter_batch'] = $filter_batch;
		$data['filter_kiosk'] = $filter_kiosk;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/invoice_kiosk.tpl', $data));
	}
	
	public function invoice(){
		$data=array();
		
		if (isset($this->request->get['sale_id'])) {
			$sale_id = $this->request->get['sale_id'];
		} else {
			$sale_id = 0;
		}
		
		if (isset($this->request->get['kiosk_id'])) {
			$kiosk_id = $this->request->get['kiosk_id'];
		} else {
			$kiosk_id = 0;
		}
		
		$this->load->model('report/invoice_kiosk');
		
		$invoice = $this->model_report_invoice_kiosk->getSale(array('sale_id'=>$sale_id,'kiosk_id'=>$kiosk_id));
		if($invoice){
			$sale_detail_info = $this->model_report_invoice_kiosk->getSaleDetail(array('sale_id'=>$sale_id,'kiosk_id'=>$kiosk_id));
			
			/*$this->Sale->create_sales_items_temp_table();

			$this->sale_lib->clear_all();
			$sale_info = $this->Sale->get_info($sale_id)->row_array();*/
			$data['cart']=array();
			
			foreach($sale_detail_info as $detail){
				$sale_item = $this->model_report_invoice_kiosk->getSaleItems(array('sale_id'=>$sale_id,'kiosk_id'=>$kiosk_id));
			
				//$this->sale_lib->copy_entire_sale($sale_id,$detail->sale_detail_id);
				
				$data['voucher'] = $invoice['voucher'];
				$data['coupon'] = $invoice['coupon'];
			
				//$cart = $this->sale_lib->get_cart();
				foreach($sale_item as $item){
					$item_info=$this->Item->get_info($item['item_id']);
					$supplier = $this->Supplier->get_info($item_info->supplier_id);
					$data['cart'][$key]=$item;
					$data['cart'][$key]['npwp_number'] = $supplier->npwp_number;
					$data['cart'][$key]['agency_name'] = $supplier->agency_name;
					$data['cart'][$key]['invoice_number'] = $sale_info['invoice_number'];
				}
				$supplier = $this->Supplier->get_info($detail->supplier_id);
				$data['npwp_number'] = $supplier->npwp_number;
				$data['agency_name'] = $supplier->agency_name;
				$data['payments'] = $this->sale_lib->get_payments();
				$data['subtotal'] = $this->sale_lib->get_subtotal();
				$data['discounted_subtotal'] = $this->sale_lib->get_subtotal(TRUE);
				$data['tax_exclusive_subtotal'] = $this->sale_lib->get_subtotal(TRUE, TRUE);
				$data['taxes'] = $this->sale_lib->get_taxes();
				$data['total'] = $this->sale_lib->get_total();
				$data['discount'] = $this->sale_lib->get_discount();
				$data['receipt_title'] = $this->lang->line('sales_receipt');
				$data['transaction_time'] = date($this->config->item('dateformat') . ' ' . $this->config->item('timeformat'), strtotime($sale_info['sale_time']));
				$data['transaction_date'] = date($this->config->item('dateformat'), strtotime($sale_info['sale_time']));
				$data['show_stock_locations'] = $this->Stock_location->show_locations('sales');
				$customer_id = $this->sale_lib->get_customer();
				$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
				$emp_info = $this->Employee->get_info($employee_id);
				$data['amount_change'] = $this->sale_lib->get_amount_due() * -1;
				$data['amount_due'] = $this->sale_lib->get_amount_due();
				$data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;
			
				if($customer_id != -1)
				{
					$cust_info = $this->Customer->get_info($customer_id);
					if (isset($cust_info->company_name))
					{
						$data['customer'] = $cust_info->company_name;
					}
					else
					{
						$data['customer'] = $cust_info->first_name.' '.$cust_info->last_name;
					}
					$data['first_name'] = $cust_info->first_name;
					$data['last_name'] = $cust_info->last_name;
					$data['customer_address'] = $cust_info->address_1;
					$data['customer_location'] = $cust_info->zip . ' ' . $cust_info->city;
					$data['customer_email'] = $cust_info->email;
					$data['account_number'] = $cust_info->account_number;
					$data['customer_info'] = implode("\n", array(
						$data['customer'],
						$data['customer_address'],
						$data['customer_location'],
						$data['account_number']
					));
				}
				$data['sale_id'] = '#'.$sale_id;
				$data['comments'] = $sale_info['comment'];
				$data['company_info'] = implode("\n", array(
					$this->config->item('address'),
					$this->config->item('phone'),
					$this->config->item('account_number')
				));
				$data['barcode'] = $this->barcode_lib->generate_receipt_barcode($data['sale_id']);
				$data['print_after_sale'] = $is_print;
			}
			$this->response->setOutput($this->load->view('report/invoice_kiosk_print.tpl', $data));
		}
	}
}
