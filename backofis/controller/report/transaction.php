<?php
class ControllerReportTransaction extends Controller {
	const BATCH_PREFIX = "B03-";
	
	public function index() {
		$this->load->language('report/invoice_vendor');

		$this->document->setTitle($this->language->get('heading_title4'));

		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}
		
		if (isset($this->request->get['filter_seller'])) {
			$filter_seller = $this->request->get['filter_seller'];
		} else {
			$filter_seller = '';
		}
		
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '';
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}
		
		if (isset($this->request->get['filter_seller'])) {
			$url .= '&filter_seller=' . $this->request->get['filter_seller'];
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title4'),
			'href' => $this->url->link('report/transaction', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/invoice_vendor');
		$this->load->model('upload/product');
		$this->load->model('localisation/order_status');
		
		$data['heading_title3'] = $this->language->get('heading_title4');
			
		$data['text_list'] = $this->language->get('text_transaction_list');
		$data['text_batch'] = $this->language->get('text_batch');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_merchant'] = $this->language->get('text_merchant');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');

		$data['column_order_no'] = $this->language->get('column_order_no');
		$data['column_vendor_name'] = $this->language->get('column_vendor_name');
		$data['column_vendor_type'] = $this->language->get('column_vendor_type');
		$data['column_kiosk_id'] = $this->language->get('column_kiosk_id');
		$data['column_transaction_date'] = $this->language->get('column_transaction_date');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_sku'] = $this->language->get('column_sku');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total_report'] = $this->language->get('column_total_report');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_commission'] = $this->language->get('column_commission');
		$data['column_shipping'] = $this->language->get('column_shipping');
		$data['column_online_payment'] = $this->language->get('column_online_payment');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_vendor_balance'] = $this->language->get('column_vendor_balance');
		$data['column_payment_type'] = $this->language->get('column_payment_type');
		$data['column_revenue_type'] = $this->language->get('column_revenue_type');
		$data['column_business_type'] = $this->language->get('column_business_type');
		$data['column_tax_type'] = $this->language->get('column_tax_type');
		$data['column_relation_type'] = $this->language->get('column_relation_type');
		$data['column_payment_total'] = $this->language->get('column_payment_total');
		$data['column_bank'] = $this->language->get('column_bank');
		$data['column_status'] = $this->language->get('text_status');
		
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_batch'] = $this->language->get('column_batch');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_download'] = $this->language->get('column_download');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_online_payment'] = $this->language->get('column_online_payment');

		$data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/transaction/view_detail', '', 'SSL');
		$data['download_report'] = $this->url->link('report/transaction/download_report', '', 'SSL');
		
		$data['sellers'] = $this->model_report_invoice_vendor->getSellers();
		$data['batchs'] = $this->model_report_invoice_vendor->getBatchs();
		$data['statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['invoices'] = array();

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'filter_seller'	     => $filter_seller,
			'filter_status'	     => $filter_status,
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);

		//$invoice_total = $this->model_report_invoice_vendor->getTotalTransaction($filter_data);

		$invoices = $this->model_report_invoice_vendor->getTransaction($filter_data);

		$invoice_total = sizeof($invoices);
		
		foreach ($invoices as $invoice) {
			$data['invoices'][] = array(
				'invoice'=> $invoice['invoice'],
				'nickname'=> $invoice['nickname'],
				'relation_type'=> $invoice['relation_type'],
				'tax_type'=> $invoice['tax_type'],
				'kiosk_name'=> $invoice['kiosk_name'],
				'revenue_type'=> $invoice['revenue_type'],
				'payment_type'=> $invoice['payment_type'],
				'account_name'=> $invoice['account_name'],
				'date_added'=> $invoice['date_added'],
				'quantity'=> $invoice['quantity'],
				'sku'=> $invoice['sku'],
				'price'=> $this->currency->format($invoice['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total'=> $this->currency->format($invoice['total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'payment_total'=> $this->currency->format($invoice['payment_total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax'=> $this->currency->format($invoice['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'commission_base'=> $this->currency->format($invoice['commission_base'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'shipping_fee'=> $this->currency->format($invoice['shipping_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee'=> $this->currency->format($invoice['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission'=> $this->currency->format($invoice['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission_tax'=> $this->currency->format($invoice['store_commission_tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'status'=> $invoice['status']
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/transaction', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));

		$data['filter_batch'] = $filter_batch;
		$data['filter_seller'] = $filter_seller;
		$data['filter_status'] = $filter_status;
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/transaction.tpl', $data));
	}
	
	public function download_report(){
		$this->load->model('report/invoice_vendor');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}
		
		if (isset($this->request->get['filter_seller'])) {
			$filter_seller = $this->request->get['filter_seller'];
		} else {
			$filter_seller = '';
		}
		
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '';
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
		
		$this->model_report_invoice_vendor->reportTransaction($filter_batch,$filter_seller,$filter_status,$filter_date_start,$filter_date_end);
	}	
}