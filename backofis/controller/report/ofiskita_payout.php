<?php
class ControllerReportOfiskitaPayout extends Controller {
	const BATCH_PREFIX = "B03-";
	
	private $error = array();
	
	public function index() {
		$this->load->language('report/invoice_vendor');

		$this->document->setTitle($this->language->get('heading_title2'));
		$this->load->model('report/invoice_vendor');
		if ((isset($this->request->get['paid']) && ($this->request->get['paid'] == 'true' || $this->request->get['paid'] == true)) && $this->validateForm()) {
			if (isset($this->request->get['seller_id'])) {
				$seller_id = $this->request->get['seller_id'];
			} else {
				$seller_id = 0;
			}
			if (isset($this->request->get['batch_id'])) {
				$batch_id = $this->request->get['batch_id'];
			} else {
				$batch_id = 0;
			}
			$this->model_report_invoice_vendor->paid($batch_id,$seller_id);

			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}

		if (isset($this->request->get['filter_merchant'])) {
			$filter_merchant = $this->request->get['filter_merchant'];
		} else {
			$filter_merchant = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}

		if (isset($this->request->get['filter_merchant'])) {
			$url .= '&filter_merchant=' . $this->request->get['filter_merchant'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title2'),
			'href' => $this->url->link('report/ofiskita_payout', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		
		$this->load->model('upload/product');
		
		$data['heading_title2'] = $this->language->get('heading_title2');
			
		$data['text_list'] = $this->language->get('text_payout_list');
		$data['text_batch'] = $this->language->get('text_batch');
		$data['text_merchant'] = $this->language->get('text_merchant');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_batch'] = $this->language->get('column_batch');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_download'] = $this->language->get('column_download');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_tax'] = $this->language->get('column_tax');

		$data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_paid'] = $this->language->get('button_paid');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/ofiskita_payout/view_detail', '', 'SSL');
		$data['payout'] = $this->url->link('report/ofiskita_payout', '', 'SSL');
		$data['download_report'] = $this->url->link('report/ofiskita_payout/download_report', '', 'SSL');
		
		$data['sellers'] = $this->model_report_invoice_vendor->getSellers();
		$data['batchs'] = $this->model_report_invoice_vendor->getBatchs();

		$data['invoices'] = array();

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'filter_merchant' 	 => $filter_merchant,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);

		$invoice_total = $this->model_report_invoice_vendor->getTotalInvoice($filter_data);

		$invoices = $this->model_report_invoice_vendor->getInvoiceVendor($filter_data);

		foreach ($invoices as $invoice) {
			$data['invoices'][] = array(
				'batch' 		=> ControllerReportOfiskitaPayout::BATCH_PREFIX . $invoice['seller_id'] . "-" . $invoice['batch_id'],
				'batch_id' 		=> $invoice['batch_id'],
				'is_download' 	=> $invoice['is_download'],
				'is_paid' 		=> ($invoice['date_paid']==NULL || $invoice['date_paid']=='' ? false : true),
				'merchant' 		=> $invoice['nickname'],
				'seller_id' 	=> $invoice['seller_id'],
				'total_nominal' => $this->currency->format($invoice['total_nominal'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission' => $this->currency->format($invoice['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 			=> $this->currency->format($invoice['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/ofiskita_payout', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));

		$data['filter_merchant'] = $filter_merchant;
		$data['filter_batch'] = $filter_batch;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/ofiskita_payout.tpl', $data));
	}
	
	public function view_detail(){
		$this->load->language('report/invoice_vendor');
		
		$this->document->setTitle($this->language->get('heading_title2'));
		
		$this->load->model('report/invoice_vendor');
		if ((isset($this->request->get['paid']) && ($this->request->get['paid'] == 'true' || $this->request->get['paid'] == true)) && $this->validateForm()) {
			if (isset($this->request->get['seller_id'])) {
				$seller_id = $this->request->get['seller_id'];
			} else {
				$seller_id = 0;
			}
			if (isset($this->request->get['batch_id'])) {
				$batch_id = $this->request->get['batch_id'];
			} else {
				$batch_id = 0;
			}
			$this->model_report_invoice_vendor->paid($batch_id,$seller_id);

			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['heading_title2'] = $this->language->get('heading_title2');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['column_order_no'] = $this->language->get('column_order_no');
		$data['column_vendor_name'] = $this->language->get('column_vendor_name');
		$data['column_vendor_type'] = $this->language->get('column_vendor_type');
		$data['column_kiosk_id'] = $this->language->get('column_kiosk_id');
		$data['column_payment_type'] = $this->language->get('column_payment_type');
		$data['column_transaction_date'] = $this->language->get('column_transaction_date');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_sku'] = $this->language->get('column_sku');
		$data['column_code'] = $this->language->get('column_code');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total_report'] = $this->language->get('column_total_report');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_commission'] = $this->language->get('column_commission');
		$data['column_shipping'] = $this->language->get('column_shipping');
		$data['column_online_payment'] = $this->language->get('column_online_payment');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_vendor_balance'] = $this->language->get('column_vendor_balance');
		
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_paid'] = $this->language->get('button_paid');
		$url = '';
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		$data['payout'] = $this->url->link('report/ofiskita_payout/view_detail',  'token=' . $this->session->data['token'] . $url . '&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$data['seller_id'] = $seller_id;
		$data['batch_id'] = $batch_id;
		
		$data['token'] = $this->session->data['token'];
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		if ($query->num_rows) {
			$data['nickname']  = $query->row['nickname'];
		}else{
			$data['nickname']  = "-";
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title2'),
			'href' => $this->url->link('report/ofiskita_payout', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $data['nickname'],
			'href' => $this->url->link('report/ofiskita_payout/view_detail', 'token=' . $this->session->data['token'] . $url . '&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL')
		);
		
		$filter_data = array(
			'seller_id'		=> $seller_id,
			'batch_id'		=> $batch_id,
			'filter_merchant'		=> $seller_id,
			'filter_batch'		=> $batch_id,
			'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'         => $this->config->get('config_limit_admin')
		);
		
		$invoices = $this->model_report_invoice_vendor->getInvoiceVendor($filter_data);
		
		if(!empty($invoices)){
			$data['is_paid']=($invoices[0]['date_paid']==NULL || $invoices[0]['date_paid']=='' ? false : true);
		}else{
			$data['is_paid']=true;
		}
		
		$detail_total = $this->model_report_invoice_vendor->getTotalDetail($filter_data);
		
		$data['details'] = array();
		
		$details = $this->model_report_invoice_vendor->getReportDetail($filter_data);
		
		$total_shipping_fee = 0;
		$total_vendor_balance = 0;
		
		foreach ($details as $detail) {
			// $total_shipping_fee += $this->model_report_invoice_vendor->getShippingPrice($detail['order_id']);
			$total_vendor_balance += $detail['vendor_balance'];
			
			$data['details'][] = array(
				'summary_id' 			=> $detail['summary_id'],
				'batch_id' 				=> $detail['batch_id'],
				'order_id' 				=> $detail['order_id'],
				'order_detail_id' 		=> $detail['order_detail_id'],
				'invoice' 				=> $detail['invoice'],
				'kiosk_id' 				=> $detail['kiosk_id'],
				'kiosk_name' 			=> $detail['kiosk_name'],
				'seller_id' 			=> $detail['seller_id'],
				'nickname' 				=> $detail['nickname'],
				'tax_type' 			=> $detail['tax_type'],
				'payment_type' 			=> $detail['payment_type'],
				'transaction_date' 		=> date('d/m/Y H:i', strtotime($detail['transaction_date'])),
				'date_added' 			=> date('d/m/Y H:i', strtotime($detail['date_added'])),
				'product_id' 			=> $detail['product_id'],
				'quantity' 				=> $detail['quantity'],
				'sku' 					=> $detail['sku'],
				'code' 					=> $detail['code'],
				'price' 				=> $this->currency->format($detail['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total' 				=> $this->currency->format($detail['total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 					=> $this->currency->format($detail['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'commission_base' 		=> $this->currency->format($detail['commission_base'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'shipping_fee' 			=> $this->currency->format($detail['shipping_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee'	=> $this->currency->format($detail['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission'		=> $this->currency->format($detail['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission_tax'	=> $this->currency->format($detail['store_commission_tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'vendor_balance'		=> $this->currency->format($detail['vendor_balance'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		// $data['final_vendor_balance'] = $this->currency->format(($total_shipping_fee+$total_vendor_balance), $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		$data['final_vendor_balance'] = $this->currency->format($total_vendor_balance, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		
		$pagination = new Pagination();
		$pagination->total = $detail_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/ofiskita_payout/view_detail', 'token=' . $this->session->data['token'] . $url . '&page={page}' .'&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($detail_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($detail_total - $this->config->get('config_limit_admin'))) ? $detail_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $detail_total, ceil($detail_total / $this->config->get('config_limit_admin')));
		
		$data['download_report'] = $this->url->link('report/ofiskita_payout/download_report', '', 'SSL');
		$data['link_back'] = $this->url->link('report/ofiskita_payout', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('report/ofiskita_payout_form.tpl', $data));
	}
	
	public function download_report(){
		$this->load->model('report/invoice_vendor');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		$this->model_report_invoice_vendor->reportOfiskitaPayout($seller_id, $batch_id);
	}	
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'report/ofiskita_payout')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}