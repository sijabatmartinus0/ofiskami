<?php
class ControllerReportInvoiceSummary extends Controller {
	const BATCH_PREFIX = "B03-";
	
	public function index() {
		$this->load->language('report/invoice_vendor');

		$this->document->setTitle($this->language->get('heading_title6'));
		$this->load->model('report/invoice_vendor');
		
		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title6'),
			'href' => $this->url->link('report/invoice_summary', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['heading_title'] = $this->language->get('heading_title6');
			
		$data['text_list'] = $this->language->get('text_summary_list');
		$data['text_batch'] = $this->language->get('text_batch');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['column_batch'] = $this->language->get('column_batch');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_download'] = $this->language->get('column_download');
		$data['column_transaction_fee'] = $this->language->get('column_transaction_fee');
		$data['column_commission_fee'] = $this->language->get('column_commission_fee');

		$data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_created'] = $this->language->get('button_created');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/invoice_summary/view_detail', '', 'SSL');
		$data['billing'] = $this->url->link('report/invoice_summary', '', 'SSL');
		$data['download_report'] = $this->url->link('report/invoice_summary/download_report', '', 'SSL');

		$data['batchs'] = $this->model_report_invoice_vendor->getBatchs();

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['invoices'] = array();

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);

		$invoice_total = $this->model_report_invoice_vendor->getTotalInvoiceSummary($filter_data);

		$invoices = $this->model_report_invoice_vendor->getInvoiceSummary($filter_data);

		foreach ($invoices as $invoice) {
			$data['invoices'][] = array(
				'batch' 		=> ControllerReportInvoiceSummary::BATCH_PREFIX . $invoice['seller_id'] . "-" . $invoice['batch_id'],
				'batch_id' 		=> $invoice['batch_id'],
				'merchant' 		=> $invoice['nickname'],
				'seller_id' 	=> $invoice['seller_id'],
				'is_created' 		=> ($invoice['date_created']==NULL || $invoice['date_created']=='' ? false : true),
				'is_download' 	=> $this->model_report_invoice_vendor->isDownload(
					array(
						'seller_id'		=> $invoice['seller_id'],
						'batch_id'		=> $invoice['batch_id']
					)
				),
				'total_nominal' => $this->currency->format($invoice['total_nominal'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission' => $this->currency->format($invoice['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee' => $this->currency->format($invoice['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 			=> $this->currency->format($invoice['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/invoice_summary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));
		$data['filter_batch'] = $filter_batch;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/invoice_summary.tpl', $data));
	}
	
	public function view_detail(){
		$this->load->language('report/invoice_vendor');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('report/invoice_vendor');
		
		$data['heading_title'] = $this->language->get('heading_title6');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['column_vendor_name'] = $this->language->get('column_vendor_name');
		$data['column_bank'] = $this->language->get('column_bank');
		$data['column_bank_account'] = $this->language->get('column_bank_account');
		$data['column_total_transaction'] = $this->language->get('column_total_transaction');
		$data['column_total_fee'] = $this->language->get('column_total_fee');
		$data['text_add_deposit'] = $this->language->get('text_add_deposit');
		$data['text_vendor_balance'] = $this->language->get('text_vendor_balance');
		
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$url = '';
	
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['billing'] = $this->url->link('report/invoice_summary/view_detail', 'token=' . $this->session->data['token'] . $url . '&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL');
		
		$data['batch_id'] = $batch_id;
		
		$data['token'] = $this->session->data['token'];
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/invoice_summary', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $data['nickname'],
			'href' => $this->url->link('report/invoice_summary/view_detail', 'token=' . $this->session->data['token'] . $url . '&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL')
		);
		
		$filter_data = array(
			'batch_id'		=> $batch_id,
			'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'         => $this->config->get('config_limit_admin')
		);

		
		$detail_total = $this->model_report_invoice_vendor->getTotalInvoiceSummaryDetail($filter_data);
		
		$data['details'] = array();
		
		$details = $this->model_report_invoice_vendor->getInvoiceSummaryDetail($filter_data);
		
		foreach ($details as $detail) {
			$data['details'][] = array(
				'nickname' 			=> $detail['nickname'],
				'bank' 				=> $detail['bank'],
				'bank_account' 					=> $detail['bank_account'],
				'total_transaction' 				=> $this->currency->format($detail['total_transaction'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total_fee' 				=> $this->currency->format($detail['total_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'add_deposit' 					=> $this->currency->format($detail['add_deposit'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total_refund' 		=> $this->currency->format($detail['total_refund'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $detail_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/invoice_summary/view_detail', 'token=' . $this->session->data['token'] . $url . '&page={page}' .'&seller_id=' . $seller_id . '&batch_id=' . $batch_id, 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($detail_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($detail_total - $this->config->get('config_limit_admin'))) ? $detail_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $detail_total, ceil($detail_total / $this->config->get('config_limit_admin')));
		
		$data['download_report'] = $this->url->link('report/invoice_summary/download_report', '', 'SSL');
		$data['link_back'] = $this->url->link('report/invoice_summary', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('report/invoice_summary_form.tpl', $data));
	}
	
	public function download_report(){
		$this->load->model('report/invoice_vendor');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		$this->model_report_invoice_vendor->reportInvoiceSummary($batch_id);
	}
}
