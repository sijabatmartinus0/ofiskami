<?php
class ControllerReportOfiskitaIncome extends Controller {
	const BATCH_PREFIX = "B03-";
	
	public function index() {
		$this->load->language('report/invoice_vendor');

		$this->document->setTitle($this->language->get('heading_title3'));

		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}
		
		if (isset($this->request->get['filter_seller'])) {
			$filter_seller = $this->request->get['filter_seller'];
		} else {
			$filter_seller = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}
		
		if (isset($this->request->get['filter_seller'])) {
			$url .= '&filter_seller=' . $this->request->get['filter_seller'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title3'),
			'href' => $this->url->link('report/ofiskita_income', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/invoice_vendor');
		$this->load->model('upload/product');
		
		$data['heading_title3'] = $this->language->get('heading_title3');
			
		$data['text_list'] = $this->language->get('text_list');
		$data['text_batch'] = $this->language->get('text_batch');
		$data['text_merchant'] = $this->language->get('text_merchant');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_batch'] = $this->language->get('column_batch');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_download'] = $this->language->get('column_download');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_online_payment'] = $this->language->get('column_online_payment');

		$data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/ofiskita_income/view_detail', '', 'SSL');
		$data['download_report'] = $this->url->link('report/ofiskita_income/download_report', '', 'SSL');
		
		$data['sellers'] = $this->model_report_invoice_vendor->getSellers();
		$data['batchs'] = $this->model_report_invoice_vendor->getBatchs();

		$data['invoices'] = array();

		$filter_data = array(
			'filter_batch'	     => $filter_batch,
			'filter_seller'	     => $filter_seller,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);

		$invoice_total = $this->model_report_invoice_vendor->getTotalIncome($filter_data);

		$invoices = $this->model_report_invoice_vendor->getIncome($filter_data);

		foreach ($invoices as $invoice) {
			$data['invoices'][] = array(
				'batch_id' 		=> $invoice['batch_id'],
				'seller_id' 		=> $invoice['seller_id'],
				'nickname' 		=> $invoice['nickname'],
				'is_download' 	=> $invoice['is_download'],
				'tax' 			=> $this->currency->format($invoice['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission' => $this->currency->format($invoice['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee' => $this->currency->format($invoice['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $invoice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/ofiskita_income', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($invoice_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($invoice_total - $this->config->get('config_limit_admin'))) ? $invoice_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $invoice_total, ceil($invoice_total / $this->config->get('config_limit_admin')));

		$data['filter_batch'] = $filter_batch;
		$data['filter_seller'] = $filter_seller;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/ofiskita_income.tpl', $data));
	}
	
	public function view_detail(){
		$this->load->language('report/invoice_vendor');
		$this->load->model('report/invoice_vendor');
		
		$this->document->setTitle($this->language->get('heading_title3'));
		
		$data['heading_title3'] = $this->language->get('heading_title3');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_order_no'] = $this->language->get('column_order_no');
		$data['column_vendor_name'] = $this->language->get('column_vendor_name');
		$data['column_vendor_type'] = $this->language->get('column_vendor_type');
		$data['column_kiosk_id'] = $this->language->get('column_kiosk_id');
		$data['column_payment_type'] = $this->language->get('column_payment_type');
		$data['column_transaction_date'] = $this->language->get('column_transaction_date');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_sku'] = $this->language->get('column_sku');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total_report'] = $this->language->get('column_total_report');
		$data['column_tax'] = $this->language->get('column_tax');
		$data['column_commission'] = $this->language->get('column_commission');
		$data['column_shipping'] = $this->language->get('column_shipping');
		$data['column_online_payment'] = $this->language->get('column_online_payment');
		$data['column_ofiskita_commission'] = $this->language->get('column_ofiskita_commission');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_vendor_balance'] = $this->language->get('column_vendor_balance');
		$data['column_tax_ppn'] = $this->language->get('column_tax_ppn');
		$data['column_online_payment'] = $this->language->get('column_online_payment');
		
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$url = '';
		
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$data['batch_id'] = $batch_id;
		$data['seller_id'] = $seller_id;
		
		$data['token'] = $this->session->data['token'];
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title3'),
			'href' => $this->url->link('report/ofiskita_income', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $batch_id,
			'href' => $this->url->link('report/ofiskita_income/view_detail', 'token=' . $this->session->data['token'] . $url .  '&batch_id=' . $batch_id, 'SSL')
		);
		
		$filter_data = array(
			'batch_id'		=> $batch_id,
			'seller_id'		=> $seller_id,
			'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'         => $this->config->get('config_limit_admin')
		);
		
		$detail_total = $this->model_report_invoice_vendor->getTotalDetail($filter_data);
		
		$data['details'] = array();
		
		$details = $this->model_report_invoice_vendor->getReportDetail($filter_data);
		
		$total_store_commission = 0;
		$total_store_commission_tax = 0;
		$total_online_payment = 0;
		
		foreach ($details as $detail) {
			$total_store_commission += $detail['store_commission'];
			$total_store_commission_tax += $detail['store_commission_tax'];
			$total_online_payment += $detail['online_payment_fee'];
			
			$data['details'][] = array(
				'summary_id' 			=> $detail['summary_id'],
				'batch_id' 				=> $detail['batch_id'],
				'order_id' 				=> $detail['order_id'],
				'order_detail_id' 		=> $detail['order_detail_id'],
				'invoice' 				=> $detail['invoice'],
				'kiosk_id' 				=> $detail['kiosk_id'],
				'kiosk_name' 			=> $detail['kiosk_name'],
				'seller_id' 			=> $detail['seller_id'],
				'nickname' 				=> $detail['nickname'],
				'tax_type' 			=> $detail['tax_type'],
				'payment_type' 			=> $detail['payment_type'],
				'transaction_date' 		=> date('d/m/Y H:i', strtotime($detail['transaction_date'])),
				'date_added' 			=> date('d/m/Y H:i', strtotime($detail['date_added'])),
				'product_id' 			=> $detail['product_id'],
				'quantity' 				=> $detail['quantity'],
				'sku' 					=> $detail['sku'],
				'price' 				=> $this->currency->format($detail['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total' 				=> $this->currency->format($detail['total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 					=> $this->currency->format($detail['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'commission_base' 		=> $this->currency->format($detail['commission_base'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'shipping_fee' 			=> $this->currency->format($detail['shipping_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee'	=> $this->currency->format($detail['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission'		=> $this->currency->format($detail['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission_tax'	=> $this->currency->format($detail['store_commission_tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'vendor_balance'		=> $this->currency->format($detail['vendor_balance'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		
		$data['total_store_commission'] = $this->currency->format($total_store_commission, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		$data['total_store_commission_tax'] = $this->currency->format($total_store_commission_tax, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		$data['total_online_payment'] = $this->currency->format($total_online_payment, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		
		$pagination = new Pagination();
		$pagination->total = $detail_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/ofiskita_income/view_detail', 'token=' . $this->session->data['token'] . $url . '&page={page}' . '&batch_id=' . $batch_id, 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($detail_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($detail_total - $this->config->get('config_limit_admin'))) ? $detail_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $detail_total, ceil($detail_total / $this->config->get('config_limit_admin')));
		
		$data['download_report'] = $this->url->link('report/ofiskita_income/download_report', '', 'SSL');
		$data['link_back'] = $this->url->link('report/ofiskita_income', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('report/ofiskita_income_form.tpl', $data));
	}
	
	public function download_report(){
		$this->load->model('report/invoice_vendor');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['batch_id'])) {
			$batch_id = $this->request->get['batch_id'];
		} else {
			$batch_id = 0;
		}
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		$this->model_report_invoice_vendor->reportOfiskitaIncome($batch_id,$seller_id);
	}	
}