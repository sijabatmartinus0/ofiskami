<?php
class ControllerReportCustomerActivity extends Controller {
	public function index() {
		$this->load->language('report/customer_activity');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}

		if (isset($this->request->get['filter_ip'])) {
			$filter_ip = $this->request->get['filter_ip'];
		} else {
			$filter_ip = null;
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode($this->request->get['filter_customer']);
		}

		if (isset($this->request->get['filter_ip'])) {
			$url .= '&filter_ip=' . $this->request->get['filter_ip'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('report/customer_activity', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'text' => $this->language->get('heading_title')
		);

		$this->load->model('report/customer');

		$data['activities'] = array();

		$filter_data = array(
			'filter_customer'   => $filter_customer,
			'filter_ip'         => $filter_ip,
			'filter_date_start'	=> $filter_date_start,
			'filter_date_end'	=> $filter_date_end,
			'start'             => ($page - 1) * 20,
			'limit'             => 20
		);

		$activity_total = $this->model_report_customer->getTotalCustomerActivities($filter_data);

		$results = $this->model_report_customer->getCustomerActivities($filter_data);

		foreach ($results as $result) {
			$comment = vsprintf($this->language->get('text_' . $result['key']), unserialize($result['data']));

			$find = array(
				'customer_id=',
				'order_id='
			);

			$replace = array(
				$this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=', 'SSL'),
				$this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=', 'SSL')
			);

			$data['activities'][] = array(
				'comment'    => str_replace($find, $replace, $comment),
				'ip'         => $result['ip'],
				'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added']))
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_comment'] = $this->language->get('column_comment');
		$data['column_ip'] = $this->language->get('column_ip');
		$data['column_date_added'] = $this->language->get('column_date_added');

		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_ip'] = $this->language->get('entry_ip');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode($this->request->get['filter_customer']);
		}

		if (isset($this->request->get['filter_ip'])) {
			$url .= '&filter_ip=' . $this->request->get['filter_ip'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		$pagination = new Pagination();
		$pagination->total = $activity_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/customer_activity', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($activity_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($activity_total - $this->config->get('config_limit_admin'))) ? $activity_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $activity_total, ceil($activity_total / $this->config->get('config_limit_admin')));

		$data['filter_customer'] = $filter_customer;
		$data['filter_ip'] = $filter_ip;
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/customer_activity.tpl', $data));
	}

	//unverified
	public function customer(){
		$this->load->language('customer/customer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('report/verified');
		$this->getCustomer();
	}

	public function getCustomer(){
		if (isset($this->request->get['filter_nama'])) {
			$filter_nama = $this->request->get['filter_nama'];
		} else {
			$filter_nama = null;
		}

		if (isset($this->request->get['filter_date_start'])) {
	           $filter_date_start = $this->request->get['filter_date_start'];
	    } else {
	        $filter_date_start = '';
	    }

	    if (isset($this->request->get['filter_date_end'])) {
	        $filter_date_end = $this->request->get['filter_date_end'];
	    } else {
	        $filter_date_end = '';
	    }

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'telephone';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		if (isset($this->request->get['filter_nama'])) {
			$url .= '&filter_nama=' . $this->request->get['filter_nama'];
		}

		if (isset($this->request->get['filter_date_start'])) {
	        $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
	    }

	    if (isset($this->request->get['filter_date_end'])) {
	        $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
	    }

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['customer'] = array();
	    $filter_data = array(
			'filter_nama'            => $filter_nama,
			'filter_date_start'  	 => $filter_date_start,
			'filter_date_end'		=> $filter_date_end,
			'sort'                 	 => $sort,
			'order'                	 => $order,
			'start'                	 => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                	 => $this->config->get('config_limit_admin')
		);
		// var_dump($filter_data); die();         
		$customer_total = $this->model_report_verified->getTotalCustomers($filter_data);

		$results = $this->model_report_verified->getCustomer($filter_data);
		// var_dump($total); die();
		foreach ($results as $result) {
			$data['customer'][] = array(
				'nama'      => $result['nama'],
				'email'      => $result['email'],
				'telephone'        => $result['telephone'],
				'date_added'      => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
			);
			
		}
		$data['heading_title'] = $this->language->get('heading_title');
			
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_delete'] = $this->language->get('text_delete');
		$data['text_missing'] = $this->language->get('text_missing');
		$data['column_customer_no'] = $this->language->get('column_customer_no');
		$data['column_customer_nama'] = $this->language->get('column_customer_nama');
		$data['column_customer_email'] = $this->language->get('column_customer_email');
		$data['column_customer_telephone'] = $this->language->get('column_customer_telephone');
		$data['column_customer_date_added'] = $this->language->get('column_customer_date_added');
	        
			
		$data['entry_customer_nama'] = $this->language->get('entry_customer_nama');
		$data['entry_customer_date_start'] = $this->language->get('entry_customer_date_start');
		$data['entry_customer_date_end'] = $this->language->get('entry_customer_date_end');

			
		$data['button_filter'] = $this->language->get('button_filter');
			
		$data['button_download'] = $this->language->get('button_download');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_nama'])) {
			$url .= '&filter_nama=' . $this->request->get['filter_nama'];
		}
			
		if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['nama'] = $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . '&sort=nama' . $url, 'SSL');
		$data['email'] = $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		$data['telephone'] = $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . '&sort=telephone' . $url, 'SSL');
		$data['date_added'] = $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		if (isset($this->request->get['filter_nama'])) {
			$url .= '&filter_nama=' . $this->request->get['filter_nama'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
			
		$data['download_report'] = $this->url->link('report/customer_activity/download_list_customer', 'token=' . $this->session->data['token'] . $url, 'SSL');
			
		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/customer_activity/customer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

		$data['filter_nama']         = $filter_nama;
		$data['filter_date_start']  = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/customer_verified.tpl', $data));
		}

	function download_list_customer(){
        $this->load->model('report/verified');
        $datas = $this->request->post;
        $filter_data = array(
          	'filter_nama'	=> $datas['filter_nama'],
			'filter_date_start'   => $datas['filter_date_start'],
			'filter_date_end'  	 => $datas['filter_date_end']
        );
        $results = $this->model_report_verified->getCustomer($filter_data);
        $this->model_report_verified->exportToExcel($results);
    }
}