<?php
class ControllerReportMerchant extends Controller {
	public function index() {
		$this->load->language('report/merchant');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_period'])) {
			$filter_period = $this->request->get['filter_period'];
		} else {
			$filter_period = '1';
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = '';
		}

		if (isset($this->request->get['filter_merchant'])) {
			$filter_merchant = $this->request->get['filter_merchant'];
		} else {
			$filter_merchant = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_period'])) {
			$url .= '&filter_period=' . $this->request->get['filter_period'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_merchant'])) {
			$url .= '&filter_merchant=' . $this->request->get['filter_merchant'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/merchant', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/merchant');

		$data['merchants'] = array();

		$filter_data = array(
			'filter_year'	     => $filter_year,
			'filter_period'	     => $filter_period,
			'filter_merchant' 	 => $filter_merchant,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);

		$merchant_total = $this->model_report_merchant->getTotalMerchant($filter_data);

		$merchants = $this->model_report_merchant->getMerchantList($filter_data);

		foreach ($merchants as $merchant) {
			$data['merchants'][] = array(
				'nickname' 		=> $merchant['nickname'],
				'seller_id' 	=> $merchant['seller_id'],
				'total_order'   => $merchant['total_order'],
				'score'			=> $this->model_report_merchant->finalScore($merchant['seller_id'], $filter_period, $filter_year)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
			
		$data['text_list'] = $this->language->get('text_list');
		$data['text_period'] = $this->language->get('text_period');
		$data['text_year'] = $this->language->get('text_year');
		$data['text_merchant'] = $this->language->get('text_merchant');
		$data['text_month_first'] = $this->language->get('text_month_first');
		$data['text_month_second'] = $this->language->get('text_month_second');

		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_score'] = $this->language->get('column_score');
		$data['column_clicks'] = $this->language->get('column_clicks');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/merchant/view_detail', '', 'SSL');
		$data['download_report'] = $this->url->link('report/merchant/download_report', '', 'SSL');

		$pagination = new Pagination();
		$pagination->total = $merchant_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/merchant', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($merchant_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($merchant_total - $this->config->get('config_limit_admin'))) ? $merchant_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $merchant_total, ceil($merchant_total / $this->config->get('config_limit_admin')));

		$data['filter_period'] = $filter_period;
		$data['filter_year'] = $filter_year;
		$data['filter_merchant'] = $filter_merchant;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/merchant.tpl', $data));
	}
	public function sku() {
		$this->load->language('report/merchant');

		$this->document->setTitle($this->language->get('heading_title_1'));

		if (isset($this->request->get['filter_merchant'])) {
                    $filter_merchant = $this->request->get['filter_merchant'];
                } else {
                    $filter_merchant = 0;
                }
                
                if (isset($this->request->get['filter_date_start'])) {
                    $filter_date_start = $this->request->get['filter_date_start'];
                } else {
                    $filter_date_start = '';
                }

                if (isset($this->request->get['filter_date_end'])) {
                    $filter_date_end = $this->request->get['filter_date_end'];
                } else {
                    $filter_date_end = '';
                }
                
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_merchant'])) {
			$url .= '&filter_merchant=' . $this->request->get['filter_merchant'];
		}
                
                if (isset($this->request->get['filter_date_start'])) {
                    $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
                }

                if (isset($this->request->get['filter_date_end'])) {
                    $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
                }
                
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_1'),
			'href' => $this->url->link('report/merchant/sku', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/merchant');

		$data['merchants'] = array();

		$filter_data = array(
                        'filter_date_start' => $filter_date_start,
                        'filter_date_end' => $filter_date_end,
			'filter_merchant' 	 => $filter_merchant,
			'start'              => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              => $this->config->get('config_limit_admin')
		);
                $filter_data_total = array(
                    'filter_merchant' 	 => $filter_merchant
                );
		$merchant_total = $this->model_report_merchant->getTotalMerchantFilter($filter_data);
                //var_dump($merchant_total);die();
		$merchants = $this->model_report_merchant->getSkuMerchant($filter_data);
//                echo '<pre>';
//                var_dump($merchants);
//                die();
                
                foreach($total_skus as $total_sku){
                    $data['total_skus'][] = array(
				'nickname' 	=> $total_sku['nickname'],
				'seller_id' 	=> $total_sku['seller_id'],
                                'total'   => $total_sku['total'],
			);
                }
		foreach ($merchants as $merchant) {
			$data['merchants'][] = array(
				'nickname' 		=> $merchant['nickname'],
				'seller_id' 	=> $merchant['seller_id'],
				//'date_created'   => date($this->language->get('date_format_short'), strtotime($merchant['date_created'])),
                                //'filter'   => $total_sku['total'],
                                'total'   => $merchant['total'],
                                'total1'   => $merchant['total1'],
				//'score'			=> $this->model_report_merchant->finalScore($merchant['seller_id'], $filter_period, $filter_year)
			);
		}
                
//                echo '<pre>';
//                var_dump($data['merchants']);
//                die();
		$data['heading_title'] = $this->language->get('heading_title_1');
                $data['text_list'] = $this->language->get('text_list');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_date_created'] = $this->language->get('text_date_created');
		$data['text_date_ended'] = $this->language->get('text_ended');
		$data['text_merchant'] = $this->language->get('text_merchant');
		$data['text_month_first'] = $this->language->get('text_month_first');
		$data['text_month_second'] = $this->language->get('text_month_second');

		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['column_date_created'] = $this->language->get('column_date_created');
                $data['column_date_ended'] = $this->language->get('column_date_ended');
		$data['column_clicks'] = $this->language->get('column_clicks');
		$data['column_total_sku'] = $this->language->get('column_total_sku');
		$data['column_penambahan_sku'] = $this->language->get('column_penambahan_sku');
                $data['column_action'] = $this->language->get('column_action');
                $data['button_view_detail'] = $this->language->get('button_view_detail');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_download'] = $this->language->get('button_download');
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('report/merchant/view_detail', '', 'SSL');
		$data['download_report'] = $this->url->link('report/merchant/download_sku_report', 'token=' . $this->session->data['token'], 'SSL');
                $data['sellers'] = $this->model_report_merchant->getSellers();
		$pagination = new Pagination();
		$pagination->total = $merchant_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/merchant/sku', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($merchant_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($merchant_total - $this->config->get('config_limit_admin'))) ? $merchant_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $merchant_total, ceil($merchant_total / $this->config->get('config_limit_admin')));

		$data['filter_merchant'] = $filter_merchant;
                $data['filter_date_start'] = $filter_date_start;
                $data['filter_date_end'] = $filter_date_end;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
                $this->response->setOutput($this->load->view('report/sku_merchant.tpl', $data));
	}	
	// public function view_detail(){
		// $this->load->language('report/merchant');
		// $this->load->model('report/merchant');
		
		// $data['heading_title'] = $this->language->get('heading_title');
		// $data['column_number'] = $this->language->get('column_number');
		// $data['column_criteria'] = $this->language->get('column_criteria');
		// $data['column_weight'] = $this->language->get('column_weight');
		// $data['column_score_view'] = $this->language->get('column_score_view');
		// $data['column_final_score'] = $this->language->get('column_final_score');
		// $data['column_total_score'] = $this->language->get('column_total_score');
		// $data['column_hundred'] = $this->language->get('column_hundred');
		// $data['column_merchant'] = $this->language->get('column_merchant');
		
		// $data['criteria_1'] = $this->language->get('criteria_1');
		// $data['criteria_2'] = $this->language->get('criteria_2');
		// $data['criteria_3'] = $this->language->get('criteria_3');
		// $data['criteria_4'] = $this->language->get('criteria_4');
		// $data['criteria_5'] = $this->language->get('criteria_5');
		// $data['criteria_6'] = $this->language->get('criteria_6');
		
		// $data['button_download'] = $this->language->get('button_download');
		
		// if (isset($this->request->get['seller_id'])) {
			// $seller_id = $this->request->get['seller_id'];
		// } else {
			// $seller_id = 0;
		// }
		
		// if (isset($this->request->get['period'])) {
			// $period = $this->request->get['period'];
		// } else {
			// $period = 0;
		// }
		
		// if (isset($this->request->get['year'])) {
			// $year = $this->request->get['year'];
		// } else {
			// $year = '';
		// }
		
		// $data['seller_id'] = $seller_id;
		// $data['period'] = $period;
		// $data['year'] = $year;
		// $data['token'] = $this->session->data['token'];
		
		// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		// if ($query->num_rows) {
			// $data['nickname']  = $query->row['nickname'];
		// }else{
			// $data['nickname']  = "-";
		// }
		
		// $data['weight1'] = $this->config->get('config_report_weight_approve_merchant');
		// $data['weight2'] = $this->config->get('config_report_weight_approve_sla');
		// $data['weight3'] = $this->config->get('config_report_weight_delivered_sla');
		// $data['weight4'] = $this->config->get('config_report_weight_review');
		// $data['weight5'] = $this->config->get('config_report_weight_review_accuracy');
		// $data['weight6'] = $this->config->get('config_report_weight_complain');
		
		// $data['download_report'] = $this->url->link('report/merchant/download_report', '', 'SSL');
		
		// $data['score_approve_merchant'] = $this->model_report_merchant->getScoreApproveMerchant($seller_id, $period, $year);
		// $data['score_approve_sla'] = $this->model_report_merchant->getScoreApproveSla($seller_id, $period, $year);
		// $data['score_delivered_sla'] = $this->model_report_merchant->getScoreDeliveredSla($seller_id, $period, $year);
		// $data['score_review'] = $this->model_report_merchant->getScoreReview($seller_id, $period, $year);
		// $data['score_review_accuracy'] = $this->model_report_merchant->getScoreReviewAccuracy($seller_id, $period, $year);
		// $data['score_complain'] = $this->model_report_merchant->getScoreReturn($seller_id, $period, $year);
		
		// $data['total_score'] = sprintf("%0.2f", $data['score_approve_merchant'] + $data['score_approve_sla'] + $data['score_delivered_sla'] + $data['score_review'] + $data['score_review_accuracy'] + $data['score_complain']);
		
		// $data['final_approve_merchant'] = sprintf("%0.2f", ($this->config->get('config_report_weight_approve_merchant') * $this->model_report_merchant->getScoreApproveMerchant($seller_id, $period, $year)) / 100);
		// $data['final_approve_sla'] = sprintf("%0.2f", (($this->config->get('config_report_weight_approve_sla') * $this->model_report_merchant->getScoreApproveSla($seller_id, $period, $year)) / 100));
		// $data['final_delivered_sla'] = sprintf("%0.2f", (($this->config->get('config_report_weight_delivered_sla') * $this->model_report_merchant->getScoreDeliveredSla($seller_id, $period, $year)) / 100));
		// $data['final_review'] = sprintf("%0.2f", (($this->config->get('config_report_weight_review') * $this->model_report_merchant->getScoreReview($seller_id, $period, $year)) / 100));
		// $data['final_review_accuracy'] = sprintf("%0.2f", (($this->config->get('config_report_weight_review_accuracy') * $this->model_report_merchant->getScoreReviewAccuracy($seller_id, $period, $year)) / 100));
		// $data['final_complain'] = sprintf("%0.2f", (($this->config->get('config_report_weight_complain') * $this->model_report_merchant->getScoreReturn($seller_id, $period, $year)) / 100));
		
		// $data['final_score'] = $this->model_report_merchant->finalScore($seller_id, $period, $year);
		
		// $this->response->setOutput($this->load->view('report/merchant_evaluation.tpl', $data));
	// }
	
	public function view_detail(){
		$this->load->language('report/merchant');
		$this->load->model('report/merchant');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_period'] = $this->language->get('text_period');
		$data['text_month_first'] = $this->language->get('text_month_first');
		$data['text_month_second'] = $this->language->get('text_month_second');
		$data['text_order'] = $this->language->get('text_order');
		
		$data['column_number'] = $this->language->get('column_number');
		$data['column_criteria'] = $this->language->get('column_criteria');
		$data['column_weight'] = $this->language->get('column_weight');
		$data['column_score_view'] = $this->language->get('column_score_view');
		$data['column_final_score'] = $this->language->get('column_final_score');
		$data['column_total_score'] = $this->language->get('column_total_score');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_hundred'] = $this->language->get('column_hundred');
		$data['column_merchant'] = $this->language->get('column_merchant');
		
		$data['criteria_1'] = $this->language->get('criteria_1');
		$data['criteria_2'] = $this->language->get('criteria_2');
		$data['criteria_3'] = $this->language->get('criteria_3');
		$data['criteria_4'] = $this->language->get('criteria_4');
		$data['criteria_5'] = $this->language->get('criteria_5');
		$data['criteria_6'] = $this->language->get('criteria_6');
		
		$data['button_download'] = $this->language->get('button_download');
		$data['button_back'] = $this->language->get('button_back');
		$url = '';
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		if (isset($this->request->get['period'])) {
			$period = $this->request->get['period'];
		} else {
			$period = 0;
		}
		
		if (isset($this->request->get['year'])) {
			$year = $this->request->get['year'];
		} else {
			$year = '';
		}
		
		$data['seller_id'] = $seller_id;
		$data['period'] = $period;
		$data['year'] = $year;
		$data['token'] = $this->session->data['token'];
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		if ($query->num_rows) {
			$data['nickname']  = $query->row['nickname'];
		}else{
			$data['nickname']  = "-";
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/merchant', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $data['nickname'],
			'href' => $this->url->link('report/merchant/view_detail', 'token=' . $this->session->data['token'] . $url . '&seller_id=' . $seller_id . '&period=' . $period . '&year=' . $year, 'SSL')
		);
		
		$data['weight1'] = $this->config->get('config_report_weight_approve_merchant');
		$data['weight2'] = $this->config->get('config_report_weight_approve_sla');
		$data['weight3'] = $this->config->get('config_report_weight_delivered_sla');
		$data['weight4'] = $this->config->get('config_report_weight_review');
		$data['weight5'] = $this->config->get('config_report_weight_review_accuracy');
		$data['weight6'] = $this->config->get('config_report_weight_complain');
		
		$data['download_report'] = $this->url->link('report/merchant/download_report', '', 'SSL');
		$data['link_back'] = $this->url->link('report/merchant', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['score_approve_merchant'] = sprintf("%0.2f", $this->model_report_merchant->getScoreApproveMerchant($seller_id, $period, $year));
		$data['score_approve_sla'] = sprintf("%0.2f", $this->model_report_merchant->getScoreApproveSla($seller_id, $period, $year));
		$data['score_delivered_sla'] = sprintf("%0.2f", $this->model_report_merchant->getScoreDeliveredSla($seller_id, $period, $year));
		$data['score_review'] = sprintf("%0.2f", $this->model_report_merchant->getScoreReview($seller_id, $period, $year));
		$data['score_review_accuracy'] = sprintf("%0.2f", $this->model_report_merchant->getScoreReviewAccuracy($seller_id, $period, $year));
		$data['score_complain'] = sprintf("%0.2f", $this->model_report_merchant->getScoreReturn($seller_id, $period, $year));
		
		$data['total_score'] = sprintf("%0.2f", $data['score_approve_merchant'] + $data['score_approve_sla'] + $data['score_delivered_sla'] + $data['score_review'] + $data['score_review_accuracy'] + $data['score_complain']);
		
		$data['final_approve_merchant'] = sprintf("%0.2f", ($this->config->get('config_report_weight_approve_merchant') * $this->model_report_merchant->getScoreApproveMerchant($seller_id, $period, $year)) / 100);
		$data['final_approve_sla'] = sprintf("%0.2f", (($this->config->get('config_report_weight_approve_sla') * $this->model_report_merchant->getScoreApproveSla($seller_id, $period, $year)) / 100));
		$data['final_delivered_sla'] = sprintf("%0.2f", (($this->config->get('config_report_weight_delivered_sla') * $this->model_report_merchant->getScoreDeliveredSla($seller_id, $period, $year)) / 100));
		$data['final_review'] = sprintf("%0.2f", (($this->config->get('config_report_weight_review') * $this->model_report_merchant->getScoreReview($seller_id, $period, $year)) / 100));
		$data['final_review_accuracy'] = sprintf("%0.2f", (($this->config->get('config_report_weight_review_accuracy') * $this->model_report_merchant->getScoreReviewAccuracy($seller_id, $period, $year)) / 100));
		$data['final_complain'] = sprintf("%0.2f", (($this->config->get('config_report_weight_complain') * $this->model_report_merchant->getScoreReturn($seller_id, $period, $year)) / 100));
		
		$data['final_score'] = $this->model_report_merchant->finalScore($seller_id, $period, $year);
		
		$data['total_order'] = $this->model_report_merchant->getTotalOrder($seller_id, $period, $year);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('report/merchant_form.tpl', $data));
	}
	
	public function download_report(){
		$this->load->model('report/merchant');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = 0;
		}
		
		if (isset($this->request->get['period'])) {
			$period = $this->request->get['period'];
		} else {
			$period = 0;
		}
		
		if (isset($this->request->get['year'])) {
			$year = $this->request->get['year'];
		} else {
			$year = '';
		}
		
		$this->model_report_merchant->reportMerchantEvaluation($seller_id, $period, $year);
	}
	function download_sku_report(){
            $this->load->model('report/merchant');
            $datas = $this->request->post;
//            echo '<pre>';
//            var_dump($datas);
//            die();
            $filter_data = array(
                'filter_date_start' => $datas['filter_date_start'],
                'filter_date_end' => $datas['filter_date_end'],
                'filter_merchant' => $datas['filter_merchant']
            );
            
            $results = $this->model_report_merchant->getSkuMerchant($filter_data);
            


            $this->model_report_merchant->reportSkuMerchant($results);
        }		
}
