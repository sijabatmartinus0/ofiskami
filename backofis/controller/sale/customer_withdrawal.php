<?php
class ControllerSaleCustomerWithdrawal extends Controller{
	private $error = array();
	
	public function getTableData() {
		$this->load->language('sale/customer_withdrawal');
		$colMap = array(
			'customer' => 'ms.firstname',
			'type' => 'payment_type',
			'description' => 'mpay.description',
			'date_created' => 'mpay.date_created',
			'date_paid' => 'mpay.date_paid'
		);
		
		$sorts = array('payment_type', 'customer', 'amount', 'description', 'payment_status', 'date_created', 'date_paid');
		$filters = array_diff($sorts, array('payment_status', 'type'));
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);
		$filterTableParams = array();
		
		if (isset($this->request->get['filter_date_start'])) {
			$filterTableParams['filter_date_start']= $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filterTableParams['filter_date_end']= $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filterTableParams['filter_status']= $this->request->get['filter_status'];
		}
		
		$results = $this->payment->getPayments(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'filters_table' => $filterTableParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// actions
			$actions = "";
			if ($result['amount'] > 0 && $result['payment_status'] == Payment::STATUS_UNPAID && in_array($result['payment_type'], array(Payment::TYPE_PAYOUT, Payment::TYPE_PAYOUT_REQUEST))) {
			/*if (!empty($result['payment_data']) && filter_var($result['payment_data'], FILTER_VALIDATE_EMAIL)) { 
				$actions .= "<a class='ms-button ms-button-paypal' title='" . $this->language->get('text_payout_paypal') . "'></a>";
			} else {
				$actions .= "<a class='ms-button ms-button-paypal-bw' title='" . $this->language->get('text_payout_paypal_invalid') . "'></a>";
			}*/
			}
			if ($result['amount'] > 0 && $result['payment_status'] == Payment::STATUS_UNPAID) { 
				$actions .= "<a class='ms-button ms-button-mark' title='" . $this->language->get('text_mark') . "'></a>";
			}
			$actions .= "<a class='ms-button ms-button-delete' title='" . $this->language->get('text_delete') . "'></a>";
			
			// paymentstatus
			$paymentstatus = "<select name='ms-payment-status'>";
			
			$msPayment = new ReflectionClass('Payment');
			foreach ($msPayment->getConstants() as $cname => $cval) {
			if (strpos($cname, 'STATUS_') !== FALSE) {
				$paymentstatus .= "<option value='$cval'" . ($result['payment_status'] == $cval ? "selected='selected'" : '') . ">" . $this->language->get('text_status_' . $cval) . "</option>";
			}
			}
			$paymentstatus .= "
			</select>
			<span class='ms-button-small ms-button-apply ms-button-status' title='Save' />
			";

			
			$columns[] = array_merge(
				$result,
				array(
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['payment_id']}' />",
					'payment_type' => $this->language->get('text_type_' . $result['payment_type']),
					'customer' => "<a href='".$this->url->link('multiseller/seller/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')."'>{$result['firstname']}</a>",
					'amount' => $this->currency->format(abs($result['amount']),$result['currency_code']),
					'description' => (mb_strlen($result['mpay.description']) > 80 ? mb_substr($result['mpay.description'], 0, 80) . '...' : $result['mpay.description']),
					'payment_status' => $paymentstatus,
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['mpay.date_created'])),
					'date_paid' => $result['mpay.date_paid'] ? date($this->language->get('date_format_short'), strtotime($result['mpay.date_paid'])) : '',
					'actions' => $actions
				)
			);
		}
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}
	
	public function index() {
		$this->load->language('sale/customer_withdrawal');
		$this->document->addStyle('view/stylesheet/multimerch/multiseller.css');
		$this->document->addStyle('view/javascript/multimerch/datatables/css/jquery.dataTables.css');
		$this->document->addScript('view/javascript/multimerch/datatables/js/jquery.dataTables.min.js');
		$this->document->addScript('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');

		// paypal listing payment confirmation
		if (isset($this->request->post['payment_status']) && strtolower($this->request->post['payment_status']) == 'completed') {
			$this->data['success'] = $this->language->get('text_completed');
		}
				
		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
		
		$data['payout_requests']['amount_pending'] = $this->currency->format($this->payment->getTotalAmount(array(
			'payment_type' => array(Payment::TYPE_PAYOUT_REQUEST),
			'payment_status' => array(Payment::STATUS_UNPAID)
		)), $this->config->get('config_currency'));
		
		$data['payout_requests']['amount_paid'] = $this->currency->format($this->payment->getTotalAmount(array(
			'payment_type' => array(Payment::TYPE_PAYOUT_REQUEST),
			'payment_status' => array(Payment::STATUS_PAID)
		)), $this->config->get('config_currency'));

		$data['payouts']['amount_pending'] = $this->currency->format($this->payment->getTotalAmount(array(
			'payment_type' => array(Payment::TYPE_PAYOUT),
			'payment_status' => array(Payment::STATUS_UNPAID)
		)), $this->config->get('config_currency'));
		
		$data['payouts']['amount_paid'] = $this->currency->format($this->payment->getTotalAmount(array(
			'payment_type' => array(Payment::TYPE_PAYOUT),
			'payment_status' => array(Payment::STATUS_PAID)
		)), $this->config->get('config_currency'));
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		$data['token'] = $this->session->data['token'];		
		$data['heading'] = $this->language->get('text_heading');
		$this->document->setTitle($this->language->get('text_heading'));
		
		$data['text_payout_requests'] = $this->language->get('text_payout_requests');
		$data['text_payouts'] = $this->language->get('text_payouts');
		$data['text_pending'] = $this->language->get('text_pending');
		$data['text_new'] = $this->language->get('text_new');
		$data['text_paid'] = $this->language->get('text_paid');
		$data['text_deduct'] = $this->language->get('text_deduct');
		$data['text_payout_paypal'] = $this->language->get('text_payout_paypal');
		$data['text_payout_paypal_invalid'] = $this->language->get('text_payout_paypal_invalid');
		$data['text_mark'] = $this->language->get('text_mark');
		$data['text_delete'] = $this->language->get('text_delete');

		$data['text_type_' . Payment::TYPE_PAYOUT] = $this->language->get('text_type_' . Payment::TYPE_PAYOUT);
		$data['text_type_' . Payment::TYPE_PAYOUT_REQUEST] = $this->language->get('text_type_' . Payment::TYPE_PAYOUT_REQUEST);

		$data['text_status_' . Payment::STATUS_UNPAID] = $this->language->get('text_status_' . Payment::STATUS_UNPAID);
		$data['text_status_' . Payment::STATUS_PAID] = $this->language->get('text_status_' . Payment::STATUS_PAID);
		$data['text_filter_date_start'] = $this->language->get('text_filter_date_start');
		$data['text_filter_date_end'] = $this->language->get('text_filter_date_end');
		$data['text_filter_status'] = $this->language->get('text_filter_status');
		$data['text_button_filter'] = $this->language->get('text_button_filter');
		$data['text_button_export'] = $this->language->get('text_button_export');
		$data['error_fromto'] = $this->language->get('error_fromto');
		$data['error_fromstore'] = $this->language->get('error_fromstore');
		$data['error_tostore'] = $this->language->get('error_tostore');
		$data['error_norequests'] = $this->language->get('error_norequests');
		$data['success_created'] = $this->language->get('success_created');

		$data['label_customer'] = $this->language->get('label_customer');
		$data['label_all_sellers'] = $this->language->get('label_all_sellers');
		$data['label_amount'] = $this->language->get('label_amount');
		$data['label_product'] = $this->language->get('label_product');
		$data['label_net_amount'] = $this->language->get('label_net_amount');
		$data['label_days'] = $this->language->get('label_days');
		$data['label_from'] = $this->language->get('label_from');
		$data['label_to'] = $this->language->get('label_to');
		$data['label_paypal'] = $this->language->get('label_paypal');
		$data['label_date_created'] = $this->language->get('label_date_created');
		$data['label_status'] = $this->language->get('label_status');
		$data['label_image'] = $this->language->get('label_image');
		$data['label_date_modified'] = $this->language->get('label_date_modified');
		$data['label_date_paid'] = $this->language->get('label_date_paid');
		$data['label_date'] = $this->language->get('label_date');
		$data['label_description'] = $this->language->get('label_description');
		$data['error_payment_filter_date'] = $this->language->get('error_payment_filter_date');
		
		$data['label_enabled'] = $this->language->get('label_enabled');
		$data['label_disabled'] = $this->language->get('label_disabled');
		$data['label_apply'] = $this->language->get('label_apply');
		$data['label_action'] = $this->language->get('label_action');
		$data['label_type'] = $this->language->get('label_type');
		$data['label_type_checkbox'] = $this->language->get('label_type_checkbox');
		$data['label_type_date'] = $this->language->get('label_type_date');
		$data['label_type_datetime'] = $this->language->get('label_type_datetime');
		$data['label_type_file'] = $this->language->get('label_type_file');
		$data['label_type_image'] = $this->language->get('label_type_image');
		$data['label_type_radio'] = $this->language->get('label_type_radio');
		$data['label_type_select'] = $this->language->get('label_type_select');
		$data['label_type_text'] = $this->language->get('label_type_text');
		$data['label_type_textarea'] = $this->language->get('label_type_textarea');
		$data['label_type_time'] = $this->language->get('label_type_time');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		$data['label_store'] = $this->language->get('label_store');
		$data['label_id'] = $this->language->get('label_id');
		$data['label_payment_method'] = $this->language->get('label_payment_method');
		$data['label_payment_method_balance'] = $this->language->get('label_payment_method_balance');
		$data['label_payment_paid'] = $this->language->get('label_payment_paid');
		$data['label_payment_deduct'] = $this->language->get('label_payment_deduct');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payouts'),
			'href' => $this->url->link('sale/customer_withdrawal', '', 'SSL')
		);
		
		$data['link_report_payout'] = html_entity_decode($this->url->link('sale/customer_withdrawal/link_report_payout', 'token=' . $this->session->data['token']));
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('sale/customer_withdrawal.tpl', $data));
	}
	
	public function jxUpdateStatus() {
		$this->load->language('sale/customer_withdrawal');
		$data = $this->request->get;
		$json = array();
		if (isset($data['payment_id']) && isset($data['payment_status'])) {
			$this->payment->updatePayment($data['payment_id'], array(
				'payment_status' => $data['payment_status'],
				'date_paid' => ($data['payment_status'] == Payment::STATUS_PAID ? date( 'Y-m-d H:i:s') : NULL)
			));
			
			$payment = $this->payment->getPayments(array('payment_id' => $data['payment_id'], 'single' => 1));
			
			switch($data['payment_status']) {
				case Payment::STATUS_PAID:
					switch($payment['payment_type']) {
						
						case Payment::TYPE_PAYOUT:
						case Payment::TYPE_PAYOUT_REQUEST:
							// charge balance
							$this->balance->addBalanceEntry($payment['customer_id'], array(
								'withdrawal_id' => $payment['payment_id'],
								'balance_type' => Balance::BALANCE_TYPE_WITHDRAWAL,
								'amount' => -$payment['amount'],
								'description' => $payment['mpay.description']
							));
							break;
					}
					//Send Email
					$email=array();
					$email['text_customer'] = $this->language->get('text_customer');
					$email['text_withdrawal'] = $this->language->get('text_withdrawal');
					$email['text_amount'] = $this->language->get('text_amount');
					$email['text_date_added'] = $this->language->get('text_date_added');
					$email['text_description'] = $this->language->get('text_description');
					$email['text_withdrawal_link'] = $this->language->get('text_withdrawal_link');
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status');
					$email['text_withdrawal_request'] = $this->language->get('text_withdrawal_request');
					$email['text_footer'] = $this->language->get('text_footer');
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					
					$this->load->model('sale/customer');
					$customer= $this->model_sale_customer->getCustomer($payment['customer_id']);

					$email['store_url']=HTTP_CATALOG;
					$email['title']=$subject;
					$email['store_name']=$this->config->get('config_name');
					$email['logo']=HTTP_CATALOG . 'image/' . $this->config->get('config_logo');
					$email['withdrawal_status']=$this->language->get('text_status_'.$data['payment_status']);
					$email['withdrawal_link']=HTTP_CATALOG.'index.php?route=account/transaction';
					$email['customer']=$customer['firstname']." ".$customer['lastname'];
					$email['withdrawal']=$this->language->get('text_payout_requests');
					$email['date_added']=date($this->language->get('date_format_short'), strtotime($payment['mpay.date_created']));
					$email['amount']=$this->currency->format(abs($payment['amount']),$payment['currency_code']);
					$email['description']=(mb_strlen($payment['mpay.description']) > 80 ? mb_substr($payment['mpay.description'], 0, 80) . '...' : $payment['mpay.description']);
					
					$html=$this->load->view('mail/customer_withdrawal.tpl', $email);
					var_dump($this->config->get('config_mail'));die();
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($customer['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
					
					break;
			}
			
			$json['payment'] = array(
				'payment_type' => $payment['payment_type'],
				'payment_status' => $payment['payment_status'],
				'payment_date' => $payment['mpay.date_paid'] ? date($this->language->get('date_format_short'), strtotime($payment['mpay.date_paid'])) : ''
			);
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxDelete() {
		$this->load->language('sale/customer_withdrawal');
		$data = $this->request->get;
		$json = array();
		if (isset($data['payment_id'])) {
					$payment = $this->payment->getPayments(array('payment_id' => $data['payment_id'], 'single' => 1));
					
					//Send Email
					$email=array();
					$email['text_customer'] = $this->language->get('text_customer');
					$email['text_withdrawal'] = $this->language->get('text_withdrawal');
					$email['text_amount'] = $this->language->get('text_amount');
					$email['text_date_added'] = $this->language->get('text_date_added');
					$email['text_description'] = $this->language->get('text_description');
					$email['text_withdrawal_link'] = $this->language->get('text_withdrawal_link');
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status');
					$email['text_withdrawal_request'] = $this->language->get('text_withdrawal_request');
					$email['text_footer'] = $this->language->get('text_footer');
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					
					$this->load->model('sale/customer');
					$customer= $this->model_sale_customer->getCustomer($payment['customer_id']);

					$email['store_url']=HTTP_CATALOG;
					$email['title']=$subject;
					$email['store_name']=$this->config->get('config_name');
					$email['logo']=HTTP_CATALOG . 'image/' . $this->config->get('config_logo');
					$email['withdrawal_status']=$this->language->get('text_status_1')."/Reject";
					$email['withdrawal_link']=HTTP_CATALOG.'index.php?route=account/transaction';
					$email['customer']=$customer['firstname']." ".$customer['lastname'];
					$email['withdrawal']=$this->language->get('text_payout_requests');
					$email['date_added']=date($this->language->get('date_format_short'), strtotime($payment['mpay.date_created']));
					$email['amount']=$this->currency->format(abs($payment['amount']),$payment['currency_code']);
					$email['description']=(mb_strlen($payment['mpay.description']) > 80 ? mb_substr($payment['mpay.description'], 0, 80) . '...' : $payment['mpay.description']);
					
					$html=$this->load->view('mail/customer_withdrawal.tpl', $email);
	
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($customer['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
					
			$this->payment->deletePayment($data['payment_id']);
			$this->session->data['success'] = $this->language->get('success_payment_deleted');
		}		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function link_report_payout(){

		$filterTableParams = array();
		
		if (isset($this->request->get['filter_date_start'])) {
			$filterTableParams['filter_date_start']= $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filterTableParams['filter_date_end']= $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$filterTableParams['filter_status']= $this->request->get['filter_status'];
		}
		
		$this->payment->exportPayout(
			array(),
			array(
				'filters_table' => $filterTableParams
			)
		);
	}
	
	// public function create() {
		// $this->load->language('sale/customer_withdrawal');
		// $this->document->addStyle('view/stylesheet/multimerch/multiseller.css');
		
		// $this->load->model('sale/customer');
		
		// $results = $this->model_sale_customer->getCustomers();
	
		// foreach ($results as $r) {
			// $data['customers'][] = array(
				// 'name' => "{$r['name']}",
				// 'customer_id' => $r['customer_id']
				// );
		// }
		
		// $msPayment = new ReflectionClass('Payment');
		// foreach ($msPayment->getConstants() as $cname => $cval) {
			// if (strpos($cname, 'TYPE_') !== FALSE && !in_array($cname, array('TYPE_RECURRING', 'TYPE_SALE'))) {
				// $data['payment_types'][$cval] = $this->language->get('text_type_' . $cval);
			// }
		// }
		
		// $data['token'] = $this->session->data['token'];		
		// $data['heading'] = $this->language->get('text_heading');
		// $this->document->setTitle($this->language->get('text_heading'));
		
		// $this->load->model('setting/setting');
		// $store_info = $this->model_setting_setting->getSetting('config', 0);
		
		// $data['store_name'] = $store_info['config_name'];
		// $data['text_payout_requests'] = $this->language->get('text_payout_requests');
		// $data['text_payouts'] = $this->language->get('text_payouts');
		// $data['text_pending'] = $this->language->get('text_pending');
		// $data['text_new'] = $this->language->get('text_new');
		// $data['text_paid'] = $this->language->get('text_paid');
		// $data['text_deduct'] = $this->language->get('text_deduct');
		// $data['text_payout_paypal'] = $this->language->get('text_payout_paypal');
		// $data['text_payout_paypal_invalid'] = $this->language->get('text_payout_paypal_invalid');
		// $data['text_mark'] = $this->language->get('text_mark');
		// $data['text_delete'] = $this->language->get('text_delete');
		// $data['text_type_' . Payment::TYPE_SIGNUP] = $this->language->get('text_type_' . Payment::TYPE_SIGNUP);
		// $data['text_type_' . Payment::TYPE_LISTING] = $this->language->get('text_type_' . Payment::TYPE_LISTING);
		// $data['text_type_' . Payment::TYPE_PAYOUT] = $this->language->get('text_type_' . Payment::TYPE_PAYOUT);
		// $data['text_type_' . Payment::TYPE_PAYOUT_REQUEST] = $this->language->get('text_type_' . Payment::TYPE_PAYOUT_REQUEST);
		// $data['text_type_' . Payment::TYPE_FAULT] = $this->language->get('text_type_' . Payment::TYPE_FAULT);
		// $data['text_type_' . Payment::TYPE_NON_COOPERATION] = $this->language->get('text_type_' . Payment::TYPE_NON_COOPERATION);
		// $data['text_type_' . Payment::TYPE_DEPOSIT_FEE] = $this->language->get('text_type_' . Payment::TYPE_DEPOSIT_FEE);
		// $data['text_type_' . Payment::TYPE_RECURRING] = $this->language->get('text_type_' . Payment::TYPE_RECURRING);
		// $data['text_type_' . Payment::TYPE_SALE] = $this->language->get('text_type_' . Payment::TYPE_SALE);
		// $data['text_status_' . Payment::STATUS_UNPAID] = $this->language->get('text_status_' . Payment::STATUS_UNPAID);
		// $data['text_status_' . Payment::STATUS_PAID] = $this->language->get('text_status_' . Payment::STATUS_PAID);
		// $data['text_filter_date_start'] = $this->language->get('text_filter_date_start');
		// $data['text_filter_date_end'] = $this->language->get('text_filter_date_end');
		// $data['text_filter_status'] = $this->language->get('text_filter_status');
		// $data['text_button_filter'] = $this->language->get('text_button_filter');
		// $data['text_button_export'] = $this->language->get('text_button_export');
		// $data['error_fromto'] = $this->language->get('error_fromto');
		// $data['error_fromstore'] = $this->language->get('error_fromstore');
		// $data['error_tostore'] = $this->language->get('error_tostore');
		// $data['error_norequests'] = $this->language->get('error_norequests');
		// $data['success_created'] = $this->language->get('success_created');

		// $data['label_seller'] = $this->language->get('label_seller');
		// $data['label_all_sellers'] = $this->language->get('label_all_sellers');
		// $data['label_amount'] = $this->language->get('label_amount');
		// $data['label_product'] = $this->language->get('label_product');
		// $data['label_net_amount'] = $this->language->get('label_net_amount');
		// $data['label_days'] = $this->language->get('label_days');
		// $data['label_from'] = $this->language->get('label_from');
		// $data['label_to'] = $this->language->get('label_to');
		// $data['label_paypal'] = $this->language->get('label_paypal');
		// $data['label_date_created'] = $this->language->get('label_date_created');
		// $data['label_status'] = $this->language->get('label_status');
		// $data['label_image'] = $this->language->get('label_image');
		// $data['label_date_modified'] = $this->language->get('label_date_modified');
		// $data['label_date_paid'] = $this->language->get('label_date_paid');
		// $data['label_date'] = $this->language->get('label_date');
		// $data['label_description'] = $this->language->get('label_description');
		// $data['error_payment_filter_date'] = $this->language->get('error_payment_filter_date');
		
		// $data['label_enabled'] = $this->language->get('label_enabled');
		// $data['label_disabled'] = $this->language->get('label_disabled');
		// $data['label_apply'] = $this->language->get('label_apply');
		// $data['label_action'] = $this->language->get('label_action');
		// $data['label_type'] = $this->language->get('label_type');
		// $data['label_type_checkbox'] = $this->language->get('label_type_checkbox');
		// $data['label_type_date'] = $this->language->get('label_type_date');
		// $data['label_type_datetime'] = $this->language->get('label_type_datetime');
		// $data['label_type_file'] = $this->language->get('label_type_file');
		// $data['label_type_image'] = $this->language->get('label_type_image');
		// $data['label_type_radio'] = $this->language->get('label_type_radio');
		// $data['label_type_select'] = $this->language->get('label_type_select');
		// $data['label_type_text'] = $this->language->get('label_type_text');
		// $data['label_type_textarea'] = $this->language->get('label_type_textarea');
		// $data['label_type_time'] = $this->language->get('label_type_time');
		// $data['text_image_manager'] = $this->language->get('text_image_manager');
		// $data['text_browse'] = $this->language->get('text_browse');
		// $data['text_clear'] = $this->language->get('text_clear');
		// $data['label_store'] = $this->language->get('label_store');
		// $data['label_id'] = $this->language->get('label_id');
		// $data['label_payment_method'] = $this->language->get('label_payment_method');
		// $data['label_payment_method_balance'] = $this->language->get('label_payment_method_balance');
		// $data['label_payment_paid'] = $this->language->get('label_payment_paid');
		// $data['label_payment_deduct'] = $this->language->get('label_payment_deduct');
		// $data['button_save'] = $this->language->get('button_save');
		// $data['button_cancel'] = $this->language->get('button_cancel');

		// $data['breadcrumbs'] = array();

		// $data['breadcrumbs'][] = array(
			// 'text' => $this->language->get('text_home'),
			// 'href' => $this->url->link('common/dashboard')
		// );

		// $data['breadcrumbs'][] = array(
			// 'text' => $this->language->get('text_payouts'),
			// 'href' => $this->url->link('sale/customer_withdrawal', '', 'SSL')
		// );
		
		// $data['breadcrumbs'][] = array(
			// 'text' => $this->language->get('text_payouts_create'),
			// 'href' => $this->url->link('sale/customer_withdrawal/create', '', 'SSL')
		// );
		
		
		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['footer'] = $this->load->controller('common/footer');
		// $data['header'] = $this->load->controller('common/header');
		// $this->response->setOutput($this->load->view('sale/customer_withdrawal_form.tpl', $data));
	// }
	
	public function getBallance(){
		$data = $this->request->get;
		$json = array();
		if (isset($data['customer_id'])) {
			$json['amount']=(int)$this->balance->getSellerBalance($data['customer_id']);
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	// public function jxSave() {
		// $this->load->language('sale/customer_withdrawal');
		// $json = array();
		// $data = $this->request->post['payment'];
		
		// if ((!$data['from'] && !$data['to']) || ($data['from'] && $data['to'])) {
			// $json['errors']['payment[from]'] = $this->language->get('error_payment_fromto');
			// $json['errors']['payment[to]'] = $this->language->get('error_payment_fromto');
		// }

		// if (!$data['from'] && !in_array($data['type'], array(Payment::TYPE_PAYOUT, Payment::TYPE_PAYOUT_REQUEST, Payment::TYPE_FAULT,Payment::TYPE_NON_COOPERATION))) {
			// $json['errors']['payment[type]'] = $this->language->get('error_payment_fromstore');
		// } else if ($data['from'] && in_array($data['type'], array(Payment::TYPE_PAYOUT, Payment::TYPE_PAYOUT_REQUEST))) {
			// $json['errors']['payment[type]'] = $this->language->get('error_payment_tostore');
		// }
		
		// if ((float)$data['amount'] <= 0) {
			// $json['errors']['payment[amount]'] = $this->language->get('error_payment_amount');
		// }
	
		// if (empty($json['errors'])) {
			
			// $payment_id = $this->payment->createPayment(array(
				// 'customer_id' => ($data['from'] ? $data['from'] : $data['to']),
				// 'payment_type' => $data['type'],
				// 'payment_status' => (isset($data['paid']) && $data['paid'] ? 2 : 1),
				// 'payment_method' => $data['method'],
				// 'user_id' => $this->user->getId(),
				// 'amount' => $data['amount'],
				// 'currency_id' => $this->currency->getId($this->config->get('config_currency')),
				// 'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
				// 'description' => $data['description']
			// ));

			// if (isset($data['paid']) && $data['paid']) {
				// $this->payment->updatePayment($payment_id, array(
					// 'date_paid' => date( 'Y-m-d H:i:s')
				// ));
			// }
			
			
			
			// if (isset($data['deduct']) && $data['deduct']) {
				// switch ($data['type']) {
					// case Payment::TYPE_SIGNUP:
						// $balance_type = Balance::BALANCE_TYPE_SIGNUP;
						// break;
						
					// case Payment::TYPE_LISTING:
						// $balance_type = Balance::BALANCE_TYPE_LISTING;
						// break;
						
					// case Payment::TYPE_PAYOUT:
					// case Payment::TYPE_PAYOUT_REQUEST:
						// $balance_type = Balance::BALANCE_TYPE_WITHDRAWAL;
						// break;
						
					// case Payment::TYPE_FAULT:
						// $balance_type = Balance::BALANCE_TYPE_FAULT;
						// break;
						
					// case Payment::TYPE_NON_COOPERATION:
						// $balance_type = Balance::BALANCE_TYPE_NON_COOPERATION;
						// break;
						
					// default: 
						// $balance_type = Balance::BALANCE_TYPE_GENERIC;
				// }
				
				// $this->balance->addBalanceEntry($data['from'] ? $data['from'] : $data['to'], array(
					// 'balance_type' => $balance_type,
					// 'amount' => -$data['amount'],
					// 'description' => $data['description']
				// ));
			// }
			
			// $this->session->data['success'] = $this->language->get('success_created');
		// } else {
		// }
	
		// $this->response->setOutput(json_encode($json));
	// }
}
?>