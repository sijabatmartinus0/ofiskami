<?php

class ControllerSaleCustomerTransaction extends Controller {
	public function getTableData() {
		$colMap = array(
			'id' => 'balance_id',
			'customer' => '`firstname`',
			'description' => 'mb.description',
			'date_created' => 'mb.date_created'
		);
		
		$sorts = array('id', 'customer', 'amount', 'description', 'date_created');
		$filters = $sorts;
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->balance->getBalanceEntries(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			$columns[] = array_merge(
				$result,
				array(
					'id' => $result['balance_id'],
					'customer' => $result['firstname'],
					'amount' => $this->currency->format($result['amount'], $this->config->get('config_currency')),
					'description' => (mb_strlen($result['mb.description']) > 80 ? mb_substr($result['mb.description'], 0, 80) . '...' : $result['mb.description']),
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['mb.date_created'])),
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}
		
	public function index() {
		$this->load->language('sale/customer_transaction');
		$this->document->addStyle('view/stylesheet/multimerch/multiseller.css');
		$this->document->addStyle('view/javascript/multimerch/datatables/css/jquery.dataTables.css');
		$this->document->addScript('view/javascript/multimerch/datatables/js/jquery.dataTables.min.js');
		$this->document->addScript('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}		

		$data['token'] = $this->session->data['token'];
		$data['text_heading'] = $this->language->get('text_heading');
		$data['label_id'] = $this->language->get('label_id');
		$data['label_customer'] = $this->language->get('label_customer');
		$data['label_net_amount'] = $this->language->get('label_net_amount');
		$data['label_date'] = $this->language->get('label_date');
		$data['label_description'] = $this->language->get('label_description');

		
		$this->document->setTitle($this->language->get('text_heading'));
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_transactions'),
			'href' => $this->url->link('sale/customer_transaction', '', 'SSL')
		);
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('sale/customer_transaction.tpl', $data));
	}
}
?>
