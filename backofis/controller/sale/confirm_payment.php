<?php
class ControllerSaleConfirmPayment extends Controller {
	public function index() {
		$this->load->language('sale/confirm_payment');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = '';
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = '';
		}

		if (isset($this->request->get['filter_invoice'])) {
			$filter_invoice = $this->request->get['filter_invoice'];
		} else {
			$filter_invoice = '';
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = '';
		}
		
		if (isset($this->request->get['filter_bank'])) {
			$filter_bank = $this->request->get['filter_bank'];
		} else {
			$filter_bank = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . $this->request->get['filter_customer'];
		}

		if (isset($this->request->get['filter_invoice'])) {
			$url .= '&filter_invoice=' . $this->request->get['filter_invoice'];
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
		
		if (isset($this->request->get['filter_bank'])) {
			$url .= '&filter_bank=' . $this->request->get['filter_bank'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/confirm_payment', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('sale/confirm_payment');

		//bank
		$data['banks'] = array();
		
		$banks = $this->model_sale_confirm_payment->getAccounts();
		
		foreach ($banks as $bank) {
			$data['banks'][] = array(
				'account_destination_id'	=> $bank['account_id'],
				'name'						=> $bank['name'],
				'account_id'				=> $bank['account_id'],
				'account_name'				=> $bank['account_name']
			);
		}
		
		$data['payments'] = array();

		$filter_data = array(
			'filter_customer'	    => $filter_customer,
			'filter_order_id'	    => $filter_order_id,
			'filter_invoice' 	 	=> $filter_invoice,
			'filter_date_added' 	=> $filter_date_added,
			'filter_bank' 			=> $filter_bank,
			'start'              	=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'              	=> $this->config->get('config_limit_admin')
		);

		$confirm_total = $this->model_sale_confirm_payment->totalConfirmPaymentOrders($filter_data);

		$payments = $this->model_sale_confirm_payment->getConfirmPaymentOrders($filter_data);

		foreach ($payments as $payment) {
			$count_merge_order_id = $this->model_sale_confirm_payment->getCountOrderIdFromDetails($payment['order_id']);
			
			$data['payments'][] = array(
				'order_id'			=> $payment['order_id'],
				'invoice' 			=> $payment['invoice'],
				'customer'   		=> $payment['customer'],
				'account_name'  	=> $payment['account_name'],
				'account_number'  	=> $payment['account_number'],
				'bank_origin'  		=> $this->model_sale_confirm_payment->getAccountDescription($payment['account_id']),
				'bank_destination'  => $this->model_sale_confirm_payment->getAccountDescription($payment['account_destination_id']),
				'total'   			=> $this->currency->format($payment['total'], $payment['currency_code'], $payment['currency_value']),
				'date_added'   		=> date($this->language->get('date_format_short'), strtotime($payment['date_added'])),
				'count_merge'		=> $count_merge_order_id
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
			
		$data['text_list'] = $this->language->get('text_list');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_detail'] = $this->language->get('text_detail');
		$data['text_bank'] = $this->language->get('text_bank');
		$data['text_bank_origin'] = $this->language->get('text_bank_origin');
		$data['text_choose'] = $this->language->get('text_choose');
		
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_invoice'] = $this->language->get('column_invoice');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_accept'] = $this->language->get('button_accept');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_search'] = $this->language->get('button_search');
		
		
		$data['token'] = $this->session->data['token'];
		$data['view_detail'] = $this->url->link('sale/confirm_payment/view_detail', '', 'SSL');
		// $data['download_report'] = $this->url->link('report/merchant/download_report', '', 'SSL');

		$pagination = new Pagination();
		$pagination->total = $confirm_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/confirm_payment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($confirm_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($confirm_total - $this->config->get('config_limit_admin'))) ? $confirm_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $confirm_total, ceil($confirm_total / $this->config->get('config_limit_admin')));

		$data['filter_order_id'] = $filter_order_id;
		$data['filter_customer'] = $filter_customer;
		$data['filter_invoice'] = $filter_invoice;
		$data['filter_date_added'] = $filter_date_added;
		$data['filter_bank'] = $filter_bank;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/confirm_payment.tpl', $data));
	}
	
	public function paymentConfirmation() {
		$this->load->language('sale/order');
		$data['text_loading'] = $this->language->get('text_loading');
		
		unset($this->session->data['cookie']);

		// Set up the API session
		if ($this->user->hasPermission('modify', 'sale/order')) {
			$this->load->model('user/api');

			$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

			if ($api_info) {
				$curl = curl_init();

				// Set SSL if required
				if (substr(HTTPS_CATALOG_LOCAL, 0, 5) == 'https') {
					curl_setopt($curl, CURLOPT_PORT, 443);
				}

				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLINFO_HEADER_OUT, true);
				curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_URL, HTTPS_CATALOG_LOCAL . 'index.php?route=api/login');
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($api_info));

				$json = curl_exec($curl);

				if (!$json) {
					$data['error_warning'] = sprintf($this->language->get('error_curl'), curl_error($curl), curl_errno($curl));
				} else {
					$response = json_decode($json, true);
				}

				if (isset($response['cookie'])) {
					$this->session->data['cookie'] = $response['cookie'];
				}
			}
		}

		if (isset($response['cookie'])) {
			$this->session->data['cookie'] = $response['cookie'];
		} else {
			$data['error_warning'] = $this->language->get('error_permission');
		}
		
		$data['text_pc_payment_method']                  = $this->language->get('text_pc_payment_method');
		$data['text_pc_date_payment']                    = $this->language->get('text_pc_date_payment');
		$data['text_pc_account']                         = $this->language->get('text_pc_account');
		$data['text_pc_account_destination']             = $this->language->get('text_pc_account_destination');
		$data['text_pc_account_name']                    = $this->language->get('text_pc_account_name');
		$data['text_pc_account_destination_name']        = $this->language->get('text_pc_account_destination_name');
		$data['text_pc_account_number']                  = $this->language->get('text_pc_account_number');
		$data['text_pc_account_destination_number']      = $this->language->get('text_pc_account_destination_number');
		$data['text_pc_total']                           = $this->language->get('text_pc_total');
		$data['text_pc_total_order']                           = $this->language->get('text_pc_total_order');
		$data['text_pc_attachment']                      = $this->language->get('text_pc_attachment');
		$data['text_pc_information']                     = $this->language->get('text_pc_information');
		$data['text_pc_status']                    		 = $this->language->get('text_pc_status');
		$data['text_pc_no_confirmation']                 = $this->language->get('text_pc_no_confirmation');
		$data['text_pc_download']                 		 = $this->language->get('text_pc_download');
			
		$data['button_payment_confirmation_reject'] 	 = $this->language->get('button_payment_confirmation_reject');
		$data['button_payment_confirmation_accept']		 = $this->language->get('button_payment_confirmation_accept');
			
		$data['text_pc_attachment']                		 = $this->language->get('text_pc_attachment');
		$data['text_pc_no_attachment']                   = $this->language->get('text_pc_no_attachment');
		$data['column_order_id']              			 = $this->language->get('column_order_id');
				
		$this->load->model('sale/order');
		
		// Payment Confirmation
		$order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
		if($order_info){
			if($order_info['payment_code']=="employee_program"){
				$data['payment_confirmation']=array(
					'date_payment'=>'-',
					'account'=>'-',
					'account_name'=>'-',
					'account_number'=>'-',
					'total'=>$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']),
					'total_amount'=>$order_info['total']*1,
					'total_order'=>$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']),
					'total_order_amount'=>$order_info['total']*1,
					'image'=>'',
					'account_branch'=>'-',
					'information'=>'-',
					'account_destination'=>'-',
					'account_destination_name'=>'-',
					'account_destination_number'=>'-'
				);
			}
			else if($order_info['payment_code']=="free_checkout"){
				$data['payment_confirmation']=array(
					'date_payment'=>'-',
					'account'=>'-',
					'account_name'=>'-',
					'account_number'=>'-',
					'total'=>$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']),
					'total_amount'=>$order_info['total']*1,
					'total_order'=>$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']),
					'total_order_amount'=>$order_info['total']*1,
					'image'=>'',
					'account_branch'=>'-',
					'information'=>'-',
					'account_destination'=>'-',
					'account_destination_name'=>'-',
					'account_destination_number'=>'-'
				);
			}
			else{
				$data['payment_confirmation'] = $this->model_sale_order->getPaymentConfirmation($this->request->get['order_id']);
			}
			if(isset($data['payment_confirmation']) && !empty($data['payment_confirmation'])){
				$data['order_status_id'] = $order_info['order_status_id'];
				$data['order_id'] = $this->request->get['order_id'];
				$this->load->model('localisation/order_status');

				$order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);
				if ($order_status_info) {
					$data['order_status'] = $order_status_info['name'];
				} else {
					$data['order_status'] = '';
				}
				$data['payment_confirmation']['payment_method'] = $order_info['payment_method'];
				if($order_info['payment_code']!="employee_program"){
					$data['payment_confirmation']['date_payment'] = date($this->language->get('date_format_short'), strtotime($data['payment_confirmation']['date_payment']));
					$data['payment_confirmation']['total'] = $this->currency->format($data['payment_confirmation']['total'], $order_info['currency_code'], $order_info['currency_value']);
					$data['payment_confirmation']['total_amount'] = $data['payment_confirmation']['total'];
					$data['payment_confirmation']['total_order'] = $this->currency->format($data['payment_confirmation']['total_order'], $order_info['currency_code'], $order_info['currency_value']);
					$data['payment_confirmation']['total_order_amount'] = $data['payment_confirmation']['total_order'];
				}
				$this->load->model('tool/image');
				if (isset($data['payment_confirmation']['image']) && is_file(DIR_IMAGE . $data['payment_confirmation']['image'])) {
					$ext=pathinfo($data['payment_confirmation']['image'], PATHINFO_EXTENSION);
					
					if($ext=="PNG" || $ext=="png" ||$ext=="JPG" ||$ext=="jpg" ||$ext=="JPEG" ||$ext=="jpeg"){
						$data['payment_confirmation']['image'] = $this->model_tool_image->resize($data['payment_confirmation']['image'], 500, 500);
					}else{
						$data['payment_confirmation']['image'] = HTTP_CATALOG.'image/' .$data['payment_confirmation']['image'];
					}
				}else{
					$data['payment_confirmation']['image']='';
				} 
			}
		}
		
		$data['token'] = $this->session->data['token'];
		$data['order_id'] = $this->request->get['order_id'];
		
		$this->response->setOutput($this->load->view('sale/confirm_payment_detail.tpl', $data));
	}
	
}