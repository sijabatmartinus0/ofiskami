<?php
class ControllerModuleDbanner extends Controller {
	private $error = array(); 
	
	public function index() 
	{   
		$this->load->language('module/dbanner');

		$this->document->setTitle($this->language->get('heading_title_clean'));

		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('dbanner', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->document->addScript('view/javascript/gridster/jquery.gridster.min.js');
		$this->document->addStyle('view/javascript/gridster/jquery.gridster.min.css');

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_margins'] = $this->language->get('text_margins');
		$data['text_effects'] = $this->language->get('text_effects');
		$data['text_settings'] = $this->language->get('text_settings');
		$data['text_banners'] = $this->language->get('text_banners');
		$data['text_add_new_widget'] = $this->language->get('text_add_new_widget');
		$data['text_edit_widget'] = $this->language->get('text_edit_widget');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_title'] = $this->language->get('text_title');
		$data['text_link'] = $this->language->get('text_link');
		$data['text_save'] = $this->language->get('text_save');
		$data['text_cancel'] = $this->language->get('text_cancel');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_padding'] = $this->language->get('entry_padding');
		$data['entry_mobile_layout'] = $this->language->get('entry_mobile_layout');
		$data['entry_margin_left'] = $this->language->get('entry_margin_left');
		$data['entry_margin_top'] = $this->language->get('entry_margin_top');
		$data['entry_margin_right'] = $this->language->get('entry_margin_right');
		$data['entry_margin_bottom'] = $this->language->get('entry_margin_bottom');
		$data['entry_effect_type'] = $this->language->get('entry_effect_type');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_show_on_mobile'] = $this->language->get('entry_show_on_mobile');

		$data['info_mobile_layout'] = $this->language->get('info_mobile_layout');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['effects_array'] = array();

		$array = array();
		$array['name'] = "No effects";
		$array['effects'] = array("none");
		array_push($data['effects_array'], $array);

		$array = array();
		$array['name'] = "Attention Seekers";
		$array['effects'] = array("bounce", "flash", "pulse", "rubberBand", "shake", "swing", "tada", "wobble");
		array_push($data['effects_array'], $array);

		$array = array();
		$array['name'] = "Bouncing Entrances";
		$array['effects'] = array("bounceIn", "bounceInDown", "bounceInLeft", "bounceInRight", "bounceInUp");
		array_push($data['effects_array'], $array);

		$array = array();
		$array['name'] = "Fading Entrances";
		$array['effects'] = array("fadeIn", "fadeInDown", "fadeInDownBig", "fadeInLeft", "fadeInLeftBig", "fadeInRight", "fadeInRightBig", "fadeInUp", "fadeInUpBig");
		array_push($data['effects_array'], $array);

        $array = array();
		$array['name'] = "Flippers";
		$array['effects'] = array("flip", "flipInX", "flipInY");
		array_push($data['effects_array'], $array);

		$array = array();
		$array['name'] = "Lightspeed";
		$array['effects'] = array("lightSpeedIn");
		array_push($data['effects_array'], $array);

        $array = array();
		$array['name'] = "Rotating Entrances";
		$array['effects'] = array("rotateIn", "rotateInDownLeft", "rotateInDownRight", "rotateInUpLeft", "rotateInUpRight");
		array_push($data['effects_array'], $array);

        $array = array();
		$array['name'] = "Sliding Entrances";
		$array['effects'] = array("slideInUp", "slideInDown", "slideInLeft", "slideInRight");
		array_push($data['effects_array'], $array);

        $array = array();
		$array['name'] = "Specials";
		$array['effects'] = array("rollIn",);
		array_push($data['effects_array'], $array);

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (!empty($module_info)) {
			$data['title'] = $module_info['title'];
		} else {
			$data['title'] = '';
		}

		if (isset($this->request->post['padding'])) {
			$data['padding'] = $this->request->post['padding'];
		} elseif (!empty($module_info)) {
			$data['padding'] = $module_info['padding'];
		} else {
			$data['padding'] = 0;
		}

		if (isset($this->request->post['mobile-layout'])) {
			$data['mobile_layout'] = $this->request->post['mobile-layout'];
		} elseif (!empty($module_info)) {
			$data['mobile_layout'] = $module_info['mobile-layout'];
		} else {
			$data['mobile_layout'] = 0;
		}

		if (isset($this->request->post['margin_top'])) {
			$data['margin_top'] = $this->request->post['margin_top'];
		} elseif (!empty($module_info)) {
			$data['margin_top'] = $module_info['margin-top'];
		} else {
			$data['margin_top'] = 0;
		}

		if (isset($this->request->post['margin_left'])) {
			$data['margin_left'] = $this->request->post['margin_left'];
		} elseif (!empty($module_info)) {
			$data['margin_left'] = $module_info['margin-left'];
		} else {
			$data['margin_left'] = 0;
		}

		if (isset($this->request->post['margin_right'])) {
			$data['margin_right'] = $this->request->post['margin_right'];
		} elseif (!empty($module_info)) {
			$data['margin_right'] = $module_info['margin-right'];
		} else {
			$data['margin_right'] = 0;
		}

		if (isset($this->request->post['margin_bottom'])) {
			$data['margin_bottom'] = $this->request->post['margin_bottom'];
		} elseif (!empty($module_info)) {
			$data['margin_bottom'] = $module_info['margin-bottom'];
		} else {
			$data['margin_bottom'] = 0;
		}

		if (isset($this->request->post['gridster'])) {
			$gridster = json_decode(stripslashes(html_entity_decode($this->request->post['gridster'])));
		} elseif (!empty($module_info)) {
			$gridster = json_decode(stripslashes(html_entity_decode($module_info['gridster'])));
		} else {
			$gridster = '';
		}

		if (isset($this->request->post['effect_type'])) {
			$data['effect_type'] = $this->request->post['effect_type'];
		} elseif (!empty($module_info)) {
			$data['effect_type'] = $module_info['effect_type'];
		} else {
			$data['effect_type'] = '';
		}

		$this->load->model('tool/image');

		$data['image_thumb_default'] = $this->model_tool_image->resize('no_image.png', 100, 100); 

		$data['gridster'] = array();
		if($gridster != '') {
			foreach ($gridster as $grid) {
				if (is_file(DIR_IMAGE . $grid->img)) {
					$grid->img_thumb = $this->model_tool_image->resize($grid->img, 100, 100);
				} else {
					$grid->img_thumb = $data['image_thumb_default'];
				}
				array_push($data['gridster'], get_object_vars($grid));
			}
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/dbanner', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/dbanner', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/dbanner', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/dbanner', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/dbanner.tpl', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/dbanner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>
