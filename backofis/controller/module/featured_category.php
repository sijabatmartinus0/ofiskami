<?php

class ControllerModuleFeaturedCategory extends Controller {
	public $name = 'featured_category';
	public  $data = array();
	public $settings = array(
		"featured_category_banner_image_width" => 300,
		"featured_category_banner_image_height" => 150,
		"featured_category_product_image_width" => 50,
		"featured_category_product_image_height" => 50,
		"featured_category_status" => 0,
		"featured_category_data" => array()
	);
	
	private $error = array(); 
	
	public function __construct($registry) {
		parent::__construct($registry);
		$this->registry = $registry;
	}	
	
	public function install() {
		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('featured_category', $this->settings);
	}

	public function uninstall() {
		
	}
	
	public function index() {
		$this->language->load('module/featured_category');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('module/featured_category');
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		
		$data['token'] = $this->session->data['token'];
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('featured_category', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		//Getter Setter Data
		if (isset($this->request->post['featured_category_banner_image_width'])) {
			$data['featured_category_banner_image_width'] = $this->request->post['featured_category_banner_image_width'];
		} else {
			$data['featured_category_banner_image_width'] = $this->config->get('featured_category_banner_image_width'); 
		} 
		if (isset($this->request->post['featured_category_banner_image_height'])) {
			$data['featured_category_banner_image_height'] = $this->request->post['featured_category_banner_image_height'];
		} else {
			$data['featured_category_banner_image_height'] = $this->config->get('featured_category_banner_image_height'); 
		} 
		if (isset($this->request->post['featured_category_product_image_width'])) {
			$data['featured_category_product_image_width'] = $this->request->post['featured_category_product_image_width'];
		} else {
			$data['featured_category_product_image_width'] = $this->config->get('featured_category_product_image_width'); 
		} 
		if (isset($this->request->post['featured_category_product_image_height'])) {
			$data['featured_category_product_image_height'] = $this->request->post['featured_category_product_image_height'];
		} else {
			$data['featured_category_product_image_height'] = $this->config->get('featured_category_product_image_height'); 
		}
		if (isset($this->request->post['featured_category_status'])) {
			$data['featured_category_status'] = $this->request->post['featured_category_status'];
		} else {
			$data['featured_category_status'] = $this->config->get('featured_category_status'); 
		}

		$data['action'] = $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'], 'SSL');	
		
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		//Text
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_module'] = $this->language->get('text_module');
		$data['text_form'] = $this->language->get('text_form');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_enabled'] = $this->language->get('text_enabled');
		
		$data['error_permission'] = $this->language->get('error_permission');
		
		$data['banner_image'] = $this->language->get('banner_image');
		$data['product_image'] = $this->language->get('product_image');
		
		$data['text_width'] = $this->language->get('text_width');
		$data['text_height'] = $this->language->get('text_height');
		
		$data['tab_category'] = $this->language->get('tab_category');
		$data['tab_setting'] = $this->language->get('tab_setting');
		
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_mobile'] = $this->language->get('entry_mobile');
		
		
		$data['button_category_add'] = $this->language->get('button_category_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		//breadcrumbs
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'], 'SSL')
   		);
		//Error
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		// Images
		if (isset($this->request->post['featured_category_data'])) {
			$featured_categories = $this->request->post['featured_category_data'];
		} else {
			$featured_categories = $this->config->get('featured_category_data');
		} 
		$data['featured_categories'] = array();

		if(!empty($featured_categories)){
			foreach ($featured_categories as $featured_category) {
				if (is_file(DIR_IMAGE . $featured_category['image'])) {
					$image = $featured_category['image'];
					$thumb = $featured_category['image'];
				} else {
					$image = '';
					$thumb = 'no_image.png';
				}
				
				if (is_file(DIR_IMAGE . $featured_category['image_mobile'])) {
					$image_mobile = $featured_category['image_mobile'];
					$thumb_mobile = $featured_category['image_mobile'];
				} else {
					$image_mobile = '';
					$thumb_mobile = 'no_image.png';
				}
				
				
				$category_id = $featured_category['category_id'];
				$category = $this->model_catalog_category->getCategory($category_id);
				if(empty($category)){
					$category_name = "";
				}else{
					$category_name = $category['name'];
				}
				
				$data['featured_categories'][] = array(
					'image'      	=> $image,
					'thumb'      	=> $this->model_tool_image->resize($thumb, 100, 100),
					'image_mobile'  => $image_mobile,
					'thumb_mobile'  => $this->model_tool_image->resize($thumb_mobile, 100, 100),
					'category_id' 	=> $category_id,
					'category' 		=> $category_name,
					'sort_order' 	=> $featured_category['sort_order']
				);
			}
		}
		
        if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                $this->session->data['success'] = '';
        } else {
                $data['success'] = '';
        }		
		
		
		$this->load->model('design/layout');
		$data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->template = "module/{$this->name}.tpl";
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view($this->template, $data));
	}		
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/featured_category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$featured_categories = $this->request->post['featured_category_data'];
		
		foreach ($featured_categories as $featured_category) {
			if (empty($featured_category['category_id']) && (int)$featured_category['category_id']==0) {
				$this->error['warning'] = $this->language->get('error_category');
				break;
			}
		}
		
		return !$this->error;
	}	
}
?>
