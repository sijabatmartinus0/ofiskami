<?php

class ControllerModuleProductComments extends Controller {
	public $name = 'productcomments';
	public  $data = array();
	public $settings = array(
		"productcomments_pcconf_perpage" => '',
		"productcomments_pcconf_email" => '',
		"productcomments_pcconf_maxlen" => 500,
		"productcomments_pcconf_enforce_customer_data" => 0,
		"productcomments_pcconf_allow_guests" => 0,
		"productcomments_pcconf_enable_customer_captcha" => 0
	);
	
	private $error = array(); 
	
	public function __construct($registry) {
		parent::__construct($registry);
		$this->registry = $registry;
	}	
	
	/*private function editSettings() {
		$this->load->model("setting/setting");
		
		$set = $this->model_setting_setting->getSetting($this->name);
		
		foreach ($this->settings as $name=>$value) {
			if (!array_key_exists($name,$set)) {
				$set[$name] = $value;
			}
		}
		
		foreach($set as $s=>$v) {
			if (isset($this->request->post[$s])) {
				$set[$s] = $this->request->post[$s];
				$data[$s] = $this->request->post[$s];
			} elseif ($this->config->get($s)) {
				$data[$s] = $this->config->get($s);
			}
		}
		
		$this->model_setting_setting->editSetting($this->name, $set);
	}*/
	
	public function install() {
		$this->load->model('setting/setting');
		$this->load->model("module/productcomments");
		$this->model_module_productcomments->createTable();
		$this->model_setting_setting->editSetting('productcomments', $this->settings);
	}

	public function uninstall() {
		$this->model_module_productcomments->dropTable();
	}
	
	/*public function saveSettings() {
		$json = array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
	        $this->editSettings();
	        $this->session->data['success'] = $this->language->get('pc_success');
		} else {
			$json['errors'][] = $this->language->get('error_permission');
		}
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));			
		}			
	}*/
	
	public function index() {
		$this->language->load('module/productcomments');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('module/productcomments');
		
		$data['token'] = $this->session->data['token'];
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('productcomments', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		/*foreach($this->settings as $s=>$v) {
			$data[$s] = $this->config->get($s);
		}*/
		
		//Getter Setter Data
		if (isset($this->request->post['productcomments_pcconf_email'])) {
			$data['productcomments_pcconf_email'] = $this->request->post['productcomments_pcconf_email'];
		} else {
			$data['productcomments_pcconf_email'] = $this->config->get('productcomments_pcconf_email'); 
		} 
		if (isset($this->request->post['productcomments_pcconf_perpage'])) {
			$data['productcomments_pcconf_perpage'] = $this->request->post['productcomments_pcconf_perpage'];
		} else {
			$data['productcomments_pcconf_perpage'] = $this->config->get('productcomments_pcconf_perpage'); 
		} 
		if (isset($this->request->post['productcomments_pcconf_maxlen'])) {
			$data['productcomments_pcconf_maxlen'] = $this->request->post['productcomments_pcconf_maxlen'];
		} else {
			$data['productcomments_pcconf_maxlen'] = $this->config->get('productcomments_pcconf_maxlen'); 
		} 
		if (isset($this->request->post['productcomments_pcconf_enforce_customer_data'])) {
			$data['productcomments_pcconf_enforce_customer_data'] = $this->request->post['productcomments_pcconf_enforce_customer_data'];
		} else {
			$data['productcomments_pcconf_enforce_customer_data'] = $this->config->get('productcomments_pcconf_enforce_customer_data'); 
		} 
		if (isset($this->request->post['productcomments_pcconf_allow_guests'])) {
			$data['productcomments_pcconf_allow_guests'] = $this->request->post['productcomments_pcconf_allow_guests'];
		} else {
			$data['productcomments_pcconf_allow_guests'] = $this->config->get('productcomments_pcconf_allow_guests'); 
		} 
		if (isset($this->request->post['productcomments_pcconf_enable_customer_captcha'])) {
			$data['productcomments_pcconf_enable_customer_captcha'] = $this->request->post['productcomments_pcconf_enable_customer_captcha'];
		} else {
			$data['productcomments_pcconf_enable_customer_captcha'] = $this->config->get('productcomments_pcconf_enable_customer_captcha'); 
		} 

		//$this->setBreadcrumbs();
		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}		
        //$data['action'] = $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		//Text
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_module'] = $this->language->get('text_module');
		
		$data['error_permission'] = $this->language->get('error_permission');
		$data['pc_success'] = $this->language->get('pc_success');
		
		$data['pc_settings'] = $this->language->get('pc_settings');
		$data['pc_comments'] = $this->language->get('pc_comments');
		
		$data['pc_config_email'] = $this->language->get('pc_config_email');
		$data['pc_config_email_note'] = $this->language->get('pc_config_email_note');
		$data['pc_config_perpage'] = $this->language->get('pc_config_perpage');
		$data['pc_config_perpage_note'] = $this->language->get('pc_config_perpage_note');
		$data['pc_config_allow_guests'] = $this->language->get('pc_config_allow_guests');
		$data['pc_config_allow_guests_note'] = $this->language->get('pc_config_allow_guests_note');
		$data['pc_config_enforce_customer_data'] = $this->language->get('pc_config_enforce_customer_data');
		$data['pc_config_enforce_customer_data_note'] = $this->language->get('pc_config_enforce_customer_data_note');
		$data['pc_config_enable_customer_captcha'] = $this->language->get('pc_config_enable_customer_captcha');
		$data['pc_config_enable_customer_captcha_note'] = $this->language->get('pc_config_enable_customer_captcha_note');
		
		$data['pc_config_maxlen'] = $this->language->get('pc_config_maxlen');
		$data['pc_config_maxlen_note'] = $this->language->get('pc_config_maxlen_note');
		
		$data['text_id'] = $this->language->get('text_id');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_save'] = $this->language->get('text_save');
		$data['text_delete'] = $this->language->get('text_delete');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_display'] = $this->language->get('text_display');
		
		$data['text_form'] = $this->language->get('text_form');
		
		$data['text_no_comments_added'] = $this->language->get('text_no_comments_added');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		//breadcrumbs
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link("module/{$this->name}", 'token=' . $this->session->data['token'], 'SSL')
   		);
		//Error
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['pcconf_perpage'])) {
			$data['error_pcconf_perpage'] = $this->error['pcconf_perpage'];
		} else {
			$data['error_pcconf_perpage'] = '';
		}
		
		if (isset($this->error['pcconf_email'])) {
			$data['error_pcconf_email'] = $this->error['pcconf_email'];
		} else {
			$data['error_pcconf_email'] = '';
		}
		
		if (isset($this->error['pcconf_maxlen'])) {
			$data['error_pcconf_maxlen'] = $this->error['pcconf_maxlen'];
		} else {
			$data['error_pcconf_maxlen'] = '';
		}
		
        if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                $this->session->data['success'] = '';
        } else {
                $data['success'] = '';
        }		
		
		$page = (isset($this->request->get['page'])) ? $this->request->get['page'] : 1;
		$total_comments = $this->model_module_productcomments->getTotalComments();
		
		$sort = array(
			'order_by' => 'pc.create_time',
			'order_way' => 'DESC',
			'offset' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')			
		);
		
		$data['pcComments'] = $this->model_module_productcomments->getComments(array(), $sort);
		
		$pagination = new Pagination();
		$pagination->total = $total_comments;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/productcomments', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();	
		
		$this->load->model('design/layout');
		$data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->template = "module/{$this->name}.tpl";
		/*$this->children = array(
			'common/header',	
			'common/footer'	
		);*/
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view($this->template, $data));
	}		

	public function editComment() {
		$this->model_module_productcomments->editComment($this->request->post);
	}
	 
	public function deleteComment() {
		$this->model_module_productcomments->deleteComment($this->request->get['comment_id']);
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/productcomments')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['productcomments_pcconf_perpage'])) {
			$this->error['pcconf_perpage'] = $this->language->get('error_pcconf_perpage');
		}
		
		if (empty($this->request->post['productcomments_pcconf_email'])) {
			$this->error['pcconf_email'] = $this->language->get('error_pcconf_email');
		}

		if (empty($this->request->post['productcomments_pcconf_maxlen'])) {
			$this->error['pcconf_maxlen'] = $this->language->get('error_pcconf_maxlen');
		}
		
		return !$this->error;
	}	
}
?>
