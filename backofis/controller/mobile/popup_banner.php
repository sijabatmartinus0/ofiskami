<?php

class ControllerMobilePopUpBanner extends Controller {
	public  $data = array();
	public $settings = array(
		"mobile_popup_banner_image_width" => 300,
		"mobile_popup_banner_image_height" => 150
	);
	
	private $error = array(); 	
	
	public function index() {
		$this->language->load('mobile/popup_banner');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('mobile/popup_banner');
		$this->load->model('tool/image');
		
		$data['token'] = $this->session->data['token'];
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['success'] = $this->language->get('text_success');

			$this->model_setting_setting->editSetting('mobile_popup_banner', $this->settings);
			
			$this->model_mobile_popup_banner->addPopUpBanner($this->request->post);
		
		}
		
		//Getter Setter Data
		if (isset($this->request->post['mobile_popup_banner_image_width'])) {
			$data['mobile_popup_banner_image_width'] = $this->request->post['mobile_popup_banner_image_width'];
		} else {
			$data['mobile_popup_banner_image_width'] = $this->config->get('mobile_popup_banner_image_width'); 
		} 
		if (isset($this->request->post['mobile_popup_banner_image_height'])) {
			$data['mobile_popup_banner_image_height'] = $this->request->post['mobile_popup_banner_image_height'];
		} else {
			$data['mobile_popup_banner_image_height'] = $this->config->get('mobile_popup_banner_image_height'); 
		} 
		
		$popup_banner_info = $this->model_mobile_popup_banner->getPopUpBanner();
		
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($popup_banner_info)) {
			$data['image'] = $popup_banner_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($popup_banner_info) && is_file(DIR_IMAGE . $popup_banner_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($popup_banner_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);	
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($popup_banner_info)) {
			$data['status'] = $popup_banner_info['status'];
		} else {
			$data['status'] = true;
		}

		$data['action'] = $this->url->link("mobile/popup_banner", 'token=' . $this->session->data['token'], 'SSL');	
		
        //Text
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_module'] = $this->language->get('text_module');
		$data['text_form'] = $this->language->get('text_form');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_enabled'] = $this->language->get('text_enabled');
		
		$data['error_permission'] = $this->language->get('error_permission');
		
		$data['popup_image'] = $this->language->get('popup_image');
		
		$data['text_width'] = $this->language->get('text_width');
		$data['text_height'] = $this->language->get('text_height');
		
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_status'] = $this->language->get('entry_status');
		
		$data['button_save'] = $this->language->get('button_save');
		
		//breadcrumbs
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link("mobile/popup_banner", 'token=' . $this->session->data['token'], 'SSL')
   		);
		//Error
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		
        if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                $this->session->data['success'] = '';
        } else {
                $data['success'] = '';
        }		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view("mobile/popup_banner.tpl", $data));
	}		
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'mobile/popup_banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}	
}
?>
