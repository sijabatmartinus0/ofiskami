<?php

class ControllerMobileSlideBanner extends Controller {
	public  $data = array();
	public $settings = array(
		"mobile_slide_banner_image_width" => 300,
		"mobile_slide_banner_image_height" => 150
	);
	public $slide_types = array(
		array(
			"id"=>"product",
			"name"=>"Product"
		),
		array(
			"id"=>"category",
			"name"=>"Category"
		),
		array(
			"id"=>"url",
			"name"=>"URL"
		)
	);
	
	private $error = array(); 
	
	public function index() {
		$this->language->load('mobile/slide_banner');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('mobile/slide_banner');
		$this->load->model('tool/image');
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		
		$data['token'] = $this->session->data['token'];
		
		if (!isset($this->request->post['slide_banner_data'])){
			$this->request->post['slide_banner_data']=array();
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['success'] = $this->language->get('text_success');

			$this->model_setting_setting->editSetting('mobile_slide_banner', $this->settings);
			
			$this->model_mobile_slide_banner->addSlideBanner($this->request->post['slide_banner_data']);
		}
		
		//Getter Setter Data
		if (isset($this->request->post['mobile_slide_banner_image_width'])) {
			$data['mobile_slide_banner_image_width'] = $this->request->post['mobile_slide_banner_image_width'];
		} else {
			$data['mobile_slide_banner_image_width'] = $this->config->get('mobile_slide_banner_image_width'); 
		} 
		if (isset($this->request->post['mobile_slide_banner_image_height'])) {
			$data['mobile_slide_banner_image_height'] = $this->request->post['mobile_slide_banner_image_height'];
		} else {
			$data['mobile_slide_banner_image_height'] = $this->config->get('mobile_slide_banner_image_height'); 
		}

		$data['action'] = $this->url->link("mobile/slide_banner", 'token=' . $this->session->data['token'], 'SSL');	
		
        //Text
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_module'] = $this->language->get('text_module');
		$data['text_form'] = $this->language->get('text_form');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_enabled'] = $this->language->get('text_enabled');
		
		$data['error_permission'] = $this->language->get('error_permission');
		
		$data['slide_image'] = $this->language->get('slide_image');
		
		$data['text_width'] = $this->language->get('text_width');
		$data['text_height'] = $this->language->get('text_height');
		
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_setting'] = $this->language->get('tab_setting');
		
		$data['entry_type'] = $this->language->get('entry_type');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_value'] = $this->language->get('entry_value');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_status'] = $this->language->get('entry_status');
		
		
		$data['button_slide_add'] = $this->language->get('button_slide_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		//breadcrumbs
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link("mobile/slide_banner", 'token=' . $this->session->data['token'], 'SSL')
   		);
		//Error
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		// Images
		if (!empty($this->request->post['slide_banner_data'])) {
			$slide_banner_infos = $this->request->post['slide_banner_data'];
		} else {
			$slide_banner_infos = $this->model_mobile_slide_banner->getSlideBanner();
		} 
		
		$data['slide_banners'] = array();

		foreach ($slide_banner_infos as $slide_banner_info) {
			if (is_file(DIR_IMAGE . $slide_banner_info['image'])) {
				$image = $slide_banner_info['image'];
				$thumb = $slide_banner_info['image'];
			} else {
				$image = '';
				$thumb = 'no_image.png';
			}
			
			if($slide_banner_info['type']=="category"){
				$value_text = $this->model_catalog_category->getCategory((int)$slide_banner_info['value'])['name'];
			}else if($slide_banner_info['type']=="product"){
				$value_text = $this->model_catalog_product->getProduct((int)$slide_banner_info['value'])['name'];
			}else if($slide_banner_info['type']=="url"){
				$value_text = $slide_banner_info['value'];
			}else{
				$value_text = '';
			}
			
			$data['slide_banners'][] = array(
				'image'      	=> $image,
				'thumb'      	=> $this->model_tool_image->resize($thumb, 100, 100),
				'type' 			=> $slide_banner_info['type'],
				'value' 		=> $slide_banner_info['value'],
				'value_text' 	=> $value_text,
				'sort_order' 	=> $slide_banner_info['sort_order'],
				'status' 	=> $slide_banner_info['status']
			);
		}
		
        if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                $this->session->data['success'] = '';
        } else {
                $data['success'] = '';
        }

		$data['slide_types'] 	= $this->slide_types;		
		
		$data['header'] 		= $this->load->controller('common/header');
		$data['column_left'] 	= $this->load->controller('common/column_left');
		$data['footer'] 		= $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('mobile/slide_banner.tpl', $data));
	}		
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'mobile/slide_banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$slide_banners = $this->request->post['slide_banner_data'];
		
		foreach ($slide_banners as $slide_banner) {
			if (empty($slide_banner['value']) && (int)$slide_banner['value']==0) {
				$this->error['warning'] = $this->language->get('error_value');
				break;
			}
		}
		
		return !$this->error;
	}	
}
?>
