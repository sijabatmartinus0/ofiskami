<?php

class ControllerMultisellerSeller extends ControllerMultisellerBase {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->document->addScript('backofis/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('backofis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('backofis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
	}
		
	public function getTableData() {
		$colMap = array(
			'seller' => '`c.name`',
			'email' => 'c.email',
			'balance' => '`current_balance`',
			'date_created' => '`ms.date_created`',
			'status' => '`ms.seller_status`'
		);

		$sorts = array('seller', 'email', 'total_sales', 'total_products', 'total_earnings', 'date_created', 'balance', 'status', 'date_created');
		$filters = array_diff($sorts, array('status'));
		
		//var_dump($this->request->get);
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsSeller->getSellers(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'total_products' => 1,
				'total_earnings' => 1,
				'current_balance' => 1
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// actions
			$actions = "";
			/*if ($this->currency->format($this->MsLoader->MsBalance->getSellerBalance($result['seller_id']) - $this->MsLoader->MsBalance->getReservedSellerFunds($result['seller_id']), $this->config->get('config_currency'), '', FALSE) > 0) {
				if (!empty($result['ms.paypal']) && filter_var($result['ms.paypal'], FILTER_VALIDATE_EMAIL)) {
					$actions .= "<a class='ms-button ms-button-paypal' title='" . $this->language->get('ms_catalog_sellers_balance_paypal') . "'></a>";
				} else {
					$actions .= "<a class='ms-button ms-button-paypal-bw' title='".$this->language->get('ms_payment_payout_paypal_invalid') . "'></a>";
				}
			}*/
			
			if((int)$result['seller_type']==0 && (int)$result['premium_status']==1){
				$actions .= "<a class='ms-button ms-button-premium' href='" . $this->url->link('multiseller/seller/premium', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL') . "' title='".$this->language->get('button_premium')."'></a>";
			}
			
			$actions .= "<a class='ms-button ms-button-edit' href='" . $this->url->link('multiseller/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL') . "' title='".$this->language->get('button_edit')."'></a>";
			$actions .= "<a class='ms-button ms-button-delete' href='" . $this->url->link('multiseller/seller/delete', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL') . "' title='".$this->language->get('button_delete')."'></a>";

			$available = $this->MsLoader->MsBalance->getSellerBalance($result['seller_id']) - $this->MsLoader->MsBalance->getReservedSellerFunds($result['seller_id']);
			
			// build table data
			$seller="<a style='float:left' href='".$this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['seller_id'], 'SSL')."'>{$result['c.name']}({$result['ms.nickname']})</a>";
			
			if((int)$result['seller_type']==1 && (int)$result['premium_status']==2){
				$seller.="<div class='ms-premium-user'></div>";
			}
			
			$columns[] = array_merge(
				$result,
				array(
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['seller_id']}' />",
					'seller' => $seller,
					'email' => $result['c.email'],
					'total_earnings' => $this->currency->format($this->MsLoader->MsSeller->getTotalEarnings($result['seller_id']), $this->config->get('config_currency')),
					'balance' => $this->currency->format($this->MsLoader->MsBalance->getSellerBalance($result['seller_id']), $this->config->get('config_currency')) . '/' . $this->currency->format($available > 0 ? $available : 0, $this->config->get('config_currency')),
					'status' => $this->language->get('ms_seller_status_' . $result['ms.seller_status']),
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['ms.date_created'])),
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
  			'iTotalDisplayRecords' => $total, //count($results),
			'aaData' => $columns
		)));
	}
	
	public function jxSaveSellerInfo() {
		$this->validate(__FUNCTION__);
		$data = $this->request->post;
		$seller = $this->MsLoader->MsSeller->getSeller($data['seller']['seller_id']);
		$json = array();
		$this->load->model('sale/customer');
		
		if (empty($data['seller']['seller_id'])) {
			// creating new seller
			if (empty($data['seller']['nickname'])) {
				$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_empty'); 
			} else if (mb_strlen($data['seller']['nickname']) < 4 || mb_strlen($data['seller']['nickname']) > 128 ) {
				$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_length');			
			} else if ($this->MsLoader->MsSeller->nicknameTaken($data['seller']['nickname'])) {
				$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_taken');
			} else {
				switch($this->config->get('msconf_nickname_rules')) {
					case 1:
						// extended latin
						if(!preg_match("/^[a-zA-Z0-9_\-\s\x{00C0}-\x{017F}]+$/u", $data['seller']['nickname'])) {
							$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_latin');
						}
						break;
						
					case 2:
						// utf8
						if(!preg_match("/((?:[\x01-\x7F]|[\xC0-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF7][\x80-\xBF]{3}){1,100})./x", $data['seller']['nickname'])) {
							$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_utf8');
						}
						break;
						
					case 0:
					default:
						// alnum
						if(!preg_match("/^[a-zA-Z0-9_\-\s]+$/", $data['seller']['nickname'])) {
							$json['errors']['seller[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_alphanumeric');
						}
						break;
				}
			}
			
			if (empty($data['customer']['customer_id'])) {
				// creating new customer
				$this->language->load('sale/customer');

				if ((mb_strlen($data['customer']['firstname']) < 1) || (mb_strlen($data['customer']['firstname']) > 32)) {
			  		$json['errors']['customer[firstname]'] = $this->language->get('error_firstname');
				}
		
				if ((mb_strlen($data['customer']['lastname']) < 1) || (mb_strlen($data['customer']['lastname']) > 32)) {
			  		$json['errors']['customer[lastname]'] = $this->language->get('error_lastname');
				}
		
				if ((mb_strlen($data['customer']['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $data['customer']['email'])) {
			  		$json['errors']['customer[email]'] = $this->language->get('error_email');
				}
				
				if (!$this->checkDate($data['customer']['dob'])) {
					$json['errors']['customer[dob]'] = $this->language->get('error_dob');
				}else if(date($data['customer']['dob']) > date("Y-m-d",strtotime('-15 years'))){
					$json['errors']['customer[dob]'] = $this->language->get('error_dob_value');
				}
		
				$customer_info = $this->model_sale_customer->getCustomerByEmail($data['customer']['email']);
				
				if (!isset($this->request->get['customer_id'])) {
					if ($customer_info) {
						$json['errors']['customer[email]'] = $this->language->get('error_exists');
					}
				} else {
					if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
						$json['errors']['customer[email]'] = $this->language->get('error_exists');
					}
				}
				
				/*if ($data['customer']['password'] || (!isset($this->request->get['customer_id']))) {
			  		if ((mb_strlen($data['customer']['password']) < 4) || (mb_strlen($data['customer']['password']) > 20)) {
						$json['errors']['customer[password]'] = $this->language->get('error_password');
			  		}
			
			  		if ($data['customer']['password'] != $data['customer']['password_confirm']) {
						$json['errors']['customer[password_confirm]'] = $this->language->get('error_confirm');
			  		}
				}*/				
			}
			
			if (!isset($data['seller']['logistic']) || empty($data['seller']['logistic'])) {
			$json['errors']['sellerinfo_logistic'] = $this->language->get('ms_error_sellerinfo_logistic_empty');
		}
		
		
		}
		
		if (!isset($data['seller']['country']) ||$data['seller']['country']=='' ||(int)$data['seller']['country']==0) {
			$json['errors']['seller[country]'] = $this->language->get('ms_error_sellerinfo_country_empty');
		}
		
		if (!isset($data['seller']['city']) ||$data['seller']['city']=='' ||(int)$data['seller']['city']==0) {
			$json['errors']['seller[city]'] = $this->language->get('ms_error_sellerinfo_city_empty');
		}
		
		if (!isset($data['seller']['zone']) ||$data['seller']['zone']=='' ||(int)$data['seller']['zone']==0) {
			$json['errors']['seller[zone]'] = $this->language->get('ms_error_sellerinfo_zone_empty');
		}
		
		if (!isset($data['seller']['district']) ||$data['seller']['district']=='' ||(int)$data['seller']['district']==0) {
			$json['errors']['seller[district]'] = $this->language->get('ms_error_sellerinfo_district_empty');
		}
		
		if (!isset($data['seller']['subdistrict']) ||$data['seller']['subdistrict']=='' ||(int)$data['seller']['subdistrict']==0) {
			$json['errors']['seller[subdistrict]'] = $this->language->get('ms_error_sellerinfo_subdistrict_empty');
		}else{
			$this->load->model('localisation/subdistrict');
			$location_data = $this->model_localisation_subdistrict->getLocation($data['seller']['subdistrict']);
			if($location_data){
				$data['seller']['country']=$location_data['country_id'];
				$data['seller']['zone']=$location_data['zone_id'];
				$data['seller']['city']=$location_data['city_id'];
				$data['seller']['district']=$location_data['district_id'];
				$data['seller']['subdistrict']=$location_data['subdistrict_id'];
				$data['seller']['postcode']=$location_data['postcode'];
			}else{
				$json['errors']['seller[subdistrict]'] = $this->language->get('ms_error_sellerinfo_subdistrict_empty');
			}
		}
		
		if (!isset($data['seller']['postcode']) ||$data['seller']['postcode']=='') {
			$json['errors']['seller[postcode]'] = $this->language->get('ms_error_sellerinfo_postcode_empty');
		}
		
		if (!isset($data['seller']['account']) ||$data['seller']['account']=='' ||(int)$data['seller']['account']==0) {
			$json['errors']['seller[account]'] = $this->language->get('ms_error_sellerinfo_account_empty');
		}
		
		if (!isset($data['seller']['account_name']) ||$data['seller']['account_name']=='') {
			$json['errors']['seller[account_name]'] = $this->language->get('ms_error_sellerinfo_account_name_empty');
		}
		
		if (!isset($data['seller']['account_number']) ||$data['seller']['account_number']=='') {
			$json['errors']['seller[account_number]'] = $this->language->get('ms_error_sellerinfo_account_number_empty');
		}
		
		if (!isset($data['seller']['npwp_number']) ||$data['seller']['npwp_number']=='') {
			$json['errors']['seller[npwp_number]'] = $this->language->get('ms_error_sellerinfo_npwp_number_empty');
		}
		
		if (!isset($data['seller']['npwp_address']) ||$data['seller']['npwp_address']=='') {
			$json['errors']['seller[npwp_address]'] = $this->language->get('ms_error_sellerinfo_npwp_address_empty');
		}
		
		if (mb_strlen($data['seller']['address']) < 20 ) {
			$json['errors']['seller[address]'] = $this->language->get('ms_error_sellerinfo_address_length');
		}
		
		if (mb_strlen($data['seller']['telephone']) < 7 ) {
			$json['errors']['seller[telephone]'] = $this->language->get('ms_error_sellerinfo_telephone_length');
		}
		
		if (strlen($data['seller']['company']) > 50 ) {
			$json['errors']['seller[company]'] = 'Company name cannot be longer than 50 characters';
		}
		if (empty($json['errors'])) {
			$mails = array();
			if (empty($data['seller']['seller_id'])) {
				// creating new seller
				if (empty($data['customer']['customer_id'])) {
					// creating new customer
					$data['customer']['hash'] = md5( rand(0,1000).time()); 
					$data['customer']['code'] = $this->generate_random_password(5);
					$data['customer']['change_password'] =  md5( rand(0,1000).time().$this->generate_random_password(5)); 
					$this->model_sale_customer->addCustomer(
						array_merge(
							$data['customer'],
							array(
								'password' => $this->generate_random_password(10),
								'telephone' => $data['seller']['telephone'],
								'fax' => '',
								'account_id' => '',
								'account_name' => '',
								'account_number' => '',
								'customer_group_id' => $this->config->get('config_customer_group_id'),
								'newsletter' => 1,
								'status' => 1,
								'approved' => 1,
								'safe' => 1,
							)
						)
					);
					
					$customer_info = $this->model_sale_customer->getCustomerByEmail($data['customer']['email']);
					$this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_info['customer_id'] . "'");
					
					$data['seller']['seller_id'] = $customer_info['customer_id'];
				} else {
					$data['seller']['seller_id'] = $data['customer']['customer_id'];
				}
				
				$this->MsLoader->MsSeller->createSeller(
					array_merge(
						$data['seller'],
						array(
							'approved' => 1,
							'status' => 2,
							'dob' => $data['customer']['dob']
						)
					)
				);
				
				$seller = $this->MsLoader->MsSeller->getSeller($data['seller']['seller_id']);
				
				$mails[] = array(
					'type' => MsMail::SMT_SELLER_ACCOUNT_CREATED_BY_ADMIN,
					'data' => array(
						'recipients' => $seller['c.email'],
						'addressee' => $seller['ms.nickname'],
						'message' => (isset($data['seller']['message']) ? $data['seller']['message'] : ''),
						'seller_id' => $seller['seller_id'],
						'link' => HTTPS_CATALOG . 'index.php?route=seller/password&code=' .$seller['c.change_password']
					)
				);
				
				$this->MsLoader->MsMail->sendMails($mails);
				
			} else {
				// edit seller
				$mails[] = array(
					'type' => MsMail::SMT_SELLER_ACCOUNT_MODIFIED,
					'data' => array(
						'recipients' => $seller['c.email'],
						'addressee' => $seller['ms.nickname'],
						'message' => (isset($data['seller']['message']) ? $data['seller']['message'] : ''),
						'seller_id' => $seller['seller_id']
					)
				);
	// echo '<pre>'; print_r($seller); echo '</pre>'; die();
				switch ($data['seller']['status']) {
					case MsSeller::STATUS_INACTIVE:
					case MsSeller::STATUS_DISABLED:
					case MsSeller::STATUS_DELETED:
					case MsSeller::STATUS_INCOMPLETE:
						$products = $this->MsLoader->MsProduct->getProducts(array(
							'seller_id' => $seller['seller_id']
						));
						
						foreach ($products as $p) {
							$this->MsLoader->MsProduct->changeStatus($p['product_id'], $data['seller']['status']);
						}
						
						$data['seller']['approved'] = 0;
						break;
					case MsSeller::STATUS_ACTIVE:
						if ($seller['ms.seller_status'] == MsSeller::STATUS_INACTIVE && $this->config->get('msconf_allow_inactive_seller_products')) {
							$products = $this->MsLoader->MsProduct->getProducts(array(
								'seller_id' => $seller['seller_id']
							));
							
							foreach ($products as $p) {
								$this->MsLoader->MsProduct->changeStatus($p['product_id'], $data['seller']['status']);
								if ($this->config->get('msconf_product_validation') == MsProduct::MS_PRODUCT_VALIDATION_NONE) {
									$this->MsLoader->MsProduct->approve($p['product_id']);
								}
							}
						}
						
						$data['seller']['approved'] = 1;
						break;
				}
							
				$this->MsLoader->MsSeller->adminEditSeller(
					array_merge(
						$data['seller'],
						array(
							'approved' => 1,
						)
					)				
				);
				
				$this->load->model('multiseller/seller');
				$this->model_multiseller_seller->deleteCategories($data['seller_id']);
				$commission_categories = $this->model_multiseller_seller->getCategories();
				
				foreach($commission_categories as $commission_category){
					
					if (!empty($data['category'][$commission_category['category_id']]['percent'])) {
						if ((int)$data['category'][$commission_category['category_id']]['percent'] > 100) {
							$json['errors']['category['.$commission_category['category_id'].'][percent]'] = $this->language->get('ms_error_percent');
						}else if(!is_numeric($data['category'][$commission_category['category_id']]['percent'])){
							$json['errors']['category['.$commission_category['category_id'].'][percent]'] = $this->language->get('ms_error_number_only');
						}
					}
					
					if (!empty($data['category'][$commission_category['category_id']]['flat'])) {
						if(!is_numeric($data['category'][$commission_category['category_id']]['flat'])){
							$json['errors']['category['.$commission_category['category_id'].'][flat]'] = $this->language->get('ms_error_number_only');
						}
					}
					
					if (empty($json['errors'])) {
						if(!empty($data['category'][$commission_category['category_id']]['percent']) || !empty($data['category'][$commission_category['category_id']]['flat'])){
							$this->model_multiseller_seller->addCategories(
								array(
									'seller_id' 	=> $data['seller_id'],
									'category_id' 	=> $commission_category['category_id'],
									'flat' 			=> $data['category'][$commission_category['category_id']]['flat'],
									'percent' 		=> $data['category'][$commission_category['category_id']]['percent']						
								)
							);
						}
					}
				}
			}
			
			if ($data['seller']['notify']) {
				$this->MsLoader->MsMail->sendMails($mails);
			}
			
			$this->session->data['success'] = 'Seller account data saved.';
		}
		
		$this->response->setOutput(json_encode($json));
	}	
	
	
	// simple paypal balance payout
	public function jxPayBalance() {
		$json = array();
		$seller_id = isset($this->request->get['seller_id']) ? $this->request->get['seller_id'] : 0;
		$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		if (!$seller) return;
		
		$amount = $this->MsLoader->MsBalance->getSellerBalance($seller_id) - $this->MsLoader->MsBalance->getReservedSellerFunds($seller_id);
		
		if (!$amount) return;

		//create payment
		$payment_id = $this->MsLoader->MsPayment->createPayment(array(
			'seller_id' => $seller_id,
			'payment_type' => MsPayment::TYPE_PAYOUT,
			'payment_status' => MsPayment::STATUS_UNPAID,
			'payment_data' => $seller['ms.paypal'],
			'payment_method' => MsPayment::METHOD_PAYPAL,
			'amount' => $this->currency->format($amount, $this->config->get('config_currency'), '', FALSE),
			'currency_id' => $this->currency->getId($this->config->get('config_currency')),
			'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
			'description' => sprintf($this->language->get('ms_payment_royalty_payout'), $seller['name'], $this->config->get('config_name'))
		));
		
		// render paypal form
		$this->data['payment_data'] = array(
			'sandbox' => $this->config->get('msconf_paypal_sandbox'),
			'action' => $this->config->get('msconf_paypal_sandbox') ? "https://www.sandbox.paypal.com/cgi-bin/webscr" : "https://www.paypal.com/cgi-bin/webscr",
			'business' => $seller['ms.paypal'],
			'item_name' => sprintf($this->language->get('ms_payment_royalty_payout'), $seller['name'], $this->config->get('config_name')),
			'amount' => $this->currency->format($amount, $this->config->get('config_currency'), '', FALSE),
			'currency_code' => $this->config->get('config_currency'),
			'return' => $this->url->link('multiseller/seller', 'token=' . $this->session->data['token']),
			'cancel_return' => $this->url->link('multiseller/seller', 'token=' . $this->session->data['token']),
			'notify_url' => HTTP_CATALOG . 'index.php?route=payment/multimerch-paypal/payoutIPN',
			'custom' => $payment_id
		);
		
		list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('payment/multimerch-paypal');
		$json['form'] = $this->load->view($template, $this->data);
		$json['success'] = 1;

		$this->response->setOutput(json_encode($json));
	}
	
	public function delete() {
		$seller_id = isset($this->request->get['seller_id']) ? $this->request->get['seller_id'] : 0;
		$this->MsLoader->MsSeller->deleteSeller($seller_id);
		$this->response->redirect($this->url->link('multiseller/seller', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function index() {
		$this->validate(__FUNCTION__);

		// paypal listing payment confirmation
		if (isset($this->request->post['payment_status']) && strtolower($this->request->post['payment_status']) == 'completed') {
			$this->data['success'] = $this->language->get('ms_payment_completed');
		}

		$this->data['total_balance'] = sprintf($this->language->get('ms_catalog_sellers_total_balance'), $this->currency->format($this->MsLoader->MsBalance->getTotalBalanceAmount(), $this->config->get('config_currency')), $this->currency->format($this->MsLoader->MsBalance->getTotalBalanceAmount(array('seller_status' => array(MsSeller::STATUS_ACTIVE))), $this->config->get('config_currency')));
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$this->data['token'] = $this->session->data['token'];		
		$this->data['heading'] = $this->language->get('ms_catalog_sellers_heading');
		$this->data['link_create_seller'] = $this->url->link('multiseller/seller/create', 'token=' . $this->session->data['token'], 'SSL');
		$this->document->setTitle($this->language->get('ms_catalog_sellers_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multiseller/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multiseller/seller', '', 'SSL'),
			)
		));
		
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/seller.tpl', $this->data));
	}
	
	public function create() {
		$this->validate(__FUNCTION__);
		$this->load->model('localisation/country');
		$this->load->model('tool/image');
		$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		$this->load->model('localisation/account');
		$this->data['accounts'] = $this->model_localisation_account->getAccounts();
		
		$this->load->model('localisation/logistic');
		$this->data['logistics'] = $this->model_localisation_logistic->getLogistic();
			
		$this->data['account_id'] = 0;
		$this->data['customers'] = $this->MsLoader->MsSeller->getCustomers();
		$this->data['seller_groups'] =$this->MsLoader->MsSellerGroup->getSellerGroups();
		$this->data['seller'] = FALSE;
		
		//edit by arin
		$this->data['seller_type'] = '';
		$this->data['seller_group'] = '';

		$this->data['currency_code'] = $this->config->get('config_currency');
		$this->data['token'] = $this->session->data['token'];
		$this->data['heading'] = $this->language->get('ms_catalog_sellerinfo_heading');
		$this->document->setTitle($this->language->get('ms_catalog_sellerinfo_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multiseller/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multiseller/seller', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_catalog_sellers_newseller'),
				'href' => $this->url->link('multiseller/seller/create', 'SSL'),
			)
		));		
		
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/seller-form.tpl', $this->data));
	}	
	
	public function update() {
		$this->validate(__FUNCTION__);
		$this->load->model('tool/image');	

		$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);

		$this->data['seller_groups'] =$this->MsLoader->MsSellerGroup->getSellerGroups();  
			
		if (!empty($seller)) {
			$this->load->model('localisation/account');
			$this->data['accounts'] = $this->model_localisation_account->getAccounts();
		
			$seller['logistic'] = $this->MsLoader->MsSeller->getLogistics($this->request->get['seller_id']);
			$this->data['country_id'] = $seller['ms.country_id'];
			
			$this->load->model('localisation/country');
			$this->data['countries'] = $this->model_localisation_country->getCountries();
		
			$this->data['zone_id'] = $seller['ms.zone_id'];
			
			$this->load->model('localisation/zone');
			$this->data['zones'] = $this->model_localisation_zone->getZonesByCountryId($this->data['country_id']);
			
			$this->data['city_id'] = $seller['ms.city_id'];
			
			$this->load->model('localisation/city');
			$this->data['cities'] = $this->model_localisation_city->getCitiesByZoneId($this->data['zone_id']);
			
			$this->data['district_id'] = $seller['ms.district_id'];
			
			$this->load->model('localisation/district');
			$this->data['districts'] = $this->model_localisation_district->getDistrictsByCityId($this->data['city_id']);
			
			$this->data['subdistrict_id'] = $seller['ms.subdistrict_id'];
			
			$this->load->model('localisation/subdistrict');
			$this->data['subdistricts'] = $this->model_localisation_subdistrict->getSubdistrictsByDistrictId($this->data['district_id']);
			
			$this->load->model('localisation/logistic');
			$this->data['logistics'] = $this->model_localisation_logistic->getLogistic();
			
			$this->data['account_id'] = $seller['ms.account_id'];
			
			//edit by arin
			$this->data['seller_type'] = $seller['seller_type'];
			$this->data['seller_group'] = $seller['ms.seller_group'];
			$this->data['seller_id'] = $this->request->get['seller_id'];
			
			$rates = $this->MsLoader->MsCommission->calculateCommission(array('seller_id' => $this->request->get['seller_id']));
			$actual_fees = '';
			foreach ($rates as $rate) {
				if ($rate['rate_type'] == MsCommission::RATE_SIGNUP) continue;
				$actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . $rate['percent'] . '%+' . $this->currency->getSymbolLeft() .  $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', FALSE) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
			}
			
			$this->data['seller'] = $seller;
			$this->data['seller']['actual_fees'] = $actual_fees;
			
			if (!empty($seller['ms.avatar'])) {
				$this->data['seller']['avatar']['name'] = $seller['ms.avatar'];
				$this->data['seller']['avatar']['thumb'] = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_preview_seller_avatar_image_width'), $this->config->get('msconf_preview_seller_avatar_image_height'));
				//$this->session->data['multiseller']['files'][] = $seller['avatar'];
			}
			
			if (is_null($seller['ms.commission_id']))
				$rates = NULL;
			else
				$rates = $this->MsLoader->MsCommission->getCommissionRates($seller['ms.commission_id']);
			
			$this->data['seller']['commission_id'] = $seller['ms.commission_id'];	
			$this->data['seller']['commission_rates'] = $rates;
		}else{
			$this->response->redirect($this->url->link('multiseller/seller/create', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->data['currency_code'] = $this->config->get('config_currency');
		$this->data['token'] = $this->session->data['token'];		
		$this->data['heading'] = $this->language->get('ms_catalog_sellerinfo_heading');
		$this->document->setTitle($this->language->get('ms_catalog_sellerinfo_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multiseller/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multiseller/seller', '', 'SSL'),
			),			
			array(
				'text' => $seller['ms.nickname'],
				'href' => $this->url->link('multiseller/seller/update', '&seller_id=' . $seller['seller_id'], 'SSL'),
			)
		));		
		
		$this->load->model('multiseller/seller');
		
		$this->data['commission_categories'] = $this->model_multiseller_seller->getCategories($this->request->get['seller_id']);
		
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/seller-form.tpl', $this->data));
	}
	private function generate_random_password($length = 10) {
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('_','.');
		$final_array = array_merge($alphabets,$numbers);
			 
		$password = '';
	  
		while($length--) {
		  $key = array_rand($final_array);
		  $password .= $final_array[$key];
		}
	  
		return $password;
  }
    private function checkDate($date, $format = 'Y-m-d'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
  }
	
	public function premium(){
		if(isset($this->request->get['seller_id'])){
			$seller_id=$this->request->get['seller_id'];
		}else{
			$seller_id=0;
		}
		
		$this->MsLoader->MsSeller->grantPremiumSeller($seller_id);
		
		$this->session->data['success'] = 'Seller account granted premium access.';
		$this->response->redirect($this->url->link('multiseller/seller', 'token=' . $this->session->data['token'], 'SSL'));
	}
}
?>
