<?php
// Heading
$_['heading_title']        = 'OpenCart';

// Text
$_['text_order']           = 'Orders';
$_['text_order_status']    = 'Pending';
$_['text_checkout_status']    = 'Checkout';
$_['text_expired_status']    = 'Expired';
$_['text_complete_status'] = 'Completed';
$_['text_customer']        = 'Customers';
$_['text_online']          = 'Customers Online';
$_['text_seller']          = 'Sellers';
$_['text_seller_fault']    = 'Seller Fault';
$_['text_approval']        = 'Pending approval';
$_['text_product']         = 'Products';
$_['text_stock']           = 'Out of stock';
$_['text_review']          = 'Reviews';
$_['text_return']          = 'Returns';
$_['text_affiliate']       = 'Affiliates';
$_['text_store']           = 'Stores';
$_['text_front']           = 'Store Front';
$_['text_help']            = 'Help';
$_['text_homepage']        = 'Homepage';
$_['text_support']         = 'Support Forum';
$_['text_documentation']   = 'Documentation';
$_['text_logout']          = 'Logout';

$_['text_confirm_payment']          = 'Confirm Payment';
$_['text_withdrawal']          		= 'Withdrawal Request';