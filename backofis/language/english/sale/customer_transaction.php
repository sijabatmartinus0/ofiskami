<?php
$_['text_heading'] = 'Transactions';
$_['text_transactions'] = 'Transactions';
$_['label_id'] = '#';
$_['label_customer'] = 'Customer';
$_['label_net_amount'] = 'Net amount';
$_['label_date'] = 'Date';
$_['label_description'] = 'Description';
