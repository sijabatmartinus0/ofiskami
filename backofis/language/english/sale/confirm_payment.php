<?php
// Heading
$_['heading_title']    	= 'Confirm Payment Order';
$_['detail_title']    	= 'Confirm Payment Detail';

// Text
$_['text_list']     	= 'Order List';
$_['text_order_id']     = 'Order ID';
$_['text_customer']     = 'Customer';
$_['text_invoice']     	= 'Invoice No';
$_['text_date_added']   = 'Date Added';
$_['text_no_results']   = 'No results';
$_['text_detail']   	= 'Payment Confirmation Detail';
$_['text_bank']   		= 'Bank Destination';
$_['text_bank_origin']  = 'Bank Origin';
$_['text_choose']   	= 'Please choose';

// Column
$_['column_customer']  	= 'Customer';
$_['column_order_id']  	= 'Order ID';
$_['column_invoice']  	= 'Invoice No';
$_['column_total']  	= 'Total';
$_['column_date_added'] = 'Date Added';
$_['column_action'] 	= 'Action';

// Button
$_['button_accept'] 	= 'Accept';
$_['button_cancel'] 	= 'Cancel';
$_['button_submit'] 	= 'Submit';
$_['button_search'] 	= 'Search';

?>