<?php
$_['text_heading'] = 'Withdrawals';
$_['text_breadcrumbs'] = 'Withdrawals';
$_['text_payout_requests'] = 'Withdrawal requests';
$_['text_payouts'] = 'Manual withdrawals';
$_['text_payouts_create'] = 'Create';
$_['text_pending'] = 'Pending';
$_['text_new'] = 'New withdrawal';
$_['text_paid'] = 'Paid';
$_['text_deduct'] = 'Deduct from balance';
$_['text_payout_paypal'] = 'Pay out via PayPal';
$_['text_payout_paypal_invalid'] = 'PayPal address not specified or invalid';
$_['text_mark'] = 'Mark as paid';
$_['text_delete'] = 'Delete payment record';

$_['text_type_' . Payment::TYPE_PAYOUT] = 'Manual Withdrawal';
$_['text_type_' . Payment::TYPE_PAYOUT_REQUEST] = 'Withdrawal request';


$_['text_status_' . Payment::STATUS_UNPAID] = 'Unpaid';
$_['text_status_' . Payment::STATUS_PAID] = 'Paid';

$_['text_filter_date_start'] = 'From';
$_['text_filter_date_end'] = 'To';
$_['text_filter_status'] = 'Status';
$_['text_button_filter'] = 'Filter';
$_['text_button_export'] = 'Export';

$_['error_fromto'] = 'Only store to seller and seller to store payments are supported.';
$_['error_fromstore'] = 'Outgoing payments (store to seller) can only be of types Payout or Payout request';
$_['error_tostore'] = 'Incoming payments (seller to store) can not be of types Payout or Payout request';
$_['error_amount'] = 'Please specify a valid payment amount';
$_['error_norequests'] = 'Error: no valid payouts to process. Please make sure the sellers have enough funds and valid PayPal addresses specified';
$_['success_created'] = 'Payment successfully created';

$_['label_none'] = 'None';
$_['label_customer'] = 'Customer';
$_['label_all_sellers'] = 'All sellers';
$_['label_amount'] = 'Amount';
$_['label_product'] = 'Product';
$_['label_net_amount'] = 'Net amount';
$_['label_days'] = 'days';
$_['label_from'] = 'From';
$_['label_to'] = 'To';
$_['label_paypal'] = 'PayPal';
$_['label_date_created'] = 'Date created';
$_['label_status'] = 'Status';
$_['label_image'] = 'Image';
$_['label_date_modified'] = 'Date modified';
$_['label_date_paid'] = 'Date paid';
$_['label_date'] = 'Date';
$_['label_description'] = 'Description';

$_['label_enabled'] = 'Enabled';
$_['label_disabled'] = 'Disabled';
$_['label_apply'] = 'Apply';
$_['label_type'] = 'Type';
$_['label_type_checkbox'] = 'Checkbox';
$_['label_type_date'] = 'Date';
$_['label_type_datetime'] = 'Date &amp; Time';
$_['label_type_file'] = 'File';
$_['label_type_image'] = 'Image';
$_['label_type_radio'] = 'Radio';
$_['label_type_select'] = 'Select';
$_['label_type_text'] = 'Text';
$_['label_type_textarea'] = 'Textarea';
$_['label_type_time'] = 'Time';
$_['text_image_manager'] = 'Image Manager';
$_['text_browse'] = 'Browse';
$_['text_clear'] = 'Clear';
$_['label_store'] = 'Store';
$_['label_id'] = '#';
$_['label_action'] = 'Action';

$_['label_payment_method'] = 'Payment method';
$_['label_payment_method_balance'] = 'Customer balance';
$_['label_payment_paid'] = 'Paid';
$_['label_payment_deduct'] = 'Deduct from balance';

$_['error_payment_filter_date'] = 'Please fill the date range filter!';

$_['error_withdraw_norequests'] = 'Error: no payouts to process';
$_['error_withdraw_response'] = 'Error: no response';
$_['error_withdraw_status'] = 'Error: unsuccessful transaction';
$_['success'] = 'Success';
$_['success_transactions'] = 'Transactions successfully completed';
$_['success_payment_deleted'] = 'Payment deleted';
$_['text_success']                 = 'Success: You have modified settings!';

$_['text_subject'] 				= '%s - Withdrawal Update';
$_['text_customer'] 			= 'Customer: ';
$_['text_withdrawal'] 			= 'Withdrawal: ';
$_['text_date_added'] 			= 'Date Request: ';
$_['text_amount'] 				= 'Amount: ';
$_['text_description'] 			= 'Description: ';
$_['text_withdrawal_link'] 		= 'To view your withdrawal status click on the link below:';
$_['text_withdrawal_status'] 	= 'Your withdrawal request has been updated to the following status:';
$_['text_withdrawal_request'] 	= 'Withdrawal Request';
$_['text_footer'] 				= 'Please reply to this email if you have any questions.';



