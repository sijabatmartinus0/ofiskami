<?php
// Heading
$_['heading_title']      = 'Espay Payment Gateway';

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified Square Gate One Payment Gateway details!';
$_['text_edit']          = 'Edit SGO Payment';

// Entry
$_['entry_bank']         = 'Bank Transfer Instructions';
$_['entry_total']        = 'Total';
$_['entry_sgopayment_id']        = 'Payment Id';
$_['entry_sgopayment_password']  = 'Password';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['help_sgopaymentid']		= 'Id that used for partner identification.';
$_['help_sgopayment_password']		= 'Password that used for web service identification.';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify sgo payment gateway module!';
$_['error_payment_id']   = 'Payment Id Required!';
$_['error_password']     = 'Password Required!';


$_['text_sgopayment']     = '<a onclick="window.open(\'http://www.sgo.co.id/\');"><img src="view/image/payment/espay.png" alt="Square Gate One Payment Gateways" title="Square Gate One Payment Gateways" style="border: 1px solid #EEEEEE;" /></a>';

?>