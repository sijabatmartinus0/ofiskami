<?php
// Heading
$_['heading_title']		 = 'Employee Program';

// Text
$_['text_payment']		 = 'Payment';
$_['text_success']		 = 'Success: You have modified bank transfer details!';
$_['text_edit']          = 'Edit Employee Program';

// Entry
$_['entry_bank']		 = 'Employee Program Instructions';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']	 = 'Geo Zone';
$_['entry_status']		 = 'Status';
$_['entry_sort_order']	 = 'Sort Order';
$_['entry_employee_program_customer_group']	 = 'Customer Group';
$_['entry_employee_program_customer_group_select_all']	 = 'Select All';
$_['entry_employee_program_customer_group_unselect_all']	 = 'Unselect All';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment bank transfer!';
$_['error_bank']         = 'Employee Program Instructions Required!';