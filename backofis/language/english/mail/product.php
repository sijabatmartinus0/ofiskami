<?php
// Text
$_['text_update_product']  = 'Your wishlist product has been ';
$_['text_update_product_link']          = 'To view your wishlist product click on the link below:';
$_['text_subject']          	= '%s - Restock Product';
$_['text_product']          	= 'Product';
$_['text_model']          	= 'Model';
$_['text_sku']          	= 'SKU';
$_['text_manufacturer']          	= 'Manufacturer';