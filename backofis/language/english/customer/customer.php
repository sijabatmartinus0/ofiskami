<?php
	$_['heading_title'] 	= 'Customer';

	$_['text_list']			= 'List Customer';

	$_['column_customer_no']	= 'No';
	$_['column_customer_nama']	= 'Nama';
	$_['column_customer_email'] = 'Email';
	$_['column_customer_telephone'] = 'Telephone';
	$_['column_customer_date_added'] = 'Date Registered';

	$_['entry_customer_nama']	= 'Nama';
	$_['entry_customer_date_start']	= 'Date Start';
	$_['entry_customer_date_end']	= 'Date End';

	$_['button_filter']		= 'Filter';
	$_['button_download']	= 'Download to Excel';

	$_['error_warning']                           = 'Warning: Please check the form carefully for errors!';
    $_['error_permission']                        = 'Warning: You do not have permission to modify orders!';
    $_['error_curl']                              = 'Warning: CURL error %s(%s)!';
    $_['error_action']                            = 'Warning: Could not complete this action!';
?>