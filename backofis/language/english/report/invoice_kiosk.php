<?php
// Heading
$_['heading_title']    = 'Invoice Kiosk';

// Text
$_['text_batch']       	= 'Batch';
$_['text_kiosk']     = 'Kiosk';
$_['text_no_results']   = 'No results';
$_['text_no_data']   = 'No data';
$_['text_yes']   		= 'Yes';
$_['text_no']   		= 'No';

// Column
$_['column_kiosk']  = 'Kiosk';
$_['column_batch']     = 'Batch';
$_['column_invoice']     = 'Invoice';
$_['column_action']    = 'Action';

// Button
$_['button_view_detail'] 	= 'View Detail';
$_['button_print'] 		= 'Print';
$_['button_filter'] 		= 'Search';
$_['button_download'] 		= 'Download to Excel';
$_['button_back'] 			= 'Back';
$_['button_paid'] 			= 'Payout';
$_['button_created'] 			= 'Create Billing';

$_['text_success']           = 'Success: You have modified report payout!';
$_['error_permission']       = 'Warning: You do not have permission to modify report payout!';
$_['text_success_billing']           = 'Success: You have modified report billing!';
$_['error_permission_billing']       = 'Warning: You do not have permission to modify report billing!';

?>