<?php
// Heading
$_['heading_title']    = 'Merchant Evaluation Report';
$_['heading_title_1']    = 'Merchant SKU Report';
// Text
$_['text_list']         = 'Merchant List';
$_['text_period']       = 'Period';
$_['text_year']       	= 'Year';
$_['text_merchant']     = 'Merchant';
$_['text_month_first']  = 'January - June';
$_['text_month_second'] = 'July - December';
$_['text_order'] 		= 'order(s)';

//text sku
$_['text_name']         = 'Merchant Name';
$_['text_date_created']       = 'Date Start';
$_['text_date_ended']       	= 'Date End';
$_['text_total']     = 'Total SKU';

// Column
$_['column_merchant']  = 'Merchant';
$_['column_score']     = 'Final Score<br>(1 - 5)';
$_['column_total']     = 'Total Order';
$_['column_penambahan_sku'] = 'Penambahan SKU';
$_['column_total_sku']     = 'Total SKU';
$_['column_action']    = 'Action';
$_['column_date_created']       = 'Date Start';
$_['column_date_ended']       	= 'Date End';

// Button
$_['button_view_detail'] = 'View Detail';
$_['button_filter'] 	= 'Search';
$_['button_download'] 	= 'Download to Excel';
$_['button_back'] 		= 'Back';

//report view detail
$_['column_number']			= 'No';
$_['column_criteria']		= 'Criteria';
$_['column_weight']			= 'Weight<br>(%)';
$_['column_weight_xls']		= 'Weight';
$_['column_score_view']		= 'Score<br>(1-5)';
$_['column_score_view_xls']	= 'Score (1-5)';
$_['column_final_score']	= 'Final Score';
$_['column_total_score']	= 'TOTAL';
$_['column_hundred']		= '100%';

//criteria
$_['criteria_1']    		= 'Approve Order by Merchant';
$_['criteria_2']    		= 'Reject Order by SLA';
$_['criteria_3']    		= 'Undelivered Order by SLA';
$_['criteria_4']    		= 'Quality';
$_['criteria_5']    		= 'Accuracy';
$_['criteria_6']    		= 'Customer Complain';
