<?php
// Heading
$_['heading_title']    = 'Bill Due';
$_['heading_title2']    = 'Payout';
$_['heading_title3']    = 'Income';
$_['heading_title4']    = 'Transaction';
$_['heading_title5']    = 'Penalty';
$_['heading_title6']    = 'Summary';

// Text
$_['text_list']         = 'Income List';
$_['text_billdue_list']         = 'Bill Due List';
$_['text_summary_list']         = 'Summary List';
$_['text_summary']         = 'Summary';
$_['text_payout_list']         = 'Payout List';
$_['text_transaction_list']         = 'Transaction List';
$_['text_penalty_list']         = 'Penalty List';
$_['text_status']         = 'Status';
$_['text_date']         = 'Modified Date';
$_['text_batch']       	= 'Batch';
$_['text_merchant']     = 'Seller';
$_['text_no_results']   = 'No results';
$_['text_no_data']   = 'No data';
$_['text_download_date']   = 'Download Date';
$_['text_yes']   		= 'Yes';
$_['text_no']   		= 'No';

$_['text_bill_due_vat']   		= 'Bill Due(Include VAT)';
$_['text_minimum_deposit']   		= 'Minimum Deposit';
$_['text_deposit_balance']   		= 'Deposit Balance';
$_['text_add_deposit']   		= 'Add Deposit';
$_['text_vendor_balance']   		= 'Total Vendor Balance';

$_['text_transaction']   		= 'Transaction';
$_['text_deposit_deduction']   		= 'Deposit Deduction Detail';
$_['text_total_payout']   		= 'Total Payout';
$_['text_deposit']   		= 'Deposit';

$_['entry_date_start']   		= 'Modified Date';
$_['entry_date_end']   		= 'To Date';

$_['text_total_commission_tax']   		= 'Total(Commission+Tax)';
$_['text_total_online_payment_fee']   		= 'Total(Online Payment Fee)';
// Column
$_['column_merchant']  = 'Seller';
$_['column_batch']     = 'Batch';
$_['column_total']     = 'Total Nominal';
$_['column_action']    = 'Action';
$_['column_download']  = 'Downloaded';

// Button
$_['button_view_detail'] 	= 'View Detail';
$_['button_filter'] 		= 'Search';
$_['button_download'] 		= 'Download to Excel';
$_['button_back'] 			= 'Back';
$_['button_paid'] 			= 'Payout';
$_['button_created'] 			= 'Create Billing';

//report view detail
$_['column_order_no']			= 'Receipt No.';
$_['column_vendor_name']		= 'Seller Name';
$_['column_vendor_type']		= 'Seller Type';
$_['column_kiosk_id']			= '#ID Kiosk';
$_['column_transaction_date']	= 'Transaction Date';
$_['column_total_transaction']	= 'Total Transaction';
$_['column_quantity']			= 'Qty';
$_['column_sku']				= 'SKU';
$_['column_code']				= 'Product Code';
$_['column_price']				= 'Price';
$_['column_total_report']		= 'Total';
$_['column_tax']				= 'Tax (PPN)';
$_['column_commission']			= 'Commission Base';
$_['column_shipping']			= 'Shipping Fee';
$_['column_online_payment']		= 'Online Payment Fee';
$_['column_ofiskita_commission']= 'Commission';
$_['column_tax_ppn']			= 'Tax (PPN for Commission)';
$_['column_vendor_balance']		= 'Vendor Balance';
$_['column_payment_type']		= 'Payment Type';
$_['column_revenue_type']		= 'Revenue Type';
$_['column_business_type']		= 'Business Type';
$_['column_tax_type']			= 'Tax Type';
$_['column_relation_type']		= 'Relation Type';
$_['column_payment_total']		= 'Payment Total';
$_['column_bank']				= 'Bank';
$_['column_bank_account']				= 'Bank Account';
$_['column_detail']				= 'Detail';
$_['column_revenue']				= 'Revenue';
$_['column_penalty']				= 'Penalty';
$_['column_description']				= 'Description';
$_['column_amount']				= 'Amount';
$_['column_date_added']				= 'Date';
$_['column_disc_ofiskita']				= 'Disc. by Ofiskita';
$_['column_disc_merchant']				= 'Disc. by Merchant';
$_['column_paid_customer']				= 'Paid by Customer';
$_['column_cash_over']				= 'Cash Over';
$_['column_commission_fee']				= 'Commission Fee';
$_['column_transaction_fee']				= 'Transaction Fee';
$_['column_total_fee']				= 'Total Fee';
$_['column_refund']				= 'Refund';

$_['text_success']           = 'Success: You have modified report payout!';
$_['error_permission']       = 'Warning: You do not have permission to modify report payout!';
$_['text_success_billing']           = 'Success: You have modified report billing!';
$_['error_permission_billing']       = 'Warning: You do not have permission to modify report billing!';

?>