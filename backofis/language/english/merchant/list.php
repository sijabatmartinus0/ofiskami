<?php

//Heading
    $_['heading_title']     = 'Merchant';
    
    //Text
    $_['text_list']         = 'Merchant List';
    $_['text_add']          = 'Add Merchant';
    $_['text_edit']         = 'Edit Merchant';
    $_['text_merchant_detail'] = 'Merchant Details';
    $_['text_merchant_id']      = 'Merchant ID';
    $_['text_merchant_nama']    = 'Nama';
    $_['text_merchant_badan_usaha'] = 'Badan Usaha';
    $_['text_merchant_email']       = 'Email';
    $_['text_merchant_telephone']   = 'Nomor Telepon';
    $_['text_merchant_kategori_produk'] = 'Kategori Produk';
    $_['text_merchant_website_toko']    = 'Website Toko';
    $_['text_merchant_npwp']            = 'NPWP';
    $_['text_delete']                  = 'Are you sure to delete this merchant?';
    $_['text_success']        = 'Success: You have modified options!';

    $_['text_yes']      = 'Yes';
    $_['text_no']      = 'No';
    $_['text_request']      = 'Request';
    $_['text_approve']      = 'Approve';
    $_['text_reject']       = 'Reject';
    
    //column
    $_['column_merchant_id']      = 'Merchant ID';
    $_['column_merchant_nama']    = 'Nama';
    $_['column_merchant_badan_usaha'] = 'Badan Usaha';
    $_['column_merchant_email']       = 'Email';
    $_['column_merchant_telephone']   = 'Nomor Telepon';
    $_['column_merchant_kategori_produk'] = 'Kategori Produk';
    $_['column_merchant_website_toko']    = 'Website Toko';
    $_['column_merchant_npwp']            = 'NPWP';
    $_['column_merchant_status']            = 'Status';
    $_['column_merchant_action']            = 'Action';
    $_['column_merchant_created_at']            = 'Created At';
    $_['column_merchant_updated_at']            = 'Updated At';

    //entry
    $_['entry_merchant_id']      = 'Merchant ID';
    $_['entry_merchant_nama']    = 'Nama';
    $_['entry_merchant_badan_usaha'] = 'Badan Usaha';
    $_['entry_merchant_kategori_produk'] = 'Kategori Produk';
    $_['entry_merchant_website_toko']    = 'Website Toko';
    $_['entry_merchant_npwp']            = 'NPWP';
    $_['entry_merchant_status']    = 'Status';

    $_['error_warning']                           = 'Warning: Please check the form carefully for errors!';
    $_['error_permission']                        = 'Warning: You do not have permission to modify orders!';
    $_['error_curl']                              = 'Warning: CURL error %s(%s)!';
    $_['error_action']                            = 'Warning: Could not complete this action!';


    //email
    $_['text_subject_merchant'] = 'Pendaftaran Merchant';
    $_['text_title_merchant'] = 'Pendaftaran Merchant';
    $_['text_header_merchant'] = 'Pendaftaran anda telah diterima';
    $_['message_merchant'] = 'Terima kasih sudah mendaftar sebagai merchant Ofiskita.';
    $_['message_link_merchant'] = 'Anda dapat melihat status order terakhir anda pada link berikut <a style=\'color: #348eda;\' href=\'%s\'>Lihat Order</a>';

    $_['text_message_warning']      = 'Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat ofiskita.com bukan alamat lainnya.';
?>