<?php
// Heading
$_['heading_title']          	= 'Pop Up Banner';

// Entry
$_['popup_image']           	= 'Pop Up Image';
$_['text_form']           		= 'Setting Pop Up Banner';
$_['text_width']      			= 'Width';
$_['text_height'] 	     		= 'Height';
$_['entry_image']            	= 'Image';
$_['entry_status']           	= 'Status';

// Error
$_['error_warning']          	= 'Warning: Please check the form carefully for errors!';
$_['error_permission']       	= 'Warning: You do not have permission to modify categories!';

