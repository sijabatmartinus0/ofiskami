<?php
// Heading
$_['heading_title']          	= 'Slide Banner';

// Entry
$_['slide_image']           	= 'Slide Image';
$_['text_form']           		= 'Setting Slide Banner';
$_['text_width']      			= 'Width';
$_['text_height'] 	     		= 'Height';
$_['entry_image']            	= 'Image';
$_['entry_status']           	= 'Status';
$_['entry_type']           	= 'Type';
$_['entry_value']           	= 'Value';
$_['entry_sort_order']           	= 'Sort Order';

$_['button_slide_add']           	= 'Add Slide';

// Tab
$_['tab_data']           	= 'Data';
$_['tab_setting']           	= 'Setting';

// Error
$_['error_warning']          	= 'Warning: Please check the form carefully for errors!';
$_['error_permission']       	= 'Warning: You do not have permission to modify categories!';

