<?php
// Text
$_['heading_title']        = 'Upload Customer';
$_['text_customer']        = 'Customer Group';
$_['text_choose']          = 'Please choose';
$_['text_success_upload']  = 'Your file was successfully uploaded!';
$_['text_success_save']    = "Customer's email saved";

// Entry
$_['entry_upload']         = 'Upload File';
$_['entry_choose_file']    = 'Choose file';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify extensions!';
$_['error_temporary']   = 'Warning: There are some temporary files that require deleting. Click the clear button to remove them!';
$_['error_upload']		= 'Upload required';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_customer']    = 'Please choose customer group';
$_['error_email']    	= 'Invalid input at row ';
$_['error_format']    	= 'Error template';

//Help
$_['help_upload']     	   = 'Upload .xls or .xlsx file';
$_['help_customer']        = 'Choose one of customer group';

?>