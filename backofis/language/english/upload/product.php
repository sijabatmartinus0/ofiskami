<?php
// Text
$_['heading_title']        = 'Upload Product';
$_['text_category']        = 'Product Category';
$_['text_seller']          = 'Seller';
$_['text_choose']          = 'Please choose';
$_['text_success_upload']  = 'Your file was successfully uploaded!';
$_['text_success_save']    = "Product saved";

// Entry
$_['entry_upload']         = 'Upload File';
$_['entry_choose_file']    = 'Choose file';
$_['entry_overwrite']      = 'Files that will be overwritten';
$_['entry_progress']       = 'Progress';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify extensions!';
$_['error_temporary']   = 'Warning: There are some temporary files that require deleting. Click the clear button to remove them!';
$_['error_upload']		= 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_category']    = 'Please choose product category';
$_['error_seller']      = 'Please choose seller group';
$_['error_sku']      	= 'Duplicate SKU at row ';
$_['error_agi']      	= 'Can not found attribute group id from the choosen product category';
$_['error_attr_name']   = 'Can not found attribute name at column ';
$_['error_manufacturer']= 'Can not found manufacturer at row ';
$_['error_format']		= 'Invalid template format';
$_['error_numeric']		= 'Invalid input (numeric only) at row ';
$_['error_date']		= 'Invalid date format (use YYYY-MM-DD format) at row ';
$_['error_blank']		= 'You have empty value at row ';
$_['error_image']		= 'File extension of image must be in .jpg, .jpeg, or .png at row ';

//Help
$_['help_upload']     	   = 'Upload .xls or .xlsx file';
$_['help_product']     	   = 'Choose one of product categories';
$_['help_seller']     	   = 'Choose one of the sellers';

?>