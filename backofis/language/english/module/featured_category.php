<?php

$_['heading_title']    	= 'Featured Category';
$_['text_module']      	= 'Modules';

$_['text_form'] 		= 'Featured Category Setting';
$_['banner_image'] 		= 'Banner Size';
$_['product_image'] 	= 'Product Size';

$_['text_width'] 		= 'Width';
$_['text_height'] 		= 'Height';

$_['entry_category'] 	= 'Category';
$_['entry_sort_order'] 	= 'Sort Order';
$_['entry_image'] 		= 'Image';
$_['entry_status'] 		= 'Status';
$_['entry_mobile'] 		= 'Image Mobile';

$_['tab_category'] 		= 'Category';
$_['tab_setting'] 		= 'Setting';

$_['text_success']     	= 'Success: You have modified product comments module!';

$_['button_save'] 		= 'Save';
$_['button_cancel'] 	= 'Cancel';
$_['button_category_add'] 	= 'Add Category';

// Error
$_['error_permission'] 	= 'Warning: You do not have permission to modify module!';
$_['error_category'] 	= 'Warning: Please fill blank category!';

?>