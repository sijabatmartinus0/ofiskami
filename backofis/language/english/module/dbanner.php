<?php
// Heading
$_['heading_title']          = '<i class="fa fa-th-large"></i> <b>DBanner</b>';
$_['heading_title_clean']    = 'DBanner';

// Entry
$_['entry_name']           = 'Module Name';
$_['entry_title']          = 'Title';
$_['entry_grid_set']       = 'Grid columns to occupy grid options';
$_['entry_grid_offset']    = 'Grid left offset grid offset';
$_['entry_padding']        = 'Boxes padding';
$_['entry_mobile_layout']  = 'Image full width on mobile';
$_['entry_margin_left']    = 'Margin left';
$_['entry_margin_top']     = 'Margin top';
$_['entry_margin_right']   = 'Margin right';
$_['entry_margin_bottom']  = 'Margin bottom';
$_['entry_effect_type']    = 'Effect type';
$_['entry_show_on_mobile'] = 'Show on mobile';

// Text
$_['text_module']          = 'Modules';
$_['text_success']         = 'Success: You have modified module DBanner!';
$_['text_edit']            = 'Edit';
$_['text_settings']        = 'Settings';
$_['text_margins']         = 'Margins';
$_['text_effects']         = 'Effects';
$_['text_banners']         = 'Banners';
$_['text_add_new_widget']  = 'Add new widget';
$_['text_edit_widget']     = 'Edit widget';
$_['text_title']           = 'Title';
$_['text_link']            = 'Link';

$_['text_save']            = 'Save';
$_['text_cancel']          = 'Cancel';

$_['info_mobile_layout']     = 'This option works only if Image full width on mobile is enabled.';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module DBanner!';
$_['error_image']         = 'Image width &amp; height dimensions required!';

$_['entry_status']      = 'Status';
$_['entry_heading']     = 'Heading Title';
$_['entry_description'] = 'Content';
?>
