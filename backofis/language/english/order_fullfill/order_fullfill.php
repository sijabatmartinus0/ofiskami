<?php

//Heading
    $_['heading_title']     = 'Order Fulfill';
    
    //Text
    $_['text_list']         = 'Order Fulfill List';
    $_['text_add']          = 'Add Merchant';
    $_['text_edit']         = 'Edit Merchant';
    $_['text_merchant_detail'] = 'Merchant Details';
    $_['text_merchant_id']      = 'Merchant ID';
    $_['text_merchant_nama']    = 'Nama';
    $_['text_merchant_badan_usaha'] = 'Badan Usaha';
    $_['text_merchant_email']       = 'Email';
    $_['text_merchant_telephone']   = 'Nomor Telepon';
    $_['text_merchant_kategori_produk'] = 'Kategori Produk';
    $_['text_merchant_website_toko']    = 'Website Toko';
    $_['text_merchant_npwp']            = 'NPWP';
    $_['text_delete']                  = 'Are you sure to delete this merchant?';
    $_['text_success']        = 'Success: You have modified options!';

    $_['text_yes']      = 'Yes';
    $_['text_no']      = 'No';
    $_['text_request']      = 'Request';
    $_['text_approve']      = 'Approve';
    $_['text_reject']       = 'Reject';
    
    //column
    $_['column_receipt_no']      = 'Receipt No';
    $_['column_no_resi']    = 'Nomor Resi';
    $_['column_id_kios'] = '#ID Kiosk';
    $_['column_transaction_date']       = 'Transaction Date';
    $_['column_quantity']   = 'Qty';
    $_['column_sku'] = 'SKU';
    $_['column_product_code']    = 'Product Code';
    $_['column_price_per_item']            = 'Price per Item';
    $_['column_shipping_fee']            = 'Shipping Fee';
    $_['column_discount_ofiskita']            = 'Final Price After Discount';
    $_['column_discount_merchant']            = 'Total Disc. by Merchant';
    $_['column_commission_fee']            = 'Commission Fee (%)';
    $_['column_transaction_fee']            = 'Transaction Fee (%)';
    $_['column_status_order']           = 'Status';
    //entry
    $_['entry_seller_id']       = 'Seller Id';
    $_['entry_receipt_no']      = 'Receipt No';
    $_['entry_start_date']    = 'Start Date';
    $_['entry_end_date'] = 'End Date';
    $_['entry_status_order'] = 'Status';

    $_['error_warning']                           = 'Warning: Please check the form carefully for errors!';
    $_['error_permission']                        = 'Warning: You do not have permission to modify orders!';
    $_['error_curl']                              = 'Warning: CURL error %s(%s)!';
    $_['error_action']                            = 'Warning: Could not complete this action!';
    $_['button_download']   = 'Download to Excel';

    //email
    $_['text_subject_merchant'] = 'Pendaftaran Merchant';
    $_['text_title_merchant'] = 'Pendaftaran Merchant';
    $_['text_header_merchant'] = 'Pendaftaran anda telah diterima';
    $_['message_merchant'] = 'Terima kasih sudah mendaftar sebagai merchant Ofiskita.';
    $_['message_link_merchant'] = 'Anda dapat melihat status order terakhir anda pada link berikut <a style=\'color: #348eda;\' href=\'%s\'>Lihat Order</a>';

    $_['text_message_warning']      = 'Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat ofiskita.com bukan alamat lainnya.';
?>
