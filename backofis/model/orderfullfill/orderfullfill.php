<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list
 *
 * @author MartinusIS
 */
class ModelOrderFullfillOrderFullfill extends Model {

    public function getOrderFullfill($data = array()) {
        $sql = "SELECT * FROM (SELECT CONCAT(invoice_prefix, invoice_no) as receipt_no, od.shipping_receipt_number as no_resi, opd.kiosk_id, od.date_added, op.quantity, p.sku, p.code, p.price, od.shipping_price, od.order_status_id, mcc.percent as commission_fee, os.name, od.seller_id FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id = op.order_detail_id LEFT JOIN " . DB_PREFIX . "product p ON op.product_id = p.product_id LEFT JOIN " . DB_PREFIX . "ms_order_product_data opd ON od.order_detail_id = opd.order_detail_id LEFT JOIN " . DB_PREFIX . "product_special ps ON p.product_id = ps.product_id LEFT JOIN " . DB_PREFIX . "ms_product x ON p.product_id = x.product_id LEFT JOIN " . DB_PREFIX . "ms_commission_category mcc ON x.seller_id = mcc.seller_id LEFT JOIN " . DB_PREFIX . "product_to_category z ON p.product_id = z.product_id AND z.category_id = mcc.category_id LEFT JOIN " . DB_PREFIX . "order_status os ON od.order_status_id = os.order_status_id) as x WHERE order_status_id IN (5, 19, 21)";
       if (!empty($data['filter_seller_id'])) {
            $sql .= " AND seller_id = '" . (int) $data['filter_seller_id'] . "'";
        } else {
            $sql .= " AND seller_id > '0'";
        }
	if (!empty($data['filter_status_id'])) {
            $sql .= " AND order_status_id = '" . (int) $data['filter_status_id'] . "'";
        } else {
            $sql .= " AND order_status_id > '0'";
        }
        if (!empty($data['filter_receipt_no'])) {
            $sql .= " AND receipt_no = '" . $this->db->escape($data['filter_receipt_no']) . "'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= DATE('" . $this->db->escape($data['filter_date_start']) . "')";
        }
        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_end']) . "')";
        }

        $sort_data = array(
            'receipt_no',
            'start_date',
            'end_date',
	    'no_resi',
            'kiosk_id',
            'sku',
            'code',
            'price',
            
            'order_status_id',
            'commission_fee',
            'quantity',
            'shipping_price',
            'name'
        );
        $sql .= " GROUP BY sku";
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY receipt_no";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        
        //echo $sql;die();
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalOrderFullfill($data = array()) {
        $sql = "SELECT * FROM (SELECT CONCAT(invoice_prefix, invoice_no) as receipt_no, od.shipping_receipt_number as no_resi, opd.kiosk_id, od.date_added, op.quantity, p.sku, p.code, p.price, od.shipping_price, od.order_status_id, mcc.percent as commission_fee, os.name, od.seller_id FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id = op.order_detail_id LEFT JOIN " . DB_PREFIX . "product p ON op.product_id = p.product_id LEFT JOIN " . DB_PREFIX . "ms_order_product_data opd ON od.order_detail_id = opd.order_detail_id LEFT JOIN " . DB_PREFIX . "product_special ps ON p.product_id = ps.product_id LEFT JOIN " . DB_PREFIX . "ms_product x ON p.product_id = x.product_id LEFT JOIN " . DB_PREFIX . "ms_commission_category mcc ON x.seller_id = mcc.seller_id LEFT JOIN " . DB_PREFIX . "product_to_category z ON p.product_id = z.product_id AND z.category_id = mcc.category_id LEFT JOIN " . DB_PREFIX . "order_status os ON od.order_status_id = os.order_status_id) as x WHERE order_status_id IN (5, 19, 21)";
        if (!empty($data['filter_seller_id'])) {
            $sql .= " AND seller_id = '" . (int) $data['filter_seller_id'] . "'";
        } else {
            $sql .= " AND seller_id > '0'";
        }
	if (!empty($data['filter_receipt_no'])) {
            $sql .= " AND receipt_no = '" . $this->db->escape($data['filter_receipt_no']) . "'";
        }
	if (!empty($data['filter_status_id'])) {
            $sql .= " AND order_status_id = '" . (int) $data['filter_status_id'] . "'";
        } else {
            $sql .= " AND order_status_id > '0'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= DATE('" . $this->db->escape($data['filter_date_start']) . "')";
        }
        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_end']) . "')";
        }
	$sql .= " GROUP BY sku";
        $total = "SELECT COUNT(*) as total FROM($sql) as xxx";
        
        
        // echo $sql;die();
        $query = $this->db->query($total);

        return $query->row['total'];
    }

    
    public function getSellers() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_type = 1 OR (seller_type = 0  AND seller_group = 1) ORDER BY nickname ASC");

        return $query->rows;
    }
    
    public function getOrderStatuses() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id IN (5,19,21)  GROUP BY order_status_id ORDER BY name ASC");

        return $query->rows;
    }

    public function exportToExcel($results) {
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

        if ($language_info) {
            $language_directory = $language_info['directory'];
        } else {
            $language_directory = '';
        }

        $language = new Language($language_directory);
        $language->load('order_fullfill/order_fullfill');


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Admin")
                ->setLastModifiedBy("Admin")
                ->setTitle($language->get('heading_title') . ' ' . $nickname)
                ->setSubject($language->get('heading_title'))
                ->setDescription($language->get('heading_title'))
                ->setKeywords("office 2007 openxml php");

        //header style
        $headerStyleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '4C82C5',
                )
            ),
        );

        //title style
        $titleStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFFFFF',
                )
            ),
        );

        $centerStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $leftStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $rightStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //bold style
        $titleFont = array(
            'font' => array(
                'bold' => true,
                'size' => 14
            )
        );
        $boldFont = array(
            'font' => array(
                'bold' => true
            )
        );

        // Column
        $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells('B2:L2')
                ->setCellValue('B2', $language->get('heading_title'))
                ->setCellValue('B6', $language->get('column_receipt_no'))
                ->setCellValue('C6', $language->get('column_no_resi'))
                ->setCellValue('D6', $language->get('column_id_kios'))
                ->setCellValue('E6', $language->get('column_transaction_date'))
                ->setCellValue('F6', $language->get('column_quantity'))
                ->setCellValue('G6', $language->get('column_sku'))
                ->setCellValue('H6', $language->get('column_product_code'))
                ->setCellValue('I6', $language->get('column_price_per_item'))
                ->setCellValue('J6', $language->get('column_shipping_fee'))
                ->setCellValue('K6', $language->get('column_commission_fee'))
                ->setCellValue('L6', $language->get('column_status_order'));

        //row
        $row = 7;
        foreach ($results as $result) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . $row, $result['receipt_no'])
                    ->setCellValue('C' . $row, (isset($result['no_resi']) ? $result['no_resi'] : '-'))
                    ->setCellValue('D' . $row, (isset($result['kiosk_id']) ? $result['kiosk_id'] : '-'))
                    ->setCellValue('E' . $row, date('d/m/Y H:i', strtotime($result['date_added'])))
                    ->setCellValue('F' . $row, $result['quantity'])
                    ->setCellValueExplicit('G' . $row, $result['sku'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValueExplicit('H' . $row, $result['code'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValueExplicit('I' . $row, (int) $result['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
                    ->setCellValueExplicit('J' . $row, (int) $result['shipping_price'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
                    ->setCellValue('K' . $row, $result['commission_fee'])
                    ->setCellValue('L' . $row, $result['name']);
            $row++;
        }

        
        $sheet->getStyle('B2')->applyFromArray($titleFont);
        $sheet->getStyle('B2')->applyFromArray($centerStyleArray);
        $sheet->getStyle('B6:L6')->applyFromArray($titleStyleArray);
        $sheet->getStyle('B6:L6')->applyFromArray($boldFont);
        $sheet->getStyle('B')->applyFromArray($centerStyleArray);
        $sheet->getStyle('C')->applyFromArray($centerStyleArray);
        $sheet->getStyle('D')->applyFromArray($centerStyleArray);
        $sheet->getStyle('E')->applyFromArray($centerStyleArray);
        $sheet->getStyle('F')->applyFromArray($centerStyleArray);
        $sheet->getStyle('G')->applyFromArray($centerStyleArray);
        $sheet->getStyle('H')->applyFromArray($centerStyleArray);
        $sheet->getStyle('I')->applyFromArray($centerStyleArray);
        $sheet->getStyle('J')->applyFromArray($centerStyleArray);
        $sheet->getStyle('K')->applyFromArray($centerStyleArray);
        $sheet->getStyle('L')->applyFromArray($centerStyleArray);
        
        $sheet->getColumnDimension('B')->setWidth(18);
        $sheet->getColumnDimension('C')->setWidth(18);
        $sheet->getColumnDimension('D')->setWidth(18);
        $sheet->getColumnDimension('E')->setWidth(18);
        $sheet->getColumnDimension('F')->setWidth(18);
        $sheet->getColumnDimension('G')->setWidth(18);
        $sheet->getColumnDimension('H')->setWidth(18);
        $sheet->getColumnDimension('I')->setWidth(18);
        $sheet->getColumnDimension('J')->setWidth(18);
        $sheet->getColumnDimension('K')->setWidth(24);
        $sheet->getColumnDimension('L')->setWidth(22);
        
	//set column color
        $sheet->getStyle('B6:L6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B13:E13')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //        $sheet->getStyle('C15:C16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B15:B16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //set border line
        $border_style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'A3A3A3'),
                ),
            ),
        );

        $border_thick = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '8C8C8C'),
                ),
            ),
        );

        //        $sheet->getStyle("B6:E13")->applyFromArray($border_style);
        //        $sheet->getStyle("B15:E16")->applyFromArray($border_style);
        // Rename worksheet
        $sheet->setTitle($language->get('heading_title'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Set logo header
        // $objDrawing = new PHPExcel_Worksheet_Drawing();
        // $objDrawing->setName('Ofiskita');
        // $objDrawing->setDescription('Ofiskita');
        // $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
        // $objDrawing->setCoordinates('A1'); 
        // $objDrawing->setWidthAndHeight(200,63);
        // $objDrawing->setWorksheet($sheet);
        // Redirect output to a client�s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $language->get('heading_title') . " " . '' . '".xlsx');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

}
