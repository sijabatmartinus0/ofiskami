<?php
class ModelLocalisationAccount extends Model {
	public function getAccounts() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "account WHERE status = '1'");

		return $query->rows;
	}

	public function getAccountById($account_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "account WHERE account_id = '" . (int)$account_id . "' AND status = '1'");

		return $query->row;
	}
}