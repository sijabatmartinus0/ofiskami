<?php
class ModelLocalisationSubdistrict extends Model {
	public function getSubdistrict($subdistrict_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subdistrict WHERE subdistrict_id = '" . (int)$subdistrict_id . "' AND status = '1'");

		return $query->row;
	}

	public function getSubdistrictsByDistrictId($district_id) {
		$subdistrict_data = $this->cache->get('subdistrict.' . (int)$district_id);

		if (!$subdistrict_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subdistrict WHERE district_id = '" . (int)$district_id . "' AND status = '1' ORDER BY name");

			$subdistrict_data = $query->rows;

			$this->cache->set('subdistrict.' . (int)$district_id, $subdistrict_data);
		}

		return $subdistrict_data;
	}
	
	public function getLocation($subdistrict_id) {
		$query = $this->db->query("
		SELECT
			c.country_id,
			z.zone_id,
			c1.city_id,
			d.district_id,
			s.subdistrict_id,
			s.code as postcode
		FROM 
			" . DB_PREFIX . "country c,
			" . DB_PREFIX . "zone z,
			" . DB_PREFIX . "city c1,
			" . DB_PREFIX . "district d,
			" . DB_PREFIX . "subdistrict s
		WHERE 
			1=1
			AND s.district_id=d.district_id
			AND d.city_id=c1.city_id
			AND c1.zone_id=z.zone_id
			AND z.country_id=c.country_id
			AND s.subdistrict_id='".(int)$subdistrict_id."' 
			AND s.status = '1'
		");

		return $query->row;
	}
}