<?php
class ModelLocalisationPpBranch extends Model {
	public function getPpBranch($pp_branch_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pp_branch WHERE pp_branch_id = '" . (int)$pp_branch_id . "' AND status = '1'");

		return $query->row;
	}

	public function getPpBranchs() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pp_branch WHERE status = '1'");

		return $query->rows;
	}
}