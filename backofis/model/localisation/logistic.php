<?php
class ModelLocalisationLogistic extends Model {
	public function getLogistic() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping WHERE status = '1'");

		return $query->rows;
	}

	public function getLogisticById($logistic_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping WHERE shipping_id = '" . (int)$logistic_id . "' AND status = '1'");

		return $query->row;
	}
}