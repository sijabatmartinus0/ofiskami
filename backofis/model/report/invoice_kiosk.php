<?php
class ModelReportInvoiceKiosk extends Model {
	public function getInvoices($data = array()) {
		$sql = "SELECT osd.sale_id ,sr.batch_id, sr.kiosk_name, sr.invoice, sr.kiosk_id
					FROM 
						oc_summary_report sr, 
						ospos_sales_detail osd
					WHERE 
						sr.invoice = osd.invoice_number";
		
		if (isset($data['filter_batch']) && $data['filter_batch']!='') {
			$sql .=" AND sr.batch_id = '".$this->db->escape($data['filter_batch'])."' ";
		}
		
		if (isset($data['filter_kiosk']) && $data['filter_kiosk']!='') {
			$sql .=" AND sr.kiosk_id = '".(int)$this->db->escape($data['filter_kiosk'])."' ";
		}
		
		$sql .=" GROUP BY sr.invoice, sr.kiosk_id";	
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSale($data = array()) {
		$sql = "SELECT * FROM ospos_sales WHERE 1=1 ";
		
		if (isset($data['sale_id']) && $data['sale_id']!='') {
			$sql .=" AND sale_id = '".(int)$this->db->escape($data['sale_id'])."' ";
		}
		
		if (isset($data['kiosk_id']) && $data['kiosk_id']!='') {
			$sql .=" AND kiosk_id = '".(int)$this->db->escape($data['kiosk_id'])."' ";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSaleDetail($data = array()){
		$sql = "SELECT * FROM ospos_sales_detail WHERE 1=1 ";
		
		if (isset($data['sale_id']) && $data['sale_id']!='') {
			$sql .=" AND sale_id = '".(int)$this->db->escape($data['sale_id'])."' ";
		}
		
		if (isset($data['kiosk_id']) && $data['kiosk_id']!='') {
			$sql .=" AND kiosk_id = '".(int)$this->db->escape($data['kiosk_id'])."' ";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSaleItems($data = array()){
		$sql = "SELECT * FROM ospos_sales_items LEFT JOIN ospos_sales_detail ON ospos_sales_items.sale_detail_id=ospos_sales_detail.sale_detail_id WHERE 1=1 ";
		
		if (isset($data['sale_id']) && $data['sale_id']!='') {
			$sql .=" AND ospos_sales_detail.sale_id = '".(int)$this->db->escape($data['sale_id'])."' ";
		}
		
		if (isset($data['sale_detail_id']) && $data['sale_detail_id']!='') {
			$sql .=" AND ospos_sales_detail.sale_detail_id = '".(int)$this->db->escape($data['sale_detail_id'])."' ";
		}
		
		if (isset($data['kiosk_id']) && $data['kiosk_id']!='') {
			$sql .=" AND ospos_sales_detail.kiosk_id = '".(int)$this->db->escape($data['kiosk_id'])."' ";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalInvoice($data = array()) {
		$sql = "SELECT osd.sale_id ,sr.batch_id, sr.kiosk_name, sr.invoice
					FROM 
						oc_summary_report sr, 
						ospos_sales_detail osd
					WHERE 
						sr.invoice = osd.invoice_number";
		
		if (isset($data['filter_batch']) && $data['filter_batch']!='') {
			$sql .=" AND sr.batch_id = '".$this->db->escape($data['filter_batch'])."' ";
		}
		
		if (isset($data['filter_kiosk']) && $data['filter_kiosk']!='') {
			$sql .=" AND sr.kiosk_id = '".(int)$this->db->escape($data['filter_kiosk'])."' ";
		}
		
		$sql .=" GROUP BY sr.invoice, sr.kiosk_id";	

		$query = $this->db->query($sql);

		return $query->num_rows;
	}
	
	public function getTotalPaidInvoice($data = array()) {
		$query = $this->db->query("SELECT tbp.batch_id,tbp.seller_id FROM " . DB_PREFIX . "transaction_batch_payout tbp INNER JOIN " . DB_PREFIX .  "transaction_batch tb ON tb.batch_id = tbp.batch_id WHERE tb.is_closed = 0 ".($data['filter_merchant']!='' || $data['filter_merchant']!=0? "AND tbp.seller_id = '".$this->db->escape($data['filter_merchant'])."'":"")." AND tbp.batch_id LIKE '%".$this->db->escape($data['filter_batch'])."%'");

		return $query->num_rows;
	}
	
	public function getTotalCreatedInvoice($data = array()) {
		$query = $this->db->query("SELECT tbp.batch_id,tbp.seller_id FROM " . DB_PREFIX . "transaction_batch_billing tbp INNER JOIN " . DB_PREFIX .  "transaction_batch tb ON tb.batch_id = tbp.batch_id WHERE tb.is_closed = 0 ".($data['filter_merchant']!='' || $data['filter_merchant']!=0? "AND tbp.seller_id = '".$this->db->escape($data['filter_merchant'])."'":"")." AND tbp.batch_id LIKE '%".$this->db->escape($data['filter_batch'])."%'");

		return $query->num_rows;
	}
	
	public function getTransaction($data = array()) {
		$sql="SELECT summary.* FROM ( ";
		if ((isset($data['filter_status']) && $data['filter_status']=='') || (int)$data['filter_status']==5) {
			$sql.="
				SELECT 
				invoice,
				nickname,
				relation_type,
				tax_type,
				kiosk_name,
				revenue_type,
				payment_type,
				account_name,
				transaction_date as date_added,
				quantity,
				sku,
				code,
				price,
				total,
				payment_total,
				tax,
				commission_base,
				shipping_fee,
				online_payment_fee,
				store_commission,
				store_commission_tax,
				'Complete' as status
				FROM " . DB_PREFIX . "summary_report WHERE 1=1 AND kiosk_id IS NOT NULL AND kiosk_name IS NOT NULL
				";
				
				if (isset($data['filter_seller']) && $data['filter_seller']!='') {
					$sql .=" AND seller_id = '".(int)$data['filter_seller']."' ";
				}
				
				if (isset($data['filter_batch']) && $data['filter_batch']!='') {
					$sql .=" AND batch_id = '".$this->db->escape($data['filter_batch'])."' ";
				}
				
				if (isset($data['filter_date_start']) && $data['filter_date_start']!='' && isset($data['filter_date_end']) && $data['filter_date_end']!='') {
					$sql .=" AND transaction_date BETWEEN '".$this->db->escape($data['filter_date_start'])."' AND '".$this->db->escape($data['filter_date_end'])."' ";
				}
				
				$sql.=" UNION ALL ";
		}
		
		$sql.="
		SELECT 
		CONCAT(od.invoice_prefix,od.invoice_no) as invoice,
		ms.nickname as nickname,
		opd.relation_type as relation_type,
		opd.tax_type as tax_type,
		sr.kiosk_name as kiosk_name,
		opd.revenue_type as revenue_type,
		o.payment_method as payment_type,
		a.name as account_name,
		od.date_added,
		op.quantity as quantity,
		p.sku as sku,
		p.code as code,
		op.price as price,
		op.total as total,
		o.total as payment_total,
		sr.tax as tax,
		sr.commission_base as commission_base,
		od.shipping_price as shipping_fee,
		sr.online_payment_fee as online_payment_fee,
		sr.store_commission as store_commission,
		sr.store_commission_tax as store_commission_tax,
		os.name as status
		FROM " . DB_PREFIX . "order_product op
		LEFT JOIN " . DB_PREFIX . "order o on op.order_id=o.order_id
		LEFT JOIN " . DB_PREFIX . "order_payment_confirmation opc on o.order_id=opc.order_id
		LEFT JOIN " . DB_PREFIX . "account a on opc.account_destination_id=a.account_id
		LEFT JOIN " . DB_PREFIX . "order_detail od on op.order_detail_id=od.order_detail_id
		LEFT JOIN " . DB_PREFIX . "ms_order_product_data opd on op.product_id=opd.product_id AND op.order_detail_id=opd.order_detail_id AND op.order_id=opd.order_id
		LEFT JOIN " . DB_PREFIX . "ms_seller ms ON od.seller_id=ms.seller_id
		LEFT JOIN " . DB_PREFIX . "order_status os on od.order_status_id=os.order_status_id AND os.language_id=1
		LEFT JOIN " . DB_PREFIX . "product p ON op.product_id=p.product_id
		LEFT JOIN " . DB_PREFIX . "summary_report sr ON op.product_id=sr.product_id AND od.order_detail_id=sr.order_detail_id AND od.order_id=sr.order_id
		WHERE 1=1 
		";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND od.seller_id = '".(int)$data['filter_seller']."' ";
		}
		
		if (isset($data['filter_status']) && $data['filter_status']!='') {
			$sql .=" AND od.order_status_id = '".(int)$data['filter_status']."' ";
		}
		
		if (isset($data['filter_batch']) && $data['filter_batch']!='') {
			$sql .=" AND sr.batch_id = '".$this->db->escape($data['filter_batch'])."' ";
		}
		
		if (isset($data['filter_date_start']) && $data['filter_date_start']!='' && isset($data['filter_date_end']) && $data['filter_date_end']!='') {
			$sql .=" AND od.date_added BETWEEN '".$this->db->escape($data['filter_date_start'])."' AND '".$this->db->escape($data['filter_date_end'])."' ";
		}
		
		$sql .=" ) as summary ORDER BY summary.date_added DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalTransaction($data = array()) {
	    $sql="SELECT sumtable.* FROM ( ";
		if ((isset($data['filter_status']) && $data['filter_status']=='') || (int)$data['filter_status']==5) {
			$sql.="
				SELECT 
				invoice,
				nickname,
				relation_type,
				tax_type,
				kiosk_name,
				revenue_type,
				payment_method,
				account_name,
				transaction_date as date_added,
				quantity,
				sku,
				code,
				price,
				total,
				payment_total,
				tax,
				commission_base,
				shipping_fee,
				online_payment_fee,
				store_commission,
				store_commission_tax,
				'Complete' as status
				FROM " . DB_PREFIX . "summary_report WHERE 1=1 AND kiosk_id IS NOT NULL AND kiosk_name IS NOT NULL
				";
				
				if (isset($data['filter_seller']) && $data['filter_seller']!='') {
					$sql .=" AND seller_id = '".(int)$data['filter_seller']."' ";
				}
				
				if (isset($data['filter_batch']) && $data['filter_batch']!='') {
					$sql .=" AND batch_id = '".$this->db->escape($data['filter_batch'])."' ";
				}
				
				if (isset($data['filter_date_start']) && $data['filter_date_start']!='' && isset($data['filter_date_end']) && $data['filter_date_end']!='') {
					$sql .=" AND transaction_date BETWEEN '".$this->db->escape($data['filter_date_start'])."' AND '".$this->db->escape($data['filter_date_end'])."' ";
				}
				
				$sql.=" UNION ALL ";
		}
		
		$sql.="
		SELECT 
		CONCAT(od.invoice_prefix,od.invoice_no) as invoice,
		ms.nickname as nickname,
		sr.relation_type as relation_type,
		sr.tax_type as tax_type,
		sr.kiosk_name as kiosk_name,
		sr.revenue_type as revenue_type,
		o.payment_method as payment_method,
		a.name as account_name,
		od.date_added,
		op.quantity as quantity,
		p.sku as sku,
		p.code as code,
		op.price as price,
		op.total as total,
		o.total as payment_total,
		sr.tax as tax,
		sr.commission_base as commission_base,
		od.shipping_price as shipping_fee,
		sr.online_payment_fee as online_payment_fee,
		sr.store_commission as store_commission,
		sr.store_commission_tax as store_commission_tax,
		os.name as status
		FROM " . DB_PREFIX . "order_product op
		LEFT JOIN " . DB_PREFIX . "order o on op.order_id=o.order_id
		LEFT JOIN " . DB_PREFIX . "order_payment_confirmation opc on o.order_id=opc.order_id
		LEFT JOIN " . DB_PREFIX . "account a on opc.account_destination_id=a.account_id
		LEFT JOIN " . DB_PREFIX . "order_detail od on op.order_detail_id=od.order_detail_id
		LEFT JOIN " . DB_PREFIX . "ms_seller ms ON od.seller_id=ms.seller_id
		LEFT JOIN " . DB_PREFIX . "order_status os on od.order_status_id=os.order_status_id AND os.language_id=1
		LEFT JOIN " . DB_PREFIX . "product p ON op.product_id=p.product_id
		LEFT JOIN " . DB_PREFIX . "summary_report sr ON op.product_id=sr.product_id AND od.order_detail_id=sr.order_detail_id AND od.order_id=sr.order_id
		WHERE 1=1 
		";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND od.seller_id = '".(int)$data['filter_seller']."' ";
		}
		
		if (isset($data['filter_status']) && $data['filter_status']!='') {
			$sql .=" AND od.order_status_id = '".(int)$data['filter_status']."' ";
		}
		
		if (isset($data['filter_batch']) && $data['filter_batch']!='') {
			$sql .=" AND sr.batch_id = '".$this->db->escape($data['filter_batch'])."' ";
		}
		
		if (isset($data['filter_date_start']) && $data['filter_date_start']!='' && isset($data['filter_date_end']) && $data['filter_date_end']!='') {
			$sql .=" AND od.date_added BETWEEN '".$this->db->escape($data['filter_date_start'])."' AND '".$this->db->escape($data['filter_date_end'])."' ";
		}
		
		$sql .=" ) as sumtable ORDER BY date_added DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->num_rows;
	}
	
	public function getIncome($data = array()) {
		$sql = "SELECT sr.batch_id, sum(sr.online_payment_fee) as online_payment_fee, tb.is_download, sum(sr.store_commission) as store_commission, sum(sr.store_commission_tax) as tax, sr.seller_id, sr.nickname FROM " . DB_PREFIX . "summary_report sr INNER JOIN " . DB_PREFIX .  "transaction_batch tb ON tb.batch_id = sr.batch_id WHERE tb.is_closed = 0 AND sr.batch_id LIKE '%".$this->db->escape($data['filter_batch'])."%' ";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND sr.seller_id = '".(int)$data['filter_seller']."' ";
		}
		$sql .=" GROUP BY sr.batch_id,sr.seller_id ";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSummaryPenalty($data = array()) {
		$sql = "SELECT l.seller_id, (SUM(l.amount)-SUM(IFNULL(ld.prepayment,0))) as penalty, s.nickname
				FROM " . DB_PREFIX . "ms_loan l 
				LEFT JOIN
						(SELECT loan_id, SUM(payment) as prepayment
							FROM " . DB_PREFIX . "ms_loan_detail
							GROUP BY loan_id) as ld	ON ld.loan_id=l.loan_id
				LEFT JOIN " . DB_PREFIX . "ms_seller s ON  l.seller_id=s.seller_id
				WHERE 1=1 AND l.is_finish=1 ";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND l.seller_id = '".(int)$data['filter_seller']."' ";
		}
		$sql .=" GROUP BY l.seller_id ORDER BY l.date_created ASC";

		$query = $this->db->query($sql);
	
		return $query->rows;
	}
	
	public function getPenalties($data = array()) {
		$sql = "SELECT 
					l.loan_id, 
					l.batch_id, 
					l.seller_id, 
					(l.amount-IFNULL(ld.prepayment,0)) as penalty, 
					l.amount, 
					IFNULL(ld.prepayment,0) as paid, 
					l.description, 
					l.date_created
					FROM " . DB_PREFIX . "ms_loan as l LEFT JOIN
						(SELECT loan_id, SUM(payment) as prepayment
							FROM " . DB_PREFIX . "ms_loan_detail
							GROUP BY loan_id) as ld	ON ld.loan_id=l.loan_id		
					WHERE 1=1 AND l.is_finish=1 ";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND l.seller_id = '".(int)$data['filter_seller']."' ";
		}
		$sql .=" ORDER BY l.date_created ASC ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);
	
		return $query->rows;
	}
	
	public function getTotalIncome($data = array()) {
		$sql = "SELECT sr.batch_id, sum(sr.online_payment_fee) as online_payment_fee, tb.is_download, sum(sr.store_commission) as store_commission, sum(sr.store_commission_tax) as tax, sr.seller_id, sr.nickname FROM " . DB_PREFIX . "summary_report sr INNER JOIN " . DB_PREFIX .  "transaction_batch tb ON tb.batch_id = sr.batch_id WHERE tb.is_closed = 0 AND sr.batch_id LIKE '%".$this->db->escape($data['filter_batch'])."%' ";
		
		if (isset($data['filter_seller']) && $data['filter_seller']!='') {
			$sql .=" AND sr.seller_id = '".(int)$data['filter_seller']."' ";
		}
		$sql .=" GROUP BY sr.batch_id,sr.seller_id ";
		
		$query = $this->db->query($sql);

		return $query->num_rows;
	}
	
	public function getBatchs(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction_batch");
		
		return $query->rows;
	}
	
	public function getReportDetail($data=array()){
		$sql ="SELECT * FROM " . DB_PREFIX .  "summary_report WHERE batch_id='".$this->db->escape($data['batch_id'])."'";
		
		if (isset($data['seller_id'])) {
			$sql .= " AND seller_id='".(int)$this->db->escape($data['seller_id'])."'";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalDetail($data = array()){
		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX .  "summary_report WHERE batch_id='".$this->db->escape($data['batch_id'])."'";

		if (isset($data['seller_id'])) {
			$sql .= " AND seller_id='".(int)$this->db->escape($data['seller_id'])."'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getShippingPrice($order_id){
		$query = $this->db->query("SELECT sum(shipping_price) as price FROM " . DB_PREFIX . "order_detail WHERE order_id='".(int)$order_id."'");
		
		return $query->row['price'];
	}
	
	public function isDownload($data=array()){
		// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction_batch_download WHERE seller_id='".(int)$this->db->escape($data['seller_id'])."' AND batch_id='".$this->db->escape($data['batch_id'])."' AND report_type='".$this->db->escape($data['report_type'])."'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction_batch_download WHERE seller_id='".(int)$this->db->escape($data['seller_id'])."' AND batch_id='".$this->db->escape($data['batch_id'])."'");
		
		if($query->num_rows){
			return true;
		}else{
			return false;
		}
	}
	
	public function reportInvoiceVendor($seller_id, $batch_id){
		//update status is_download
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "transaction_batch_download SET seller_id='".(int)$seller_id."', batch_id='".$batch_id."', report_type='invoice to vendor'");
		
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$this->load->model('report/invoice_vendor');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('report/invoice_vendor');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		if ($query->num_rows) {
			$nickname  = $query->row['nickname'];
		}else{
			$nickname = "-";
		}
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title') . ' ' . $nickname)
									 ->setSubject($language->get('heading_title'))
									 ->setDescription($language->get('heading_title'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', $language->get('heading_title'))
					->setCellValue('B3', '#'.$language->get('column_merchant') . ':')
					->setCellValue('C3', $seller_id)
					->setCellValue('B4', $language->get('column_merchant') . ':')
					->setCellValue('C4', $nickname)
					->setCellValue('B5', $language->get('column_batch') . ':')
					->setCellValue('C5', ControllerReportInvoiceVendor::BATCH_PREFIX . $seller_id . '-' . $batch_id)
					->setCellValue('B6', $language->get('text_download_date') . ':')
					->setCellValue('C6', date('d-m-Y'));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B9', $language->get('column_order_no'))
					->setCellValue('C9', $language->get('column_vendor_name'))
					->setCellValue('D9', $language->get('column_tax_type'))
					->setCellValue('E9', $language->get('column_kiosk_id'))
					->setCellValue('F9', $language->get('column_revenue_type'))
					->setCellValue('G9', $language->get('column_payment_type'))
					->setCellValue('H9', $language->get('column_bank'))
					->setCellValue('I9', $language->get('column_transaction_date'))
					->setCellValue('J9', $language->get('column_quantity'))
					->setCellValue('K9', $language->get('column_sku'))
					->setCellValue('L9', $language->get('column_code'))
					->setCellValue('M9', $language->get('column_price'))
					->setCellValue('N9', $language->get('column_total_report'))
					->setCellValue('O9', $language->get('column_tax'))
					->setCellValue('P9', $language->get('column_commission'))
					->setCellValue('Q9', $language->get('column_shipping'))
					->setCellValue('R9', $language->get('column_ofiskita_commission'))
					->setCellValue('S9', $language->get('column_tax_ppn'));
		
		$details = $this->model_report_invoice_vendor->getReportDetail(
			array(
				'seller_id'		=> $seller_id,
				'batch_id'		=> $batch_id
			)
		);
		
		$row = 10;
		
		$total_store_commission = 0;
		$total_store_commission_tax = 0;
		$total_online_payment_fee = 0;
		$temp_order_id=0;
		
		foreach($details as $detail){
			if($temp_order_id!=$detail['order_id']){
				$temp_order_id=$detail['order_id'];
				$total_online_payment_fee+=$detail['online_payment_fee'];
			}
			$total_store_commission += $detail['store_commission'];
			$total_store_commission_tax += $detail['store_commission_tax'];
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $detail['invoice'])
						->setCellValue('C'.$row, $detail['nickname'])
						->setCellValue('D'.$row, $detail['tax_type'])
						->setCellValue('E'.$row, (isset($detail['kiosk_name']) ? $detail['kiosk_name'] : '-'))
						->setCellValue('F'.$row, $detail['revenue_type'])
						->setCellValue('G'.$row, $detail['payment_type'])
						->setCellValue('H'.$row, $detail['account_name'])
						->setCellValue('I'.$row, date('d/m/Y H:i', strtotime($detail['transaction_date'])))
						->setCellValue('J'.$row, $detail['quantity'])
						->setCellValueExplicit('K'.$row, $detail['sku'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('L'.$row, $detail['code'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('M'.$row, (int)$detail['total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('N'.$row, (int)$detail['total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('O'.$row, (int)$detail['tax'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('P'.$row, (int)$detail['commission_base'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('Q'.$row, (int)$detail['shipping_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('R'.$row, (int)$detail['store_commission'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('S'.$row, "", PHPExcel_Cell_DataType::TYPE_STRING);
			$row++;
		}
		
		//total
		//$total_store_commission = $this->currency->format($total_store_commission, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		//$total_store_commission_tax = $this->currency->format($total_store_commission_tax, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B'.$row.':Q'.$row)
					->setCellValue('B'.$row, strtoupper($language->get('column_total_report')))
					->setCellValueExplicit('R'.$row, (int)$total_store_commission, PHPExcel_Cell_DataType::TYPE_NUMERIC)
					->setCellValueExplicit('S'.$row, (int)$total_store_commission_tax, PHPExcel_Cell_DataType::TYPE_NUMERIC);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B7', $language->get('text_total_commission_tax') . ':')
					->setCellValueExplicit('C7', (int)($total_store_commission+$total_store_commission_tax), PHPExcel_Cell_DataType::TYPE_NUMERIC);
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B3')->applyFromArray($boldFont);				//merchant
		$sheet->getStyle('C3')->applyFromArray($leftStyleArray);				//merchant
		$sheet->getStyle('B4')->applyFromArray($boldFont);				//batch
		$sheet->getStyle('B5')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B6')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B7')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B9:S9')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B9:S9')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B9:S9')->applyFromArray($centerStyleArray);	//thead
		$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);	//total merge
		$sheet->getStyle('B'.$row)->applyFromArray($boldFont);			//total merge
		$sheet->getStyle('R'.$row)->applyFromArray($rightStyleArray);	//total store commission
		$sheet->getStyle('S'.$row)->applyFromArray($rightStyleArray);	//total store commission tax
		
		$sheet->getStyle('B9:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('D9:'.'H'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('L10:'.'S'.$row)->applyFromArray($rightStyleArray);
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(36);
		$sheet->getColumnDimension('C')->setWidth(35);
		$sheet->getColumnDimension('D')->setWidth(24);
		$sheet->getColumnDimension('E')->setWidth(25);
		$sheet->getColumnDimension('F')->setWidth(24);
		$sheet->getColumnDimension('G')->setWidth(25);
		$sheet->getColumnDimension('H')->setWidth(25);
		$sheet->getColumnDimension('I')->setWidth(25);
		$sheet->getColumnDimension('J')->setWidth(12);
		$sheet->getColumnDimension('K')->setWidth(18);
		$sheet->getColumnDimension('L')->setWidth(21);
		$sheet->getColumnDimension('M')->setWidth(21);
		$sheet->getColumnDimension('N')->setWidth(21);
		$sheet->getColumnDimension('O')->setWidth(21);
		$sheet->getColumnDimension('P')->setWidth(21);
		$sheet->getColumnDimension('Q')->setWidth(21);
		$sheet->getColumnDimension('R')->setWidth(21);
		$sheet->getColumnDimension('S')->setWidth(24);
		
		//set column color
		$sheet->getStyle('B9:S9')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B'.$row.':O'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		$sheet->getStyle('R'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('S'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("B9:S".$row)->applyFromArray($border_style);
		
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		if ($nickname=="-") {
			$nickname  = "";
		}
		if ($batch_id=="-") {
			$batch_id  = "";
		}
		
		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.str_replace(" ","_",$language->get('heading_title')) . ($nickname!="" ? ('-' . str_replace(" ","_",$nickname)):""). ($batch_id!="" ? ('-' . str_replace(" ","_",$batch_id)):"") .'.xlsx');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function reportOfiskitaPayout($seller_id, $batch_id){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('report/invoice_vendor');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		if ($query->num_rows) {
			$nickname  = $query->row['nickname'];
		}else{
			$nickname = "-";
		}
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title2') . ' ' . $nickname)
									 ->setSubject($language->get('heading_title2'))
									 ->setDescription($language->get('heading_title2'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', $language->get('heading_title2'))
					->setCellValue('B3', '#'.$language->get('column_merchant') . ':')
					->setCellValue('C3', $seller_id)
					->setCellValue('B4', $language->get('column_merchant') . ':')
					->setCellValue('C4', $nickname)
					->setCellValue('B5', $language->get('column_batch') . ':')
					->setCellValue('C5', ControllerReportOfiskitaPayout::BATCH_PREFIX . $seller_id . '-' . $batch_id)
					->setCellValue('B6', $language->get('text_download_date') . ':')
					->setCellValue('C6', date('d-m-Y'))
					->setCellValue('B7', $language->get('text_total_payout') . ':')
					->setCellValue('B8', $language->get('text_deposit') . ':')
					->setCellValue('B10', $language->get('text_transaction'));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B11', $language->get('column_order_no'))
					->setCellValue('C11', $language->get('column_vendor_name'))
					->setCellValue('D11', $language->get('column_vendor_type'))
					->setCellValue('E11', $language->get('column_kiosk_id'))
					->setCellValue('F11', $language->get('column_revenue_type'))
					->setCellValue('G11', $language->get('column_payment_type'))
					->setCellValue('H11', $language->get('column_bank'))
					->setCellValue('I11', $language->get('column_transaction_date'))
					->setCellValue('J11', $language->get('column_quantity'))
					->setCellValue('K11', $language->get('column_sku'))
					->setCellValue('L11', $language->get('column_code'))
					->setCellValue('M11', $language->get('column_price'))
					->setCellValue('N11', $language->get('column_total_report'))
					->setCellValue('O11', $language->get('column_tax'))
					->setCellValue('P11', $language->get('column_commission'))
					->setCellValue('Q11', $language->get('column_ofiskita_commission'))
					->setCellValue('R11', $language->get('column_shipping'))
					->setCellValue('S11', $language->get('column_online_payment'))
					->setCellValue('T11', $language->get('column_vendor_balance'));
		
		$details = $this->getReportDetail(
			array(
				'seller_id'		=> $seller_id,
				'batch_id'		=> $batch_id
			)
		);
		
		$row = 12;
		
		$total_vendor_balance = 0;
		
		foreach($details as $detail){
			$total_vendor_balance += $detail['vendor_balance'];
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $detail['invoice'])
						->setCellValue('C'.$row, $detail['nickname'])
						->setCellValue('D'.$row, $detail['tax_type'])
						->setCellValue('E'.$row, (isset($detail['kiosk_name']) ? $detail['kiosk_name'] : '-'))
						->setCellValue('F'.$row, $detail['revenue_type'])
						->setCellValue('G'.$row, $detail['payment_type'])
						->setCellValue('H'.$row, $detail['account_name'])
						->setCellValue('I'.$row, date('d/m/Y H:i', strtotime($detail['transaction_date'])))
						->setCellValue('J'.$row, $detail['quantity'])
						->setCellValueExplicit('K'.$row, $detail['sku'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('L'.$row, $detail['code'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('M'.$row, (int)$detail['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('N'.$row, (int)$detail['total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('O'.$row, (int)$detail['tax'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('P'.$row, (int)$detail['commission_base'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('Q'.$row, (int)$detail['store_commission'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('R'.$row, (int)$detail['shipping_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('S'.$row, (int)$detail['online_payment_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('T'.$row, (int)$detail['vendor_balance'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$row++;
		}
		
		//total
		$total_vendor_balance = $total_vendor_balance;
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B'.$row.':S'.$row)
					->setCellValue('B'.$row, strtoupper($language->get('column_total_report')))
					->setCellValueExplicit('T'.$row, $total_vendor_balance, PHPExcel_Cell_DataType::TYPE_NUMERIC);
		
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B3')->applyFromArray($boldFont);				//merchant
		$sheet->getStyle('C3')->applyFromArray($leftStyleArray);				//merchant
		$sheet->getStyle('B4')->applyFromArray($boldFont);				//batch
		$sheet->getStyle('B5')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B6')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B7')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B8')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B10')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B11:T11')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B11:T11')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B11:T11')->applyFromArray($centerStyleArray);	//thead
		$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);	//total merge
		$sheet->getStyle('B'.$row)->applyFromArray($boldFont);			//total merge
		$sheet->getStyle('T'.$row)->applyFromArray($rightStyleArray);	//total vendor balance
		
		$sheet->getStyle('B11:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('D11:'.'H'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('J12:'.'T'.$row)->applyFromArray($rightStyleArray);
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(36);
		$sheet->getColumnDimension('C')->setWidth(35);
		$sheet->getColumnDimension('D')->setWidth(24);
		$sheet->getColumnDimension('E')->setWidth(25);
		$sheet->getColumnDimension('F')->setWidth(25);
		$sheet->getColumnDimension('G')->setWidth(24);
		$sheet->getColumnDimension('H')->setWidth(24);
		$sheet->getColumnDimension('I')->setWidth(25);
		$sheet->getColumnDimension('J')->setWidth(12);
		$sheet->getColumnDimension('K')->setWidth(18);
		$sheet->getColumnDimension('L')->setWidth(21);
		$sheet->getColumnDimension('M')->setWidth(21);
		$sheet->getColumnDimension('N')->setWidth(21);
		$sheet->getColumnDimension('O')->setWidth(21);
		$sheet->getColumnDimension('P')->setWidth(21);
		$sheet->getColumnDimension('Q')->setWidth(21);
		$sheet->getColumnDimension('R')->setWidth(21);
		$sheet->getColumnDimension('S')->setWidth(21);
		$sheet->getColumnDimension('T')->setWidth(28);
		
		//set column color
		$sheet->getStyle('B11:T11')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B'.$row.':S'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		$sheet->getStyle('T'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('T'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("B11:T".$row)->applyFromArray($border_style);
		
		//Get Payment
		$sql = "SELECT amount
				FROM " . DB_PREFIX . "ms_payment
				WHERE seller_id = " . (int)$seller_id . "
				 AND batch_id = '" . $batch_id . "' 
				 AND payment_type = '".MsPayment::TYPE_PAYOUT_REQUEST."' 
				 ";
		$res 		= $this->db->query($sql);
		$payout 	= $res->row;
		
		if(!empty($payout)){
			$totalPayout = $payout['amount'];
		}else{
			$totalPayout = 0;
		}
		
		$totalBalance 	= $this->MsLoader->MsBalance->getSellerBalance($seller_id,$batch_id) - $totalPayout;
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('C7', (int)$totalPayout, PHPExcel_Cell_DataType::TYPE_NUMERIC)
					->setCellValueExplicit('C8', (int)$totalBalance, PHPExcel_Cell_DataType::TYPE_NUMERIC);
					
		//Deduction
		$row+=2;
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$row, $language->get('text_deposit_deduction'));
					
		$sheet->getStyle('B'.$row)->applyFromArray($boldFont);	
		
		$row+=1;
		$rowDeductionStart=$row;
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$row, $language->get('column_description'))
					->setCellValue('C'.$row, $language->get('column_amount'));
		$sheet->getStyle('B'.$row.':C'.$row)->applyFromArray($centerStyleArray);		
		$sheet->getStyle('B'.$row.':C'.$row)->applyFromArray($boldFont);	
		$sheet->getStyle('B'.$row.':C'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		$deductions = $this->getDeductions(
			array(
				'seller_id'		=> $seller_id,
				'batch_id'		=> $batch_id
			)
		);
		$row+=1;
		if(!empty($deductions)){
			$totalDeduction=0;
			foreach($deductions as $deduction){
				$totalDeduction+=(int)$deduction['penalty'];
				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $deduction['description'])
						->setCellValueExplicit('C'.$row, $deduction['penalty'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$row++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$row, strtoupper($language->get('column_total_report')))
					->setCellValueExplicit('C'.$row, $totalDeduction, PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$sheet->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');	
			$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);		
			$sheet->getStyle('B'.$row)->applyFromArray($boldFont);
			$sheet->getStyle('C'.($rowDeductionStart+1).':C'.$row)->applyFromArray($rightStyleArray);
			
		}else{
			$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B'.$row.':C'.$row)
					->setCellValue('B'.$row, strtoupper($language->get('text_no_data')));
			$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);
		}
		
		$sheet->getStyle("B".$rowDeductionStart.":C".$row)->applyFromArray($border_style);
				
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title2'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		if ($nickname == "-") {
			$nickname  = "";
		}
		if ($batch_id=="-") {
			$batch_id  = "";
		}
		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.$language->get('heading_title2') .($nickname!="" ? '-' . str_replace(" ","_",$nickname):""). ($batch_id!="" ? ('-' . str_replace(" ","_",$batch_id)):"")  .'.xlsx');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function getDeductions($data=array()){
		$sql ="SELECT l.description, ld.payment as penalty FROM " . DB_PREFIX .  "ms_loan_detail ld LEFT JOIN " . DB_PREFIX .  "ms_loan l ON l.loan_id=ld.loan_id  WHERE ld.paid_batch_id='".$this->db->escape($data['batch_id'])."'";
		
		if (isset($data['seller_id'])) {
			$sql .= " AND l.seller_id='".(int)$this->db->escape($data['seller_id'])."'";
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function reportOfiskitaIncome($batch_id,$seller_id=0){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$this->load->model('report/invoice_vendor');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$seller_info = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		$language = new Language($language_directory);
		$language->load('report/invoice_vendor');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title3') . '_' . $batch_id.$seller_id)
									 ->setSubject($language->get('heading_title3'))
									 ->setDescription($language->get('heading_title3'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', $language->get('heading_title3'))
					->setCellValue('B3', $language->get('column_batch') . ':')
					->setCellValue('C3', $batch_id)
					->setCellValue('B4', $language->get('column_merchant') . ':')
					->setCellValue('C4', $seller_info['ms.nickname'])
					->setCellValue('B5', $language->get('text_download_date') . ':')
					->setCellValue('C5', date('d-m-Y'));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B7', $language->get('column_order_no'))
					->setCellValue('C7', $language->get('column_ofiskita_commission'))
					->setCellValue('D7', $language->get('column_online_payment'))
					->setCellValue('E7', $language->get('column_tax_ppn'));
		
		$details = $this->model_report_invoice_vendor->getReportDetail(
			array(
				'batch_id'		=> $batch_id,
				'seller_id'		=> $seller_id
			)
		);
		
		$row = 8;
		
		$total_store_commission = 0;
		$total_store_commission_tax = 0;
		$total_online_payment = 0;
		
		foreach($details as $detail){
			$total_store_commission += $detail['store_commission'];
			$total_store_commission_tax = $detail['store_commission_tax'];
			$total_online_payment += $detail['online_payment_fee'];
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $detail['invoice'])
						->setCellValue('C'.$row, $this->currency->format($detail['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())))
						->setCellValue('D'.$row, $this->currency->format($detail['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())))
						->setCellValue('E'.$row, "");
			$row++;
		}
		
		//total
		$total_store_commission = $this->currency->format($total_store_commission, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		$total_store_commission_tax = $this->currency->format($total_store_commission_tax, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		$total_online_payment = $this->currency->format($total_online_payment, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$row, strtoupper($language->get('column_total_report')))
					->setCellValue('C'.$row, $total_store_commission)
					->setCellValue('D'.$row, $total_online_payment)
					->setCellValue('E'.$row, $total_store_commission_tax);
		
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B3')->applyFromArray($boldFont);				//batch
		$sheet->getStyle('B4')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B5')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B7:E7')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B7:E7')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B7:E7')->applyFromArray($centerStyleArray);	//thead
		$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);	//total merge
		$sheet->getStyle('B'.$row)->applyFromArray($boldFont);			//total merge
		
		$sheet->getStyle('B7:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('B7:'.'E'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('C8:'.'E'.$row)->applyFromArray($rightStyleArray);
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(36);
		$sheet->getColumnDimension('C')->setWidth(24);
		$sheet->getColumnDimension('D')->setWidth(24);
		$sheet->getColumnDimension('E')->setWidth(24);
		
		//set column color
		$sheet->getStyle('B7:E7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		$sheet->getStyle('C'.$row.':E'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("B7:E".$row)->applyFromArray($border_style);
		
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title3'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename=".$language->get('heading_title3') . "_" . $seller_id."-".$batch_id .".xlsx");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function getSellers(){
		//$sql = "SELECT sr.seller_id, ms.nickname FROM " . DB_PREFIX . "summary_report sr INNER JOIN " . DB_PREFIX .  "transaction_batch tb ON tb.batch_id = sr.batch_id, " . DB_PREFIX .  "ms_seller ms WHERE sr.seller_id=ms.seller_id AND tb.is_closed = 0";
		//$sql .=" GROUP BY sr.batch_id,sr.seller_id ";
		$sql = "SELECT seller_id, nickname FROM " . DB_PREFIX . "ms_seller WHERE seller_status != 0 AND (seller_type=1 OR (seller_type=0 AND seller_group=1 ))";
		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function paid($batch_id,$seller_id){
		$count_batch = $this->getTotalInvoice(array('filter_merchant'=>$seller_id,'filter_batch'=>$batch_id));
		if($count_batch>0){
			$count_payout = $this->getTotalPaidInvoice(array('filter_merchant'=>$seller_id,'filter_batch'=>$batch_id));
			if($count_payout==0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "transaction_batch_payout SET seller_id = '" . (int)$seller_id . "', batch_id = '" . $this->db->escape($batch_id) . "', user_id = '" . (int)$this->user->getId() . "'");
			}
		}
		
		return true;
	}
	public function created($batch_id,$seller_id){
		$count_batch = $this->getTotalInvoice(array('filter_merchant'=>$seller_id,'filter_batch'=>$batch_id));
		if($count_batch>0){
			$count_payout = $this->getTotalCreatedInvoice(array('filter_merchant'=>$seller_id,'filter_batch'=>$batch_id));
			if($count_payout==0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "transaction_batch_billing SET seller_id = '" . (int)$seller_id . "', batch_id = '" . $this->db->escape($batch_id) . "', user_id = '" . (int)$this->user->getId() . "'");
			}
		}
		
		return true;
	}
	
	public function reportTransaction($filter_batch,$filter_seller,$filter_status,$filter_date_start,$filter_date_end){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$this->load->model('report/invoice_vendor');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('report/invoice_vendor');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title4') . '_' . $filter_batch.$filter_seller)
									 ->setSubject($language->get('heading_title4'))
									 ->setDescription($language->get('heading_title4'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', $language->get('heading_title4'));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B4:B5')
					->setCellValue('B4', $language->get('column_order_no'))
					->mergeCells('C4:C5')
					->setCellValue('C4', $language->get('column_vendor_name'))
					->mergeCells('D4:D5')
					->setCellValue('D4', $language->get('column_relation_type'))
					->mergeCells('E4:E5')
					->setCellValue('E4', $language->get('column_tax_type'))
					->mergeCells('F4:F5')
					->setCellValue('F4', $language->get('column_kiosk_id'))
					->mergeCells('G4:G5')
					->setCellValue('G4', $language->get('column_revenue_type'))
					->mergeCells('H4:H5')
					->setCellValue('H4', $language->get('column_payment_type'))
					->mergeCells('I4:I5')
					->setCellValue('I4', $language->get('column_bank'))
					->mergeCells('J4:J5')
					->setCellValue('J4', $language->get('column_transaction_date'))
					->mergeCells('K4:K5')
					->setCellValue('K4', $language->get('column_quantity'))
					->mergeCells('L4:L5')
					->setCellValue('L4', $language->get('column_sku'))
					->mergeCells('M4:M5')
					->setCellValue('M4', $language->get('column_price'))
					->mergeCells('N4:N5')
					->setCellValue('N4', $language->get('column_total_report'))
					->mergeCells('O4:O5')
					->setCellValue('O4', $language->get('column_payment_total'))
					->mergeCells('P4:R4')
					->setCellValue('P4', $language->get('column_detail'))
					->setCellValue('P5', $language->get('column_tax'))
					->setCellValue('Q5', $language->get('column_commission'))
					->setCellValue('R5', $language->get('column_shipping'))
					->mergeCells('S4:T4')
					->setCellValue('S4', $language->get('column_revenue'))
					->setCellValue('S5', $language->get('column_ofiskita_commission'))
					->setCellValue('T5', $language->get('column_tax_ppn'))
					->mergeCells('U4:U5')
					->setCellValue('U4', $language->get('text_status'));
		
		$details = $this->getTransaction(
			array(
				'filter_batch'	     => $filter_batch,
				'filter_seller'	     => $filter_seller,
				'filter_status'	     => $filter_status,
				'filter_date_start'	     => $filter_date_start,
				'filter_date_end'	     => $filter_date_end
			)
		);
		
		$row = 6;
		
		foreach($details as $detail){
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $detail['invoice'])
						->setCellValue('C'.$row, $detail['nickname'])
						->setCellValue('D'.$row, $detail['relation_type'])
						->setCellValue('E'.$row, $detail['tax_type'])
						->setCellValue('F'.$row, (isset($detail['kiosk_name']) ? $detail['kiosk_name'] : '-'))
						->setCellValue('G'.$row, $detail['revenue_type'])
						->setCellValue('H'.$row, $detail['payment_type'])
						->setCellValue('I'.$row, $detail['account_name'])
						->setCellValue('J'.$row, date('d/m/Y H:i', strtotime($detail['date_added'])))
						->setCellValue('K'.$row, $detail['quantity'])
						->setCellValueExplicit('L'.$row, $detail['sku'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('M'.$row, (int)$detail['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('N'.$row, (int)$detail['total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('O'.$row, (int)$detail['payment_total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('P'.$row, (int)$detail['tax'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('Q'.$row, (int)$detail['commission_base'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('R'.$row, (int)$detail['shipping_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('S'.$row, (int)$detail['store_commission'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('T'.$row, (int)$detail['store_commission_tax'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValue('U'.$row, $detail['status']);
			$row++;
		}
		$row--;
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B4:U4')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B5:U5')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B4:U4')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B4:U4')->applyFromArray($centerStyleArray);	//thead
		$sheet->getStyle('B5:U5')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B5:U5')->applyFromArray($centerStyleArray);	//thead
		
		$sheet->getStyle('B6:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('B6:'.'U'.$row)->applyFromArray($centerStyleArray);
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(36);
		$sheet->getColumnDimension('C')->setWidth(35);
		$sheet->getColumnDimension('D')->setWidth(24);
		$sheet->getColumnDimension('E')->setWidth(25);
		$sheet->getColumnDimension('F')->setWidth(25);
		$sheet->getColumnDimension('G')->setWidth(24);
		$sheet->getColumnDimension('H')->setWidth(24);
		$sheet->getColumnDimension('I')->setWidth(25);
		$sheet->getColumnDimension('J')->setWidth(25);
		$sheet->getColumnDimension('K')->setWidth(12);
		$sheet->getColumnDimension('L')->setWidth(21);
		$sheet->getColumnDimension('M')->setWidth(21);
		$sheet->getColumnDimension('N')->setWidth(21);
		$sheet->getColumnDimension('O')->setWidth(21);
		$sheet->getColumnDimension('P')->setWidth(21);
		$sheet->getColumnDimension('Q')->setWidth(21);
		$sheet->getColumnDimension('R')->setWidth(21);
		$sheet->getColumnDimension('S')->setWidth(21);
		$sheet->getColumnDimension('T')->setWidth(32);
		$sheet->getColumnDimension('U')->setWidth(25);
		
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B5:U5')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B5:U5')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B5:U5')->applyFromArray($centerStyleArray);	//thead
		
		$sheet->getStyle('B6:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('D6:'.'H'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('K6:'.'T'.$row)->applyFromArray($rightStyleArray);
		$sheet->getStyle('U6:'.'U'.$row)->applyFromArray($centerStyleArray);
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle('B4:U5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle("B4:U".$row)->applyFromArray($border_style);
		
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title4'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename=".$language->get('heading_title4') .".xlsx");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function reportPenalty($seller_id=0){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$this->load->model('report/invoice_vendor');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$seller_info = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		$language = new Language($language_directory);
		$language->load('report/invoice_vendor');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title5') . '_' . $seller_id)
									 ->setSubject($language->get('heading_title5'))
									 ->setDescription($language->get('heading_title5'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		$nickname = $seller_info['ms.nickname'];
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', $language->get('heading_title5'))
					->setCellValue('B3', $language->get('column_merchant') . ':')
					->setCellValue('C3', $seller_info['ms.nickname'])
					->setCellValue('B4', $language->get('text_download_date') . ':')
					->setCellValue('C4', date('d-m-Y'));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B6', $language->get('column_batch'))
					->setCellValue('C6', $language->get('column_description'))
					->setCellValue('D6', $language->get('column_date_added'))
					->setCellValue('E6', $language->get('column_penalty'));
		
		$details = $this->model_report_invoice_vendor->getPenalties(
			array(
				'filter_seller'		=> $seller_id
			)
		);
		
		$row = 7;
		
		$total_penalty = 0;
		
		foreach($details as $detail){
			$total_penalty += $detail['penalty'];
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$row, $detail['batch_id'])
						->setCellValue('C'.$row, $detail['description'])
						->setCellValue('D'.$row, date('d/m/Y H:i', strtotime($detail['date_created'])))
						->setCellValue('E'.$row, $this->currency->format($detail['penalty'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())));
			$row++;
		}
		
		//total
		$total_penalty = $this->currency->format($total_penalty, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()));
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B'.$row.':D'.$row)
					->setCellValue('B'.$row, strtoupper($language->get('column_total_report')))
					->setCellValue('E'.$row, $total_penalty);
		
		//text style
		$sheet->getStyle('B2')->applyFromArray($titleFont);				//title
		$sheet->getStyle('B3')->applyFromArray($boldFont);				//batch
		$sheet->getStyle('B4')->applyFromArray($boldFont);				//download date
		$sheet->getStyle('B6:E6')->applyFromArray($titleStyleArray);	//thead
		$sheet->getStyle('B6:E6')->applyFromArray($boldFont);			//thead
		$sheet->getStyle('B6:E6')->applyFromArray($centerStyleArray);	//thead
		$sheet->getStyle('B'.$row)->applyFromArray($centerStyleArray);	//total merge
		$sheet->getStyle('B'.$row)->applyFromArray($boldFont);			//total merge
		
		$sheet->getStyle('B6:'.'B'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('B6:'.'E'.$row)->applyFromArray($centerStyleArray);
		$sheet->getStyle('E7:'.'E'.$row)->applyFromArray($rightStyleArray);
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(25);
		$sheet->getColumnDimension('C')->setWidth(36);
		$sheet->getColumnDimension('D')->setWidth(24);
		$sheet->getColumnDimension('E')->setWidth(24);
		
		//set column color
		$sheet->getStyle('B6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		$sheet->getStyle('C'.$row.':E'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("B6:E".$row)->applyFromArray($border_style);
		
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title5'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.$language->get('heading_title5') .($nickname!="" ? '-' . str_replace(" ","_",$nickname):"").'.xlsx');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
}
?>
