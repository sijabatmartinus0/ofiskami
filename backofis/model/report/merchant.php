<?php
class ModelReportMerchant extends Model {
	public function getMerchantList($data = array()) {
		if($data['filter_period'] == 1){
			$sql = "SELECT ms.nickname, ms.seller_id, (SELECT count(*) FROM " . DB_PREFIX . "order_detail where seller_id = ms.seller_id AND MONTH(date_added) BETWEEN 1 AND 6 AND YEAR(date_added) = '".$this->db->escape($data['filter_year'])."') as total_order FROM " . DB_PREFIX . "ms_seller ms WHERE ms.nickname LIKE '%".$this->db->escape($data['filter_merchant'])."%' ORDER BY nickname ASC";
		}else if($data['filter_period'] == 2){
			$sql = "SELECT ms.nickname, ms.seller_id, (SELECT count(*) FROM " . DB_PREFIX . "order_detail where seller_id = ms.seller_id AND MONTH(date_added) BETWEEN 7 AND 12 AND YEAR(date_added) = '".$this->db->escape($data['filter_year'])."') as total_order FROM " . DB_PREFIX . "ms_seller ms WHERE ms.nickname LIKE '%".$this->db->escape($data['filter_merchant'])."%' ORDER BY nickname ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getTotalSkuMerchant($data = array()){
        $sql = "SELECT a.nickname, a.seller_id, COALESCE(total, 0) as total  FROM " . DB_PREFIX . "ms_seller a left join (select c.status, seller_id,count(*)as total from oc_ms_product b, oc_product c where b.product_id=c.product_id and c.status='1' GROUP by seller_id) as bb on a.seller_id=bb.seller_id where a.seller_status='1'";
    
        if (!empty($data['filter_merchant'])) {
            $sql .= " AND a.seller_id = '" . (int) $data['filter_merchant'] . "'";
        } else {
            $sql .= " AND a.seller_id > '0'";
        }
        $sql .= " GROUP BY a.seller_id ORDER BY a.nickname ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getSkuMerchant($data = array()) {
        //$sql = "SELECT ms.nickname, ms.seller_id, ms.date_created, (SELECT COUNT(*) FROM " . DB_PREFIX . "ms_product WHERE seller_id = ms.seller_id) as total FROM " . DB_PREFIX . "ms_seller ms";
        //$sql = "select c.nickname , b.seller_id, count(*) as total from  " . DB_PREFIX . "product a JOIN " . DB_PREFIX . "ms_product b ON a.product_id = b.product_id LEFT JOIN " . DB_PREFIX . "ms_seller c ON b.seller_id = c.seller_id WHERE a.status = '1' AND c.seller_status = '1'";
        $sql1 = "select c.status, seller_id,count(*)as total from oc_ms_product b, oc_product c where b.product_id=c.product_id and c.status='1'";
        $sql2 = "select c.status, seller_id,count(*)as total1 from oc_ms_product b, oc_product c where b.product_id=c.product_id and c.status='1' GROUP by seller_id";
        if (!empty($data['filter_date_start'])) {
            $sql1 .= " AND DATE(c.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql1 .= " AND DATE(c.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }
        $sql1 .= " GROUP by b.seller_id";
        $sql = "SELECT a.nickname, a.seller_id, COALESCE(total1, 0) as total1, COALESCE(total, 0) as total  FROM " . DB_PREFIX . "ms_seller a left join (".$sql2.") as bcb on a.seller_id=bcb.seller_id left join (".$sql1.") as bb on a.seller_id=bb.seller_id where a.seller_status='1'";
        if (!empty($data['filter_merchant'])) {
            $sql .= " AND a.seller_id = '" . (int) $data['filter_merchant'] . "'";
        } else {
            $sql .= " AND a.seller_id > '0'";
        }
        
        
        $sql .= " GROUP BY a.seller_id ORDER BY a.nickname ASC";
//        echo $sql;
//        die();
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
//        echo $sql;
//        die();
        return $query->rows;
    }

    public function getTotalMerchantFilter($data = array()) {
        //$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "ms_seller ms WHERE ms.nickname LIKE '%" . $this->db->escape($data['filter_merchant']) . "%' ORDER BY nickname ASC");
        $query = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "ms_seller ms WHERE ms.seller_status = '1'";
        if (!empty($data['filter_merchant'])) {
            $sql .= " WHERE ms.seller_id = '" . (int) $data['filter_merchant'] . "'";
        } else {
            $sql .= " AND ms.seller_id > '0'";
        }

        $query .= " ORDER BY ms.nickname ASC";
        
        $query = $this->db->query($query);
        
        return $query->row['total'];
    }	
	public function getTotalMerchant($data = array()) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "ms_seller ms WHERE ms.nickname LIKE '%".$this->db->escape($data['filter_merchant'])."%' ORDER BY nickname ASC");

		return $query->row['total'];
	}
	
	public function getTotalOrder($seller_id, $period, $year){
		if($period == 1){
			$query = $this->db->query("SELECT COUNT(*) as total_order FROM " . DB_PREFIX . "order_detail WHERE seller_id = '".(int)$seller_id."' AND MONTH(date_added) BETWEEN 1 AND 6 AND YEAR(date_added) = '".$year."'");
		}else if($period == 2){
			$query = $this->db->query("SELECT COUNT(*) as total_order FROM " . DB_PREFIX . "order_detail WHERE seller_id = '".(int)$seller_id."' AND MONTH(date_added) BETWEEN 7 AND 12 AND YEAR(date_added) = '".$year."'");
		}
		
		return $query->row['total_order'];
	}
	
	public function getScoreApproveMerchant($seller_id, $period, $year){
		if($period == 1){
			$query_reject = $this->db->query("SELECT COUNT(*) as total_reject FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'Merchant' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 18 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 18 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_reject = $this->db->query("SELECT COUNT(*) as total_reject FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'Merchant' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 18 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 18 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$total_reject = $query_reject->row['total_reject'];
		$total_approve = $query_approve->row['total_approve'];
		
		if($order == 0){
			return 0;
		}else if($total_reject == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = ($total_approve /($total_approve + $total_reject))*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function getScoreApproveSla($seller_id, $period, $year){
		if($period == 1){
			$query_reject = $this->db->query("SELECT COUNT(*) as total_reject FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'System' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 18 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 18 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_reject = $this->db->query("SELECT COUNT(*) as total_reject FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'System' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 18 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 18 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$total_reject = $query_reject->row['total_reject'];
		$total_approve = $query_approve->row['total_approve'];
		
		if($order == 0){
			return 0;
		}else if($total_reject == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = ($total_approve /($total_approve + $total_reject))*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function getScoreDeliveredSla($seller_id, $period, $year){
		if($period == 1){
			$query_undelivered = $this->db->query("SELECT COUNT(*) as total_undelivered FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'System' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 19 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 19 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_undelivered = $this->db->query("SELECT COUNT(*) as total_undelivered FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE od.seller_id ='".(int)$seller_id."' AND ohd.order_status_id = 23 AND ohd.identity = 'System' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."' AND (SELECT COUNT(*) FROM " . DB_PREFIX . "order_history_detail ohd2 INNER JOIN " . DB_PREFIX . "order_detail od2 ON od2.order_detail_id = ohd2.order_detail_id WHERE ohd2.order_status_id = 19 AND od2.seller_id='".(int)$seller_id."' AND ohd2.order_detail_id = ohd.order_detail_id) = 0 ");
			
			$query_approve = $this->db->query("SELECT COUNT(*) as total_approve FROM " . DB_PREFIX . "order_history_detail ohd INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = ohd.order_detail_id WHERE ohd.order_status_id = 19 and od.seller_id = '".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$total_undelivered = $query_undelivered->row['total_undelivered'];
		$total_approve = $query_approve->row['total_approve'];
		
		if($order == 0){
			return 0;
		}else if($total_undelivered == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = ($total_approve /($total_approve + $total_undelivered))*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function getScoreReview($seller_id, $period, $year){
		if($period == 1){
			$query_rating = $this->db->query("SELECT SUM(r.rating) as rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$query_total_rating = $this->db->query("SELECT (COUNT(r.rating) * 5) as total_rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_rating = $this->db->query("SELECT SUM(r.rating) as rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$query_total_rating = $this->db->query("SELECT (COUNT(r.rating) * 5) as total_rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$rating = $query_rating->row['rating'];
		$total_rating = $query_total_rating->row['total_rating'];
		
		if($order == 0){
			return 0;
		}else if($total_rating == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = ($rating/$total_rating)*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function getScoreReviewAccuracy($seller_id, $period, $year){
		if($period == 1){
			$query_rating = $this->db->query("SELECT SUM(r.rating_accuracy) as rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$query_total_rating = $this->db->query("SELECT (COUNT(r.rating_accuracy) * 5) as total_rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_rating = $this->db->query("SELECT SUM(r.rating_accuracy) as rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$query_total_rating = $this->db->query("SELECT (COUNT(r.rating_accuracy) * 5) as total_rating FROM " . DB_PREFIX . "review r INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = r.product_id INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id AND mp.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$rating = $query_rating->row['rating'];
		$total_rating = $query_total_rating->row['total_rating'];
		
		if($order == 0){
			return 0;
		}else if($total_rating == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = ($rating/$total_rating)*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function getScoreReturn($seller_id, $period, $year){
		if($period == 1){
			$query_return = $this->db->query("SELECT COUNT(*) as total_return FROM " . DB_PREFIX . "return r INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id WHERE r.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 1 AND 6 AND YEAR(od.date_added) = '".$year."'");
			
			$query_complete = $this->db->query("SELECT COUNT(*) as total_order_complete FROM " . DB_PREFIX . "order_detail WHERE seller_id ='".(int)$seller_id."' AND MONTH(date_added) BETWEEN 1 AND 6 AND YEAR(date_added) = '".$year."' AND order_status_id = 5");
			
			$order = (int)$this->getTotalOrder($seller_id, 1, $year);
		}else if($period == 2){
			$query_return = $this->db->query("SELECT COUNT(*) as total_return FROM " . DB_PREFIX . "return r INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id = r.order_detail_id WHERE r.seller_id='".(int)$seller_id."' AND MONTH(od.date_added) BETWEEN 7 AND 12 AND YEAR(od.date_added) = '".$year."'");
			
			$query_complete = $this->db->query("SELECT COUNT(*) as total_order_complete FROM " . DB_PREFIX . "order_detail WHERE seller_id ='".(int)$seller_id."' AND MONTH(date_added) BETWEEN 1 AND 6 AND YEAR(date_added) = '".$year."' AND order_status_id = 5");
			
			$order = (int)$this->getTotalOrder($seller_id, 2, $year);
		}
		
		$return = $query_return->row['total_return'];
		$total_order_complete = $query_complete->row['total_order_complete'];
		
		if($order == 0){
			return 0;
		}else if($total_order_complete == 0){
			return sprintf('%0.2f', 5);
		}else{	
			$score = (($total_order_complete-$return)/$total_order_complete)*5;
			return sprintf('%0.2f', $score);
		}
	}
	
	public function finalScore($seller_id, $period, $year){
		$approve_merchant = ($this->config->get('config_report_weight_approve_merchant') * $this->getScoreApproveMerchant($seller_id, $period, $year)) / 100;
		$approve_sla = ($this->config->get('config_report_weight_approve_sla') * $this->getScoreApproveSla($seller_id, $period, $year)) / 100;
		$delivered_sla = ($this->config->get('config_report_weight_delivered_sla') * $this->getScoreDeliveredSla($seller_id, $period, $year)) / 100;
		$review = ($this->config->get('config_report_weight_review') * $this->getScoreReview($seller_id, $period, $year)) / 100;
		$review_accuracy = ($this->config->get('config_report_weight_review_accuracy') * $this->getScoreReviewAccuracy($seller_id, $period, $year)) / 100;
		$complain = ($this->config->get('config_report_weight_complain') * $this->getScoreReturn($seller_id, $period, $year)) / 100;
		
		$final_score = $approve_merchant + $approve_sla + $delivered_sla + $review + $review_accuracy + $complain;
		
		return sprintf ('%0.2f', $final_score);
	}
	
	public function reportMerchantEvaluation($seller_id, $period, $year){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('report/merchant');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $this->db->escape($seller_id) . "'");

		if ($query->num_rows) {
			$nickname  = $query->row['nickname'];
		}else{
			$nickname = "-";
		}
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Admin")
									 ->setLastModifiedBy("Admin")
									 ->setTitle($language->get('heading_title') . ' ' . $nickname)
									 ->setSubject($language->get('heading_title'))
									 ->setDescription($language->get('heading_title'))
									 ->setKeywords("office 2007 openxml php");
									 
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B2:E2')
					->setCellValue('B2', $language->get('heading_title'))
					->setCellValue('D4', $language->get('column_merchant') . ':')
					->setCellValue('E4', $nickname)
					->setCellValue('B6', $language->get('column_criteria'))
					->setCellValue('C6', $language->get('column_weight_xls'))
					->setCellValue('D6', $language->get('column_score_view_xls'))
					->setCellValue('E6', $language->get('column_final_score'));
		
		$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B7', $language->get('criteria_1'))
						->setCellValue('B8', $language->get('criteria_2'))
						->setCellValue('B9', $language->get('criteria_3'))
						->setCellValue('B10', $language->get('criteria_4'))
						->setCellValue('B11', $language->get('criteria_5'))
						->setCellValue('B12', $language->get('criteria_6'))
						->setCellValue('C7', $this->config->get('config_report_weight_approve_merchant') . '%')
						->setCellValue('C8', $this->config->get('config_report_weight_approve_sla') . '%')
						->setCellValue('C9', $this->config->get('config_report_weight_delivered_sla') . '%')
						->setCellValue('C10', $this->config->get('config_report_weight_review') . '%')
						->setCellValue('C11', $this->config->get('config_report_weight_review_accuracy') . '%')
						->setCellValue('C12', $this->config->get('config_report_weight_complain') . '%');
		
		$score_approve_merchant = $this->model_report_merchant->getScoreApproveMerchant($seller_id, $period, $year);
		$score_approve_sla =  $this->model_report_merchant->getScoreApproveSla($seller_id, $period, $year);
		$score_delivered_sla = $this->model_report_merchant->getScoreDeliveredSla($seller_id, $period, $year);
		$score_review = $this->model_report_merchant->getScoreReview($seller_id, $period, $year);
		$score_review_accuracy = $this->model_report_merchant->getScoreReviewAccuracy($seller_id, $period, $year);
		$score_complain = $this->model_report_merchant->getScoreReturn($seller_id, $period, $year);
		$final_score = $this->model_report_merchant->finalScore($seller_id, $period, $year);
		
		$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('D7', $score_approve_merchant)
						->setCellValue('D8', $score_approve_sla)
						->setCellValue('D9', $score_delivered_sla)
						->setCellValue('D10', $score_review)
						->setCellValue('D11', $score_review_accuracy)
						->setCellValue('D12', $score_complain)
						->setCellValue('E7', ($this->config->get('config_report_weight_approve_merchant') * $score_approve_merchant)/100)
						->setCellValue('E8',  sprintf("%0.2f", ($this->config->get('config_report_weight_approve_sla') * $score_approve_sla) / 100))
						->setCellValue('E9',  sprintf("%0.2f", ($this->config->get('config_report_weight_delivered_sla') * $score_delivered_sla) / 100))
						->setCellValue('E10',  sprintf("%0.2f", ($this->config->get('config_report_weight_review') * $score_review) / 100)) 
						->setCellValue('E11',  sprintf("%0.2f", ($this->config->get('config_report_weight_review_accuracy') * $score_review_accuracy) / 100))
						->setCellValue('E12',  sprintf("%0.2f", ($this->config->get('config_report_weight_complain') * $score_complain) / 100))
						->setCellValue('E13',  $final_score)
						->setCellValue('D13','=SUM(D7:D12)');
		
		if($period == 1){
			$month = "January - June";
		}else if ($period == 2){
			$month = "July - December";
		}
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B13', $language->get('column_total_score'))
					->setCellValue('C13', $language->get('column_hundred'))
					->setCellValue('B15', $language->get('column_total'))
					->setCellValue('B16',  $language->get('text_period'))
					->mergeCells('C15:E15')
					->mergeCells('C16:E16')
					->setCellValue('C15',  $this->getTotalOrder($seller_id, $period, $year))
					->setCellValue('C16',  $month . ' ' . $year);
		
		$sheet->getStyle('B2')->applyFromArray($titleFont);
		$sheet->getStyle('B2')->applyFromArray($centerStyleArray);
		$sheet->getStyle('D4')->applyFromArray($rightStyleArray);
		$sheet->getStyle('E4')->applyFromArray($boldFont);
		$sheet->getStyle('B6:E6')->applyFromArray($titleStyleArray);
		$sheet->getStyle('B6:E6')->applyFromArray($boldFont);
		$sheet->getStyle('C7:E13')->applyFromArray($centerStyleArray);
		$sheet->getStyle('B13')->applyFromArray($boldFont);		
		$sheet->getStyle('B13')->applyFromArray($centerStyleArray);		
		$sheet->getStyle('E4')->applyFromArray($centerStyleArray);		
		$sheet->getStyle('B15:E16')->applyFromArray($centerStyleArray);		
		$sheet->getStyle('B15:B16')->applyFromArray($boldFont);		
		
		//set column width
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->getColumnDimension('C')->setWidth(18);
		$sheet->getColumnDimension('D')->setWidth(18);
		$sheet->getColumnDimension('E')->setWidth(18);
		
		//set column color
		$sheet->getStyle('B6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B13:E13')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');$sheet->getStyle('C15:C16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('B15:B16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("B6:E13")->applyFromArray($border_style);
		$sheet->getStyle("B15:E16")->applyFromArray($border_style);
		
		// Rename worksheet
		$sheet->setTitle($language->get('heading_title'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		// $objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Ofiskita');
		// $objDrawing->setDescription('Ofiskita');
		// $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
		// $objDrawing->setCoordinates('A1'); 
		// $objDrawing->setWidthAndHeight(200,63);
		// $objDrawing->setWorksheet($sheet);

		// Redirect output to a client�s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$language->get('heading_title') . " " . $nickname .'".xlsx');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	public function reportSkuMerchant($results) {
        //        echo '<pre>';
        //        var_dump($results);
        //        die();
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

        if ($language_info) {
            $language_directory = $language_info['directory'];
        } else {
            $language_directory = '';
        }

        $language = new Language($language_directory);
        $language->load('report/merchant');


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Admin")
                ->setLastModifiedBy("Admin")
                ->setTitle($language->get('heading_title') . ' ' . $nickname)
                ->setSubject($language->get('heading_title'))
                ->setDescription($language->get('heading_title'))
                ->setKeywords("office 2007 openxml php");

        //header style
        $headerStyleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '4C82C5',
                )
            ),
        );

        //title style
        $titleStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFFFFF',
                )
            ),
        );

        $centerStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $leftStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $rightStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //bold style
        $titleFont = array(
            'font' => array(
                'bold' => true,
                'size' => 14
            )
        );
        $boldFont = array(
            'font' => array(
                'bold' => true
            )
        );

        // Column
        $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells('B2:D2')
                ->setCellValue('B2', $language->get('heading_title_1'))
                ->setCellValue('B6', $language->get('column_merchant'))
                ->setCellValue('C6', $language->get('column_penambahan_sku'))
                ->setCellValue('D6', $language->get('column_total_sku'));

        //row
        $row = 7;
        foreach ($results as $result) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . $row, $result['nickname'])
                    ->setCellValue('C' . $row, $result['total'])
                    ->setCellValue('D' . $row, $result['total1']);
            $row++;
        }

        
        $sheet->getStyle('B2')->applyFromArray($titleFont);
        $sheet->getStyle('B2')->applyFromArray($centerStyleArray);
        $sheet->getStyle('B6:D6')->applyFromArray($titleStyleArray);
        $sheet->getStyle('B6:D6')->applyFromArray($boldFont);
        $sheet->getStyle('B')->applyFromArray($centerStyleArray);
        $sheet->getStyle('C')->applyFromArray($centerStyleArray);
        $sheet->getStyle('D')->applyFromArray($centerStyleArray);
        
        $sheet->getColumnDimension('B')->setWidth(35);
        $sheet->getColumnDimension('C')->setWidth(18);
        $sheet->getColumnDimension('D')->setWidth(18);

        //set column color
        $sheet->getStyle('B6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B13:E13')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //        $sheet->getStyle('C15:C16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B15:B16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //set border line
        $border_style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'A3A3A3'),
                ),
            ),
        );

        $border_thick = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '8C8C8C'),
                ),
            ),
        );

        //        $sheet->getStyle("B6:E13")->applyFromArray($border_style);
        //        $sheet->getStyle("B15:E16")->applyFromArray($border_style);
        // Rename worksheet
        $sheet->setTitle($language->get('heading_title_1'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Set logo header
        // $objDrawing = new PHPExcel_Worksheet_Drawing();
        // $objDrawing->setName('Ofiskita');
        // $objDrawing->setDescription('Ofiskita');
        // $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
        // $objDrawing->setCoordinates('A1'); 
        // $objDrawing->setWidthAndHeight(200,63);
        // $objDrawing->setWorksheet($sheet);
        // Redirect output to a client�s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $language->get('heading_title_1') . " " . '' . '".xlsx');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
    public function getSellers() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_type = 1 OR (seller_type = 0  AND seller_group = 1) ORDER BY nickname ASC");

        return $query->rows;
    }
}
?>
