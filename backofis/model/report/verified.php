<?php 
	class ModelReportVerified extends Model {
		public function getCustomer($data = array()){
			$sql = "select * from (SELECT CONCAT(firstname, ' ', lastname) as nama, email, telephone, date_added, verified FROM " . DB_PREFIX . "customer) as x WHERE verified = '0'";
			if (!empty($data['filter_nama'])) {
	            $sql .= " AND nama LIKE '%" . $this->db->escape($data['filter_nama']) . "%'";
	        }

	        if (!empty($data['filter_date_start'])) {
	            $sql .= " AND DATE(date_added) >= DATE('" . $this->db->escape($data['filter_date_start']) . "')";
	        }
	        if (!empty($data['filter_date_end'])) {
	            $sql .= " AND DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_end']) . "')";
	        }

	        $sort_data = array(
	            'nama',
	            'start_date',
	            'end_date'
	        );
	        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
	            $sql .= " ORDER BY " . $data['sort'];
	        } else {
	            $sql .= " ORDER BY nama";
	        }

	        if (isset($data['order']) && ($data['order'] == 'DESC')) {
	            $sql .= " DESC";
	        } else {
	            $sql .= " ASC";
	        }

	        if (isset($data['start']) || isset($data['limit'])) {
	            if ($data['start'] < 0) {
	                $data['start'] = 0;
	            }

	            if ($data['limit'] < 1) {
	                $data['limit'] = 20;
	            }

	            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
	        }
	        $query = $this->db->query($sql);
	        return $query->rows;
		}

		public function getTotalCustomers($data = array()){
			$sql = "SELECT CONCAT(firstname, ' ', lastname) as nama, email, telephone, date_added, verified FROM " . DB_PREFIX . "customer as x WHERE verified = '0'";
			
	        if (!empty($data['filter_date_start'])) {
	            $sql .= " AND DATE(date_added) >= DATE('" . $this->db->escape($data['filter_date_start']) . "')";
	        }
	        if (!empty($data['filter_date_end'])) {
	            $sql .= " AND DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_end']) . "')";
	        }

	        $sql1 = "SELECT COUNT(*) as total from ($sql) as z ";
	        if (!empty($data['filter_nama'])) {
	            $sql1 .= " WHERE nama LIKE '%" . $this->db->escape($data['filter_nama']) . "%'";
	        }
	        // echo $sql1; die();
	        $query = $this->db->query($sql1);
	        return $query->row['total'];
		}

		public function exportToExcel($results) {
	        error_reporting(E_ALL);
	        ini_set('display_errors', TRUE);
	        ini_set('display_startup_errors', TRUE);
	        date_default_timezone_set('Europe/London');

	        if (PHP_SAPI == 'cli')
	            die('This example should only be run from a Web Browser');

	        /** Include PHPExcel */
	        require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


	        // Create new PHPExcel object
	        $objPHPExcel = new PHPExcel();
	        $sheet = $objPHPExcel->getActiveSheet();

	        $this->load->model('localisation/language');
	        $language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

	        if ($language_info) {
	            $language_directory = $language_info['directory'];
	        } else {
	            $language_directory = '';
	        }

	        $language = new Language($language_directory);
	        $language->load('customer/customer');


	        // Set document properties
	        $objPHPExcel->getProperties()->setCreator("Admin")
	                ->setLastModifiedBy("Admin")
	                ->setTitle($language->get('heading_title') . ' ' . $nickname)
	                ->setSubject($language->get('heading_title'))
	                ->setDescription($language->get('heading_title'))
	                ->setKeywords("office 2007 openxml php");

	        //header style
	        $headerStyleArray = array(
	            'font' => array(
	                'bold' => true,
	            ),
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	            ),
	            'fill' => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'startcolor' => array(
	                    'argb' => '4C82C5',
	                )
	            ),
	        );

	        //title style
	        $titleStyleArray = array(
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	            ),
	            'fill' => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'startcolor' => array(
	                    'argb' => 'FFFFFF',
	                )
	            ),
	        );

	        $centerStyleArray = array(
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	            )
	        );

	        $leftStyleArray = array(
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	            )
	        );

	        $rightStyleArray = array(
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	            )
	        );

	        //bold style
	        $titleFont = array(
	            'font' => array(
	                'bold' => true,
	                'size' => 14
	            )
	        );
	        $boldFont = array(
	            'font' => array(
	                'bold' => true
	            )
	        );

	        // Column
	        $objPHPExcel->setActiveSheetIndex(0)
	                ->mergeCells('B2:E2')
	                ->setCellValue('B2', $language->get('heading_title'))
	                ->setCellValue('B6', $language->get('column_customer_nama'))
	                ->setCellValue('C6', $language->get('column_customer_email'))
	                ->setCellValue('D6', $language->get('column_customer_telephone'))
	                ->setCellValue('E6', $language->get('column_customer_date_added'));

	        //row
	        $row = 7;
	        foreach ($results as $result) {
	            $objPHPExcel->setActiveSheetIndex(0)
	                    ->setCellValue('B' . $row, $result['nama'])
	                    ->setCellValue('C' . $row, (isset($result['email']) ? $result['email'] : '-'))
	                    ->setCellValueExplicit('D' . $row, $result['telephone'], PHPExcel_Cell_DataType::TYPE_STRING)
	                    ->setCellValue('E' . $row, date('d/m/Y H:i', strtotime($result['date_added'])));
	            $row++;
	        }

	        
	        $sheet->getStyle('B2')->applyFromArray($titleFont);
	        $sheet->getStyle('B2')->applyFromArray($centerStyleArray);
	        $sheet->getStyle('B6:E6')->applyFromArray($titleStyleArray);
	        $sheet->getStyle('B6:E6')->applyFromArray($boldFont);
	        $sheet->getStyle('B')->applyFromArray($centerStyleArray);
	        $sheet->getStyle('C')->applyFromArray($centerStyleArray);
	        $sheet->getStyle('D')->applyFromArray($centerStyleArray);
	        $sheet->getStyle('E')->applyFromArray($centerStyleArray);
	        

	        $sheet->getColumnDimension('B')->setWidth(30);
	        $sheet->getColumnDimension('C')->setWidth(36);
	        $sheet->getColumnDimension('D')->setWidth(18);
	        $sheet->getColumnDimension('E')->setWidth(18);

	        //set column color
	        $sheet->getStyle('B6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
	        
	        //set border line
	        $border_style = array(
	            'borders' => array(
	                'allborders' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_THIN,
	                    'color' => array('argb' => 'A3A3A3'),
	                ),
	            ),
	        );

	        $border_thick = array(
	            'borders' => array(
	                'bottom' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_THICK,
	                    'color' => array('argb' => '8C8C8C'),
	                ),
	            ),
	        );

	        // Rename worksheet
	        $sheet->setTitle($language->get('heading_title'));

	        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
	        $objPHPExcel->setActiveSheetIndex(0);

	        // Redirect output to a client�s web browser (Excel2007)
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment;filename="' . $language->get('heading_title') . ($nickname != "" ? '-' . str_replace(" ", "_", $nickname) : "") . '.xlsx');
	        header('Cache-Control: max-age=0');
	        // If you're serving to IE 9, then the following may be needed
	        header('Cache-Control: max-age=1');

	        // If you're serving to IE over SSL, then the following may be needed
	        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	        header('Pragma: public'); // HTTP/1.0

	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	        $objWriter->save('php://output');
	        exit;
	    }
	}
?>