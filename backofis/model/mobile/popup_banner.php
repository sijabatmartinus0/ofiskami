<?php
class ModelMobilePopUpBanner extends Model {
	public function addPopUpBanner($data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "mobile_popup_banner");
		$this->db->query("INSERT INTO " . DB_PREFIX . "mobile_popup_banner SET image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "',date_modified = NOW(), date_added = NOW()");
	}

	public function getPopUpBanner() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_popup_banner LIMIT 0,1");

		return $query->row;
	}
}
