<?php
class ModelMobileSlideBanner extends Model {
	public function addSlideBanner($datas) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "mobile_slide_banner");
		foreach ($datas as $data) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "mobile_slide_banner SET image = '" . $this->db->escape($data['image']) . "', type = '" . $this->db->escape($data['type']) . "', value = '" . $this->db->escape($data['value']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "',date_modified = NOW(), date_added = NOW()");
		}
	}

	public function getSlideBanner() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_slide_banner");

		return $query->rows;
	}
}
