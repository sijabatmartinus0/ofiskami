<?php
class ModelUploadCustomer extends Model {
	public function getCustomers() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON cg.customer_group_id = cgd.customer_group_id WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY name");

		return $query->rows;
	}
	
	public function readExcel($filename){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/IOFactory.php';

		$inputFileName = DIR_UPLOAD_CUSTOMER . $filename;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestDataRow(); 
		$highestColumn = $sheet->getHighestDataColumn();
		
		$rowData = array();
		//  Loop through each row of the worksheet in turn
		for ($row = 2; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row)[0];
		}
		
		return $rowData;
	}
	
	public function validateExcel($filename){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/IOFactory.php';

		$inputFileName = DIR_UPLOAD_CUSTOMER . $filename;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestColumn = $sheet->getHighestDataColumn();
		
		if($highestColumn == "A"){
			return true;
		}else{
			return false;
		}
	}
	
	public function saveUpload($data=array()){
		$query_check = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_email WHERE email='".$this->db->escape($data['email'])."'");
		
		if($query_check->num_rows){
			$query_del = $this->db->query("DELETE FROM " . DB_PREFIX . "customer_email WHERE email='".$this->db->escape($data['email'])."'");	
		}

		$query_add = $this->db->query("INSERT INTO " . DB_PREFIX . "customer_email SET email='".$this->db->escape($data['email'])."', customer_group_id='".(int)$data['customer_group']."', date_added = NOW(), date_modified = NOW()");
	}
}
?>