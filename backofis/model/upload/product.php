<?php
class ModelUploadProduct extends Model {
	public function getSellers() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_type = 1 OR (seller_type = 0  AND seller_group = 1) ORDER BY seller_id");

		return $query->rows;
	}
	
	public function readHeaderExcel($filename){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/IOFactory.php';

		$inputFileName = DIR_UPLOAD_PRODUCT . $filename;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0);  
		$highestColumn = $sheet->getHighestDataColumn();
		
		$rowData = array();
		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= 1; $row++){ 
			//  Read a row of data into an array
			$rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row)[0];
		}
		
		return $rowData;
	}
	
	public function readExcel($filename){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/IOFactory.php';

		$inputFileName = DIR_UPLOAD_PRODUCT . $filename;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestDataRow(); 
		$highestColumn = $sheet->getHighestDataColumn();
		
		$rowData = array();
		//  Loop through each row of the worksheet in turn
		for ($row = 2; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row)[0];
		}
		
		return $rowData;
	}
	
	public function validateExcel($filename, $count_attribute){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/IOFactory.php';
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/Cell.php';

		$inputFileName = DIR_UPLOAD_PRODUCT . $filename;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn());
		
		if($highestColumn == (33+$count_attribute)){
			return true;
		}else{
			return false;
		}
	}
	
	public function getColumnExcel($column){
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel/Cell.php';

		$column_name = PHPExcel_Cell::stringFromColumnIndex($column);
		
		return $column_name;
	}
	
	public function checkSku($sku){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE sku='".$this->db->escape($sku)."' AND status=1");
		
		if((int)$query->num_rows>0){
			return false;
		}else{
			return true;
		}
	}
	
	public function getAttGroupId($category_id){
		$query = $this->db->query("SELECT attribute_group_id FROM " . DB_PREFIX . "category WHERE category_id='".(int)$category_id."' AND (attribute_group_id != 0 OR attribute_group_id != '' ) AND status=1");
		
		if($query->num_rows){
			$attribute_group_id = $query->row['attribute_group_id'];
		}else{
			$attribute_group_id = 0;
		}
		
		return $attribute_group_id;
	}
	
	public function getAttId($attribute_group_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute WHERE attribute_group_id = '".(int)$attribute_group_id."'");
		
		return $query->rows;
	}
	
	public function getAttDescription($attribute_desc, $attribute_id, $language_id){
		$query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "attribute_description WHERE name= '".$this->db->escape($attribute_desc)."' AND attribute_id='".(int)$attribute_id."' AND language_id='".(int)$language_id."'");
		
		if($query->num_rows){
			return $query->row['attribute_id'];
		}else{
			return 0;
		}
	}
	
	public function countAttId($attribute_group_id){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "attribute WHERE attribute_group_id = '".(int)$attribute_group_id."'");
		
		return $query->row['total'];
	}
	
	public function getManufacturerId($manufacturer){
		$query = $this->db->query("SELECT manufacturer_id FROM " . DB_PREFIX . "manufacturer WHERE name ='".$this->db->escape($manufacturer)."'");
		
		if($query->num_rows){
			return $query->row['manufacturer_id'];
		}else{
			return false;
		}
		
	}
	
	public function getCategoryPath($category_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE category_id='".(int)$category_id."'");
		
		if($query->num_rows){
			return $query->rows;
		}
	}
	
	public function saveUpload($data=array()){
		/*add to oc_product*/
		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET model='".$this->db->escape($data['p.model'])."', code='".$this->db->escape($data['p.code'])."', sku='".$this->db->escape($data['p.sku'])."', location='".$this->db->escape($data['p.location'])."',  quantity='".(int)$data['p.quantity']."', image='".$this->db->escape($data['p.image'])."', manufacturer_id='".(int)$data['p.manufacturer_id']."', price='".(float)$data['p.price']."', date_available='".(isset($data['p.date_available']) ? $this->db->escape($data['p.date_available']) : date("Y-m-d")) ."', weight='".(float)$data['p.weight']."',  length='".(float)$data['p.length']."', width='".(float)$data['p.width']."', height='".(float)$data['p.height']."',  minimum='".(isset($data['p.minimum']) ? (int)$data['p.minimum'] : 1) ."', stock_status_id = 6, shipping = 1, points = 0, tax_class_id = 9, weight_class_id = 1, length_class_id = 1, subtract = 1, sort_order = 0, status = 1, viewed = 0, date_added = NOW(), date_modified = NOW()");
		
		$product_id = $this->db->getLastId();
		
		/*add to oc_product_description*/
		for($x = 1; $x <= 2; $x++){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '".(int)$product_id."', language_id = '".(int)$x."', name='".$this->db->escape($data['pd.name'.$x.''])."', description='".$this->db->escape($data['pd.description'.$x.''])."', tag='".$this->db->escape($data['pd.tag'])."', meta_title='".$this->db->escape($data['pd.meta_title'])."', meta_description='".$this->db->escape($data['pd.meta_description'])."', meta_keyword='".$this->db->escape($data['pd.meta_keyword'])."'");
		}
		
		/*add to oc_product_image*/
		if(!empty($data['pi.image2'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id='".(int)$product_id."', image='".$this->db->escape($data['pi.image2'])."', sort_order = 0 ");
		}
		if(!empty($data['pi.image3'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id='".(int)$product_id."', image='".$this->db->escape($data['pi.image3'])."', sort_order = 0 ");
		}
		
		/*add to oc_product_discount*/
		if(!empty($data['pdi.quantity']) && !empty($data['pdi.priority']) && !empty($data['pdi.price'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id='".(int)$product_id."', customer_group_id = 1, quantity='".(int)$data['pdi.quantity']."', priority='".(int)$data['pdi.priority']."', price='".(float)$data['pdi.price']."', date_start='".$this->db->escape($data['pdi.date_start'])."', date_end='".$this->db->escape($data['pdi.date_end'])."'");
		}
		
		/*add to oc_product_special*/
		if(!empty($data['ps.priority']) && !empty($data['ps.price'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id='".(int)$product_id."', customer_group_id = 1, priority='".(int)$data['ps.priority']."', price='".(float)$data['ps.price']."', date_start='".$this->db->escape($data['ps.date_start'])."', date_end='".$this->db->escape($data['ps.date_end'])."'");
		}
		
		/*add to oc_ms_product*/
		$this->db->query("INSERT INTO " . DB_PREFIX . "ms_product SET product_id='".(int)$product_id."', seller_id = '".(int)$data['ms.seller_id']."', number_sold = 0, product_status = 1, product_approved = 1");
		
		/*add to oc_product_to_category*/
		// $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id='".(int)$product_id."', category_id='".(int)$this->db->escape($data['category_id'])."'");
		
		/*add to oc_product_to_store*/
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id='".(int)$product_id."', store_id = 0");
		
		return $product_id;
	}
	
	public function saveUploadAttr($data=array()){ 
		/*add to oc_product_special*/
		if(!empty($data['pa.text'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id='".(int)$this->db->escape($data['pa.product_id'])."', attribute_id='".(int)$this->db->escape($data['pa.attribute_id'])."', language_id='".(int)$this->db->escape($data['pa.language_id'])."', text='".$this->db->escape($data['pa.text'])."'");
		}
	}
	
	public function saveCategoryPath($data=array()){
		if(!empty($data['path_id'])){
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id='".(int)$this->db->escape($data['product_id'])."', category_id='".(int)$this->db->escape($data['path_id'])."'");
		}
	}
}
?>