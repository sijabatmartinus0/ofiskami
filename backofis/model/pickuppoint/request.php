<?php
class ModelPickupPointRequest extends Model {

	public function getRequests($data = array()) {
		$sql = "SELECT 
					mspr.request_id as request_id,
					ppb.pp_branch_name as branch_name,
					mss.nickname as nickname,
					mspr.date_added as date_added
				FROM " . DB_PREFIX . "ms_product_request mspr
				LEFT JOIN " . DB_PREFIX . "ms_seller mss ON mss.seller_id=mspr.seller_id
				LEFT JOIN " . DB_PREFIX . "pp_branch ppb ON ppb.pp_branch_id = mspr.kiosk_id
				WHERE mspr.status=1 ";

		if (isset($data['filter_branch']) && !is_null($data['filter_branch'])) {
			$sql .= " AND ppb.pp_branch_id = '" . (int)$data['filter_branch'] . "'";
		}

		$sort_data = array(
			'mss.nickname',
			'ppb.pp_branch_name',
			'mspr.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY mss.nickname";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalRequests($data = array()) {
		$sql = "SELECT COUNT(*) AS total
				FROM " . DB_PREFIX . "ms_product_request mspr
				LEFT JOIN " . DB_PREFIX . "ms_seller mss ON mss.seller_id=mspr.seller_id
				LEFT JOIN " . DB_PREFIX . "pp_branch ppb ON ppb.pp_branch_id = mspr.kiosk_id
				WHERE mspr.status=1 ";

		if (isset($data['filter_branch']) && !is_null($data['filter_branch'])) {
			$sql .= " AND ppb.pp_branch_id = '" . (int)$data['filter_branch'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}