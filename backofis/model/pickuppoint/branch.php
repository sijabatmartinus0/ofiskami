<?php
class ModelPickupPointBranch extends Model {
	public function addBranch($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "pp_branch SET pp_branch_name = '" . $this->db->escape($data['branch']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', company = '" . $this->db->escape($data['company']) . "', address = '" . $this->db->escape($data['address']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();
		
	}

	public function editBranch($branch_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "pp_branch SET pp_branch_name = '" . $this->db->escape($data['branch']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', company = '" . $this->db->escape($data['company']) . "', address = '" . $this->db->escape($data['address']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', status = '" . (int)$data['status'] . "' WHERE pp_branch_id = '" . (int)$branch_id . "'");
	}

	public function deleteBranch($branch_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "pp_branch WHERE pp_branch_id = '" . (int)$branch_id . "'");
	}

	public function getBranch($branch_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "pp_branch opb WHERE opb.pp_branch_id = '" . (int)$branch_id . "'");

		return $query->row;
	}

	public function getBranchs($data = array()) {
		$sql = "SELECT opb.*,opb.pp_branch_name as branch_name, oc1.name AS city FROM " . DB_PREFIX . "pp_branch opb LEFT JOIN " . DB_PREFIX . "city oc1 ON (opb.city_id = oc1.city_id) WHERE 1=1 ";
		
		$sort_data = array(
			'company',
			'city',
			'branch_name',
			'status',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY opb.company";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalBranchs($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pp_branch ";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}		
}