<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list
 *
 * @author MartinusIS
 */
class ModelMerchantList extends Model {

    public function getMerchant($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "merchant";

        // if (isset($data['filter_order_status'])) {
        //     $implode = array();

        //     $order_statuses = explode(',', $data['filter_order_status']);

        //     foreach ($order_statuses as $order_status_id) {
        //         $implode[] = "o.order_status_id = '" . (int) $order_status_id . "'";
        //     }

        //     if ($implode) {
        //         $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
        //     } else {
                
        //     }
        // } else {
        //     $sql .= " WHERE o.order_status_id > '0'";
        // }

        // if (!empty($this->user->getCustomerGroup()) && $this->user->getCustomerGroupCommaSeparated() != '') {
        //     $sql .= " AND o.customer_group_id in(" . $this->db->escape($this->user->getCustomerGroupCommaSeparated()) . ")";
        // }

        if (!empty($data['filter_merchant_id'])) {
            $sql .= " WHERE merchant_id = '" . (int) $data['filter_merchant_id'] . "'";
        }

        if (!empty($data['filter_nama'])) {
            $sql .= " WHERE nama LIKE '%" . $this->db->escape($data['filter_nama']) . "%'";
        }

        if (!empty($data['filter_badan_usaha'])) {
            $sql .= " WHERE badan_usaha LIKE '%" . $this->db->escape($data['filter_badan_usaha']) . "%'";
        }

        if (!empty($data['filter_kategori_produk'])) {
            $sql .= " WHERE kategori_produk LIKE '%" . $this->db->escape($data['filter_kategori_produk']) . "%'";
        }

        if (!empty($data['filter_website_toko'])) {
            $sql .= " WHERE website_toko LIKE '%" . $this->db->escape($data['filter_website_toko']) . "%'";
        }

        if (!empty($data['filter_npwp'])) {
            $sql .= " WHERE npwp LIKE '%" . $this->db->escape($data['filter_npwp']) . "%'";
        }

        $sort_data = array(
            'merchant_id',
            'nama',
            'badan_usaha',
            'kategori_produk',
            'website_toko',
            'npwp'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY merchant_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalMerchants($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "merchant`";
        
        if (!empty($data['filter_merchant_id'])) {
            $sql .= " AND merchant_id = '" . (int)$data['filter_merchant_id'] . "'";
        }

        if (!empty($data['filter_nama'])) {
            $sql .= " AND nama LIKE '%" . $this->db->escape($data['filter_nama']) . "%'";
        }

        if (!empty($data['filter_badan_usaha'])) {
            $sql .= " AND badan_usaha LIKE '%" . $this->db->escape($data['filter_badan_usaha']) . "%'";
        }

        if (!empty($data['filter_kategori_produk'])) {
            $sql .= " AND kategori_produk LIKE '%" . $this->db->escape($data['filter_kategori_produk']) . "%'";
        }

        if (!empty($data['filter_website_toko'])) {
            $sql .= " AND website_toko LIKE '%" . $this->db->escape($data['filter_website_toko']) . "%'";
        }

        if (!empty($data['filter_npwp'])) {
            $sql .= " AND npwp LIKE '%" . $this->db->escape($data['filter_npwp']) . "%'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function editMerchant($merchant_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "merchant` SET npwp = '" . $this->db->escape($data['npwp']) . "', state = '" . $this->db->escape($data['state']) . "' WHERE merchant_id = '" . (int)$merchant_id . "'");

        $merchant_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "merchant` WHERE merchant_id = '" . (int)$merchant_id . "'");
        $this->load->language('merchant/list');
//        foreach($merchant_query->rows as $merchant){
//            $mail = new Mail($this->config->get('config_mail'));
//            $mail->setTo($merchant['email']);
//            $mail->setFrom($this->config->get('config_email'));
//            $mail->setSender($this->config->get('config_name'));
//            $mail->setSubject($this->language->get('text_subject_merchant'));
//            
//            /*Custom by M*/
//            $data['title']=$this->language->get('text_title_merchant');
//            $data['header']=$this->language->get('text_header_merchant');
//            $data['nama']=$merchant['nama'];
//            //$data['message_merchant']=sprintf($this->language->get('message_pickup'),$invoice);
//            $data['message_warning']=$this->language->get('text_message_warning');
//            $mail->setHtml($this->load->view('default/template/mail/pickup_pickup.tpl', $data));
//            
//            $mail->send();
//            $data['message']="Thanks for your message!";
//        }
    }

    public function getOption($merchant_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "merchant` WHERE merchant_id = '" . (int)$merchant_id . "'");
        return $query->row;
    }
    public function exportToExcel($results) {
        //        echo '<pre>';
        //        var_dump($results);
        //        die();
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

        if ($language_info) {
            $language_directory = $language_info['directory'];
        } else {
            $language_directory = '';
        }

        $language = new Language($language_directory);
        $language->load('merchant/list');


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Admin")
                ->setLastModifiedBy("Admin")
                ->setTitle($language->get('heading_title') . ' ' . $nickname)
                ->setSubject($language->get('heading_title'))
                ->setDescription($language->get('heading_title'))
                ->setKeywords("office 2007 openxml php");

        //header style
        $headerStyleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '4C82C5',
                )
            ),
        );

        //title style
        $titleStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFFFFF',
                )
            ),
        );

        $centerStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $leftStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $rightStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //bold style
        $titleFont = array(
            'font' => array(
                'bold' => true,
                'size' => 14
            )
        );
        $boldFont = array(
            'font' => array(
                'bold' => true
            )
        );

        // Column
        $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells('B2:L2')
                ->setCellValue('B2', $language->get('heading_title'))
                ->setCellValue('B6', $language->get('column_merchant_id'))
                ->setCellValue('C6', $language->get('column_merchant_nama'))
                ->setCellValue('D6', $language->get('column_merchant_badan_usaha'))
                ->setCellValue('E6', $language->get('column_merchant_email'))
                ->setCellValue('F6', $language->get('column_merchant_telephone'))
                ->setCellValue('G6', $language->get('column_merchant_kategori_produk'))
                ->setCellValue('H6', $language->get('column_merchant_website_toko'))
                ->setCellValue('I6', $language->get('column_merchant_npwp'))
                ->setCellValue('J6', $language->get('column_merchant_status'))
                ->setCellValue('K6', $language->get('column_merchant_created_at'))
                ->setCellValue('L6', $language->get('column_merchant_updated_at'));

        //row
        $row = 7;
        foreach ($results as $result) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . $row, $result['merchant_id'])
                    ->setCellValue('C' . $row, $result['nama'])
                    ->setCellValue('D' . $row, $result['badan_usaha'])
                    ->setCellValue('E' . $row, $result['email'])
                    ->setCellValueExplicit('F' . $row, $result['no_telp'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('G' . $row, $result['kategori_produk'])
                    ->setCellValue('H' . $row, $result['website_toko'])
                    ->setCellValue('I' . $row, $result['npwp'])
                    ->setCellValue('J' . $row, $result['state'])
                    ->setCellValue('K' . $row, $result['created_at'])
                    ->setCellValue('L' . $row, $result['updated_at']);
            $row++;
        }

        
        $sheet->getStyle('B2')->applyFromArray($titleFont);
        $sheet->getStyle('B2')->applyFromArray($centerStyleArray);
        $sheet->getStyle('B6:L6')->applyFromArray($titleStyleArray);
        $sheet->getStyle('B6:L6')->applyFromArray($boldFont);
        $sheet->getStyle('B')->applyFromArray($centerStyleArray);
        $sheet->getStyle('C')->applyFromArray($centerStyleArray);
        $sheet->getStyle('D')->applyFromArray($centerStyleArray);
        $sheet->getStyle('E')->applyFromArray($centerStyleArray);
        $sheet->getStyle('F')->applyFromArray($centerStyleArray);
        $sheet->getStyle('G')->applyFromArray($centerStyleArray);
        $sheet->getStyle('H')->applyFromArray($centerStyleArray);
        $sheet->getStyle('I')->applyFromArray($centerStyleArray);
        $sheet->getStyle('J')->applyFromArray($centerStyleArray);
        $sheet->getStyle('K')->applyFromArray($centerStyleArray);
        $sheet->getStyle('L')->applyFromArray($centerStyleArray);

        $sheet->getColumnDimension('B')->setWidth(35);
        $sheet->getColumnDimension('C')->setWidth(35);
        $sheet->getColumnDimension('D')->setWidth(35);
        $sheet->getColumnDimension('E')->setWidth(35);
        $sheet->getColumnDimension('F')->setWidth(35);
        $sheet->getColumnDimension('G')->setWidth(35);
        $sheet->getColumnDimension('H')->setWidth(35);
        $sheet->getColumnDimension('I')->setWidth(18);
        $sheet->getColumnDimension('J')->setWidth(18);
        $sheet->getColumnDimension('K')->setWidth(35);
        $sheet->getColumnDimension('L')->setWidth(35);

        //set column color
        $sheet->getStyle('B6:L6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B13:E13')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //        $sheet->getStyle('C15:C16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B15:B16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //set border line
        $border_style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'A3A3A3'),
                ),
            ),
        );

        $border_thick = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '8C8C8C'),
                ),
            ),
        );

        //        $sheet->getStyle("B6:E13")->applyFromArray($border_style);
        //        $sheet->getStyle("B15:E16")->applyFromArray($border_style);
        // Rename worksheet
        $sheet->setTitle($language->get('heading_title_1'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Set logo header
        // $objDrawing = new PHPExcel_Worksheet_Drawing();
        // $objDrawing->setName('Ofiskita');
        // $objDrawing->setDescription('Ofiskita');
        // $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
        // $objDrawing->setCoordinates('A1'); 
        // $objDrawing->setWidthAndHeight(200,63);
        // $objDrawing->setWorksheet($sheet);
        // Redirect output to a client�s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $language->get('heading_title') . " " . '' . '".xlsx');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}
