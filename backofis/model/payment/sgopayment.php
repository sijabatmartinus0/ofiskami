<?php
class ModelPaymentSgopayment extends Model {
	
	public function install (){
		
		$sql = "CREATE TABLE `".DB_PREFIX."sgopayment` (
				`id`  int(255) NOT NULL AUTO_INCREMENT ,
				`cart_id`  int(255) NOT NULL ,
				`total`  varchar(255) NOT NULL ,
				`currency` VARCHAR(255) NOT NULL,
				`ket` VARCHAR(255) NOT NULL,
				PRIMARY KEY (`id`)
				)";
		
		$this->db->query($sql);
		
	}
}