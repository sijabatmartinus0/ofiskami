<?php
class ModelSaleConfirmPayment extends Model {
	public function getConfirmPaymentOrders($data = array()) {
		$sql = "SELECT DISTINCT CONCAT(o.firstname, ' ' ,o.lastname) as customer, '' as invoice, o.order_id, od.date_added, o.total, o.currency_code, o.currency_value, opc.account_name, opc.account_number, opc.account_id, opc.account_destination_id FROM " . DB_PREFIX . "order o INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_id = o.order_id INNER JOIN " . DB_PREFIX . "order_payment_confirmation opc ON opc.order_id = o.order_id WHERE date(od.date_added) LIKE '%".$this->db->escape($data['filter_date_added'])."%' AND o.order_id LIKE '%". $this->db->escape($data['filter_order_id']) ."%'  AND CONCAT(o.firstname, o.lastname) LIKE '%".$this->db->escape($data['filter_customer'])."%' AND opc.account_destination_id LIKE '%".$this->db->escape($data['filter_bank'])."%' AND o.order_status_id = 2 ORDER BY order_id DESC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function totalConfirmPaymentOrders($data = array()) {
		$sql = "SELECT COUNT(DISTINCT o.order_id) as total FROM " . DB_PREFIX . "order o INNER JOIN " . DB_PREFIX . "order_detail od ON od.order_id = o.order_id WHERE date(od.date_added) LIKE '%".$this->db->escape($data['filter_date_added'])."%' AND o.order_id LIKE '%". $this->db->escape($data['filter_order_id']) ."%'  AND CONCAT(o.firstname, o.lastname) LIKE '%".$this->db->escape($data['filter_customer'])."%' AND o.order_status_id = 2";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getCountOrderIdFromDetails($order_id){
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) WHERE od.order_id = '".(int)$order_id."'");

		return $query->row['total'];
	}
	
	public function getAccounts() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "account_destination ad INNER JOIN " . DB_PREFIX . "account a ON a.account_id = ad.account_id WHERE ad.status = '1'");

		return $query->rows;
	}
	
	public function getAccountDescription($account_id) {
		$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "account WHERE status = '1' AND account_id='".(int)$account_id."'");

		return $query->row['name'];
	}
	
}
?>