<table class="table table-bordered table-striped">
			<?php if(!empty($payment_confirmation)) {?>
				<tr>
                <td><?php echo $column_order_id; ?></td>
                <td colspan="3"><?php echo $order_id; ?></td>
              </tr>
				<tr>
                <td><?php echo $text_pc_payment_method; ?></td>
                <td colspan="3"><?php echo $payment_confirmation['payment_method']; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_pc_date_payment; ?></td>
                <td colspan="3"><?php echo $payment_confirmation['date_payment']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_account; ?></td>
                <td><?php echo $payment_confirmation['account']; ?> <?php echo isset($payment_confirmation['account_branch']) ? "(".$payment_confirmation['account_branch'].")" : ''; ?></td>
				<td><?php echo $text_pc_account_destination; ?></td>
                <td><?php echo $payment_confirmation['account_destination']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_account_name; ?></td>
                <td><?php echo $payment_confirmation['account_name']; ?></td>
				<td><?php echo $text_pc_account_destination_name; ?></td>
                <td><?php echo $payment_confirmation['account_destination_name']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_account_number; ?></td>
                <td><?php echo $payment_confirmation['account_number']; ?></td>
				<td><?php echo $text_pc_account_destination_number; ?></td>
                <td><?php echo $payment_confirmation['account_destination_number']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_total_order; ?></td>
                <td colspan="3"><?php echo $payment_confirmation['total_order']; ?></td>
              </tr>
              <tr>
			  <?php 
				$style='';
				if($payment_confirmation['total_order_amount']==$payment_confirmation['total_amount']){
					$style="style='background:#34A853;color:#ffffff;' ";
				}else if($payment_confirmation['total_order_amount']>$payment_confirmation['total_amount']){
					$style="style='background:#EA4335;color:#ffffff;' ";
				}else if($payment_confirmation['total_order_amount']<$payment_confirmation['total_amount']){
					$style="style='background:#4285F4;color:#ffffff;' ";
				}
			  ?>
                <td><?php echo $text_pc_total; ?></td>
                <td colspan="3" <?php echo $style;?>><?php echo $payment_confirmation['total']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_attachment; ?></td>
                <td colspan="3">
				<?php if($payment_confirmation['image']!=''){ ?>
					<object data="<?php echo $payment_confirmation['image']; ?>" width="500" height="500">
					</object></br>
					<a href="<?php echo $payment_confirmation['image']; ?>"><?php echo $text_pc_attachment; ?></a>
				<?php }else{ echo $text_pc_no_attachment; } ?>
				</td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_information; ?></td>
                <td colspan="3"><?php echo $payment_confirmation['information']; ?></td>
              </tr>
			  <tr>
                <td><?php echo $text_pc_status; ?></td>
                <td colspan="3" style="color:#ff0000" class="order-status"><?php echo $order_status; ?></td>
              </tr>
			  <?php if((int)$order_status_id==2){ ?>
			  <tr id="button-payment_confirmation_detail">
				<td colspan="4">
				<div class="text-right">
					<button id="button-reject-payment" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-close"></i> <?php echo $button_payment_confirmation_reject; ?></button>
					<button id="button-accept-payment" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_payment_confirmation_accept; ?></button>
				</div>
				</td>
			  </tr>
			  <?php } ?>
			<?php } else {?>
				<tr>
                <td><?php echo $text_pc_no_confirmation; ?></td>
              </tr>
			<?php } ?>
			</table>
			
 <script type="text/javascript">
	$('#button-accept-payment').on('click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/paymentAccept&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reject-payment').button('loading');	
			$('#button-accept-payment').button('loading');				
		},
		complete: function() {
			$('#button-reject-payment').button('reset');
			$('#button-accept-payment').button('reset');			
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#payment_confirmation_detail').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				url = 'index.php?route=sale/confirm_payment&token=<?php echo $token; ?>';
				location = url;	
				
				$('.order-status').empty().html(json['payment-status']);
				
				$('#payment_confirmation_detail').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');	

				$('#button-payment_confirmation_detail').empty();					
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#button-reject-payment').on('click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/paymentReject&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reject-payment').button('loading');	
			$('#button-accept-payment').button('loading');				
		},
		complete: function() {
			$('#button-reject-payment').button('reset');
			$('#button-accept-payment').button('reset');			
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#payment_confirmation_detail').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				url = 'index.php?route=sale/confirm_payment&token=<?php echo $token; ?>';
				location = url;	

				$('.order-status').empty().html(json['payment-status']);
				
				$('#payment_confirmation_detail').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');	

				$('#button-payment_confirmation_detail').empty();			
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

</script>