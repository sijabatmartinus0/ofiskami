<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    <h1><?php echo $text_order_id; ?> #<?php echo $order['order_id']; ?></h1>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td colspan="3"><?php echo $text_order_detail; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 30%;vertical-align: top;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?>
            </address>
            <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
            <b><?php echo $text_website; ?></b> <a href="<?php echo $order['store_url']; ?>"><?php echo $order['store_url']; ?></a></td>
          <td style="width: 30%;vertical-align: top;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            </td>
			<td style="width: 40%;vertical-align: top;">
			<b><?php echo $text_customer; ?></b><br />
			<address>
            <?php echo $order['payment_address']; ?>
            </address>
			</td>
        </tr>
      </tbody>
    </table>
	
	<?php foreach ($order['order_detail'] as $order_detail) { ?>
	<table class="table table-bordered">
	  <thead>
        <tr>
          <td colspan="5">#<?php echo $order_detail['invoice_no']; ?></td>
        </tr>
      </thead>
      <tbody>
	  <tr>
        <td style="width: 50%;vertical-align: top;">
			<b><?php echo $text_seller; ?></b><br />
			<address>
            <?php echo $order_detail['seller_address']; ?>
            </address>
		</td>
		<td style="width: 50%;vertical-align: top;" colspan="4">
			<b><?php echo $text_shipping_address; ?></b><br />
			<address>
            <?php echo $order_detail['shipping_address']; ?>
            </address>
			<b><?php echo $text_delivery_type; ?></b><br />
			<?php echo $order_detail['delivery_type']; ?>
		</td>
      </tr>
	  <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td><b><?php echo $column_model; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
	<?php foreach ($order_detail['products'] as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td><?php echo $product['model']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>
    <?php } ?>
	<?php foreach ($order_detail['vouchers'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
    <?php } ?>
	<tr>
        <td class="text-right" colspan="4"><b><?php echo $text_shipping_price; ?></b></td>
        <td class="text-right"><?php echo $order_detail['shipping_price']; ?></td>
    </tr>
	<tr>
        <td class="text-right" colspan="4"><b><?php echo $text_total_invoice; ?></b></td>
        <td class="text-right"><?php echo $order_detail['total']; ?></td>
    </tr>
	<tbody>
	</table>
	<?php } ?>
    <table class="table table-bordered">
      <tbody>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>
</div>
</body>
</html>