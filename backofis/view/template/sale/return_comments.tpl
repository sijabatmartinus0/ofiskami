<?php if($return_comments) {?>
<div class="content-comments">
<?php foreach($return_comments as $return_comment){ ?>
	<div class="content">
		<div class="comment-header">
			<span class="comment-name">
				<h4>
				<b>
				<?php 
					if ($return_comment['created_by'] == $return_comment['seller_id']){
						echo $return_comment['nickname'];
					}else{
						echo $return_comment['firstname'];
					}
				 ?>
				 </b>
				<?php 
					if ($return_comment['created_by'] == $return_comment['seller_id']){ ?>
					<span class="badge-orange">	
					<?php echo $column_merchant; ?>
					</span>
				<?php }else{ ?>
					<span class="badge-green">
					<?php echo $column_customer; ?>
					</span>
				<?php } ?>
				</span>
				<small class="muted"><i><?php echo $return_comment['created_date'] ?></i></small>
				</h4>
			</span>
		</div>
		<div class="comment-content"><?php echo $return_comment['comment'] ?></div>
	</div>
	<hr />
<?php } ?>
	<br />
</div>
<?php } else { ?>
<div class="panel-heading">
	<h4 class="panel-title">
	<a><?php echo $text_no_data; ?></a>
	</h4>
</div>
<?php } ?>