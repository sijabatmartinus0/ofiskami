<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="ms-transaction-page">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $text_heading; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
   <?php if (isset($success) && $success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_heading; ?></h3>
      </div>
      <div class="panel-body">
		<div class="table-responsive">
		<table class="list table table-bordered table-hover" style="text-align: center" id="list-transactions">
		<thead>
			<tr>
				<td class="tiny"><?php echo $label_id; ?></td>
				<td class="medium"><?php echo $label_customer; ?></a></td>
				<td class="small"><?php echo $label_net_amount; ?></a></td>
				<td><?php echo $label_description; ?></a></td>
				<td class="medium"><?php echo $label_date; ?></a></td>
			</tr>
			<tr class="filter">
				<td><input type="text"/></td>
				<td><input type="text"/></td>
				<td><input type="text"/></td>
				<td><input type="text"/></td>
				<td><input type="text"/></td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		</div>
      </div>
	</div>
  </div>
</div>

<script type="text/javascript">
$(function() {
	$.fn.dataTableExt.sErrMode = 'throw';

	if (typeof msGlobals.config_language != 'undefined') {
		$.extend($.fn.dataTable.defaults, {
			"oLanguage": {
				"sUrl": msGlobals.config_language
			}
		});
	}

	$.extend($.fn.dataTable.defaults, {
		"bProcessing": true,
		"bSortCellsTop": true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"bAutoWidth": false,
		"bLengthChange": false,
		"iDisplayLength": 50
		//"iDisplayLength": msGlobals.config_admin_limit
		/*
		"fnDrawCallback":function(){
			if ( $('.dataTables_paginate span span.paginate_button').size()) {
				$('.dataTables_paginate')[0].style.display = "block";
			} else {
				$('.dataTables_paginate')[0].style.display = "none";
			}
		}*/
	});
	
	
	$("body").delegate(".dataTable .filter input[type='text']", "keyup",  function() {
		$(this).parents(".dataTable").dataTable().fnFilter(this.value, $(this).parents(".dataTable").find("thead tr.filter td").index($(this).parent("td")));
	});
});
$(document).ready(function() {
	$('#list-transactions').dataTable( {
		"sAjaxSource": "index.php?route=sale/customer_transaction/getTableData&token=<?php echo $token; ?>",
		"aoColumns": [
			{ "mData": "id" },
			{ "mData": "customer" },
			{ "mData": "amount" },
			{ "mData": "description" },
			{ "mData": "date_created" },
		],
        "aaSorting":  [[4,'desc']]
	});
});
</script>
<?php echo $footer; ?> 