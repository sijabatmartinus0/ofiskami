<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="ms-payout">
  <div class="page-header">
    <div class="container-fluid">

      <h1><?php echo $heading; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].$_SERVER['REMOTE_ADDR']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
   <?php if (isset($success) && $success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading; ?></h3>
      </div>
      <div class="panel-body">
		<div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="filter_date_start"><?php echo $text_filter_date_start; ?></label>
				<div class="input-group date">
                    <input id="filter_date_start" type="text" name="filter_date_start" placeholder="<?php echo $text_filter_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                    <span class="input-group-btn">
					<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
				</div>
			  </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="filter_date_end"><?php echo $text_filter_date_end; ?></label>
                <div class="input-group date">
                    <input id="filter_date_end" type="text" name="filter_date_end" placeholder="<?php echo $text_filter_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                    <span class="input-group-btn">
					<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
				</div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="filter_status"><?php echo $text_filter_status; ?></label>
                <select name="filter_status" id="filter_status" class="form-control">
                  <option value=""></option>
                  <option value="<?php echo Payment::STATUS_UNPAID;?>"><?php echo $text_status_1; ?></option>
                  <option value="<?php echo Payment::STATUS_PAID;?>"><?php echo $text_status_2; ?></option>
                </select>
              </div>
				<div class="pull-right">
					<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo $text_button_filter; ?></button>
					<button type="button" id="button-export" class="btn btn-success"><i class="fa fa-file-excel-o "></i> <?php echo $text_button_export; ?></button>
				</div>
			</div>
          </div>
        </div>
        <p><?php echo $text_payout_requests; ?>: <b><?php echo $payout_requests['amount_pending'];?></b> <?php echo strtolower($text_pending); ?> / <b><?php echo $payout_requests['amount_paid'];?></b> <?php echo strtolower($text_paid); ?></p>
        <p><?php echo $text_payouts; ?>: <b><?php echo $payouts['amount_pending'];?></b> <?php echo strtolower($text_pending); ?> / <b><?php echo $payouts['amount_paid'];?></b> <?php echo strtolower($text_paid); ?></p>
		<div class="table-responsive">
        <form action="" method="post" enctype="multipart/form-data" id="form">
		<table class="list table table-bordered table-hover" style="text-align: center" id="list-payments">
            <thead>
            <tr>
                <td class="tiny"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                <td class="small"><?php echo $label_type; ?></td>
                <td class="medium"><?php echo $label_customer; ?></td>
                <td class="small"><?php echo $label_amount; ?></td>
                <td><?php echo $label_description; ?></td>
                <td class="medium"><?php echo $label_status; ?></td>
                <td class="medium"><?php echo $label_date_created; ?></td>
                <td class="medium"><?php echo $label_date_paid; ?></td>
                <td class="medium"><?php echo $label_action; ?></td>
            </tr>
            <tr class="filter">
                <td></td>
                <td></td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td></td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td></td>
            </tr>
            </thead>

            <tbody></tbody>
        </table>
	    </form>
        </div>
      </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$(function() {
	$.fn.dataTableExt.sErrMode = 'throw';

	if (typeof msGlobals.config_language != 'undefined') {
		$.extend($.fn.dataTable.defaults, {
			"oLanguage": {
				"sUrl": msGlobals.config_language
			}
		});
	}

	$.extend($.fn.dataTable.defaults, {
		"bProcessing": true,
		"bSortCellsTop": true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"bAutoWidth": false,
		"bLengthChange": false,
		"iDisplayLength": 50
		//"iDisplayLength": msGlobals.config_admin_limit
		/*
		"fnDrawCallback":function(){
			if ( $('.dataTables_paginate span span.paginate_button').size()) {
				$('.dataTables_paginate')[0].style.display = "block";
			} else {
				$('.dataTables_paginate')[0].style.display = "none";
			}
		}*/
	});
	
	
	$("body").delegate(".dataTable .filter input[type='text']", "keyup",  function() {
		$(this).parents(".dataTable").dataTable().fnFilter(this.value, $(this).parents(".dataTable").find("thead tr.filter td").index($(this).parent("td")));
	});
});
$(document).ready(function() {
	$('#list-payments').dataTable( {
		"sAjaxSource": "index.php?route=sale/customer_withdrawal/getTableData&token=<?php echo $token; ?>",
		"aoColumns": [
			{ "mData": "checkbox", "bSortable": false },
			{ "mData": "payment_type" },
			{ "mData": "customer" },
			{ "mData": "amount" },
			{ "mData": "description" },
			{ "mData": "payment_status" },
			{ "mData": "date_created" },
			{ "mData": "date_paid" },
			{ "mData": "actions", "bSortable": false, "sClass": "text-right" }
		],
        "aaSorting":  [[6,'desc']]
	});
	
	$('#button-filter').on('click', function() {
		if(($('#filter_date_start').val()=='' && $('#filter_date_end').val()=='') || ($('#filter_date_start').val()!='' && $('#filter_date_end').val()!='')){
		var filter='&filter_date_start='+$('#filter_date_start').val()+'&filter_date_end='+$('#filter_date_end').val()+'&filter_status='+$('#filter_status').val();
		$('#list-payments').dataTable().fnDestroy();
		$('#list-payments').dataTable( {
			"sAjaxSource": "index.php?route=sale/customer_withdrawal/getTableData&token=<?php echo $token; ?>"+filter,
			"aoColumns": [
				{ "mData": "checkbox", "bSortable": false },
				{ "mData": "payment_type" },
				{ "mData": "customer" },
				{ "mData": "amount" },
				{ "mData": "description" },
				{ "mData": "payment_status" },
				{ "mData": "date_created" },
				{ "mData": "date_paid" },
				{ "mData": "actions", "bSortable": false, "sClass": "text-right" }
			],
			"aaSorting":  [[6,'desc']]
		});
		}else{
			alert('<?php echo $error_payment_filter_date; ?>');
		}
	});	
	
	$('#button-export').on('click', function() {
		if(($('#filter_date_start').val()=='' && $('#filter_date_end').val()=='') || ($('#filter_date_start').val()!='' && $('#filter_date_end').val()!='')){
			location='<?php echo $link_report_payout; ?>&filter_date_start='+$('#filter_date_start').val() + '&filter_date_end='+$('#filter_date_end').val() + '&filter_status='+$('#filter_status').val();
		}else{
			alert('<?php echo $error_payment_filter_date; ?>');
		}
    });
	
	$(document).on('click', '.ms-button-status, .ms-button-mark', function() {
		var button = $(this);
		var row = button.parents('tr');
		var payment_id = row.children('td:first').find('input:checkbox').val();
		var payment_status = button.hasClass('ms-button-mark') ? '<?php echo Payment::STATUS_PAID; ?>' : button.prev('select').find('option:selected').val();
		button.hide().before(button.hasClass('ms-button-mark') ? '<a class="ms-button ms-loading" />' : '<a class="ms-button ms-button-small ms-loading" />');

		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=sale/customer_withdrawal/jxUpdateStatus&payment_id='+ payment_id +'&payment_status='+ payment_status +'&token=<?php echo $token; ?>',
			complete: function(jqXHR, textStatus) {
				button.show();
				row.find('.ms-loading').remove();
			},
			success: function(jsonData) {
				if (jsonData.payment) {
					if (jsonData.payment.payment_status == <?php echo Payment::STATUS_UNPAID; ?>) {
						if (row.children('td:last-child').find('.ms-button-mark').length == 0) row.children('td:last-child').prepend('<a class="ms-button ms-button-mark" title="<?php echo $text_mark; ?>"></a>');
						
						if (jsonData.payment.payment_type == <?php echo Payment::TYPE_PAYOUT; ?> || jsonData.payment.payment_type == <?php echo Payment::TYPE_PAYOUT_REQUEST; ?>)
							if (row.children('td:last-child').find('.ms-button-paypal').length == 0) row.children('td:last-child').prepend('<a class="ms-button ms-button-paypal" title="<?php echo $text_payout_paypal; ?>"></a>');

					} else if (jsonData.payment.payment_status == <?php echo Payment::STATUS_PAID; ?>) {
						row.children('td:last-child').find('.ms-button-mark, .ms-button-paypal').remove();
					}
					
					row.children('td:last-child').prev('td').html(jsonData.payment.payment_date);
					row.find('select[name="ms-payment-status"]').val(jsonData.payment.payment_status);
				}
				row.children('td').effect("highlight", {color: '#BBDF8D'}, 2000);
			}
		});
	});
	
	$(document).on('click', '.ms-button-delete', function() {
		var payment_id = $(this).parents('tr').children('td:first').find('input:checkbox').val();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=sale/customer_withdrawal/jxDelete&payment_id='+ payment_id +'&token=<?php echo $token; ?>',
			beforeSend: function() {
				$('.warning').text('').hide();
			},
			complete: function(jqXHR, textStatus) {
				window.location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.warning').text(textStatus).show();
			},
			success: function(jsonData) {
				window.location.reload();
			}
		});
	});	
	
});
</script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script> 
<?php echo $footer; ?> 
