<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" ><?php echo $text_order_id; ?></label>
					<input type="text" name="filter_order_id" placeholder="<?php echo $text_order_id; ?>" value="<?php echo $filter_order_id; ?>" class="form-control number" />
				</div>
            </div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" ><?php echo $text_invoice; ?></label>
					<input type="text" name="filter_invoice" placeholder="<?php echo $text_invoice; ?>" value="<?php echo $filter_invoice; ?>" class="form-control" />
				</div>
            </div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" ><?php echo $text_bank; ?></label>
					<select name="filter_bank" class="form-control">
						<option value=""></option>
						<?php foreach($banks as $bank) { ?>
						<?php if($filter_bank == $bank['account_destination_id']){ ?>
							<option value="<?php echo $bank['account_destination_id']; ?>" selected="selected" ><?php echo $bank['name']; ?></option>
						<?php }else{ ?>
							<option value="<?php echo $bank['account_destination_id']; ?>"><?php echo $bank['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
            </div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" ><?php echo $text_customer; ?></label>
					<input type="text" name="filter_customer" placeholder="<?php echo $text_customer; ?>" value="<?php echo $filter_customer; ?>" class="form-control" />
				</div>
            </div>
			<div class="col-sm-6">
			<div class="form-group">
                <label class="control-label" for="input-date-modified"><?php echo $text_date_added; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $text_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              </div>
			  
			<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_search; ?></button>
          </div>
        </div>
        <div class="table-responsive" id="order_list">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td width="10%"><?php echo $column_order_id; ?></td>
                <!--<td><?php echo $column_invoice; ?></td>-->
                <td><?php echo $text_bank_origin; ?></td>
                <td><?php echo $text_bank; ?></td>
                <td><?php echo $column_customer; ?></td>
                <td><?php echo $column_total; ?></td>
                <td width="15%"><?php echo $column_date_added; ?></td>
				<td width="10%"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($payments){ ?>
			
				<?php 
				foreach ($payments as $payment) { ?>
				<tr>
					<td class="text-center"><?php echo $payment['order_id']; ?></td>
					<td><?php echo $payment['bank_origin']; ?><br/><?php echo $payment['account_name'] ?> - <?php echo $payment['account_number']; ?></td>
					<td class="text-center"><?php echo $payment['bank_destination']; ?></td>
					<td><?php echo $payment['customer']; ?></td>
					<td class="text-right"><?php echo $payment['total']; ?></td>
					<td class="text-center"><?php echo $payment['date_added']; ?></td>
					<td class="text-center">
						<a data-toggle="modal" data-target="#myModal" data-orderid="<?php echo $payment['order_id']; ?>" title="<?php echo $button_accept; ?>" id="<?php echo $payment['order_id']; ?>detail" class="btn btn-danger"><i class="fa fa-check"></i></a> 
					</td>
					
					<script type="text/javascript">
					$("#<?php echo $payment['order_id']; ?>detail").on('click', function(){
						$('.pcAttention2').show();
						$('#payment_confirmation_detail').empty().load('index.php?route=sale/confirm_payment/paymentConfirmation&token=<?php echo $token; ?>&order_id=<?php echo $payment['order_id']; ?>',function(){$('.pcAttention2').hide();});
					});
					</script>
				</tr>
				<?php
					}
				?>
				<?php } else { ?>
				<tr>
                  <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                </tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
 
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; right: 150px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h1><?php echo $text_detail; ?></h1>
			</div>
			<div class="modal-body" >
			<div class="pcAttention2" style="display:none;margin: 0 auto;text-align:center"><img src="view/image/loading.gif" alt="" /></div>
			<div id="payment_confirmation_detail">
				
			</div>
		</div>
		<!--<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" ><?php echo $button_cancel; ?></button>
			<button type="button" class="btn btn-primary" ><?php echo $button_submit; ?></button>
		</div>-->
	</div>
</div>
</div>
  
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/confirm_payment&token=<?php echo $token; ?>';
	
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	var filter_customer = $('input[name=\'filter_customer\']').val();
	var filter_date_added = $('input[name=\'filter_date_added\']').val();
	var filter_invoice = $('input[name=\'filter_invoice\']').val();
	var filter_bank = $('select[name=\'filter_bank\']').val();
	
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}	
	
	if (filter_invoice) {
		url += '&filter_invoice=' + encodeURIComponent(filter_invoice);
	}
	
	if (filter_bank) {
		url += '&filter_bank=' + encodeURIComponent(filter_bank);
	}

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>