<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!--<button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button>-->
        <button type="submit" form="myForm" formaction="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></button>
        <!--<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>-->
		</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
         <div class="well">
          <form method="post" id="myForm" enctype="multipart/form-data" action="<?php echo $download_report; ?>">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-seller"><?php echo $entry_seller_id; ?></label>
                <select class="form-control" value="" id="seller" name="filter_seller_id">
                  <option value="0" selected="selected">All Seller</option>
                  <?php foreach($sellers as $seller){ ?>
                    <?php if ($seller['seller_id'] == $filter_seller_id) { ?>
                      <option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $seller['seller_id']; ?>"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
	      <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status_order; ?></label>
                <select class="form-control" value="" id="status" name="filter_status_id">
                  <option value="0" selected="selected">All Status</option>
                  <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $filter_status_id) { ?>
                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-merchant-id"><?php echo $entry_receipt_no; ?></label>
                <input type="text" name="filter_receipt_no" value="<?php echo $filter_receipt_no; ?>" placeholder="<?php echo $entry_receipt_no; ?>" id="input-order-id" class="number form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_start_date; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_end_date; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
		</div>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
          </form>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left"><?php if ($sort == 'receipt_no') { ?>
                    <a href="<?php echo $sort_receipt_no; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_receipt_no; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_receipt_no; ?>"><?php echo $column_receipt_no; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'no_resi') { ?>
                    <a href="<?php echo $sort_no_resi; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_no_resi; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_no_resi; ?>"><?php echo $column_no_resi; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'no_resi') { ?>
                    <a href="<?php echo $sort_no_resi; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_id_kios; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_no_resi; ?>"><?php echo $column_id_kios; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_transaction_date; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_transaction_date; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'sku') { ?>
                    <a href="<?php echo $sort_sku; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_sku; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sku; ?>"><?php echo $column_sku; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'code') { ?>
                    <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_product_code; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_code; ?>"><?php echo $column_product_code; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_price_per_item; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price_per_item; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'shipping_price') { ?>
                    <a href="<?php echo $sort_shipping_price; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_shipping_fee; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_shipping_price; ?>"><?php echo $column_shipping_fee; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'commission_fee') { ?>
                    <a href="<?php echo $sort_commission_fee; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_commission_fee; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_commission_fee; ?>"><?php echo $column_commission_fee; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_status_order; ?>" class="<?php echo strtolower($order_fullfills); ?>"><?php echo $column_status_order; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status_order; ?>"><?php echo $column_status_order; ?></a>
                    <?php } ?>
                  </td>
                </tr>
              </thead>
              <tbody>
                <?php if ($order_fullfill) { ?>
                <?php foreach ($order_fullfill as $order_fullfills) { ?>
                <tr>
                  <td class="text-center"><?php echo $order_fullfills['receipt_no']; ?></td>
                  <td class="text-left">
			<?php if($order_fullfills['no_resi']){
				echo $order_fullfills['no_resi'];
		  	} else{
				echo '-';
			} ?>
		 </td>
                  
                  <td class="text-left"><?php 
                    if($order_fullfills['kiosk_id'] != NULL) {
                      echo $order_fullfills['kiosk_id'];
                    } else{
                      echo '-';
                    } ?>
                      
                    </td>
                  <td class="text-left"><?php echo $order_fullfills['date_added']; ?></td>
                  <td class="text-left"><?php echo $order_fullfills['quantity']; ?></td>
                  <td class="text-left"><?php echo $order_fullfills['sku']; ?></td>
                  <td class="text-left"><?php echo $order_fullfills['code']; ?></td>
                  <td class="text-left"><?php echo $order_fullfills['price'];?></td>
                  <td class="text-left"><?php echo $order_fullfills['shipping_price'];?></td>
                  <td class="text-left"><?php echo $order_fullfills['commission_fee'];?></td>
                  <td class="text-left"><?php echo $order_fullfills['order_status_id'];?></td>
                </tr>
        <!-- <?php if(sizeof($order['order_detail'])>1){?>
        <?php array_splice($order['order_detail'], 0, 1);?>
        
        <?php }?> -->
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="14"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/customer/reportFullfill&token=<?php echo $token; ?>';
	var filter_seller_id = $('select[name=\'filter_seller_id\']').val();
   	 if (filter_seller_id) {
      	url += '&filter_seller_id=' + encodeURIComponent(filter_seller_id);
    	}
	var filter_status_id = $('select[name=\'filter_status_id\']').val();
  	if (filter_status_id) {
    		url += '&filter_status_id=' + encodeURIComponent(filter_status_id);
  	}
	var filter_receipt_no = $('input[name=\'filter_receipt_no\']').val();
	if (filter_receipt_no) {
		url += '&filter_receipt_no=' + encodeURIComponent(filter_receipt_no);
	}
	
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}
	
	var filter_date_end = $('input[name=\'filter_date_end\']').val();
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

				
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}	
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);
	
	var selected = $('input[name^=\'selected\']:checked');
	
	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}
	
	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);
			
			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

$('a[id^=\'button-delete\']').on('click', function(e) {
	e.preventDefault();
	
	if (confirm('<?php echo $text_delete; ?>')) {
		location = $(this).attr('href');
	}
});
//--></script> 
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

//--></script>
</div>
<?php echo $footer; ?>
