<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-option" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-option" class="form-horizontal">
          <div class="form-group">
              <label class="col-sm-2 control-label" for="input-npwp"><?php echo $entry_merchant_npwp; ?></label>
                <div class="col-sm-10">
                  <select name="npwp" id="input-npwp" class="form-control">
                    <?php if ($npwp) { ?>
                      <option value="<?php echo $text_yes; ?>" selected="selected"><?php echo $text_yes; ?></option>
                      <option value="<?php echo $text_no; ?>"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <<option value="<?php echo $text_yes; ?>"><?php echo $text_yes; ?></option>
                    <option value="<?php echo $text_no; ?>" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_merchant_status; ?></label>
                <div class="col-sm-10">
                  <select name="state" id="input-status" class="form-control">
                    <?php if ($state == 'Request') { ?>
                      <option value="<?php echo $text_request; ?>" selected="selected"><?php echo $text_request; ?></option>
                      <option value="<?php echo $text_approve; ?>"><?php echo $text_approve; ?></option>
                      <option value="<?php echo $text_reject; ?>"><?php echo $text_reject; ?></option>
                    <?php } elseif ($state == 'Approve') { ?>
                      <option value="<?php echo $text_request; ?>"><?php echo $text_request; ?></option>
                      <option value="<?php echo $text_approve; ?>" selected="selected"><?php echo $text_approve; ?></option>
                      <option value="<?php echo $text_reject; ?>"><?php echo $text_reject; ?></option>
                    <?php } else { ?>
                    <<option value="<?php echo $text_request; ?>"><?php echo $text_request; ?></option>
                    <option value="<?php echo $text_approve; ?>"><?php echo $text_approve; ?></option>
                    <option value="<?php echo $text_reject; ?>" selected="selected"><?php echo $text_reject; ?></option>
                    <?php } ?>
                  </select>
                </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('select[name=\'type\']').on('change', function() {
	if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image') {
		$('#option-value').show();
	} else {
		$('#option-value').hide();
	}
});


var option_value_row = <?php echo $option_value_row; ?>;



$('input[name=\'sort_order\']').keypress(function (e){
	var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});

//--></script></div>
<?php echo $footer; ?>