<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!--<button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button>-->
	<button type="submit" form="myForm" formaction="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></button>
        <!--<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>-->
		</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
         <div class="well">
	 <form method="post" id="myForm" enctype="multipart/form-data" action="<?php echo $download_report; ?>">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-merchant-id"><?php echo $entry_merchant_id; ?></label>
                <input type="text" name="filter_merchant_id" value="<?php echo $filter_merchant_id; ?>" placeholder="<?php echo $entry_merchant_id; ?>" id="input-order-id" class="number form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-nama"><?php echo $entry_merchant_nama; ?></label>
                <input type="text" name="filter_nama" value="<?php echo $filter_nama; ?>" placeholder="<?php echo $entry_merchant_nama; ?>" id="input-customer" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-badan-usaha"><?php echo $entry_merchant_badan_usaha; ?></label>
                <input type="text" name="filter_badan_usaha" value="<?php echo $filter_badan_usaha; ?>" placeholder="<?php echo $entry_merchant_badan_usaha; ?>" id="input-badan-usaha" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-kategori_produk"><?php echo $entry_merchant_kategori_produk; ?></label>
                <input type="text" name="filter_kategori_produk" value="<?php echo $filter_kategori_produk; ?>" placeholder="<?php echo $entry_merchant_kategori_produk; ?>" id="input-kategori_produk" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-website-toko"><?php echo $entry_merchant_website_toko; ?></label>
                <input type="text" name="filter_website_toko" value="<?php echo $filter_website_toko; ?>" placeholder="<?php echo $entry_merchant_website_toko; ?>" id="input-website-toko" class="form-control" />
              </div>
              <!-- <div class="form-group">
                <label class="control-label" for="input-npwp"><?php echo $entry_merchant_npwp; ?></label>
                <input type="text" name="filter_npwp" value="<?php echo $filter_npwp; ?>" placeholder="<?php echo $entry_merchant_npwp; ?>" id="input-npwp" class="form-control" />
              </div> -->
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
	 </form>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left"><?php if ($sort == 'merchant_id') { ?>
                    <a href="<?php echo $sort_merchant; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_merchant; ?>"><?php echo $column_merchant_id; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'nama') { ?>
                    <a href="<?php echo $sort_nama; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_nama; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_nama; ?>"><?php echo $column_merchant_nama; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'badan_usaha') { ?>
                    <a href="<?php echo $sort_badan_usaha; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_badan_usaha; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_badan_usaha; ?>"><?php echo $column_merchant_badan_usaha; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'email') { ?>
                    <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_email; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_email; ?>"><?php echo $column_merchant_email; ?></a>
                    <?php } ?>
                  </td>
		  <td><?php echo $column_merchant_telephone; ?></td>
                  <td class="text-left"><?php if ($sort == 'kategori_produk') { ?>
                    <a href="<?php echo $sort_kategori_produk; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_kategori_produk; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_kategori_produk; ?>"><?php echo $column_merchant_kategori_produk; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'website_toko') { ?>
                    <a href="<?php echo $sort_website_toko; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_website_toko; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_website_toko; ?>"><?php echo $column_merchant_website_toko; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'npwp') { ?>
                    <a href="<?php echo $sort_npwp; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_npwp; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_npwp; ?>"><?php echo $column_merchant_npwp; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'state') { ?>
                    <a href="<?php echo $sort_state; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_state; ?>"><?php echo $column_merchant_status; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'created_at') { ?>
                    <a href="<?php echo $sort_created_at; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_created_at; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_created_at; ?>"><?php echo $column_merchant_created_at; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'updated_at') { ?>
                    <a href="<?php echo $sort_updated_at; ?>" class="<?php echo strtolower($merchants); ?>"><?php echo $column_merchant_updated_at; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_updated_at; ?>"><?php echo $column_merchant_updated_at; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $column_merchant_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($merchant) { ?>
                <?php foreach ($merchant as $merchants) { ?>
                <tr>
                  <td class="text-center"><?php echo $merchants['merchant_id']; ?></td>
                  <td class="text-left"><?php echo $merchants['nama']; ?></td>
                  
                  <td class="text-left"><?php echo $merchants['badan_usaha']; ?></td>
                  <td class="text-left"><?php echo $merchants['email']; ?></td>
		  <td class="text-left"><?php echo $merchants['no_telp']; ?></td>
                  <td class="text-left"><?php echo $merchants['kategori_produk']; ?></td>
                  <td class="text-left"><?php echo $merchants['website_toko']; ?></td>
                  <td class="text-left"><?php echo $merchants['npwp']; ?></td>
                  <td class="text-left"><?php echo $merchants['state'];?></td>
                  <td class="text-left"><?php echo $merchants['created_at'];?></td>
                  <td class="text-left"><?php echo $merchants['updated_at'];?></td>
                  <td class="text-center">
                    <!-- <a href="<?php echo $merchants['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>  -->
                    <a href="<?php echo $merchants['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  </td> 
                </tr>
        <!-- <?php if(sizeof($order['order_detail'])>1){?>
        <?php array_splice($order['order_detail'], 0, 1);?>
        
        <?php }?> -->
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/customer/merchant&token=<?php echo $token; ?>';
	
	var filter_merchant_id = $('input[name=\'filter_merchant_id\']').val();
	if (filter_merchant_id) {
		url += '&filter_merchant_id=' + encodeURIComponent(filter_merchant_id);
	}
	
	var filter_nama = $('input[name=\'filter_nama\']').val();
	
	if (filter_nama) {
		url += '&filter_nama=' + encodeURIComponent(filter_nama);
	}
	
	var filter_badan_usaha = $('input[name=\'filter_badan_usaha\']').val();
  
  if (filter_badan_usaha) {
    url += '&filter_badan_usaha=' + encodeURIComponent(filter_badan_usaha);
  }

  var filter_kategori_produk = $('input[name=\'filter_kategori_produk\']').val();
  
  if (filter_kategori_produk) {
    url += '&filter_kategori_produk=' + encodeURIComponent(filter_kategori_produk);
  }

  var filter_website_toko = $('input[name=\'filter_website_toko\']').val();
  
  if (filter_website_toko) {
    url += '&filter_website_toko=' + encodeURIComponent(filter_website_toko);
  }

  var filter_npwp = $('input[name=\'filter_npwp\']').val();
  
  if (filter_npwp) {
    url += '&filter_npwp=' + encodeURIComponent(filter_npwp);
  }
				
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}	
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);
	
	var selected = $('input[name^=\'selected\']:checked');
	
	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}
	
	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);
			
			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

$('a[id^=\'button-delete\']').on('click', function(e) {
	e.preventDefault();
	
	if (confirm('<?php echo $text_delete; ?>')) {
		location = $(this).attr('href');
	}
});
//--></script> 
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

//--></script>
</div>
<?php echo $footer; ?>
