<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title5; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
			<div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_merchant; ?></label>
					<select class="form-control" value="" id="batch" name="filter_seller">
					<option value="" selected="selected"></option>
						<?php foreach($sellers as $seller){ ?>
						<?php if ($seller['seller_id'] == $filter_seller) { ?>
							<option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $seller['seller_id']; ?>" ><?php echo $seller['nickname']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
			  <div class="form-group">
				<label class="control-label" for="input-merchant"></label>
					<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
			  </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
				<td><?php echo $column_merchant; ?></td>
                <td><?php echo $column_penalty; ?></td>
				<td width="10%"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($invoices){ ?>
				<?php foreach($invoices as $invoice){ ?>
					<tr>
						<td class="text-center"><?php echo $invoice['nickname']; ?></td>
						<td class="text-right"><?php echo $invoice['penalty']; ?></td>
						<td style="text-align: center;">
						<a data-sellerid="<?php echo $invoice['seller_id']; ?>" data-toggle="tooltip" title="<?php echo $button_view_detail; ?>" id="<?php echo $invoice['seller_id']; ?>view" class="btn btn-danger link_view_detail"><i class="fa fa-eye"></i></a> 
						<a data-sellerid="<?php echo $invoice['seller_id']; ?>" id="<?php echo $invoice['seller_id']; ?>download" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a></td>
					</tr>	

			<script type="text/javascript">
				$('#<?php echo $invoice['seller_id']; ?>view').on('click', function(){
					$('#<?php echo $invoice['seller_id']; ?>view').attr('href', '<?php echo $view_detail; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid'));
				});
				$('#<?php echo $invoice['seller_id']; ?>download').on('click', function(){
					$('#<?php echo $invoice['seller_id']; ?>download').attr('href', '<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid'));
				});
			</script> 			
			
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="3" class="text-center"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/penalty&token=<?php echo $token; ?>';
	
	var filter_seller = $('select[name=\'filter_seller\']').val();

	if (filter_seller) {
		url += '&filter_seller=' + encodeURIComponent(filter_seller);
	}

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>