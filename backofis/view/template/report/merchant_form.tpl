<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	<div class="pull-right">
		<a href="<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id=<?php echo $seller_id; ?>&period=<?php echo $period; ?>&year=<?php echo $year; ?>" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_download; ?>" ><i class="fa fa-download"></i></a>
		<a href="<?php echo $link_back; ?>" class="btn btn-default"  data-toggle="tooltip" title="<?php echo $button_back; ?>"><i class="fa fa-reply"></i></a>
	</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i><?php echo $heading_title; ?> <?php echo $nickname; ?></h3>
      </div>
      <div class="panel-body">
		
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group required">
					<?php if($period == 1){ ?>
						<label class="control-label"><?php echo $text_period; ?>: <?php echo $text_month_first; ?></label> 
					<?php }else if($period == 2){ ?>
						<label class="control-label"><?php echo $text_period; ?>: <?php echo $text_month_second; ?></label>
					<?php } ?>
				</div>
				<div class="form-group required">
					<label class="control-label"><?php echo $column_total; ?>: <?php echo $total_order; ?> <?php echo $text_order; ?></label>
				</div>
            </div>
          </div>
        
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td width="5%"><?php echo $column_number; ?></td>
				<td><?php echo $column_criteria; ?></td>
				<td><?php echo $column_weight; ?></td>
				<td><?php echo $column_score_view; ?></td>
				<td><?php echo $column_final_score; ?></td>
              </tr>
            </thead>
            <tbody>
				<tr style="text-align: center;">
					<td>1</td>
					<td class="text-left"><?php echo $criteria_1; ?></td>
					<td><?php echo $weight1; ?>%</td>
					<td><?php echo $score_approve_merchant; ?></td>
					<td><?php echo $final_approve_merchant; ?></td>
				  </tr>
				  <tr style="text-align: center;">
					<td>2</td>
					<td class="text-left"><?php echo $criteria_2; ?></td>
					<td><?php echo $weight2; ?>%</td>
					<td><?php echo $score_approve_sla; ?></td>
					<td><?php echo $final_approve_sla; ?></td>
				  </tr>
				  <tr style="text-align: center;">
					<td>3</td>
					<td class="text-left"><?php echo $criteria_3; ?></td>
					<td><?php echo $weight3; ?>%</td>
					<td><?php echo $score_delivered_sla; ?></td>
					<td><?php echo $final_delivered_sla; ?></td>
				  </tr>
				  <tr style="text-align: center;">
					<td>4</td>
					<td class="text-left"><?php echo $criteria_4; ?></td>
					<td><?php echo $weight4; ?>%</td>
					<td><?php echo $score_review; ?></td>
					<td><?php echo $final_review; ?></td>
				  </tr>
				  <tr style="text-align: center;">
					<td>5</td>
					<td class="text-left"><?php echo $criteria_5; ?></td>
					<td><?php echo $weight5; ?>%</td>
					<td><?php echo $score_review_accuracy; ?></td>
					<td><?php echo $final_review_accuracy; ?></td>
				  </tr>
				  <tr style="text-align: center;">
					<td>6</td>
					<td class="text-left"><?php echo $criteria_6; ?></td>
					<td><?php echo $weight6; ?>%</td>
					<td><?php echo $score_complain; ?></td>
					<td><?php echo $final_complain; ?></td>
				  </tr>
				  <tr>
					<td colspan="2" style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: center;"><?php echo $column_total_score; ?></td>
					<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: center;"><?php echo $column_hundred; ?></td>
					<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: center;"><?php echo $total_score; ?></td>
					<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: center;"><?php echo $final_score; ?></td>
				  </tr>
            </tbody>
          </table>
        </div>
       <div class="row">
         
        </div>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>