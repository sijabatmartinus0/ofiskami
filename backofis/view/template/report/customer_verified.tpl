<?php echo $header; ?><?php echo $column_left; 
$count = 1;
?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="myForm" formaction="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
         <div class="well">
          <form method="post" id="myForm" enctype="multipart/form-data" action="<?php echo $download_report; ?>">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-customer-nama"><?php echo $entry_customer_nama; ?></label>
                <input type="text" name="filter_nama" value="<?php echo $filter_nama; ?>" placeholder="<?php echo $entry_customer_nama; ?>" id="input-customer-nama" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_start_date; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_end_date; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
          </form>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td><?php echo $column_customer_no; ?></td>
                  <td class="text-left"><?php if ($sort == 'nama') { ?>
                    <a href="<?php echo $nama; ?>" class="<?php echo strtolower($customers); ?>"><?php echo $column_customer_nama; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $nama; ?>"><?php echo $column_customer_nama; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'email') { ?>
                    <a href="<?php echo $email; ?>" class="<?php echo strtolower($customers); ?>"><?php echo $column_customer_email; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $email; ?>"><?php echo $column_customer_email; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'telephone') { ?>
                    <a href="<?php echo $telephone; ?>" class="<?php echo strtolower($customers); ?>"><?php echo $column_customer_telephone; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $telephone; ?>"><?php echo $column_customer_telephone; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'date_added') { ?>
                    <a href="<?php echo $date_added; ?>" class="<?php echo strtolower($customers); ?>"><?php echo $column_customer_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $date_added; ?>"><?php echo $column_customer_date_added; ?></a>
                    <?php } ?>
                  </td>
                </tr>
              </thead>
              <tbody>
              	<?php if ($customer) { ?>
                <?php foreach ($customer as $customers) { ?>
                <tr>
                  <td class="text-center"><?php echo $count++; ?></td>
                  <td class="text-left"><?php echo $customers['nama']; ?></td>
                  <td class="text-left"><?php 
                    if($customers['email']) {
                      echo $customers['email'];
                    } else{
                      echo '-';
                    } ?>
                  </td>
                  <td class="text-left"><?php 
                    if($customers['telephone']) {
                      echo $customers['telephone'];
                    } else{
                      echo '-';
                    } ?>
                  </td>
                  <td class="text-left"><?php echo $customers['date_added']; ?></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="14"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/customer_activity/customer&token=<?php echo $token; ?>';
	var filter_nama = $('input[name=\'filter_nama\']').val();
	
	if (filter_nama) {
		url += '&filter_nama=' + encodeURIComponent(filter_nama);
	}
	
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}
	
	var filter_date_end = $('input[name=\'filter_date_end\']').val();
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

				
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}	
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);
	
	var selected = $('input[name^=\'selected\']:checked');
	
	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}
	
	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);
			
			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

$('a[id^=\'button-delete\']').on('click', function(e) {
	e.preventDefault();
	
	if (confirm('<?php echo $text_delete; ?>')) {
		location = $(this).attr('href');
	}
});
//--></script> 
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

//--></script>
</div>
<?php echo $footer; ?>
