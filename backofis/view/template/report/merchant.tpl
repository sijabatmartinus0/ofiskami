<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" for="input-period"><?php echo $text_period; ?></label>
					<select name="filter_period" id="input-period" class="form-control">
						<?php 
							if ($filter_period == 1){
						?>			
							<option value="1" selected><?php echo $text_month_first; ?></option>
							<option value="2"><?php echo $text_month_second; ?></option>
						<?php
							}else if($filter_period == 2){ ?>
							<option value="1"><?php echo $text_month_first; ?></option>
							<option value="2" selected><?php echo $text_month_second; ?></option>
						<?php	
							}
						?>
					  
					</select>
				</div>
				<div class="form-group">
					<label class="control-label" for="input-year"><?php echo $text_year; ?></label>
					<select name="filter_year" id="input-year" class="form-control">
						<?php 
							for($x=2014; $x <= (int)date("Y"); $x++){
								if ($x == $filter_year){
						?>			
						<option value="<?php echo $x; ?>" selected ><?php echo $x; ?></option>
						<?php
							}else{ ?>
								<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
						<?php	
								}
							}
						?>
					</select>
				</div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_merchant; ?></label>
                <input type="text" name="filter_merchant" placeholder="<?php echo $text_merchant; ?>" value="<?php echo $filter_merchant; ?>" id="input-merchant" class="form-control" />
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td><?php echo $column_merchant; ?></td>
                <td><?php echo $column_total; ?></td>
                <td><?php echo $column_score; ?></td>
				<td width="10%"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
				<?php foreach($merchants as $merchant){ ?>
					<tr>
						<td class="text-left"><?php echo $merchant['nickname']; ?></td>
						<td style="text-align: center;"><?php echo $merchant['total_order']; ?></td>
						<?php if($merchant['score'] == 0){ ?>
							<td style="text-align: center;">-</td>
						<?php }else{ ?>
							<td style="text-align: center;"><?php echo $merchant['score']; ?></td>
						<?php } ?>
						<td style="text-align: center;"><a data-sellerid="<?php echo $merchant['seller_id']; ?>" data-toggle="tooltip" title="<?php echo $button_view_detail; ?>" id="<?php echo $merchant['seller_id']; ?>view" class="btn btn-danger link_view_detail"><i class="fa fa-eye"></i></a> <a data-sellerid="<?php echo $merchant['seller_id']; ?>" id="<?php echo $merchant['seller_id']; ?>download" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a></td>
					</tr>	

			<script type="text/javascript">
				$('#<?php echo $merchant['seller_id']; ?>view').on('click', function(){
					$('#<?php echo $merchant['seller_id']; ?>view').attr('href', '<?php echo $view_detail; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid')+'&period='+$('#input-period').val()+'&year='+$('#input-year').val());
				});
				$('#<?php echo $merchant['seller_id']; ?>download').on('click', function(){
					$('#<?php echo $merchant['seller_id']; ?>download').attr('href', '<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid')+'&period='+$('#input-period').val()+'&year='+$('#input-year').val());
				});
			</script> 			
			
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/merchant&token=<?php echo $token; ?>';
	
	var filter_period = $('select[name=\'filter_period\']').val();
	
	if (filter_period) {
		url += '&filter_period=' + encodeURIComponent(filter_period);
	}

	var filter_year = $('select[name=\'filter_year\']').val();
	
	if (filter_year) {
		url += '&filter_year=' + encodeURIComponent(filter_year);
	}
	
	var filter_merchant = $('input[name=\'filter_merchant\']').val();
	
	if (filter_merchant) {
		url += '&filter_merchant=' + encodeURIComponent(filter_merchant);
	}	

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>