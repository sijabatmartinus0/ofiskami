<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	<div class="pull-right">
		<?php if($is_created==false){ ?>
			<a data-sellerid="<?php echo $seller_id; ?>" data-batchid="<?php echo $batch_id; ?>" id="<?php echo $seller_id; ?>create" data-toggle="tooltip" title="<?php echo $button_created; ?>" class="btn btn-info"><i class="fa fa-file-text"></i></a>
		<?php } ?>
		<script type="text/javascript">
				<?php if($is_created==false){ ?>
				$('#<?php echo $seller_id; ?>create').on('click', function(){
					$('#<?php echo $seller_id; ?>create').attr('href', '<?php echo $billing; ?>&token=<?php echo $token; ?>&created=true&seller_id='+$(this).data('sellerid')+'&batch_id='+$(this).data('batchid'));
				});
				<?php } ?>
		</script>
		<a href="<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id=<?php echo $seller_id; ?>&batch_id=<?php echo $batch_id; ?>" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_download; ?>" ><i class="fa fa-download"></i></a>
		<a href="<?php echo $link_back; ?>" class="btn btn-default"  data-toggle="tooltip" title="<?php echo $button_back; ?>"><i class="fa fa-reply"></i></a>
	</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i><?php echo $heading_title; ?> <?php echo $nickname; ?></h3>
      </div>
      <div class="panel-body">
		
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group required">
					
				</div>
				<div class="form-group required">
					
				</div>
            </div>
          </div>
        
        <div class="table-responsive responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td width="5%"><?php echo $column_order_no; ?></td>
				<td><?php echo $column_vendor_name; ?></td>
				<td><?php echo $column_tax_type; ?></td>
				<td><?php echo $column_kiosk_id; ?></td>
				<td><?php echo $column_payment_type; ?></td>
				<td><?php echo $column_transaction_date; ?></td>
				<td><?php echo $column_quantity; ?></td>
				<td><?php echo $column_sku; ?></td>
				<td><?php echo $column_code; ?></td>
				<td><?php echo $column_price; ?></td>
				<td><?php echo $column_shipping; ?></td>
				<td><?php echo $column_disc_ofiskita; ?></td>
				<td><?php echo $column_disc_merchant; ?></td>
				<td><?php echo $column_total_report; ?></td>
				<td><?php echo $column_commission_fee; ?></td>
				<td><?php echo $column_transaction_fee; ?></td>
				<td><?php echo $column_vendor_balance; ?></td>
            </tr>
            </thead>
            <tbody>
			<?php if($details){ ?>
				<?php foreach($details as $detail){ ?>
				<tr>
					<td class="text-center"><?php echo $detail['invoice']; ?></td>
					<td class="text-center"><?php echo $detail['nickname']; ?></td>
					<td class="text-center"><?php echo $detail['tax_type']; ?></td>
					<td class="text-center" class="text-center"><?php if($detail['kiosk_name']){ echo $detail['kiosk_name']; }else{ echo "-";} ?></td>
					<td class="text-center"><?php echo $detail['payment_type']; ?></td>
					<td class="text-center"><?php echo $detail['transaction_date']; ?></td>
					<td class="text-center"><?php echo $detail['quantity']; ?></td>
					<td class="text-center"><?php echo $detail['sku']; ?></td>
					<td class="text-center"><?php echo $detail['code']; ?></td>
					<td class="text-right"><?php echo $detail['price']; ?></td>
					<td class="text-right"><?php echo $detail['shipping_fee']; ?></td>
					<td class="text-right"><?php echo $detail['discount_ofiskita']; ?></td>
					<td class="text-right"><?php echo $detail['discount_merchant']; ?></td>
					<td class="text-right"><?php echo $detail['total']; ?></td>
					<td class="text-right"><?php echo $detail['store_commission']; ?></td>
					<td class="text-right"><?php echo $detail['online_payment_fee']; ?></td>
					<td class="text-right"><?php echo $detail['vendor_balance']; ?></td>
				</tr>
				<?php } ?>
				<!--<tr>
					<td colspan="14" style="font-weight: bold; font-size: 14px;" class="text-center"><?php echo strtoupper($column_total_report); ?></td>
					<td class="text-right"><?php echo $total_store_commission; ?></td>
					<td class="text-right"><?php echo $total_store_commission_tax; ?></td>
				<tr>-->
			<?php }else { ?>
				<tr>
					<td colspan="6" class="text-center"><?php echo $text_no_results; ?><td>
				</tr>
			<?php } ?>
            </tbody>
          </table>
        </div>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>