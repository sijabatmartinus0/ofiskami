<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title3; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
			<div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_status; ?></label>
					<select class="form-control" value="" id="status" name="filter_status">
					<option value="" selected="selected"></option>
						<?php foreach($statuses as $status){ ?>
						<?php if ($status['order_status_id'] == $filter_status) { ?>
							<option value="<?php echo $status['order_status_id']; ?>" selected="selected"><?php echo $status['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $status['order_status_id']; ?>" ><?php echo $status['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_batch; ?></label>
					<select class="form-control" value="" id="batch" name="filter_batch">
					<option value="" selected="selected"></option>
						<?php foreach($batchs as $batch){ ?>
						<?php if ($batch['batch_id'] == $filter_batch) { ?>
							<option value="<?php echo $batch['batch_id']; ?>" selected="selected"><?php echo $batch['batch_id']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $batch['batch_id']; ?>" ><?php echo $batch['batch_id']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_merchant; ?></label>
					<select class="form-control" value="" id="batch" name="filter_seller">
					<option value="" selected="selected"></option>
						<?php foreach($sellers as $seller){ ?>
						<?php if ($seller['seller_id'] == $filter_seller) { ?>
							<option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $seller['seller_id']; ?>" ><?php echo $seller['nickname']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
			  <div class="form-group">
				<label class="control-label" for="input-merchant"></label>
					<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
					<button type="button" id="button-download" class="btn btn-warning pull-right"><i class="fa fa-download"></i> <?php echo $button_download; ?></button>
            
			  </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td class="fit"><?php echo $column_order_no; ?></td>
				<td class="fit" style="width:300px;"><?php echo $column_vendor_name; ?></td>
				<td class="fit" style="width:200px;"><?php echo $column_relation_type; ?></td>
				<td class="fit"><?php echo $column_tax_type; ?></td>
				<td class="fit"><?php echo $column_kiosk_id; ?></td>
                <td class="fit"><?php echo $column_revenue_type; ?></td>
                <td class="fit"><?php echo $column_payment_type; ?></td>
                <td class="fit"><?php echo $column_bank; ?></td>
                <td class="fit"><?php echo $column_transaction_date; ?></td>
                <td class="fit"><?php echo $column_quantity; ?></td>
                <td class="fit"><?php echo $column_sku; ?></td>
                <td class="fit"><?php echo $column_price; ?></td>
                <td class="fit"><?php echo $column_total_report; ?></td>
                <td class="fit"><?php echo $column_payment_total; ?></td>
                <td class="fit"><?php echo $column_tax; ?></td>
                <td class="fit"><?php echo $column_commission; ?></td>
                <td class="fit"><?php echo $column_shipping; ?></td>
                <td class="fit"><?php echo $column_ofiskita_commission; ?></td>
                <td class="fit"><?php echo $column_tax_ppn; ?></td>
                <td class="fit"><?php echo $column_status; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($invoices){ ?>
				<?php foreach($invoices as $invoice){ ?>
					<tr>
						<td class="text-center fit"><?php echo $invoice['invoice']; ?></td>
						<td class="text-center fit"><?php echo $invoice['nickname']; ?></td>
						<td class="text-center fit"><?php echo $invoice['relation_type']; ?></td>
						<td class="text-center fit"><?php echo $invoice['tax_type']; ?></td>
						<td class="text-center fit"><?php echo $invoice['kiosk_name']; ?></td>
						<td class="text-center fit"><?php echo $invoice['revenue_type']; ?></td>
						<td class="text-center fit"><?php echo $invoice['payment_type']; ?></td>
						<td class="text-center fit"><?php echo $invoice['account_name']; ?></td>
						<td class="text-center fit"><?php echo $invoice['date_added']; ?></td>
						<td class="text-center fit"><?php echo $invoice['quantity']; ?></td>
						<td class="text-left fit"><?php echo $invoice['sku']; ?></td>
						<td class="text-right fit"><?php echo $invoice['price']; ?></td>
						<td class="text-right fit"><?php echo $invoice['total']; ?></td>
						<td class="text-right fit"><?php echo $invoice['payment_total']; ?></td>
						<td class="text-right fit"><?php echo $invoice['tax']; ?></td>
						<td class="text-right fit"><?php echo $invoice['commission_base']; ?></td>
						<td class="text-right fit"><?php echo $invoice['shipping_fee']; ?></td>
						<td class="text-right fit"><?php echo $invoice['store_commission']; ?></td>
						<td class="text-right fit"><?php echo $invoice['store_commission_tax']; ?></td>
						<td class="text-center fit"><?php echo $invoice['status']; ?></td>
					</tr>		
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="19" class="text-center"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/transaction&token=<?php echo $token; ?>';
	
	var filter_batch = $('select[name=\'filter_batch\']').val();
	var filter_seller = $('select[name=\'filter_seller\']').val();
	var filter_status = $('select[name=\'filter_status\']').val();
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_batch) {
		url += '&filter_batch=' + encodeURIComponent(filter_batch);
	}	
	
	if (filter_seller) {
		url += '&filter_seller=' + encodeURIComponent(filter_seller);
	}
	
	if (filter_status) {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	location = url;
});
$('#button-download').on('click', function() {
	url = 'index.php?route=report/transaction/download_report&token=<?php echo $token; ?>';
	
	var filter_batch = $('select[name=\'filter_batch\']').val();
	var filter_seller = $('select[name=\'filter_seller\']').val();
	var filter_status = $('select[name=\'filter_status\']').val();
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_batch) {
		url += '&filter_batch=' + encodeURIComponent(filter_batch);
	}	
	
	if (filter_seller) {
		url += '&filter_seller=' + encodeURIComponent(filter_seller);
	}
	
	if (filter_status) {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>