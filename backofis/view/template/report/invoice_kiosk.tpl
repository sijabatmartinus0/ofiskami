<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" for="input-period"><?php echo $text_kiosk; ?></label>
					<select class="form-control" value="" id="filter_kiosk" name="filter_kiosk">
					<option value="" selected="selected"></option>
						<?php foreach($kiosks as $kiosk){ ?>
							<?php if ($kiosk['pp_branch_id'] == $filter_kiosk) { ?>
								<option value="<?php echo $kiosk['pp_branch_id']; ?>" selected="selected"><?php echo $kiosk['pp_branch_name']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $kiosk['pp_branch_id']; ?>"><?php echo $kiosk['pp_branch_name']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_batch; ?></label>
                <select class="form-control" value="" id="batch" name="filter_batch">
					<option value="" selected="selected"></option>
						<?php foreach($batchs as $batch){ ?>
						<?php if ($batch['batch_id'] == $filter_batch) { ?>
							<option value="<?php echo $batch['batch_id']; ?>" selected="selected"><?php echo $batch['batch_id']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $batch['batch_id']; ?>" ><?php echo $batch['batch_id']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td><?php echo $column_batch; ?></td>
				<td><?php echo $column_kiosk; ?></td>
				<td><?php echo $column_invoice; ?></td>
				<td><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($invoices){ ?>
				<?php foreach($invoices as $invoice){ ?>
					<tr>
						<td style="text-align: center;"><?php echo $invoice['batch_id']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['kiosk_name']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['invoice']; ?></td>
						<td style="text-align: center;">
						<a data-saleid="<?php echo $invoice['sale_id']; ?>" data-kioskid="<?php echo $invoice['kiosk_id']; ?>" data-toggle="tooltip" title="<?php echo $button_print; ?>" id="<?php echo $invoice['batch_id']."-".$invoice['sale_id']."-".$invoice['kiosk_id']; ?>print" class="btn btn-danger link_view_detail"><i class="fa fa-print"></i></a> 
						</td>
					</tr>	

			<script type="text/javascript">
				$('#<?php echo $invoice['batch_id']."-".$invoice['sale_id']."-".$invoice['kiosk_id']; ?>print').on('click', function(){
					$('#<?php echo $invoice['batch_id']."-".$invoice['sale_id']."-".$invoice['kiosk_id']; ?>print').attr('href', '<?php echo $print; ?>&token=<?php echo $token; ?>&sale_id='+$(this).data('saleid')+'&kiosk_id='+$(this).data('kioskid'));
				});
			</script> 			
			
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="6" class="text-center"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/invoice_kiosk&token=<?php echo $token; ?>';

	var filter_batch = $('select[name=\'filter_batch\']').val();
	
	if (filter_batch) {
		url += '&filter_batch=' + encodeURIComponent(filter_batch);
	}

	var filter_kiosk = $('select[name=\'filter_kiosk\']').val();
	
	if (filter_kiosk) {
		url += '&filter_kiosk=' + encodeURIComponent(filter_kiosk);
	}	

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>