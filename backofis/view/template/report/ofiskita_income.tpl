<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title3; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_batch; ?></label>
					<select class="form-control" value="" id="batch" name="filter_batch">
					<option value="" selected="selected"></option>
						<?php foreach($batchs as $batch){ ?>
						<?php if ($batch['batch_id'] == $filter_batch) { ?>
							<option value="<?php echo $batch['batch_id']; ?>" selected="selected"><?php echo $batch['batch_id']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $batch['batch_id']; ?>" ><?php echo $batch['batch_id']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_merchant; ?></label>
					<select class="form-control" value="" id="batch" name="filter_seller">
					<option value="" selected="selected"></option>
						<?php foreach($sellers as $seller){ ?>
						<?php if ($seller['seller_id'] == $filter_seller) { ?>
							<option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $seller['seller_id']; ?>" ><?php echo $seller['nickname']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
			  <div class="form-group">
				<label class="control-label" for="input-merchant"></label>
					<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
			  </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td><?php echo $column_batch; ?></td>
				<td><?php echo $column_merchant; ?></td>
                <td><?php echo $column_ofiskita_commission; ?></td>
                <td><?php echo $column_tax; ?></td>
                <td><?php echo $column_online_payment; ?></td>
				<td width="10%"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($invoices){ ?>
				<?php foreach($invoices as $invoice){ ?>
					<tr>
						<td class="text-center"><?php echo $invoice['batch_id']; ?></td>
						<td class="text-center"><?php echo $invoice['nickname']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['store_commission']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['tax']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['online_payment_fee']; ?></td>
						<td style="text-align: center;">
						<a  data-batchid="<?php echo $invoice['batch_id']; ?>" data-sellerid="<?php echo $invoice['seller_id']; ?>" data-toggle="tooltip" title="<?php echo $button_view_detail; ?>" id="<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>view" class="btn btn-danger link_view_detail"><i class="fa fa-eye"></i></a> 
						<a data-batchid="<?php echo $invoice['batch_id']; ?>" data-sellerid="<?php echo $invoice['seller_id']; ?>" id="<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>download" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a></td>
					</tr>	

			<script type="text/javascript">
				$('#<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>view').on('click', function(){
					$('#<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>view').attr('href', '<?php echo $view_detail; ?>&token=<?php echo $token; ?>&batch_id='+$(this).data('batchid')+'&seller_id='+$(this).data('sellerid'));
				});
				$('#<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>download').on('click', function(){
					$('#<?php echo $invoice['batch_id'].$invoice['seller_id']; ?>download').attr('href', '<?php echo $download_report; ?>&token=<?php echo $token; ?>&batch_id='+$(this).data('batchid')+'&seller_id='+$(this).data('sellerid'));
				});
			</script> 			
			
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="6" class="text-center"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/ofiskita_income&token=<?php echo $token; ?>';
	
	var filter_batch = $('select[name=\'filter_batch\']').val();
	var filter_seller = $('select[name=\'filter_seller\']').val();
	
	if (filter_batch) {
		url += '&filter_batch=' + encodeURIComponent(filter_batch);
	}	
	
	if (filter_seller) {
		url += '&filter_seller=' + encodeURIComponent(filter_seller);
	}

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>