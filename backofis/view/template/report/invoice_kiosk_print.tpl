<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="receipt_wrapper">
	<div id="receipt_header">
		<div id="company_name" style="font-size: 150%;
    font-weight: bold;"><img id="image" src="data:image/jpg;base64,<?php echo $image;?>" style="max-width: 150px;
    max-height: 150px;" alt="company_logo" /></div>
		<div id="company_address">Alamat</div>
		<div id="company_phone">021 000 0000</div>
	</div>
	<div id="receipt_general_info" style="padding: 10px 0px 0px 0px;border-top: 1px solid #E0E0E0;border-bottom: 1px solid #E0E0E0;">
		<div class="employee-info" style="width: 100%;height: 20px;position: relative;">
		<div id="employee" style="left: 0;position: absolute;">Rizki</div>
		<div id="sale_time" style="right: 0;position: absolute;margin-bottom: 5px;">16/09/2016 15:40</div>
		</div>
	</div>

	<table id="receipt_items" style="border:none;position: relative;
    border-collapse: collapse;
    margin-top: 15px;
    margin-bottom: 15px;
    width: 100%;">
	<?php
	//foreach(array_reverse($cart, true) as $line=>$item)
	//{
	?>
		<tr>
		<td colspan="4" style="width:100%;font-weight:bold">Deli Pencil</th>
		</tr>
		<tr>
		<td colspan="4" style="width:100%;">Invoice #INV/KIOSK/0000011</th>
		</tr>
		<tr>
		<td colspan="4" style="width:100%;">Seller : AXI - 00112233</th>
		</tr>
		<tr>
			<td style="width:50%;">Qty. 10.00</td>
			<td style="width:10%;">@</td>
			<td style="width:20%;"><?php echo $this->currency->format(12000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
			<td style="width:20%;text-align:right;"><?php echo $this->currency->format(120000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
		</tr>
		<tr>
			<td style="width:50%;">Discount</td>
			<td style="width:10%;">&nbsp;</td>
			<td style="width:20%;"><?php echo $this->currency->format(0, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
			<td style="width:20%;">&nbsp;</td>
		</tr>
	    <tr>
	    <td colspan="2">Deli pencil buat nulis gans</td>
		<td >10100011</td>
	    </tr>
	    <?php //if ($item['discount'] > 0) : ?>
		<tr>
			<td colspan="3" class="discount"> 0% Discount</td><td class="total-value"><?php echo $this->currency->format(120000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
		</tr>
		<?php //endif; ?>

	<?php
	//}
	?>
	
	<?php //if ($this->Appconfig->get('show_total_discount') && $discount > 0): ?> 
	<tr>
	<td colspan="3" style='text-align:right;border-top:1px solid #E0E0E0;'>Sub Total:</td>
	<td style='text-align:right;border-top:1px solid #E0E0E0;'><?php echo $this->currency->format(120000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>
	<tr>
		<td colspan="3" class="total-value">Disc %:</td>
		<td class="total-value"><?php echo $this->currency->format(0, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>
	<?php //endif; ?>
	<tr>
	</tr>
	<?php $border = true;//(!$this->Appconfig->get('receipt_show_taxes') && !($this->Appconfig->get('show_total_discount') && $discount > 0)); ?> 
	<?php //if(!empty($voucher)) { ?>
	<tr>
	<td colspan="3" style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right;'>Voucher(AGHALPELNAS)</td>
	<td style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right'><?php echo $this->currency->format(10000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>
	<?php //}?>
	<?php //if(!empty($coupon)) { ?>
	<tr>
	<td colspan="3" style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right;'>Coupon(AGHARPELNAS)</td>
	<td style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right'><?php echo $this->currency->format(10000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>
	<?php //}?>
	<tr>
	<td colspan="3" style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right;'>Total</td>
	<td style='<?php echo $border? 'border-top: 1px solid #E0E0E0;' :''; ?>text-align:right'><?php echo $this->currency->format(100000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>

	<?php
	/*$only_sale_check = TRUE;
	$show_giftcard_remainder = FALSE;
	foreach($payments as $payment_id=>$payment)
	{ 
		$only_sale_check &= $payment['payment_type'] == $this->lang->line('sales_check');
		$splitpayment=explode(':',$payment['payment_type']);
		$show_giftcard_remainder |= $splitpayment[0] == $this->lang->line('sales_giftcard');*/
  		?>
		<tr>
		<td colspan="3" style="text-align:right;">Cash </td>
		<td style="text-align:right"><div class="total-value" style="text-align:right"><?php echo $this->currency->format(100000, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></div></td>
	    </tr>
	<?php
	//}
	?>

	<tr>
		<td colspan="3" style='text-align:right;'> Change Due </td>
		<td style='text-align:right'><?php echo $this->currency->format(0, $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())); ?></td>
	</tr>

	</table>

	<div id="sale_return_policy" style="width: 80%;margin: 0 auto;text-align: center;">
		Return Policy
	</div>
	<div id="barcode" style="margin-top: 10px;
    text-align: center;">
		<br>
		121
	</div>
</div>
</body>
</html>