<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<base href="<?php echo $base; ?>" />
<style type="text/css"> 
html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    color: #666666;
    font-weight: 500;
    line-height: 18px;
    text-rendering: optimizeLegibility;
}
.btn-primary {
    color: #FFF;
    background-color: #1E91CF;
    border-color: #1978AB;
}
.btn {
    display: inline-block;
    margin-bottom: 0px;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 8px 13px;
    font-size: 12px;
    line-height: 1.42857;
    border-radius: 3px;
    -moz-user-select: none;
}
.pull-right {
    float: right;
}
.btn:hover {
	background-color:#1978AB;
}
a:link {
    text-decoration: none;
}
</style>
</head>
<body>
<div style="margin: 10px 200px 0px 200px;">
  <div style="page-break-after: always;">
    <center><u><h1><?php echo $heading_title; ?></h1></u></center>
	<div style="margin-top: 20px; margin-bottom: 10px; text-align: right; font-size: 14px;">
		<?php echo $column_merchant; ?>: <b><?php echo $nickname; ?></b>
	</div>
    <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px; text-align: center;">
    <thead>
      <tr>
        <td width="5%" style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_number; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_criteria; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_weight; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_score_view; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_final_score; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">1</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_1; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight1; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_approve_merchant; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_approve_merchant; ?></td>
      </tr>
	  <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">2</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_2; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight2; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_approve_sla; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_approve_sla; ?></td>
      </tr>
	  <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">3</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_3; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight3; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_delivered_sla; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_delivered_sla; ?></td>
      </tr>
	  <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">4</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_4; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight4; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_review; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_review; ?></td>
      </tr>
	  <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">5</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_5; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight5; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_review_accuracy; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_review_accuracy; ?></td>
      </tr>
	  <tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;">6</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $criteria_6; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $weight6; ?>%</td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $score_complain; ?></td>
        <td style="vertical-align: top; font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; text-align: center;"><?php echo $final_complain; ?></td>
      </tr>
	  <tr>
        <td colspan="2" style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_total_score; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $column_hundred; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $total_score; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222; text-align: center;"><?php echo $final_score; ?></td>
      </tr>
    </tbody>
	
  </table>
  
  </div>
	  <div class="pull-right">
	  <a href="<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id=<?php echo $seller_id; ?>&period=<?php echo $period; ?>&year=<?php echo $year; ?>" type="button" id="download_report" class="btn btn-primary pull-right"><?php echo $button_download; ?></a>
	  </div>
</div>
</body>
</html>