<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" for="input-period"><?php echo $text_merchant; ?></label>
					<select class="form-control" value="" id="seller" name="filter_merchant">
					<option value="" selected="selected"></option>
						<?php foreach($sellers as $seller){ ?>
							<?php if ($seller['seller_id'] == $filter_merchant) { ?>
								<option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $seller['seller_id']; ?>"><?php echo $seller['nickname']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-merchant"><?php echo $text_batch; ?></label>
                <select class="form-control" value="" id="batch" name="filter_batch">
					<option value="" selected="selected"></option>
						<?php foreach($batchs as $batch){ ?>
						<?php if ($batch['batch_id'] == $filter_batch) { ?>
							<option value="<?php echo $batch['batch_id']; ?>" selected="selected"><?php echo $batch['batch_id']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $batch['batch_id']; ?>" ><?php echo $batch['batch_id']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td><?php echo $column_batch; ?></td>
                <td><?php echo $column_merchant; ?></td>
                <td><?php echo $column_ofiskita_commission; ?></td>
                <td><?php echo $column_tax; ?></td>
				<td><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
			<?php if($invoices){ ?>
				<?php foreach($invoices as $invoice){ ?>
					<tr>
						<td class="text-center"><?php echo $invoice['batch']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['merchant']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['store_commission']; ?></td>
						<td style="text-align: center;"><?php echo $invoice['tax']; ?></td>
						<td style="text-align: center;">
						<a data-sellerid="<?php echo $invoice['seller_id']; ?>" data-batchid="<?php echo $invoice['batch_id']; ?>" data-toggle="tooltip" title="<?php echo $button_view_detail; ?>" id="<?php echo $invoice['seller_id'].$invoice['batch']; ?>view" class="btn btn-danger link_view_detail"><i class="fa fa-eye"></i></a> 
						<a data-sellerid="<?php echo $invoice['seller_id']; ?>" data-batchid="<?php echo $invoice['batch_id']; ?>" id="<?php echo $invoice['seller_id'].$invoice['batch']; ?>download" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a>
						<?php if($invoice['is_paid']==false){ ?>
						<a data-sellerid="<?php echo $invoice['seller_id']; ?>" data-batchid="<?php echo $invoice['batch_id']; ?>" id="<?php echo $invoice['seller_id'].$invoice['batch']; ?>pay" data-toggle="tooltip" title="<?php echo $button_paid; ?>" class="btn btn-info"><i class="fa fa-money"></i></a>
						<?php } ?>
						</td>
					</tr>	

			<script type="text/javascript">
				$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>view').on('click', function(){
					$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>view').attr('href', '<?php echo $view_detail; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid')+'&batch_id='+$(this).data('batchid'));
				});
				$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>download').on('click', function(){
					$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>download').attr('href', '<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id='+$(this).data('sellerid')+'&batch_id='+$(this).data('batchid'));
				});
				<?php if($invoice['is_paid']==false){ ?>
				$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>pay').on('click', function(){
					$('#<?php echo $invoice['seller_id'].$invoice['batch']; ?>pay').attr('href', '<?php echo $payout; ?>&token=<?php echo $token; ?>&paid=true&seller_id='+$(this).data('sellerid')+'&batch_id='+$(this).data('batchid'));
				});
				<?php } ?>
			</script> 			
			
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="6" class="text-center"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/ofiskita_payout&token=<?php echo $token; ?>';
	
	var filter_merchant = $('select[name=\'filter_merchant\']').val();
	
	if (filter_merchant) {
		url += '&filter_merchant=' + encodeURIComponent(filter_merchant);
	}
	
	var filter_batch = $('select[name=\'filter_batch\']').val();
	
	if (filter_batch) {
		url += '&filter_batch=' + encodeURIComponent(filter_batch);
	}	

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>