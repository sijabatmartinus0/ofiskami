<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <!--<a href="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a>-->
                <button type="submit" form="myForm" formaction="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <form method="post" id="myForm" enctype="multipart/form-data" action="<?php echo $download_report; ?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-period"><?php echo $entry_merchant; ?></label>
                                    <select class="form-control" value="" id="seller" name="filter_merchant">
                                        <option value="0" selected="selected">All Seller</option>
                                        <?php foreach($sellers as $seller){ ?>
                                            <?php if ($seller['seller_id'] == $filter_merchant) { ?>
                                            <option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $seller['seller_id']; ?>"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr style="text-align: center;">
                                <td><?php echo $column_merchant; ?></td>
                                <!--<td><?php echo $column_date_created; ?></td>-->
                                <td><?php echo $column_penambahan_sku;?></td>
                                <td><?php echo $column_total_sku; ?></td>
                                <!--<td width="10%"><?php echo $column_action; ?></td>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($merchants) { ?>
                            <?php foreach($merchants as $merchant){ ?>
                            <tr>
                                <td class="text-left"><?php echo $merchant['nickname']; ?></td> 
                                <!--<td style="text-align: center;"><?php echo $merchant['date_created']; ?></td>-->
                                <td style="text-align: center;"><?php echo $merchant['total']; ?></td>
                                <td style="text-align: center;">
                                    <?php echo $merchant['total1']; ?>
                                </td>
                            </tr>
                        <script type="text/javascript">
                            $('#<?php echo $merchant['seller_id']; ?>view').on('click', function(){
                            $('#<?php echo $merchant['seller_id']; ?>view').attr('href', '<?php echo $view_detail; ?>&token=<?php echo $token; ?>&seller_id=' + $(this).data('sellerid') + '&period=' + $('#input-period').val() + '&year=' + $('#input-year').val());
                            });
                                    $('#<?php echo $merchant['seller_id']; ?>download').on('click', function(){
                            $('#<?php echo $merchant['seller_id']; ?>download').attr('href', '<?php echo $download_report; ?>&token=<?php echo $token; ?>&seller_id=' + $(this).data('sellerid') + '&period=' + $('#input-period').val() + '&year=' + $('#input-year').val());
                            });                        </script>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td class="text-center" colspan="7"><?php echo 'No Result'; ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
    $('#button-filter').on('click', function() {
        url = 'index.php?route=report/merchant/sku&token=<?php echo $token; ?>';
        var filter_merchant = $('select[name=\'filter_merchant\']').val();

            if (filter_merchant) {
                url += '&filter_merchant=' + encodeURIComponent(filter_merchant);
            }
        var filter_date_start = $('input[name=\'filter_date_start\']').val();
                if (filter_date_start) {
        url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
        }

        var filter_date_end = $('input[name=\'filter_date_end\']').val();
                if (filter_date_end) {
        url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
        }
        location = url;
        });
                //--></script> 
    <script type="text/javascript"><!--
  $('.date').datetimepicker({
        pickTime: false
        });
                //--></script></div>
<?php echo $footer; ?>