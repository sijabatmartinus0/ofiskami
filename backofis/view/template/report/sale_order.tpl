<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <!--<a href="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a>-->
                <button type="submit" form="myForm" formaction="<?php echo $download_report; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <form method="post" id="myForm" enctype="multipart/form-data" action="<?php echo $download_report; ?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-period"><?php echo $entry_merchant; ?></label>
                                    <select class="form-control" value="" id="seller" name="filter_merchant">
                                        <option value="0" selected="selected">All Seller</option>
                                        <?php foreach($sellers as $seller){ ?>
                                        <?php if ($seller['seller_id'] == $filter_merchant) { ?>
                                        <option value="<?php echo $seller['seller_id']; ?>" selected="selected"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $seller['seller_id']; ?>"><?php echo $seller['nickname']; ?> <?php if ($seller['company']){ ?> (<?php echo $seller['company']; ?>) <?php } ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-group"><?php echo $entry_group; ?></label>
                                    <select name="filter_group" id="input-group" class="form-control">
                                        <?php foreach ($groups as $group) { ?>
                                        <?php if ($group['value'] == $filter_group) { ?>
                                        <option value="<?php echo $group['value']; ?>" selected="selected"><?php echo $group['text']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $group['value']; ?>"><?php echo $group['text']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                    <select name="filter_order_status_id" id="input-status" class="form-control">
                                        <option value="0"><?php echo $text_all_status; ?></option>
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>
                            <!--<div class="col-sm-6">
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>-->
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-left"><?php echo $column_date_start; ?></td>
                                <td class="text-left"><?php echo $column_date_end; ?></td>
                                <td class="text-right"><?php echo $column_orders; ?></td>
                                <td class="text-left"><?php echo $column_seller; ?></td>
                                <td class="text-left"><?php echo $column_commission_fee; ?></td>
                                <td class="text-right"><?php echo $column_payment_online; ?></td>
                                <td class="text-left"><?php echo $column_batch; ?></td>
                                <!--                <td class="text-right"><?php echo $column_products; ?></td>
                                                <td class="text-right"><?php echo $column_tax; ?></td>
                                                <td class="text-right"><?php echo $column_total; ?></td>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($orders) { ?>
                            <?php foreach ($orders as $order) { ?>
                            <tr>
                                <td class="text-left"><?php echo $order['date_added']; ?></td>
                                <td class="text-left"><?php echo $order['date_added']; ?></td>
                                <td class="text-right"><?php echo $order['order_id']; ?></td>
                                <td class="text-left"><?php echo $order['nickname']; ?></td>
                                <td class="text-left"><?php echo $order['store_commission']; ?></td>
                                <td class="text-right"><?php echo $order['price']; ?></td>
                                <td class="text-right"><?php if($order['batch_id'] == ''){
                                    echo '-';
                                    } else{
                                    echo $order['batch_id']; 
                                    } ?>
                                </td>
                                <!--<td class="text-right"><?php echo $order['products']; ?></td>
                                <td class="text-right"><?php echo $order['tax']; ?></td>
                                <td class="text-right"><?php echo $order['total']; ?></td>-->
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('#button-filter').on('click', function () {
            url = 'index.php?route=report/sale_order&token=<?php echo $token; ?>';

            var filter_date_start = $('input[name=\'filter_date_start\']').val();

            if (filter_date_start) {
                url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
            }

            var filter_date_end = $('input[name=\'filter_date_end\']').val();

            if (filter_date_end) {
                url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
            }

            var filter_group = $('select[name=\'filter_group\']').val();

            if (filter_group) {
                url += '&filter_group=' + encodeURIComponent(filter_group);
            }

            var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();

            if (filter_order_status_id != 0) {
                url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
            }

            var filter_merchant = $('select[name=\'filter_merchant\']').val();

            if (filter_merchant) {
                url += '&filter_merchant=' + encodeURIComponent(filter_merchant);
            }
            location = url;
        });
        //--></script> 
    <script type="text/javascript"><!--
  $('.date').datetimepicker({
            pickTime: false
        });
        //--></script></div>
<?php echo $footer; ?>