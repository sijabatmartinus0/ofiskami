<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	<div class="pull-right">
		<a href="<?php echo $download_report; ?>&token=<?php echo $token; ?>&batch_id=<?php echo $batch_id; ?>&seller_id=<?php echo $seller_id; ?>" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_download; ?>" ><i class="fa fa-download"></i></a>
		<a href="<?php echo $link_back; ?>" class="btn btn-default"  data-toggle="tooltip" title="<?php echo $button_back; ?>"><i class="fa fa-reply"></i></a>
	</div>
      <h1><?php echo $heading_title5; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i><?php echo $heading_title5; ?></h3>
      </div>
      <div class="panel-body">
		
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group required">
					
				</div>
				<div class="form-group required">
					
				</div>
            </div>
          </div>
        
        <div class="table-responsive responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
                <td><?php echo $column_batch; ?></td>
				<td><?php echo $column_description; ?></td>
				<td><?php echo $column_date_added; ?></td>
				<td><?php echo $column_penalty; ?></td>
            </tr>
            </thead>
            <tbody>
			<?php if($details){ ?>
				<?php foreach($details as $detail){ ?>
				<tr>
					<td class="text-center"><?php echo $detail['batch_id']; ?></td>
					<td class="text-center"><?php echo $detail['description']; ?></td>
					<td class="text-center"><?php echo $detail['date_added']; ?></td>
					<td class="text-right"><?php echo $detail['penalty']; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td style="font-weight: bold; font-size: 14px;" colspan="3" class="text-center"><?php echo strtoupper($column_total_report); ?></td>
					<td class="text-right"><?php echo $total_penalty; ?></td>
				<tr>
			<?php }else { ?>
				<tr>
					<td colspan="5" class="text-center"><?php echo $text_no_results; ?></td>
				</tr>
			<?php } ?>
            </tbody>
          </table>
        </div>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>