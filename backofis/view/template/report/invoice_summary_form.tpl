<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	<div class="pull-right">
		<a href="<?php echo $download_report; ?>&token=<?php echo $token; ?>&batch_id=<?php echo $batch_id; ?>" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_download; ?>" ><i class="fa fa-download"></i></a>
		<a href="<?php echo $link_back; ?>" class="btn btn-default"  data-toggle="tooltip" title="<?php echo $button_back; ?>"><i class="fa fa-reply"></i></a>
	</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i><?php echo $heading_title; ?> <?php echo $nickname; ?></h3>
      </div>
      <div class="panel-body">
		
          <div class="row">
            <div class="col-sm-6">
				<div class="form-group required">
					
				</div>
				<div class="form-group required">
					
				</div>
            </div>
          </div>
        
        <div class="table-responsive responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="text-align: center;">
				<td><?php echo $column_vendor_name; ?></td>
				<td><?php echo $column_bank; ?></td>
				<td><?php echo $column_bank_account; ?></td>
				<td><?php echo $column_total_transaction; ?></td>
				<td><?php echo $column_total_fee; ?></td>
				<td><?php echo $text_add_deposit; ?></td>
				<td><?php echo $text_vendor_balance; ?></td>
            </tr>
            </thead>
            <tbody>
			<?php if($details){ ?>
				<?php foreach($details as $detail){ ?>
				<tr>
					<td class="text-center"><?php echo $detail['nickname']; ?></td>
					<td class="text-center"><?php echo $detail['bank']; ?></td>
					<td class="text-center"><?php echo $detail['bank_account']; ?></td>
					<td class="text-center"><?php echo $detail['total_transaction']; ?></td>
					<td class="text-center"><?php echo $detail['total_fee']; ?></td>
					<td class="text-center"><?php echo $detail['add_deposit']; ?></td>
					<td class="text-center"><?php echo $detail['total_refund']; ?></td>
				</tr>
				<?php } ?>
			<?php }else { ?>
				<tr>
					<td colspan="6" class="text-center"><?php echo $text_no_results; ?><td>
				</tr>
			<?php } ?>
            </tbody>
          </table>
        </div>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>