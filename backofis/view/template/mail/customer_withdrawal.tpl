<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" width="100" height="100" /></a>
  
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_withdrawal_status; ?> <b><?php echo $withdrawal_status; ?></b></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_withdrawal_link; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $withdrawal_link; ?></p>
  
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_withdrawal_request; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
		  <b><?php echo $text_customer; ?></b> <?php echo $customer; ?><br />
		  <b><?php echo $text_withdrawal; ?></b> <?php echo $withdrawal; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
          <b><?php echo $text_amount; ?></b> <?php echo $amount; ?><br />
		  <b><?php echo $text_description; ?></b> <?php echo $description; ?><br />
        </td>
      </tr>
    </tbody>
  </table>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>
</div>
</body>
</html>
