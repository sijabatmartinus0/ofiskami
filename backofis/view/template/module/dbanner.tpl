<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-dbanner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-dbanner" class="form-horizontal">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
				</div>
				<div class="panel-body">

					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="status" id="input-status" class="form-control">
								<?php if ($status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
						<div class="col-sm-10">
							<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
							<?php if ($error_name) { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $text_settings; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_title; ?></label>
						<div class="col-sm-10">
							<input type="text" name="title" value="<?php echo $title; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-padding"><?php echo $entry_padding; ?></label>
						<div class="col-sm-10">
							<input type="text" name="padding" value="<?php echo $padding; ?>" placeholder="<?php echo $entry_padding; ?>" id="input-padding" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-mobile-layout"><?php echo $entry_mobile_layout; ?></label>
						<div class="col-sm-10">
							<select name="mobile-layout" id="input-mobile-layout" class="form-control">
								<?php if ($mobile_layout) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
								<option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
								<option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $text_effects; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-effect-type"><?php echo $entry_effect_type; ?></label>
						<div class="col-sm-10">
							<select name="effect_type" id="input-effect-type" class="form-control">
								<?php foreach ($effects_array as $optgroup) { ?>
								<optgroup label="<?php echo $optgroup['name']; ?>">
									<?php foreach ($optgroup['effects'] as $value) { ?>
										<?php if ($value == $effect_type) { ?>
										<option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
										<?php } else { ?>
										<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
										<?php } ?>
									<?php } ?>
								</optgroup>
								<?php } ?>								
							</select>
						</div>
					</div>
				</div>
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $text_margins; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-1 control-label" for="input-margin-left"><?php echo $entry_margin_left; ?></label>
						<div class="col-sm-2">
							<input type="text" name="margin-left" value="<?php echo $margin_left; ?>" placeholder="0" id="input-margin-left" class="form-control" />
						</div>
						<label class="col-sm-1 control-label" for="input-margin-top"><?php echo $entry_margin_top; ?></label>
						<div class="col-sm-2">
							<input type="text" name="margin-top" value="<?php echo $margin_top; ?>" placeholder="0" id="input-margin-top" class="form-control" />
						</div>
						<label class="col-sm-1 control-label" for="input-margin-right"><?php echo $entry_margin_right; ?></label>
						<div class="col-sm-2">
							<input type="text" name="margin-right" value="<?php echo $margin_right; ?>" placeholder="0" id="input-margin-right" class="form-control" />
						</div>
						<label class="col-sm-1 control-label" for="input-margin-bottom"><?php echo $entry_margin_bottom; ?></label>
						<div class="col-sm-2">
							<input type="text" name="margin-bottom" value="<?php echo $margin_bottom; ?>" placeholder="0" id="input-margin-bottom" class="form-control" />
						</div>
					</div>
				</div>
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $text_banners; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-2">
							<div class="btn btn-primary button-add-dbanner-box"><i class="fa fa-plus"></i> <?php echo $text_add_new_widget; ?></div>
						</div>
					</div>
					<div class="gridster">
						<ul>
							<?php $i = 0; ?>
							<?php foreach ($gridster as $data) { ?>
							<?php $i++; ?>
							<li data-row="<?php echo $data['row']; ?>" data-col="<?php echo $data['col']; ?>" data-sizex="<?php echo $data['size_x']; ?>" data-sizey="<?php echo $data['size_y']; ?>">
								<i class="button-remove-dbanner-box fa fa-remove"></i>
								<div class="li-image-box">
									<img class="img-thumbnail" src="<?php echo $data['img_thumb']; ?>" alt="" title="" data-placeholder="<?php echo $data['img_thumb']; ?>" />
									<input type="hidden" name="image" value="<?php echo $data['img']; ?>" id="input-image">
								</div>
								<div class="li-button-box">
									<button type="button" class="btn btn-primary btn-lg button-edit" data-toggle="modal"><?php echo $text_edit; ?></button>
									<input type="hidden" value="<?php echo $data['modal']; ?>" name="modal-data" id="modal-data-input"/>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<input type="hidden" value="" name="gridster" id="gridster-input"/>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="widget-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<form action="" method="post" enctype="multipart/form-data" id="form-modal" class="form-horizontal">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><?php echo $text_edit_widget; ?></h4>
				</div>
				<div class="modal-body text-center">
					<div class="form-group">
						<div class="li-image-box">
							<a href="" id="thumb-image-modal" data-toggle="image" class="img-thumbnail modal-img-thumbnail">
								<img src="" alt="" title="" data-placeholder="" />
							</a>
							<input type="hidden" name="image-modal" value="" id="input-image-modal">
						</div>
					</div>

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist" id="languageTab">
						<?php foreach ($languages as $lang) { ?>
						<li>
							<a href="#language-<?php echo $lang['code']; ?>" data-toggle="tab">
								<img src="view/image/flags/<?php echo $lang['image']; ?>" title="<?php echo $lang['name']; ?>" /> 
								<?php echo $lang['name']; ?>
							</a>
						</li>
						<?php } ?>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<?php foreach ($languages as $lang) { ?>
						<div role="tabpanel" class="tab-pane" id="language-<?php echo $lang['code']; ?>">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-modal-name-<?php echo $lang['code']; ?>"><?php echo $text_title; ?></label>
									<div class="col-sm-10">
										<input type="text" name="modal-name-<?php echo $lang['code']; ?>" value="" placeholder="" id="input-modal-name-<?php echo $lang['code']; ?>" class="form-control" />
									</div>
								</div> 
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-modal-link-<?php echo $lang['code']; ?>"><?php echo $text_link; ?></label>
									<div class="col-sm-10">
										<input type="text" name="modal-link-<?php echo $lang['code']; ?>" value="" placeholder="" id="input-modal-link-<?php echo $lang['code']; ?>" class="form-control" />
									</div>
								</div> 
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-modal-show-mobile-<?php echo $lang['code']; ?>">
										<span data-toggle="tooltip" data-container="#tab-general" title="" data-original-title="<?php echo $info_mobile_layout; ?>">
											<?php echo $entry_show_on_mobile; ?>
										</span>
									</label>
									<div class="col-sm-10">
										<select name="modal-show-mobile-<?php echo $lang['code']; ?>" id="input-modal-show-mobile-<?php echo $lang['code']; ?>" class="form-control">
											<option value="1"><?php echo $text_yes; ?></option>
											<option value="0"><?php echo $text_no; ?></option>
										</select>
									</div>
								</div> 
							</div>
						</div>
						<?php } ?>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_cancel; ?></button>
					<button type="button" class="btn btn-primary save-modal"><?php echo $text_save; ?></button>
				</div>	
			</div>
		</form>
	</div>
</div>

<?php echo $footer; ?>

<script type="text/javascript">
var gridster;
var widget_edited;

function generateLiElement() {
	var liContent =  '<li>';
	liContent += '<i class="button-remove-dbanner-box fa fa-remove"></i>';
	liContent += '<div class="li-image-box">';
	liContent += '<img class="img-thumbnail" src="<?php echo $image_thumb_default; ?>" alt="" title="" data-placeholder="<?php echo $image_thumb_default; ?>">';
	liContent += '<input type="hidden" name="image" value="" id="input-image">';
	liContent += '</div>';
	liContent += '<div class="li-button-box">';
	liContent += '<button type="button" class="btn btn-primary btn-lg button-edit" data-toggle="modal"><?php echo $text_edit; ?></button>';
	liContent += '<input type="hidden" value="" name="modal-data" id="modal-data-input"/>';
	liContent += '</div>';
	liContent += '</li>';
	return liContent;
}

$(function(){

	gridsterSettings = {
		widget_base_dimensions: [90, 90],
		widget_margins: [3, 3],
		helper: 'clone',
		min_cols: 12,
		max_cols: 12,
		min_rows: 2,
		minWidth: 180,
		minHeight: 180,
		resize: {
			enabled: true,
			min_size: [2, 2]
		},
		serialize_params: function ($w, wgd) {
			return {
				id: wgd.el[0].id,
				col: wgd.col,
				row: wgd.row,
				size_y:wgd.size_y,
				size_x:wgd.size_x,
				img: $("input[name^='image']",$w).val(),
				modal: $("input[name='modal-data']",$w).val()
			};
		}
	}

	// Init Gridster
	gridster = $(".gridster ul").gridster(gridsterSettings).data('gridster');

	// Add new li element
	$("body").delegate(".button-add-dbanner-box", "click", function() {
		gridster.add_widget.apply(gridster,  [generateLiElement(), 2, 2]);
	});

	// Remove li element
	$("body").delegate(".button-remove-dbanner-box", "click", function() {
		gridster.remove_widget(this.parentNode);
	});

	// JSON data when submit form
	$('#form-dbanner').submit(function() {
		var data = gridster.serialize();
		$('#gridster-input').val(JSON.stringify(data));
	});

	// Select first tab of modal
	$('#languageTab a:first').tab('show');

	// Modal show
	$(document).on('click', '.button-edit', function(e){
		widget_edited = $(this);
		$('#widget-edit').find('form')[0].reset();
		var data = widget_edited.siblings('input[name="modal-data"]').val();
		if(data) {
			var arr = data.split('&').map(function(element) {
				var elements = element.split('=');
			    $('input[name="'+elements[0]+'"]').val(decodeURIComponent(elements[1].replace(/\+/g, '%20')));
			    if(elements[0].indexOf("modal-show-mobile") > -1) {
			    	$('select[name="'+elements[0]+'"] option[value="'+elements[1]+'"]').attr('selected', 'selected');
			    }
			});
		}

		// Set the thumbnail image in modal
		var liImageBox = widget_edited.parent().siblings('.li-image-box');
		var thumbnailSrc = liImageBox.find('.img-thumbnail').attr('src');
		var image = liImageBox.find('input[name="image"]').val();
		$('.modal-img-thumbnail').find('img').attr('src', thumbnailSrc);
		$('input[name="image-modal"]').val(image);		

		$('#widget-edit').modal('show');
	});

	// Modal copy data to input gridster
	$('#widget-edit .save-modal').on('click', function(e){
		var data = $('#form-modal').serialize();
		widget_edited.siblings('input[name="modal-data"]').val(data);
		var liImageBox = widget_edited.parent().siblings('.li-image-box');

		// Set the thumbnail image in gridster box
		var thumbnailSrcValue = $('.modal-img-thumbnail').find('img').attr('src');
		liImageBox.find('.img-thumbnail').attr('src', thumbnailSrcValue);
		var imageVal = $('input[name="image-modal"]').val();
		liImageBox.children('input[name="image"]').val(imageVal);

		$('#widget-edit').modal('hide');
	});

});
</script>

<style type="text/css">
.gridster ul {
	background-color: #EFEFEF;
	margin: 0 auto;
	padding: 0;
	list-style: none;
}
.gridster li.gs-w {
	background: #DDD;
	cursor: pointer;
	-webkit-box-shadow: 0 0 1px rgba(0,0,0,0.6);
	box-shadow: 0 0 1px rgba(0,0,0,0.6);
}
.gridster li.gs-w .li-image-box {
	position: absolute;
	top: 50%;
	left: 50%;
	margin-top: -74px;
	margin-left: -54px;
}
.gridster li.gs-w .button-remove-dbanner-box {
	position: absolute;
	right: 5px;
	top: 5px;
}
.gridster li.gs-w .li-button-box {
	position: absolute; 
	bottom: 10px; 
	left: 0; 
	width: 100%; 
	text-align: center;
}
</style>