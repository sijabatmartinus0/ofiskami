<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product-comment" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" id="saveSettings"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		<div class="form-group required">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_email"><span data-toggle="tooltip" title="<?php echo $pc_config_email_note; ?>"><?php echo $pc_config_email; ?></span></label>
						<div class="col-sm-10">
						  <input type="text" name="productcomments_pcconf_email" value="<?php echo $productcomments_pcconf_email; ?>" placeholder="<?php echo $pc_config_email; ?>" id="productcomments_pcconf_email" class="form-control" />
							<?php if ($error_pcconf_email) { ?>
						    <div class="text-danger"><?php echo $error_pcconf_email; ?></div>
						    <?php } ?>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_allow_guests"><span data-toggle="tooltip" title="<?php echo $pc_config_allow_guests_note; ?>"><?php echo $pc_config_allow_guests; ?></span></label>
						<div class="col-sm-10">
						  <div class="radio">
							<label>
							  <input type="radio" name="productcomments_pcconf_allow_guests" value="1" <?php if($productcomments_pcconf_allow_guests == 1) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_yes; ?>
								</label>
								<label>
				                <input type="radio" name="productcomments_pcconf_allow_guests" value="0" <?php if($productcomments_pcconf_allow_guests == 0) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_no; ?>
							</label>
						  </div>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_enforce_customer_data"><span data-toggle="tooltip" title="<?php echo $pc_config_enforce_customer_data_note; ?>"><?php echo $pc_config_enforce_customer_data; ?></span></label>
						<div class="col-sm-10">
						  <div class="radio">
							<label>
							  <input type="radio" name="productcomments_pcconf_enforce_customer_data" value="1" <?php if($productcomments_pcconf_enforce_customer_data == 1) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_yes; ?>
								</label>
								<label>
				                <input type="radio" name="productcomments_pcconf_enforce_customer_data" value="0" <?php if($productcomments_pcconf_enforce_customer_data == 0) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_no; ?>
							</label>
						  </div>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_enable_customer_captcha"><span data-toggle="tooltip" title="<?php echo $pc_config_enable_customer_captcha_note; ?>"><?php echo $pc_config_enable_customer_captcha; ?></span></label>
						<div class="col-sm-10">
						  <div class="radio">
							<label>
							  <input type="radio" name="productcomments_pcconf_enable_customer_captcha" value="1" <?php if($productcomments_pcconf_enable_customer_captcha == 1) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_yes; ?>
								</label>
								<label>
				                <input type="radio" name="productcomments_pcconf_enable_customer_captcha" value="0" <?php if($productcomments_pcconf_enable_customer_captcha == 0) { ?> checked="checked" <?php } ?>  />
				                <?php echo $text_no; ?>
							</label>
						  </div>
						</div>
					  </div>
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_maxlen"><span data-toggle="tooltip" title="<?php echo $pc_config_maxlen; ?>"><?php echo $pc_config_maxlen; ?></span></label>
						<div class="col-sm-10">
						  <input type="text" name="productcomments_pcconf_maxlen" value="<?php echo $productcomments_pcconf_maxlen; ?>" placeholder="<?php echo $pc_config_maxlen; ?>" id="productcomments_pcconf_maxlen" class="form-control" />
							<?php if ($error_pcconf_maxlen) { ?>
						    <div class="text-danger"><?php echo $error_pcconf_maxlen; ?></div>
						    <?php } ?>
						</div>
					  </div>
					  <div class="form-group required">
						<label class="col-sm-2 control-label" for="productcomments_pcconf_perpage"><span data-toggle="tooltip" title="<?php echo $pc_config_perpage_note; ?>"><?php echo $pc_config_perpage; ?></span></label>
						<div class="col-sm-10">
						  <input type="text" name="productcomments_pcconf_perpage" value="<?php echo $productcomments_pcconf_perpage; ?>" placeholder="<?php echo $pc_config_perpage; ?>" id="productcomments_pcconf_perpage" class="form-control" />
							<?php if ($error_pcconf_perpage) { ?>
						    <div class="text-danger"><?php echo $error_pcconf_perpage; ?></div>
						    <?php } ?>
						</div>
					  </div>
		</form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
function saveComment(comment_id) {
	$("#pcForm" + comment_id + ' textarea.ckeditor').each(function () {
		$(this).val(CKEDITOR.instances[$(this).attr('id')].getData());
	});

    $.ajax({
    	type: "POST",
      	url: 'index.php?route=module/productcomments/editComment&token=<?php echo $token; ?>',
      	data: $("#pcForm"+comment_id).serialize(),
      	dataType: 'json',
      	beforeSend: function() {
	      	$("#pcForm" + comment_id).find('.q-content').slideToggle('fast');
	      	$("#pcForm" + comment_id).find('.q-heading').addClass("loading");
      	},
      	complete: function() {
      		$("#pcForm" + comment_id).find('.q-heading').removeClass("loading").removeClass("red").addClass("green");
      		$("#pcForm" + comment_id).find('.q-content').removeClass("red").addClass("green");
      		$("#pcForm" + comment_id).find('.q-heading').effect("highlight", {color: '#BBDF8D'}, 2000);
      	},
      	success: function(json) {
      	}
    });
}

function delComment(comment_id) {
    $.ajax({
    	type: "GET",
      	url: 'index.php?route=module/productcomments/deleteComment&comment_id='+ comment_id +'&token=<?php echo $token; ?>',
      	beforeSend: function() {
	      	$("#pcForm" + comment_id).find('.q-content').slideToggle('fast');
	      	$("#pcForm" + comment_id).find('.q-heading').addClass("loading");
      	},
      	complete: function() {
      		$("#pcForm" + comment_id).parent().fadeOut(500, function() { $("#pcForm" + comment_id).parent().remove(); });
      	},
      	success: function(json) {
      	}
    });
}

$('.q-heading').bind('click', function() {
	if ($(this).hasClass('active')) {
		$(this).removeClass('active');
	} else {
		$(this).addClass('active');
	}
		
	$(this).parent().find('.q-content').slideToggle('slow');
});

//$('#tabs a').tabs();

/*$(function() {
	$("#saveSettings").click(function() {
		$("#error").html('').hide();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=module/productcomments/savesettings&token=<?php echo $token; ?>',
			data: $('#form-settings,#form-sidebar').serialize(),
			success: function(jsonData) {
				if (!$.isEmptyObject(jsonData.errors)) {
					jQuery.each(jsonData.errors, function(index, item) {
						$("#error").append('<p>'+jsonData.errors[error]+'</p>').show();
					});
				} else {
					//window.location.reload();
				}			
			}
		});
	});
});*/
</script> 


<?php echo $footer; ?>	
</div>