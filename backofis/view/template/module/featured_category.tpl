<?php echo $header; ?>
    <?php echo $column_left; ?>
        <div id="content">
            <div class="page-header">
                <div class="container-fluid">
                    <div class="pull-right">
                        <button type="submit" form="form-product-comment" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" id="saveSettings"><i class="fa fa-save"></i></button>
                        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                    <h1><?php echo $heading_title; ?></h1>
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li>
                                <a href="<?php echo $breadcrumb['href']; ?>">
                                    <?php echo $breadcrumb['text']; ?>
                                </a>
                            </li>
                            <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="container-fluid">
                <?php if ($error_warning) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
                        <?php echo $error_warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </div>
                    <?php } ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
                                    <ul class="nav nav-tabs">
										<li class="active"><a href="#tab-category" data-toggle="tab"><?php echo $tab_category; ?></a></li>
										<li><a href="#tab-setting" data-toggle="tab"><?php echo $tab_setting; ?></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab-category">
										<div class="table-responsive">
											<table id="categories" class="table table-striped table-bordered table-hover">
											  <thead>
												<tr>
												  <td class="text-left col-sm-2"><?php echo $entry_image; ?></td>
												  <td class="text-left col-sm-2"><?php echo $entry_mobile; ?></td>
												  <td class="text-left col-sm-6"><?php echo $entry_category; ?></td>
												  <td class="text-right col-sm-2"><?php echo $entry_sort_order; ?></td>
												  <td></td>
												</tr>
											  </thead>
											  <tbody>
												<?php $image_row = 0; ?>
												<?php foreach ($featured_categories as $featured_category) { ?>
												<tr id="category-row<?php echo $image_row; ?>">
												  <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $featured_category['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="featured_category_data[<?php echo $image_row; ?>][image]" value="<?php echo $featured_category['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
												  <td class="text-left"><a href="" id="thumb-image-mobile<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $featured_category['thumb_mobile']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="featured_category_data[<?php echo $image_row; ?>][image_mobile]" value="<?php echo $featured_category['image_mobile']; ?>" id="input-image-mobile<?php echo $image_row; ?>" /></td>
												  <td class="text-left"><input type="text" name="featured_category_data[<?php echo $image_row; ?>][category]" value="<?php echo $featured_category['category']; ?>" placeholder="<?php echo $entry_category; ?>" class="form-control category" /><input type="hidden" name="featured_category_data[<?php echo $image_row; ?>][category_id]" value="<?php echo $featured_category['category_id']; ?>"/></td>
												  <td class="text-right"><input type="text" name="featured_category_data[<?php echo $image_row; ?>][sort_order]" value="<?php echo $featured_category['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="number form-control" /></td>
												  <td class="text-left"><button type="button" onclick="$('#category-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
												</tr>
												<?php $image_row++; ?>
												<?php } ?>
											  </tbody>
											  <tfoot>
												<tr>
												  <td colspan="4"></td>
												  <td class="text-left"><button type="button" onclick="addCategory();" data-toggle="tooltip" title="<?php echo $button_category_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
												</tr>
											  </tfoot>
											</table>
										  </div>
										</div>
										<div class="tab-pane" id="tab-setting">
											<div class="form-group required">
												<label class="col-sm-2 control-label" for="featured_category_banner_image_width"><span data-toggle="tooltip" title="<?php echo $banner_image; ?>"><?php echo $banner_image; ?></span></label>
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-6">
															<input type="text" name="featured_category_banner_image_width" value="<?php echo $featured_category_banner_image_width; ?>" placeholder="<?php echo $text_width; ?>" id="featured_category_banner_image_width" class="form-control" readonly="readonly" />
														</div>
														<div class="col-sm-6">
															<input type="text" name="featured_category_banner_image_height" value="<?php echo $featured_category_banner_image_height; ?>" placeholder="<?php echo $text_height; ?>" id="featured_category_banner_image_height" class="form-control" readonly="readonly" />
														</div>
													</div>
												</div>
											</div>
											<div class="form-group required">
												<label class="col-sm-2 control-label" for="featured_category_product_image_width"><span data-toggle="tooltip" title="<?php echo $product_image; ?>"><?php echo $product_image; ?></span></label>
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-6">
															<input type="text" name="featured_category_product_image_width" value="<?php echo $featured_category_product_image_width; ?>" placeholder="<?php echo $text_width; ?>" id="featured_category_product_image_width" class="form-control" readonly="readonly" />
														</div>
														<div class="col-sm-6">
															<input type="text" name="featured_category_product_image_height" value="<?php echo $featured_category_product_image_height; ?>" placeholder="<?php echo $text_height; ?>" id="featured_category_product_image_height" class="form-control" readonly="readonly" />
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
												<div class="col-sm-10">
												  <select name="featured_category_status" id="input-status" class="form-control">
													<?php if ($featured_category_status) { ?>
													<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												  </select>
												</div>
											</div>
										</div>
									</div>
                                </form>
                            </div>
                        </div>
            </div>
        </div>
<script type="text/javascript"><!--
var category_row = <?php echo sizeof($featured_categories); ?>;

function addCategory() {
	html  = '<tr id="category-row' + category_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + category_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="featured_category_data[' + category_row + '][image]" value="" id="input-image' + category_row + '" /></td>';
	html += '  <td class="text-right"><input type="text" name="featured_category_data[' + category_row + '][category]" value="" placeholder="<?php echo $entry_category; ?>" class="form-control category" /><input type="hidden" name="featured_category_data[' + category_row + '][category_id]"/></td>';
	html += '  <td class="text-right"><input type="text" name="featured_category_data[' + category_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="number form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#category-row' + category_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#categories tbody').append(html);
	
	category_row++;
}

$('#categories').delegate('.category', 'click', function() {
	var selector = $(this);
	selector.autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['category_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			selector.val(item['label']);
			selector.siblings().val(item['value']);
		}
	});
});
//--></script> 
<?php echo $footer; ?>