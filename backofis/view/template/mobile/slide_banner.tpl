<?php echo $header; ?>
    <?php echo $column_left; ?>
        <div id="content">
            <div class="page-header">
                <div class="container-fluid">
                    <div class="pull-right">
                        <button type="submit" form="form-product-comment" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" id="saveSettings"><i class="fa fa-save"></i></button>
                        </div>
                    <h1><?php echo $heading_title; ?></h1>
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li>
                                <a href="<?php echo $breadcrumb['href']; ?>">
                                    <?php echo $breadcrumb['text']; ?>
                                </a>
                            </li>
                            <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="container-fluid">
                <?php if ($error_warning) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
                        <?php echo $error_warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </div>
                    <?php } ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
                                    <ul class="nav nav-tabs">
										<li class="active"><a href="#tab-category" data-toggle="tab"><?php echo $tab_data; ?></a></li>
										<li><a href="#tab-setting" data-toggle="tab"><?php echo $tab_setting; ?></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab-category">
										<div class="table-responsive">
											<table id="slides" class="table table-striped table-bordered table-hover">
											  <thead>
												<tr>
												  <td class="text-left col-sm-3"><?php echo $entry_image; ?></td>
												  <td class="text-left col-sm-2"><?php echo $entry_type; ?></td>
												  <td class="text-left col-sm-4"><?php echo $entry_value; ?></td>
												  <td class="text-right col-sm-1"><?php echo $entry_sort_order; ?></td>
												  <td class="text-right col-sm-2"><?php echo $entry_status; ?></td>
												  <td></td>
												</tr>
											  </thead>
											  <tbody>
												<?php $image_row = 0; $class=''; ?>
												<?php foreach ($slide_banners as $slide_banner) { ?>
												<tr id="category-row<?php echo $image_row; ?>">
													<td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $slide_banner['thumb'];?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="slide_banner_data[<?php echo $image_row; ?>][image]" value="<?php echo $slide_banner['image'];?>" id="input-image<?php echo $image_row; ?>" /></td>
													<td class="text-right">
														<select name="slide_banner_data[<?php echo $image_row; ?>][type]" class="form-control type">
														<?php foreach ($slide_types as $slide_type) { ?>
															<?php if($slide_type['id']==$slide_banner['type']){ ?>
															<option value="<?php echo $slide_type['id']; ?>" selected="selected"><?php echo $slide_type['name']; ?></option>
															<?php }else{ ?>
															<option value="<?php echo $slide_type['id']; ?>"><?php echo $slide_type['name']; ?></option>
															<?php } ?>
														<?php } ?>
														</select></td>
													<td class="text-right"><input type="text" name="slide_banner_data[<?php echo $image_row; ?>][value_text]" value="<?php echo $slide_banner['value_text'];?>" placeholder="<?php echo $entry_value; ?>" class="form-control value-text <?php echo $slide_banner['type'];?>" /><input type="hidden" name="slide_banner_data[<?php echo $image_row; ?>][value]" value="<?php echo $slide_banner['value'];?>"></td>
													<td class="text-right"><input type="text" name="slide_banner_data[<?php echo $image_row; ?>][sort_order]" value="<?php echo $slide_banner['sort_order'];?>" placeholder="<?php echo $entry_sort_order; ?>" class="number form-control" /></td>
													<td class="text-right">
														<select name="slide_banner_data[<?php echo $image_row; ?>][status]" class="form-control">
															<?php if ((int)$slide_banner['status']==1) { ?>
															<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
															<option value="0"><?php echo $text_disabled; ?></option>
															<?php } else { ?>
															<option value="1"><?php echo $text_enabled; ?></option>
															<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
															<?php } ?>
														</select>
													</td>
													<td class="text-left"><button type="button" onclick="$('#category-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
												</tr>
												<?php $image_row++; ?>
												<?php } ?>
											  </tbody>
											  <tfoot>
												<tr>
												  <td colspan="5"></td>
												  <td class="text-left"><button type="button" onclick="addSlide();" data-toggle="tooltip" title="<?php echo $button_slide_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
												</tr>
											  </tfoot>
											</table>
										  </div>
										</div>
										<div class="tab-pane" id="tab-setting">
											<div class="form-group required">
												<label class="col-sm-2 control-label" for="mobile_slide_banner_image_width"><span data-toggle="tooltip" title="<?php echo $slide_image; ?>"><?php echo $slide_image; ?></span></label>
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-6">
															<input type="text" name="mobile_slide_banner_image_width" value="<?php echo $mobile_slide_banner_image_width; ?>" placeholder="<?php echo $text_width; ?>" id="mobile_slide_banner_image_width" class="form-control" readonly="readonly" />
														</div>
														<div class="col-sm-6">
															<input type="text" name="mobile_slide_banner_image_height" value="<?php echo $mobile_slide_banner_image_height; ?>" placeholder="<?php echo $text_height; ?>" id="mobile_slide_banner_image_height" class="form-control" readonly="readonly" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                </form>
                            </div>
                        </div>
            </div>
        </div>
<script type="text/javascript"><!--
var data_row = <?php echo sizeof($slide_banners); ?>;
function addSlide() {
	html  = '<tr id="category-row' + data_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + data_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="slide_banner_data[' + data_row + '][image]" value="" id="input-image' + data_row + '" /></td>';
	html += '  <td class="text-right"><select name="slide_banner_data[' + data_row + '][type]" class="form-control type"><?php foreach ($slide_types as $slide_type) { ?><option value="<?php echo $slide_type['id']; ?>"><?php echo $slide_type['name']; ?></option><?php } ?></select></td>';
	html += '  <td class="text-right"><input type="text" name="slide_banner_data[' + data_row + '][value_text]" value="" placeholder="<?php echo $entry_value; ?>" class="form-control value-text product" /><input type="hidden" name="slide_banner_data[' + data_row + '][value]"></td>';
	html += '  <td class="text-right"><input type="text" name="slide_banner_data[' + data_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="number form-control" /></td>';
	html += '  <td class="text-right"><select name="slide_banner_data[' + data_row + '][status]" class="form-control"><option value="1" selected="selected"><?php echo $text_enabled; ?></option><option value="0"><?php echo $text_disabled; ?></option></select></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#category-row' + data_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#slides tbody').append(html);
	
	data_row++;
}

$('#slides').delegate('.category', 'keydown', function() {
	var selector = $(this);
	selector.autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['category_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			selector.val(item['label']);
			selector.siblings().val(item['value']);
		}
	});
});

$('#slides').delegate('.product', 'keydown', function() {
	var selector = $(this);
	selector.autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			selector.val(item['label']);
			selector.siblings().val(item['value']);
		}
	});
});

$('#slides').delegate('.url', 'keyup', function() {
	var selector = $(this);
	selector.siblings().val($(this).val());
});

$('#slides').delegate('.url', 'change', function() {
	var selector = $(this);
	selector.siblings().val($(this).val());
});

$('#slides').delegate('.type', 'change', function() {
	$(this).parent().next().find('input.value-text').removeClass('category');
	$(this).parent().next().find('input.value-text').removeClass('product');
	$(this).parent().next().find('input.value-text').removeClass('url');
	$(this).parent().next().find('input.value-text').addClass($(this).val());
});

//--></script> 
<?php echo $footer; ?>