<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success) && $success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-upload"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form class="form-horizontal" id="upload-product">
			<div class="form-group required">
				<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $text_category; ?></span></label>
				<div class="col-sm-10">
				<select class="form-control" value="" id="category" style="width: 400px;" name="category_id">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($categories as $category){ ?>
						<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_seller; ?>"><?php echo $text_seller; ?></span></label>
				<div class="col-sm-10">
				<select class="form-control" value="" id="seller" style="width: 400px;" name="seller_id">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($sellers as $seller){ ?>
						<option value="<?php echo $seller['seller_id']; ?>">[<?php echo $seller['seller_id']; ?>] <?php echo $seller['nickname']; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label" for="button-upload"><span data-toggle="tooltip" title="<?php echo $help_upload; ?>"><?php echo $entry_upload; ?></span></label>
				<div class="col-sm-10">
					<div class="input-group">
						<input type="text" name="filename" value="" placeholder="<?php echo $entry_choose_file; ?>" id="input-filename" class="form-control" readonly />
						<span class="input-group-btn">
						<button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
						</span> 
					</div>
					<input type="hidden" name="error_common" value="" class="form-control" />
				</div>
			</div>
            <br />
            <div class="pull-right">
			<!--<input type="reset" id="button-clear" class="btn btn-danger" value="<?php echo $button_clear; ?>"> -->
				<button type="button" id="button-continue" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-check"></i> <?php echo $button_continue; ?></button>
			</div>
            </div>
          </div>
        </form>
      </div>

<script type="text/javascript"><!--
$('#button-upload').on('click', function() {
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');
	
	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);		
			
			$.ajax({
				url: 'index.php?route=upload/product/upload_file&token=<?php echo $token; ?>',
				type: 'post',		
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,		
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},	
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
								
					if (json['success']) {
						alert(json['success']);
						
						$('input[name=\'filename\']').attr('value', json['filename']);
						// $('input[name=\'mask\']').attr('value', json['mask']);
					}
				},			
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

$("#button-continue").click(function() {
			var button = $(this);
			var id = $(this).attr('id');
			$.ajax({
				type: "POST",
				dataType: "json",
				url: 'index.php?route=upload/product/uploadProcess&token=<?php echo $token; ?>',
				data: $('#upload-product').serialize(),
				beforeSend: function() {
					$('div.text-danger').remove();
					$('.alert-danger').hide().find('i').text('');
				},
				complete: function(jqXHR, textStatus) {
					button.show().prev('span.wait').remove();
                    $('.alert-danger').hide().find('i').text('');
				},
				error: function(jqXHR, textStatus, errorThrown) {
                   $('.alert-danger').show().find('i').text(textStatus);
				},
				success: function(jsonData) {
					if (!jQuery.isEmptyObject(jsonData.errors)) {
						for (error in jsonData.errors) {
							$('[name="'+error+'"]').after('<div class="text-danger">' + jsonData.errors[error].join('') + '</div>');
						}
						window.scrollTo(0,0);
					} else {
						window.location = 'index.php?route=upload/product&token=<?php echo $token; ?>';
					}
				}
			});
		});
//--></script>
</div>
<?php echo $footer; ?>