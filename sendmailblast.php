<?php
	include 'config.php';
	require_once DIR_LIBRARY . '/mail.php';
	header("Access-Control-Allow-Origin: *");
	if($_SERVER['HTTP_TOKEN']=='ae6e6ad9bc298b3d4f2dd60e1b6ceaba'){
		if (empty($_POST['email'])) {
			$email = "";
		} else {
			$email = $_POST['email'];
		}
	
		//send email
		$config_mail = array(
			'protocol'		=> 'mail',
			'parameter'		=> '',
			'smtp_hostname'	=> 'ssl://smtp.gmail.com',
			'smtp_username'	=> 'coeagit.dev@gmail.com',
			'smtp_password'	=> 'Agit2015!',
			'smtp_port'		=> '465',
			'smtp_timeout'	=> '7'
		);
			
		$message = "Ofiskita Winner Announcement";
		$voucher_sender = 'admin@ofiskita.com';
		$store_name = 'Ofiskita';
		$subject = 'Ofiskita Winner Announcement';
		
		$html = '<html>
<head><title>'.$subject.'</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
</head>
<body style="font-family: verdana,geneva,sans-serif; font-size: 16px; color: #606060; margin:0; padding:0; ">
<div style="margin-top: 20px; border-top: 1px solid #CCCCCC; width: 100%;">
	<center>
		<img alt="" src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/1f554a3d-25d6-4c30-bdbd-9c26c9f74fb5.png" width="200" style="margin-top: 10px;">
	</center>
	
	<center>
	<div style=" width: 70%;">
		<div style="text-align: left;">
			<br>
			<br>
			
			<h1 style="color: #D83826; font-family: Helvetica; font-size: 38px; font-style: normal; font-weight: bold; line-height: 100%; letter-spacing: -1px; vertical-align: top; text-align: center; font-size: 26px;">Good day,</h1>
			
			<br>
			
			<p>First of all, we would like to thank you for coming over to <span style="color:#660033"><strong>#Ofiskita</strong> <strong>Popup Store</strong></span>!</p>
			
			<p>Thank you for your participation in our <span style="color:#660033">#<strong>Ofiskita Selfie/Groufie Competition</strong></span> and registration at <span style="color:#660033">#<strong>Ofiskita</strong></span><strong> </strong>to get a chance of winning our Grand Prize <span style="color:#660033"><strong>Color Multifunction Fuji Xerox DocuPrint CM 215 b</strong></span>!!<br>
			<br>
			Here are all the winners:</p>
			
			<p><u><strong>Selfie - eCoupon IDR 150.000</strong></u></p>
			
			<ul>
				<li>Jason Kasim</li>
				<li>Adho Fadilah</li>
				<li>Ryan Ariandi</li>
			</ul>
			
			<p><u><strong>Groufie - eCoupon IDR 500.000</strong></u></p>
			
			<ul>
				<li>Dewi Rokhmah Pyriana</li>
				<li>Andarulita Bakri</li>
			</ul>
			
			<p><u><strong>Grand Prize - Color Multifunction Fuji Xerox DocuPrint CM 215 b</strong></u></p>
			
			<ul>
				<li>Hendri Gunawan</li>
				<li>Reyhan Valiant</li>
			</ul>
			
			<br>
			<p>An email will be sent out to each winner to make arrangements to get your prizes out to you.<br>
			<br>
			Congratulations again on your win and see you soon in our upcoming <span style="color:#660033"><strong>#Ofiskita</strong> <strong>Popup Store</strong></span> near you!</p>
			<br>
			<center>
				<table width="35%">
				<tr>
					<td width="25%">
						<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
					<td width="25%">
						<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
					<td width="25%">
						<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
					<td width="25%">
						<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
				</tr>
				</table>
				</center>
		</div>
	</div>
	</center>
	<br>
	<br>
	<div style="background-color: #D83826; border-bottom: 1px solid #841719; padding: 9px 18px; color: #EEEEEE; font-size: 12px; height: 25%; text-align: center;">
		<center>
			<div style="border-top: 2px solid #fff; width: 50%; margin: 10px;">
			</div>
		</center>
		<br>
		<em>Copyright © 2016 Ofiskita Powered by Astragraphia, All rights reserved.</em><br>
			<br>
			<strong>Address:</strong><br>
			Jl. Kramat Raya No. 43<br>
			Senen, Jakarta<br>
			10450
	</div>
</div>


</body>
</html>';
		
		$mail = new Mail($config_mail);
		$mail->setTo($email);
		$mail->setFrom($voucher_sender);
		$mail->setSender($store_name);
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->setText($message);
		$mail->send();
	}
?>