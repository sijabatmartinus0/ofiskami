<?php
class ModelModuleProductComments extends Model {
    public function getTotalComments($data = array()) {
        $sql = "SELECT count(*) AS total
        		FROM `" . DB_PREFIX . "productcomments` pc,
				`" . DB_PREFIX . "customer` c
				WHERE 1 = 1 AND c.customer_id=pc.parent_id "
				. (isset($data['displayed']) ? " AND pc.display = 1" : '')
				. (isset($data['product_id']) ? " AND pc.product_id = " . (int)$data['product_id'] : '');

        $res = $this->db->query($sql);
        return $res->row['total'];
    }	
	/*Custom by M*/
	public function getTotalCommentsPerCustomer($data = array()) {
        $sql = "SELECT COUNT(DISTINCT parent_id) AS total
        		FROM `" . DB_PREFIX . "productcomments` pc
				WHERE 1 = 1"
				. (isset($data['displayed']) ? " AND pc.display = 1" : '')
				. (isset($data['parent_id']) ? " AND pc.parent_id <> " . (int)$data['parent_id'] : '')
				. (isset($data['product_id']) ? " AND pc.product_id = " . (int)$data['product_id'] : '');

        $res = $this->db->query($sql);
        return $res->row['total'];
    }

	public function getTotalCommentsPerCustomerSeller($data = array()) {
        $sql = "SELECT COUNT(DISTINCT parent_id) AS total
        		FROM `" . DB_PREFIX . "productcomments` pc
				WHERE 1 = 1"
				. (isset($data['displayed']) ? " AND pc.display = 1" : '')
				. (isset($data['parent_id']) ? " AND pc.parent_id = " . (int)$data['parent_id'] : '')
				. (isset($data['product_id']) ? " AND pc.product_id = " . (int)$data['product_id'] : '');

        $res = $this->db->query($sql);
        return $res->row['total'];
    }	
	
	public function getComments($data = array(), $sort = array()) {
		$sql = "SELECT  *
				FROM " . DB_PREFIX . "productcomments pc
				WHERE 1 = 1 "
				. (isset($data['displayed']) ? " AND pc.display = 1" : '')
				. (isset($data['product_id']) ? " AND pc.product_id = " . (int)$data['product_id'] : '')
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);

		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
		}

		return $res->rows;
	}
	
	public function getCommentsPerProductDetailPerCustomer($data = array()) {
		$sql = 'SELECT customer_id, parent_id, name, create_time, comment, flag FROM '. DB_PREFIX .'productcomments WHERE parent_id='. (int)$data['parent_id'] .' AND product_id='. (int)$data['product_id'];

		$res = $this->db->query($sql);
		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
			$row['create_time'] = date('d/m/Y',$row['create_time']);
			$row['name'] = htmlspecialchars($row['name']);
		}
		return $res->rows;
	}
	
	public function getCommentsPerProductDetail($data = array(), $sort = array()) {
		$sql = '
		SELECT
			c.firstname,
			sub_table.id,
			sub_table.parent_id,
			sub_table.customer_id,
			sub_table.name,
			sub_table.email,
			sub_table.comment,
			sub_table.create_time,
			sub_table.flag,
			(SELECT count(*) FROM '. DB_PREFIX .'productcomments WHERE product_id=sub_table.product_id AND parent_id=sub_table.parent_id) AS total 
		FROM
			(SELECT
					*,
					@rn:=CASE
						WHEN @var_customer_id = pc.parent_id THEN @rn + 1
						ELSE 1
					END AS rn,
					@var_customer_id:=pc.parent_id
			FROM
				(SELECT @var_customer_id:=NULL, @rn:=NULL) vars, (SELECT * FROM '. DB_PREFIX .'productcomments ORDER BY id DESC) as pc
			ORDER BY pc.parent_id , pc.id DESC) AS sub_table
			INNER JOIN '. DB_PREFIX .'customer c 
				ON c.customer_id=sub_table.parent_id,
		   (SELECT DISTINCT parent_id FROM '. DB_PREFIX .'productcomments '. DB_PREFIX .'productcomments '. (isset($data['product_id']) ? ' WHERE product_id  = ' . (int)$data['product_id'] : '') .' '. (isset($sort['limit']) ? ' LIMIT '.(int)$sort['offset'].', '.(int)($sort['limit']) : '') .' ) AS p
		WHERE
			rn <= 2 AND
			sub_table.parent_id IN (p.parent_id)
			'. (isset($data['product_id']) ? ' AND sub_table.product_id  = ' . (int)$data['product_id'] : '') .'
			'. (isset($data['parent_id']) ? ' AND sub_table.parent_id  = ' . (int)$data['parent_id'] : '') .'
		ORDER BY c.customer_id , sub_table.id ASC';

		$res = $this->db->query($sql);
		$result = array();
		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
			$row['create_time'] = date('d/m/Y',$row['create_time']);
			$row['name'] = htmlspecialchars($row['name']);
			$result['comments'][] = $row;
			$result['total'] = $row['total'];
		}
		return $result;
	}
	
	public function getCommentsPerProduct($data = array(), $sort = array()) {
		$sql = '
		SELECT
			c.firstname,
			sub_table.id,
			sub_table.parent_id,
			sub_table.customer_id,
			sub_table.name,
			sub_table.email,
			sub_table.comment,
			sub_table.create_time,
			sub_table.flag,
			(SELECT count(*) FROM '. DB_PREFIX .'productcomments WHERE product_id=sub_table.product_id AND parent_id=sub_table.parent_id) AS total 
		FROM
			(SELECT
					*,
					@rn:=CASE
						WHEN @var_customer_id = pc.parent_id THEN @rn + 1
						ELSE 1
					END AS rn,
					@var_customer_id:=pc.parent_id
			FROM
				(SELECT @var_customer_id:=NULL, @rn:=NULL) vars, (SELECT * FROM '. DB_PREFIX .'productcomments ORDER BY id DESC) as pc
			ORDER BY pc.parent_id , pc.id DESC) AS sub_table
			INNER JOIN '. DB_PREFIX .'customer c 
				ON c.customer_id=sub_table.parent_id,
		   (SELECT DISTINCT parent_id FROM '. DB_PREFIX .'productcomments '. (isset($data['product_id']) ? ' WHERE product_id  = ' . (int)$data['product_id'] : '') .' '. (isset($data['parent_id']) && (int)$data['parent_id']!=0 ? ' AND parent_id  <> ' . (int)$data['parent_id'] : '') .' '. (isset($sort['limit']) ? ' LIMIT '.(int)$sort['offset'].', '.(int)($sort['limit']) : '') .' ) AS p
		WHERE
			rn <= 2 AND
			sub_table.parent_id IN (p.parent_id)
			'. (isset($data['product_id']) ? ' AND sub_table.product_id  = ' . (int)$data['product_id'] : '') .'
			'. (isset($data['parent_id']) && (int)$data['parent_id']!=0 ? ' AND sub_table.parent_id  <> ' . (int)$data['parent_id'] : '') .'
		ORDER BY c.customer_id , sub_table.id ASC';
		
		$res = $this->db->query($sql);

		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
		}
		
		$result = array();
		foreach ($res->rows as &$row)  {
			$result[$row['parent_id']]['comments'][] = $row;
			$result[$row['parent_id']]['customer'] = $row['firstname'];
			$result[$row['parent_id']]['flag'] = $row['flag'];
			$result[$row['parent_id']]['total'] = $row['total'];
		}
		return $result;
	}

	public function getCommentsPerProductSeller($data = array(), $sort = array()) {
		$sql = '
		SELECT
			c.firstname,
			sub_table.id,
			sub_table.parent_id,
			sub_table.customer_id,
			sub_table.name,
			sub_table.email,
			sub_table.comment,
			sub_table.create_time,
			sub_table.flag,
			(SELECT count(*) FROM '. DB_PREFIX .'productcomments WHERE product_id=sub_table.product_id AND parent_id=sub_table.parent_id) AS total 
		FROM
			(SELECT
					*,
					@rn:=CASE
						WHEN @var_customer_id = pc.parent_id THEN @rn + 1
						ELSE 1
					END AS rn,
					@var_customer_id:=pc.parent_id
			FROM
				(SELECT @var_customer_id:=NULL, @rn:=NULL) vars, (SELECT * FROM '. DB_PREFIX .'productcomments ORDER BY id DESC) as pc
			ORDER BY pc.parent_id , pc.id DESC) AS sub_table
			INNER JOIN '. DB_PREFIX .'customer c 
				ON c.customer_id=sub_table.parent_id,
		   (SELECT DISTINCT parent_id FROM '. DB_PREFIX .'productcomments '. (isset($data['product_id']) ? ' WHERE product_id  = ' . (int)$data['product_id'] : '') .' '. (isset($sort['limit']) ? ' LIMIT '.(int)$sort['offset'].', '.(int)($sort['limit']) : '') .' ) AS p
		WHERE
			rn <= 2 AND
			sub_table.parent_id IN (p.parent_id)
			'. (isset($data['product_id']) ? ' AND sub_table.product_id  = ' . (int)$data['product_id'] : '') .'
			'. (isset($data['parent_id']) && (int)$data['parent_id']!=0 ? ' AND sub_table.parent_id  = ' . (int)$data['parent_id'] : '') .'
		ORDER BY c.customer_id , sub_table.id ASC';
		
		$res = $this->db->query($sql);

		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
		}
		
		$result = array();
		foreach ($res->rows as &$row)  {
			$result[$row['parent_id']]['comments'][] = $row;
			$result[$row['parent_id']]['customer'] = $row['firstname'];
			$result[$row['parent_id']]['flag'] = $row['flag'];
			$result[$row['parent_id']]['total'] = $row['total'];
		}
		return $result;
	}
	
	public function getCommentsPerProductParent($data = array(), $sort = array()) {
		$sql = '
		SELECT
			c.firstname,
			sub_table.id,
			sub_table.parent_id,
			sub_table.customer_id,
			sub_table.name,
			sub_table.email,
			sub_table.comment,
			sub_table.create_time,
			sub_table.flag,
			(SELECT count(*) FROM '. DB_PREFIX .'productcomments WHERE product_id=sub_table.product_id AND parent_id=sub_table.parent_id) AS total 
		FROM
			(SELECT
					*,
					@rn:=CASE
						WHEN @var_customer_id = pc.parent_id THEN @rn + 1
						ELSE 1
					END AS rn,
					@var_customer_id:=pc.parent_id
			FROM
				(SELECT @var_customer_id:=NULL, @rn:=NULL) vars, (SELECT * FROM '. DB_PREFIX .'productcomments ORDER BY id DESC) as pc
			ORDER BY pc.parent_id , pc.id DESC) AS sub_table
			INNER JOIN '. DB_PREFIX .'customer c 
				ON c.customer_id=sub_table.parent_id,
		   (SELECT DISTINCT parent_id FROM '. DB_PREFIX .'productcomments '. (isset($sort['limit']) ? ' LIMIT '.(int)$sort['offset'].', '.(int)($sort['limit']) : '') .' ) AS p
		WHERE
			rn <= 2 AND
			sub_table.parent_id IN (p.parent_id)
			'. (isset($data['product_id']) ? ' AND sub_table.product_id  = ' . (int)$data['product_id'] : '') .'
			'. ((int)$data['parent_id']!=0 ? ' AND sub_table.parent_id  = ' . (int)$data['parent_id'] : '') .'
		ORDER BY c.customer_id , sub_table.id ASC';
		
		$res = $this->db->query($sql);

		foreach ($res->rows as &$row)  {
			$row['comment'] = htmlspecialchars_decode($row['comment']);
		}
		
		$result = array();
		foreach ($res->rows as &$row)  {
			$result['comments'][] = $row;
			$result['customer'] = $row['firstname'];
			$result['flag'] = $row['flag'];
			$result['total'] = $row['total'];
		}
		return $result;
	}	
	
	/*Custom by M*/
	public function addComment($comment) {
		$sql = "UPDATE `" . DB_PREFIX . "productcomments`
					SET flag=0 WHERE 1=1 "
						. (isset($comment['parent_id']) ? ' AND parent_id  = ' . (int)$comment['parent_id'] : '');
		
		if($this->db->query($sql)){
			$sql = "INSERT INTO `" . DB_PREFIX . "productcomments`
					(customer_id, user_id, parent_id, flag, product_id, name, email, comment, display, create_time)
					VALUES(" 
						. (isset($comment["customer_id"]) ?  (int)$comment["customer_id"] : 'NULL') . ','
						. (isset($comment["user_id"]) ?  (int)$comment["user_id"] : 'NULL') . ','
						. (isset($comment["parent_id"]) ?  (int)$comment["parent_id"] : 'NULL') . ','
						. (isset($comment["flag"]) ?  (int)$comment["flag"] : '1') . ','
						. (int)$comment["product_id"] . ','
						. "'" . $this->db->escape($comment["name"]) . "',"
						. "'" . $this->db->escape($comment["email"]) . "',"
						. "'" . $this->db->escape($comment["comment"]) . "',
						1,
						UNIX_TIMESTAMP(NOW())
					)";

			if ($res = $this->db->query($sql)) {
				if (filter_var($this->config->get('pcconf_email'), FILTER_VALIDATE_EMAIL)) {
					$this->load->model('catalog/product');
					$prod = $this->model_catalog_product->getProduct($comment["product_id"]);
					$product_name = strip_tags(htmlspecialchars_decode($prod['name']));
					$comment['comment'] = htmlspecialchars_decode($comment['comment']);

					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$headers .= 'From: ' . $this->config->get('config_email') . "\r\n";
					$headers .= 'Reply-To: ' . $comment["email"];
					
					$to = $this->config->get('pcconf_email');
					$baseurl = $this->config->get('config_url');
					$subject = sprintf($this->language->get('pc_mail_subject'), $this->config->get('config_name'), $product_name);
					$message = sprintf($this->language->get('pc_mail'), "<a href='" . $this->url->link('catalog/product', 'product_id=' . $prod['product_id'], 'SSL') . "'>$product_name</a>", $comment['name'], $comment['email'], nl2br($comment['comment']), $this->url->link("module/productcomments", 'SSL'));
					return mail($to, $subject, nl2br($message),$headers);
					
					//Send Email
					/*
							$this->load->language('mail/customer');

							$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

							$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";

							if (!$customer_group_info['approval']) {
								$message .= $this->language->get('text_login') . "\n";
							} else {
								$message .= $this->language->get('text_approval') . "\n";
							}

							$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
							$message .= $this->language->get('text_services') . "\n\n";
							$message .= $this->language->get('text_thanks') . "\n";
							$message .= $this->config->get('config_name');

							$mail = new Mail($this->config->get('config_mail'));
							$mail->setTo($data['email']);
							$mail->setFrom($this->config->get('config_email'));
							$mail->setSender($this->config->get('config_name'));
							$mail->setSubject($subject);
							$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
							$mail->send();
							*/
				}
			};
		};
    }    
    public function productOwnedBySeller($product_id,$seller_id){
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "ms_product`
				WHERE seller_id = " . (int)$seller_id . " 
				AND product_id = " . (int)$product_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['total'];
	}

}