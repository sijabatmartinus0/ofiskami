<?php
class ModelPickupOrder extends Model {
    public function getTotalOrders() {
        $sql = "SELECT count(*) AS total
        		FROM `" . DB_PREFIX . "order_detail` 
				WHERE 1 = 1".
				" AND order_detail_status_id = 1".
				" AND order_status_id IN (19,22)".
				" AND pp_branch_id = ".(int) $this->pickup->getBranchId();

        $res = $this->db->query($sql);
        return $res->row['total'];
    }
	
	public function getOrders($data = array(), $sort = array()) {
		$sql = "SELECT * FROM  
				(SELECT 
				ood.order_id,
				ood.order_detail_id,
				CONCAT(ood.invoice_prefix,ood.invoice_no) as invoice,
				CONCAT(od.payment_firstname,' ',od.payment_lastname) as customer,
				ood.pp_name as recipient,
				ood.pp_telephone as telephone,
				(case when ood.order_status_id=19 then 'Waiting' else os.name end) as status,
				ood.order_status_id,
				ood.order_detail_status_id,
				ood.date_added,
				ood.date_modified 
				FROM " . DB_PREFIX . "order_detail ood
				LEFT JOIN " . DB_PREFIX . "order od ON od.order_id=ood.order_id 
				LEFT JOIN " . DB_PREFIX . "order_status os ON os.order_status_id=ood.order_status_id AND os.language_id=".(int)$this->config->get('config_language_id')
				." WHERE 1 = 1 "
				." AND ood.order_detail_status_id = 1 "
				." AND ood.order_status_id IN (19,22) "
				." AND ood.delivery_type_id = 2 "
				." AND ood.pp_branch_id = ".(int) $this->pickup->getBranchId(). ") as data"
				." WHERE 1 = 1 "
				. (isset($data['key']) ? " AND (data.invoice LIKE '%".$this->db->escape($data['key'])."%'  OR data.customer LIKE '%".$this->db->escape($data['key'])."%'  OR data.recipient LIKE '%".$this->db->escape($data['key'])."%' OR data.telephone LIKE '%".$this->db->escape($data['key'])."%')" : '')
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
	
		$res = $this->db->query($sql);

		return $res->rows;
	}
	
	public function getOrder($order_detail_id){
		$sql = "SELECT 
				ood.order_id,
				ood.order_detail_id,
				od.payment_method,
				CONCAT(ood.invoice_prefix,ood.invoice_no) as invoice,
				CONCAT(od.payment_firstname,' ',od.payment_lastname) as customer,
				ood.pp_name as recipient,
				ood.pp_email as email,
				ood.pp_telephone as telephone,
				(case when ood.order_status_id=19 then 'Waiting' else os.name end) as status,
				ood.order_status_id,
				ood.order_detail_status_id,
				ood.date_added,
				ood.date_modified 
				FROM " . DB_PREFIX . "order_detail ood
				LEFT JOIN " . DB_PREFIX . "order od ON od.order_id=ood.order_id 
				LEFT JOIN " . DB_PREFIX . "order_status os ON os.order_status_id=ood.order_status_id AND os.language_id=".(int)$this->config->get('config_language_id')
				." WHERE 1 = 1 "
				." AND ood.order_detail_status_id = 1 "
				." AND ood.order_status_id IN (19,22) "
				." AND ood.delivery_type_id = 2 "
				." AND ood.pp_branch_id = ".(int) $this->pickup->getBranchId()
				." AND ood.order_detail_id = ".(int) $order_detail_id;
	
		$res = $this->db->query($sql);

		return $res->row;
	}
	
	public function getOrderProducts($order_detail_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");

		return $query->rows;
	}
	
	public function updatePickup ($order_detail_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET order_status_id = '21', date_modified = NOW() WHERE order_detail_id = '" . (int)$order_detail_id . "'");

		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_detail` WHERE order_detail_id = '" . (int)$order_detail_id . "'");
			
		$this->load->language('mail/pickup');
		
		foreach($order_query->rows as $order){
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id = '" . (int)$order['order_id'] . "', order_detail_id = '" . (int)$order['order_detail_id'] . "', order_status_id = '21', notify = '0',user_id = '" . (int)$this->pickup->getId() . "',identity = 'Pick Up Point', date_added = NOW()");
			
			$branch_query=$this->getBranch();
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($order['pp_email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($this->language->get('text_subject_pickup'));
			
			/*Custom by M*/
			$data['title']=$this->language->get('text_title_pickup');
			$data['header']=$this->language->get('text_header_pickup');
			$invoice=$order['invoice_prefix'].$order['invoice_no'];
			$data['pp_name']=$order['pp_name'];
			$data['pp_branch_name']=$branch_query['pp_branch_name'];
			$data['pp_address']=$branch_query['company']."<br/>".$branch_query['address']."<br/>".$branch_query['city'].",".$branch_query['zone'].",".$branch_query['country'].",".$branch_query['postcode'];
			$data['message_pickup']=sprintf($this->language->get('message_pickup'),$invoice);
			$data['message_link_pickup']=sprintf($this->language->get('message_link_pickup'), $this->url->link('account/order/info', '&order_id='.(int)$order['order_id'].'&order_detail_id='.(int)$order['order_detail_id']));
			$data['message_warning']=$this->language->get('text_message_warning');
			
			$mail->setHtml($this->load->view('default/template/mail/pickup_pickup.tpl', $data));
			$mail->send();
		}
	}
	
	public function updateReceive ($order_detail_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET order_status_id = '22', date_modified = NOW() WHERE order_detail_id = '" . (int)$order_detail_id . "'");

		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_detail` WHERE order_detail_id = '" . (int)$order_detail_id . "'");
			
		$this->load->language('mail/pickup');
		
		foreach($order_query->rows as $order){
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id = '" . (int)$order['order_id'] . "', order_detail_id = '" . (int)$order['order_detail_id'] . "', order_status_id = '22', notify = '0',user_id = '" . (int)$this->pickup->getId() . "',identity = 'Pick Up Point', date_added = NOW()");
			
			$branch_query=$this->getBranch();
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($order['pp_email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($this->language->get('text_subject_receive'));
			
			/*Custom by M*/
			$data['title']=$this->language->get('text_title_receive');
			$data['header']=$this->language->get('text_header_receive');
			$invoice=$order['invoice_prefix'].$order['invoice_no'];
			$data['pp_name']=$order['pp_name'];
			$data['pp_branch_name']=$branch_query['pp_branch_name'];
			$data['pp_address']=$branch_query['company']."<br/>".$branch_query['address']."<br/>".$branch_query['city'].",".$branch_query['zone'].",".$branch_query['country'].",".$branch_query['postcode'];
			$data['message_receive']=sprintf($this->language->get('message_receive'),$invoice);
			$data['message_link_receive']=sprintf($this->language->get('message_link_receive'), $this->url->link('account/order/info', '&order_id='.(int)$order['order_id'].'&order_detail_id='.(int)$order['order_detail_id']));
			$data['message_warning']=$this->language->get('text_message_warning');
			
			$mail->setHtml($this->load->view('default/template/mail/pickup_receive.tpl', $data));
			$mail->send();
		}
	}
	
	public function getBranch(){
		$sql = "SELECT 
			opb.pp_branch_id,
			opb.pp_branch_name,
			opb.company,
			opb.telephone,
			opb.country_id,
			oc.name as country,
			opb.zone_id,
			oz.name as zone,
			opb.city_id,
			oc1.name as city,
			opb.district_id,
			od.name as district,
			opb.subdistrict_id,
			os.name as subdistrict,
			opb.postcode,
			opb.address  
				FROM " . DB_PREFIX . "pp_branch opb"
				." LEFT JOIN " . DB_PREFIX . "country oc ON oc.country_id=opb.country_id  "
				." LEFT JOIN " . DB_PREFIX . "zone oz ON oz.zone_id=opb.zone_id  "
				." LEFT JOIN " . DB_PREFIX . "city oc1 ON oc1.city_id=opb.city_id  "
				." LEFT JOIN " . DB_PREFIX . "district od ON od.district_id=opb.district_id  "
				." LEFT JOIN " . DB_PREFIX . "subdistrict os ON os.subdistrict_id=opb.subdistrict_id  "
				." WHERE 1 = 1 "
				." AND opb.pp_branch_id = ".(int) $this->pickup->getBranchId();
	
		$res = $this->db->query($sql);

		return $res->row;
	}
	
	public function getUpdatePickup($order_detail_id){
        $sql = "SELECT *
        		FROM `" . DB_PREFIX . "order_detail` 
				WHERE 1 = 1".
				" AND order_detail_status_id = 1".
				" AND order_status_id =22".
				" AND order_detail_id = ".(int) $order_detail_id.
				" AND pp_branch_id = ".(int) $this->pickup->getBranchId();

        $res = $this->db->query($sql);
        if($res->num_rows){
			return true;
		}else{
			return false;
		}
	}
	
	public function getUpdateReceive($order_detail_id){
        $sql = "SELECT *
        		FROM `" . DB_PREFIX . "order_detail` 
				WHERE 1 = 1".
				" AND order_detail_status_id = 1".
				" AND order_status_id =19".
				" AND order_detail_id = ".(int) $order_detail_id.
				" AND pp_branch_id = ".(int) $this->pickup->getBranchId();

        $res = $this->db->query($sql);
        if($res->num_rows){
			return true;
		}else{
			return false;
		}
	}
	
	 public function getTotalHistoryOrders() {
        $sql = "SELECT count(*) AS total
        		FROM `" . DB_PREFIX . "order_detail` 
				WHERE 1 = 1".
				" AND order_detail_status_id = 1".
				" AND order_status_id IN (5,21)".
				" AND pp_branch_id = ".(int) $this->pickup->getBranchId();

        $res = $this->db->query($sql);
        return $res->row['total'];
    }
	
	public function getHistoryOrders($data = array(), $sort = array()) {
		$sql = "SELECT * FROM  
				(SELECT 
				ood.order_id,
				ood.order_detail_id,
				CONCAT(ood.invoice_prefix,ood.invoice_no) as invoice,
				CONCAT(od.payment_firstname,' ',od.payment_lastname) as customer,
				ood.pp_name as recipient,
				ood.pp_telephone as telephone,
				os.name as status,
				ood.order_status_id,
				ood.order_detail_status_id,
				ood.date_added,
				ood.date_modified 
				FROM " . DB_PREFIX . "order_detail ood
				LEFT JOIN " . DB_PREFIX . "order od ON od.order_id=ood.order_id 
				LEFT JOIN " . DB_PREFIX . "order_status os ON os.order_status_id=ood.order_status_id AND os.language_id=".(int)$this->config->get('config_language_id')
				." WHERE 1 = 1 "
				." AND ood.order_detail_status_id = 1 "
				." AND ood.order_status_id IN (5,21) "
				." AND ood.delivery_type_id = 2 "
				." AND ood.pp_branch_id = ".(int) $this->pickup->getBranchId(). ") as data"
				." WHERE 1 = 1 "
				. (isset($data['key']) ? " AND (data.invoice LIKE '%".$this->db->escape($data['key'])."%'  OR data.customer LIKE '%".$this->db->escape($data['key'])."%'  OR data.recipient LIKE '%".$this->db->escape($data['key'])."%' OR data.telephone LIKE '%".$this->db->escape($data['key'])."%')" : '')
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
	
		$res = $this->db->query($sql);

		return $res->rows;
	}
	
	public function getHistoryOrder($order_detail_id){
		$sql = "SELECT 
				ood.order_id,
				ood.order_detail_id,
				od.payment_method,
				CONCAT(ood.invoice_prefix,ood.invoice_no) as invoice,
				CONCAT(od.payment_firstname,' ',od.payment_lastname) as customer,
				ood.pp_name as recipient,
				ood.pp_email as email,
				ood.pp_telephone as telephone,
				(case when ood.order_status_id=19 then 'Waiting' else os.name end) as status,
				ood.order_status_id,
				ood.order_detail_status_id,
				ood.date_added,
				ood.date_modified 
				FROM " . DB_PREFIX . "order_detail ood
				LEFT JOIN " . DB_PREFIX . "order od ON od.order_id=ood.order_id 
				LEFT JOIN " . DB_PREFIX . "order_status os ON os.order_status_id=ood.order_status_id AND os.language_id=".(int)$this->config->get('config_language_id')
				." WHERE 1 = 1 "
				." AND ood.order_detail_status_id = 1 "
				." AND ood.order_status_id IN (5,21) "
				." AND ood.delivery_type_id = 2 "
				." AND ood.pp_branch_id = ".(int) $this->pickup->getBranchId()
				." AND ood.order_detail_id = ".(int) $order_detail_id;
	
		$res = $this->db->query($sql);

		return $res->row;
	}
}