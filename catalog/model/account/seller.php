<?php
class ModelAccountSeller extends Model {
	public function getSellerByProduct($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_product WHERE product_id = '" . (int)$product_id . "'");

		return $query->row;
	}
	
	public function getSeller($seller_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");

		return $query->row;
	}
	
	public function getLogisticSeller($seller_id){
		$query = $this->db->query("SELECT 
			os.shipping_id AS shipping_id, 
			os.code as shipping_code,
			os.name AS shipping, 
			oss.shipping_service_id AS shipping_service_id, 
			oss.name AS shipping_service, 
			oss.description AS description
		FROM " . DB_PREFIX . "ms_seller_logistic omsl
		LEFT JOIN " . DB_PREFIX . "shipping os ON os.shipping_id=omsl.logistic_id
		LEFT JOIN " . DB_PREFIX . "shipping_service oss ON oss.shipping_id=os.shipping_id AND oss.status=1
		WHERE omsl.seller_id=".(int)$seller_id);

		return $query->rows;
	}
	
	public function getLogisticCodeSeller($seller_id){
		$query = $this->db->query("SELECT 
			os.shipping_id AS shipping_id, 
			os.code as shipping_code
		FROM " . DB_PREFIX . "ms_seller_logistic omsl
		LEFT JOIN " . DB_PREFIX . "shipping os ON os.shipping_id=omsl.logistic_id
		WHERE omsl.seller_id=".(int)$seller_id);

		return $query->rows;
	}

}