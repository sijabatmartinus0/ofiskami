<?php
class ModelAccountReview extends Model {
	public function getCountNotificationReview($customer_id){
		$sql = "SELECT 
				COUNT(*) as total
				FROM oc_order o
				LEFT JOIN " . DB_PREFIX . "order_detail od ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "ms_seller ms ON od.seller_id=ms.seller_id
				LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id=op.order_detail_id
				LEFT JOIN " . DB_PREFIX . "product p ON op.product_id=p.product_id
				LEFT JOIN " . DB_PREFIX . "review r ON op.product_id=r.product_id AND od.order_detail_id=r.order_detail_id
				WHERE 1=1 
				AND o.customer_id='" . (int)$customer_id."'
				AND od.order_status_id=5
				AND od.delivery_type_id IN(1,2)
				AND r.review_id IS NULL";
		$res = $this->db->query($sql);
		return $order_total=$res->row['total'];
	}
	
	public function getReview($data=array()){
		$sql_condition='';
		if($data['key']=='all'){
			$sql_condition='';
		}else if($data['key']=='unread'){
			$sql_condition=' AND r.review_id IS NULL ';
		}else{
			return array();
		}
		$sql = "SELECT 
				od.order_detail_id,
				od.order_id,
				CONCAT(od.invoice_prefix,od.invoice_no) as invoice,
				ms.nickname as seller,
				ms.avatar as avatar,
				od.date_added as date_added,
				op.name as product_name,
				p.image as product_image,
				p.product_id as product_id,
				r.review_id as review_id,
				r.rating as review_rating,
				r.text as review_text,
				r.date_added as review_date_added,
				r.author as review_author,
				r.rating_accuracy as review_accuracy
				FROM oc_order o
				LEFT JOIN " . DB_PREFIX . "order_detail od ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "ms_seller ms ON od.seller_id=ms.seller_id
				LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id=op.order_detail_id
				LEFT JOIN " . DB_PREFIX . "product p ON op.product_id=p.product_id
				LEFT JOIN " . DB_PREFIX . "review r ON op.product_id=r.product_id AND od.order_detail_id=r.order_detail_id
				WHERE 1=1 
				AND o.customer_id='" . (int)$data['customer_id']."'
				AND od.order_status_id=5
				AND od.delivery_type_id IN(1,2) "
				. (isset($data['search']) ? " AND (op.name LIKE '%".$this->db->escape($data['search'])."%' OR CONCAT(od.invoice_prefix,od.invoice_no) LIKE '%".$this->db->escape($data['search'])."%')" : '')
				. $sql_condition
				. " ORDER BY od.order_detail_id DESC";
		$res = $this->db->query($sql);
		return $res->rows;
	}
	
	public function checkReview($data=array()){
		$sql = "SELECT COUNT(*) as total
				FROM " . DB_PREFIX . "order o
				LEFT JOIN " . DB_PREFIX . "order_detail od ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id=op.order_detail_id
				WHERE 1=1 
				AND o.customer_id='" . (int)$this->customer->getId()."'
				AND od.order_status_id=5
				AND od.delivery_type_id IN(1,2)
				AND od.order_detail_id='" . (int)$data['order_detail_id']."'
				AND od.order_id='" . (int)$data['order_id']."'
				AND op.product_id='" . (int)$data['product_id']."'";
		$res = $this->db->query($sql);
		if((int)$res->row['total']>0){
			$sql = "SELECT COUNT(*) as total
				FROM " . DB_PREFIX . "order o
				LEFT JOIN " . DB_PREFIX . "order_detail od ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "order_product op ON od.order_detail_id=op.order_detail_id
				LEFT JOIN " . DB_PREFIX . "review r ON op.product_id=r.product_id
				WHERE 1=1 
				AND o.customer_id='" . (int)$this->customer->getId()."'
				AND od.order_status_id=5
				AND od.delivery_type_id IN(1,2)
				AND r.order_detail_id='" . (int)$data['order_detail_id']."'
				AND r.order_id='" . (int)$data['order_id']."'
				AND r.product_id='" . (int)$data['product_id']."'";
			$res = $this->db->query($sql);
			if((int)$res->row['total']>0){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	public function addReview($data=array()){
		$this->event->trigger('pre.review.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "review SET author = '" . $this->customer->getFirstName()." ".$this->customer->getLastName() . "', customer_id = '" . (int)$this->customer->getId() . "', order_detail_id = '" . (int)$data['order_detail_id'] . "', order_id = '" . (int)$data['order_id'] . "', product_id = '" . (int)$data['product_id'] . "', text = '" . $this->db->escape($data['text']) . "', rating = '" . (int)$data['rating'] . "', status='1', rating_accuracy = '" . (int)$data['rating_accuracy'] . "', date_added = NOW()");

		$review_id = $this->db->getLastId();
		
		$select_seller = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_product WHERE product_id ='".(int)$data['product_id']."'");
		
		$seller_id = $select_seller->row['seller_id'];
		
		$email_seller = $this->getEmail((int)$seller_id);

		// if ($this->config->get('config_review_mail')) {
			$this->load->language('mail/review');
			$this->load->model('catalog/product');
			$product_info = $this->model_catalog_product->getProduct($data['product_id']);

			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			// $message  = $this->language->get('text_waiting') . "\n";
			// $message .= sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name']))) . "\n";
			// $message .= sprintf($this->language->get('text_reviewer'), $this->db->escape(strip_tags($this->customer->getFirstName()." ".$this->customer->getLastName()))) . "\n";
			// $message .= sprintf($this->language->get('text_rating'), $this->db->escape(strip_tags($data['rating']))) . "\n";
			// $message .= sprintf($this->language->get('text_rating_accuracy'), $this->db->escape(strip_tags($data['rating_accuracy']))) . "\n";
			// $message .= $this->language->get('text_review') . "\n";
			// $message .= $this->db->escape(strip_tags($data['text'])) . "\n\n";
			$content=array();
			$content['title'] = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$content['text_greeting'] = $this->language->get('text_greeting_merchant');
			$content['text_reviewer'] = sprintf($this->language->get('text_reviewer'), $this->db->escape(strip_tags($this->customer->getFirstName()." ".$this->customer->getLastName())));
			$content['text_product'] = sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name'])));
			$content['text_rating'] = sprintf($this->language->get('text_rating'), $this->db->escape(strip_tags($data['rating'])));
			$content['text_rating_accuracy'] = sprintf($this->language->get('text_rating_accuracy'), $this->db->escape(strip_tags($data['rating_accuracy'])));
			$content['text_message'] = $this->db->escape(strip_tags($data['text']));

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/review.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/review.tpl', $content);
			} else {
				$html = $this->load->view('default/template/mail/review.tpl', $content);
			}
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($email_seller);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
			
			// Send to additional alert emails
			// $emails = explode(',', $this->config->get('config_mail_alert'));

			// foreach ($emails as $email) {
				// if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
					// $mail->setTo($email);
					// $mail->send();
				// }
			// }
		// }
		
		$sql = "SELECT *
				FROM " . DB_PREFIX . "review 
				WHERE 1=1 
				AND review_id='" . (int)$review_id."'
				AND status=1";
		$res = $this->db->query($sql);
		
		$this->event->trigger('post.review.add', $review_id);
		
		return $res->row;
	}
	
	public function getEmail($customer_id){
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE customer_id = '" . (int)$customer_id. "'");

		return $query->row['email'];
	}
}