<?php
class ModelAccountAddress extends Model {
	public function addAddress($data) {
		$this->event->trigger('pre.customer.add.address', $data);

		/*$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$this->customer->getId() . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "'");

		$address_id = $this->db->getLastId();*/
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$this->customer->getId() . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', alias = '" . $this->db->escape($data['alias']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? serialize($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}

		$this->event->trigger('post.customer.add.address', $address_id);

		return $address_id;
	}

	public function editAddress($address_id, $data) {
		$this->event->trigger('pre.customer.edit.address', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', alias = '" . $this->db->escape($data['alias']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "' WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}

		$this->event->trigger('post.customer.edit.address', $address_id);
	}

	public function deleteAddress($address_id) {
		$this->event->trigger('pre.customer.delete.address', $address_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.delete.address', $address_id);
	}

	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}
			
			$city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$address_query->row['city_id'] . "'");

			if ($city_query->num_rows) {
				$city = $city_query->row['name'];
				$city_code = $city_query->row['code'];
			} else {
				$city = '';
				$city_code = '';
			}
			
			$district_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$address_query->row['district_id'] . "'");

			if ($district_query->num_rows) {
				$district = $district_query->row['name'];
				$district_code = $district_query->row['code'];
			} else {
				$district = '';
				$district_code = '';
			}
			
			$subdistrict_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$address_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query->num_rows) {
				$subdistrict = $subdistrict_query->row['name'];
				$subdistrict_code = $subdistrict_query->row['code'];
			} else {
				$subdistrict = '';
				$subdistrict_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'alias'       => $address_query->row['alias'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'subdistrict_id' => $address_query->row['subdistrict_id'],
				'subdistrict'    => $subdistrict,
				'district_id'    => $address_query->row['district_id'],
				'district'       => $district,
				'city_id'        => $address_query->row['city_id'],
				'city'           => $city,
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => unserialize($address_query->row['custom_field'])
			);

			return $address_data;
		} else {
			return false;
		}
	}
	
	public function getAddressByCustomer() {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "' limit 0,1");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}
			
			$city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$address_query->row['city_id'] . "'");

			if ($city_query->num_rows) {
				$city = $city_query->row['name'];
				$city_code = $city_query->row['code'];
			} else {
				$city = '';
				$city_code = '';
			}
			
			$district_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$address_query->row['district_id'] . "'");

			if ($district_query->num_rows) {
				$district = $district_query->row['name'];
				$district_code = $district_query->row['code'];
			} else {
				$district = '';
				$district_code = '';
			}
			
			$subdistrict_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$address_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query->num_rows) {
				$subdistrict = $subdistrict_query->row['name'];
				$subdistrict_code = $subdistrict_query->row['code'];
			} else {
				$subdistrict = '';
				$subdistrict_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'alias'       => $address_query->row['alias'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'subdistrict_id' => $address_query->row['subdistrict_id'],
				'subdistrict'    => $subdistrict,
				'district_id'    => $address_query->row['district_id'],
				'district'       => $district,
				'city_id'        => $address_query->row['city_id'],
				'city'           => $city,
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => unserialize($address_query->row['custom_field'])
			);

			return $address_data;
		} else {
			return false;
		}
	}

	public function getAddresses() {
		$address_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		foreach ($query->rows as $result) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'alias'       => $result['alias'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $result['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => unserialize($result['custom_field'])

			);
		}

		return $address_data;
	}

	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
	
	public function getCheckoutAddress($data){
		$address=array();
		$sql="
			SELECT 
				oc2.telephone,
				oa.address_id,
				oa.customer_id,
				oa.firstname,
				oa.lastname,
				oa.alias,
				oa.company,
				oa.address_1,
				oa.address_2,
				oa.city,
				oa.postcode,
				oa.country_id,
				oa.zone_id,
				oa.city_id,
				oa.district_id,
				oa.subdistrict_id,
				oa.custom_field,
				oc.name as country,
				oz.name as zone,
				oc1.name as city,
				od.name as district,
				os.name as subdistrict 
			FROM " . DB_PREFIX . "address  oa
			LEFT JOIN " . DB_PREFIX . "customer oc2 ON oc2.customer_id=oa.customer_id
			LEFT JOIN " . DB_PREFIX . "country oc ON oc.country_id=oa.country_id
			LEFT JOIN " . DB_PREFIX . "zone oz ON oz.zone_id=oa.zone_id
			LEFT JOIN " . DB_PREFIX . "city oc1 ON oc1.city_id=oa.city_id
			LEFT JOIN " . DB_PREFIX . "district od ON od.district_id=oa.district_id
			LEFT JOIN " . DB_PREFIX . "subdistrict os ON os.subdistrict_id=oa.subdistrict_id
			WHERE oa.customer_id=".(int)$data['customer_id'].
			(isset($data['address_id']) ? " AND oa.address_id=" .(int)$data['address_id'] : "");
			
		$query = $this->db->query($sql);
		
		if($query->num_rows){
			if(isset($data['address_id'])){
				$result=$query->row;
				$temp=array();
				$temp['id']=$result['address_id'];
				$temp['name']=$result['alias'];
				$temp['telephone']=$result['telephone'];
				$temp['detail']=$result['firstname']."<br>".
								$result['address_1']." ".$result['address_2']."<br>".
								$result['city'].",".$result['district'].",".$result['subdistrict'].",".$result['postcode']."<br>".$result['zone'].",".$result['country'];
				$address=$temp;
			}else{
				foreach ($query->rows as $result) {
					$temp=array();
					$temp['id']=$result['address_id'];
					$temp['name']=$result['alias'];
					$temp['telephone']=$result['telephone'];
					$temp['detail']='<strong>'.$result['firstname']."</strong><br>".
									$result['address_1']." ".$result['address_2']."<br>".
									$result['city'].",".$result['district'].",".$result['subdistrict'].",".$result['postcode']."<br>".$result['zone'].",".$result['country'];
					$address[]=$temp;
				}
			}
		}
		return $address;
	}
	public function getCheckoutCustomerAddress($data){
		$sql="SELECT * FROM " . DB_PREFIX . "address  oa WHERE customer_id=".(int)$data['customer_id'].(isset($data['address_id']) ? " AND address_id=" .(int)$data['address_id'] : "");
			
		$query = $this->db->query($sql);

		return $query->row; 
	}
	
	public function getCheckoutSellerAddress($data){
		$sql="SELECT oms.* FROM " . DB_PREFIX . "ms_product omp LEFT JOIN " . DB_PREFIX . "ms_seller oms ON oms.seller_id=omp.seller_id where omp.product_status=1 AND omp.product_id=".(int)$data['product_id'];
			
		$query = $this->db->query($sql);

		return $query->row; 
	}
}