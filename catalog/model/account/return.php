<?php
class ModelAccountReturn extends Model {
	public function addReturn($data) {
		$this->event->trigger('pre.return.add', $data);
		$product_id = $data['input_product_id'];
		
		/**upload picture**/
		$today = date("Y_m_d");
		$path = DIR_IMAGE . '/return_picture/' . $today ;
		
		if (!empty($_FILES['picture_return']['tmp_name'])){
			//create new directory
			if (!file_exists($path)) {
				mkdir(DIR_IMAGE . '/return_picture/' . $today . '/', 0777, true);
			}
			$folder = DIR_IMAGE . '/return_picture/' . $today . '/';
			
			//rename
			$this->load->model('account/return');
			$return_id_pict = $this->model_account_return->getLastIdReturn();
			
			$pict_format = explode(".", basename($_FILES['picture_return']['name']));
			$picture_return_product='return_picture/' . $today . '/' . $return_id_pict.'_'. $product_id.'_'.$this->customer->getId().'.'.$pict_format[1];
			
			$folder = $folder . basename($_FILES['picture_return']['name']);
			
			move_uploaded_file($_FILES['picture_return']['tmp_name'], $folder);
			
			rename($folder, DIR_IMAGE . '/return_picture/' . $today . '/'. $return_id_pict.'_'.$product_id.'_'.$this->customer->getId().'.'.$pict_format[1]);
		}else{
			$picture_return_product = '';
		}
		
		$this->load->model('catalog/product');
		$seller_id = $this->model_catalog_product->getMsProductById($product_id);
		
		$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 24, date_modified=NOW()  WHERE order_detail_id='".(int)$this->db->escape($data['input_order_detail_id'])."'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$data['order_id']."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id'])."', order_status_id = 24, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Customer', date_added=NOW()");
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "return` SET order_id = '" . (int)$data['order_id'] . "', customer_id = '" . (int)$this->customer->getId() . "', product_id='".(int)$this->db->escape($product_id)."', order_detail_id='".(int)$this->db->escape($data['input_order_detail_id'])."', seller_id = '".(int)$seller_id."', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', product = '" . $this->db->escape($data['product']) . "', model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', opened = '" . (int)$data['opened'] . "', return_reason_id = '" . (int)$data['return_reason_id'] . "', return_action_id = '".(int)$data['return_action_id']."', return_status_id = '" . (int)$this->config->get('config_return_status_id') . "', comment = '" . $this->db->escape($data['comment']) . "', image='".$this->db->escape($picture_return_product)."', is_action_changed=0, confirm_action_changed=0, date_ordered = '" . $this->db->escape($data['date_ordered']) . "', sla='".(int)$this->config->get('config_sla_return')."', date_added = NOW(), date_modified = NOW()");

		$return_id = $this->db->getLastId();
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "return_history` SET return_id = '" . (int)$return_id . "', return_status_id = '" . (int)$this->config->get('config_return_status_id') . "', notify=0, comment='".$this->db->escape($data['comment'])."', date_added = NOW()");
		
		$this->event->trigger('post.return.add', $return_id);

		return $return_id;
	}

	public function getReturn($return_id) {
		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, r.email, r.telephone, r.product, r.model, r.quantity, r.opened, (SELECT rr.name FROM " . DB_PREFIX . "return_reason rr WHERE rr.return_reason_id = r.return_reason_id AND rr.language_id = '" . (int)$this->config->get('config_language_id') . "') AS reason, (SELECT ra.name FROM " . DB_PREFIX . "return_action ra WHERE ra.return_action_id = r.return_action_id AND ra.language_id = '" . (int)$this->config->get('config_language_id') . "') AS action, (SELECT rs.name FROM " . DB_PREFIX . "return_status rs WHERE rs.return_status_id = r.return_status_id AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, r.comment, r.date_ordered, r.date_added, r.date_modified, CONCAT(od.invoice_prefix, od.invoice_no) as invoice_no, r.image, r.product_id, r.return_status_id, r.is_action_changed, r.confirm_action_changed, r.sla FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX ."order_detail od ON r.order_detail_id = od.order_detail_id WHERE r.return_id = '" . (int)$return_id . "' AND customer_id = '" . $this->customer->getId() . "'");

		return $query->row;
	}
	
	public function getReturnNotif($return_id) {
		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, r.email, r.telephone, r.product, r.model, r.quantity, r.opened, (SELECT rr.name FROM " . DB_PREFIX . "return_reason rr WHERE rr.return_reason_id = r.return_reason_id AND rr.language_id = o.language_id) AS reason, (SELECT ra.name FROM " . DB_PREFIX . "return_action ra WHERE ra.return_action_id = r.return_action_id AND ra.language_id = o.language_id) AS action, (SELECT rs.name FROM " . DB_PREFIX . "return_status rs WHERE rs.return_status_id = r.return_status_id AND rs.language_id = o.language_id) AS status, r.comment, r.date_ordered, r.date_added, r.date_modified, CONCAT(od.invoice_prefix, od.invoice_no) as invoice_no, r.image, r.product_id, r.return_status_id, r.is_action_changed, r.confirm_action_changed, r.sla FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX ."order_detail od ON r.order_detail_id = od.order_detail_id LEFT JOIN " . DB_PREFIX ."order o ON r.order_id = o.order_id WHERE r.return_id = '" . (int)$return_id . "'");

		return $query->row;
	}

	public function getReturns($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, rs.name as status, r.date_added FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX . "return_status rs ON (r.return_status_id = rs.return_status_id) WHERE r.customer_id = '" . $this->customer->getId() . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY r.return_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalReturns() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "return`WHERE customer_id = '" . $this->customer->getId() . "'");

		return $query->row['total'];
	}

	// public function getReturnHistories($return_id) {
		// $query = $this->db->query("SELECT rh.date_added, rs.name AS status, rh.comment, rh.notify FROM " . DB_PREFIX . "return_history rh LEFT JOIN " . DB_PREFIX . "return_status rs ON rh.return_status_id = rs.return_status_id WHERE rh.return_id = '" . (int)$return_id . "' AND rh.notify = '1' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY rh.date_added ASC");

		// return $query->rows;
	// }
	
	public function getReturnHistories($return_id) {
		$query = $this->db->query("SELECT rh.date_added, rs.name AS status, rh.comment, rh.notify FROM " . DB_PREFIX . "return_history rh LEFT JOIN " . DB_PREFIX . "return_status rs ON rh.return_status_id = rs.return_status_id WHERE rh.return_id = '" . (int)$return_id . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY rh.date_added ASC");

		return $query->rows;
	}
	
	public function getLastIdReturn(){
		$query = $this->db->query("SELECT return_id FROM " . DB_PREFIX . "return ORDER BY return_id DESC LIMIT 1");
		
		$last_id = $query->row['return_id'];
		
		return $last_id+1;
	}
	
	public function getQuantityProduct($product_id, $order_detail_id){
		$query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "order_product WHERE product_id='".(int)$product_id."' AND order_detail_id='".(int)$order_detail_id."'");
		
		return $query->row['quantity'];
	}
	
	public function completeReturn($return_id){
		$this->db->query("UPDATE " . DB_PREFIX . "return SET return_status_id=3, date_modified=NOW() WHERE return_id='".(int)$return_id."'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "return_history SET return_id='".(int)$return_id."', return_status_id=3, notify=0, date_added=NOW()");
		
		$query_order_detail_id = $this->db->query("SELECT * FROM " . DB_PREFIX . "return WHERE return_id='".(int)$return_id."'");
		$order_detail_id = $query_order_detail_id->row['order_detail_id'];
		$order_id= $query_order_detail_id->row['order_id'];
		
		$getProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '".$order_detail_id."'");
		
		$products = $getProduct->rows;
		$isComplete = true;
		foreach($products as $product){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "return WHERE product_id='".(int)$product['product_id']."' AND order_detail_id= '".(int)$order_detail_id."'");
			if ($query->num_rows) {
				$isComplete = false;
				
				$query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "return r INNER JOIN " . DB_PREFIX . "return_history rh ON rh.return_id = r.return_id WHERE r.product_id='".(int)$product['product_id']."' AND rh.return_status_id = 3 AND r.order_detail_id='".(int)$order_detail_id."'");
				
				if ($query2->num_rows) {
					$isComplete = true;
				}	
			}else{
				$isComplete = false;
			}
		}
		
		if($isComplete == true){
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id=21, date_modified=NOW() WHERE order_detail_id='".(int)$order_detail_id."'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$order_id."', order_detail_id = '".(int)$order_detail_id."', order_status_id = 21, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Customer', date_added=NOW()");
		}
	}
	
	public function confirmAction($return_id){
		$this->db->query("UPDATE " . DB_PREFIX . "return SET confirm_action_changed = 1, date_modified=NOW() WHERE return_id='".(int)$return_id."'");
		
		$this->sendNotifConfirmAction($return_id);
	}
	
	public function sendNotification($return_id, $status_id){
		$return_info = $this->getReturnNotif($return_id);
		
		if ($return_info) {
			
			$this->load->model('account/order');
			
			$return_id = $return_info['return_id'];
			$order_id = $return_info['order_id'];
			
			$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
			
			if ($order_query->num_rows) {
				$language_id = $order_query->row['language_id'];
				$store_name = $order_query->row['store_name'];
				$store_url = $order_query->row['store_url'];
			} else {
				$language_id = '';
				$store_name = '';
				$store_url = '';
			}
			
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($language_id);

			if ($language_info) {
				$language_directory = $language_info['directory'];
			} else {
				$language_directory = '';
			}
			
			$language = new Language($language_directory);
			$language->load('mail/return');
			$language->load('default');
			
			$subject = sprintf($language->get('text_new_subject'), $store_name, $return_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), $return_id);

			$data['text_new_received'] = $language->get('text_new_received');
			$data['text_update_return_status'] = $language->get('text_update_return_status');
			$data['text_new_link'] = $language->get('text_new_link');
			$data['text_new_return_detail'] = $language->get('text_new_return_detail');
			$data['text_new_return_id'] = $language->get('text_new_return_id');
			$data['text_new_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_date_added');
			$data['text_date_returned'] = $language->get('text_date_returned');
			$data['text_invoice'] = $language->get('text_invoice');
			$data['text_customer'] = $language->get('text_customer');
			$data['text_telephone'] = $language->get('text_telephone');
			$data['text_email'] = $language->get('text_email');
			$data['text_yes'] = $language->get('text_yes');
			$data['text_no'] = $language->get('text_no');
			$data['text_information'] = $language->get('text_information');
			$data['text_prod_name'] = $language->get('text_prod_name');
			$data['text_prod_model'] = $language->get('text_prod_model');
			$data['text_prod_qty'] = $language->get('text_prod_qty');
			$data['text_prod_open'] = $language->get('text_prod_open');
			$data['text_ret_reason'] = $language->get('text_ret_reason');
			$data['text_ret_comment'] = $language->get('text_ret_comment');
			$data['text_ret_status'] = $language->get('text_ret_status');
			$data['text_ret_action'] = $language->get('text_ret_action');
			$data['text_update_footer'] = $language->get('text_update_footer');
			$data['text_merchant_complete'] = $language->get('text_merchant_complete');
			$data['text_merchant_link'] = $language->get('text_merchant_link');

			$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			
			$data['link'] = $store_url . 'index.php?route=account/return/info&return_id=' . $return_id;
			$data['link_merchant'] = $store_url . 'index.php?route=seller/account-return-list/detail&return_id=' . $return_id;
			
			$data['order_id'] = $order_id;
			$data['return_id'] = $return_id;
			$data['store_name'] = $store_name;
			$data['store_url'] = $store_url;
			$data['firstname'] = $return_info['firstname'];
			$data['lastname'] = $return_info['lastname'];
			$data['email'] = $return_info['email'];
			$data['telephone'] = $return_info['telephone'];
			$data['product'] = $return_info['product'];
			$data['model'] = $return_info['model'];
			$data['quantity'] = $return_info['quantity'];
			
			if($return_info['opened'] == 0){
				$data['opened'] = $language->get('text_no');
			}else if($return_info['opened'] == 1){
				$data['opened'] = $language->get('text_yes');
			}
			
			$data['reason'] = $return_info['reason'];
			$data['action'] = $return_info['action'];
			$data['status'] = $return_info['status'];
			$data['comment'] = $return_info['comment'];
			
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($return_info['date_added']));
			$data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($return_info['date_ordered']));
			$data['date_modified'] = date($this->language->get('date_format_short'), strtotime($return_info['date_modified']));
			
			$data['invoice_no'] = $return_info['invoice_no'];
			$data['product_id'] = $return_info['product_id'];
			$data['return_status_id'] = $return_info['return_status_id'];
			$data['status_id'] = $status_id;

			$email_merchant = $this->model_account_order->getMerchantInfo($return_info['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/return_add.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/return_add.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/return_add.tpl', $data);
			}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/return_complete.tpl')) {
				$html_merchant = $this->load->view($this->config->get('config_template') . '/template/mail/return_complete.tpl', $data);
			} else {
				$html_merchant = $this->load->view('default/template/mail/return_complete.tpl', $data);
			}
			
			if($status_id == 1){
				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($email_merchant);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($store_name);
				$mail->setSubject($subject);
				$mail->setHtml($html_merchant);
				$mail->send();
			}else if($status_id == 2){
				$mail_self = new Mail($this->config->get('config_mail'));
				$mail_self->setTo($return_info['email']);
				$mail_self->setFrom($this->config->get('config_email'));
				$mail_self->setSender($store_name);
				$mail_self->setSubject($subject);
				$mail_self->setHtml($html);
				$mail_self->send();
			}else if($status_id == 3){
				$mail_self = new Mail($this->config->get('config_mail'));
				$mail_self->setTo($return_info['email']);
				$mail_self->setFrom($this->config->get('config_email'));
				$mail_self->setSender($store_name);
				$mail_self->setSubject($subject);
				$mail_self->setHtml($html);
				$mail_self->send();
				
				$mail_merchant = new Mail($this->config->get('config_mail'));
				$mail_merchant->setTo($email_merchant);
				$mail_merchant->setFrom($this->config->get('config_email'));
				$mail_merchant->setSender($store_name);
				$mail_merchant->setSubject($subject);
				$mail_merchant->setHtml($html_merchant);
				$mail_merchant->send();
			}
			
		}
	}
	
	public function sendNotifConfirmAction($return_id){
		$return_info = $this->getReturnNotif($return_id);
		
		if ($return_info) {
			
			$this->load->model('account/order');
			
			$return_id = $return_info['return_id'];
			$order_id = $return_info['order_id'];
			
			$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
			
			if ($order_query->num_rows) {
				$language_id = $order_query->row['language_id'];
				$store_name = $order_query->row['store_name'];
				$store_url = $order_query->row['store_url'];
			} else {
				$language_id = '';
				$store_name = '';
				$store_url = '';
			}
			
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($language_id);

			if ($language_info) {
				$language_directory = $language_info['directory'];
			} else {
				$language_directory = '';
			}
			
			$language = new Language($language_directory);
			$language->load('mail/return');
			$language->load('default');
			
			$subject = sprintf($language->get('text_new_confirm'), $store_name, $return_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($language->get('text_new_confirm'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), $return_id);

			$data['text_action_change'] = $language->get('text_action_change');
			$data['text_update_return_status'] = $language->get('text_update_return_status');
			$data['text_new_link'] = $language->get('text_new_link');
			$data['text_new_return_detail'] = $language->get('text_new_return_detail');
			$data['text_new_return_id'] = $language->get('text_new_return_id');
			$data['text_new_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_date_added');
			$data['text_date_returned'] = $language->get('text_date_returned');
			$data['text_invoice'] = $language->get('text_invoice');
			$data['text_customer'] = $language->get('text_customer');
			$data['text_telephone'] = $language->get('text_telephone');
			$data['text_email'] = $language->get('text_email');
			$data['text_yes'] = $language->get('text_yes');
			$data['text_no'] = $language->get('text_no');
			$data['text_information'] = $language->get('text_information');
			$data['text_prod_name'] = $language->get('text_prod_name');
			$data['text_prod_model'] = $language->get('text_prod_model');
			$data['text_prod_qty'] = $language->get('text_prod_qty');
			$data['text_prod_open'] = $language->get('text_prod_open');
			$data['text_ret_reason'] = $language->get('text_ret_reason');
			$data['text_ret_comment'] = $language->get('text_ret_comment');
			$data['text_ret_status'] = $language->get('text_ret_status');
			$data['text_ret_action'] = $language->get('text_ret_action');
			$data['text_update_footer'] = $language->get('text_update_footer');
			$data['text_merchant_complete'] = $language->get('text_merchant_complete');
			$data['text_merchant_link'] = $language->get('text_merchant_link');
			$data['text_new_confirm'] = $language->get('text_new_confirm');
			$data['text_action_confirm'] = $language->get('text_action_confirm');
			$data['text_discussion_link'] = $language->get('text_discussion_link');

			$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			
			$data['link'] = $store_url . 'index.php?route=account/return/info&return_id=' . $return_id;
			$data['link_merchant'] = $store_url . 'index.php?route=seller/account-return-list/detail&return_id=' . $return_id;
			
			$data['order_id'] = $order_id;
			$data['return_id'] = $return_id;
			$data['store_name'] = $store_name;
			$data['store_url'] = $store_url;
			$data['firstname'] = $return_info['firstname'];
			$data['lastname'] = $return_info['lastname'];
			$data['email'] = $return_info['email'];
			$data['telephone'] = $return_info['telephone'];
			$data['product'] = $return_info['product'];
			$data['model'] = $return_info['model'];
			$data['quantity'] = $return_info['quantity'];
			
			if($return_info['opened'] == 0){
				$data['opened'] = $language->get('text_no');
			}else if($return_info['opened'] == 1){
				$data['opened'] = $language->get('text_yes');
			}
			
			$data['reason'] = $return_info['reason'];
			$data['action'] = $return_info['action'];
			$data['status'] = $return_info['status'];
			$data['comment'] = $return_info['comment'];
			
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($return_info['date_added']));
			$data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($return_info['date_ordered']));
			$data['date_modified'] = date($this->language->get('date_format_short'), strtotime($return_info['date_modified']));
			
			$data['invoice_no'] = $return_info['invoice_no'];
			$data['product_id'] = $return_info['product_id'];
			$data['return_status_id'] = $return_info['return_status_id'];
			$data['confirm_action_changed'] = $return_info['confirm_action_changed'];

			$email_merchant = $this->model_account_order->getMerchantInfo($return_info['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/return_action_change.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/return_action_change.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/return_action_change.tpl', $data);
			}
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($email_merchant);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
		}
	}
	
	public function getReturnsDashboard($customer_id,$limit){
		$sql = "SELECT 
				r.return_id,
				r.product,
				CONCAT(od.invoice_prefix,'',od.invoice_no) as invoice,
				rs.name as status
				FROM " . DB_PREFIX . "return r
				LEFT JOIN " . DB_PREFIX . "order_detail od ON od.order_detail_id=r.order_detail_id
				LEFT JOIN " . DB_PREFIX . "order o ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "return_status rs ON rs.return_status_id=r.return_status_id
				WHERE o.customer_id='".(int)$customer_id."' AND rs.language_id='" . (int)$this->config->get('config_language_id') . "'
				ORDER BY r.date_added DESC
				LIMIT 0,".$limit;
				
		$res = $this->db->query($sql);
		return $res->rows;
	}
}