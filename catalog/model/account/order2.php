<?php
class ModelAccountOrder extends Model {
	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND order_status_id > '0'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
				'ip'                      => $order_query->row['ip']
			);
		} else {
			return false;
		}
	}

	public function getOrders($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 1;
		}

		$query = $this->db->query("SELECT o.order_id, o.firstname, o.lastname, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value, o.order_status_id FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND o.order_status_id > '0' AND o.store_id = '" . (int)$this->config->get('config_store_id') . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.order_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	//edit by arin
	public function getOrderDetailId($order_detail_id){
		$query = $this->db->query("SELECT order_detail_id AS order_detail_id FROM " . DB_PREFIX . "order_detail WHERE order_detail_id = '".(int)$order_detail_id."'");

		return $query->row['order_detail_id'];
	}
	
	public function getOrderDetail($order_detail_id) {
		$order_query = $this->db->query("SELECT od.order_id, od.invoice_prefix, od.invoice_no, o.store_id, o.store_name, o.store_url, o.customer_id, o.firstname, o.lastname, o.telephone, o.fax, o.email, o.payment_firstname, o.payment_lastname, o.payment_company, o.payment_address_1, o.payment_address_2, o.payment_postcode, o.payment_city, o.payment_zone_id, o.payment_zone, o.payment_country_id, o.payment_country, o.payment_address_format, o.payment_method, od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, o.language_id, o.currency_id, od.currency_code, od.currency_value, od.date_modified, od.date_added, o.ip, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.shipping_receipt_number, od.pp_name, od.pp_email, od.pp_telephone, pb.company, pb.country_id, pb.zone_id, pb.city_id, pb.district_id, pb.subdistrict_id, pb.postcode, pb.address, ov.to_name, ov.to_email, ov.code, o.total as order_total, ov.description, ov.message, od.seller_id, ov.from_name, ov.from_email FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb ON (od.pp_branch_id = pb.pp_branch_id) LEFT JOIN " . DB_PREFIX . "order_voucher ov ON (od.order_detail_id = ov.order_detail_id) WHERE od.order_detail_id = '" . (int)$order_detail_id . "' AND o.customer_id = '" . (int)$this->customer->getId() . "' AND od.order_status_id > '0'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			//address pickup point
			
			$country_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['country_id'] . "'");

			if ($country_query_pp->num_rows) {
				$country_pp = $country_query_pp->row['name'];
			} else {
				$country_pp = '';
			}
			
			$zone_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['zone_id'] . "'");

			if ($zone_query_pp->num_rows) {
				$zone_pp = $zone_query_pp->row['name'];
			} else {
				$zone_pp = '';
			}
			
			$city_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$order_query->row['city_id'] . "'");

			if ($city_query_pp->num_rows) {
				$city_pp = $city_query_pp->row['name'];
			} else {
				$city_pp = '';
			}
			
			$district_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$order_query->row['district_id'] . "'");

			if ($district_query_pp->num_rows) {
				$district_pp = $district_query_pp->row['name'];
			} else {
				$district_pp = '';
			}
			
			$subdistrict_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$order_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query_pp->num_rows) {
				$subdistrict_pp = $subdistrict_query_pp->row['name'];
			} else {
				$subdistrict_pp = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'seller_id'             => $order_query->row['seller_id'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
				'ip'                      => $order_query->row['ip'],
				'delivery_type'           => $order_query->row['delivery_type'],
				'shipping_id'             => $order_query->row['shipping_id'],
				'shipping_name'           => $order_query->row['shipping_name'],
				'shipping_service_name'   => $order_query->row['shipping_service_name'],
				'shipping_insurance'      => $order_query->row['shipping_insurance'],
				'shipping_price'          => $order_query->row['shipping_price'],
				'pp_branch_id'            => $order_query->row['pp_branch_id'],
				'pp_branch_name'          => $order_query->row['pp_branch_name'],
				'delivery_type_id'        => $order_query->row['delivery_type_id'],
				'shipping_receipt_number' => $order_query->row['shipping_receipt_number'],
				'pp_name' 			  	  => $order_query->row['pp_name'],
				'pp_email' 			  	  => $order_query->row['pp_email'],
				'pp_telephone' 			  => $order_query->row['pp_telephone'],
				'company' 			  	  => $order_query->row['company'],
				'postcode'		  	  	  => $order_query->row['postcode'],
				'address' 			  	  => $order_query->row['address'],
				'country_pp' 			  => $country_pp,
				'zone_pp' 			  	  => $zone_pp,
				'city_pp' 			  	  => $city_pp,
				'district_pp' 			  => $district_pp,
				'subdistrict_pp' 		  => $subdistrict_pp,
				'to_name' 		  		  => $order_query->row['to_name'],
				'to_email' 		  		  => $order_query->row['to_email'],
				'code'	 		  		  => $order_query->row['code'],
				'order_total'	  		  => $order_query->row['order_total'],
				'description'	  		  => $order_query->row['description'],
				'message'		  		  => $order_query->row['message'],
				'from_name'		  		  => $order_query->row['from_name'],
				'from_email'		  		  => $order_query->row['from_email']
			);
		} else {
			return false;
		}
	}
	
	public function getInvoiceConfirmationById($order_id) {
		$query = $this->db->query("SELECT invoice_prefix, invoice_no FROM " . DB_PREFIX . "order_detail WHERE order_id = '" . (int)$order_id . "' AND order_status_id > '0'");

		return $query->rows;
	}
	
	public function getTotalConfirmationById($order_id) {
		$query = $this->db->query("SELECT total as total FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}
	
	public function getOrderDetails($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 1;
		}

		$query = $this->db->query("SELECT od.order_id, od.order_detail_id, od.invoice_no, od.invoice_prefix, od.shipping_firstname, od.shipping_lastname, os.name as status, od.date_added, od.total, od.currency_code, od.currency_value, od.order_status_id, od.shipping_insurance, od.delivery_type_id, od.pp_name, ov.to_name, o.total as order_total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order_status os ON (od.order_status_id = os.order_status_id) LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "order_voucher ov ON (od.order_detail_id = ov.order_detail_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND od.order_status_id > '0' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY od.order_detail_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getCountOrderIdFromDetails($order_id){
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND od.order_status_id > '0' AND od.order_id = '".(int)$order_id."'");

		return $query->row['total'];
	}
	
	public function getCountVoucherFromDetails($order_id){
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "order_voucher ov ON (od.order_id = ov.order_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND od.order_status_id > '0' AND od.order_id = '".(int)$order_id."'");

		return $query->row['total'];
	}
	
	public function getQuantity($order_detail_id){
		$query = $this->db->query("SELECT SUM(quantity) AS qty FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order_detail od ON (op.order_detail_id = od.order_detail_id) WHERE op.order_detail_id='".(int)$order_detail_id."'");

		return $query->row['qty'];
	}
	
	public function getOrderProductDetails($order_id, $order_detail_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND order_detail_id = '" . (int)$order_detail_id . "'");

		return $query->rows;
	}
	
	public function getOrderHistoryDetails($order_id, $order_detail_id) {
		$query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify, od.order_detail_id, oh.identity, os.description, os.order_status_id FROM " . DB_PREFIX . "order_history_detail oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id  LEFT JOIN " . DB_PREFIX . "order_detail od ON oh.order_detail_id = od.order_detail_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' AND od.order_detail_id = '".(int)$order_detail_id."' ORDER BY oh.date_added DESC");
		return $query->rows;
	}
	
	public function getTotalOrderProductsByInvoice($order_id, $order_detail_id) {
		$query = $this->db->query("SELECT sum(quantity) AS total FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND order_detail_id='".(int)$order_detail_id."'");

		return $query->row['total'];
	}
	
	public function getTotalPricePerInvoice($order_detail_id){
		$query = $this->db->query("SELECT SUM(total) AS total FROM " . DB_PREFIX . "order_product WHERE order_detail_id='".(int)$order_detail_id."'");

		return $query->row['total'];
	}
	
	public function getTotalPriceVoucher($order_id){
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "order_voucher WHERE order_id='".(int)$order_id."'");

		return $query->row['total'];
	}
	
	public function isSuccess($order_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_detail WHERE order_status_id = 2 AND order_id='".(int)$order_id."'");
		
		if($query->num_rows){
			return true;
		}else {
			return false;
		}
	}
	
	// public function isConfirmed($order_id){
		// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_detail od ON od.order_id = o.order_id WHERE od.order_status_id = 1 AND od.order_id=".(int)$order_id." AND o.customer_id = ".(int)$this->customer->getId() ."");
		
		// if($query->num_rows){
			// return true;
		// }else {
			// return false;
		// }
	// }
	
	public function isConfirmed($order_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_detail od ON od.order_id = o.order_id WHERE od.order_status_id = 1 AND od.order_id=".(int)$order_id."");
		
		if($query->num_rows){
			return true;
		}else {
			return false;
		}
	}
	
	public function isCompleteReturn($order_detail_id, $product_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "return WHERE order_detail_id='".(int)$order_detail_id."' AND product_id='".(int)$product_id."' ORDER BY date_added DESC LIMIT 1");
		
		if ($query->num_rows) {
			return array(
				'return_status_id'       => $query->row['return_status_id']
			);
		} else {
			return false;
		}
		
	}
	
	public function checkOrder($order_id){
		$query = $this->db->query("SELECT distinct(delivery_type_id) FROM " . DB_PREFIX . "order_detail WHERE order_id = '".(int)$order_id."'");
		
		return $query->rows;
	}
	
	/*------------------------*/
	
	public function getOrderProduct($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->row;
	}

	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}
	
	public function getOrderProductsByOrderDetail($order_detail_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");

		return $query->rows;
	}

	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}

	public function getOrderVouchers($order_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}
	
	public function getTotalOrderVouchers($order_id) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

	public function getOrderHistories($order_id) {
		$query = $this->db->query("SELECT date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added");

		return $query->rows;
	}
	
	public function getTotalOrders() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o WHERE customer_id = '" . (int)$this->customer->getId() . "' AND o.order_status_id > '0' AND o.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row['total'];
	}

	public function getTotalOrderProductsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrderVouchersByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}
	
	public function receiveOrder($order_detail_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_detail WHERE order_status_id = 19 AND delivery_type <> 3 AND order_detail_id='".(int)$order_detail_id."'");
		
		if($query->num_rows){
			$order_detail=$query->row;
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 21,date_modified=NOW() WHERE order_detail_id = '".(int)$order_detail_id."'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$order_detail['order_id']."', order_detail_id = '".(int)$order_detail['order_detail_id']."', order_status_id = 21, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Customer', date_added=NOW()");
		}
	}
	
	public function completeOrder($order_detail_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_detail WHERE order_status_id = 21 AND delivery_type <> 3 AND order_detail_id='".(int)$order_detail_id."'");
		
		if($query->num_rows){
			$order_detail=$query->row;
			$order_id=(int)$order_detail['order_id'];
			$order_status_id=5;
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 5,date_modified=NOW() WHERE order_detail_id = '".(int)$order_detail_id."'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$order_detail['order_id']."', order_detail_id = '".(int)$order_detail['order_detail_id']."', order_status_id = 5, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Customer', date_added=NOW()");
			
			// order status is changed
			    $this->language->load('multiseller/multiseller');
				$ms_order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");

                // order completed
				if (in_array($order_status_id, $this->config->get('msconf_credit_order_statuses'))) {
					$sendmail = false;
					$seller_id=0;
					if($ms_order_product_query->num_rows){
					foreach ($ms_order_product_query->rows as $order_product) {
						// increment sold counter
						$this->db->query("UPDATE " . DB_PREFIX . "ms_product SET number_sold  = (number_sold + " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");
						$seller_id = $this->MsLoader->MsProduct->getSellerId($order_product['product_id']);
						if (!$seller_id) continue;
						
						// check adaptive payments
						$payment = $this->MsLoader->MsPayment->getPayments(array(
							'order_id' => $order_id,
							'order_detail_id' => $order_detail_id,
							'seller_id' => $seller_id,
							'payment_type' => array(MsPayment::TYPE_SALE),
							'payment_status' => array(MsPayment::STATUS_PAID),
							'single' => 1
						));
						
						if ($payment) {
							$sendmail = true;
							continue;
						}
						
						$balance_entry = $this->MsLoader->MsBalance->getBalanceEntry(
							array(
								'seller_id' => $seller_id,
								'product_id' => $order_product['product_id'],
								'order_id' => $order_id,
								'order_detail_id' => $order_detail_id,
								'balance_type' => MsBalance::MS_BALANCE_TYPE_SALE
							)
						);
						
						if (!$balance_entry) {
                            $order_data = $this->MsLoader->MsOrderData->getOrderData(
                                array(
                                    'product_id' => $order_product['product_id'],
                                    'order_id' => $order_product['order_id'],
                                    'single' => 1
                                )
                            );

							$this->MsLoader->MsBalance->addBalanceEntry(
								$seller_id,
								array(
									'order_id' => $order_product['order_id'],
									'order_detail_id' => $order_product['order_detail_id'],
									'product_id' => $order_product['product_id'],
									'balance_type' => MsBalance::MS_BALANCE_TYPE_SALE,
									'amount' => $order_data['seller_net_amt'],
									'description' => sprintf($this->language->get('ms_transaction_sale'),  ($order_product['quantity'] > 1 ? $order_product['quantity'] . ' x ' : '')  . $order_product['name'], $this->currency->format($order_data['store_commission_flat'] + $order_data['store_commission_pct'], $this->config->get('config_currency')))
								)
							);
							$sendmail = true;
						} else {
							// send order status change mails
						}
					}
						$this->MsLoader->MsBalance->addBalanceEntry($seller_id,
							array(
								'order_id' => $order_id,
								'order_detail_id' => $order_detail_id,
								'balance_type' => MsBalance::MS_BALANCE_TYPE_SHIPPING,
								'amount' => $order_detail['shipping_price'],
								'description' => $this->language->get('ms_transaction_shipping')
							)
						);
					}
					
					
						
					
					if ($sendmail) $this->MsLoader->MsMail->sendOrderMails($order_id);
				} else if (in_array($order_status_id, $this->config->get('msconf_debit_order_statuses'))) {
				// order refunded
				$sendmail = false;
				foreach ($ms_order_product_query->rows as $order_product) {
					// decrement sold counter
					$this->db->query("UPDATE " . DB_PREFIX . "ms_product SET number_sold  = (number_sold - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");
				
					$seller_id = $this->MsLoader->MsProduct->getSellerId($order_product['product_id']);
					if (!$seller_id) continue;
					$refund_balance_entry = $this->MsLoader->MsBalance->getBalanceEntry(
						array(
							'seller_id' => $seller_id,
							'product_id' => $order_product['product_id'],
							'order_id' => $order_id,
							'order_detail_id' => $order_detail_id,
							'balance_type' => MsBalance::MS_BALANCE_TYPE_REFUND
						)
					);
					
					if (!$refund_balance_entry) {
						$balance_entry = $this->MsLoader->MsBalance->getBalanceEntry(
							array(
								'seller_id' => $seller_id,
								'product_id' => $order_product['product_id'],
								'order_id' => $order_id,
								'order_detail_id' => $order_detail_id,
								'balance_type' => MsBalance::MS_BALANCE_TYPE_SALE
							)
						);
				
						if ($balance_entry) {
							$this->MsLoader->MsBalance->addBalanceEntry(
								$balance_entry['seller_id'],
								array(
									'order_id' => $balance_entry['order_id'],
									'order_detail_id' => $balance_entry['order_detail_id'],
									'product_id' => $balance_entry['product_id'],
									'balance_type' => MsBalance::MS_BALANCE_TYPE_REFUND,
									'amount' => -1 * $balance_entry['amount'],
									'description' => sprintf($this->language->get('ms_transaction_refund'),  ($order_product['quantity'] > 1 ? $order_product['quantity'] . ' x ' : '')  . $order_product['name'])
								)
							);
							
							// todo send refund mails
							// $this->MsLoader->MsMail->sendOrderMails($order_id);
						} else {
							// send order status change mails
						}
						
					}
				}
			}
			$this->sendNotification($order_detail_id, 5);
		}
	}
	
	public function getMerchantInfo($product_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_product WHERE product_id='".(int)$product_id."'");
		
		$merchant_id = $query->row['seller_id'];
		
		$query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id='".(int)$merchant_id."'");
		
		return $query2->row['email'];
	}
	
	public function sendNotification($order_detail_id, $status_id){
		$this->load->model('seller/account_pending_order');
		
		$order_info = $this->model_seller_account_pending_order->getOrderNotification($order_detail_id);
		
		if ($order_info) {
			// Fraud Detection
			$this->load->model('account/customer');
			$this->load->model('catalog/product');

			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');

				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

				if (!$safe && $risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Ban IP
			if (!$safe) {
				$status = false;

				if ($order_info['customer_id']) {
					$results = $this->model_account_customer->getIps($order_info['customer_id']);

					foreach ($results as $result) {
						if ($this->model_account_customer->isBanIp($result['ip'])) {
							$status = true;

							break;
						}
					}
				} else {
					$status = $this->model_account_customer->isBanIp($order_info['ip']);
				}

				if ($status) {
					$order_status_id = $this->config->get('config_order_status_id');
				}
			}
			
			$order_id = $order_info['order_id'];
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");
			
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
			if ($order_status_query->num_rows) {
				$order_status = $order_status_query->row['name'];
			} else {
				$order_status = '';
			}
			
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($order_info['language_id']);

			if ($language_info) {
				$language_directory = $language_info['directory'];
			} else {
				$language_directory = '';
			}
			
			$language = new Language($language_directory);
			$language->load('mail/order');
			$language->load('default');
			
			$subject = sprintf($language->get('text_subject'), $order_info['store_name'], $order_id, $order_detail_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($language->get('text_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id, $order_detail_id);

			$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$data['text_link'] = $language->get('text_new_link');
			$data['text_download'] = $language->get('text_new_download');
			$data['text_order_detail'] = $language->get('text_new_order_detail');
			$data['text_instruction'] = $language->get('text_new_instruction');
			$data['text_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_new_date_added');
			$data['text_payment_method'] = $language->get('text_new_payment_method');
			$data['text_shipping_method'] = $language->get('text_new_shipping_method');
			$data['text_email'] = $language->get('text_new_email');
			$data['text_telephone'] = $language->get('text_new_telephone');
			$data['text_ip'] = $language->get('text_new_ip');
			$data['text_order_status'] = $language->get('text_new_order_status');
			$data['text_payment_address'] = $language->get('text_new_payment_address');
			$data['text_shipping_address'] = $language->get('text_new_shipping_address');
			$data['text_product'] = $language->get('text_new_product');
			$data['text_model'] = $language->get('text_new_model');
			$data['text_quantity'] = $language->get('text_new_quantity');
			$data['text_price'] = $language->get('text_new_price');
			$data['text_total'] = $language->get('text_new_total');
			$data['text_footer'] = $language->get('text_new_footer');
			$data['text_shipping_price'] = $language->get('text_shipping_price');
			$data['text_receipt_number'] = $language->get('text_receipt_number');
			$data['text_merchant_complete'] = $language->get('text_merchant_complete');
			$data['text_merchant_link'] = $language->get('text_merchant_link');

			$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			$data['store_name'] = $order_info['store_name'];
			$data['store_url'] = $order_info['store_url'];
			$data['customer_id'] = $order_info['customer_id'];
			$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . '&order_detail_id=' . $order_detail_id;
			
			$data['order_id'] = $order_id;
			$data['order_detail_id'] = $order_detail_id;
			$data['payment_method'] = $order_info['payment_method'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];
			$data['order_status'] = $order_status;
			
			$data['delivery_type_id'] = $order_info['delivery_type_id'];
			
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);
		
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}';
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}'
			);

			$replace_pp = array(
				'pp_name'		=> $order_info['pp_name'],
				'company' 		=> $order_info['company'],
				'pp_branch_name'=> $order_info['pp_branch_name'],
				'address'      	=> $order_info['address'],
				'subdistrict_pp'=> $order_info['subdistrict_pp'],
				'district_pp'   => $order_info['district_pp'],
				'city_pp' 		=> $order_info['city_pp'],
				'zone_pp'      	=> $order_info['zone_pp'],
				'postcode' 		=> $order_info['postcode'],
				'country_pp'  	=> $order_info['country_pp']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($order_detail_id);
			
			$data['shipping_address'] = $shipping_address;
			$data['address_pp'] = $address_pp;
			$data['total_price_invoice'] = $this->currency->format($total_price_invoice, $order_info['currency_code'], $order_info['currency_value']);
			$data['shipping_price'] = $this->currency->format($order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			$data['shipping_name'] = $order_info['shipping_name'];
			$data['shipping_service_name'] = $order_info['shipping_service_name'];
			$data['pp_branch_name'] = $order_info['pp_branch_name'];
			$data['delivery_type'] = $order_info['delivery_type'];
			$data['shipping_receipt_number'] = $order_info['shipping_receipt_number'];
			$data['order_status_id'] = $order_info['order_status_id'];
			
			/*data product*/
			$data['products'] = array();
			
			$products = $this->model_seller_account_pending_order->getProductOrder($order_id, $order_detail_id);
			
			foreach ($products as $product) {	
					
				$data['products'][] = array(
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'model' 				=> $product['model'],
					'nickname' 				=> $product['nickname'],
					'company' 				=> $product['company'],
					'total'					=> $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']),
					'price'					=> $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'])
				);
				$product_id = $product['product_id'];
			}
			
			$email_merchant = $this->getMerchantInfo($product_id);
			
			// Vouchers
			$data['vouchers'] = array();

			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_voucher_query->rows as $voucher) {
				$data['vouchers'][] = array(
					'description' 	=> $voucher['description'],
					'code' 			=> $voucher['code'],
					'to_name' 		=> $voucher['to_name'],
					'to_email' 		=> $voucher['to_email'],
					'amount'      	=> $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
				);
				$data['to_name']  = $voucher['to_name'];
				$data['to_email'] = $voucher['to_email'];
			}
			
			$data['text_order_id']  = $language->get('text_update_order');
			$data['text_order_detail_id']  = $language->get('text_update_order_detail');
			$data['text_date_added'] = $language->get('text_update_date_added');

			if ($order_status) {
				$data['text_update_order_status'] = $language->get('text_update_order_status');
			}

			if ($order_info['customer_id']) {
				$data['text_update_link'] = $language->get('text_update_link');
				$data['text_link_direct'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . '&order_detail_id=' . $order_detail_id;
			}

			$data['text_footer'] = $language->get('text_update_footer');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order_complete.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/order_complete.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/order_complete.tpl', $data);
			}
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($email_merchant);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
		}
	}
	
	
	
	public function getCountNotificationOrder($customer_id){
		$sql = "SELECT count(1) as total
				FROM `" . DB_PREFIX . "order_detail` od
				LEFT JOIN `" . DB_PREFIX . "order` o ON o.order_id=od.order_id
				WHERE o.customer_id='" . (int)$customer_id."' AND od.order_status_id NOT IN(5,23)";
				
		$res = $this->db->query($sql);
		return $order_total=$res->row['total'];
	}
	
	public function getOrdersDashboard($customer_id,$limit){
		$sql = "SELECT 
				od.order_id,
				od.order_detail_id,
				CONCAT(od.invoice_prefix,'',od.invoice_no) as invoice,
				os.name as status
				FROM " . DB_PREFIX . "order_detail od
				LEFT JOIN " . DB_PREFIX . "order o ON o.order_id=od.order_id
				LEFT JOIN " . DB_PREFIX . "order_status os ON os.order_status_id=od.order_status_id
				WHERE o.customer_id='".(int)$customer_id."'
				ORDER BY od.date_added DESC
				LIMIT 0,".$limit;
				
		$res = $this->db->query($sql);
		return $res->rows;
	}
}