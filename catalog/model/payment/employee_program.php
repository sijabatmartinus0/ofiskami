<?php
class ModelPaymentEmployeeProgram extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/employee_program');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('employee_program_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('employee_program_total') > 0 && $this->config->get('employee_program_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('employee_program_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		if (!empty($this->config->get('employee_program_customer_group')) && in_array($this->customer->getGroupId(),$this->config->get('employee_program_customer_group'))) {
			$status = true;
		}else {
			$status = false;
		}
		
		if ($total <= 0.00) {
			$status = false;
		} else {
			$status = true;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'employee_program',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('employee_program_sort_order')
			);
		}

		return $method_data;
	}
}