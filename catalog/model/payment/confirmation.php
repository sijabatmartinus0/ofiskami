<?php
class ModelPaymentConfirmation extends Model {
	public function insertPaymentConfirmation($data) {
		$this->event->trigger('pre.payment.add', $data);
		
		//rename variable
		$this->load->model('account/order');
		$order_id = $this->db->escape($data['input_order_id']); 
		$customer_info = $this->model_account_order->getOrder($order_id);
		$today = date("Ymd");
		$customer_id = $customer_info['customer_id'];
		
		$payment_method = explode('-', $data['input_method_payment']);
		$payment_method_id = $payment_method[0];
		$payment_method_name = $payment_method[1];
		
		if (!empty($_FILES['picture_confirmation']['tmp_name'])){
			//create new directory
			mkdir(DIR_IMAGE . '/payment_confirmation/' . $order_id . '/', 0777, true);
			$folder = DIR_IMAGE . '/payment_confirmation/' . $order_id . '/';
			
			//setting the new name
			$pict_format = explode(".", basename($_FILES['picture_confirmation']['name']));
			$picture_payment_confirmation='payment_confirmation/' . $order_id . '/'.$order_id.'_'.$customer_id.'_'.$today.'.'.$pict_format[1];
			
			$folder = $folder . basename($_FILES['picture_confirmation']['name']);
			
			move_uploaded_file($_FILES['picture_confirmation']['tmp_name'], $folder);
			
			rename($folder, DIR_IMAGE . '/payment_confirmation/' . $order_id . '/'.$order_id.'_'.$customer_id.'_'.$today.'.'.$pict_format[1]);
		}else{
			$picture_payment_confirmation = '';
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_confirmation SET order_id = '".(int)$order_id."', date_payment = '" . $this->db->escape($data['input_date_payment']) . "', date_added = NOW(), date_modified = NOW(), account_id = '" . (int)$this->db->escape($data['id-account-bank']) . "', account_name = '" . $this->db->escape($data['input_account_name']) . "', account_number = '" . $this->db->escape($data['input_account_number']) . "', account_branch = '" . $this->db->escape($data['input_branch']) . "', account_destination_id = '" . (int)$this->db->escape($data['input_account_destination']) . "', total = '" . (float)$this->db->escape($data['input_amount']) . "', information = '" . (isset($data['input_remarks']) ? $data['input_remarks'] : '') . "', image='".$picture_payment_confirmation."', payment_method_id = '". (int)$payment_method_id ."', payment_method = '".$payment_method_name."', status = '1'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_confirmation_history SET order_id = '".(int)$this->db->escape($data['input_order_id'])."',  date_payment = '" . $this->db->escape($data['input_date_payment']) . "', date_added = NOW(), date_modified = NOW(), account_id = '" . (int)$this->db->escape($data['id-account-bank']) . "', account_name = '" . $this->db->escape($data['input_account_name']) . "', account_number = '" . $this->db->escape($data['input_account_number']) . "', account_branch = '" . $this->db->escape($data['input_branch']) . "', account_destination_id = '" . (int)$this->db->escape($data['input_account_destination']) . "', total = '" . (float)$this->db->escape($data['input_amount']) . "', information = '" . (isset($data['input_remarks']) ? $data['input_remarks'] : '') . "', image='".$picture_payment_confirmation."', payment_method_id = '". (int)$payment_method_id ."', payment_method = '".$payment_method_name."', status = '1'");
		
		$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = '2' WHERE order_id = '".$order_id."'");
		
		$this->event->trigger('post.payment.add', $order_payment_confirmation_id);

		return $order_payment_confirmation_id;
	}
	
	public function getTotalPaymentById($order_payment_confirmation_id){
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_payment_confirmation WHERE order_id = '" . (int)$order_payment_confirmation_id . "' AND status = '1'");

		return $query->row;	
	}
	
	public function insertDetailHistory($order_id, $order_detail_id, $comment='', $notify = false){
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$order_id."', order_detail_id='".(int)$order_detail_id."', notify='".(int)$notify."', comment='".$this->db->escape($comment)."', order_status_id = '2', user_id='".(int)$this->customer->getId()."', identity = 'Customer', date_added=NOW()");
	}
	
	public function getOrderDetailId($order_id) {
		$query = $this->db->query("SELECT order_detail_id FROM " . DB_PREFIX . "order_detail WHERE order_id = '" . (int)$order_id . "' AND order_status_id > '0'");

		return $query->rows;
	}
	
	public function getPaymentMethods(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment_method WHERE language_id = '".(int)$this->config->get('config_language_id')."'");
		
		return $query->rows;
	}
}