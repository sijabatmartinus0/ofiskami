<?php
class ModelShippingShipping extends Model {
	/*Dummy Data*/
	public function getShippingByCustomerId($customer_id) {
		$sql="
		SELECT 
		os.shipping_id,
		os.name as shipping,
		oss.shipping_service_id,
		oss.name as shipping_service,
		oss.description,
		ossp.price,
		ossp.insurance 
		FROM " . DB_PREFIX . "shipping os
		LEFT JOIN " . DB_PREFIX . "shipping_service oss ON os.shipping_id=oss.shipping_id
		LEFT JOIN " . DB_PREFIX . "shipping_service_price ossp ON oss.shipping_id=ossp.shipping_id AND oss.shipping_service_id=ossp.shipping_service_id
		";
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getShippingCode($shipping_id){
		$result="";
		$sql="SELECT code FROM " . DB_PREFIX . "shipping WHERE shipping_id=".(int)$shipping_id;
		$query = $this->db->query($sql);

		if(isset($query->row['code'])){
			$result=$query->row['code'];
		}
		return $result;
	}
	
	public function getShipping($shipping_id){
		$result="";
		$sql="SELECT name FROM " . DB_PREFIX . "shipping WHERE shipping_id=".(int)$shipping_id;
		$query = $this->db->query($sql);

		if(isset($query->row['name'])){
			$result=$query->row['name'];
		}
		return $result;
	}
	
	public function getShippingService($shipping_service_id){
		$result="";
		$sql="SELECT name FROM " . DB_PREFIX . "shipping_service WHERE status=1 AND shipping_service_id=".(int)$shipping_service_id;
		$query = $this->db->query($sql);

		if(isset($query->row['name'])){
			$result=$query->row['name'];
		}
		return $result;
	}
}