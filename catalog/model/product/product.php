<?php
class ModelProductProduct extends Model {
	public function addProductComment($data=array()){
		$this->load->model('catalog/product');
		$seller_id = $this->model_catalog_product->getMsProductById($data['product_id']);
		$created_by = $this->customer->getId();
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "topic_comments SET customer_id='".(int)$created_by."', product_id='".(int)$data['product_id']."', seller_id='".(int)$seller_id."', topic='".$this->db->escape($data['comment'])."',  created_date = NOW(), modified_date=NOW(), status=1");
		
		$id_topic = $this->db->getLastId();
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_comments SET id_topic='".(int)$id_topic."', created_by='".(int)$created_by."', comment='".$this->db->escape($data['comment'])."', created_date = NOW(), status=1");
		
		$this->sendNotifComment($id_topic, $created_by, 1);
	}
	
	public function replyComment($data=array()){
		$created_by = $this->customer->getId();
		
		$this->db->query("INSERT INTO ". DB_PREFIX ."product_comments SET id_topic='".(int)$data['reply_id_topic']."', created_by='".(int)$created_by."', comment='".$this->db->escape($data['reply_comment'])."', created_date=NOW(), status=1");
		
		$this->db->query("UPDATE ". DB_PREFIX ."topic_comments SET modified_date=NOW() WHERE id_topic='".(int)$data['reply_id_topic']."'");
		
		$this->sendNotifComment($data['reply_id_topic'], $created_by, 2);
	}
	
	public function listTopicComments($data=array(), $sort = array()){
		$query = $this->db->query("SELECT tc.id_topic, tc.customer_id, tc.product_id, tc.seller_id, tc.topic, c.firstname, c.email, s.nickname, tc.created_date FROM " . DB_PREFIX . "topic_comments tc LEFT JOIN " . DB_PREFIX . "customer c ON tc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON tc.seller_id = s.seller_id WHERE tc.status=1 AND tc.product_id='".(int)$data['product_id']."' ".(isset($data['topic_id']) ? " AND tc.id_topic=" .(int)$data['topic_id'] : "")." ORDER BY tc.modified_date DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");

		return $query->rows;
	}
	
	public function countTopicComments($data=array()){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "topic_comments WHERE product_id='".(int)$data['product_id']."' ".(isset($data['topic_id']) ? " AND id_topic=" .(int)$data['topic_id'] : "")." AND status=1");

		return $query->row['total'];
	}
	
	public function listProductComments($product_id){
		$query = $this->db->query("SELECT tc.id_topic, tc.customer_id, tc.product_id, tc.seller_id, tc.topic, pc.id_comment, pc.comment, pc.created_date, pc.created_by, c.firstname, c.email, s.nickname FROM " . DB_PREFIX . "topic_comments tc LEFT JOIN " .DB_PREFIX . "product_comments pc ON tc.id_topic = pc.id_topic LEFT JOIN " . DB_PREFIX . "customer c ON tc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON tc.seller_id = s.seller_id WHERE tc.status=1 AND pc.status=1 AND tc.product_id='".(int)$product_id."' ORDER BY tc.modified_date DESC");

		return $query->rows;
	}
	
	public function loadCommentsPerTopic($id_topic){
		$query = $this->db->query("SELECT tc.id_topic, tc.customer_id, tc.product_id, tc.seller_id, tc.topic, pc.id_comment, pc.comment, pc.created_date, pc.created_by, c.firstname, c.email, s.nickname FROM " . DB_PREFIX . "topic_comments tc LEFT JOIN " .DB_PREFIX . "product_comments pc ON tc.id_topic = pc.id_topic LEFT JOIN " . DB_PREFIX . "customer c ON tc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON tc.seller_id = s.seller_id WHERE tc.status=1 AND pc.status=1 AND tc.id_topic='".(int)$id_topic."' ORDER BY tc.modified_date DESC");

		return $query->rows;
	}
	
	public function loadTopCommentsPerTopic($id_topic){
		$query = $this->db->query("(SELECT tc.id_topic, tc.customer_id, tc.product_id, tc.seller_id, tc.topic, pc.id_comment, pc.comment, pc.created_date, pc.created_by, c.firstname, c.email, s.nickname FROM " . DB_PREFIX . "topic_comments tc LEFT JOIN " .DB_PREFIX . "product_comments pc ON tc.id_topic = pc.id_topic LEFT JOIN " . DB_PREFIX . "customer c ON tc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON tc.seller_id = s.seller_id WHERE tc.status=1 AND pc.status=1 AND tc.id_topic='".(int)$id_topic."' AND tc.topic != pc.comment ORDER BY pc.created_date DESC LIMIT 2) ORDER BY created_date ASC");

		return $query->rows;
	}
	
	public function sendNotifComment($id_topic, $created_by, $type){
		$product_comments = $this->loadCommentsPerTopic($id_topic);
		
		$store_name = $this->config->get('config_name');
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage((int)$this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
			
		$language = new Language($language_directory);
		$language->load('mail/product');
		$language->load('default');
		
		// HTML Mail
		$data = array();
		
		if($type == 1){ //new product comment
			$subject = sprintf($language->get('text_new_subject'), $store_name);
			$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
		}else if ($type == 2){ //new reply
			$subject = sprintf($language->get('text_reply_subject'), $this->config->get('config_name'));
			$data['title'] = sprintf($language->get('text_reply_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
		}
		
		$data['text_new_reply'] = $language->get('text_new_reply');
		
		$data['text_footer'] = $language->get('text_footer');
		$data['text_merchant'] = $language->get('text_merchant');
		$data['text_customer'] = $language->get('text_customer');
		$data['text_link'] = $language->get('text_link');
		$data['store_name'] = $store_name;
		$data['store_url'] = HTTP_SERVER;
		
		$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			
		/*data product*/
		$data['product_comments'] = array();
			
		foreach ($product_comments as $product_comment) {	
			$data['product_comments'][] = array(
				'id_topic'    			=> $product_comment['id_topic'],
				'customer_id'   		=> $product_comment['customer_id'],
				'seller_id' 			=> $product_comment['seller_id'],
				'created_date'			=> date('d/m/Y H:i', strtotime($product_comment['created_date'])),
				'id_comment'			=> $product_comment['id_comment'],
				'comment' 				=> $product_comment['comment'],
				'created_by' 			=> $product_comment['created_by'],
				'firstname'				=> $product_comment['firstname'],
				'email'					=> $product_comment['email'],
				'nickname'				=> $product_comment['nickname'],
				'product_id'			=> $product_comment['product_id']
			);
			$link = '&product_id=' . $product_comment['product_id'] .'&topic_id='.$product_comment['id_topic'];
			$seller_id = $product_comment['seller_id'];
			$customer_id = $product_comment['customer_id'];
			$product_id = $product_comment['product_id'];
		}
		
		$this->load->model('seller/account_return_list');
		$sent_to = $this->model_seller_account_return_list->getCustomerEmail($seller_id);
		$sent_to_customer = $this->model_seller_account_return_list->getCustomerEmail($customer_id);
		
		$product_query = $this->db->query("SELECT pd.name, p.model FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "product p ON pd.product_id = p.product_id WHERE p.product_id = '" . (int)$product_id . "'");
		
		if($product_query->num_rows){
			$product_name = $product_query->row['name'];
			$product_model = $product_query->row['model'];
		}else{
			$product_name = '';
			$product_model = '';
		}
		
		$data['text_new_comment'] = sprintf($language->get('text_new_comment'), $product_name);
		
		
		
		$data['link'] = HTTP_SERVER . 'index.php?route=seller/account-product/comment'.$link;
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/product_comment.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/product_comment.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/product_comment.tpl', $data);
		}
		
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($sent_to);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($store_name);
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
		
		$data['link'] = HTTP_SERVER . 'index.php?route=product/product'.$link;
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/product_comment.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/product_comment.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/product_comment.tpl', $data);
		}
		
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($sent_to_customer);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($store_name);
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
	}
	
	/* edit by arin */
	public function getRateSuccess($product_id){
		$query_complete = $this->db->query("SELECT COUNT(op.order_product_id) as total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order_detail od ON op.order_detail_id = od.order_detail_id WHERE op.product_id='".(int)$product_id."' AND od.order_status_id = 5");
		
		$complete = (int)$query_complete->row['total'];
		
		$query_reject = $this->db->query("SELECT COUNT(op.order_product_id) as total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order_detail od ON op.order_detail_id = od.order_detail_id WHERE op.product_id='".(int)$product_id."' AND od.order_status_id = 23");
		
		$reject = (int)$query_reject->row['total'];
		
		if($complete == 0 && $reject == 0){
			$score = 0;
		}else{
			$score = ($complete/($complete+$reject))* 100;
		}
		
		// if($complete == 0 && $reject == 0){
			// return 0;
		// }else if($complete == 0 && $reject != 0){
			// return 0;
		// }else{	
			return array(
				'score'       	=> round($score),
				'total_order'	=> $complete+$reject
			);
		// }
	}
}

?>