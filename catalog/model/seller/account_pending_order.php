<?php
class ModelSellerAccountPendingOrder extends Model {
	public function loadShippingData($order_id, $invoice, $status_id) {
		$order_query = $this->db->query("SELECT od.order_id, o.language_id, o.currency_id, o.payment_method, c.firstname, c.lastname, od.order_detail_id, od. seller_id, od.invoice_prefix, od.invoice_no, od.date_added,  od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, od.currency_code, od.currency_value, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.shipping_receipt_number, od.sla_response, od.sla_shipping, od.pp_name, od.pp_email, od.pp_telephone, pb.company, pb.country_id, pb.zone_id, pb.city_id, pb.district_id, pb.subdistrict_id, pb.postcode, pb.address FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb ON (od.pp_branch_id = pb.pp_branch_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = '".(int)$status_id."' AND CONCAT(od.invoice_prefix, od.invoice_no) = '".$invoice."' AND od.order_id='".(int)$order_id."'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			//address pickup point
			
			$country_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['country_id'] . "'");

			if ($country_query_pp->num_rows) {
				$country_pp = $country_query_pp->row['name'];
			} else {
				$country_pp = '';
			}
			
			$zone_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['zone_id'] . "'");

			if ($zone_query_pp->num_rows) {
				$zone_pp = $zone_query_pp->row['name'];
			} else {
				$zone_pp = '';
			}
			
			$city_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$order_query->row['city_id'] . "'");

			if ($city_query_pp->num_rows) {
				$city_pp = $city_query_pp->row['name'];
			} else {
				$city_pp = '';
			}
			
			$district_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$order_query->row['district_id'] . "'");

			if ($district_query_pp->num_rows) {
				$district_pp = $district_query_pp->row['name'];
			} else {
				$district_pp = '';
			}
			
			$subdistrict_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$order_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query_pp->num_rows) {
				$subdistrict_pp = $subdistrict_query_pp->row['name'];
			} else {
				$subdistrict_pp = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'payment_method'          => $order_query->row['payment_method'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'order_detail_id'         => $order_query->row['order_detail_id'],
				'seller_id'          	  => $order_query->row['seller_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'date_added'              => $order_query->row['date_added'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'shipping_id'             => $order_query->row['shipping_id'],
				'shipping_name'           => $order_query->row['shipping_name'],
				'shipping_service_name'   => $order_query->row['shipping_service_name'],
				'shipping_insurance'      => $order_query->row['shipping_insurance'],
				'shipping_price'          => $order_query->row['shipping_price'],
				'pp_branch_id'            => $order_query->row['pp_branch_id'],
				'pp_branch_name'          => $order_query->row['pp_branch_name'],
				'delivery_type'           => $order_query->row['delivery_type'],
				'delivery_type_id'        => $order_query->row['delivery_type_id'],
				'shipping_receipt_number' => $order_query->row['shipping_receipt_number'],
				'sla_response' 			  => $order_query->row['sla_response'],
				'sla_shipping' 			  => $order_query->row['sla_shipping'],
				'pp_name' 			  	  => $order_query->row['pp_name'],
				'pp_email' 			  	  => $order_query->row['pp_email'],
				'pp_telephone' 			  => $order_query->row['pp_telephone'],
				'company' 			  	  => $order_query->row['company'],
				'postcode'		  	  	  => $order_query->row['postcode'],
				'address' 			  	  => $order_query->row['address'],
				'country_pp' 			  => $country_pp,
				'zone_pp' 			  	  => $zone_pp,
				'city_pp' 			  	  => $city_pp,
				'district_pp' 			  => $district_pp,
				'subdistrict_pp' 		  => $subdistrict_pp
			);
		} else {
			return false;
		}
	}
	
	public function loadShippingDataById($order_detail_id) {
		$order_query = $this->db->query("SELECT od.order_id, o.language_id, o.currency_id, o.payment_method, c.firstname, c.lastname, od.order_detail_id, od. seller_id, od.invoice_prefix, od.invoice_no, od.date_added,  od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, od.currency_code, od.currency_value, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.shipping_receipt_number, od.sla_response, od.sla_shipping, od.pp_name, od.pp_email, od.pp_telephone, pb.company, pb.country_id, pb.zone_id, pb.city_id, pb.district_id, pb.subdistrict_id, pb.postcode, pb.address, o.store_name, o.store_url, o.customer_id, o.email, o.telephone FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb ON (od.pp_branch_id = pb.pp_branch_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 18 AND od.sla_shipping >= 0 AND od.order_detail_id = '".(int)$order_detail_id."'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			//address pickup point
			
			$country_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['country_id'] . "'");

			if ($country_query_pp->num_rows) {
				$country_pp = $country_query_pp->row['name'];
			} else {
				$country_pp = '';
			}
			
			$zone_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['zone_id'] . "'");

			if ($zone_query_pp->num_rows) {
				$zone_pp = $zone_query_pp->row['name'];
			} else {
				$zone_pp = '';
			}
			
			$city_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$order_query->row['city_id'] . "'");

			if ($city_query_pp->num_rows) {
				$city_pp = $city_query_pp->row['name'];
			} else {
				$city_pp = '';
			}
			
			$district_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$order_query->row['district_id'] . "'");

			if ($district_query_pp->num_rows) {
				$district_pp = $district_query_pp->row['name'];
			} else {
				$district_pp = '';
			}
			
			$subdistrict_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$order_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query_pp->num_rows) {
				$subdistrict_pp = $subdistrict_query_pp->row['name'];
			} else {
				$subdistrict_pp = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'payment_method'          => $order_query->row['payment_method'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'order_detail_id'         => $order_query->row['order_detail_id'],
				'seller_id'          	  => $order_query->row['seller_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'date_added'              => $order_query->row['date_added'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'shipping_id'             => $order_query->row['shipping_id'],
				'shipping_name'           => $order_query->row['shipping_name'],
				'shipping_service_name'   => $order_query->row['shipping_service_name'],
				'shipping_insurance'      => $order_query->row['shipping_insurance'],
				'shipping_price'          => $order_query->row['shipping_price'],
				'pp_branch_id'            => $order_query->row['pp_branch_id'],
				'pp_branch_name'          => $order_query->row['pp_branch_name'],
				'delivery_type'           => $order_query->row['delivery_type'],
				'delivery_type_id'        => $order_query->row['delivery_type_id'],
				'shipping_receipt_number' => $order_query->row['shipping_receipt_number'],
				'sla_response' 			  => $order_query->row['sla_response'],
				'sla_shipping' 			  => $order_query->row['sla_shipping'],
				'pp_name' 			  	  => $order_query->row['pp_name'],
				'pp_email' 			  	  => $order_query->row['pp_email'],
				'pp_telephone' 			  => $order_query->row['pp_telephone'],
				'company' 			  	  => $order_query->row['company'],
				'postcode'		  	  	  => $order_query->row['postcode'],
				'address' 			  	  => $order_query->row['address'],
				'country_pp' 			  => $country_pp,
				'zone_pp' 			  	  => $zone_pp,
				'city_pp' 			  	  => $city_pp,
				'district_pp' 			  => $district_pp,
				'subdistrict_pp' 		  => $subdistrict_pp,
				'store_name'	 		  => $order_query->row['store_name'],
				'store_url'	 		  	  => $order_query->row['store_url'],
				'customer_id'	 		  => $order_query->row['customer_id'],
				'email'	 		  		  => $order_query->row['email'],
				'telephone'	 		  	  => $order_query->row['telephone']
			);
		} else {
			return false;
		}
	}
	
	public function getOrderNotification($order_detail_id) {
		$order_query = $this->db->query("SELECT od.order_id, o.language_id, o.currency_id, o.payment_method, c.firstname, c.lastname, od.order_detail_id, od. seller_id, od.invoice_prefix, od.invoice_no, od.date_added,  od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, od.currency_code, od.currency_value, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.shipping_receipt_number, od.sla_response, od.sla_shipping, od.pp_name, od.pp_email, od.pp_telephone, pb.company, pb.country_id, pb.zone_id, pb.city_id, pb.district_id, pb.subdistrict_id, pb.postcode, pb.address, o.store_name, o.store_url, o.customer_id, o.email, o.telephone FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb ON (od.pp_branch_id = pb.pp_branch_id) WHERE od.order_detail_id = '".(int)$order_detail_id."'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			//address pickup point
			
			$country_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['country_id'] . "'");

			if ($country_query_pp->num_rows) {
				$country_pp = $country_query_pp->row['name'];
			} else {
				$country_pp = '';
			}
			
			$zone_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['zone_id'] . "'");

			if ($zone_query_pp->num_rows) {
				$zone_pp = $zone_query_pp->row['name'];
			} else {
				$zone_pp = '';
			}
			
			$city_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$order_query->row['city_id'] . "'");

			if ($city_query_pp->num_rows) {
				$city_pp = $city_query_pp->row['name'];
			} else {
				$city_pp = '';
			}
			
			$district_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$order_query->row['district_id'] . "'");

			if ($district_query_pp->num_rows) {
				$district_pp = $district_query_pp->row['name'];
			} else {
				$district_pp = '';
			}
			
			$subdistrict_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$order_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query_pp->num_rows) {
				$subdistrict_pp = $subdistrict_query_pp->row['name'];
			} else {
				$subdistrict_pp = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'payment_method'          => $order_query->row['payment_method'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'order_detail_id'         => $order_query->row['order_detail_id'],
				'seller_id'          	  => $order_query->row['seller_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'date_added'              => $order_query->row['date_added'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'shipping_id'             => $order_query->row['shipping_id'],
				'shipping_name'           => $order_query->row['shipping_name'],
				'shipping_service_name'   => $order_query->row['shipping_service_name'],
				'shipping_insurance'      => $order_query->row['shipping_insurance'],
				'shipping_price'          => $order_query->row['shipping_price'],
				'pp_branch_id'            => $order_query->row['pp_branch_id'],
				'pp_branch_name'          => $order_query->row['pp_branch_name'],
				'delivery_type'           => $order_query->row['delivery_type'],
				'delivery_type_id'        => $order_query->row['delivery_type_id'],
				'shipping_receipt_number' => $order_query->row['shipping_receipt_number'],
				'sla_response' 			  => $order_query->row['sla_response'],
				'sla_shipping' 			  => $order_query->row['sla_shipping'],
				'pp_name' 			  	  => $order_query->row['pp_name'],
				'pp_email' 			  	  => $order_query->row['pp_email'],
				'pp_telephone' 			  => $order_query->row['pp_telephone'],
				'company' 			  	  => $order_query->row['company'],
				'postcode'		  	  	  => $order_query->row['postcode'],
				'address' 			  	  => $order_query->row['address'],
				'country_pp' 			  => $country_pp,
				'zone_pp' 			  	  => $zone_pp,
				'city_pp' 			  	  => $city_pp,
				'district_pp' 			  => $district_pp,
				'subdistrict_pp' 		  => $subdistrict_pp,
				'store_name'	 		  => $order_query->row['store_name'],
				'store_url'	 		  	  => $order_query->row['store_url'],
				'customer_id'	 		  => $order_query->row['customer_id'],
				'email'	 		  		  => $order_query->row['email'],
				'telephone'	 		  	  => $order_query->row['telephone']
			);
		} else {
			return false;
		}
	}
	
	public function headerShippingData($status_id, $sort = array()){
		$query = $this->db->query("SELECT o.order_id, od.order_detail_id, c.firstname, c.lastname, od.invoice_prefix, od.invoice_no, od.date_added, od.shipping_receipt_number, od.date_modified, od.sla_response, od.sla_shipping, pb.company, pb.address, o.total, o.language_id FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb on od.pp_branch_id=pb.pp_branch_id WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = '".(int)$status_id."' AND od.sla_response >= 0 ORDER BY od.date_added DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		
		return $query->rows;
	}
	
	public function searchAccountShippingStatus($data = array(), $sort = array()){
		$query = $this->db->query("SELECT o.order_id, od.order_detail_id, c.firstname, c.lastname, od.invoice_prefix, od.invoice_no, od.date_added, od.shipping_receipt_number, od.date_modified, o.total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 19 AND (c.firstname LIKE '%".$data['search_all']."%' OR c.lastname LIKE '%".$data['search_all']."%' OR CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$data['search_all']."%' OR od.shipping_receipt_number LIKE '%".$data['search_all']."%') ORDER BY od.date_added DESC".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		
		return $query->rows;
	}
	
	public function countSearchAccountShippingStatus($data = array()){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 19 AND (c.firstname LIKE '%".$data['search_all']."%' OR c.lastname LIKE '%".$data['search_all']."%' OR CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$data['search_all']."%' OR od.shipping_receipt_number LIKE '%".$data['search_all']."%')");
		
		return $query->row['total'];
	}
	
	public function headerShippingDataById($order_detail_id){
		$query = $this->db->query("SELECT o.order_id, od.order_detail_id, c.firstname, c.lastname, od.invoice_prefix, od.invoice_no, od.date_added, od.sla_response, od.sla_shipping, o.total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 18 AND od.sla_shipping >= 0 AND od.order_detail_id='".(int)$order_detail_id."' AND order_detail_status_id = 1 ORDER BY od.date_added DESC");
		
		return $query->rows;
	}

	public function loadProductData($order_id, $order_detail_id){
		$query = $this->db->query("SELECT op.order_id, op.order_detail_id, op.product_id, op.name, op.quantity, op.price, op.total, od.comment, op.model FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "ms_product mp ON op.product_id =  mp.product_id LEFT JOIN " . DB_PREFIX . "order_detail od ON op.order_detail_id =  od.order_detail_id WHERE mp.seller_id='" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND op.order_id ='".(int)$order_id."' AND op.order_detail_id='".(int)$order_detail_id."'");
		
		return $query->rows;
	}
	
	public function getProductOrder($order_id, $order_detail_id){
		$query = $this->db->query("SELECT op.order_product_id, op.tax, op.order_id, op.order_detail_id, op.product_id, op.name, op.quantity, op.price, op.total, od.comment, op.model, ms.nickname, ms.company, p.sku, op.comment FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "ms_product mp ON op.product_id =  mp.product_id LEFT JOIN " . DB_PREFIX . "order_detail od ON op.order_detail_id =  od.order_detail_id LEFT JOIN " . DB_PREFIX . "ms_seller ms ON mp.seller_id =  ms.seller_id LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = op.product_id WHERE op.order_id ='".(int)$order_id."' AND op.order_detail_id='".(int)$order_detail_id."'");
		
		return $query->rows;
	}
	
	public function totalPriceProductByOrderId($order_id){
		$query = $this->db->query("SELECT sum(total) as total FROM " . DB_PREFIX . "order_product WHERE order_id='".(int)$order_id."'");
		
		return $query->row['total'];
	}
	
	public function totalPriceProductByInvoice($order_detail_id){
		$query = $this->db->query("SELECT sum(total) as total FROM " . DB_PREFIX . "order_product WHERE order_detail_id='".(int)$order_detail_id."'");
		
		$query2 = $this->db->query("SELECT shipping_price as shipping_price FROM " . DB_PREFIX . "order_detail WHERE order_detail_id='".(int)$order_detail_id."'");
		
		return $query->row['total']+$query2->row['shipping_price'];
	}
	
	public function totalQuantityProductByInvoice($order_detail_id){
		$query = $this->db->query("SELECT sum(quantity) as qty FROM " . DB_PREFIX . "order_product WHERE order_detail_id='".(int)$order_detail_id."'");
		
		return $query->row['qty'];
	}
	
	public function insertResponseOrder($data){
		$this->event->trigger('pre.response.add', $data);
		$order_detail_id = (int)$this->db->escape($data['input_order_detail_id']);
		$order_id = (int)$this->db->escape($data['input_order_id']);
		$is_accepted = (int)$this->db->escape($data['rbt_response']);
		
		if($is_accepted == 1){
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 18, order_detail_status_id = '".(int)$this->db->escape($data['rbt_response'])."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id'])."'");
		
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id'])."', order_status_id = 18, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
			
			$this->sendNotification($order_detail_id, 18);
		}else if($is_accepted == 0){
			$this->language->load('multiseller/multiseller');
			// Update Order Detail
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 23, order_detail_status_id = '".(int)$this->db->escape($data['rbt_response'])."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id'])."'");
			
			// Add Fault
			$this->db->query("UPDATE " . DB_PREFIX . "ms_seller_fault SET fault = fault-1 WHERE seller_id = '". (int)$this->MsLoader->MsSeller->getSellerId() ."'");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id'])."', order_status_id = 23, notify=0, comment='".$this->db->escape($data['rbt_reason'])."', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
			// RESTOCK
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {
					$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

					$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
					}
				}
				
				/*
				// Remove coupon, vouchers and reward points history
				$this->load->model('account/order');

				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'unconfirm')) {
						$this->{'model_total_' . $order_total['code']}->unconfirm($order_id);
					}
				}
				*/

			// REFUND
					
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_detail WHERE order_status_id = 23 AND delivery_type <> 3 AND order_detail_id='".(int)$order_detail_id."'");
					$order_detail=array();
					if($query->num_rows){
						$order_detail=$query->row;
					}
					
					$batch_id='';
					//Create Batch
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction_batch WHERE is_closed='1'");
					if($query->num_rows>0){
						$batch_id=$query->row['batch_id'];
					}else{
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction_batch WHERE batch_id like '%".date('-mY')."' ORDER BY batch_id DESC");
						/*if($query->num_rows>0){
							$batch=explode('-',$query->row['batch_id']);
							$batch_id=((int)$batch[0]+1)."-".date('mY');
						}else{
							$batch_id = 1 ."-".date('mY');
						}*/
				if($query->num_rows>0){
					$batch=explode('-',$query->row['batch_id']);
					if((int)$batch[0]>=2){
						$batch_id = 1 ."-".date('mY', strtotime('+1 month'));
					}else{
						$batch_id=((int)$batch[0]+1)."-".date('mY');
					}
				}else{
					$batch_id = 1 ."-".date('mY');
				}
						
						$this->db->query("INSERT INTO " . DB_PREFIX . "transaction_batch SET batch_id='".$batch_id."', date_open = NOW()");
					}
					
					// Add Loan
					$this->MsLoader->MsLoan->addLoan(
						array(
							'seller_id' => (int)$this->MsLoader->MsSeller->getSellerId(),
							'order_id' => (int)$this->db->escape($data['input_order_id']),
							'order_detail_id' => (int)$this->db->escape($data['input_order_detail_id']),
							'batch_id' => $batch_id,
							'amount' => 1 * $this->config->get('msconf_seller_fault_amount'),
							'description' => sprintf($this->language->get('ms_reject_order_description'), $order_detail['invoice_prefix'].$order_detail['invoice_no'])
						)
					);
					
					$ms_order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");
					
					$sendmail = false;
					$customer_id=0;
					$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
					if($customer_query->num_rows){
						$customer_id=$customer_query->row['customer_id'];
					}
					
					if($ms_order_product_query->num_rows){
					$payout_amount=0;
					foreach ($ms_order_product_query->rows as $order_product) {
						// increment sold counter
						// $this->db->query("UPDATE " . DB_PREFIX . "ms_product SET number_sold  = (number_sold - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");
						
						if (!$customer_id) continue;
						
						
						$balance_entry = $this->balance->getBalanceEntry(
							array(
								'customer_id' => $customer_id,
								'product_id' => $order_product['product_id'],
								'order_id' => $order_id,
								'order_detail_id' => $order_detail_id,
								'balance_type' => Balance::BALANCE_TYPE_REFUND_PRODUCT
							)
						);
						
						if (!$balance_entry) {
                            $order_data = $this->MsLoader->MsOrderData->getOrderData(
                                array(
                                    'product_id' => $order_product['product_id'],
                                    'order_id' => $order_product['order_id'],
                                    'single' => 1
                                )
                            );

							$this->balance->addBalanceEntry(
								$customer_id,
								array(
									'order_id' => $order_product['order_id'],
									'order_detail_id' => $order_product['order_detail_id'],
									'product_id' => $order_product['product_id'],
									'balance_type' => Balance::BALANCE_TYPE_REFUND_PRODUCT,
									'amount' => $order_product['total'],
									'description' => sprintf($this->language->get('ms_transaction_refund'),  ($order_product['quantity'] > 1 ? $order_product['quantity'] . ' x ' : '')  . $order_product['name'])
								)
							);
							$payout_amount+=$order_product['total'];
							$sendmail = true;
						} else {
							// send order status change mails
						}
					}
						$this->balance->addBalanceEntry($customer_id,
							array(
								'order_id' => $order_id,
								'order_detail_id' => $order_detail_id,
								'balance_type' => Balance::BALANCE_TYPE_REFUND_SHIPPING,
								'amount' => $order_detail['shipping_price'],
								'description' => $this->language->get('ms_transaction_refund_shipping')
							)
						);
						$payout_amount+=$order_detail['shipping_price'];
					
					$customer_query = $this->db->query("SELECT c.*,a.name as account FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "account a ON a.account_id=c.account_id WHERE customer_id = '" . (int)$customer_id . "'");
					if($customer_query->num_rows){
						$customer_email=$customer_query->row['email'];
						$firstname=$customer_query->row['firstname'];
						$lastname=$customer_query->row['lastname'];
						$account=$customer_query->row['account'];
						$account_number=$customer_query->row['account_number'];
					}else{
						$customer_email="noname@ofiskita.com";
						$firstname="";
						$lastname="";
						$account="";
						$account_number="";
					}
					//Auto Payout Customer
					$this->payment->createPayment(array(
							'user_id' => $customer_id,
							'customer_id' => $customer_id,
							'payment_type' => Payment::TYPE_PAYOUT_REQUEST,
							'payment_status' => Payment::STATUS_UNPAID,
							'payment_method' => Payment::METHOD_TRANSFER,
							'payment_data' => $firstname." ".$lastname."(".$account."-".$account_number.")",
							'amount' => $payout_amount,
							'currency_id' => $this->currency->getId($this->config->get('config_currency')),
							'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
							'description' => sprintf($this->language->get('text_description_withdrawal'), $this->language->get('text_method_transfer'), $firstname." ".$lastname."(".$account."-".$account_number.")", date("d/m/Y")),
						)
					);
					
					//Email Notification
					$description=sprintf($this->language->get('text_description_withdrawal'), $this->language->get('text_method_transfer'), $firstname." ".$lastname."(".$account."-".$account_number.")", date("d/m/Y"));
					// Send Email to Customer
					$this->load->language('mail/customer_withdrawal');
					$email=array();
					$email['text_customer'] = $this->language->get('text_customer');
					$email['text_withdrawal'] = $this->language->get('text_withdrawal');
					$email['text_amount'] = $this->language->get('text_amount');
					$email['text_date_added'] = $this->language->get('text_date_added');
					$email['text_description'] = $this->language->get('text_description');
					$email['text_withdrawal_link'] = $this->language->get('text_withdrawal_link');
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status');
					$email['text_withdrawal_request'] = $this->language->get('text_withdrawal_request');
					$email['text_footer'] = $this->language->get('text_footer');
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

					$email['store_url']=HTTP_SERVER;
					$email['title']=$subject;
					$email['store_name']=$this->config->get('config_name');
					$email['logo']=HTTP_SERVER . 'image/' . $this->config->get('config_logo');
					$email['withdrawal_link']=HTTP_SERVER.'index.php?route=account/transaction';
					$email['customer']=$firstname." ".$lastname;
					$email['withdrawal']=$this->language->get('text_withdrawal_request');
					$email['date_added']=date('d/m/Y');
					$email['amount']=$this->currency->format($payout_amount);
					$email['description']=$description;
					
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/customer_withdrawal.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/customer_withdrawal.tpl', $email);
					} else {
						$html = $this->load->view('default/template/mail/customer_withdrawal.tpl', $email);
					}
	
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($customer_email);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
					// Send Email to Admin
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status_admin');
					
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/customer_withdrawal_admin.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/customer_withdrawal_admin.tpl', $email);
					} else {
						$html = $this->load->view('default/template/mail/customer_withdrawal_admin.tpl', $email);
					}
					
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($customer_email);
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
					
					}
			
			$this->sendNotification($order_detail_id, 23);
		}
		
		$this->event->trigger('post.response.add', $order_detail_id);

		return $order_detail_id;
	}
	
	public function insertResponseOrderMany($order_detail_id, $order_id){
		
		$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 18, order_detail_status_id = '1', date_modified=NOW() WHERE order_detail_id = '".(int)$order_detail_id."'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$order_id."', order_detail_id = '".(int)$order_detail_id."', order_status_id = 18, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
		
		$this->sendNotification($order_detail_id, 18);
	}
	
	public function loadListOrderRelease($sort = array()) {
		$query = $this->db->query("SELECT od.order_id, o.language_id, o.currency_id, o.payment_method, c.firstname, c.lastname, od.order_detail_id, od. seller_id, od.invoice_prefix, od.invoice_no, od.date_added,  od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_district, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, od.currency_code, od.currency_value, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.sla_response, od.sla_shipping, od.pp_name, od.pp_email, od.pp_telephone FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 18 AND od.sla_shipping >= 0 AND od.order_detail_status_id = 1 ORDER BY od.date_added DESC".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");

		return $query->rows;
	}
	
	public function countLoadListOrderRelease($status_id) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = '".(int)$status_id."' AND od.order_detail_status_id = 1 ");

		return $query->row['total'];
	}
	
	public function countPendingOrders() {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 3");

		return $query->row['total'];
	}
	
	public function searchListOrderRelease($data = array(), $sort = array()) {
		$query = $this->db->query("SELECT od.order_id, o.language_id, o.currency_id, o.payment_method, c.firstname, c.lastname, od.order_detail_id, od. seller_id, od.invoice_prefix, od.invoice_no, od.date_added,  od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_district, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, od.currency_code, od.currency_value, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.sla_response, od.sla_shipping, od.pp_name, od.pp_email, od.pp_telephone FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 18 AND od.sla_shipping >= 0 AND od.order_detail_status_id = 1 AND (CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$data['invoice']."%' OR od.shipping_firstname LIKE '%".$data['invoice']."%' OR od.shipping_lastname LIKE '%".$data['invoice']."%') AND od.shipping_id LIKE '%".$data['delivery_agent']."%' AND od.sla_shipping LIKE '%".$data['deadline']."%' ORDER BY od.date_added DESC".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");

		return $query->rows;
	}
	
	public function countSearchListOrderRelease($data = array()) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (o.customer_id = c.customer_id) WHERE od.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND od.order_status_id = 18 AND od.sla_shipping >= 0 AND od.order_detail_status_id = 1 AND (CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$data['invoice']."%' OR od.shipping_firstname LIKE '%".$data['invoice']."%' OR od.shipping_lastname LIKE '%".$data['invoice']."%') AND od.shipping_id LIKE'%".$data['delivery_agent']."%'");

		return $query->row['total'];
	}
	
	/*public function insertReceiptNumber($data){
		if(isset($data['rbt_change_delivery'])){
			$change_agent = $this->db->escape($data['rbt_change_delivery']);
		}else{
			$change_agent=0;
		}
		
		if ($change_agent == 1){
			$shipping_service = $this->db->escape($data['input_shipping_service']);
			
			$shipping_sql="
				SELECT 
				os.shipping_id,
				os.name as shipping,
				oss.shipping_service_id,
				oss.name as shipping_service
				FROM " . DB_PREFIX . "shipping os
				LEFT JOIN " . DB_PREFIX . "shipping_service oss ON os.shipping_id=oss.shipping_id
				LEFT JOIN " . DB_PREFIX . "shipping_service_price ossp ON oss.shipping_id=ossp.shipping_id AND oss.shipping_service_id=ossp.shipping_service_id 
				WHERE oss.shipping_service_id='". (int)$shipping_service ."'";
			$shipping_query = $this->db->query($shipping_sql);
			if($shipping_query->num_rows){
				$shipping=$shipping_query->row;
				$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 19, shipping_receipt_number = '".$this->db->escape($data['input_receipt_number'])."', shipping_id='".(int)$shipping['shipping_id']."', shipping_name='".$shipping['shipping']."', shipping_service_id='".(int)$shipping['shipping_service_id']."', shipping_service_name='".$shipping['shipping_service']."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
			}
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 19, shipping_receipt_number = '".$this->db->escape($data['input_receipt_number'])."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
		}
	}*/
	
	public function insertReceiptNumber($data){
		if(isset($data['rbt_change_delivery'])){
			$change_agent = $this->db->escape($data['rbt_change_delivery']);
		}else{
			$change_agent=0;
		}
		
		if ($change_agent == 1){
			$shipping_service = $this->db->escape($data['input_shipping_service']);
			
			$shipping_sql="
				SELECT 
				os.shipping_id,
				os.name as shipping,
				oss.shipping_service_id,
				oss.name as shipping_service
				FROM " . DB_PREFIX . "shipping os
				LEFT JOIN " . DB_PREFIX . "shipping_service oss ON os.shipping_id=oss.shipping_id
				LEFT JOIN " . DB_PREFIX . "shipping_service_price ossp ON oss.shipping_id=ossp.shipping_id AND oss.shipping_service_id=ossp.shipping_service_id 
				WHERE oss.shipping_service_id='". (int)$shipping_service ."'";
			$shipping_query = $this->db->query($shipping_sql);
			if($shipping_query->num_rows){
				$shipping=$shipping_query->row;
				$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 19, shipping_receipt_number = '".$this->db->escape($data['input_receipt_number'])."', shipping_id='".(int)$shipping['shipping_id']."', shipping_name='".$shipping['shipping']."', shipping_service_id='".(int)$shipping['shipping_service_id']."', shipping_service_name='".$shipping['shipping_service']."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
				
				$this->sendNotification((int)$data['input_order_detail_id_release'], 19);
			}
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 19, shipping_receipt_number = '".$this->db->escape($data['input_receipt_number'])."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
			
			$this->sendNotification((int)$data['input_order_detail_id_release'], 19);
		}
	}
	
	public function editReceiptNumber($data){
		$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET shipping_receipt_number = '".$this->db->escape($data['input_receipt_number'])."', date_modified=NOW() WHERE order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."' AND order_status_id = 19");
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
	}

	public function acceptPickup($data){
		$this->event->trigger('pre.receipt.add', $data);
		$order_detail_id = $this->db->escape($data['input_order_detail_id_release']);
		$order_id = $this->db->escape($data['input_order_id_release']);
		
		$this->db->query("UPDATE " . DB_PREFIX . "order_detail SET order_status_id = 19, date_modified=NOW() WHERE delivery_type_id='2' AND order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id='".(int)$this->db->escape($data['input_order_id_release'])."', order_detail_id = '".(int)$this->db->escape($data['input_order_detail_id_release'])."', order_status_id = 19, notify=0, comment='', user_id='".(int)$this->customer->getId()."', identity='Merchant', date_added=NOW()");
		
		$this->sendNotification($order_detail_id, 19);
		
		$this->event->trigger('post.receipt.add', $order_detail_id);

		return $order_detail_id;
	}
	
	public function getTotalWeight($order_detail_id){
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$products = $this->model_account_order->getOrderProductsByOrderDetail($order_detail_id);
		$total_weight = 0;
		foreach($products as $product){
			$product_info = $this->model_catalog_product->getProduct($product['product_id']);
			$total_weight += ($product_info['weight']*$product['quantity']);
		}
		
		return $total_weight;
	}
	
	public function getWeightClass($order_detail_id){
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$products = $this->model_account_order->getOrderProductsByOrderDetail($order_detail_id);
		$total_weight = 0;
		foreach($products as $product){
			$product_info = $this->model_catalog_product->getProduct($product['product_id']);
			$weight_class = $product_info['weight_class_id'];
		}
		
		$query = $this->db->query("SELECT * from " . DB_PREFIX . "weight_class_description WHERE weight_class_id='".(int)$weight_class."'");
		
		// $weight = $query->row['unit'];		
		if((int)$weight_class == 0){			
			$weight = "";
		}else{			
			$weight = $query->row['unit'];					
		}
		
		return $weight;
	}
	
	public function sendNotification($order_detail_id, $status_id){
		$order_info = $this->getOrderNotification($order_detail_id);
		
		if ($order_info) {
			// Fraud Detection
			$this->load->model('account/customer');
			$this->load->model('catalog/product');

			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');

				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

				if (!$safe && $risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Ban IP
			if (!$safe) {
				$status = false;

				if ($order_info['customer_id']) {
					$results = $this->model_account_customer->getIps($order_info['customer_id']);

					foreach ($results as $result) {
						if ($this->model_account_customer->isBanIp($result['ip'])) {
							$status = true;

							break;
						}
					}
				} else {
					$status = $this->model_account_customer->isBanIp($order_info['ip']);
				}

				if ($status) {
					$order_status_id = $this->config->get('config_order_status_id');
				}
			}
			
			$order_id = $order_info['order_id'];
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_detail_id = '" . (int)$order_detail_id . "'");
			
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
			if ($order_status_query->num_rows) {
				$order_status = $order_status_query->row['name'];
			} else {
				$order_status = '';
			}
			
			$decline_reason = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_history_detail WHERE order_status_id = 23 AND order_detail_id='".(int)$order_detail_id."'");
			
			if ($decline_reason->num_rows) {
				$reason = $decline_reason->row['comment'];
			} else {
				$reason = '';
			}
			
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($order_info['language_id']);

			if ($language_info) {
				$language_directory = $language_info['directory'];
			} else {
				$language_directory = '';
			}
			
			$language = new Language($language_directory);
			$language->load('mail/order');
			$language->load('default');
			
			$subject = sprintf($language->get('text_subject'), $order_info['store_name'], $order_id, $order_detail_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($language->get('text_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id, $order_detail_id);

			$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$data['text_link'] = $language->get('text_new_link');
			$data['text_download'] = $language->get('text_new_download');
			$data['text_order_detail'] = $language->get('text_new_order_detail');
			$data['text_instruction'] = $language->get('text_new_instruction');
			$data['text_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_new_date_added');
			$data['text_payment_method'] = $language->get('text_new_payment_method');
			$data['text_shipping_method'] = $language->get('text_new_shipping_method');
			$data['text_email'] = $language->get('text_new_email');
			$data['text_telephone'] = $language->get('text_new_telephone');
			$data['text_ip'] = $language->get('text_new_ip');
			$data['text_order_status'] = $language->get('text_new_order_status');
			$data['text_payment_address'] = $language->get('text_new_payment_address');
			$data['text_shipping_address'] = $language->get('text_new_shipping_address');
			$data['text_product'] = $language->get('text_new_product');
			$data['text_model'] = $language->get('text_new_model');
			$data['text_quantity'] = $language->get('text_new_quantity');
			$data['text_price'] = $language->get('text_new_price');
			$data['text_total'] = $language->get('text_new_total');
			$data['text_footer'] = $language->get('text_new_footer');
			$data['text_shipping_price'] = $language->get('text_shipping_price');
			$data['text_receipt_number'] = $language->get('text_receipt_number');
			$data['text_merchant_complete'] = $language->get('text_merchant_complete');
			$data['text_merchant_link'] = $language->get('text_merchant_link');
			$data['text_decline_reason'] = $language->get('text_decline_reason');
			$data['text_update_order_status'] = $language->get('text_update_order_status');

			$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			$data['store_name'] = $order_info['store_name'];
			$data['store_url'] = $order_info['store_url'];
			$data['customer_id'] = $order_info['customer_id'];
			$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . '&order_detail_id=' . $order_detail_id;
			
			$data['order_id'] = $order_id;
			$data['order_detail_id'] = $order_detail_id;
			$data['payment_method'] = $order_info['payment_method'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];
			$data['order_status'] = $order_status;
			$data['reason'] = $reason;
			
			$data['delivery_type_id'] = $order_info['delivery_type_id'];
			
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);
		
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}';
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}'
			);

			$replace_pp = array(
				'pp_name'		=> $order_info['pp_name'],
				'company' 		=> $order_info['company'],
				'pp_branch_name'=> $order_info['pp_branch_name'],
				'address'      	=> $order_info['address'],
				'subdistrict_pp'=> $order_info['subdistrict_pp'],
				'district_pp'   => $order_info['district_pp'],
				'city_pp' 		=> $order_info['city_pp'],
				'zone_pp'      	=> $order_info['zone_pp'],
				'postcode' 		=> $order_info['postcode'],
				'country_pp'  	=> $order_info['country_pp']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->totalPriceProductByInvoice($order_detail_id);
			
			$data['shipping_address'] = $shipping_address;
			$data['address_pp'] = $address_pp;
			$data['total_price_invoice'] = $this->currency->format($total_price_invoice, $order_info['currency_code'], $order_info['currency_value']);
			$data['shipping_price'] = $this->currency->format($order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			$data['shipping_name'] = $order_info['shipping_name'];
			$data['shipping_service_name'] = $order_info['shipping_service_name'];
			$data['pp_branch_name'] = $order_info['pp_branch_name'];
			$data['delivery_type'] = $order_info['delivery_type'];
			$data['shipping_receipt_number'] = $order_info['shipping_receipt_number'];
			$data['order_status_id'] = $order_info['order_status_id'];
			
			/*data product*/
			$data['products'] = array();
			
			$products = $this->getProductOrder($order_id, $order_detail_id);
			
			foreach ($products as $product) {	
					
				$data['products'][] = array(
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'model' 				=> $product['model'],
					'nickname' 				=> $product['nickname'],
					'company' 				=> $product['company'],
					'total'					=> $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']),
					'price'					=> $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			// Vouchers
			$data['vouchers'] = array();

			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_voucher_query->rows as $voucher) {
				$data['vouchers'][] = array(
					'description' 	=> $voucher['description'],
					'code' 			=> $voucher['code'],
					'to_name' 		=> $voucher['to_name'],
					'to_email' 		=> $voucher['to_email'],
					'amount'      	=> $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
				);
				$data['to_name']  = $voucher['to_name'];
				$data['to_email'] = $voucher['to_email'];
			}
			
			$data['text_order_id']  = $language->get('text_update_order');
			$data['text_order_detail_id']  = $language->get('text_update_order_detail');
			$data['text_date_added'] = $language->get('text_update_date_added');

			if ($order_status) {
				$data['text_update_order_status'] = $language->get('text_update_order_status');
			}

			if ($order_info['customer_id']) {
				$data['text_update_link'] = $language->get('text_update_link');
				$data['text_link_direct'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . '&order_detail_id=' . $order_detail_id;
			}

			$data['text_footer'] = $language->get('text_update_footer');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/pending_order.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/pending_order.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/pending_order.tpl', $data);
			}
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
		}
	}
	
	public function getOrderDetail($order_detail_id) {
		$order_query = $this->db->query("SELECT od.order_id, od.invoice_prefix, od.invoice_no, o.store_id, o.store_name, o.store_url, o.customer_id, o.firstname, o.lastname, o.telephone, o.fax, o.email, o.payment_firstname, o.payment_lastname, o.payment_company, o.payment_address_1, o.payment_address_2, o.payment_postcode, o.payment_city, o.payment_zone_id, o.payment_zone, o.payment_country_id, o.payment_country, o.payment_address_format, o.payment_method, od.shipping_firstname, od.shipping_lastname, od.shipping_company, od.shipping_address_1, od.shipping_address_2, od.shipping_postcode, od.shipping_city, od.shipping_zone_id, od.shipping_zone, od.shipping_country_id, od.shipping_country, od.shipping_address_format, od.shipping_method, od.comment, od.total, od.order_status_id, o.language_id, o.currency_id, od.currency_code, od.currency_value, od.date_modified, od.date_added, o.ip, od.shipping_id, od.shipping_name, od.shipping_service_name, od.shipping_insurance, od.shipping_price, od.pp_branch_id, od.pp_branch_name, od.delivery_type, od.delivery_type_id, od.shipping_receipt_number, od.pp_name, od.pp_email, od.pp_telephone, pb.company, pb.country_id, pb.zone_id, pb.city_id, pb.district_id, pb.subdistrict_id, pb.postcode, pb.address, ov.to_name, ov.to_email, ov.code, o.total as order_total, ov.description, ov.message, od.seller_id FROM " . DB_PREFIX . "order_detail od LEFT JOIN " . DB_PREFIX . "order o ON (od.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "pp_branch pb ON (od.pp_branch_id = pb.pp_branch_id) LEFT JOIN " . DB_PREFIX . "order_voucher ov ON (od.order_detail_id = ov.order_detail_id) WHERE od.order_detail_id = '" . (int)$order_detail_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			//address pickup point
			
			$country_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['country_id'] . "'");

			if ($country_query_pp->num_rows) {
				$country_pp = $country_query_pp->row['name'];
			} else {
				$country_pp = '';
			}
			
			$zone_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['zone_id'] . "'");

			if ($zone_query_pp->num_rows) {
				$zone_pp = $zone_query_pp->row['name'];
			} else {
				$zone_pp = '';
			}
			
			$city_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$order_query->row['city_id'] . "'");

			if ($city_query_pp->num_rows) {
				$city_pp = $city_query_pp->row['name'];
			} else {
				$city_pp = '';
			}
			
			$district_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "district` WHERE district_id = '" . (int)$order_query->row['district_id'] . "'");

			if ($district_query_pp->num_rows) {
				$district_pp = $district_query_pp->row['name'];
			} else {
				$district_pp = '';
			}
			
			$subdistrict_query_pp = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subdistrict` WHERE subdistrict_id = '" . (int)$order_query->row['subdistrict_id'] . "'");

			if ($subdistrict_query_pp->num_rows) {
				$subdistrict_pp = $subdistrict_query_pp->row['name'];
			} else {
				$subdistrict_pp = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
				'ip'                      => $order_query->row['ip'],
				'delivery_type'           => $order_query->row['delivery_type'],
				'shipping_id'             => $order_query->row['shipping_id'],
				'shipping_name'           => $order_query->row['shipping_name'],
				'shipping_service_name'   => $order_query->row['shipping_service_name'],
				'shipping_insurance'      => $order_query->row['shipping_insurance'],
				'shipping_price'          => $order_query->row['shipping_price'],
				'pp_branch_id'            => $order_query->row['pp_branch_id'],
				'pp_branch_name'          => $order_query->row['pp_branch_name'],
				'delivery_type_id'        => $order_query->row['delivery_type_id'],
				'shipping_receipt_number' => $order_query->row['shipping_receipt_number'],
				'pp_name' 			  	  => $order_query->row['pp_name'],
				'pp_email' 			  	  => $order_query->row['pp_email'],
				'pp_telephone' 			  => $order_query->row['pp_telephone'],
				'company' 			  	  => $order_query->row['company'],
				'postcode'		  	  	  => $order_query->row['postcode'],
				'address' 			  	  => $order_query->row['address'],
				'country_pp' 			  => $country_pp,
				'zone_pp' 			  	  => $zone_pp,
				'city_pp' 			  	  => $city_pp,
				'district_pp' 			  => $district_pp,
				'subdistrict_pp' 		  => $subdistrict_pp,
				'to_name' 		  		  => $order_query->row['to_name'],
				'to_email' 		  		  => $order_query->row['to_email'],
				'code'	 		  		  => $order_query->row['code'],
				'order_total'	  		  => $order_query->row['order_total'],
				'description'	  		  => $order_query->row['description'],
				'message'		  		  => $order_query->row['message'],
				'seller_id'		  		  => $order_query->row['seller_id']
			);
		} else {
			return false;
		}
	}
}
