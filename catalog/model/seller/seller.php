<?php
class ModelSellerSeller extends Model {
	public function getCategories($seller_id=0) {
		$query = $this->db->query("SELECT c.category_id, c.parent_id, c.top, c.column, c.sort_order, c.status, c.category_type, c.attribute_group_id, cd.name, mcc.flat, mcc.percent FROM " . DB_PREFIX . "category c LEFT JOIN ". DB_PREFIX."category_description cd ON c.category_id = cd.category_id LEFT JOIN ". DB_PREFIX."ms_commission_category mcc ON c.category_id = mcc.category_id AND mcc.seller_id='".(int)$seller_id."' WHERE c.category_type = 0 AND c. status = 1 AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cd.name");

		return $query->rows;
	}
	
	public function addCategories($data=array()) {
		$query_add = $this->db->query("INSERT INTO " . DB_PREFIX . "ms_commission_category SET seller_id='".(int)$data['seller_id']."', category_id='".(int)$data['category_id']."', flat='".(float)$data['flat']."', percent='".(float)$data['percent']."', date_added = NOW()");

	}
	
	public function deleteCategories($seller_id){
		$query_delete = $this->db->query("DELETE FROM " . DB_PREFIX . "ms_commission_category WHERE seller_id='".(int)$seller_id."'");
	}
}
?>