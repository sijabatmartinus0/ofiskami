<?php
class ModelSellerAccountTransaction extends Model {
	public function loadPaymentTransactions($start = 0, $limit = 20){
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY date_created DESC LIMIT " . (int)$start . "," . (int)$limit);
		
		return $query->rows;
	}
	
	public function searchPaymentTransactions($data=array(), $sort=array()){
		if($data['date_start_payment']==null && $data['date_end_payment']==null){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY date_created DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_paid) BETWEEN '".$data['date_start_payment']."' AND '".$data['date_end_payment']."' ORDER BY date_created DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		}
		
		return $query->rows;
	}
	
	public function getReportPaymentTransactions($data=array()){
		if($data['date_start_payment']==null && $data['date_end_payment']==null){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY date_created DESC");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_paid) BETWEEN '".$data['date_start_payment']."' AND '".$data['date_end_payment']."' ORDER BY date_created DESC ");
		}
		
		return $query->rows;
	}
	
	public function countPaymentTransaction(){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."'");
		
		return $query->row['total'];
	}
	
	public function countSearchPaymentTransaction($data=array()){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "view_ms_payment WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_paid) BETWEEN '".$this->db->escape($data['date_start_payment'])."' AND '".$this->db->escape($data['date_end_payment'])."'");
		
		return $query->row['total'];
	}
	
	public function loadReportTransactions($start = 0, $limit = 20){
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "summary_report  WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY transaction_date DESC LIMIT " . (int)$start . "," . (int)$limit);
		
		return $query->rows;
	}
	
	public function searchReportTransactions($data=array(), $sort=array()){
if($data['date_start_report']==null && $data['date_end_report']==null && ($data['batch']==null || $data['batch']=='')){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY transaction_date DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId()."' " .( !empty($data['date_start_report']) && !empty($data['date_end_report']) ? " AND (date(transaction_date) BETWEEN '".$data['date_start_report']."' AND '".$data['date_end_report']."') ":""). ($data['batch']!='' || $data['batch']!=null ? " AND batch_id='".$this->db->escape($data['batch'])."' " : '')." ORDER BY date_added DESC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");		
		}
		
		return $query->rows;
	}
	
	public function getReportTransactions($data=array()){
		if($data['date_start_report']==null && $data['date_end_report']==null && ($data['batch']==null || $data['batch']=='')){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY transaction_date DESC ");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId()."' " .( !empty($data['date_start_report']) && !empty($data['date_end_report']) ? " AND (date(transaction_date) BETWEEN '".$data['date_start_report']."' AND '".$data['date_end_report']."') ":""). ($data['batch']!='' || $data['batch']!=null ? " AND batch_id='".$this->db->escape($data['batch'])."' " : '')." ORDER BY date_added DESC ");
		}
		
		return $query->rows;
	}
	
	public function countReportTransaction(){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."'");
		
		return $query->row['total'];
	}
	
	public function countSearchReportTransaction($data=array()){
		//$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND ((date(transaction_date) BETWEEN '".$this->db->escape($data['date_start_report'])."' AND '".$this->db->escape($data['date_end_report'])."')". ($data['batch']!='' || $data['batch']!=null ? " OR batch_id='".$this->db->escape($data['batch'])."' " : '').")");
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "summary_report WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId()."' ".( !empty($data['date_start_report']) && !empty($data['date_end_report']) ? " AND (date(transaction_date) BETWEEN '".$data['date_start_report']."' AND '".$data['date_end_report']."') ":""). ($data['batch']!='' || $data['batch']!=null ? " AND batch_id='".$this->db->escape($data['batch'])."' " : '')." ORDER BY date_added DESC ");
		//echo $query->row['total'];
		return $query->row['total'];
	}
	
	public function loadBalanceTransactions($start = 0, $limit = 20){
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY date_created ASC LIMIT " . (int)$start . "," . (int)$limit);
		return $query->rows;
	}
	
	public function searchBalanceTransactions($data=array(), $sort=array()){
		if($data['date_start_balance']==null && $data['date_end_balance']==null){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY date_created  ASC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_created) BETWEEN '".$data['date_start_balance']."' AND '".$data['date_end_balance']."' ORDER BY date_created ASC ".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ");
		}
		
		return $query->rows;
	}
	
	public function getReportBalanceTransactions($data=array()){
		if($data['date_start_balance']==null && $data['date_end_balance']==null){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' ORDER BY balance,date_created DESC");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_created) BETWEEN '".$data['date_start_balance']."' AND '".$data['date_end_balance']."' ORDER BY balance,date_created DESC ");
		}
		
		return $query->rows;
	}
	
	public function countBalanceTransaction(){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."'");
		
		return $query->row['total'];
	}
	
	public function countSearchBalanceTransaction($data=array()){
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "view_ms_balance WHERE seller_id='". (int)$this->MsLoader->MsSeller->getSellerId() ."' AND date(date_created) BETWEEN '".$this->db->escape($data['date_start_balance'])."' AND '".$this->db->escape($data['date_end_balance'])."'");
		
		return $query->row['total'];
	}
	
	public function reportPaymentTransactions($data=array()){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		if($data['date_start_payment']==null && $data['date_end_payment'] == null){
			$range_date = 'All';
		}else{
			$date_start = date('d/m/Y', strtotime($data['date_start_payment']));
			$date_end = date('d/m/Y', strtotime($data['date_end_payment']));
			$range_date = $date_start . ' - ' . $date_end;
		}
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			// $language_code = $language_info['code'];
			$language_directory = $language_info['directory'];
		} else {
			// $language_code = '';
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('multiseller/multiseller');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator($this->MsLoader->MsSeller->getNickname())
									 ->setLastModifiedBy($this->MsLoader->MsSeller->getNickname())
									 ->setTitle($language->get('ms_rep_payment'))
									 ->setSubject($language->get('ms_payment_trans'))
									 ->setDescription($language->get('ms_payment_trans'))
									 ->setKeywords("office 2007 openxml php");

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B1:I1')
					->setCellValue('B1', $language->get('ms_rep_payment'))
					->mergeCells('B2:I2')
					->setCellValue('B2', $language->get('text_date_paid').': '.$range_date)
					->mergeCells('B3:I3')
					->setCellValue('B3', $language->get('ms_download_date').': '.date("d/m/Y"))
					->setCellValue('A6', $language->get('ms_number'))
					->setCellValue('B6', $language->get('text_requester'))
					->setCellValue('C6', $language->get('ms_account_sellerinfo_company'))
					->setCellValue('D6', $language->get('text_email2'))
					->setCellValue('E6', $language->get('text_pay_type'))
					->setCellValue('F6', $language->get('text_pay_status'))
					->setCellValue('G6', $language->get('text_date_created'))
					->setCellValue('H6', $language->get('text_date_paid'))
					->setCellValue('I6', $language->get('ms_amount'));
		
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		$sheet->getStyle('A6:I6')->applyFromArray($headerStyleArray);
		$sheet->getStyle('B1')->applyFromArray($titleFont);
		$sheet->getStyle('A1:I3')->applyFromArray($titleStyleArray);
		
		
		//data from db
		$payments = $this->model_seller_account_transaction->getReportPaymentTransactions(
			array(
				'date_start_payment' 	=> $data['date_start_payment'],
				'date_end_payment' 		=> $data['date_end_payment']
			)
		);
		
		$row = 7;
		$no = 1;
		$total= 0;
		$total_amount = '';
		
		foreach($payments as $payment){
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, $no)
						->setCellValue('B'.$row, $payment['firstname'])
						->setCellValue('C'.$row, $payment['company'])
						->setCellValue('D'.$row, $payment['email'])
						->setCellValue('E'.$row, $payment['payment_type'])
						->setCellValue('F'.$row, $payment['payment_status'])
						->setCellValue('G'.$row, date('d/m/Y', strtotime($payment['date_created'])))
						->setCellValue('H'.$row, (isset($payment['date_paid']) ? date('d/m/Y', strtotime($payment['date_paid'])) : '-'))
						->setCellValue('I'.$row, $this->currency->format($payment['amount'], $payment['currency_code'], $payment['currency_value']));
			$row++;
			$no++;
			$total+= $payment['amount'];
			$total_amount = $this->currency->format($total, $payment['currency_code'], $payment['currency_value']);
		}
		$last_row = $row - 1;
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('A'.$row.':H'.$row)
					->setCellValue('A'.$row, $language->get('ms_total_amount'))
					->setCellValue('I'.$row, $total_amount);
		
		$sheet->getStyle('A'.$row)->applyFromArray($boldFont);		
		$sheet->getStyle('A'.$row)->applyFromArray($centerStyleArray);		
		$sheet->getStyle('I'.$row)->applyFromArray($rightStyleArray);		
		
		//set column width
		$sheet->getColumnDimension('A')->setWidth(8);
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->getColumnDimension('C')->setWidth(30);
		$sheet->getColumnDimension('D')->setWidth(30);
		$sheet->getColumnDimension('E')->setWidth(20);
		$sheet->getColumnDimension('F')->setWidth(18);
		$sheet->getColumnDimension('G')->setWidth(19);
		$sheet->getColumnDimension('H')->setWidth(19);
		$sheet->getColumnDimension('I')->setWidth(15);
		
		//body style 
		$sheet->getStyle('B7:D'.$last_row)->getAlignment()->setWrapText(true);
		$sheet->getStyle('A7:A'.$last_row)->applyFromArray($centerStyleArray);	//column no style
		$sheet->getStyle('B7:C'.$last_row)->applyFromArray($leftStyleArray);	//column name & company style
		$sheet->getStyle('D7:H'.$last_row)->applyFromArray($centerStyleArray);	//column email,pay type,pay status,date style
		$sheet->getStyle('I7:I'.$last_row)->applyFromArray($rightStyleArray);	//column amount style
		
		//set column color
		$sheet->getStyle('A7:I'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		$sheet->getStyle('A'.$row.':I'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("A6:I".$row)->applyFromArray($border_style);
		$sheet->getStyle('A3:I3')->applyFromArray($border_thick);
		// Rename worksheet
		$sheet->setTitle('Payment Transactions');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		/*
		// Set logo header
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Ofiskita');
		$objDrawing->setDescription('Ofiskita');
		$objDrawing->setPath(DIR_IMAGE . 'header_report.png');
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWidthAndHeight(500,148);
		$objDrawing->setWorksheet($sheet);
		*/

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$language->get('ms_rep_payment').'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	public function reportTransactions($data=array()){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		if($data['date_start_report']==null && $data['date_end_report'] == null){
			$range_date = 'All';
		}else{
			$date_start = date('d/m/Y', strtotime($data['date_start_report']));
			$date_end = date('d/m/Y', strtotime($data['date_end_report']));
			
			$range_date = $date_start . ' - ' . $date_end;
		}
		
		if($data['batch']=='' || $data['batch']==null){
			$batch = 'All';
		}else{
			$batch = $data['batch'];
		}
		
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			// $language_code = $language_info['code'];
			$language_directory = $language_info['directory'];
		} else {
			// $language_code = '';
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('multiseller/multiseller');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator($this->MsLoader->MsSeller->getNickname())
									 ->setLastModifiedBy($this->MsLoader->MsSeller->getNickname())
									 ->setTitle($language->get('ms_rep_trans'))
									 ->setSubject($language->get('ms_account_transactions'))
									 ->setDescription($language->get('ms_account_transactions'))
									 ->setKeywords("office 2007 openxml php");

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B1:S1')
					->setCellValue('B1', $language->get('ms_rep_trans'))
					->mergeCells('B2:S2')
					->setCellValue('B2', $language->get('ms_download_date') . ': ' .date("d/m/Y"))
					->setCellValue('A6', $language->get('ms_number'))
					->setCellValue('B6', $language->get('column_batch'))
					->setCellValue('C6', $language->get('column_order_no'))
					->setCellValue('D6', $language->get('column_vendor_name'))
					->setCellValue('E6', $language->get('column_vendor_type'))
					->setCellValue('F6', $language->get('column_kiosk_id'))
					->setCellValue('G6', $language->get('column_payment_type'))
					->setCellValue('H6', $language->get('column_bank'))
					->setCellValue('I6', $language->get('column_transaction_date'))
					->setCellValue('J6', $language->get('column_quantity'))
					->setCellValue('K6', $language->get('column_sku'))
					->setCellValue('L6', $language->get('column_price'))
					->setCellValue('M6', $language->get('column_total_report'))
					->setCellValue('N6', $language->get('column_tax'))
					->setCellValue('O6', $language->get('column_commission'))
					->setCellValue('P6', $language->get('column_ofiskita_commission'))
					->setCellValue('Q6', $language->get('column_shipping'))
					->setCellValue('R6', $language->get('column_online_payment'))
					->setCellValue('S6', $language->get('column_vendor_balance'));
					
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		$sheet->getStyle('A6:S6')->applyFromArray($headerStyleArray);
		$sheet->getStyle('B1')->applyFromArray($titleFont);
		$sheet->getStyle('A1:S3')->applyFromArray($titleStyleArray);
		
		//data from db
		$reports = $this->model_seller_account_transaction->getReportTransactions(
			array(
				'date_start_report' 	=> $data['date_start_report'],
				'date_end_report' 		=> $data['date_end_report'],
				'batch' 		=> $data['batch']
			)
		);
		
		$row = 7;
		$no = 1;
		$temp = 0;
		$total_vendor_balance=0;
		foreach($reports as $report){
		$total_vendor_balance += $report['vendor_balance'];
			/*if((int)$report['merge'] == 1){
				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, $no)
						->setCellValue('B'.$row, $report['invoice'])
						->setCellValue('C'.$row, $report['customer_name'])
						->setCellValue('D'.$row, $report['product_name'])
						->setCellValue('E'.$row, $report['product_sku'])
						->setCellValue('F'.$row, $report['product_quantity'])
						->setCellValue('G'.$row, $this->currency->format($report['product_price'], $report['currency_code'], $report['currency_value']))
						->setCellValue('H'.$row, $this->currency->format($report['product_total'], $report['currency_code'], $report['currency_value']))
						->setCellValue('I'.$row, $this->currency->format($report['shipping_price'], $report['currency_code'], $report['currency_value']))
						->setCellValue('J'.$row, $this->currency->format($report['total'], $report['currency_code'], $report['currency_value']))
						->setCellValue('K'.$row, date('d/m/Y', strtotime($report['date_added'])))
						->setCellValue('L'.$row, date('d/m/Y', strtotime($report['date_modified'])));
				
			}else{
				if($row <= $temp){
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$row, $no)
							->setCellValue('C'.$row, $report['customer_name'])
							->setCellValue('D'.$row, $report['product_name'])
							->setCellValue('E'.$row, $report['product_sku'])
							->setCellValue('F'.$row, $report['product_quantity'])
							->setCellValue('G'.$row, $this->currency->format($report['product_price'], $report['currency_code'], $report['currency_value']))
							->setCellValue('H'.$row, $this->currency->format($report['product_total'], $report['currency_code'], $report['currency_value']))
							->setCellValue('K'.$row, date('d/m/Y', strtotime($report['date_added'])))
							->setCellValue('L'.$row, date('d/m/Y', strtotime($report['date_modified'])));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$row, $no)
							->mergeCells('B'.$row.':B'.($row + (int)$report['merge'] - 1))
							->setCellValue('B'.$row, $report['invoice'])
							->setCellValue('C'.$row, $report['customer_name'])
							->setCellValue('D'.$row, $report['product_name'])
							->setCellValue('E'.$row, $report['product_sku'])
							->setCellValue('F'.$row, $report['product_quantity'])
							->setCellValue('G'.$row, $this->currency->format($report['product_price'], $report['currency_code'], $report['currency_value']))
							->setCellValue('H'.$row, $this->currency->format($report['product_total'], $report['currency_code'], $report['currency_value']))
							->mergeCells('I'.$row.':I'.($row + (int)$report['merge'] - 1))
							->setCellValue('I'.$row, $this->currency->format($report['shipping_price'], $report['currency_code'], $report['currency_value']))
							->mergeCells('J'.$row.':J'.($row + (int)$report['merge'] - 1))
							->setCellValue('J'.$row, $this->currency->format($report['total'], $report['currency_code'], $report['currency_value']))
							->setCellValue('K'.$row, date('d/m/Y', strtotime($report['date_added'])))
							->setCellValue('L'.$row, date('d/m/Y', strtotime($report['date_modified'])));
					$temp = $row + (int)$report['merge'] - 1;
				}
				}*/
				if($report['kiosk_name']){ $kiosk= $report['kiosk_name']; }else{ $kiosk= "-";}
				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, $no)
						->setCellValue('B'.$row, MsPayment::BATCH_PREFIX . $report['seller_id'] . "-" .$report['batch_id'])
						->setCellValue('C'.$row, $report['invoice'])
						->setCellValue('D'.$row, $report['nickname'])
						->setCellValue('E'.$row, $report['tax_type'])
						->setCellValue('F'.$row, $kiosk)
						->setCellValue('G'.$row, $report['payment_type'])
						->setCellValue('H'.$row, $report['account_name'])
						->setCellValue('I'.$row, date('d/m/Y', strtotime($report['transaction_date'])))
						->setCellValue('J'.$row, $report['quantity'])
						->setCellValueExplicit('K'.$row, $report['code'], PHPExcel_Cell_DataType::TYPE_STRING)
						->setCellValueExplicit('L'.$row, (int)$report['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('M'.$row, (int)$report['total'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('N'.$row, (int)$report['tax'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('O'.$row, (int)$report['commission_base'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('P'.$row, (int)$report['store_commission'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('Q'.$row, (int)$report['shipping_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('R'.$row, (int)$report['online_payment_fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('S'.$row, (int)$report['vendor_balance'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						
			$row++;
			$no++;
		}
		
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('A'.$row.':R'.$row)
					->setCellValue('A'.$row, strtoupper($language->get('column_total_report')))
					->setCellValueExplicit('S'.$row, $total_vendor_balance, PHPExcel_Cell_DataType::TYPE_NUMERIC);
		
		$row++;		
		$last_row = $row - 1;
		
		//set column width
		$sheet->getColumnDimension('A')->setWidth(8);
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->getColumnDimension('C')->setWidth(30);
		$sheet->getColumnDimension('D')->setWidth(30);
		$sheet->getColumnDimension('E')->setWidth(9);
		$sheet->getColumnDimension('F')->setWidth(24);
		$sheet->getColumnDimension('G')->setWidth(16);
		$sheet->getColumnDimension('H')->setWidth(16);
		$sheet->getColumnDimension('I')->setWidth(16);
		$sheet->getColumnDimension('J')->setWidth(16);
		$sheet->getColumnDimension('K')->setWidth(30);
		$sheet->getColumnDimension('L')->setWidth(16);
		$sheet->getColumnDimension('M')->setWidth(16);
		$sheet->getColumnDimension('N')->setWidth(16);
		$sheet->getColumnDimension('O')->setWidth(16);
		$sheet->getColumnDimension('P')->setWidth(16);
		$sheet->getColumnDimension('Q')->setWidth(16);
		$sheet->getColumnDimension('R')->setWidth(16);
		$sheet->getColumnDimension('S')->setWidth(16);
		
		//body style 
		$sheet->getStyle('C7:D'.$last_row)->getAlignment()->setWrapText(true);
		$sheet->getStyle('A7:B'.$last_row)->applyFromArray($centerStyleArray);	//column no, invoice style
		$sheet->getStyle('C7:D'.$last_row)->applyFromArray($leftStyleArray);	//column name & product style
		$sheet->getStyle('E7:K'.$last_row)->applyFromArray($centerStyleArray);	//column sku, qty style	//column total style
		$sheet->getStyle('L7:S'.$last_row)->applyFromArray($rightStyleArray);	//column sku, qty style
		
		//set column color
		$sheet->getStyle('A7:S'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("A6:S".$last_row)->applyFromArray($border_style);
		$sheet->getStyle('A3:S3')->applyFromArray($border_thick);
		// Rename worksheet
		$sheet->setTitle('Transactions Report');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		/*$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Ofiskita');
		$objDrawing->setDescription('Ofiskita');
		$objDrawing->setPath(DIR_IMAGE . 'header_report.png');
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWidthAndHeight(500,148);
		$objDrawing->setWorksheet($sheet);
		*/
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$language->get('ms_rep_trans').'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
	
	public function reportBalanceTransactions($data=array()){
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		if($data['date_start_balance']==null && $data['date_end_balance'] == null){
			$range_date = 'All';
		}else{
			$date_start = date('d/m/Y', strtotime($data['date_start_balance']));
			$date_end = date('d/m/Y', strtotime($data['date_end_balance']));
			$range_date = $date_start . ' - ' . $date_end;
		}
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			// $language_code = $language_info['code'];
			$language_directory = $language_info['directory'];
		} else {
			// $language_code = '';
			$language_directory = '';
		}
		
		$language = new Language($language_directory);
		$language->load('multiseller/multiseller');
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator($this->MsLoader->MsSeller->getNickname())
									 ->setLastModifiedBy($this->MsLoader->MsSeller->getNickname())
									 ->setTitle($language->get('ms_rep_balance'))
									 ->setSubject($language->get('text_balance'))
									 ->setDescription($language->get('text_balance'))
									 ->setKeywords("office 2007 openxml php");

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B1:G1')
					->setCellValue('B1', $language->get('ms_rep_balance'))
					->mergeCells('B2:G2')
					->setCellValue('B2', $language->get('text_transaction_date') . ': ' .$range_date)
					->mergeCells('B3:G3')
					->setCellValue('B3', $language->get('ms_download_date') . ': ' .date("d/m/Y"))
					->setCellValue('A6', $language->get('ms_number'))
					->setCellValue('B6', $language->get('ms_invoice'))
					->setCellValue('C6', $language->get('text_transaction_date'))
					->setCellValue('D6', $language->get('ms_account_product_description'))
					->setCellValue('E6', $language->get('text_transaction_code'))
					->setCellValue('F6', $language->get('ms_amount'))
					->setCellValue('G6', $language->get('text_balance'));
		
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		$sheet->getStyle('A6:G6')->applyFromArray($headerStyleArray);
		$sheet->getStyle('B1')->applyFromArray($titleFont);
		$sheet->getStyle('A1:G3')->applyFromArray($titleStyleArray);
		
		//data from db
		$balances = $this->model_seller_account_transaction->getReportBalanceTransactions(
			array(
				'date_start_balance' 	=> $data['date_start_balance'],
				'date_end_balance' 		=> $data['date_end_balance']
			)
		);
		
		$row = 7;
		$no = 1;
		
		foreach($balances as $balance){
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, $no)
						->setCellValue('B'.$row, $balance['invoice'])
						->setCellValue('C'.$row, date('d/m/Y', strtotime($balance['date_created'])))
						->setCellValue('D'.$row, $balance['description'])
						->setCellValue('E'.$row, $balance['tran_code'])
						->setCellValueExplicit('F'.$row, (int)$balance['amount'], PHPExcel_Cell_DataType::TYPE_NUMERIC)
						->setCellValueExplicit('G'.$row, (int)$balance['balance'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$row++;
			$no++;
		}
		$last_row = $row - 1;
		
		//filter
		// $objPHPExcel->getActiveSheet()->setAutoFilter('A7:G'.$last_row);
		
		//set column width
		$sheet->getColumnDimension('A')->setWidth(8);
		$sheet->getColumnDimension('B')->setWidth(15);
		$sheet->getColumnDimension('C')->setWidth(18);
		$sheet->getColumnDimension('D')->setWidth(43);
		$sheet->getColumnDimension('E')->setWidth(18);
		$sheet->getColumnDimension('F')->setWidth(16);
		$sheet->getColumnDimension('G')->setWidth(16);
		
		//body style 
		$sheet->getStyle('D7:D'.$last_row)->getAlignment()->setWrapText(true);
		$sheet->getStyle('A7:C'.$last_row)->applyFromArray($centerStyleArray);	//column no, invoice, date style
		$sheet->getStyle('D7:D'.$last_row)->applyFromArray($leftStyleArray);	//column desc style
		$sheet->getStyle('E7:E'.$last_row)->applyFromArray($centerStyleArray);	//column trans_code style
		$sheet->getStyle('F7:G'.$last_row)->applyFromArray($rightStyleArray);	//column amount, balance style
		
		//set column color
		$sheet->getStyle('A7:G'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
						
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("A6:G".$last_row)->applyFromArray($border_style);
		$sheet->getStyle('A3:G3')->applyFromArray($border_thick);
		// Rename worksheet
		$sheet->setTitle('Balance');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		/*
		// Set logo header
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Ofiskita');
		$objDrawing->setDescription('Ofiskita');
		$objDrawing->setPath(DIR_IMAGE . 'header_report.png');
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWidthAndHeight(500,148);
		$objDrawing->setWorksheet($sheet);
		*/

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$language->get('ms_rep_balance').'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
}
