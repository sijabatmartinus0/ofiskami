<?php
class ModelSellerAccountReturnList extends Model {
	public function getReturns($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, rs.name as status, r.date_added, CONCAT(od.invoice_prefix, od.invoice_no) as invoice_no, r.product, r.model FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX . "return_status rs ON (r.return_status_id = rs.return_status_id) LEFT JOIN " . DB_PREFIX . "order_detail od ON r.order_detail_id=od.order_detail_id WHERE r.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY r.return_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function searchReturns($data=array(), $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, rs.name as status, r.date_added, CONCAT(od.invoice_prefix, od.invoice_no) as invoice_no, r.product, r.model FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX . "return_status rs ON (r.return_status_id = rs.return_status_id) LEFT JOIN " . DB_PREFIX . "order_detail od ON r.order_detail_id=od.order_detail_id WHERE r.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' AND CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$data['search_invoice']."%' AND CONCAT(r.firstname, ' ', r.lastname) LIKE '%".$data['search_customer']."%' AND r.product LIKE '%".$data['search_product']."%' AND r.return_status_id LIKE '%".$data['search_status']."%' ORDER BY r.return_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function countSearchReturns($data=array()) {
		
		$query = $this->db->query("SELECT COUNT(*) as total FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX . "return_status rs ON (r.return_status_id = rs.return_status_id) LEFT JOIN " . DB_PREFIX . "order_detail od ON r.order_detail_id=od.order_detail_id WHERE r.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' AND CONCAT(od.invoice_prefix, od.invoice_no) LIKE '%".$this->db->escape($data['search_invoice'])."%' AND CONCAT(r.firstname, ' ', r.lastname) LIKE '%".$this->db->escape($data['search_customer'])."%' AND r.product LIKE '%".$this->db->escape($data['search_product'])."%' AND r.return_status_id LIKE '%".(int)$data['search_status']."%'");

		return $query->row['total'];
	}
	
	public function getTotalReturns() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "return`WHERE seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "'");

		return $query->row['total'];
	}
	
	public function getTotalReturnOrders($seller_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "return`WHERE seller_id = '" . (int)$seller_id . "' AND return_status_id=1");

		return $query->row['total'];
	}
	
	public function getReturn($return_id) {
		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, r.email, r.telephone, r.product, r.model, r.quantity, r.opened, (SELECT rr.name FROM " . DB_PREFIX . "return_reason rr WHERE rr.return_reason_id = r.return_reason_id AND rr.language_id = '" . (int)$this->config->get('config_language_id') . "') AS reason, (SELECT ra.name FROM " . DB_PREFIX . "return_action ra WHERE ra.return_action_id = r.return_action_id AND ra.language_id = '" . (int)$this->config->get('config_language_id') . "') AS action, (SELECT rs.name FROM " . DB_PREFIX . "return_status rs WHERE rs.return_status_id = r.return_status_id AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, r.comment, r.date_ordered, r.date_added, r.date_modified, CONCAT(od.invoice_prefix, od.invoice_no) as invoice_no, r.image, r.product_id, r.return_status_id, r.return_action_id, r.is_action_changed, r.confirm_action_changed, r.sla FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX ."order_detail od ON r.order_detail_id = od.order_detail_id WHERE r.return_id = '" . (int)$return_id . "' AND r.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "'");

		return $query->row;
	}
	
	public function getReturnHistories($return_id) {
		$query = $this->db->query("SELECT rh.date_added, rs.name AS status, rh.comment, rh.notify FROM " . DB_PREFIX . "return_history rh LEFT JOIN " . DB_PREFIX . "return_status rs ON rh.return_status_id = rs.return_status_id WHERE rh.return_id = '" . (int)$return_id . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY rh.date_added ASC");

		return $query->rows;
	}
	
	public function acceptReturn($return_id){
		$this->load->model('account/return');
		
		$this->db->query("UPDATE " . DB_PREFIX . "return SET return_status_id=2, date_modified=NOW() WHERE return_id='".(int)$return_id."'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "return_history SET return_id='".(int)$return_id."', return_status_id=2, notify=0, date_added=NOW()");
		
		$this->model_account_return->sendNotification($return_id, 2);
	}
	
	public function editAction($data = array()){
		$this->db->query("UPDATE " . DB_PREFIX . "return SET return_action_id='".(int)$data['return_action_id']."', is_action_changed = 1, confirm_action_changed=0, date_modified=NOW() WHERE return_id='".(int)$data['input_return_id_action']."'");
		
		$this->sendNotifChangeAction($data['input_return_id_action'], $data['return_action_id']);
		
	}
	
	public function addReturnComment($data=array()){
		$this->load->model('catalog/product');
		$seller_id = $this->model_catalog_product->getMsProductById($data['discuss_product_id']);
		$created_by = $this->customer->getId();
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "return_comments SET return_id='".(int)$data['discuss_return_id']."', product_id='".(int)$data['discuss_product_id']."', customer_id='".(int)$this->customer->getId()."', seller_id='".(int)$seller_id."', comment='".$this->db->escape($data['discuss_comment'])."', created_by='".(int)$this->customer->getId()."', created_date = NOW()");
		
		$return_query = $this->db->query("SELECT * FROM " . DB_PREFIX .  "return WHERE return_id='".(int)$data['discuss_return_id']."'");
		
		$customer_id = $return_query->row['customer_id'];
		
		if($seller_id == $created_by){
			$this->sendNotifDiscussion($data['discuss_return_id'], $customer_id);
		}else{
			$this->sendNotifDiscussion($data['discuss_return_id'], $seller_id);
		}
	}
	
	public function listReturnComments($return_id){
		$query = $this->db->query("SELECT rc.return_id, rc.product_id, rc.comment, rc.created_date, rc.flag, c.firstname, s.nickname, rc.created_by, rc.seller_id FROM " . DB_PREFIX . "return_comments rc LEFT JOIN " . DB_PREFIX . "customer c ON rc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON rc.seller_id = s.seller_id WHERE rc.display=1 AND rc.return_id='".(int)$return_id."' ORDER BY rc.created_date ASC");

		return $query->rows;
	}
	
	public function listUpComments($return_id){
		$query = $this->db->query("(SELECT rc.return_id, rc.product_id, rc.comment, rc.created_date, rc.flag, c.firstname, s.nickname, rc.created_by, rc.seller_id FROM " . DB_PREFIX . "return_comments rc LEFT JOIN " . DB_PREFIX . "customer c ON rc.customer_id = c.customer_id LEFT JOIN " .DB_PREFIX . "ms_seller s ON rc.seller_id = s.seller_id WHERE rc.display=1 AND rc.return_id='".(int)$return_id."' ORDER BY rc.created_date DESC LIMIT 2) ORDER BY created_date ASC");

		return $query->rows;
	}
	
	public function getCustomerEmail($customer_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '".(int)$customer_id."'");
		
		if($query->num_rows){
			return $query->row['email'];
		}
	}
	
	public function sendNotifDiscussion($return_id, $created_by){
		$discussions = $this->listReturnComments($return_id);
		
		$return_query = $this->db->query("SELECT o.language_id, o.store_name, o.store_url FROM " . DB_PREFIX . "return r LEFT JOIN " . DB_PREFIX . "order o ON r.order_id = o.order_id WHERE r.return_id = '" . (int)$return_id . "'");
		if ($return_query->num_rows) {
			$language_id = $return_query->row['language_id'];
			$store_name = $return_query->row['store_name'];
			$store_url = $return_query->row['store_url'];
		} else {
			$language_id = '';
			$store_name = '';
			$store_url = '';
		}
			
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($language_id);

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
			
		$language = new Language($language_directory);
		$language->load('mail/return');
		$language->load('default');
			
		$subject = sprintf($language->get('text_new_subject_discuss'), $store_name, $return_id);
			
		// HTML Mail
		$data = array();

		$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), $return_id);
		$data['text_new_discussion'] = $language->get('text_new_discussion');
		$data['text_discussion_link'] = $language->get('text_discussion_link');
		$data['text_new_subject_discuss'] = $language->get('text_new_subject_discuss');
		$data['text_merchant'] = $language->get('text_merchant');
		$data['text_customer'] = $language->get('text_customer');
		$data['store_name'] = $store_name;
		$data['store_url'] = $store_url;
		
		$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			
		$data['link'] = $store_url . 'index.php?route=account/return/info&return_id=' . $return_id;
		$data['link_merchant'] = $store_url . 'index.php?route=seller/account-return-list/detail&return_id=' . $return_id;
		
		/*data product*/
		$data['discussions'] = array();
			
		foreach ($discussions as $discussion) {	
			$data['discussions'][] = array(
				'return_id'    			=> $discussion['return_id'],
				'product_id'   			=> $discussion['product_id'],
				'comment' 				=> $discussion['comment'],
				'created_date'			=> date('d/m/Y H:i', strtotime($discussion['created_date'])),
				'firstname'				=> $discussion['firstname'],
				'nickname' 				=> $discussion['nickname'],
				'created_by' 			=> $discussion['created_by'],
				'seller_id'				=> $discussion['seller_id']
			);
		}
			
		$sent_to = $this->getCustomerEmail($created_by);
		$is_seller = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id='".(int)$created_by."'");
		
		if($is_seller->num_rows){
			$data['is_seller'] = 1;
		}else{
			$data['is_seller'] = 0;
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/return_discussion.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/return_discussion.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/return_discussion.tpl', $data);
		}
			
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($sent_to);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($store_name);
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
	}
	
	public function sendNotifChangeAction($return_id, $return_action_id){
		$this->load->model('account/return');
		$return_info = $this->model_account_return->getReturnNotif($return_id);
		
		if ($return_info) {
			
			$this->load->model('account/order');
			
			$return_id = $return_info['return_id'];
			$order_id = $return_info['order_id'];
			
			$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
			
			if ($order_query->num_rows) {
				$language_id = $order_query->row['language_id'];
				$store_name = $order_query->row['store_name'];
				$store_url = $order_query->row['store_url'];
			} else {
				$language_id = '';
				$store_name = '';
				$store_url = '';
			}
			
			$return_action= $this->db->query("SELECT * FROM " . DB_PREFIX . "return_action WHERE return_action_id = '" . (int)$return_action_id . "' AND language_id = '".(int)$language_id."'");
			
			if ($return_action->num_rows) {
				$return_action_name = $return_action->row['name'];
			} else {
				$return_action_name = '';
			}
			
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($language_id);

			if ($language_info) {
				$language_directory = $language_info['directory'];
			} else {
				$language_directory = '';
			}
			
			$language = new Language($language_directory);
			$language->load('mail/return');
			$language->load('default');
			
			$subject = sprintf($language->get('text_new_action'), $store_name, $return_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($language->get('text_new_action'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), $return_id);

			$data['text_action_change'] = $language->get('text_action_change');
			$data['text_update_return_status'] = $language->get('text_update_return_status');
			$data['text_new_link'] = $language->get('text_new_link');
			$data['text_new_return_detail'] = $language->get('text_new_return_detail');
			$data['text_new_return_id'] = $language->get('text_new_return_id');
			$data['text_new_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_date_added');
			$data['text_date_returned'] = $language->get('text_date_returned');
			$data['text_invoice'] = $language->get('text_invoice');
			$data['text_customer'] = $language->get('text_customer');
			$data['text_telephone'] = $language->get('text_telephone');
			$data['text_email'] = $language->get('text_email');
			$data['text_yes'] = $language->get('text_yes');
			$data['text_no'] = $language->get('text_no');
			$data['text_information'] = $language->get('text_information');
			$data['text_prod_name'] = $language->get('text_prod_name');
			$data['text_prod_model'] = $language->get('text_prod_model');
			$data['text_prod_qty'] = $language->get('text_prod_qty');
			$data['text_prod_open'] = $language->get('text_prod_open');
			$data['text_ret_reason'] = $language->get('text_ret_reason');
			$data['text_ret_comment'] = $language->get('text_ret_comment');
			$data['text_ret_status'] = $language->get('text_ret_status');
			$data['text_ret_action'] = $language->get('text_ret_action');
			$data['text_update_footer'] = $language->get('text_update_footer');
			$data['text_merchant_complete'] = $language->get('text_merchant_complete');
			$data['text_merchant_link'] = $language->get('text_merchant_link');

			$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
			
			$data['link'] = $store_url . 'index.php?route=account/return/info&return_id=' . $return_id;
			$data['link_merchant'] = $store_url . 'index.php?route=seller/account-return-list/detail&return_id=' . $return_id;
			
			$data['order_id'] = $order_id;
			$data['return_id'] = $return_id;
			$data['store_name'] = $store_name;
			$data['store_url'] = $store_url;
			$data['firstname'] = $return_info['firstname'];
			$data['lastname'] = $return_info['lastname'];
			$data['email'] = $return_info['email'];
			$data['telephone'] = $return_info['telephone'];
			$data['product'] = $return_info['product'];
			$data['model'] = $return_info['model'];
			$data['quantity'] = $return_info['quantity'];
			
			if($return_info['opened'] == 0){
				$data['opened'] = $language->get('text_no');
			}else if($return_info['opened'] == 1){
				$data['opened'] = $language->get('text_yes');
			}
			
			$data['reason'] = $return_info['reason'];
			$data['action'] = $return_info['action'];
			$data['status'] = $return_info['status'];
			$data['comment'] = $return_info['comment'];
			
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($return_info['date_added']));
			$data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($return_info['date_ordered']));
			$data['date_modified'] = date($this->language->get('date_format_short'), strtotime($return_info['date_modified']));
			
			$data['invoice_no'] = $return_info['invoice_no'];
			$data['product_id'] = $return_info['product_id'];
			$data['return_status_id'] = $return_info['return_status_id'];
			$data['return_action_name'] = $return_action_name;
			$data['confirm_action_changed'] = '';

			$email_merchant = $this->model_account_order->getMerchantInfo($return_info['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/return_action_change.tpl')) {
				$html = $this->load->view($this->config->get('config_template') . '/template/mail/return_action_change.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/return_action_change.tpl', $data);
			}
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($return_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
		}
	}
}