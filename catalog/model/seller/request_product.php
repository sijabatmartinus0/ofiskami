<?php
class ModelSellerRequestProduct extends Model {
	public function getKiosks() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pp_branch WHERE status = 1 ORDER BY pp_branch_name ASC");

		return $query->rows;
	}
	
	public function getRequests($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}
		
		$query = $this->db->query("SELECT pr.request_id, pr.kiosk_id, pb.pp_branch_name, pr.delivery_order, pr.sla_delivery, pr.status, pr.date_added, pr.date_modify, pr.date_received FROM " . DB_PREFIX . "ms_product_request pr LEFT JOIN " . DB_PREFIX . "pp_branch pb ON pb.pp_branch_id = pr.kiosk_id WHERE pr.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' ORDER BY pr.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function searchRequests($data=array(), $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT pr.request_id, pr.kiosk_id, pb.pp_branch_name, pr.delivery_order, pr.sla_delivery, pr.status, pr.date_added, pr.date_modify, pr.date_received FROM " . DB_PREFIX . "ms_product_request pr LEFT JOIN " . DB_PREFIX . "pp_branch pb ON pb.pp_branch_id = pr.kiosk_id WHERE pr.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND pr.kiosk_id LIKE '%".$this->db->escape($data['kiosk_id'])."%' ORDER BY pr.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function getTotalRequest() {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "ms_product_request WHERE seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "'");

		return $query->row['total'];
	}
	
	public function getTotalSearchRequest($kiosk_id) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "ms_product_request WHERE seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND kiosk_id LIKE '%".$this->db->escape($kiosk_id)."%'");

		return $query->row['total'];
	}
	
	public function getRequest($request_id) {
		$query = $this->db->query("SELECT pr.request_id, pr.kiosk_id, pb.pp_branch_name, pr.delivery_order, pr.sla_delivery, pr.status, pr.date_added, pr.date_modify, pr.date_received FROM " . DB_PREFIX . "ms_product_request pr LEFT JOIN " . DB_PREFIX . "pp_branch pb ON pb.pp_branch_id = pr.kiosk_id WHERE pr.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND pr.request_id = '".(int)$request_id."' ORDER BY pr.date_added DESC");

		return $query->row;
	}
	
	public function getRequestDetail($request_id) {
		$query = $this->db->query("SELECT prd.request_id, prd.product_id, prd.quantity_request, prd.price, prd.status, p.image, pd.name FROM " . DB_PREFIX . "ms_product_request_detail prd LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = prd.product_id LEFT JOIN " . DB_PREFIX . "product_description pd ON pd.product_id = prd.product_id WHERE prd.request_id = '".(int)$request_id."' AND pd.language_id = '".(int)$this->config->get('config_language_id') ."' ORDER BY pd.name ASC");

		return $query->rows;
	}
	
	public function getCategoryProduct($product_id){
		$query = $this->db->query("SELECT MAX(category_id) as category_id FROM " . DB_PREFIX ."product_to_category WHERE product_id='".(int)$product_id."'");
		
		$category_id = $query->row['category_id'];
		
		$result = $this->db->query("SELECT cd.name as category_name FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON cd.category_id = c.category_id WHERE c.category_id = '".(int)$category_id."' AND cd.language_id = '".(int)$this->config->get('config_language_id')."'");
		
		return $result->row['category_name'];
	}
	
	public function editRequest($data){
		$query = $this->db->query("UPDATE " . DB_PREFIX ."ms_product_request SET delivery_order='".$this->db->escape($data['input_delivery_order'])."' WHERE request_id='".(int)$this->db->escape($data['input_request_id'])."'");
	}
	
	public function addRequest($kiosk_id){
		$query = $this->db->query("INSERT INTO " . DB_PREFIX ."ms_product_request SET kiosk_id='".$this->db->escape($kiosk_id)."', seller_id='".(int)$this->MsLoader->MsSeller->getSellerId()."', sla_delivery='1', status=1, date_added=NOW()");
		
		$request_id = $this->db->getLastId();
		
		return $request_id;
	}
	
	public function addRequestDetail($data=array()){
		$query = $this->db->query("INSERT INTO " . DB_PREFIX ."ms_product_request_detail SET request_id='".(int)$this->db->escape($data['request_id'])."', product_id='".(int)$this->db->escape($data['product_id'])."', quantity_request='".(int)$this->db->escape($data['quantity_request'])."', quantity_actual='".(int)$this->db->escape($data['quantity_actual'])."', price='".$this->db->escape($data['price'])."', life_cycle='".(int)$this->config->get('config_kiosk_life_cycle')."', status='".(int)MsProduct::STATUS_ON_REVIEW."'");
	}
	
	public function getProductSeller($data=array()){
		$query = $this->db->query("SELECT p.product_id, pd.name  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON pd.product_id = p.product_id LEFT JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = p.product_id WHERE mp.seller_id = '".(int)$this->MsLoader->MsSeller->getSellerId()."' AND pd.language_id = '".(int)$this->config->get('config_language_id') ."' AND pd.name LIKE '%".$this->db->escape($data['filter_name'])."%' ORDER BY pd.name ASC LIMIT " . (int)$data['start'] . "," . (int)$data['limit']);

		return $query->rows;
	}
	
	public function isMinimumPassed($product_id, $quantity){
		$query = $this->db->query("SELECT MAX(category_id) as category_id FROM " . DB_PREFIX ."product_to_category WHERE product_id='".(int)$product_id."'");
		
		$category_id = $query->row['category_id'];
		
		if((int)$category_id != 0){
			$result = $this->db->query("SELECT kiosk_minimum_stock as minimum FROM " . DB_PREFIX . "category WHERE category_id = '".(int)$category_id."'");
			$minimum = $result->row['minimum'];
		}
		
		if((int)$category_id == 0){
			return false;
		}else if((int)$quantity < (int)$minimum){
			return false;
		}else{
			return true;
		}
	}
}
?>