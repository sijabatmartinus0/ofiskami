<?php
class ModelSellerAccountProduct extends Model {
	public function getProducts($start = 0, $limit = 20, $data=array()) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT
					SQL_CALC_FOUND_ROWS "
					// additional columns
					. (isset($data['product_earnings']) ? "
						(SELECT SUM(seller_net_amt) AS seller_total
						FROM " . DB_PREFIX . "order_product op
						INNER JOIN `" . DB_PREFIX . "ms_order_product_data` mopd
							ON (op.product_id = mopd.product_id)
						WHERE op.product_id = p.product_id) as product_earnings,
					" : "")
					
					."p.product_id as 'product_id',
					p.image as 'p.image',
					p.price as 'p.price',
					sum(ps.discount) as 'specialok', 
					coalesce(sum(ps.price)) as 'specialmerchant',
					pd.name as 'pd.name',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					mp.product_status as 'mp.product_status',
					mp.product_approved as 'mp.product_approved',
					mp.number_sold as 'mp.number_sold',
					mp.list_until as 'mp.list_until',
					p.date_added as 'p.date_created',
					p.date_modified  as 'p.date_modified',
					pd.description as 'pd.description'
				FROM " . DB_PREFIX . "product p
				INNER JOIN " . DB_PREFIX . "product_description pd
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "product_special ps
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product mp
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_seller ms
					USING (seller_id)
				WHERE 1 = 1"
				
				. (isset($data['product_status']) ? " AND product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")" : '')
				. " AND ms.seller_id =  " . (int)$this->MsLoader->MsSeller->getSellerId(). " AND pd.language_id =  " . (int)$this->config->get('config_language_id') . " GROUP BY p.product_id ORDER BY ".$data['sort']." ".$data['direction']." LIMIT " . (int)$start . "," . (int)$limit);
		/*echo "SELECT
					SQL_CALC_FOUND_ROWS "
					// additional columns
					. (isset($data['product_earnings']) ? "
						(SELECT SUM(seller_net_amt) AS seller_total
						FROM " . DB_PREFIX . "order_product op
						INNER JOIN `" . DB_PREFIX . "ms_order_product_data` mopd
							ON (op.product_id = mopd.product_id)
						WHERE op.product_id = p.product_id) as product_earnings,
					" : "")
					
					."p.product_id as 'product_id',
					p.image as 'p.image',
					p.price as 'p.price',
					sum(ps.discount) as 'specialok', 
					coalesce(sum(ps.price)) as 'specialmerchant',
					pd.name as 'pd.name',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					mp.product_status as 'mp.product_status',
					mp.product_approved as 'mp.product_approved',
					mp.number_sold as 'mp.number_sold',
					mp.list_until as 'mp.list_until',
					p.date_added as 'p.date_created',
					p.date_modified  as 'p.date_modified',
					pd.description as 'pd.description'
				FROM " . DB_PREFIX . "product p
				INNER JOIN " . DB_PREFIX . "product_description pd
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "product_special ps
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product mp
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_seller ms
					USING (seller_id)
				WHERE 1 = 1"
				
				. (isset($data['product_status']) ? " AND product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")" : '')
				. " AND ms.seller_id =  " . (int)$this->MsLoader->MsSeller->getSellerId(). " AND pd.language_id =  " . (int)$this->config->get('config_language_id') . "GROUP BY p.product_id ORDER BY ".$data['sort']." ".$data['direction']." LIMIT " . (int)$start . "," . (int)$limit;die();*/

		return $query->rows;
	}
	
	public function getTotalProducts($data=array()) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "ms_product` WHERE seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND product_status IN (".$this->db->escape(implode(',', $data['product_status'])).")");

		return $query->row['total'];
	}
	
	public function getSearchProducts($start = 0, $limit = 20, $data=array()) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT
					SQL_CALC_FOUND_ROWS "
					// additional columns
					. (isset($data['product_earnings']) ? "
						(SELECT SUM(seller_net_amt) AS seller_total
						FROM " . DB_PREFIX . "order_product op
						INNER JOIN `" . DB_PREFIX . "ms_order_product_data` mopd
							ON (op.product_id = mopd.product_id)
						WHERE op.product_id = p.product_id) as product_earnings,
					" : "")
					
					."p.product_id as 'product_id',
					p.image as 'p.image',
					p.price as 'p.price',
					pd.name as 'pd.name',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					mp.product_status as 'mp.product_status',
					mp.product_approved as 'mp.product_approved',
					mp.number_sold as 'mp.number_sold',
					mp.list_until as 'mp.list_until',
					p.date_added as 'p.date_created',
					p.date_modified  as 'p.date_modified',
					pd.description as 'pd.description'
				FROM " . DB_PREFIX . "product p
				INNER JOIN " . DB_PREFIX . "product_description pd
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product mp
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_seller ms
					USING (seller_id)
				WHERE 1 = 1"
				
				. (isset($data['search_name']) ? " AND pd.name LIKE '%".$data['search_name']."%'" : '')
				. (isset($data['search_sku']) ? " AND p.sku LIKE '%".$data['search_sku']."%'" : '')
				. (isset($data['search_date_added']) ? " AND p.date_added LIKE '%".$data['search_date_added']."%'" : '')
				. (($data['search_status'] != '') ? " AND mp.product_status = '".(int)$data['search_status']."'" : " AND mp.product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")")
				. (($data['search_category'] != '') ? " AND (SELECT count(category_id) FROM oc_product_to_category cp WHERE cp.product_id=p.product_id and cp.category_id='".(int)$data['search_category']."')>0 " : "")
				. " AND ms.seller_id =  " . (int)$this->MsLoader->MsSeller->getSellerId(). " AND pd.language_id =  " . (int)$this->config->get('config_language_id') . " ORDER BY ".$data['sort']." ".$data['direction']." LIMIT " . (int)$start . "," . (int)$limit);


		return $query->rows;
	}
	
	public function getTotalSearchProducts($data=array()) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "ms_product` mp INNER JOIN " . DB_PREFIX . "product p ON p.product_id = mp.product_id INNER JOIN " . DB_PREFIX . "product_description pd ON pd.product_id = mp.product_id INNER JOIN " . DB_PREFIX . "product_to_category pc ON pc.product_id = mp.product_id  WHERE mp.seller_id = '" . (int)$this->MsLoader->MsSeller->getSellerId() . "' AND pd.language_id='".$this->config->get('config_language_id')."'"
			. (isset($data['search_name']) ? " AND pd.name LIKE '%".$data['search_name']."%'" : '')
			. (isset($data['search_sku']) ? " AND p.sku LIKE '%".$data['search_sku']."%'" : '')
			. (isset($data['search_date_added']) ? " AND p.date_added LIKE '%".$data['search_date_added']."%'" : '')
			. (($data['search_status'] != '') ? " AND mp.product_status LIKE '%".$data['search_status']."%'" : " AND mp.product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")"
			. (($data['search_category'] != '') ? " AND (SELECT count(category_id) FROM oc_product_to_category cp WHERE cp.product_id=p.product_id and cp.category_id='".(int)$data['search_category']."')>0 " : ""))
		);

		return $query->row['total'];
	}
	
	public function getCategoryProductSeller(){
		$query = $this->db->query("SELECT DISTINCT(c.category_id) FROM " . DB_PREFIX . "category_description c INNER JOIN " . DB_PREFIX . "product_to_category ptc ON ptc.category_id = c.category_id INNER JOIN " . DB_PREFIX . "ms_product mp ON mp.product_id = ptc.product_id WHERE mp.seller_id = " . (int)$this->MsLoader->MsSeller->getSellerId(). " and c.language_id = " . (int)$this->config->get('config_language_id') . "");
		
		return $query->rows;
	}
	
	public function getCategoryName($category_id){
		$query = $this->db->query("SELECT name as name FROM " . DB_PREFIX . "category_description WHERE category_id = " . (int)$category_id. " and language_id = " . (int)$this->config->get('config_language_id') . "");
		
		return $query->row['name'];
	}
}