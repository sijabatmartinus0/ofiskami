<?php
class ModelLocalisationShipping extends Model {
	public function getShipping() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping WHERE status = '1'");

		return $query->rows;
	}

	public function getShippingById($shipping_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping WHERE shipping_id = '" . (int)$shipping_id . "' AND status = '1'");

		return $query->row;
	}
	
	public function getShippingServiceById($shipping_id) {
		// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping_service WHERE shipping_id = '" . (int)$shipping_id . "' AND status = '1'");

		// return $query->rows;
		
		// $shipping_data = $this->cache->get('shipping.' . (int)$shipping_id);

		// if (!$shipping_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping_service WHERE shipping_id = '" . (int)$shipping_id . "' AND status = '1' ORDER BY name");

			$shipping_data = $query->rows;

			// $this->cache->set('shipping.' . (int)$shipping_id, $shipping_data);
		// }

		return $shipping_data;
	}
}