<?php
class ModelLocalisationCountry extends Model {
	public function getCountry($country_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND status = '1'");

		return $query->row;
	}
	
	public function getCountryKiosk($country_id) {
		$query = $this->db->query("SELECT DISTINCT c.* FROM " . DB_PREFIX . "country c, " . DB_PREFIX . "pp_branch ppb  WHERE c.country_id=ppb.country_id AND c.country_id = '" . (int)$country_id . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCountries() {
		$country_data = $this->cache->get('country.status');

		if (!$country_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE status = '1' ORDER BY name ASC");

			$country_data = $query->rows;

			$this->cache->set('country.status', $country_data);
		}

		return $country_data;
	}
}