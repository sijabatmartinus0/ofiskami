<?php
class ModelLocalisationPpBranch extends Model {
	public function getPpBranch($pp_branch_id) {
		$query = $this->db->query("
			SELECT opb.*, oc.name as country, oz.name as zone, oci.name as city, od.name as district, os.name as subdistrict FROM " . DB_PREFIX . "pp_branch opb
			LEFT JOIN " . DB_PREFIX . "country oc ON opb.country_id=oc.country_id 
			LEFT JOIN " . DB_PREFIX . "zone oz ON opb.zone_id=oz.zone_id 
			LEFT JOIN " . DB_PREFIX . "city oci ON opb.city_id=oci.city_id 
			LEFT JOIN " . DB_PREFIX . "district od ON opb.district_id=od.district_id 
			LEFT JOIN " . DB_PREFIX . "subdistrict os ON opb.subdistrict_id=os.subdistrict_id			
			WHERE opb.pp_branch_id = '" . (int)$pp_branch_id . "' AND opb.status = '1'"
		);

		return $query->row;
	}
	
	public function getPpBranchs($data=array()) {
		$sql="SELECT * FROM " . DB_PREFIX . "pp_branch ppb WHERE ppb.status = '1'"
		. (isset($data['country_id']) ? " AND ppb.country_id =  " .  (int)$data['country_id'] : '')
		. (isset($data['zone_id']) ? " AND ppb.zone_id =  " .  (int)$data['zone_id'] : '')
		. (isset($data['city_id']) ? " AND ppb.city_id =  " .  (int)$data['city_id'] : '')
		. (isset($data['district_id']) ? " AND ppb.district_id =  " .  (int)$data['district_id'] : '')
		. (isset($data['subdistrict_id']) ? " AND ppb.subdistrict_id =  " .  (int)$data['subdistrict_id'] : '')
		. (isset($data['pp_branch_name']) ? " AND ppb.pp_branch_name LIKE  '%" .  $this->db->escape($data['pp_branch_name']) ."%'" : '');
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getPpBranchDetail($pp_branch_id){
		$branch=array();
		$sql="
			SELECT 
				ppb.telephone,
				ppb.pp_branch_id,
				ppb.pp_branch_name,
				ppb.company,
				ppb.address,
				ppb.postcode,
				ppb.country_id,
				ppb.zone_id,
				ppb.city_id,
				ppb.district_id,
				ppb.subdistrict_id,
				oc.name as country,
				oz.name as zone,
				oc1.name as city,
				od.name as district,
				os.name as subdistrict 
			FROM " . DB_PREFIX . "pp_branch ppb
			LEFT JOIN " . DB_PREFIX . "country oc ON oc.country_id=ppb.country_id
			LEFT JOIN " . DB_PREFIX . "zone oz ON oz.zone_id=ppb.zone_id
			LEFT JOIN " . DB_PREFIX . "city oc1 ON oc1.city_id=ppb.city_id
			LEFT JOIN " . DB_PREFIX . "district od ON od.district_id=ppb.district_id
			LEFT JOIN " . DB_PREFIX . "subdistrict os ON os.subdistrict_id=ppb.subdistrict_id
			WHERE ppb.pp_branch_id=".(int)$pp_branch_id;

		$query = $this->db->query($sql);
		
		$result=$query->row;
		$branch['id']=$result['pp_branch_id'];
		$branch['name']=$result['pp_branch_name'];
		$branch['company']=$result['company'];
		$branch['telephone']=$result['telephone'];
		$branch['detail']=$result['address']."<br>".
						$result['city'].",".$result['district'].",".$result['subdistrict'].",".$result['postcode']."<br>".$result['zone'].",".$result['country'];
		return $branch;
	}
}