<?php
class ModelLocalisationMunicipal extends Model {
	public function getMunicipal($municipal_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "municipal WHERE municipal_id = '" . (int)$municipal_id . "' AND status = '1'");

		return $query->row;
	}

	public function getMunicipalsByCityId($city_id) {
		$muncipal_data = $this->cache->get('municipal.' . (int)$city_id);

		if (!$muncipal_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "municipal WHERE city_id = '" . (int)$city_id . "' AND status = '1' ORDER BY name");

			$muncipal_data = $query->rows;

			$this->cache->set('municipal.' . (int)$city_id, $muncipal_data);
		}

		return $muncipal_data;
	}
}