<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}
	
	public function getZoneKiosk($zone_id) {
		$query = $this->db->query("SELECT DISTINCT z.* FROM " . DB_PREFIX . "zone z," . DB_PREFIX . "pp_branch ppb WHERE z.zone_id=ppb.zone_id AND z.zone_id = '" . (int)$zone_id . "' AND z.status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {
		$zone_data = $this->cache->get('zone.' . (int)$country_id);

		if (!$zone_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$zone_data = $query->rows;

			$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}
	
	public function getZonesKioskByCountryId($country_id) {
		$zone_data = false;

		if (!$zone_data) {
			$query = $this->db->query("SELECT DISTINCT z.* FROM " . DB_PREFIX . "zone z," . DB_PREFIX . "pp_branch ppb WHERE z.zone_id=ppb.zone_id AND z.country_id = '" . (int)$country_id . "' AND z.status = '1' ORDER BY z.name");
			
			$zone_data = $query->rows;

			//$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}
}