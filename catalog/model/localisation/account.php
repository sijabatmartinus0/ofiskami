<?php
class ModelLocalisationAccount extends Model {
	public function getAccounts($sort = array()) {
		$query = $this->db->query('SELECT account_id, name, code, status FROM ' . DB_PREFIX . 'account WHERE status = 1 '. (isset($sort['limit']) ? ' LIMIT '.(int)$sort['offset'].', '.(int)($sort['limit']) : '') );

		return $query->rows;
	}

	public function getAccountById($account_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "account WHERE account_id = '" . (int)$account_id . "' AND status = '1'");

		return $query->row;
	}
	
	public function getTotalAccounts() {
		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "account WHERE status = '1'";
		$exec = $this->db->query($sql);
		return $exec->row['total'];
	}
	
	public function searchAccount($account_name,$sort = array()){
		$sql = "SELECT account_id, name, code, status FROM " .DB_PREFIX. "account WHERE status = 1 AND name LIKE '%".$account_name."%'".(isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].", ".(int)($sort['limit']) : "")." ";
		$exec = $this->db->query($sql);
		
		return $exec->rows;
	}
	
	public function totalSearchAccount($account_name){
		$sql = "SELECT COUNT(*) as total FROM " .DB_PREFIX. "account WHERE status = 1 AND name LIKE '%".$account_name."%'";
		$exec = $this->db->query($sql);
		
		return $exec->row['total'];
	}
}