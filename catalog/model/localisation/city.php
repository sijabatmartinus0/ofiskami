<?php
class ModelLocalisationCity extends Model {
	public function getCity($city_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "' AND status = '1'");

		return $query->row;
	}

	public function getCitiesByZoneId($zone_id) {
		$city_data = $this->cache->get('city.' . (int)$zone_id);

		if (!$city_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE zone_id = '" . (int)$zone_id . "' AND status = '1' ORDER BY name");

			$city_data = $query->rows;

			$this->cache->set('city.' . (int)$zone_id, $city_data);
		}

		return $city_data;
	}
	
	public function getCitiesKioskByZoneId($zone_id) {
		$city_data = false;

		if (!$city_data) {
			$query = $this->db->query("SELECT DISTINCT cc.* FROM " . DB_PREFIX . "city  cc," . DB_PREFIX . "pp_branch ppb WHERE cc.city_id=ppb.city_id AND   cc.zone_id = '" . (int)$zone_id . "' AND cc.status = '1' ORDER BY cc.name");

			$city_data = $query->rows;

			//$this->cache->set('city.' . (int)$zone_id, $city_data);
		}

		return $city_data;
	}
	
	public function addCityRO($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "city_ro SET city_id = '" . $this->db->escape($data['city_id']) . "', province_id = '" . $this->db->escape($data['province_id']) . "', province = '" . $this->db->escape($data['province']) . "', type = '" . $this->db->escape($data['type']) . "', city_name = '" . $this->db->escape($data['city_name']) . "', postal_code = '" . $this->db->escape($data['postal_code']) . "'");
	}
}