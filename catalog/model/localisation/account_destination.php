<?php
class ModelLocalisationAccountDestination extends Model {
	public function getAccountDestinations() {
		$query = $this->db->query("SELECT account_destination_id, name, account_number FROM " . DB_PREFIX . "account_destination ad left join ". DB_PREFIX."account a ON ad.account_id = a.account_id WHERE a.status = 1 AND ad.status=1");

		return $query->rows;
	}

	
}