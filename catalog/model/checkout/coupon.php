<?php
class ModelCheckoutCoupon extends Model {
	public function getCoupon($code) {
		$message = "";
		$this->load->language('checkout/coupon');
		$status = true;

		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND status = '1'");

		if ($coupon_query->num_rows) {
			
			if ((int)$coupon_query->row['date_type'] ==0) {
				$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
			}
			
			if ($coupon_query->num_rows) {
			
				if ($coupon_query->row['total'] > $this->cart->getSubTotal()) {
					$status = false;
					$message = $this->language->get('error_minimum');
				}

				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

				if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
					$status = false;
					$message = $this->language->get('error_coupon');
				}

				if ($coupon_query->row['logged'] && !$this->customer->getId()) {

					$status = false;
				$message = $this->language->get('error_customer');
				}

				if ($this->customer->getId()) {
					$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$this->customer->getId() . "'");

					if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
						$status = false;
						$message = $this->language->get('error_expired');
					}
				}

				// Newsletters
				$coupon_newsletter_data = array();

				$coupon_newsletter_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_newsletter` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

				foreach ($coupon_newsletter_query->rows as $newsletter) {
					$coupon_newsletter_data[] = $newsletter['email'];
				}
				
				$newsletter_data = array();

				if ($coupon_newsletter_data) {
				
//var_dump($coupon_newsletter_data);
//echo $this->customer->getEmail();
			
			
					if (in_array($this->customer->getEmail(), $coupon_newsletter_data)) {
					
						$newsletter_data[] = $this->customer->getEmail();
					}

					if (!$newsletter_data) {
					echo "aaa";
						$status = false;
						$message = $this->language->get('error_newsletter');
					}
				}
				
				// Customers
				$coupon_customer_data = array();

				$coupon_customer_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_customer` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

				foreach ($coupon_customer_query->rows as $customer) {
					$coupon_customer_data[] = $customer['customer_id'];
				}
				
				$customer_data = array();

				if ($coupon_customer_data) {
					if (in_array($this->customer->getId(), $coupon_customer_data)) {
						$customer_data[] = $this->customer->getId();
					}

					if (!$customer_data) {
				
					
						$status = false;
						$message = $this->language->get('error_customer');
					}
				}
				
				// Products
				$coupon_product_data = array();

				$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

				foreach ($coupon_product_query->rows as $product) {
					$coupon_product_data[] = $product['product_id'];
				}

				// Categories
				$coupon_category_data = array();

				$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

				foreach ($coupon_category_query->rows as $category) {
					$coupon_category_data[] = $category['category_id'];
				}

				$product_data = array();

				if ($coupon_product_data || $coupon_category_data) {
					foreach ($this->cart->getProducts() as $product) {
						if (in_array($product['product_id'], $coupon_product_data)) {
							$product_data[] = $product['product_id'];

							continue;
						}

						foreach ($coupon_category_data as $category_id) {
							$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

							if ($coupon_category_query->row['total']) {
								$product_data[] = $product['product_id'];

								continue;
							}
						}
					}

					if (!$product_data) {
						$status = false;
						$message = $this->language->get('error_product');
					}
				}
				
				//Cart Product
				if((int)$this->cart->getTotal()==0){
					$status = false;
					$message = $this->language->get('error_cart');
				}
				
			}else {
				$status = false;
				$message = $this->language->get('error_expired');
			}
		} else {
			$status = false;
			$message = $this->language->get('error_expired');
		}


if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added'],
				'message'		=> 'Success',
				'result'		=> $status
			);
		}else{
			return array(
				'message'		=> $message,
				'result'		=> $status
			);
		}
	}
}
