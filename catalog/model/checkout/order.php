<?php
class ModelCheckoutOrder extends Model {
	public function addOrder($data) {
		$this->event->trigger('pre.order.add', $data);

		//$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? serialize($data['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? serialize($data['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', marketing_id = '" . (int)$data['marketing_id'] . "', tracking = '" . $this->db->escape($data['tracking']) . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET 
			store_id = '" . (int)$data['store_id'] . "', 
			store_name = '" . $this->db->escape($data['store_name']) . "', 
			store_url = '" . $this->db->escape($data['store_url']) . "', 
			customer_id = '" . (int)$data['customer_id'] . "', 
			customer_group_id = '" . (int)$data['customer_group_id'] . "', 
			firstname = '" . $this->db->escape($data['firstname']) . "', 
			lastname = '" . $this->db->escape($data['lastname']) . "', 
			email = '" . $this->db->escape($data['email']) . "', 
			telephone = '" . $this->db->escape($data['telephone']) . "', 
			fax = '" . $this->db->escape($data['fax']) . "', 
			custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "', 
			payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', 
			payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', 
			payment_company = '" . $this->db->escape($data['payment_company']) . "', 
			payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', 
			payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', 
			payment_subdistrict_id = '" . $this->db->escape($data['payment_subdistrict_id']) . "', 
			payment_subdistrict = '" . $this->db->escape($data['payment_subdistrict']) . "', 
			payment_district_id = '" . $this->db->escape($data['payment_district_id']) . "', 
			payment_district = '" . $this->db->escape($data['payment_district']) . "', 
			payment_city_id = '" . $this->db->escape($data['payment_city_id']) . "', 
			payment_city = '" . $this->db->escape($data['payment_city']) . "', 
			payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', 
			payment_country = '" . $this->db->escape($data['payment_country']) . "', 
			payment_country_id = '" . (int)$data['payment_country_id'] . "', 
			payment_zone = '" . $this->db->escape($data['payment_zone']) . "', 
			payment_zone_id = '" . (int)$data['payment_zone_id'] . "', 
			payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', 
			payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? serialize($data['payment_custom_field']) : '') . "', 
			payment_method = '" . $this->db->escape($data['payment_method']) . "', 
			payment_code = '" . $this->db->escape($data['payment_code']) . "', 
			comment = '" . $this->db->escape($data['comment']) . "', 
			total = '" . (float)$data['total'] . "', 
			affiliate_id = '" . (int)$data['affiliate_id'] . "', 
			commission = '" . (float)$data['commission'] . "', 
			marketing_id = '" . (int)$data['marketing_id'] . "', 
			tracking = '" . $this->db->escape($data['tracking']) . "', 
			language_id = '" . (int)$data['language_id'] . "', 
			currency_id = '" . (int)$data['currency_id'] . "', 
			currency_code = '" . $this->db->escape($data['currency_code']) . "', 
			currency_value = '" . (float)$data['currency_value'] . "', 
			ip = '" . $this->db->escape($data['ip']) . "', 
			forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', 
			user_agent = '" . $this->db->escape($data['user_agent']) . "', 
			accept_language = '" . $this->db->escape($data['accept_language']) . "', 
			date_added = NOW(), 
			date_modified = NOW()");
		
		$order_id = $this->db->getLastId();
		
		/*
		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");
	
				$order_product_id = $this->db->getLastId();
	
				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}
		*/
		
		// Gift Voucher
		$this->load->model('checkout/voucher');

		// Vouchers
		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");
	
				$order_voucher_id = $this->db->getLastId();
	
				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);
	
				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}
		
		// Totals
		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}
		
		$this->event->trigger('post.order.add', $order_id);

		return $order_id;
	}

	public function editOrder($order_id, $data) {
		$this->event->trigger('pre.order.edit', $data);
		
		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		store_id = '" . (int)$data['store_id'] . "', 
		store_name = '" . $this->db->escape($data['store_name']) . "', 
		store_url = '" . $this->db->escape($data['store_url']) . "', 
		customer_id = '" . (int)$data['customer_id'] . "', 
		customer_group_id = '" . (int)$data['customer_group_id'] . "', 
		firstname = '" . $this->db->escape($data['firstname']) . "', 
		lastname = '" . $this->db->escape($data['lastname']) . "', 
		email = '" . $this->db->escape($data['email']) . "', 
		telephone = '" . $this->db->escape($data['telephone']) . "', 
		fax = '" . $this->db->escape($data['fax']) . "', 
		custom_field = '" . $this->db->escape(serialize($data['custom_field'])) . "', 
		payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', 
		payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', 
		payment_company = '" . $this->db->escape($data['payment_company']) . "', 
		payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', 
		payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', 
		payment_subdistrict_id = '" . $this->db->escape($data['payment_subdistrict_id']) . "', 
		payment_subdistrict = '" . $this->db->escape($data['payment_subdistrict']) . "', 
		payment_district_id = '" . $this->db->escape($data['payment_district_id']) . "', 
		payment_district = '" . $this->db->escape($data['payment_district']) . "', 
		payment_city_id = '" . $this->db->escape($data['payment_city_id']) . "', 
		payment_city = '" . $this->db->escape($data['payment_city']) . "', 
		payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', 
		payment_country = '" . $this->db->escape($data['payment_country']) . "', 
		payment_country_id = '" . (int)$data['payment_country_id'] . "', 
		payment_zone = '" . $this->db->escape($data['payment_zone']) . "', 
		payment_zone_id = '" . (int)$data['payment_zone_id'] . "', 
		payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', 
		payment_custom_field = '" . $this->db->escape(serialize($data['payment_custom_field'])) . "', 
		payment_method = '" . $this->db->escape($data['payment_method']) . "', 
		payment_code = '" . $this->db->escape($data['payment_code']) . "', 
		comment = '" . $this->db->escape($data['comment']) . "', 
		total = '" . (float)$data['total'] . "', 
		affiliate_id = '" . (int)$data['affiliate_id'] . "', 
		commission = '" . (float)$data['commission'] . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		/*$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");

		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");
	
				$order_product_id = $this->db->getLastId();
	
				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}*/
		
		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		// Vouchers
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "voucher WHERE order_id = '" . (int)$order_id . "'");
		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");
	
				$order_voucher_id = $this->db->getLastId();
	
				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);
	
				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}
		
		// Totals
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}
		
		$this->event->trigger('post.order.edit', $order_id);
	}

	public function deleteOrder($order_id) {
		$this->event->trigger('pre.order.delete', $order_id);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_fraud` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE `or`, ort FROM `" . DB_PREFIX . "order_recurring` `or`, `" . DB_PREFIX . "order_recurring_transaction` `ort` WHERE order_id = '" . (int)$order_id . "' AND ort.order_recurring_id = `or`.order_recurring_id");

		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_transaction` WHERE order_id = '" . (int)$order_id . "'");

		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		$this->event->trigger('post.order.delete', $order_id);
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => unserialize($order_query->row['custom_field']),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => unserialize($order_query->row['payment_custom_field']),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_bank'          => $order_query->row['payment_bank'],
				'payment_code'            => $order_query->row['payment_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_payment_code'         => $order_query->row['order_payment_code'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added']
			);
		} else {
			return false;
		}
	}

	public function createInvoiceNoDetail($order_id) {
		$order_info = $this->getOrderDetailByOrderID($order_id);

		foreach($order_info as $order){
			if ($order && !$order['invoice_no']) {
				$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order_detail` WHERE invoice_prefix = '" . $this->db->escape($order['invoice_prefix']) . "'");

				if ($query->row['invoice_no']) {
					$invoice_no = $query->row['invoice_no'] + 1;
				} else {
					$invoice_no = 1;
				}

				$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order['invoice_prefix']) . "' WHERE order_detail_id = '" . (int)$order['order_detail_id'] . "'");
			}
		}
	}
	
	public function updatePaymentBank($order_id, $product) {
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET payment_bank = '" . $this->db->escape($product) . "' WHERE order_id = '" . (int)$order_id . "'");
	}
	
	public function getOrderDetailByOrderID($order_id){
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_detail` WHERE order_id = '" . (int)$order_id . "'");

		return $order_query->rows;
	}
	
	public function addOrderHistory($order_id, $order_status_id, $comment = '', $notify = false) {
		$this->event->trigger('pre.order.history.add', $order_id);

		$order_info = $this->getOrder($order_id);

		if ($order_info) {
			// Fraud Detection
			$this->load->model('account/customer');

			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');

				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

				if (!$safe && $risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Ban IP
			if (!$safe) {
				$status = false;

				if ($order_info['customer_id']) {
					$results = $this->model_account_customer->getIps($order_info['customer_id']);

					foreach ($results as $result) {
						if ($this->model_account_customer->isBanIp($result['ip'])) {
							$status = true;

							break;
						}
					}
				} else {
					$status = $this->model_account_customer->isBanIp($order_info['ip']);
				}

				if ($status) {
					$order_status_id = $this->config->get('config_order_status_id');
				}
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");

			// If current order status is not processing or complete but new status is processing or complete then commence completing the order
			if (!in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
					}
				}

				// Redeem coupon, vouchers and reward points
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
						$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
					}
				}

				// Add commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id'] && $this->config->get('config_affiliate_auto')) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->addTransaction($order_info['affiliate_id'], $order_info['commission'], $order_id);
				}
			}

			// If old order status is the processing or complete status but new status is not then commence restock, and remove coupon, voucher and reward history
			//if (in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && !in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
			if ((int)$order_status_id==23) {
				// Restock
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {
					$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

					$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
					}
				}

				// Remove coupon, vouchers and reward points history
				$this->load->model('account/order');

				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'unconfirm')) {
						$this->{'model_total_' . $order_total['code']}->unconfirm($order_id);
					}
				}

				// Remove commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id']) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->deleteTransaction($order_id);
				}
			}

			$this->cache->delete('product');

			// If order status is 0 then becomes greater than 0 send main html email
			if (!$order_info['order_status_id'] && $order_status_id) {
				// Check for any downloadable products
				$download_status = false;

				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					// Check if there are any linked downloads
					$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if ($product_download_query->row['total']) {
						$download_status = true;
					}
				}

				// Load the language for any mails that might be required to be sent out
				$language = new Language($order_info['language_directory']);
				$language->load('default');
				$language->load('mail/order');

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$order_status = $order_status_query->row['name'];
				} else {
					$order_status = '';
				}

				$subject = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

				// HTML Mail
				$data = array();

				$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
				$data['text_link'] = $language->get('text_new_link');
				$data['text_download'] = $language->get('text_new_download');
				$data['text_order_detail'] = $language->get('text_new_order_detail');
				$data['text_instruction'] = $language->get('text_new_instruction');
				$data['text_order_id'] = $language->get('text_new_order_id');
				$data['text_date_added'] = $language->get('text_new_date_added');
				$data['text_payment_method'] = $language->get('text_new_payment_method');
				$data['text_shipping_method'] = $language->get('text_new_shipping_method');
				$data['text_email'] = $language->get('text_new_email');
				$data['text_telephone'] = $language->get('text_new_telephone');
				$data['text_ip'] = $language->get('text_new_ip');
				$data['text_order_status'] = $language->get('text_new_order_status');
				$data['text_payment_address'] = $language->get('text_new_payment_address');
				$data['text_shipping_address'] = $language->get('text_new_shipping_address');
				$data['text_product'] = $language->get('text_new_product');
				$data['text_model'] = $language->get('text_new_model');
				$data['text_quantity'] = $language->get('text_new_quantity');
				$data['text_price'] = $language->get('text_new_price');
				$data['text_total'] = $language->get('text_new_total');
				$data['text_footer'] = $language->get('text_new_footer');

				$data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');
				$data['store_name'] = $order_info['store_name'];
				$data['store_url'] = $order_info['store_url'];
				$data['customer_id'] = $order_info['customer_id'];
				$data['link'] = $order_info['store_url'] . 'index.php?route=account/order';

				if ($download_status) {
					$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
				} else {
					$data['download'] = '';
				}

				$data['order_id'] = $order_id;
				$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
				$data['payment_method'] = $order_info['payment_method'];
				$data['shipping_method'] = '';//$order_info['shipping_method'];
				$data['email'] = $order_info['email'];
				$data['telephone'] = $order_info['telephone'];
				$data['ip'] = $order_info['ip'];
				$data['order_status'] = $order_status;

				if ($comment && $notify) {
					$data['comment'] = nl2br($comment);
				} else {
					$data['comment'] = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				/*if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
				*/
				$data['shipping_address'] = '';
				$this->load->model('tool/upload');

				// Products
				$data['products'] = array();

				foreach ($order_product_query->rows as $product) {
					$option_data = array();

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}
					
					/*vqmod change*/
					$this->load->language('multiseller/multiseller');
					$seller = $this->MsLoader->MsSeller->getSeller($this->MsLoader->MsProduct->getSellerId($product['product_id']));
					/*------------------------------*/

					$data['products'][] = array(
						//vqmod change
						'product_id' => $product['product_id'],
						'seller_text' => $seller ? "<br/ > " . $this->language->get('ms_by') . " {$seller['ms.nickname']} <br />" : '',
						
						'name'     => $product['name'],
						'model'    => $product['model'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				// Vouchers
				$data['vouchers'] = array();

				$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_voucher_query->rows as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				// Order Totals
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $total) {
					$data['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}

				// Can not send confirmation emails for CBA orders as email is unknown
				$this->load->model('payment/amazon_checkout');

				if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
					// Text Mail
					$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

					if ($comment && $notify) {
						$text .= $language->get('text_new_instruction') . "\n\n";
						$text .= $comment . "\n\n";
					}

					// Products
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['customer_id']) {
						$text .= $language->get('text_new_link') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/order';
					}

					if ($download_status) {
						$text .= $language->get('text_new_download') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/download';
					}

					// Comment
					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					$text .= "\n\n" . $language->get('text_new_footer') . "\n\n";

					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($order_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($order_info['store_name']);
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->setText($text);
					$mail->send();
				}

				// Admin Alert Mail
				if ($this->config->get('config_order_mail')) {
					$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);

					// HTML Mail
					$data['text_greeting'] = $language->get('text_new_received');
					
					if ($comment) {
						if ($order_info['comment']) {
							$data['comment'] = nl2br($comment) . '<br/><br/>' . $order_info['comment'];
						} else {
							$data['comment'] = nl2br($comment);
						}
					} else {
						if ($order_info['comment']) {
							$data['comment'] = $order_info['comment'];
						} else {
							$data['comment'] = '';
						}
					}
					$data['text_download'] = '';

					$data['text_footer'] = '';

					$data['text_link'] = '';
					$data['link'] = '';
					$data['download'] = '';

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$html = $this->load->view('default/template/mail/order.tpl', $data);
					}

					// Text
					$text  = $language->get('text_new_received') . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setReplyTo($order_info['email']);
					$mail->setSender($order_info['store_name']);
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
					$mail->send();

					// Send to additional alert emails
					$emails = explode(',', $this->config->get('config_mail_alert'));

					foreach ($emails as $email) {
						if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
							$mail->setTo($email);
							$mail->send();
						}
					}
				}
			}
			
			// If payment confirmation is rejected by admin
			if ($comment == 'reject' && $order_status_id == 1) {
				$language = new Language($order_info['language_directory']);
				$language->load('default');
				$language->load('mail/order');

				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

				$message .= $language->get('text_update_order_status') . " \n\n";
				$message .= $language->get('text_payment_rejected') . "\n\n";			
				$message .= $language->get('text_reconfirm') . "\n\n";			

				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order' . " \n";
				}

				if ($notify && $comment) {
					$message .= $language->get('text_update_comment') . " \n\n";
					$message .= strip_tags($comment) . "\n\n";
				}

				$message .= $language->get('text_update_footer');

				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject($subject);
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}

			// If order status is not 0 then send update text email
			if ($order_info['order_status_id'] && $order_status_id && $order_status_id != 1) {
				$language = new Language($order_info['language_directory']);
				$language->load('default');
				$language->load('mail/order');

				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$message = '<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; margin-bottom: 20px;">
<img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/25184501-a8ee-4ae5-8b8b-c915e6c66374.png" style="padding: 10px;">
</div>'; 
				$message .= $language->get('text_update_order') . ' ' . $order_id . "<br/>";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "<br/><br/>";

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . " <br/>";
					$message .= $order_status_query->row['name'] . "<br/><br/>";
				}

				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "<br/>";
					$message .= $order_info['store_url'] . 'index.php?route=account/order';
				}

				if ($notify && $comment) {
					$message .= $language->get('text_update_comment') . " <br/><br/>";
					$message .= strip_tags($comment) . "<br/>";
				}

				$message .= '<br><br>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; height: 15%; vertical-align: middle;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="25%">
			<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
</div>';
				// $message .= $language->get('text_update_footer');

				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}

			// If order status in the complete range create any vouchers that where in the order need to be made available.
			if (in_array($order_info['order_status_id'], $this->config->get('config_complete_status'))) {
				// Send out any gift voucher mails
				$this->load->model('checkout/voucher');

				$this->model_checkout_voucher->confirm($order_id);
			}
		}

		$this->event->trigger('post.order.history.add', $order_id);
	}
	
	public function deletePaymentConfirmation($order_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_payment_confirmation WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "order set sla_confirm_payment='".(int)$this->config->get('config_sla_confirm_payment')."' WHERE order_id = '" . (int)$order_id . "'");
	}
	
	public function generateOrderPaymentCode($order_id,$subtotal){
		$code=rand(10,100);
		$total=$subtotal+$code;
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_payment_code = '" . (float)$code . "', 
		total = '" . (float)$total . "',  
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET  
		value = '" . (float)$total . "'  
		WHERE order_id = '" . (int)$order_id . "' AND code='total'");
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order_total` SET  
		order_id = '" . (float)$order_id . "',
		value = '" . (float)$code . "',
		code = 'transfer_code',
		title = 'Transfer Code'		
		");
		
		$result=array(
		'total'=>$total,
		'code'=>$code
		);
		
		return $result;
	}

	public function addOrderHistoryDetail($order_id, $order_status_id, $comment = '', $notify = false, $user_id = 0, $identity='Admin') {
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_detail` WHERE order_id = '" . (int)$order_id . "'");
			
		foreach($order_query->rows as $order){
			if((int)$order['delivery_type_id']==3){
				$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET order_status_id = '5', date_modified = NOW() WHERE order_detail_id = '" . (int)$order['order_detail_id'] . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id = '" . (int)$order_id . "', order_detail_id = '" . (int)$order['order_detail_id'] . "', order_status_id = '5', notify = '" . (int)$notify . "',user_id = '" . (int)$user_id . "',identity = '" . $this->db->escape($identity) . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
			}else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history_detail SET order_id = '" . (int)$order_id . "', order_detail_id = '" . (int)$order['order_detail_id'] . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "',user_id = '" . (int)$user_id . "',identity = '" . $this->db->escape($identity) . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
			}
		}
	}
	
	public function getTransferCode($order_id){
		$result = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code='transfer_code'");
		if($result->num_rows){
			return $result->row['value'];
		}else{
			return 0;
		}
	}
	
	public function confirmEmployeeProgram($order_id, $order_status_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->addOrderHistory($order_id, $order_status_id);
		$this->addOrderHistoryDetail($order_id, $order_status_id, 'Auto Confirm Payment', false, $this->customer->getId(),'Customer');

	}
	
public function confirmFreeCheckout($order_id, $order_status_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->addOrderHistory($order_id, $order_status_id);
		$this->addOrderHistoryDetail($order_id, $order_status_id, 'Auto Confirm Payment', false, $this->customer->getId(),'Customer');

	}
	
	public function verifyFreeCheckout($order_id, $order_status_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->addOrderHistory($order_id, $order_status_id);
		$this->addOrderHistoryDetail($order_id, $order_status_id, 'Auto Verify Payment', false, $this->customer->getId(),'Customer');
		
		$this->notifySeller($order_id);

	}
	
	public function notifySeller($order_id){
		
			$order_info = $this->getOrder($order_id);

			if ($order_info) {
				
				$store_address = $this->config->get('config_address');
				$store_email = $this->config->get('config_email');
				$store_telephone = $this->config->get('config_telephone');
				$store_fax = $this->config->get('config_fax');
				
				$order_detail_infos = $this->getOrderDetailByOrderID($order_id);
				
				$order_detail=array();
				
				foreach ($order_detail_infos as $order_detail_info) {
						if((int)$order_detail_info['delivery_type_id']!=3){
						if ($order_detail_info['invoice_no']) {
							$invoice_no = $order_detail_info['invoice_prefix'] . $order_detail_info['invoice_no'];
						} else {
							$invoice_no = '';
						}
						
						$seller_info = $this->getSellerInfo($order_detail_info['seller_id']);
						
						if($seller_info){
							$seller_address=	$seller_info['nickname']."-".
												$seller_info['company'];
							$seller_email=$seller_info['email'];
							$seller_name=$seller_info['nickname'];
						}else{
							$seller_address='';
							$seller_email='';
							$seller_name='';
						}
						
						$products = $this->getOrderDetailProducts($order_detail_info['order_detail_id']);
						
						$product_data = array();
						
						foreach ($products as $product) {
							$option_data = array();

							$options = $this->getOrderOptions($order_id, $product['order_product_id']);

							foreach ($options as $option) {
								if ($option['type'] != 'file') {
									$value = $option['value'];
								} else {
									$upload_info = $this->getUploadByCode($option['value']);

									if ($upload_info) {
										$value = $upload_info['name'];
									} else {
										$value = '';
									}
								}

								$option_data[] = array(
									'name'  => $option['name'],
									'value' => $value
								);
							}

							$product_data[] = array(
								'name'     => $product['name'],
								'model'    => $product['model'],
								'option'   => $option_data,
								'quantity' => $product['quantity'],
								'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
								'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
							);
						}
						
						if((int)$order_detail_info['delivery_type_id']==1){
							$shipping_address=	$order_detail_info['shipping_name']."-".$order_detail_info['shipping_service_name']."</br>".
												$order_detail_info['shipping_firstname']." ".$order_detail_info['shipping_lastname']."</br>".
												$order_detail_info['shipping_company']."</br>".
												$order_detail_info['shipping_address_1']." ".$order_detail_info['shipping_address_2']."</br>".
												$order_detail_info['shipping_city'].",".$order_detail_info['shipping_zone'].",".$order_detail_info['shipping_postcode']."</br>".
												$order_detail_info['shipping_country'];
						}else if((int)$order_detail_info['delivery_type_id']==2){
							$shipping_address=	$order_detail_info['pp_branch_name']."</br>".
												$order_detail_info['pp_name']."</br>".
												$order_detail_info['pp_email']."</br>".
												$order_detail_info['pp_telephone'];
						}else if((int)$order_detail_info['delivery_type_id']==3){
							$shipping_address=	"-";
						}
						
						$voucher_data = array();
						
						$language = new Language($order_info['language_directory']);
						$language->load('default');
						$language->load('mail/order');
						
						$data_mail=array();
						$data_mail['text_verified_order']  = $language->get('text_verified_order');
						$data_mail['text_verified_date_added'] = $language->get('text_verified_date_added');

						$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_detail_info['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

						if ($order_status_query->num_rows) {
							$data_mail['text_verified_order_status'] = $language->get('text_verified_order_status');
							$data_mail['text_verified_order_status_name'] = $order_status_query->row['name'];
						}
						$data_mail['text_verified_link_url']='';
						if ($order_detail_info['seller_id']) {
							$data_mail['text_verified_link'] = $language->get('text_verified_link');
							$data_mail['text_verified_link_url'] .= $order_info['store_url'] . 'index.php?route=seller/account-pending-order';
						}

						$data_mail['text_verified_footer'] = $language->get('text_verified_footer');
						$data_mail['text_verified_message'] = $language->get('text_verified_message');
						
						$data_mail['text_seller'] = $language->get('text_seller');
						$data_mail['text_shipping_address'] = $language->get('text_shipping_address');
						$data_mail['text_delivery_type'] = $language->get('text_delivery_type');
						$data_mail['text_shipping_price'] = $language->get('text_shipping_price');
						$data_mail['text_total_invoice'] = $language->get('text_total_invoice');
						$data_mail['column_product'] = $language->get('column_product');
						$data_mail['column_model'] = $language->get('column_model');
						$data_mail['column_quantity'] = $language->get('column_quantity');
						$data_mail['column_price'] = $language->get('column_price');
						$data_mail['column_total'] = $language->get('column_total');
						
						if((int)$order_detail_info['order_status_id'] == 5){
							$data_mail['subject'] = sprintf($language->get('text_complete_subject'), $invoice_no);
						}else{
							$data_mail['subject'] = sprintf($language->get('text_verified_subject'), $invoice_no);
						}
						
						$order_detail_info = array(
							'invoice_no'=>$invoice_no,
							'shipping_address'=>$shipping_address,
							'seller_address'=>$seller_address,
							'seller_email'=>$seller_email,
							'seller_name'=>$seller_name,
							'delivery_type'=>$order_detail_info['delivery_type'],
							'products'=>$product_data,
							'vouchers'=>$voucher_data,
							'shipping_price'=>$this->currency->format($order_detail_info['shipping_price'], $order_detail_info['currency_code'], $order_detail_info['currency_value']),
							'total'=>$this->currency->format($order_detail_info['total'], $order_detail_info['currency_code'], $order_detail_info['currency_value']),
						);
						
						$data=array_merge($data_mail,$order_detail_info);
						
						if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order_verified.tpl')) {
							$html = $this->load->view($this->config->get('config_template') . '/template/mail/order_verified.tpl', $data);
						} else {
							$html = $this->load->view('default/template/mail/order_verified.tpl', $data);
						}

						$mail = new Mail($this->config->get('config_mail'));
						$mail->setTo($data['seller_email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($data['subject']);
						$mail->setHtml($html);
						$mail->send();
					}
						
				}
			}
	}
	
	public function getSellerInfo($seller_id){
		$order_query = $this->db->query("SELECT o.*,c.email FROM `" . DB_PREFIX . "ms_seller` o LEFT JOIN `" . DB_PREFIX . "customer` c ON c.customer_id=o.seller_id WHERE o.seller_id = '" . (int)$seller_id . "'");

		if ($order_query->num_rows) {
			return $order_query->row;
		} else {
			return;
		}
	}
	
	public function getOrderDetailProducts($order_detail_id) {
		$query = $this->db->query("SELECT oop.*, op.sku, op.weight, op.weight_class_id FROM " . DB_PREFIX . "order_product oop  LEFT JOIN ". DB_PREFIX . "product op ON op.product_id=oop.product_id WHERE oop.order_detail_id = '" . (int)$order_detail_id . "'");

		return $query->rows;
	}
	
	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}
	
	public function getOrderTotal($order_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code='total' ORDER BY sort_order ASC");

		if ($query->num_rows) {
			return (int)$query->row['value'];
		} else {
			return 0;
		}
		
	}
	
	public function getUploadByCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "upload` WHERE code = '" . $this->db->escape($code) . "'");

		return $query->row;
	}
	
	public function confirmSGOPayment($order_id, $order_status_id, $customer_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->addOrderHistory($order_id, $order_status_id);
		$this->addOrderHistoryDetail($order_id, $order_status_id, 'Auto Confirm Payment', false, $customer_id,'Customer');

	}
	
	public function verifySGOPayment($order_id, $order_status_id, $customer_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order_detail` SET 
		order_status_id = '" . (int)$order_status_id . "', 
		date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
		$this->addOrderHistory($order_id, $order_status_id);
		$this->addOrderHistoryDetail($order_id, $order_status_id, 'Auto Verify Payment', false, $customer_id,'Customer');
		
		$this->notifySeller($order_id);

	}
	
	public function addOnlinePaymentHistory($data){
		$this->db->query("INSERT INTO `" . DB_PREFIX . "sgopayment_history` SET 
			code = '" . $data['code'] . "', 
			value = '" . $data['value'] . "', 
			date_added = NOW()");
	}
}