<?php
class ModelTotalCoupon extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		if (isset($this->session->data['coupon'])) {
			$this->load->language('total/coupon');

			$this->load->model('checkout/coupon');

			$coupon_info = $this->model_checkout_coupon->getCoupon($this->session->data['coupon']);

			if ($coupon_info['result']) {
				$discount_total = 0;

				if (!$coupon_info['product']) {
					$sub_total = $this->cart->getSubTotal();
				} else {
					$sub_total = 0;

					foreach ($this->cart->getProducts() as $product) {
						if (in_array($product['product_id'], $coupon_info['product'])) {
							$sub_total += $product['total'];
						}
					}
				}
				
				if ($coupon_info['shipping']) {
					$total_shipping=0;
					if ($this->cart->hasShipping()) {
						$shippings=$this->cart->getShipping();
						$temp_shipping_address="";
						$temp_shipping_service_id="";
						$temp_seller_id="";
						foreach($shippings as $shipping){
							if(isset($shipping['delivery'])){
								//GROUPING ADDRESS, SELLER, DESTINATION
								if($shipping['delivery']['shipping_address']!=$temp_shipping_address || $shipping['delivery']['seller_id']!=$temp_seller_id || $shipping['delivery']['shipping_service_id']!=$temp_shipping_service_id){
									$temp_shipping_address=$shipping['delivery']['shipping_address'];
									$temp_shipping_service_id=$shipping['delivery']['shipping_service_id'];
									$temp_seller_id=$shipping['delivery']['seller_id'];
									
									$total_shipping+=$shipping['delivery']['price'];
								}
							}
						}
					}
					$sub_total = $total_shipping;
				}

				if ($coupon_info['type'] == 'F') {
					$coupon_info['discount'] = min($coupon_info['discount'], $sub_total);
				}
				
				if ($coupon_info['shipping']) {
					$discount_total = $coupon_info['discount'];
				}else{
					foreach ($this->cart->getProducts() as $product) {
						$discount = 0;

						if (!$coupon_info['product']) {
							$status = true;
						} else {
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$status = true;
							} else {
								$status = false;
							}
						}

						if ($status) {
							if ($coupon_info['type'] == 'F') {
								$discount = $coupon_info['discount'] * ($product['total'] / $sub_total);
							} elseif ($coupon_info['type'] == 'P') {
								$discount = $product['total'] / 100 * $coupon_info['discount'];
							}

							if ($product['tax_class_id']) {
								$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

								foreach ($tax_rates as $tax_rate) {
									if ($tax_rate['type'] == 'P') {
										$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
								}
							}
						}

						$discount_total += $discount;
					}
				}

				// If discount greater than total
				if ($discount_total > $total) {
					$discount_total = $total;
				}


		

                if (strtoupper(($this->session->data['coupon'])) == "DUGDUG41") {
                    if ($discount_total > 50000) {
                        $discount_total = 50000;
                        $total -= $discount_total;
                    } else {
                        $total -= $discount_total;
                    }
                } else {
                    $total -= $discount_total;
                    $total;
                }



				$total_data[] = array(
					'code'       => 'coupon',
					'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
					'value'      => -$discount_total,
					'sort_order' => $this->config->get('coupon_sort_order')
				);


               


//				$total -= $discount_total;
	

		}
		}
	}

	public function confirm($order_info, $order_total) {
		$code = '';

		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');

		if ($start && $end) {
			$code = substr($order_total['title'], $start, $end - $start);
		}

		$this->load->model('checkout/coupon');

		$coupon_info = $this->model_checkout_coupon->getCoupon($code);

		if ($coupon_info['result']) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_history` SET coupon_id = '" . (int)$coupon_info['coupon_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', customer_id = '" . (int)$order_info['customer_id'] . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "coupon_history` WHERE order_id = '" . (int)$order_id . "'");
	}
}
