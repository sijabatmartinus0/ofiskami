<?php
class ModelTotalShipping extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$total_shipping=0;
		if ($this->cart->hasShipping()) {
			$shippings=$this->cart->getShipping();
			$temp_shipping_address="";
			$temp_shipping_service_id="";
			$temp_seller_id="";
			foreach($shippings as $shipping){
				if(isset($shipping['delivery'])){
					//GROUPING ADDRESS, SELLER, DESTINATION
					if($shipping['delivery']['shipping_address']!=$temp_shipping_address || $shipping['delivery']['seller_id']!=$temp_seller_id || $shipping['delivery']['shipping_service_id']!=$temp_shipping_service_id){
						$temp_shipping_address=$shipping['delivery']['shipping_address'];
						$temp_shipping_service_id=$shipping['delivery']['shipping_service_id'];
						$temp_seller_id=$shipping['delivery']['seller_id'];
						
						/*if ($this->session->data['shipping_method']['tax_class_id']) {
							$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if (!isset($taxes[$tax_rate['tax_rate_id']])) {
									$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
								} else {
									$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
								}
							}
						}*/
						$total_shipping+=$shipping['delivery']['price'];
						$total += $shipping['delivery']['price'];
					}
				}
			}
			$total_data[] = array(
				'code'       => 'shipping',
				'title'      => 'Shipping',
				'value'      => $total_shipping,
				'sort_order' => $this->config->get('shipping_sort_order')
			);
		}
	}
}