<?php

class ControllerCheckoutCart extends Controller {

    public function index() {

        $this->load->language('checkout/cart');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'href' => $this->url->link('common/home'),
            'text' => $this->language->get('text_home')
        );

        $data['breadcrumbs'][] = array(
            'href' => $this->url->link('checkout/cart'),
            'text' => $this->language->get('heading_title')
        );

        if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_recurring_item'] = $this->language->get('text_recurring_item');
            $data['text_next'] = $this->language->get('text_next');
            $data['text_next_choice'] = $this->language->get('text_next_choice');

            $data['text_md_edit_cart'] = $this->language->get('text_md_edit_cart');
            $data['text_md_edit_address'] = $this->language->get('text_md_edit_address');
            $data['column_image'] = $this->language->get('column_image');
            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');
            //edit by arin
            $data['column_seller'] = $this->language->get('column_seller');
            $data['column_product_price'] = $this->language->get('column_product_price');
            $data['column_remark'] = $this->language->get('column_remark');
            $data['column_address'] = $this->language->get('column_address');
            $data['column_insurance'] = $this->language->get('column_insurance');
            $data['column_subtotal'] = $this->language->get('column_subtotal');
            $data['column_total_unit'] = $this->language->get('column_total_unit');
            $data['column_insurance_charge'] = $this->language->get('column_insurance_charge');
            $data['column_shipping'] = $this->language->get('column_shipping');
            $data['text_delete_all'] = $this->language->get('text_delete_all');
            $data['text_per_invoice'] = $this->language->get('text_per_invoice');
            $data['text_insurance_yes'] = $this->language->get('text_insurance_yes');
            $data['text_insurance_no'] = $this->language->get('text_insurance_no');
            $data['button_update'] = $this->language->get('button_update');
            $data['button_remove'] = $this->language->get('button_remove');
            $data['button_shopping'] = $this->language->get('button_shopping');
            $data['button_checkout'] = $this->language->get('button_checkout');
            $data['button_save'] = $this->language->get('button_save');
            $data['button_cancel'] = $this->language->get('button_cancel');

            $data['button_ok'] = $this->language->get('button_ok');

            $data['text_md_remove'] = $this->language->get('text_md_remove');
            $data['text_remove_product_confirmation'] = $this->language->get('text_remove_product_confirmation');
            $data['text_md_remove_all'] = $this->language->get('text_md_remove_all');
            $data['text_remove_all_product_confirmation'] = $this->language->get('text_remove_all_product_confirmation');
            if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $data['error_warning'] = $this->language->get('error_stock');
            } elseif (isset($this->session->data['error'])) {
                $data['error_warning'] = $this->session->data['error'];

                unset($this->session->data['error']);
            } else {
                $data['error_warning'] = '';
            }

            if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
                $data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
            } else {
                $data['attention'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            $data['action'] = $this->url->link('checkout/cart/edit');

            if ($this->config->get('config_cart_weight')) {
                $data['weight'] = $this->weight->format($this->weight->convert($this->cart->getWeight(), 2, 1), 1, $this->language->get('decimal_point'), $this->language->get('thousand_point'));
            } else {
                $data['weight'] = '';
            }

            $this->load->model('tool/image');
            $this->load->model('tool/upload');
            $this->load->model('account/seller');
            $this->load->model('account/address');
            $this->load->model('shipping/shipping');
            $this->load->model('localisation/pp_branch');
            $data['products'] = array();

            $products = $this->cart->getProducts();
            //print_r($products);
            $delivery_shipping_address = "";
            $delivery_seller_id = "";
            $delivery_shipping_service_id = "";
            $pickup_seller_id = "";
            $pickup_pp_branch = "";
            $key = "";
            $tempKey = "";
            foreach ($products as $product) {
                if (isset($product['shipping'])) {
                    if ($product['shipping']['delivery_type'] == 1) {
                        if ($product['shipping']['delivery']['shipping_address'] != $delivery_shipping_address || $product['shipping']['delivery']['seller_id'] != $delivery_seller_id || $product['shipping']['delivery']['shipping_service_id'] != $delivery_shipping_service_id) {
                            $delivery_shipping_address = $product['shipping']['delivery']['shipping_address'];
                            $delivery_seller_id = $product['shipping']['delivery']['seller_id'];
                            $delivery_shipping_service_id = $product['shipping']['delivery']['shipping_service_id'];


                            $tempKey = "delivery" . $delivery_shipping_address . $delivery_seller_id . $delivery_shipping_service_id;
                        }
                        //GET ADDRESS
                        $product['shipping']['delivery']['shipping_address'] = $this->model_account_address->getCheckoutAddress(array('customer_id' => $this->customer->getId(), 'address_id' => $product['shipping']['delivery']['shipping_address']));
                        $product['shipping']['delivery']['price_count'] = $product['shipping']['delivery']['price'];
                        $product['shipping']['delivery']['price'] = $this->currency->format($product['shipping']['delivery']['price']);
                        //GET SELLER
                        $product['shipping']['delivery']['seller'] = $this->model_account_seller->getSeller($product['shipping']['delivery']['seller_id']);
                        $product['shipping']['delivery']['seller']['link'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $product['shipping']['delivery']['seller_id']);
                        //GET SHIPPING	
                        $product['shipping']['delivery']['shipping'] = $this->model_shipping_shipping->getShipping($product['shipping']['delivery']['shipping_id']);
                        $product['shipping']['delivery']['shipping_service'] = $this->model_shipping_shipping->getShippingService($product['shipping']['delivery']['shipping_service_id']);
                    } else {

                        if ($product['shipping']['pickup']['seller_id'] != $pickup_seller_id || $product['shipping']['pickup']['branch'] != $pickup_pp_branch) {
                            $pickup_seller_id = $product['shipping']['pickup']['seller_id'];
                            $pickup_pp_branch = $product['shipping']['pickup']['branch'];
                            $tempKey = "pickup" . $pickup_seller_id . $pickup_pp_branch;
                        }

                        //GET SELLER
                        $product['shipping']['pickup']['seller'] = $this->model_account_seller->getSeller($product['shipping']['pickup']['seller_id']);
                        $product['shipping']['pickup']['seller']['link'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $product['shipping']['pickup']['seller_id']);
                        $product['shipping']['pickup']['branch'] = $this->model_localisation_pp_branch->getPpBranchDetail($product['shipping']['pickup']['branch']);
                    }
                }

                if ($tempKey != $key) {
                    $key = $tempKey;
                }
                $data['cart'][$key]['shipping'] = $product['shipping'];

                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                }
                //var_dump($product['minimum']);die();
                //this one
                if ($product['maximum'] < $product_total) {
                    $data['error_warning'] = sprintf($this->language->get('error_maximum'), $product['name'], $product['maximum']);
                }
                //var_dump ($product['maximum']); die(); 

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                } else {
                    $image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                }

                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
                } else {
                    $total = false;
                }

                $recurring = '';

                if ($product['recurring']) {
                    $frequencies = array(
                        'day' => $this->language->get('text_day'),
                        'week' => $this->language->get('text_week'),
                        'semi_month' => $this->language->get('text_semi_month'),
                        'month' => $this->language->get('text_month'),
                        'year' => $this->language->get('text_year'),
                    );

                    if ($product['recurring']['trial']) {
                        $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                    }

                    if ($product['recurring']['duration']) {
                        $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    } else {
                        $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    }
                }

                $data['cart'][$key]['products'][] = array(
                    'key' => $product['key'],
                    'thumb' => $image,
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'weight_count' => $product['weight'],
                    'weight' => $this->weight->format($product['weight'], 1),
                    'option' => $option_data,
                    'information' => $product['shipping']['information'],
                    'recurring' => $recurring,
                    'quantity' => $product['quantity'],
                    'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                    'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                    'price' => $price,
                    'total_count' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'],
                    'total' => $total,
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                );
            }

            // Gift Voucher
            $data['text_voucher'] = $this->language->get('text_voucher');
            $data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $key => $voucher) {
                    $data['vouchers'][] = array(
                        'key' => $key,
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount']),
                        'remove' => $this->url->link('checkout/cart', 'remove=' . $key)
                    );
                }
            }

            // Totals
            $this->load->model('extension/extension');

            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $total_data);
            }

            $data['totals'] = array();

            foreach ($total_data as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'])
                );
            }

            $data['continue'] = $this->url->link('common/home');

            $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

            $this->load->model('extension/extension');

            $data['checkout_buttons'] = array();

            //$data['coupon'] = $this->load->controller('checkout/coupon');
            //$data['voucher'] = $this->load->controller('checkout/voucher');
            //$data['reward'] = $this->load->controller('checkout/reward');
            //$data['shipping'] = $this->load->controller('checkout/shipping');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/checkout/cart.tpl', $data));
            }
        } else {
            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_error'] = $this->language->get('text_empty');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            unset($this->session->data['success']);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }
    }

    public function add() {
        $this->load->language('checkout/cart');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = (int) $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if (isset($this->request->post['quantity'])) {
                $quantity = (int) $this->request->post['quantity'];
            } else {
                $quantity = 1;
            }

            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            } else {
                $option = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            if (isset($this->request->post['recurring_id'])) {
                $recurring_id = $this->request->post['recurring_id'];
            } else {
                $recurring_id = 0;
            }

            $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($recurrings) {
                $recurring_ids = array();

                foreach ($recurrings as $recurring) {
                    $recurring_ids[] = $recurring['recurring_id'];
                }

                if (!in_array($recurring_id, $recurring_ids)) {
                    $json['error']['recurring'] = $this->language->get('error_recurring_required');
                }
            }

            if (!$json && !isset($this->request->post['catalog'])) {
                $this->cart->add($this->request->post['product_id'], $this->request->post['quantity'], $option, $recurring_id);


                $this->load->model('tool/image');
                $json['image'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                // Totals
                $this->load->model('extension/extension');

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $sort_order = array();

                    $results = $this->model_extension_extension->getExtensions('total');

                    foreach ($results as $key => $value) {
                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                    }

                    array_multisort($sort_order, SORT_ASC, $results);

                    foreach ($results as $result) {
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                        }
                    }

                    $sort_order = array();

                    foreach ($total_data as $key => $value) {
                        $sort_order[$key] = $value['sort_order'];
                    }

                    array_multisort($sort_order, SORT_ASC, $total_data);
                }

                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
            } else {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function edit() {
        $this->load->language('checkout/cart');

        $json = array();

        // Update
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeVoucher() {
        $this->load->language('checkout/cart');

        $json = array();

        // Remove
        if (isset($this->request->post['key'])) {
            if ($this->cart->removeVoucher($this->request->post['key'])) {

                unset($this->session->data['vouchers'][$this->request->post['key']]);

                $this->session->data['success'] = $this->language->get('text_remove');

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['reward']);

                // Totals
                $this->load->model('extension/extension');

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $sort_order = array();

                    $results = $this->model_extension_extension->getExtensions('total');

                    foreach ($results as $key => $value) {
                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                    }

                    array_multisort($sort_order, SORT_ASC, $results);

                    foreach ($results as $result) {
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                        }
                    }

                    $sort_order = array();

                    foreach ($total_data as $key => $value) {
                        $sort_order[$key] = $value['sort_order'];
                    }

                    array_multisort($sort_order, SORT_ASC, $total_data);
                }

                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function remove() {
        $this->load->language('checkout/cart');

        $json = array();

        // Remove
        if (isset($this->request->post['key'])) {
            if ($this->cart->remove($this->request->post['key'])) {

                unset($this->session->data['vouchers'][$this->request->post['key']]);

                $this->session->data['success'] = $this->language->get('text_remove');

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['reward']);

                // Totals
                $this->load->model('extension/extension');

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $sort_order = array();

                    $results = $this->model_extension_extension->getExtensions('total');

                    foreach ($results as $key => $value) {
                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                    }

                    array_multisort($sort_order, SORT_ASC, $results);

                    foreach ($results as $result) {
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                        }
                    }

                    $sort_order = array();

                    foreach ($total_data as $key => $value) {
                        $sort_order[$key] = $value['sort_order'];
                    }

                    array_multisort($sort_order, SORT_ASC, $total_data);
                }

                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function loadCart() {
        $data = array();
        $this->load->language('checkout/cart');
        if ($this->customer->isLogged()) {
            if (!$this->pickup->isPickupPoint() && !$this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
                if ((int) $this->customer->getVerified() != 0) {

                    if (!$this->MsLoader->MsSeller->isActiveTemp($this->request->get['product_id'])) {
                        $data['text_md_cart_search_pp_country'] = $this->language->get('text_md_cart_search_pp_country');
                        $data['text_md_cart_search_pp_zone'] = $this->language->get('text_md_cart_search_pp_zone');
                        $data['text_md_cart_search_pp_city'] = $this->language->get('text_md_cart_search_pp_city');
                        $data['text_md_cart_search_pp_branch'] = $this->language->get('text_md_cart_search_pp_branch');
                        $data['text_md_cart_info_pp_name'] = $this->language->get('text_md_cart_info_pp_name');
                        $data['text_md_cart_info_pp_address'] = $this->language->get('text_md_cart_info_pp_address');
                        $data['text_md_cart_info_pp_company'] = $this->language->get('text_md_cart_info_pp_company');
                        $data['text_md_cart_info_pp_telephone'] = $this->language->get('text_md_cart_info_pp_telephone');

                        $data['text_md_cart_info_shipping_name'] = $this->language->get('text_md_cart_info_shipping_name');
                        $data['text_md_cart_info_shipping_service_name'] = $this->language->get('text_md_cart_info_shipping_service_name');
                        $data['text_md_cart_info_shipping_service_insurance'] = $this->language->get('text_md_cart_info_shipping_service_insurance');
                        $data['text_md_cart_info_shipping_price'] = $this->language->get('text_md_cart_info_shipping_price');
                        $data['text_md_cart_info_total'] = $this->language->get('text_md_cart_info_total');

                        $data['text_md_cart_pickup_person_status'] = $this->language->get('text_md_cart_pickup_person_status');
                        $data['text_md_cart_pickup_person_name'] = $this->language->get('text_md_cart_pickup_person_name');
                        $data['text_md_cart_pickup_person_email'] = $this->language->get('text_md_cart_pickup_person_email');
                        $data['text_md_cart_pickup_person_phone'] = $this->language->get('text_md_cart_pickup_person_phone');

                        $data['text_select'] = $this->language->get('text_select');
                        $data['text_none'] = $this->language->get('text_none');
                        $data['text_loading'] = $this->language->get('text_loading');
                        $data['text_reload'] = $this->language->get('text_reload');

                        $this->load->model('localisation/country');

                        $data['countries'] = $this->model_localisation_country->getCountries();

                        $data['country_id'] = $this->config->get('config_country_id');
                        $data['zone_id'] = 0;
                        $data['city_id'] = 0;
                        $data['district_id'] = 0;
                        $data['subdistrict_id'] = 0;
                        $data['pp_branch_id'] = 0;

                        $data['text_md_cart_product_name'] = $this->language->get('text_md_cart_product_name');
                        $data['text_md_cart_product_amount'] = $this->language->get('text_md_cart_product_amount');
                        $data['text_md_cart_product_price'] = $this->language->get('text_md_cart_product_price');
                        $data['text_md_cart_product_information'] = $this->language->get('text_md_cart_product_information');
                        $data['button_md_cart_pick_up_point'] = $this->language->get('button_md_cart_pick_up_point');
                        $data['button_md_cart_logistic'] = $this->language->get('button_md_cart_logistic');
                        $data['button_md_cart_add_address'] = $this->language->get('button_md_cart_add_address');
                        $data['button_md_cart_cancel_address'] = $this->language->get('button_md_cart_cancel_address');

                        $data['entry_firstname'] = $this->language->get('entry_firstname');
                        $data['entry_lastname'] = $this->language->get('entry_lastname');
                        $data['entry_alias'] = $this->language->get('entry_alias');
                        $data['entry_company'] = $this->language->get('entry_company');
                        $data['entry_address_1'] = $this->language->get('entry_address_1');
                        $data['entry_address_2'] = $this->language->get('entry_address_2');
                        $data['entry_postcode'] = $this->language->get('entry_postcode');
                        $data['entry_subdistrict'] = $this->language->get('entry_subdistrict');
                        $data['entry_district'] = $this->language->get('entry_district');
                        $data['entry_city'] = $this->language->get('entry_city');
                        $data['entry_country'] = $this->language->get('entry_country');
                        $data['entry_zone'] = $this->language->get('entry_zone');

                        $data['error_warning'] = $this->language->get('error_warning');


                        $this->load->model('account/address');

                        $data['addresses'] = $this->model_account_address->getCheckoutAddress(
                                array(
                                    "customer_id" => $this->customer->getId()
                                )
                        );

                        $this->load->model('catalog/product');
                        if (isset($this->request->get['product_id'])) {
                            $data['product_id'] = $this->request->get['product_id'];
                            $product_id = $this->request->get['product_id'];
                            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

                            if ($product_info) {
                                $length = 25;
                                if (strlen($product_info['name']) > $length) {
                                    $data['product_info']['product_name'] = substr($product_info['name'], 0, $length) . "...";
                                } else {
                                    $data['product_info']['product_name'] = $product_info['name'];
                                }
                                if (isset($this->request->get['amount'])) {
                                    $data['product_info']['product_amount'] = (int) $this->request->get['amount'];
                                } else {
                                    $data['product_info']['product_amount'] = 1;
                                    $this->request->get['amount'] = 1;
                                }
                                $data['product_info']['product_information'] = '';
                                $data['product_info']['link'] = $this->url->link('product/product', '&product_id=' . $this->request->get['product_id'], 'SSL');

                                if (isset($this->request->post['option'])) {
                                    $option = array_filter($this->request->post['option']);
                                } else {
                                    $option = array();
                                }

                                $option_price = 0;
                                $option_points = 0;
                                $option_weight = 0;

                                if (empty($option)) {
                                    $data['product_info']['option'] = '';
                                } else {
                                    $data['product_info']['option'] = 'Option:<br>';
                                }

                                foreach ($option as $product_option_id => $value) {
                                    $data['product_info']['option'] .= $this->model_catalog_product->getProductOptionByID($product_option_id, $value) . '<br>';

                                    $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $product_id . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                    if ($option_query->num_rows) {
                                        if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
                                            $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                            if ($option_value_query->num_rows) {
                                                if ($option_value_query->row['price_prefix'] == '+') {
                                                    $option_price += $option_value_query->row['price'];
                                                } elseif ($option_value_query->row['price_prefix'] == '-') {
                                                    $option_price -= $option_value_query->row['price'];
                                                }
                                            }
                                        } elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
                                            foreach ($value as $product_option_value_id) {
                                                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                                if ($option_value_query->num_rows) {
                                                    if ($option_value_query->row['price_prefix'] == '+') {
                                                        $option_price += $option_value_query->row['price'];
                                                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                                                        $option_price -= $option_value_query->row['price'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                    $price = $product_info['price'];

                                    $product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int) $this->request->get['amount'] . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

                                    if ($product_discount_query->num_rows) {
                                        $price = $product_discount_query->row['price'];
                                    }

                                    // Product Specials
                                    $product_special_query = $this->db->query("SELECT coalesce(sum(price)) as price,sum(discount) as discount FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC");
                                   
                                    if ($product_special_query->num_rows) {
                                        if($product_special_query->row['discount'] > 0 && $product_special_query->row['price'] > 0){
                                                $price = $product_special_query->row['price']-$product_special_query->row['discount'];
                                        }
                                        else if ($product_special_query->row['discount'] > 0){                    
                                                $price = $product_info['price']-$product_special_query->row['discount'];
                                        }
                                        else if($product_special_query->row['price'] > 0){                    
                                                $price = $product_special_query->row['price'];
                                        }
                                        else {
                                                $price = $product_info['price'];   
                                        }
                                        
                                    }
                                    (int)$price = $price + $option_price;
                                    /*var_dump($product_info['price']);
                                    var_dump($product_special_query->row['price']);
                                    var_dump($product_special_query->row['discount']);
                                    var_dump($price);die();*/
                                    $data['product_info']['product_price'] = $this->currency->format($this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax')));
                                    $data['product_info']['price'] = $this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax'));
                                } else {
                                    $data['product_info']['product_price'] = false;
                                }
                                // echo '<pre>';
                                // var_dump($data);
                                // die();
                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form.tpl')) {
                                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form.tpl', $data));
                                } else {
                                    $this->response->setOutput($this->load->view('default/template/checkout/cart_form.tpl', $data));
                                }
                            } else {
                                $data['text_md_cart_notfound_message'] = $this->language->get('text_md_cart_notfound_message');

                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form_notfound.tpl')) {
                                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form_notfound.tpl', $data));
                                } else {
                                    $this->response->setOutput($this->load->view('default/template/checkout/cart_form_notfound.tpl', $data));
                                }
                            }
                        } else {
                            $data['text_md_cart_notfound_message'] = $this->language->get('text_md_cart_notfound_message');

                            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form_notfound.tpl')) {
                                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form_notfound.tpl', $data));
                            } else {
                                $this->response->setOutput($this->load->view('default/template/checkout/cart_form_notfound.tpl', $data));
                            }
                        }
                    } else {
			
                        //$a = $this->MsLoader->MsSeller->dateCloseStore($this->request->get['product_id']);
                        //$data['text_md_cart_activation_message'] = "Mohon Maaf Toko Sedang Tutup Sementara :)) Mulai ".$a[0]['start']." Sampai Tanggal ".$a[0]['end'];
			$data['text_md_cart_activation_message'] = "Mohon maaf untuk sementara merchant ini tidak dapat melayani pesanan anda. Happy Ied Mubarak 1438 H.";
                        $data['button_activation'] = "Keluar";
			
                        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form_close_store.tpl')) {
                            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form_close_store.tpl', $data));
                        } else {
                            $this->response->setOutput($this->load->view('default/template/checkout/cart_form_activation.tpl', $data));
                        }
                    }
                } else {
                    $data['text_md_cart_activation_message'] = $this->language->get('text_md_cart_activation_message');
                    $data['button_activation'] = $this->language->get('button_activation');
                    $data['link'] = $this->url->link('account/activate', '', 'SSL');

                    $this->session->data['redirect'] = $this->url->link('product/product', '&product_id=' . $this->request->get['product_id'], 'SSL');

                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form_activation.tpl')) {
                        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form_activation.tpl', $data));
                    } else {
                        $this->response->setOutput($this->load->view('default/template/checkout/cart_form_activation.tpl', $data));
                    }
                }
            } else {
                $this->response->setOutput($this->language->get('text_no_access'));
            }
        } else {
            $data['text_md_cart_nologin_message'] = $this->language->get('text_md_cart_nologin_message');
            $data['button_login'] = $this->language->get('button_login');
            $data['link'] = $this->url->link('account/login', '', 'SSL');

            $this->session->data['redirect'] = $this->url->link('product/product', '&product_id=' . $this->request->get['product_id'], 'SSL');
            $this->session->data['url'] = $this->url->link('product/product', '&product_id=' . $this->request->get['product_id'], 'SSL');
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_form_nologin.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart_form_nologin.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/checkout/cart_form_nologin.tpl', $data));
            }
        }
    }

    public function loadEditAddress() {
        $data = array();
        $this->load->language('checkout/cart');
        $empty = false;
        if (isset($this->request->post['key'])) {
            $count_data = 0;
            foreach ($this->request->post['key'] as $key) {
                if (!isset($this->session->data['cart'][$key]) && empty($this->session->data['cart'][$key])) {
                    $count_data++;
                }
            }

            if (sizeof($this->request->post['key']) == $count_data) {
                $empty = true;
            }
            $data['text_md_cart_search_pp_country'] = $this->language->get('text_md_cart_search_pp_country');
            $data['text_md_cart_search_pp_zone'] = $this->language->get('text_md_cart_search_pp_zone');
            $data['text_md_cart_search_pp_city'] = $this->language->get('text_md_cart_search_pp_city');
            $data['text_md_cart_search_pp_branch'] = $this->language->get('text_md_cart_search_pp_branch');
            $data['text_md_cart_info_pp_name'] = $this->language->get('text_md_cart_info_pp_name');
            $data['text_md_cart_info_pp_address'] = $this->language->get('text_md_cart_info_pp_address');
            $data['text_md_cart_info_pp_company'] = $this->language->get('text_md_cart_info_pp_company');
            $data['text_md_cart_info_pp_telephone'] = $this->language->get('text_md_cart_info_pp_telephone');

            $data['text_md_cart_info_shipping_name'] = $this->language->get('text_md_cart_info_shipping_name');
            $data['text_md_cart_info_shipping_service_name'] = $this->language->get('text_md_cart_info_shipping_service_name');
            $data['text_md_cart_info_shipping_service_insurance'] = $this->language->get('text_md_cart_info_shipping_service_insurance');
            $data['text_md_cart_info_shipping_price'] = $this->language->get('text_md_cart_info_shipping_price');
            $data['text_md_cart_info_total'] = $this->language->get('text_md_cart_info_total');

            $data['text_md_cart_pickup_person_status'] = $this->language->get('text_md_cart_pickup_person_status');
            $data['text_md_cart_pickup_person_name'] = $this->language->get('text_md_cart_pickup_person_name');
            $data['text_md_cart_pickup_person_email'] = $this->language->get('text_md_cart_pickup_person_email');
            $data['text_md_cart_pickup_person_phone'] = $this->language->get('text_md_cart_pickup_person_phone');

            $data['text_select'] = $this->language->get('text_select');
            $data['text_none'] = $this->language->get('text_none');
            $data['text_loading'] = $this->language->get('text_loading');

            $this->load->model('localisation/country');

            $data['key'] = json_encode($this->request->post['key']);
            $data['arrayKey'] = $this->request->post['key'];
            $data['countries'] = $this->model_localisation_country->getCountries();

            $data['country_id'] = $this->config->get('config_country_id');
            $data['zone_id'] = 0;
            $data['city_id'] = 0;
            $data['district_id'] = 0;
            $data['subdistrict_id'] = 0;
            $data['pp_branch_id'] = 0;

            $data['text_md_cart_product_name'] = $this->language->get('text_md_cart_product_name');
            $data['text_md_cart_product_amount'] = $this->language->get('text_md_cart_product_amount');
            $data['text_md_cart_product_price'] = $this->language->get('text_md_cart_product_price');
            $data['text_md_cart_product_information'] = $this->language->get('text_md_cart_product_information');
            $data['button_md_cart_pick_up_point'] = $this->language->get('button_md_cart_pick_up_point');
            $data['button_md_cart_logistic'] = $this->language->get('button_md_cart_logistic');
            $data['button_md_cart_add_address'] = $this->language->get('button_md_cart_add_address');
            $data['button_md_cart_cancel_address'] = $this->language->get('button_md_cart_cancel_address');

            $data['entry_firstname'] = $this->language->get('entry_firstname');
            $data['entry_lastname'] = $this->language->get('entry_lastname');
            $data['entry_alias'] = $this->language->get('entry_alias');
            $data['entry_company'] = $this->language->get('entry_company');
            $data['entry_address_1'] = $this->language->get('entry_address_1');
            $data['entry_address_2'] = $this->language->get('entry_address_2');
            $data['entry_postcode'] = $this->language->get('entry_postcode');
            $data['entry_subdistrict'] = $this->language->get('entry_subdistrict');
            $data['entry_district'] = $this->language->get('entry_district');
            $data['entry_city'] = $this->language->get('entry_city');
            $data['entry_country'] = $this->language->get('entry_country');
            $data['entry_zone'] = $this->language->get('entry_zone');

            $data['error_warning'] = $this->language->get('error_warning');


            $this->load->model('account/address');

            $data['addresses'] = $this->model_account_address->getCheckoutAddress(
                    array(
                        "customer_id" => $this->customer->getId()
                    )
            );

            if (!$empty) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/edit_address_form.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/edit_address_form.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/checkout/edit_address_form.tpl', $data));
                }
            } else {
                $this->session->data['error'] = $this->language->get('text_md_cart_empty_product');
                $this->response->setOutput($this->language->get('text_md_cart_empty_product') . "<script>$('.modal-footer>button').hide();$('#content .cart-content').html('<div class=\"text-center quickcheckout-block\"><div class=\"quickcheckout-loading\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div></div>');window.location=\"" . $this->url->link('checkout/cart') . "\";</script>");
            }
        } else {
            $this->session->data['error'] = $this->language->get('text_md_cart_empty_product');
            $this->response->setOutput($this->language->get('text_md_cart_empty_product') . "<script>$('.modal-footer>button').hide();$('#content .cart-content').html('<div class=\"text-center quickcheckout-block\"><div class=\"quickcheckout-loading\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div></div>');window.location=\"" . $this->url->link('checkout/cart') . "\";</script>");
        }
    }

    public function loadEditCart() {
        $data = array();
        $this->load->language('checkout/cart');
        if (isset($this->request->post['key'])) {
            $dataKey = $this->request->post['key'];
            $products = $this->cart->getProducts();
            $product = array();
            foreach ($products as $item) {
                if ($dataKey == $item['key']) {
                    $product = $item;
                    break;
                }
            }
            if (!empty($product)) {
                $data['text_md_cart_product_information'] = $this->language->get('text_md_cart_product_information');
                $data['text_md_cart_product_name'] = $this->language->get('text_md_cart_product_name');
                $data['text_md_cart_product_amount'] = $this->language->get('text_md_cart_product_amount');

                $data['product'] = $product;

                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/edit_cart_form.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/edit_cart_form.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/checkout/edit_cart_form.tpl', $data));
                }
            } else {
                $this->session->data['error'] = $this->language->get('text_md_cart_empty_product');
                $this->response->setOutput($this->language->get('text_md_cart_empty_product') . "<script>$('.modal-footer>button').hide();$('#content .cart-content').html('<div class=\"text-center quickcheckout-block\"><div class=\"quickcheckout-loading\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div></div>');window.location=\"" . $this->url->link('checkout/cart') . "\";</script>");
            }
        }
    }

    public function shipping() {
        $result = array();
        $this->load->language('checkout/cart');
        if ($this->customer->isLogged()) {
            if (isset($this->request->get['product_id']) && isset($this->request->get['address_id']) && isset($this->request->get['amount']) && isset($this->request->get['city_id']) && isset($this->request->get['new_address'])) {
                $PRODUCT_ID = $this->request->get['product_id'];
                $ADDRESS_ID = $this->request->get['address_id'];
                $CITY_ID = $this->request->get['city_id'];

                $NEW_ADDRESS = $this->request->get['new_address'];
                $CUSTOMERID = $this->customer->getId();

                $AMOUNT = $this->request->get['amount'];

                $this->load->model('catalog/product');
                $this->load->model('account/address');
                $this->load->model('account/seller');
                $this->load->model('localisation/city');

                //Get Customer Data
                $customer = $this->model_account_address->getCheckoutCustomerAddress(
                        array(
                            "customer_id" => $CUSTOMERID,
                            "address_id" => $ADDRESS_ID
                        )
                );
                //Get Seller Data
                $seller = $this->model_account_address->getCheckoutSellerAddress(
                        array(
                            "product_id" => $PRODUCT_ID
                        )
                );
                //Get Product Data
                $product = $this->model_catalog_product->getProduct($PRODUCT_ID);

                //Get Special Price
                if (isset($this->request->post['option'])) {
                    $options = array_filter($this->request->post['option']);
                } else {
                    $options = array();
                }
                $price = 0;
                $option_price = 0;
                $option_points = 0;
                $option_weight = 0;

                foreach ($options as $product_option_id => $value) {
                    //$data['product_info']['option'].=$this->model_catalog_product->getProductOptionByID($product_option_id,$value).'<br>';

                    $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $PRODUCT_ID . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                    if ($option_query->num_rows) {
                        if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
                            $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                            if ($option_value_query->num_rows) {
                                if ($option_value_query->row['price_prefix'] == '+') {
                                    $option_price += $option_value_query->row['price'];
                                } elseif ($option_value_query->row['price_prefix'] == '-') {
                                    $option_price -= $option_value_query->row['price'];
                                }
                            }
                        } elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
                            foreach ($value as $product_option_value_id) {
                                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                if ($option_value_query->num_rows) {
                                    if ($option_value_query->row['price_prefix'] == '+') {
                                        $option_price += $option_value_query->row['price'];
                                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                                        $option_price -= $option_value_query->row['price'];
                                    }
                                }
                            }
                        }
                    }
                }

                if (!empty($seller) && !empty($product)) {
                    if ((int) $AMOUNT > 0 && (int) $AMOUNT >= (int) $product['minimum']) {
                        $price = $product['price'];
                        $product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $PRODUCT_ID . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int) $AMOUNT . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

                        if ($product_discount_query->num_rows) {
                            $price = $product_discount_query->row['price'];
                        }

                        // Product Specials
                        $product_special_query = $this->db->query("SELECT coalesce(sum(price)) as price,sum(discount) as discount FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $PRODUCT_ID . "'AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "'AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC");
                        /*echo "SELECT (sum(price)) as price,sum(discount) as discount FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "'AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC";*/
                        //var_dump($product_special_query);die();
                        if ($product_special_query->num_rows) {
                        
                            if($product_special_query->row['discount'] > 0 && $product_special_query->row['price'] > 0){
                                                $price = $product_special_query->row['price']-$product_special_query->row['discount'];
                                        }
                                        else if ($product_special_query->row['discount'] > 0){                    
                                                $price = $product['price']-$product_special_query->row['discount'];
                                        }
                                        else if($product_special_query->row['price'] > 0){                    
                                                $price = $product_special_query->row['price'];
                                        }
                                        else {
                                                $price = $product['price'];   
                                        }
                        }

                        /*var_dump($product['price']);
                        var_dump($product_special_query->row['price']);
                        var_dump($product_special_query->row['discount']);
                        var_dump($price);die();*/
                        $price = $price + $option_price;
                        
                        $this->session->data['payment_address_id'] = $ADDRESS_ID;
                        //Use API Raja Ongkir
                        if ($NEW_ADDRESS == 1) {
                            $cityro = $this->model_localisation_city->getCity($CITY_ID);
                            if (empty($cityro)) {
                                $DESTINATION = 0;
                            } else {
                                $DESTINATION = $cityro['ro_id'];
                            }
                        } else {
                            if (!empty($customer)) {
                                $DESTINATION = $this->model_localisation_city->getCity($customer['city_id'])['ro_id'];
                            } else {
                                $DESTINATION = 0;
                            }
                        }
                        $ORIGIN = $this->model_localisation_city->getCity($seller['city_id'])['ro_id'];
                        $WEIGHT = (int) ceil($this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) * $AMOUNT);
                        $SELLER_ID = $seller['seller_id'];

                        //Get Seller Logistic
                        $logisticSeller = $this->model_account_seller->getLogisticSeller($SELLER_ID);
                        $logisticSellerCode = $this->model_account_seller->getLogisticCodeSeller($SELLER_ID);

                        $logisticRO = array();
                        //Get Courier
                        foreach ($logisticSellerCode as $logistic) {
                            $logisticAPIResult = $this->rajaongkir->getServiceRajaOngkir($ORIGIN, $DESTINATION, $WEIGHT, $logistic['shipping_code']);
                            if (!empty($logisticAPIResult)) {
                                $logisticRO[] = $logisticAPIResult[0];
                            }
                        }

                        if (!empty($logisticRO) && !empty($logisticSeller)) {
                            $productPrice = $this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')) * $AMOUNT;
                            //Set Result
                            $result['shipping'] = $this->compareShippingRO($logisticRO, $logisticSeller, $productPrice);

                            $result['address'] = $this->model_account_address->getCheckoutAddress(
                                    array(
                                        "customer_id" => $CUSTOMERID,
                                        "address_id" => $ADDRESS_ID
                                    )
                            );
                        } else {
                            $error_log = $this->language->get('text_error_log_not_match');
                        }
                    } else {
                        $shippingror_log = $this->language->get('error_amount_0');
                    }
                } else {
                    $error_log = $this->language->get('text_error_log_seller');
                }
            } else {
                $error_log = $this->language->get('text_error_log_product');
            }
        }
/*var_dump($result['shipping']);
die();*/
        //Set Response
        if (!empty($result)) {
            $result['status'] = array(
                "code" => 200,
                "description" => "OK"
            );
        } else {
            $result['status'] = array(
                "code" => 500,
                "description" => "ERROR",
                "error_log" => $error_log
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function editShipping() {
        $result = array();
        $products = array();
        $SHIPPING = array();
        $WEIGHT = 0;
        $WEIGHT_CLASS_ID = 0;
        if (isset($this->request->post['key'])) {
            foreach ($this->request->post['key'] as $key) {
                foreach ($this->cart->getProducts() as $product) {
                    if ($product['key'] == $key) {
                        $products[$key] = $product;
                        if (isset($product['shipping']['delivery'])) {
                            $SHIPPING = $product['shipping']['delivery'];
                        }
                        if (isset($product['shipping']['pickup'])) {
                            $SHIPPING = $product['shipping']['pickup'];
                        }
                        $WEIGHT += $product['weight'];
                        $WEIGHT_CLASS_ID = $product['weight_class_id'];
                        break;
                    }
                }
            }

            if (isset($this->request->get['address_id']) && isset($this->request->get['city_id']) && isset($this->request->get['new_address'])) {
                $ADDRESS_ID = $this->request->get['address_id'];
                $CITY_ID = $this->request->get['city_id'];
                $NEW_ADDRESS = $this->request->get['new_address'];
                $CUSTOMERID = $this->customer->getId();

                $this->load->model('catalog/product');
                $this->load->model('account/address');
                $this->load->model('account/seller');
                $this->load->model('localisation/city');

                //Get Customer Data
                $customer = $this->model_account_address->getCheckoutCustomerAddress(
                        array(
                            "customer_id" => $CUSTOMERID,
                            "address_id" => $ADDRESS_ID
                        )
                );
                //Get Seller Data
                $seller = $this->model_account_seller->getSeller($SHIPPING['seller_id']);


                if (!empty($seller)) {
                    $this->session->data['payment_address_id'] = $ADDRESS_ID;
                    //Use API Raja Ongkir
                    if ($NEW_ADDRESS == 1) {
                        $cityro = $this->model_localisation_city->getCity($CITY_ID);
                        if (empty($cityro)) {
                            $DESTINATION = 0;
                        } else {
                            $DESTINATION = $cityro['ro_id'];
                        }
                        //$DESTINATION=$this->model_localisation_city->getCity($CITY_ID)['ro_id'];
                    } else {
                        if (!empty($customer)) {
                            $DESTINATION = $this->model_localisation_city->getCity($customer['city_id'])['ro_id'];
                        } else {
                            $DESTINATION = 0;
                        }
                    }
                    $ORIGIN = $this->model_localisation_city->getCity($seller['city_id'])['ro_id'];
                    $WEIGHT = (int) ceil($this->weight->convert($WEIGHT, $WEIGHT_CLASS_ID, $this->config->get('config_weight_class_id')));

                    $SELLER_ID = $seller['seller_id'];

                    //Get Seller Logistic
                    $logisticSeller = $this->model_account_seller->getLogisticSeller($SELLER_ID);
                    $logisticSellerCode = $this->model_account_seller->getLogisticCodeSeller($SELLER_ID);
                    $logisticRO = array();
                    //Get Courier
                    foreach ($logisticSellerCode as $logistic) {
                        $logisticAPIResult = $this->rajaongkir->getServiceRajaOngkir($ORIGIN, $DESTINATION, $WEIGHT, $logistic['shipping_code']);
                        if (!empty($logisticAPIResult)) {
                            $logisticRO[] = $logisticAPIResult[0];
                        }
                    }

                    if (!empty($logisticRO) && !empty($logisticSeller)) {
                        $productPrice = 0;
                        //Set Result
                        $result['shipping'] = $this->compareShippingRO($logisticRO, $logisticSeller, $productPrice);

                        $result['address'] = $this->model_account_address->getCheckoutAddress(
                                array(
                                    "customer_id" => $CUSTOMERID,
                                    "address_id" => $ADDRESS_ID
                                )
                        );
                    }
                }
            }
        }

        //Set Response
        if (!empty($result)) {
            $result['status'] = array(
                "code" => 200,
                "description" => "OK"
            );
        } else {
            $result['status'] = array(
                "code" => 500,
                "description" => "ERROR"
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    private function compareShippingRO($logisticRO, $logisticSeller, $productPrice) {
        $comparedLogisticRO = array();
        foreach ($logisticRO as $itemRO) {
            foreach ($itemRO['costs'] as $costRO) {
                $temp = array();
                if (isset($costRO['service'])) {
                    $temp['shipping_code'] = $itemRO['code'];
                    $temp['shipping_service'] = $costRO['service'];
                    $temp['shipping_price'] = $this->currency->format($costRO['cost'][0]['value']);
                    $temp['product_price'] = $this->currency->format($productPrice);
                    $temp['subtotal'] = $this->currency->format(((int) $productPrice + (int) $costRO['cost'][0]['value']));
                    $comparedLogisticRO[] = $temp;
                }
            }
        }
        $shipping_info = array();
        foreach ($logisticSeller as $itemSeller) {
            foreach ($comparedLogisticRO as $itemCompared) {
                $temp = array();
                if ($itemCompared['shipping_code'] == $itemSeller['shipping_code'] && $itemCompared['shipping_service'] == $itemSeller['shipping_service']) {
                    $temp['shipping_id'] = $itemSeller['shipping_id'];
                    $temp['shipping'] = $itemSeller['shipping'];
                    $temp['shipping_service_id'] = $itemSeller['shipping_service_id'];
                    $temp['shipping_service'] = $itemSeller['shipping_service'];
                    $temp['description'] = $itemSeller['description'];
                    $temp['shipping_price'] = $itemCompared['shipping_price'];
                    $temp['product_price'] = $itemCompared['product_price'];
                    $temp['subtotal'] = $itemCompared['subtotal'];
                    $temp['insurance'] = 1;
                    $shipping_info[] = $temp;
                }
            }
        }

        $shipping = array();
        $shipping_id = null;
        $shipping_name = null;

        foreach ($shipping_info as $shippings) {
            $shipping_service_info = $shipping_info;
            if ($shipping_id != $shippings['shipping_id']) {
                if ($shipping_id != $shippings['shipping_id']) {
                    $shipping_id = $shippings['shipping_id'];
                }
                if ($shipping_name != $shippings['shipping']) {
                    $shipping_name = $shippings['shipping'];
                }
                $shipping[$shipping_id]['id'] = $shipping_id;
                $shipping[$shipping_id]['name'] = $shipping_name;

                $shipping_service = array();
                foreach ($shipping_service_info as $shippings_service) {
                    $temp_service = array();
                    if ($shippings_service['shipping_id'] == $shippings['shipping_id']) {
                        $temp_service['id'] = $shippings_service['shipping_service_id'];
                        $temp_service['name'] = $shippings_service['shipping_service'];
                        $temp_service['description'] = $shippings_service['description'];
                        $temp_service['shipping_price'] = $shippings_service['shipping_price'];
                        $temp_service['product_price'] = $shippings_service['product_price'];
                        $temp_service['subtotal'] = $shippings_service['subtotal'];
                        $temp_service['insurance'] = $shippings_service['insurance'];
                        $shipping_service[] = $temp_service;
                    }
                }
                $shipping[$shipping_id]['service'] = $shipping_service;
            }
        }
        return $shipping;
    }

    private function getTotalShippingPrice($shipping_price, $product, $amount) {
        $cost = 0;
        $weight = $amount * $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));

        $berat_dengan_dua_desimal = number_format($weight, 2, '.', ','); // Mengubah berat menjadi hanya memiliki 2 nilai desimal
        $berat_tanpa_desimal = substr($berat_dengan_dua_desimal, 0, -2); // Menghilangkan nilai desimal
        $angka_kontrol = $weight - $berat_tanpa_desimal; // mencari nilai desimal sebagai angka kontrol
        if ($weight <= 1.0) { // Jika berat barang <= 1.0 kilogram
            $cost = $shipping_price;
        } elseif ($angka_kontrol <= 0.2) { // bila berat memiliki nilai desimal 0
            $cost = $shipping_price + (($berat_tanpa_desimal - 1.0) * $shipping_price);
        } else { // bila berat memiliki nilai desimal tidak sama dengan 0
            $cost = $shipping_price + (($berat_tanpa_desimal) * $shipping_price);
        }
        return $cost;
    }

    public function editCart() {
        $this->load->language('checkout/cart');
        $json = array();

        if (isset($this->request->post['key'])) {
            $dataKey = $this->request->post['key'];

            $products = $this->cart->getProducts();
            $product = array();
            foreach ($products as $item) {
                if ($dataKey == $item['key']) {
                    $product = $item;
                    break;
                }
            }
            if (!empty($product)) {
                if (isset($this->request->post['product_amount'])) {
                    $AMOUNT = $this->request->post['product_amount'];
                } else {
                    $AMOUNT = 0;
                }

                if (isset($this->request->post['product_information'])) {
                    $INFORMATION = $this->request->post['product_information'];
                } else {
                    $INFORMATION = "";
                }

                $this->load->model('catalog/product');
                $this->load->model('shipping/shipping');

                $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                if ($AMOUNT > 0) {
                    if ($product_info) {
                        //CHECK AMOUNT
                        if (!empty($product['option'])) {
                            $options = $product['option'];
                        } else {
                            $options = array();
                        }
                        $shipping = array();
                        $stockOption = $this->model_catalog_product->checkStockOption($product['product_id'], $options, $AMOUNT);
                        if (((int) $product_info['quantity'] - $AMOUNT) >= 0 && $stockOption == true) {
                            if (isset($product['shipping']['delivery'])) {
                                //Use API Raja Ongkir
                                $DESTINATION = $product['shipping']['delivery']['destination_ro'];
                                $ORIGIN = $product['shipping']['delivery']['origin_ro'];
                                $SELLER_ID = $product['shipping']['delivery']['seller_id'];
                                $ADDRESS_ID = $product['shipping']['delivery']['shipping_address'];
                                $SHIPPING_SERVICE_ID = $product['shipping']['delivery']['shipping_service_id'];
                                $SHIPPING_ID = $product['shipping']['delivery']['shipping_id'];
                                $CART_WEIGHT = $this->cart->getWeightShipping(
                                        array(
                                            'seller_id' => $SELLER_ID,
                                            'shipping_address' => $ADDRESS_ID,
                                            'shipping_service_id' => $SHIPPING_SERVICE_ID
                                        )
                                );
                                $WEIGHT = $CART_WEIGHT + (int) ceil($this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) * $AMOUNT / $product['quantity']) - (int) ceil($this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')));
                                //echo $WEIGHT;
                                $shipping_code = $this->model_shipping_shipping->getShippingCode($SHIPPING_ID);

                                $logisticRO = $this->rajaongkir->getServiceRajaOngkir($ORIGIN, $DESTINATION, $WEIGHT, $shipping_code);
                                //echo json_encode($logisticRO);
                                if (!empty($logisticRO)) {
                                    $shipping_service = $this->model_shipping_shipping->getShippingService($SHIPPING_SERVICE_ID);
                                    $price = $this->getPriceShippingRO($logisticRO, $shipping_code, $shipping_service);
                                    if ($price > 0) {
                                        $shipping = $product['shipping'];
                                        $shipping['delivery']['price'] = $price;
                                        $shipping['information'] = $INFORMATION;
                                    } else {
                                        $json['error']['api'] = $this->language->get('error_api');
                                    }
                                } else {
                                    $json['error']['api'] = $this->language->get('error_api');
                                }
                            }
                            if (isset($product['shipping']['pickup'])) {
                                $shipping = $product['shipping'];
                                $shipping['information'] = $INFORMATION;
                            }
                            if (!isset($json['error'])) {
                                $this->cart->update($dataKey, $AMOUNT, $shipping);

                                unset($this->session->data['shipping_method']);
                                unset($this->session->data['shipping_methods']);
                                unset($this->session->data['payment_method']);
                                unset($this->session->data['payment_methods']);
                                unset($this->session->data['reward']);

                                $json['success']['url'] = $this->url->link('checkout/cart');
                            }
                        } else {
                            $json['error']['amount'] = $this->language->get('error_amount');
                        }
                    }
                } else {
                    $json['error']['amount'] = $this->language->get('error_amount_0');
                }
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function editAddress() {

        $this->load->language('checkout/cart');
        $SHIPPING_DATA = array();
        $products = array();
        $json = array();
        $this->load->model('catalog/product');
        $this->load->model('account/address');
        $this->load->model('account/seller');
        $this->load->model('localisation/city');
        $this->load->model('shipping/shipping');
        $WEIGHT = 0;
        $WEIGHT_CLASS_ID = 0;
        $DATA_KEY = explode("%", $this->request->post['key']);
        if (isset($this->request->post['key'])) {
            $DATA_KEY = explode("%", $this->request->post['key']);
            foreach ($DATA_KEY as $key) {
                foreach ($this->cart->getProducts() as $product) {
                    if ($product['key'] == $key) {
                        $products[$key] = $product;
                        if (isset($product['shipping']['delivery'])) {
                            $SHIPPING_DATA = $product['shipping']['delivery'];
                        }
                        if (isset($product['shipping']['pickup'])) {
                            $SHIPPING_DATA = $product['shipping']['pickup'];
                        }
                        $WEIGHT += $product['weight'];
                        $WEIGHT_CLASS_ID = $product['weight_class_id'];
                        break;
                    }
                }
            }


            //CHECK DELIVERY TYPE
            $shipping = array();
            $delivery_type = (int) $this->request->post['delivery_type'];

            //Get Seller Data
            $seller = $this->model_account_seller->getSeller($SHIPPING_DATA['seller_id']);
            //echo json_encode($seller);
            if ($delivery_type == 1) {
                //CHECK ADDRESS STATUS
                $address_id = 0;
                $new_address = (int) $this->request->post['new_address'];
                if ($new_address == 0) {
                    if ($this->request->post['shipping_address'] == '') {
                        $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                    } else {
                        $address_id = $this->request->post['shipping_address'];
                    }
                } else {
                    if ((utf8_strlen(trim($this->request->post['add_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['add_firstname'])) > 32)) {
                        $json['error']['delivery']['add_firstname'] = $this->language->get('error_add_firstname');
                    }

                    if ((utf8_strlen(trim($this->request->post['add_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['add_lastname'])) > 32)) {
                        $json['error']['delivery']['add_lastname'] = $this->language->get('error_add_lastname');
                    }

                    if ((utf8_strlen(trim($this->request->post['add_alias'])) < 1) || (utf8_strlen(trim($this->request->post['add_alias'])) > 32)) {
                        $json['error']['delivery']['add_alias'] = $this->language->get('error_add_alias');
                    }

                    if ((utf8_strlen(trim($this->request->post['add_address_1'])) < 3) || (utf8_strlen(trim($this->request->post['add_address_1'])) > 128)) {
                        $json['error']['delivery']['add_address_1'] = $this->language->get('error_add_address_1');
                    }

                    if (!isset($this->request->post['add_country_id']) || $this->request->post['add_country_id'] == '') {
                        $json['error']['delivery']['add_country_id'] = $this->language->get('error_add_country_id');
                    }

                    if (!isset($this->request->post['add_zone_id']) || $this->request->post['add_zone_id'] == '') {
                        $json['error']['delivery']['add_zone_id'] = $this->language->get('error_add_zone_id');
                    }

                    if (!isset($this->request->post['add_city_id']) || $this->request->post['add_city_id'] == '') {
                        $json['error']['delivery']['add_city_id'] = $this->language->get('error_add_city_id');
                    }

                    if (!isset($this->request->post['add_district_id']) || $this->request->post['add_district_id'] == '') {
                        $json['error']['delivery']['add_district_id'] = $this->language->get('error_add_district_id');
                    }

                    if (!isset($this->request->post['add_subdistrict_id']) || $this->request->post['add_subdistrict_id'] == '') {
                        $json['error']['delivery']['add_subdistrict_id'] = $this->language->get('error_add_subdistrict_id');
                    } else {
                        $this->load->model('localisation/subdistrict');
                        $location_data = $this->model_localisation_subdistrict->getLocation($this->request->post['add_subdistrict_id']);
                        if ($location_data) {
                            $this->request->post['add_country_id'] = $location_data['country_id'];
                            $this->request->post['add_zone_id'] = $location_data['zone_id'];
                            $this->request->post['add_city_id'] = $location_data['city_id'];
                            $this->request->post['add_district_id'] = $location_data['district_id'];
                            $this->request->post['add_subdistrict_id'] = $location_data['subdistrict_id'];
                            $this->request->post['add_postcode'] = $location_data['postcode'];
                        } else {
                            $json['error']['delivery']['add_subdistrict_id'] = $this->language->get('error_add_subdistrict_id');
                        }
                    }

                    if (!isset($this->request->post['add_postcode']) || $this->request->post['add_postcode'] == '') {
                        $json['error']['delivery']['add_postcode'] = $this->language->get('error_add_postcode');
                    }
                }

                if ($this->request->post['shipping_id'] == '') {
                    $json['error']['delivery']['shipping_id'] = $this->language->get('error_shipping_id');
                }

                if ($this->request->post['shipping_service_id'] == '') {
                    $json['error']['delivery']['shipping_service_id'] = $this->language->get('error_shipping_service_id');
                }

                /* if ($this->request->post['insurance']== '') {
                  $json['error']['delivery']['insurance'] = $this->language->get('error_insurance');
                  } */

                if (!isset($json['error']['delivery'])) {
                    if ($new_address == 1) {
                        $address_id = $this->model_account_address->addAddress(
                                array(
                                    "firstname" => $this->request->post['add_firstname'],
                                    "lastname" => $this->request->post['add_lastname'],
                                    "alias" => $this->request->post['add_alias'],
                                    "company" => $this->request->post['add_company'],
                                    "address_1" => $this->request->post['add_address_1'],
                                    "address_2" => $this->request->post['add_address_2'],
                                    "country_id" => $this->request->post['add_country_id'],
                                    "zone_id" => $this->request->post['add_zone_id'],
                                    "city_id" => $this->request->post['add_city_id'],
                                    "district_id" => $this->request->post['add_district_id'],
                                    "subdistrict_id" => $this->request->post['add_subdistrict_id'],
                                    "postcode" => $this->request->post['add_postcode']
                                )
                        );
                    }

                    //CHECK VALID SHIPPING
                    $customer = $this->model_account_address->getCheckoutCustomerAddress(
                            array(
                                "customer_id" => $this->customer->getId(),
                                "address_id" => $address_id
                            )
                    );

                    if (!empty($customer) && !empty($seller)) {
                        //Use API Raja Ongkir
                        $this->session->data['payment_address_id'] = $address_id;
                        $DESTINATION = $this->model_localisation_city->getCity($customer['city_id'])['ro_id'];
                        $ORIGIN = $this->model_localisation_city->getCity($seller['city_id'])['ro_id'];
                        $SELLER_ID = $seller['seller_id'];
                        $CART_WEIGHT = $this->cart->getWeightShippingOthers(
                                array(
                            'seller_id' => $SELLER_ID,
                            'shipping_address' => $address_id,
                            'shipping_service_id' => $this->request->post['shipping_service_id']
                                )
                                , $DATA_KEY
                        );
                        $WEIGHT = $CART_WEIGHT + (int) ceil($this->weight->convert($WEIGHT, $WEIGHT_CLASS_ID, $this->config->get('config_weight_class_id')));
                        //echo $WEIGHT;

                        $shipping_code = $this->model_shipping_shipping->getShippingCode($this->request->post['shipping_id']);

                        $logisticRO = $this->rajaongkir->getServiceRajaOngkir($ORIGIN, $DESTINATION, $WEIGHT, $shipping_code);

                        if (!empty($logisticRO)) {
                            $shipping_service = $this->model_shipping_shipping->getShippingService($this->request->post['shipping_service_id']);
                            $price = $this->getPriceShippingRO($logisticRO, $shipping_code, $shipping_service);
                            if ($price > 0) {
                                $shipping['delivery']['price'] = $price;
                                $shipping['delivery']['shipping_address'] = $address_id;
                                $shipping['delivery']['seller_id'] = $SELLER_ID;
                                $shipping['delivery']['origin'] = $seller['city_id'];
                                $shipping['delivery']['destination'] = $customer['city_id'];
                                $shipping['delivery']['origin_ro'] = $ORIGIN;
                                $shipping['delivery']['destination_ro'] = $DESTINATION;
                            } else {
                                $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                            }
                        } else {
                            $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                        }
                    } else {
                        $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                    }
                }

                if (!isset($json['error']['delivery'])) {
                    $shipping['delivery_type'] = $delivery_type;
                    $shipping['delivery']['shipping_id'] = $this->request->post['shipping_id'];
                    $shipping['delivery']['shipping_service_id'] = $this->request->post['shipping_service_id'];
                    $shipping['delivery']['insurance'] = 1; //$this->request->post['insurance'];
                }
            } else if ($delivery_type == 2) {
                //CHECK PICKUP PERSON STATUS

                if (isset($this->request->post['pickup_person_status'])) {
                    $pickup_person_status = (int) $this->request->post['pickup_person_status'];
                } else {
                    $pickup_person_status = 0;
                }
                if ($pickup_person_status == 1) {
                    if ((utf8_strlen(trim($this->request->post['pickup_person_name'])) < 1) || (utf8_strlen(trim($this->request->post['pickup_person_name'])) > 32)) {
                        $json['error']['pickup']['name'] = $this->language->get('error_pickup_person_name');
                    }
                    if ((utf8_strlen($this->request->post['pickup_person_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['pickup_person_email'])) {
                        $json['error']['pickup']['email'] = $this->language->get('error_pickup_person_email');
                    }

                    if ((utf8_strlen($this->request->post['pickup_person_phone']) < 3) || (utf8_strlen($this->request->post['pickup_person_phone']) > 32)) {
                        $json['error']['pickup']['phone'] = $this->language->get('error_pickup_person_phone');
                    }

                    if ((utf8_strlen($this->request->post['pickup_person_phone']) > 3) || (utf8_strlen($this->request->post['pickup_person_phone']) < 32)) {
                        if (!preg_match('/^[0-9]+$/', $this->request->post['pickup_person_phone'])) {
                            $json['error']['pickup']['phone'] = $this->language->get('error_pickup_person_phone_valid');
                        }
                    }

                    if (!isset($json['error']['pickup_person'])) {
                        $shipping['pickup']['name'] = $this->request->post['pickup_person_name'];
                        $shipping['pickup']['email'] = $this->request->post['pickup_person_email'];
                        $shipping['pickup']['phone'] = $this->request->post['pickup_person_phone'];
                    }
                }

                if ($this->request->post['pp_branch_id'] == '') {
                    $json['error']['pickup']['branch'] = $this->language->get('error_pickup_branch');
                }

                if (!isset($json['error']['pickup'])) {
                    $shipping['delivery_type'] = $delivery_type;
                    $shipping['pickup']['seller_id'] = $seller['seller_id'];
                    $shipping['pickup']['branch'] = $this->request->post['pp_branch_id'];
                    $shipping['pickup']['status'] = $pickup_person_status;
                }
            } else {
                $json['error']['delivery_type'] = $this->language->get('error_delivery_type');
            }

            if (!isset($json['error'])) {
                if (!$json) {
                    $this->cart->updateShipping($DATA_KEY, $shipping);

                    unset($this->session->data['shipping_method']);
                    unset($this->session->data['shipping_methods']);
                    unset($this->session->data['payment_method']);
                    unset($this->session->data['payment_methods']);
                    unset($this->session->data['reward']);

                    $json['success']['url'] = $this->url->link('checkout/cart');
                }
            }
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCart() {
        $this->load->language('checkout/cart');

        $json = array();
        if (!$this->pickup->isPickupPoint() && !$this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
            if ((int) $this->customer->getVerified() != 0) {
                if (isset($this->request->post['product_id'])) {
                    $product_id = (int) $this->request->post['product_id'];
                } else {
                    $product_id = 0;
                }
                $this->load->model('catalog/product');
                $this->load->model('account/address');
                $this->load->model('account/seller');
                $this->load->model('localisation/city');
                $this->load->model('shipping/shipping');

                $product_info = $this->model_catalog_product->getProduct($product_id);


                if (isset($this->request->post['product_amount'])) {
                    $product_amount = (int) $this->request->post['product_amount'];
                } else {
                    $product_amount = 0;
                }
                if (isset($this->request->post['option'])) {
                    $options = array_filter($this->request->post['option']);
                } else {
                    $options = array();
                }
                if ($product_amount > 0) {
                    if ($product_info) {
                        //CHECK STOCK
                        $stockOption = $this->model_catalog_product->checkStockOption($product_id, $options, $product_amount);
                        if (((int) $product_info['quantity'] - $product_amount) >= 0 && $stockOption == true) {
                            //CHECK DELIVERY TYPE
                            $shipping = array();
                            $delivery_type = (int) $this->request->post['delivery_type'];

                            //Get Seller Data
                            $seller = $this->model_account_address->getCheckoutSellerAddress(
                                    array(
                                        "product_id" => $product_id
                                    )
                            );
                            if ($delivery_type == 1) {
                                //CHECK ADDRESS STATUS
                                $address_id = 0;
                                $new_address = (int) $this->request->post['new_address'];
                                if ($new_address == 0) {
                                    if ($this->request->post['shipping_address'] == '') {
                                        $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                                    } else {
                                        $address_id = $this->request->post['shipping_address'];
                                    }
                                } else {
                                    //add_firstname
                                    //add_lastname
                                    //add_company
                                    //add_address_1
                                    //add_address_2
                                    //add_country_id
                                    //add_zone_id
                                    //add_city_id
                                    //add_district_id
                                    //add_subdistrict_id
                                    //add_postcode

                                    if ((utf8_strlen(trim($this->request->post['add_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['add_firstname'])) > 32)) {
                                        $json['error']['delivery']['add_firstname'] = $this->language->get('error_add_firstname');
                                    }

                                    if ((utf8_strlen(trim($this->request->post['add_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['add_lastname'])) > 32)) {
                                        $json['error']['delivery']['add_lastname'] = $this->language->get('error_add_lastname');
                                    }

                                    if ((utf8_strlen(trim($this->request->post['add_alias'])) < 1) || (utf8_strlen(trim($this->request->post['add_alias'])) > 32)) {
                                        $json['error']['delivery']['add_alias'] = $this->language->get('error_add_alias');
                                    }

                                    if ((utf8_strlen(trim($this->request->post['add_address_1'])) < 3) || (utf8_strlen(trim($this->request->post['add_address_1'])) > 128)) {
                                        $json['error']['delivery']['add_address_1'] = $this->language->get('error_add_address_1');
                                    }

                                    if (!isset($this->request->post['add_country_id']) || $this->request->post['add_country_id'] == '') {
                                        $json['error']['delivery']['add_country_id'] = $this->language->get('error_add_country_id');
                                    }

                                    if (!isset($this->request->post['add_zone_id']) || $this->request->post['add_zone_id'] == '') {
                                        $json['error']['delivery']['add_zone_id'] = $this->language->get('error_add_zone_id');
                                    }

                                    if (!isset($this->request->post['add_city_id']) || $this->request->post['add_city_id'] == '') {
                                        $json['error']['delivery']['add_city_id'] = $this->language->get('error_add_city_id');
                                    }

                                    if (!isset($this->request->post['add_district_id']) || $this->request->post['add_district_id'] == '') {
                                        $json['error']['delivery']['add_district_id'] = $this->language->get('error_add_district_id');
                                    }

                                    if (!isset($this->request->post['add_subdistrict_id']) || $this->request->post['add_subdistrict_id'] == '') {
                                        $json['error']['delivery']['add_subdistrict_id'] = $this->language->get('error_add_subdistrict_id');
                                    } else {
                                        $this->load->model('localisation/subdistrict');
                                        $location_data = $this->model_localisation_subdistrict->getLocation($this->request->post['add_subdistrict_id']);
                                        if ($location_data) {
                                            $this->request->post['add_country_id'] = $location_data['country_id'];
                                            $this->request->post['add_zone_id'] = $location_data['zone_id'];
                                            $this->request->post['add_city_id'] = $location_data['city_id'];
                                            $this->request->post['add_district_id'] = $location_data['district_id'];
                                            $this->request->post['add_subdistrict_id'] = $location_data['subdistrict_id'];
                                            $this->request->post['add_postcode'] = $location_data['postcode'];
                                        } else {
                                            $json['error']['delivery']['add_subdistrict_id'] = $this->language->get('error_add_subdistrict_id');
                                        }
                                    }

                                    if (!isset($this->request->post['add_postcode']) || $this->request->post['add_postcode'] == '') {
                                        $json['error']['delivery']['add_postcode'] = $this->language->get('error_add_postcode');
                                    }

                                    if (!isset($json['error']['delivery'])) {
                                        
                                    }
                                }

                                if ($this->request->post['shipping_id'] == '') {
                                    $json['error']['delivery']['shipping_id'] = $this->language->get('error_shipping_id');
                                }

                                if ($this->request->post['shipping_service_id'] == '') {
                                    $json['error']['delivery']['shipping_service_id'] = $this->language->get('error_shipping_service_id');
                                }

                                /* if ($this->request->post['insurance']== '') {
                                  $json['error']['delivery']['insurance'] = $this->language->get('error_insurance');
                                  } */

                                if (!isset($json['error']['delivery'])) {
                                    if ($new_address == 1) {
                                        $address_id = $this->model_account_address->addAddress(
                                                array(
                                                    "firstname" => $this->request->post['add_firstname'],
                                                    "lastname" => $this->request->post['add_lastname'],
                                                    "alias" => $this->request->post['add_alias'],
                                                    "company" => $this->request->post['add_company'],
                                                    "address_1" => $this->request->post['add_address_1'],
                                                    "address_2" => $this->request->post['add_address_2'],
                                                    "country_id" => $this->request->post['add_country_id'],
                                                    "zone_id" => $this->request->post['add_zone_id'],
                                                    "city_id" => $this->request->post['add_city_id'],
                                                    "district_id" => $this->request->post['add_district_id'],
                                                    "subdistrict_id" => $this->request->post['add_subdistrict_id'],
                                                    "postcode" => $this->request->post['add_postcode']
                                                )
                                        );
                                    }
                                    //CHECK VALID SHIPPING
                                    $customer = $this->model_account_address->getCheckoutCustomerAddress(
                                            array(
                                                "customer_id" => $this->customer->getId(),
                                                "address_id" => $address_id
                                            )
                                    );

                                    if (!empty($customer) && !empty($seller)) {
                                        //Use API Raja Ongkir
                                        $this->session->data['payment_address_id'] = $address_id;
                                        $DESTINATION = $this->model_localisation_city->getCity($customer['city_id'])['ro_id'];
                                        $ORIGIN = $this->model_localisation_city->getCity($seller['city_id'])['ro_id'];
                                        $SELLER_ID = $seller['seller_id'];
                                        $CART_WEIGHT = $this->cart->getWeightShipping(
                                                array(
                                                    'seller_id' => $SELLER_ID,
                                                    'shipping_address' => $address_id,
                                                    'shipping_service_id' => $this->request->post['shipping_service_id']
                                                )
                                        );
                                        $WEIGHT = $CART_WEIGHT + (int) ceil($this->weight->convert($product_info['weight'], $product_info['weight_class_id'], $this->config->get('config_weight_class_id')) * $product_amount);

                                        $shipping_code = $this->model_shipping_shipping->getShippingCode($this->request->post['shipping_id']);

                                        $logisticRO = $this->rajaongkir->getServiceRajaOngkir($ORIGIN, $DESTINATION, $WEIGHT, $shipping_code);

                                        if (!empty($logisticRO)) {
                                            $shipping_service = $this->model_shipping_shipping->getShippingService($this->request->post['shipping_service_id']);
                                            $price = $this->getPriceShippingRO($logisticRO, $shipping_code, $shipping_service);
                                            if ($price > 0) {
                                                $shipping['delivery']['price'] = $price;
                                                $shipping['delivery']['shipping_address'] = $address_id;
                                                $shipping['delivery']['seller_id'] = $SELLER_ID;
                                                $shipping['delivery']['origin'] = $seller['city_id'];
                                                $shipping['delivery']['destination'] = $customer['city_id'];
                                                $shipping['delivery']['origin_ro'] = $ORIGIN;
                                                $shipping['delivery']['destination_ro'] = $DESTINATION;
                                            } else {
                                                $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                                            }
                                        } else {
                                            $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                                        }
                                    } else {
                                        $json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
                                    }
                                }

                                if (!isset($json['error']['delivery'])) {
                                    $shipping['delivery_type'] = $delivery_type;
                                    $shipping['delivery']['shipping_id'] = $this->request->post['shipping_id'];
                                    $shipping['delivery']['shipping_service_id'] = $this->request->post['shipping_service_id'];
                                    $shipping['delivery']['insurance'] = 1; //$this->request->post['insurance'];
                                }
                            } else if ($delivery_type == 2) {
                                //CHECK PICKUP PERSON STATUS

                                if (isset($this->request->post['pickup_person_status'])) {
                                    $pickup_person_status = (int) $this->request->post['pickup_person_status'];
                                } else {
                                    $pickup_person_status = 0;
                                }
                                if ($pickup_person_status == 1) {
                                    if ((utf8_strlen(trim($this->request->post['pickup_person_name'])) < 1) || (utf8_strlen(trim($this->request->post['pickup_person_name'])) > 32)) {
                                        $json['error']['pickup']['name'] = $this->language->get('error_pickup_person_name');
                                    }
                                    if ((utf8_strlen($this->request->post['pickup_person_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['pickup_person_email'])) {
                                        $json['error']['pickup']['email'] = $this->language->get('error_pickup_person_email');
                                    }

                                    if ((utf8_strlen($this->request->post['pickup_person_phone']) < 3) || (utf8_strlen($this->request->post['pickup_person_phone']) > 32)) {
                                        $json['error']['pickup']['phone'] = $this->language->get('error_pickup_person_phone');
                                    }

                                    if ((utf8_strlen($this->request->post['pickup_person_phone']) > 3) || (utf8_strlen($this->request->post['pickup_person_phone']) < 32)) {
                                        if (!preg_match('/^[0-9]+$/', $this->request->post['pickup_person_phone'])) {
                                            $json['error']['pickup']['phone'] = $this->language->get('error_pickup_person_phone_valid');
                                        }
                                    }

                                    if (!isset($json['error']['pickup_person'])) {
                                        $shipping['pickup']['name'] = $this->request->post['pickup_person_name'];
                                        $shipping['pickup']['email'] = $this->request->post['pickup_person_email'];
                                        $shipping['pickup']['phone'] = $this->request->post['pickup_person_phone'];
                                    }
                                }

                                if ($this->request->post['pp_branch_id'] == '') {
                                    $json['error']['pickup']['branch'] = $this->language->get('error_pickup_branch');
                                }

                                if (!isset($json['error']['pickup'])) {
                                    $shipping['delivery_type'] = $delivery_type;
                                    $shipping['pickup']['seller_id'] = $seller['seller_id'];
                                    $shipping['pickup']['branch'] = $this->request->post['pp_branch_id'];
                                    $shipping['pickup']['status'] = $pickup_person_status;
                                }
                            } else {
                                $json['error']['delivery_type'] = $this->language->get('error_delivery_type');
                            }
                        } else {
                            $json['error']['amount'] = $this->language->get('error_amount');
                        }

                        if (!isset($json['error'])) {
                            //SET SHIPPING
                            $shipping['information'] = $this->request->post['product_information'];

                            if (isset($this->request->post['option'])) {
                                $option = array_filter($this->request->post['option']);
                            } else {
                                $option = array();
                            }

                            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

                            foreach ($product_options as $product_option) {
                                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                                }
                            }

                            if (isset($this->request->post['recurring_id'])) {
                                $recurring_id = $this->request->post['recurring_id'];
                            } else {
                                $recurring_id = 0;
                            }

                            $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

                            if ($recurrings) {
                                $recurring_ids = array();

                                foreach ($recurrings as $recurring) {
                                    $recurring_ids[] = $recurring['recurring_id'];
                                }

                                if (!in_array($recurring_id, $recurring_ids)) {
                                    $json['error']['recurring'] = $this->language->get('error_recurring_required');
                                }
                            }

                            if (!$json) {
                                $this->cart->add($this->request->post['product_id'], $product_amount, $option, $recurring_id, $shipping);


                                $this->load->model('tool/image');
                                $json['image'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));

                                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                                unset($this->session->data['shipping_method']);
                                unset($this->session->data['shipping_methods']);
                                unset($this->session->data['payment_method']);
                                unset($this->session->data['payment_methods']);

                                // Totals
                                $this->load->model('extension/extension');

                                $total_data = array();
                                $total = 0;
                                $taxes = $this->cart->getTaxes();

                                // Display prices
                                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                    $sort_order = array();

                                    $results = $this->model_extension_extension->getExtensions('total');

                                    foreach ($results as $key => $value) {
                                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                                    }

                                    array_multisort($sort_order, SORT_ASC, $results);

                                    foreach ($results as $result) {
                                        if ($this->config->get($result['code'] . '_status')) {
                                            $this->load->model('total/' . $result['code']);

                                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                                        }
                                    }

                                    $sort_order = array();

                                    foreach ($total_data as $key => $value) {
                                        $sort_order[$key] = $value['sort_order'];
                                    }

                                    array_multisort($sort_order, SORT_ASC, $total_data);
                                }

                                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
                            } else {
                                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
                            }

                            //$json=$shipping;
                        }
                    }
                } else {
                    $json['error']['amount'] = $this->language->get('error_amount_0');
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getPriceShippingRO($logisticRO, $shipping_code, $shipping_service) {
        $price = 0;
        $comparedLogisticRO = array();
        foreach ($logisticRO as $itemRO) {
            foreach ($itemRO['costs'] as $costRO) {
                $temp = array();
                if (isset($costRO['service'])) {
                    if ($itemRO['code'] == $shipping_code && $costRO['service'] == $shipping_service) {
                        if ($costRO['cost'][0]['value']) {
                            $price = $costRO['cost'][0]['value'];
                            break;
                        }
                    }
                }
            }
        }
        return $price;
        //return 0;
    }

    public function checkOption() {
        $this->load->language('checkout/cart');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = (int) $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {

            if (isset($this->request->post['quantity'])) {
                $quantity = (int) $this->request->post['quantity'];
            } else {
                $quantity = 1;
            }

            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            } else {
                $option = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            if (isset($this->request->post['recurring_id'])) {
                $recurring_id = $this->request->post['recurring_id'];
            } else {
                $recurring_id = 0;
            }

            $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($recurrings) {
                $recurring_ids = array();

                foreach ($recurrings as $recurring) {
                    $recurring_ids[] = $recurring['recurring_id'];
                }

                if (!in_array($recurring_id, $recurring_ids)) {
                    $json['error']['recurring'] = $this->language->get('error_recurring_required');
                }
            }

            if (!$json) {
                $json['success'] = true;
            } else {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
