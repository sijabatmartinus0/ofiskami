<?php

class ControllerCheckoutQuickCheckout extends Controller {

    protected $data = array();

    private function renderView($template, $data) {
        if (Front::IS_OC2) {
            return $this->load->view($template, $data);
        }
        $this->template = $template;
        return parent::render();
    }

    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->language('checkout/cart');
        $this->load->language('checkout/checkout');

        if (Front::IS_OC2) {
            $this->load->language('checkout/coupon');
            $this->load->language('checkout/voucher');

            $this->load->model('account/activity');
            $this->load->model('account/custom_field');
            $this->load->model('tool/upload');
        }
        $this->load->model('account/address');
        $this->load->model('account/customer');
        $this->load->model('account/customer_group');
        $this->load->model('checkout/checkout');
        $this->load->model('checkout/checkout');
        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');
    }

    public function index() {
        if (!$this->checkCart()) {
            $this->response->redirect($this->url->link('checkout/cart'));
            exit;
        }

        $this->load->language('checkout/checkout');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/checkout', '', 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_loading'] = $this->language->get('text_loading');
        $data['button_confirm'] = $this->language->get('button_confirm');

        $this->document->setTitle($this->language->get('heading_title'));



        if (Front::IS_OC2) {
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

            // Required by klarna
            if ($this->config->get('klarna_account') || $this->config->get('klarna_invoice')) {
                $this->document->addScript('http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js');
            }
        } else {
            $this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
            $this->document->addScript('catalog/view/theme/journal2/lib/bootstrap/button.js');
        }

        $data['default_auth'] = $this->model_checkout_checkout->getProperty($this->session->data, 'journal_checkout_account');

        $data['is_logged_in'] = true;
        // address data
        /* if ($this->isLoggedIn()) {
          $data['is_logged_in'] = true;
          //$data['payment_address'] = $this->renderAddressForm('payment');
          //$data['shipping_address'] = $this->renderAddressForm('shipping');
          } else {
          $data['is_logged_in'] = false;
          $data['allow_guest_checkout'] = $this->allowGuestCheckout();
          $data['register_form'] = $this->renderRegisterForm();
          } */

        $data['is_shipping_required'] = false;
        /* // shipping
          if ($this->isShippingRequired()) {
          $data['is_shipping_required'] = true;
          $data['shipping_methods'] = $this->shipping(true);
          } else {
          $data['is_shipping_required'] = false;
          } */

        // payment
        $data['payment_methods'] = $this->payment(true);

        $data['keperluans'] = $this->necessity(true);
        // coupon + voucher
        $data['coupon_voucher_reward'] = $this->renderCouponVoucherReward();

        // cart
        $data['cart'] = $this->cart(true);

        // checkboxes
        if (!$this->isLoggedIn() && $this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $data['text_privacy'] = sprintf($this->language->get('text_agree'), $this->url->link(Front::IS_OC2 ? 'information/information/agree' : 'information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
            } else {
                $data['text_privacy'] = '';
            }
        } else {
            $data['text_privacy'] = '';
        }

        if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link(Front::IS_OC2 ? 'information/information/agree' : 'information/information/info', 'information_id=' . $this->config->get('config_checkout_id'), 'SSL'), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        if ($data['text_privacy'] === $data['text_agree']) {
            $data['text_privacy'] = '';
        }

        $data['text_comments'] = $this->language->get('text_comments');
        if ($this->isLoggedIn()) {
            $data['entry_newsletter'] = false;
        } else {
            $data['entry_newsletter'] = sprintf($this->language->get('entry_newsletter'), $this->config->get('config_name'));
        }

        $data['comment'] = $this->model_checkout_checkout->getComment();

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (Front::IS_OC2) {
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
        } else {
            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );
        }

        $this->model_checkout_checkout->save();
        $this->response->setOutput($this->load->view('default/template/checkout/checkout_journal.tpl', $data));
    }

    public function save() {
        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address_id')) {
            $this->session->data['shipping_address'] = $this->model_account_address->getAddress($value);
            $this->model_checkout_checkout->setAddress('shipping', $this->session->data['shipping_address']);
        }

        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_country_id')) {
            $this->model_checkout_checkout->setAddress('shipping', array(
                'country_id' => $value,
                'zone_id' => $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_zone_id'),
                'postcode' => $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_postcode'),
            ));
        }

        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_method')) {
            $shipping = explode('.', $value);
            $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
        }

        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'payment_address_id')) {
            $this->session->data['payment_address'] = $this->model_account_address->getAddress($value);
            $this->model_checkout_checkout->setAddress('payment', $this->session->data['payment_address']);
        }

        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'payment_country_id')) {
            $this->model_checkout_checkout->setAddress('payment', array(
                'country_id' => $value,
                'zone_id' => $this->model_checkout_checkout->getProperty($this->request->post, 'payment_zone_id'),
                'postcode' => $this->model_checkout_checkout->getProperty($this->request->post, 'payment_postcode'),
            ));
        }

        if ($value = $this->model_checkout_checkout->getProperty($this->request->post, 'payment_method')) {
            $this->session->data['payment_method'] = $this->session->data['payment_methods'][$value];
        }

        $this->model_checkout_checkout->save();

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode(array('success' => true)));
    }

    public function confirm() {

        $order_data = $this->model_checkout_checkout->getOrder();

        $new_payment_address = null;
        $new_shipping_address = null;

        $register_account = $this->model_checkout_checkout->getProperty($this->request->post, 'account') === 'register';

        $errors = array();

        if ($this->checkCart()) {



            if ($this->isLoggedIn()) {
                /*
                  // payment data
                  if ($this->model_checkout_checkout->getProperty($this->request->post, 'payment_address') === 'existing') {
                  $address_info = $this->model_account_address->getAddress($this->model_checkout_checkout->getProperty($this->request->post, 'payment_address_id'));
                  $order_data = array_replace($order_data, $this->getAddressData($address_info, '', 'payment_'));
                  } else {
                  $new_payment_address = $this->getAddressData($this->request->post, 'payment_', 'payment_');
                  $order_data = array_replace($order_data, $new_payment_address);
                  $errors = array_merge($errors, $this->validateAddressData($new_payment_address, 'payment_'));
                  }

                  // shipping data
                  if ($this->isShippingRequired()) {
                  if ($this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address') === 'existing') {
                  $address_info = $this->model_account_address->getAddress($this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address_id'));
                  $order_data = array_replace($order_data, $this->getAddressData($address_info, '', 'shipping_'));
                  } else {
                  $new_shipping_address = $this->getAddressData($this->request->post, 'shipping_', 'shipping_');
                  $order_data = array_replace($order_data, $new_shipping_address);
                  $errors = array_merge($errors, $this->validateAddressData($new_shipping_address, 'shipping_'));
                  }
                  }
                 */
                if (isset($this->session->data['payment_address_id'])) {
                    $address_info = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
                    $order_data = array_replace($order_data, $this->getAddressData($address_info, '', 'payment_'));
                } else {
                    $address_info = $this->model_account_address->getAddressByCustomer();
                    $order_data = array_replace($order_data, $this->getAddressData($address_info, '', 'payment_'));
                }

                // customer data
                if (!$errors) {
                    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

                    $order_data['customer_id'] = $this->customer->getId();
                    $order_data['customer_group_id'] = $customer_info['customer_group_id'];
                    $order_data['firstname'] = $customer_info['firstname'];
                    $order_data['lastname'] = $customer_info['lastname'];
                    $order_data['email'] = $customer_info['email'];
                    $order_data['telephone'] = $customer_info['telephone'];
                    $order_data['fax'] = $customer_info['fax'];
                    if (Front::IS_OC2) {
                        $order_data['custom_field'] = unserialize($customer_info['custom_field']);
                    }
                }
            } else {
                // check firstname, lastname
                $errors = array_merge($errors, $this->validateUserData($this->request->post, $register_account));

                // check customer group id
                if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                    $order_data['customer_group_id'] = $this->request->post['customer_group_id'];
                } else {
                    $order_data['customer_group_id'] = $this->config->get('config_customer_group_id');
                }

                // check passwords if register
                if ($register_account) {
                    $errors = array_merge($errors, $this->validatePassword($this->request->post));
                }

                // check payment address
                $new_payment_address = $this->getAddressData($this->request->post, 'payment_', 'payment_');
                $order_data = array_replace($order_data, $new_payment_address);
                $errors = array_merge($errors, $this->validateAddressData($new_payment_address, 'payment_', false));

                // add payment firstname and lastname
                $order_data['firstname'] = $this->request->post['firstname'];
                $order_data['lastname'] = $this->request->post['lastname'];
                $order_data['email'] = $this->request->post['email'];
                $order_data['telephone'] = $this->request->post['telephone'];
                $order_data['fax'] = $this->request->post['fax'];
                $order_data['custom_field'] = $this->model_checkout_checkout->getProperty($this->request->post, 'custom_field', array());
                $order_data['payment_firstname'] = $order_data['firstname'];
                $order_data['payment_lastname'] = $order_data['lastname'];

                /*
                  // check delivery address
                  if ($this->isShippingRequired()) {
                  if (!$this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address')) {
                  $new_shipping_address = $this->getAddressData($this->request->post, 'shipping_', 'shipping_');
                  $order_data = array_replace($order_data, $new_shipping_address);
                  $errors = array_merge($errors, $this->validateAddressData($new_shipping_address, 'shipping_'));
                  } else {
                  $order_data = array_replace($order_data, $this->getAddressData($order_data, 'payment_', 'shipping_'));
                  }
                  } */
            }

            // payment method
            if ($payment_method = $this->model_checkout_checkout->getProperty($this->session->data, 'payment_methods.' . $this->model_checkout_checkout->getProperty($this->request->post, 'payment_method') . '.title')) {
                $order_data['payment_method'] = $payment_method;
                $order_data['payment_code'] = $this->model_checkout_checkout->getProperty($this->request->post, 'payment_method');
            } else {
                $errors['payment_method'] = 'no payment method';
            }

            $order_data['necessity'] = $this->request->post['keperluan'];
            /*
              // shipping method
              if ($this->isShippingRequired()) {
              $shipping = explode('.', $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_method'));
              $shipping_method = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
              if ($shipping_method) {
              $order_data['shipping_method'] = $shipping_method['title'];
              $order_data['shipping_code'] = $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_method');
              } else {
              $order_data['shipping_method'] = 'no shipping method';
              }
              }
             */

            // order totals
            $order_data['totals'] = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            if (Front::IS_OC2) {
                $this->load->model('extension/extension');
                $results = $this->model_extension_extension->getExtensions('total');
            } else {
                $this->load->model('setting/extension');
                $results = $this->model_setting_extension->getExtensions('total');
            }

            $sort_order = array();

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($order_data['totals'] as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $order_data['totals']);

            $order_data['total'] = $total;

            // order products
            $order_data['products'] = array();

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => Front::IS_OC2 ? $option['value'] : $option['option_value'],
                        'type' => $option['type']
                    );
                }

                $order_data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'specialok' =>$product['specialok'],
                    'specialmerchant' =>$product['specialmerchant'],
                    'total' => $product['total'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward'],
                    'comment' => (isset($product['shipping']['information']) ? $product['shipping']['information'] : '')
                );
            }

            foreach ($order_data['products'] as $pr) {
                $id[] = array(
                    'product_id' => $pr['product_id'],
                    'quantity' => $pr['quantity'],
                    'total' => $pr['total']
                );
            }


            $promo = $this->model_checkout_checkout->isPromo($id);

            // Gift Voucher
            $order_data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $order_data['vouchers'][] = array(
                        'description' => $voucher['description'],
                        'code' => substr(md5(mt_rand()), 0, 10),
                        'to_name' => $voucher['to_name'],
                        'to_email' => $voucher['to_email'],
                        'from_name' => $voucher['from_name'],
                        'from_email' => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message' => $voucher['message'],
                        'amount' => $voucher['amount']
                    );
                }
            }

            // comment + checkboxes
            $order_data['comment'] = $this->model_checkout_checkout->getProperty($this->request->post, 'comment');

            if (!$this->isLoggedIn() && $this->config->get('config_account_id')) {
                $this->load->model('catalog/information');

                $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

                if ($information_info && !isset($this->request->post['privacy'])) {
                    $errors['privacy'] = sprintf($this->language->get('error_agree'), $information_info['title']);
                }
            }

            if ($this->config->get('config_checkout_id')) {
                $this->load->model('catalog/information');

                $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

                if ($information_info && !isset($this->request->post['agree'])) {
                    $errors['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
                }
            }

            if ($this->config->get('config_account_id') == $this->config->get('config_checkout_id')) {
                unset($errors['privacy']);
            }

            // Validate minimum quantity requirements.
            $products = $this->cart->getProducts();

            foreach ($products as $product) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $redirect = $this->url->link('checkout/cart');
                    $errors['quantity'] = 'Error';
                    break;
                }
            }

            

            if (!$errors) {
                if ($this->isLoggedIn()) {
                    // save new payment address
                    if ($new_payment_address) {
                        $this->model_account_address->addAddress($this->getAddressData($new_payment_address, 'payment_'));
                    }

                    // save new shipping address
                    if ($new_shipping_address && $new_shipping_address !== $new_payment_address) {
                        $this->model_account_address->addAddress($this->getAddressData($new_shipping_address, 'shipping_'));
                    }
                } else if ($register_account) {
                    $redirect = $this->registerAccount();
                } else {
                    $this->session->data['guest'] = $this->getAddressData($order_data, 'payment_');
                }
            }
	$redirects= $this->url->link('checkout/cart');

            // update order
            $this->model_checkout_checkout->setOrderData($order_data);
            $this->model_checkout_checkout->saveOrder();
        } else {
            $redirect = $this->url->link('checkout/cart');
        }
        $this->session->data['journal_checkout_account'] = $this->model_checkout_checkout->getProperty($this->request->post, 'account');
        $this->session->data['journal_checkout_shipping_address'] = $this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address', '0');

        // send response
        header('Content-Type: application/json');
        echo json_encode(array(
            'errors' => $errors ? $errors : null,
            'redirect' => $redirect,
            'order_data' => $order_data,
            'promo' => $promo,
            'redirects'=>$redirects
        ));
        exit;
    }

    public function shipping($return = false) {
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');

        $data['shipping_methods'] = $this->model_checkout_checkout->getShippingMethods();
        $data['code'] = $this->model_checkout_checkout->getShippingMethodCode();

        if (!$data['shipping_methods']) {
            $data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
        } else {
            $data['error_warning'] = '';
        }

        if ($return) {
            // return $this->renderView($this->config->get('config_template') . '/template/journal2/checkout/shipping_methods.tpl', $data);
            return $this->renderView('default/template/checkout/shipping_methods_journal.tpl', $data);
        } else {
            // $this->response->setOutput($this->renderView($this->config->get('config_template') . '/template/journal2/checkout/shipping_methods.tpl', $data));
            $this->response->setOutput($this->renderView('default/template/checkout/shipping_methods_journal.tpl', $data));
        }
    }

    public function payment($return = false) {
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_title_payment_method'] = $this->language->get('text_title_payment_method');

        $data['payment_methods'] = $this->model_checkout_checkout->getPaymentMethods();
        $data['code'] = $this->model_checkout_checkout->getPaymentMethodCode();


        if (!$data['payment_methods']) {
            $data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
        } else {
            $data['error_warning'] = '';
        }

        if ($return) {
            return $this->renderView('default/template/checkout/payment_methods_journal.tpl', $data);
        } else {
            $this->response->setOutput($this->renderView('default/template/checkout/payment_methods_journal.tpl', $data));
        }
    }

    public function necessity($return = false) {

        $data['keperluans'] = $this->model_checkout_checkout->getPaymentMethods();
        //$data['keperluans'] = array();
        if(isset($_POST['keperluan'])) {
            if($_POST['keperluan'] == 'Pribadi') {
            // code for YES here
            ($data['necessity']) == 'Pribadi';

            } elseif($_POST['agree'] == 'Perusahaan') {
            // code for NO here
            ($data['necessity']) == 'Perusahaan';
            }
        }   

        //var_dump($_POST['keperluan']);die();
        if ($return) {
            return $this->renderView('default/template/checkout/keperluan.tpl', $data);
        } else {
            $this->response->setOutput($this->renderView('default/template/checkout/keperluan.tpl', $data));
        }
    }

    public function cart($return = false) {
        $this->load->model('account/seller');
        $this->load->model('account/address');
        $this->load->model('shipping/shipping');
        $this->load->model('localisation/pp_branch');
        $this->load->model('tool/image');

        $data['text_recurring_item'] = $this->language->get('text_recurring_item');
        $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
        $data['text_title_cart'] = $this->language->get('text_title_cart');
        $data['text_payment_detail'] = $this->language->get('text_payment_detail');

        $data['button_update'] = $this->language->get('button_update');
        $data['button_remove'] = $this->language->get('button_remove');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');

        //edit by arin
        $data['column_seller'] = $this->language->get('column_seller');
        $data['column_product_price'] = $this->language->get('column_product_price');
        $data['column_remark'] = $this->language->get('column_remark');
        $data['column_address'] = $this->language->get('column_address');
        $data['column_insurance'] = $this->language->get('column_insurance');
        $data['column_subtotal'] = $this->language->get('column_subtotal');
        $data['column_total_unit'] = $this->language->get('column_total_unit');
        $data['column_insurance_charge'] = $this->language->get('column_insurance_charge');
        $data['column_shipping'] = $this->language->get('column_shipping');
        $data['text_delete_all'] = $this->language->get('text_delete_all');
        $data['text_per_invoice'] = $this->language->get('text_per_invoice');
        $data['text_insurance_yes'] = $this->language->get('text_insurance_yes');
        $data['text_insurance_no'] = $this->language->get('text_insurance_no');
        $data['text_voucher'] = $this->language->get('text_voucher');

        $data['cart'] = array();
        $products = $this->cart->getProducts();
        $delivery_shipping_address = "";
        $delivery_seller_id = "";
        $delivery_shipping_service_id = "";
        $pickup_seller_id = "";
        $pickup_pp_branch = "";
        $key = "";
        $tempKey = "";
        foreach ($products as $product) {
            if (isset($product['shipping'])) {
                if ($product['shipping']['delivery_type'] == 1) {
                    if ($product['shipping']['delivery']['shipping_address'] != $delivery_shipping_address || $product['shipping']['delivery']['seller_id'] != $delivery_seller_id || $product['shipping']['delivery']['shipping_service_id'] != $delivery_shipping_service_id) {
                        $delivery_shipping_address = $product['shipping']['delivery']['shipping_address'];
                        $delivery_seller_id = $product['shipping']['delivery']['seller_id'];
                        $delivery_shipping_service_id = $product['shipping']['delivery']['shipping_service_id'];


                        $tempKey = "delivery" . $delivery_shipping_address . $delivery_seller_id . $delivery_shipping_service_id;
                    }
                    //GET ADDRESS
                    $product['shipping']['delivery']['shipping_address'] = $this->model_account_address->getCheckoutAddress(array('customer_id' => $this->customer->getId(), 'address_id' => $product['shipping']['delivery']['shipping_address']));
                    $product['shipping']['delivery']['price_count'] = $product['shipping']['delivery']['price'];
                    $product['shipping']['delivery']['price'] = $this->currency->format($product['shipping']['delivery']['price']);
                    //GET SELLER
                    $product['shipping']['delivery']['seller'] = $this->model_account_seller->getSeller($product['shipping']['delivery']['seller_id']);
                    $product['shipping']['delivery']['seller']['link'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $product['shipping']['delivery']['seller_id']);
                    //GET SHIPPING	
                    $product['shipping']['delivery']['shipping'] = $this->model_shipping_shipping->getShipping($product['shipping']['delivery']['shipping_id']);
                    $product['shipping']['delivery']['shipping_service'] = $this->model_shipping_shipping->getShippingService($product['shipping']['delivery']['shipping_service_id']);
                } else {

                    if ($product['shipping']['pickup']['seller_id'] != $pickup_seller_id || $product['shipping']['pickup']['branch'] != $pickup_pp_branch) {
                        $pickup_seller_id = $product['shipping']['pickup']['seller_id'];
                        $pickup_pp_branch = $product['shipping']['pickup']['branch'];
                        $tempKey = "pickup" . $pickup_seller_id . $pickup_pp_branch;
                    }

                    //GET SELLER
                    $product['shipping']['pickup']['seller'] = $this->model_account_seller->getSeller($product['shipping']['pickup']['seller_id']);
                    $product['shipping']['pickup']['seller']['link'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $product['shipping']['pickup']['seller_id']);
                    $product['shipping']['pickup']['branch'] = $this->model_localisation_pp_branch->getPpBranchDetail($product['shipping']['pickup']['branch']);
                }
            }

            if ($tempKey != $key) {
                $key = $tempKey;
            }
            $data['cart'][$key]['shipping'] = $product['shipping'];

            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
            }

            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            } else {
                $image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            }

            $option_data = array();

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
            } else {
                $total = false;
            }

            $recurring = '';

            if ($product['recurring']) {
                $frequencies = array(
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                );

                if ($product['recurring']['trial']) {
                    $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                }

                if ($product['recurring']['duration']) {
                    $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                } else {
                    $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                }
            }
            $data['cart'][$key]['products'][] = array(
                'key' => $product['key'],
                'thumb' => $image,
                'name' => $product['name'],
                'model' => $product['model'],
                'weight_count' => $product['weight'],
                'weight' => $this->weight->format($product['weight'], 1),
                'option' => $option_data,
                'information' => $product['shipping']['information'],
                'recurring' => $recurring,
                'quantity' => $product['quantity'],
                'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                'price' => $price,
                'total_count' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'],
                'total' => $total,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }

        $data['vouchers'] = $this->model_checkout_checkout->getVouchers();
        $data['totals'] = $this->model_checkout_checkout->getTotals();

        if ($value = $this->model_checkout_checkout->getProperty($this->session->data, 'payment_method.code')) {
            if (Front::IS_OC2) {
                $data['payment'] = $this->load->controller('payment/' . $value);
            } else {
                $data['payment'] = $this->getChild('payment/' . $this->session->data['payment_method']['code']);
            }
        } else {
            $data['payment'] = '';
        }

        if ($return) {
            return $this->renderView('default/template/checkout/cart_journal.tpl', $data);
        } else {
            $this->response->setOutput($this->renderView('default/template/checkout/cart_journal.tpl', $data));
        }
    }

    public function cart_update() {
        $key = $this->model_checkout_checkout->getProperty($this->request->post, 'key');
        $qty = $this->model_checkout_checkout->getProperty($this->request->post, 'quantity');
        $this->cart->update($key, $qty);

        $json = array();

        if (!$this->checkCart()) {
            $json['redirect'] = $this->url->link('checkout/cart');
        } else {
            $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($this->model_checkout_checkout->getTotal()));
        }

        echo json_encode($json);
        exit;
    }

    public function cart_delete() {
        $this->cart->remove($this->model_checkout_checkout->getProperty($this->request->post, 'key'));

        $json = array();

        if (!$this->checkCart()) {
            $json['redirect'] = $this->url->link('checkout/cart');
        } else {
            $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($this->model_checkout_checkout->getTotal()));
        }

        echo json_encode($json);
        exit;
    }

    public function coupon() {
        $json = array();

        $this->load->model('checkout/coupon');

        if (isset($this->request->post['coupon'])) {
            $coupon = $this->request->post['coupon'];
        } else {
            $coupon = '';
        }

        $coupon_info = $this->model_checkout_coupon->getCoupon($coupon);

        if (empty($this->request->post['coupon'])) {
            $json['error'] = $this->language->get('error_coupon');
        } elseif ($coupon_info['result']) {
            $this->session->data['coupon'] = $this->request->post['coupon'];

            $this->session->data['success'] = $this->language->get('text_success');

            $json['redirect'] = $this->url->link('checkout/cart');
        } else {
            $json['error'] = $coupon_info['message'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function voucher() {
        $json = array();

        $this->load->model('checkout/voucher');

        if (isset($this->request->post['voucher'])) {
            $voucher = $this->request->post['voucher'];
        } else {
            $voucher = '';
        }

        $voucher_info = $this->model_checkout_voucher->getVoucher($voucher);

        if (empty($this->request->post['voucher'])) {
            $json['error'] = $this->language->get('error_voucher');
        } elseif ($voucher_info) {
            $this->session->data['voucher'] = $this->request->post['voucher'];

            $this->session->data['success'] = $this->language->get('text_success');

            $json['redirect'] = $this->url->link('checkout/cart');
        } else {
            $json['error'] = $this->language->get('error_voucher');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function reward() {
        $this->load->language('checkout/reward');

        $json = array();

        $points = $this->customer->getRewardPoints();

        $points_total = 0;

        foreach ($this->cart->getProducts() as $product) {
            if ($product['points']) {
                $points_total += $product['points'];
            }
        }

        if (empty($this->request->post['reward'])) {
            $json['error'] = $this->language->get('error_reward');
        }

        if ($this->request->post['reward'] > $points) {
            $json['error'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
        }

        if ($this->request->post['reward'] > $points_total) {
            $json['error'] = sprintf($this->language->get('error_maximum'), $points_total);
        }

        if (!$json) {
            $this->session->data['reward'] = abs($this->request->post['reward']);

            $this->session->data['success'] = $this->language->get('text_success');

            if (isset($this->request->post['redirect'])) {
                $json['redirect'] = $this->url->link($this->request->post['redirect']);
            } else {
                $json['redirect'] = $this->url->link('checkout/cart');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function checkCart() {
        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            return false;
        }

        // Validate minimum quantity requirements.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ((int) $product['minimum'] > (int) $product_total) {
                return false;
            }
        }

        //Check Stock
        /* foreach ($products as $product) {
          $product_total = 0;

          foreach ($products as $product_2) {
          if ($product_2['product_id'] == $product['product_id']) {
          $product_total += $product_2['quantity'];
          }
          }

          if ((int)$product['stock'] < (int)$product_total) {
          return false;
          }
          } */

        return true;
    }

    private function isShippingRequired() {
        return $this->cart->hasShipping();
    }

    private function isLoggedIn() {
        return $this->customer->isLogged();
    }

    private function allowGuestCheckout() {
        return $this->config->get(Front::IS_OC2 ? 'config_checkout_guest' : 'config_guest_checkout') && !$this->config->get('config_customer_price') && !$this->cart->hasDownload();
    }

    /*
      private function renderAddressForm($type, $name = true) {
      $data['type'] = $type;
      $data['name'] = $name;

      $data['text_address_existing'] = $this->language->get('text_address_existing');
      $data['text_address_new'] = $this->language->get('text_address_new');
      $data['text_select'] = $this->language->get('text_select');
      $data['text_none'] = $this->language->get('text_none');

      $data['entry_firstname'] = $this->language->get('entry_firstname');
      $data['entry_lastname'] = $this->language->get('entry_lastname');
      $data['entry_company'] = $this->language->get('entry_company');
      $data['entry_tax_id'] = $this->language->get('entry_tax_id');
      $data['entry_address_1'] = $this->language->get('entry_address_1');
      $data['entry_address_2'] = $this->language->get('entry_address_2');
      $data['entry_postcode'] = $this->language->get('entry_postcode');
      $data['entry_city'] = $this->language->get('entry_city');
      $data['entry_country'] = $this->language->get('entry_country');
      $data['entry_zone'] = $this->language->get('entry_zone');

      $data['custom_fields'] = $this->model_checkout_checkout->getCustomFields($type);
      $data['order_data'] = $this->model_checkout_checkout->getOrder();

      $data['addresses'] = $this->model_account_address->getAddresses();
      $data['countries'] = $this->model_localisation_country->getCountries();

      $address = $this->model_checkout_checkout->getAddress($type);
      foreach ($address as $key => $value) {
      $data[$key] = $value;
      }

      // return $this->renderView($this->config->get('config_template') . '/template/journal2/checkout/address_form.tpl', $data);
      return $this->renderView('default/template/checkout/address_form_journal.tpl', $data);
      } */

    private function renderRegisterForm() {
        $data['text_register'] = $this->language->get('text_register');
        $data['text_guest'] = $this->language->get('text_guest');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');
        $data['text_forgotten'] = $this->language->get('text_forgotten');
        $data['text_loading'] = $this->language->get('text_loading');
        $data['button_login'] = $this->language->get('button_login');
        $data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
        $data['text_returning_customer'] = $this->language->get('text_returning_customer');

        $data['text_your_details'] = $this->language->get('text_your_details');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $data['entry_firstname'] = $this->language->get('entry_firstname');
        $data['entry_lastname'] = $this->language->get('entry_lastname');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_fax'] = $this->language->get('entry_fax');
        $data['text_your_password'] = $this->language->get('text_your_password');
        $data['entry_confirm'] = $this->language->get('entry_confirm');
        $data['text_your_address'] = $this->language->get('text_your_address');
        $data['entry_shipping'] = $this->language->get('entry_shipping');

        $data['customer_groups'] = array();
        $data['customer_group_id'] = $this->model_checkout_checkout->getCustomerGroupId();
        if (is_array($this->config->get('config_customer_group_display'))) {
            $this->load->model('account/customer_group');

            $customer_groups = $this->model_account_customer_group->getCustomerGroups();

            foreach ($customer_groups as $customer_group) {
                if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                    $data['customer_groups'][] = $customer_group;
                }
            }
        }

        /* $data['payment_address_form'] = $this->renderAddressForm('payment', false);
          $data['shipping_address_form'] = $this->renderAddressForm('shipping');
          $data['shipping_address'] = $this->model_checkout_checkout->getProperty($this->session->data, 'journal_checkout_shipping_address', '1');
          $data['is_shipping_required'] = $this->isShippingRequired(); */

        $data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');

        $data['custom_fields'] = $this->model_checkout_checkout->getCustomFields();
        $data['order_data'] = $this->model_checkout_checkout->getOrder();

        // return $this->renderView($this->config->get('config_template') . '/template/journal2/checkout/register.tpl', $data);
        return $this->renderView('default/template/checkout/register_journal.tpl', $data);
    }

    private function renderCouponVoucherReward() {
        $data['text_loading'] = $this->language->get('text_loading');
        $data['text_title_voucher'] = $this->language->get('text_title_voucher');

        $data['coupon_status'] = $this->config->get('coupon_status');
        $data['entry_coupon'] = $this->language->get('entry_coupon');
        $data['button_coupon'] = $this->language->get('button_coupon');
        $data['coupon'] = $this->model_checkout_checkout->getProperty($this->session->data, 'coupon');

        $data['voucher_status'] = $this->config->get('voucher_status');
        $data['entry_voucher'] = $this->language->get('entry_voucher');
        $data['button_voucher'] = $this->language->get('button_voucher');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['voucher'] = $this->model_checkout_checkout->getProperty($this->session->data, 'voucher');

        $points = $this->customer->getRewardPoints();

        $points_total = 0;

        foreach ($this->cart->getProducts() as $product) {
            if ($product['points']) {
                $points_total += $product['points'];
            }
        }

        $data['reward_status'] = $points && $points_total && $this->config->get('reward_status');
        $data['entry_reward'] = $this->language->get('entry_reward');
        $data['button_reward'] = $this->language->get('button_reward');
        $data['reward'] = $this->model_checkout_checkout->getProperty($this->session->data, 'reward');

        // return $this->renderView($this->config->get('config_template') . '/template/journal2/checkout/coupon_voucher_reward.tpl', $data);
        return $this->renderView('default/template/checkout/coupon_voucher_reward_journal.tpl', $data);
    }

    private function getAddressData($array, $key = '', $prefix = '') {
        $keys = array(
            'address_1',
            'address_2',
            'address_id',
            'address_format',
            'city',
            'company',
            'company_id',
            'country',
            'country_id',
            'firstname',
            'lastname',
            'method',
            'postcode',
            'tax_id',
            'zone',
            'zone_id'
        );

        $result = array();

        foreach ($keys as $k) {
            $result[$prefix . $k] = $this->model_checkout_checkout->getProperty($array, $key . $k, '');
        }

        if ($result[$prefix . 'country_id']) {
            $country_info = $this->model_localisation_country->getCountry($result[$prefix . 'country_id']);
            if ($country_info) {
                if (!$result[$prefix . 'country']) {
                    $result[$prefix . 'country'] = $country_info['name'];
                }
                $result[$prefix . 'address_format'] = $country_info['address_format'];
            }
        }

        if (!$result[$prefix . 'zone'] && $result[$prefix . 'zone_id']) {
            $zone_info = $this->model_localisation_zone->getZone($result[$prefix . 'zone_id']);
            if ($zone_info) {
                $result[$prefix . 'zone'] = $zone_info['name'];
            }
        }

        if (Front::IS_OC2) {
            $result[$prefix . 'custom_field'] = $this->model_checkout_checkout->getProperty($array, $key . 'custom_field', array());
        }

        return $result;
    }

    private function validateUserData($data, $register) {
        $errors = array();

        // firstname
        if ((utf8_strlen(trim($data['firstname'])) < 1) || (utf8_strlen(trim($data['firstname'])) > 32)) {
            $errors['firstname'] = $this->language->get('error_firstname');
        }

        // lastname
        if ((utf8_strlen(trim($data['lastname'])) < 1) || (utf8_strlen(trim($data['lastname'])) > 32)) {
            $errors['lastname'] = $this->language->get('error_lastname');
        }

        // email
        if ((utf8_strlen($data['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $data['email'])) {
            $errors['email'] = $this->language->get('error_email');
        } else if ($register && $this->model_account_customer->getTotalCustomersByEmail($data['email'])) {
            $errors['email'] = $this->language->get('error_exists');
        }

        // telephone
        if ((utf8_strlen($data['telephone']) < 3) || (utf8_strlen($data['telephone']) > 32)) {
            $errors['telephone'] = $this->language->get('error_telephone');
        }

        // Custom field validation
        if (Front::IS_OC2) {
            $custom_fields = $this->model_checkout_checkout->getCustomFields();

            foreach ($custom_fields as $custom_field) {
                if (($custom_field['location'] == 'account') && $custom_field['required'] && empty($data['custom_field'][$custom_field['custom_field_id']])) {
                    $errors['custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        return $errors;
    }

    private function validatePassword($data) {
        $errors = array();

        if ((utf8_strlen($data['password']) < 4) || (utf8_strlen($data['password']) > 20)) {
            $errors['password'] = $this->language->get('error_password');
        }

        if ($data['confirm'] != $data['password']) {
            $errors['confirm'] = $this->language->get('error_confirm');
        }

        return $errors;
    }

    private function validateAddressData($data, $key, $name = true) {
        $errors = array();

        if ($name) {
            // firstname
            if ((utf8_strlen(trim($data[$key . 'firstname'])) < 1) || (utf8_strlen(trim($data[$key . 'firstname'])) > 32)) {
                $errors[$key . 'firstname'] = $this->language->get('error_firstname');
            }

            // lastname
            if ((utf8_strlen(trim($data[$key . 'lastname'])) < 1) || (utf8_strlen(trim($data[$key . 'lastname'])) > 32)) {
                $errors[$key . 'lastname'] = $this->language->get('error_lastname');
            }
        }

        if ((utf8_strlen(trim($data[$key . 'address_1'])) < 3) || (utf8_strlen(trim($data[$key . 'address_1'])) > 128)) {
            $errors[$key . 'address_1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen($data[$key . 'city']) < 2) || (utf8_strlen($data[$key . 'city']) > 32)) {
            $errors[$key . 'city'] = $this->language->get('error_city');
        }

        $country_info = $this->model_localisation_country->getCountry($data[$key . 'country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($data[$key . 'postcode'])) < 2 || utf8_strlen(trim($data[$key . 'postcode'])) > 10)) {
            $errors[$key . 'postcode'] = $this->language->get('error_postcode');
        }

        if ($data[$key . 'country_id'] == '') {
            $errors[$key . 'country'] = $this->language->get('error_country');
        }

        if (!isset($data[$key . 'zone_id']) || $data[$key . 'zone_id'] == '') {
            $errors[$key . 'zone'] = $this->language->get('error_zone');
        }

        // Custom field validation
        if (Front::IS_OC2) {
            $custom_fields = $this->model_checkout_checkout->getCustomFields();
            foreach ($custom_fields as $custom_field) {
                if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($data[$key . 'custom_field'][$custom_field['custom_field_id']])) {
                    $errors[$key . 'custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        } else {
            $customer_group = $this->model_account_customer_group->getCustomerGroup($this->model_checkout_checkout->getProperty($this->request->post, 'customer_group_id', $this->model_checkout_checkout->getCustomerGroupId()));

            if ($customer_group) {
                // Company ID
                if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($data[$key . 'company'])) {
                    $errors[$key . 'company'] = $this->language->get('error_company_id');
                }

                // Tax ID
                if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($data[$key . 'tax_id'])) {
                    $errors[$key . 'tax_id'] = $this->language->get('error_tax_id');
                }
            }

            // VAT Validation
            $this->load->helper('vat');

            if ($country_info && $this->config->get('config_vat') && $data[$key . 'tax_id'] && (vat_validation($country_info['iso_code_2'], $data[$key . 'tax_id']) == 'invalid')) {
                $errors[$key . 'tax_id'] = $this->language->get('error_vat');
            }
        }

        return $errors;
    }

    private function registerAccount() {
        $redirect = '';

        $data = $this->getAddressData($this->request->post, 'payment_');

        $data = array_merge($data, array(
            'firstname' => $this->model_checkout_checkout->getProperty($this->request->post, 'firstname'),
            'lastname' => $this->model_checkout_checkout->getProperty($this->request->post, 'lastname'),
            'customer_group_id' => $this->model_checkout_checkout->getProperty($this->request->post, 'customer_group_id', $this->config->get('config_customer_group_id')),
            'custom_field' => array(
                'account' => $this->model_checkout_checkout->getProperty($this->request->post, 'custom_field'),
                'address' => $this->model_checkout_checkout->getProperty($this->request->post, 'payment_custom_field'),
            ),
            'email' => $this->model_checkout_checkout->getProperty($this->request->post, 'email'),
            'telephone' => $this->model_checkout_checkout->getProperty($this->request->post, 'telephone'),
            'fax' => $this->model_checkout_checkout->getProperty($this->request->post, 'fax'),
            'password' => $this->model_checkout_checkout->getProperty($this->request->post, 'password'),
            'newsletter' => $this->model_checkout_checkout->getProperty($this->request->post, 'newsletter'),
            'hash' => md5(rand(0, 1000) . time()),
            'code' => $this->generate_random_password(5)
                ));

        $customer_id = $this->model_account_customer->addCustomer($data);

        // Clear any previous login attempts for unregistered accounts.
        if (Front::IS_OC2) {
            $this->model_account_customer->deleteLoginAttempts($data['email']);
        }

        $this->session->data['account'] = 'register';

        $customer_group_info = $this->model_account_customer_group->getCustomerGroup($this->model_checkout_checkout->getProperty($this->request->post, 'customer_group_id', $this->config->get('config_customer_group_id')));

        if ($customer_group_info && !$customer_group_info['approval']) {
            $this->customer->login($data['email'], $data['password']);

            if ($this->model_checkout_checkout->getProperty($this->request->post, 'shipping_address') != '1') {
                $this->model_account_address->addAddress($this->getAddressData($this->request->post, 'shipping_'));
            }

            // Add to activity log
            $activity_data = array(
                'customer_id' => $customer_id,
                'name' => $data['firstname'] . ' ' . $data['lastname']
            );

            if (Front::IS_OC2) {
                $this->model_account_activity->addActivity('register', $activity_data);
            }
        } else {
            $redirect = $this->url->link('account/success');
        }

        return $redirect;
    }

    public function login() {
        $this->load->language('checkout/checkout');

        $json = array();

        if ($this->customer->isLogged()) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
        }

        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        if (Front::IS_OC2) {
            if (!$json) {
                $this->load->model('account/customer');

                // Check how many login attempts have been made.
                $login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

                if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
                    $json['error']['warning'] = $this->language->get('error_attempts');
                }

                // Check if customer has been approved.
                $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

                if ($customer_info && !$customer_info['approved']) {
                    $json['error']['warning'] = $this->language->get('error_approved');
                }

                if (!isset($json['error'])) {
                    if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                        $json['error']['warning'] = $this->language->get('error_login');

                        $this->model_account_customer->addLoginAttempt($this->request->post['email']);
                    } else {
                        $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
                    }
                }
            }

            if (!$json) {
                unset($this->session->data['guest']);

                $this->load->model('account/address');

                $address_info = $this->model_account_address->getAddress($this->customer->getAddressId());

                if ($this->config->get('config_tax_customer') == 'payment') {
                    $this->session->data['payment_address'] = $address_info;
                }

                if ($this->config->get('config_tax_customer') == 'shipping') {
                    $this->session->data['shipping_address'] = $address_info;
                }

                $this->model_checkout_checkout->setAddress('shipping', $address_info);
                $this->model_checkout_checkout->setAddress('payment', $address_info);
                $this->model_checkout_checkout->save();

                $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');

                // Add to activity log
                $this->load->model('account/activity');

                $activity_data = array(
                    'customer_id' => $this->customer->getId(),
                    'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
                );

                $this->model_account_activity->addActivity('login', $activity_data);
            }
        } else {
            if (!$json) {
                if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                    $json['error']['warning'] = $this->language->get('error_login');
                }

                $this->load->model('account/customer');

                $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

                if ($customer_info && !$customer_info['approved']) {
                    $json['error']['warning'] = $this->language->get('error_approved');
                }
            }

            if (!$json) {
                unset($this->session->data['guest']);

                // Default Addresses
                $this->load->model('account/address');

                $address_info = $this->model_account_address->getAddress($this->customer->getAddressId());

                if ($address_info) {
                    if ($this->config->get('config_tax_customer') == 'shipping') {
                        $this->session->data['shipping_country_id'] = $address_info['country_id'];
                        $this->session->data['shipping_zone_id'] = $address_info['zone_id'];
                        $this->session->data['shipping_postcode'] = $address_info['postcode'];
                    }

                    if ($this->config->get('config_tax_customer') == 'payment') {
                        $this->session->data['payment_country_id'] = $address_info['country_id'];
                        $this->session->data['payment_zone_id'] = $address_info['zone_id'];
                    }
                } else {
                    unset($this->session->data['shipping_country_id']);
                    unset($this->session->data['shipping_zone_id']);
                    unset($this->session->data['shipping_postcode']);
                    unset($this->session->data['payment_country_id']);
                    unset($this->session->data['payment_zone_id']);
                }

                $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function generate_random_password($length = 10) {
        $alphabets = range('A', 'Z');
        $numbers = range('0', '9');
        $additional_characters = array('_', '.');
        $final_array = array_merge($alphabets, $numbers);

        $password = '';

        while ($length--) {
            $key = array_rand($final_array);
            $password .= $final_array[$key];
        }

        return $password;
    }

}
