<?php

class ControllerPaymentSgopayment extends Controller {
	
	public function index() {
		$this->language->load('payment/sgopayment');
		
		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');
		
		//$this->document->addScript('https://secure.sgo.co.id/public/signature/js');
		$data['button_confirm'] = $this->language->get('button_confirm');
		
		$data['bank'] = nl2br($this->config->get('sgopayment_bank_' . $this->config->get('config_language_id')))."asdarea";

		$data["sgopaymentid"] = $this->config->get('sgopayment_id');
		$data["order_id"] = $this->session->data['order_id'];
		$data["total"] = $this->cart->getTotal();
		$data["back_url"] =  urlencode($this->url->link('payment/sgopayment/success')."&order_id=".$this->session->data['order_id']);
 		
		
		//$data['dir_image'] =  HTT."sgopayment/";
		
		if (empty($_SERVER["https"])){
			$data['dir_js'] =  HTTPS_SERVER."catalog/view/javascript/";
			$data['dir_image'] =  HTTPS_SERVER."catalog/view/theme/default/image/sgopayment/";
		}else{
			$data['dir_image'] =  HTTPS_SERVER."catalog/view/theme/default/image/sgopayment/";
			$data['dir_js'] =  HTTPS_SERVER."catalog/view/javascript/";
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/sgopayment.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/sgopayment.tpl';
		} else {
			$this->template = 'default/template/payment/sgopayment.tpl';
		}
		return $this->load->view($this->template, $data);
	}
	public function complete(){
		$product = $this->request->post['product'];
		if(!empty($product) && $product!=NULL && $product!='' && isset($this->session->data['order_id'])){
			$this->language->load('payment/sgopayment');
			
			$this->load->model('checkout/order');
			
			$order_id = $this->session->data['order_id'];
			
			if($product=='013:PERMATAATM'){
				$comment  = $this->language->get('text_instruction') . "\n\n";
				$comment .= $this->config->get('sgopayment_bank_' . $this->config->get('config_language_id')) . "\n\n";
				$comment .= $this->language->get('text_payment');
				
				$order=$this->model_checkout_order->getOrder($order_id);
				
				if(!empty($order) && (int)$order['order_status_id']==0){
					$this->model_checkout_order->createInvoiceNoDetail($order_id);
					
					$this->model_checkout_order->updatePaymentBank($order_id,$product);
							
					$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'), $comment, true);
				}
			}
			
			if($product=='008:MANDIRIIB'){
				$this->model_checkout_order->updatePaymentBank($order_id,$product);
			}
			
			if($product=='013:PERMATANETPAY'){
				$this->model_checkout_order->updatePaymentBank($order_id,$product);
			}
		}
	}
	public function confirm() {
		$method = $_SERVER['REQUEST_METHOD'];
		$this->load->model('checkout/order');
		$this->language->load('payment/sgopayment');
		
		if ($method == "POST"){
			header('HTTP/1.1 200 OK');
			$member_id =  $this->request->post["member_id"];
			$order_id = $this->request->post["order_id"];
			$password = $this->request->post["password"];
			$debit_from = $this->request->post["debit_from"];
			$credit_to = $this->request->post["credit_to"];
			
			//validate password
			if ($password == $this->config->get("sgopayment_password")){
				if (!$this->model_checkout_order->getOrder($order_id)){//check order id exist
					echo '1,Order Id Does Not Exist,,,';
					
					$this->model_checkout_order->addOnlinePaymentHistory(array(
						"code"=>"success_notification",
						"value"=>'1,Order Id Does Not Exist,,,'
					));
					
				}else {
				
					$comment = "";
					$comment .= $this->language->get('text_transfer')."\n\n";
					$comment .= $this->language->get('text_transfer_from')." ".$credit_to."\n\n";
					$comment .= $this->language->get('text_transfer_to')." ".$debit_from."\n\n";
					
					$reconsile_id = $member_id . " - " . $order_id .date('YmdHis');
					$value = '0,Success,'.$reconsile_id.','.$order_id.','.date('Y-m-d H:i:s').'';
					echo $value;
					
					$this->model_checkout_order->addOnlinePaymentHistory(array(
						"code"=>"success_notification",
						"value"=>$value
					));
					
					//save the order id that already pay
					
					$order=$this->model_checkout_order->getOrder($order_id);
					
					if($order['payment_bank']!="013:PERMATAATM"){
						$this->model_checkout_order->createInvoiceNoDetail($order_id);
					
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'), $comment, true);
					}
					$this->model_checkout_order->confirmSGOPayment($order_id,2,$order['customer_id']);
					
					$this->model_checkout_order->verifySGOPayment($order_id,$this->config->get('sgopayment_order_status_id'),$order['customer_id']);
					
				}
			}else{
				//
				echo '1,Password does not match,,,';
				$this->model_checkout_order->addOnlinePaymentHistory(array(
					"code"=>"success_notification",
					"value"=>'1,Password does not match,,,'
				));
			}
				
		}else {
			header('HTTP/1.1 404 Not Found');
			$this->model_checkout_order->addOnlinePaymentHistory(array(
				"code"=>"success_notification",
				"value"=>'HTTP/1.1 404 Not Found'
			));
		}
	}
	
	public function success(){
		$this->load->language('payment/sgopayment');

		$this->load->model('checkout/order');
		$product = $this->request->get['product'];
		$order=$this->model_checkout_order->getOrder($this->session->data['order_id']);
		if (!empty($order) && $order['payment_code'] == 'sgopayment') {
			if ($order['payment_method']=="Online Payment" && $order['payment_bank']=="013:PERMATAATM" && (int)$order['order_status_id']==1){
				$this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
			}else if((int)$order['order_status_id']==3){
				$this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
			}else if((int)$order['order_status_id']==0){
				if(isset($this->session->data['second_order_id'])){
					$this->session->data['error']="Failed to process Online Payment";
				}else{
					$this->session->data['error']="Failed to process Online Payment";
				}
				$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
			}else{
				$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}
		}else{
			$this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}
	}
	
	public function inquirytrx(){
		
		$method = $_SERVER['REQUEST_METHOD'];
		$this->load->model('checkout/order');
		$this->load->model('account/customer');
		if ($method == "POST"){
			header('HTTP/1.1 200 OK');
			$order_id = $this->request->post["order_id"];//get the order_id
			$password = $this->request->post["password"];//get the password from sgo
			
			//validate the password
			if ($password == $this->config->get("sgopayment_password")){
				
				//validate order id
				$order = $this->model_checkout_order->getOrder($order_id);
				
				if (empty($order) || ($order['payment_method']=="Online Payment" && $order['payment_bank']=="013:PERMATAATM" && (int)$order['order_status_id']==0) || ((int)$order['order_status_id']!=0 && $order['payment_method']!="Online Payment")){
					echo '1;Order Id Does Not Exist;;;;;';// if order id not exist show plain reponse
				}else {
					//if order id truly exist get order detail from database 
					$order_detail = $this->model_checkout_order->getOrder($order_id);
					$order_total = $this->model_checkout_order->getOrderTotal($order_id);
					$customer = $this->model_account_customer->getCustomer($order_detail['customer_id']);
					if($order_total>0){
						//0;Success;0123162509216347301;50000.00;IDR;XL10=3,XL50=2,XL100=1; 20/01/2010 01:01:01
						$value='0;Success;'.$order_id.';'.str_replace('.', '', $order_total).'.00;'.$order_detail["currency_code"].'; Pembayaran Order '.$order_id.' oleh '.$customer["lastname"].' '.$customer["firstname"].';'.date('d/m/Y H:i:s').'';
						echo $value;
						$this->model_checkout_order->addOnlinePaymentHistory(array(
							"code"=>"inquiry",
							"value"=>$value
						));
					}else{
						echo '1;Total amount 0;;;;;';// if order id not exist show plain reponse
						$this->model_checkout_order->addOnlinePaymentHistory(array(
							"code"=>"inquiry",
							"value"=>'1;Total amount 0;;;;;'
						));
					}
				}
			}else {
				// if password not true
				echo '1;Merchant Failed to Identified;;;;;';
				$this->model_checkout_order->addOnlinePaymentHistory(array(
					"code"=>"inquiry",
					"value"=>'1;Merchant Failed to Identified;;;;;'
				));
			}
		}else {
			//if request not post
			header('HTTP/1.1 404 Not Found');
			$this->model_checkout_order->addOnlinePaymentHistory(array(
				"code"=>"inquiry",
				"value"=>'HTTP/1.1 404 Not Found'
			));
		}
		
	}
	
	/*public function reportpayment(){
		$method = $_SERVER['REQUEST_METHOD'];
		$this->load->model('checkout/order');
		$this->language->load('payment/sgopayment');
		
		if ($method == "POST"){
			header('HTTP/1.1 200 OK');
			// get all the data that sent by sgo
			$member_id =  $this->request->post["member_id"];
			$order_id = $this->request->post["order_id"];
			$password = $this->request->post["password"];
			$debit_from = $this->request->post["debit_from"];
			$credit_to = $this->request->post["credit_to"];
			$product = $this->request->post["product_code"];
			
			//validate password
			if ($password == $this->config->get("sgopayment_password")){
				if (!$this->model_checkout_order->getOrder($order_id)){//check order id exist
					// if order id not exist
					echo '1,Order Id Does Not Exist,,,';
				}else {
					//get order detail
					$order_detail = $this->model_checkout_order->getOrder($order_id);
					
					$comment = "";
					$comment .= $this->language->get('text_transfer')."\n\n";
					$comment .= $this->language->get('text_transfer_from')." ".$credit_to."\n\n";
					$comment .= $this->language->get('text_transfer_to')." ".$debit_from."\n\n";
					$comment .= $this->language->get('text_transfer_product')." ".$product."\n\n";
					
					$reconsile_id = $member_id . " - " . $order_id .date('YmdHis');
					echo '0,Success,'.$reconsile_id.','.$order_id.','.date('Y-m-d H:i:s').'';
					//save the order id that already pay
					$this->model_checkout_order->confirm($order_id, $this->config->get('sgopayment_order_status_id'), $comment, true);
				}
			}else{
				//
				echo '1,Password does not match,,,';
			}
				
		}else {
			header('HTTP/1.1 404 Not Found');
		}
				
	}*/
}
