
<?php
class ControllerPaymentEmployeeProgram extends Controller {
	public function index() {
		$this->load->language('payment/employee_program');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('employee_program_bank' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/employee_program.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/employee_program.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/employee_program.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'employee_program' && in_array($this->customer->getGroupId(),$this->config->get('employee_program_customer_group'))) {
			$this->load->language('payment/employee_program');

			$this->load->model('checkout/order');
			
			$order=$this->model_checkout_order->getOrder($this->session->data['order_id']);
			if((int)$order['total']>0){
				$this->model_checkout_order->createInvoiceNoDetail($this->session->data['order_id']);

				$comment  = $this->language->get('text_instruction') . "\n\n";
				$comment .= $this->config->get('employee_program_bank' . $this->config->get('config_language_id')) . "\n\n";
				$comment .= $this->language->get('text_payment');

				$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('config_order_status_id'), $comment, true);
				
				$this->model_checkout_order->confirmEmployeeProgram($this->session->data['order_id'],$this->config->get('employee_program_order_status_id'));
			}
		}
	}
}