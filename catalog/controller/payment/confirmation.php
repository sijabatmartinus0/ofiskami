<?php
class ControllerPaymentConfirmation extends Controller {
	private $data=array();
	private $error = array();
	
	public function index() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->language('payment/confirmation');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
		
		$this->load->model('localisation/account_destination');
		$this->load->model('localisation/account');
		$this->load->model('payment/confirmation');
		$this->load->model('checkout/order');
		$this->load->model('account/order');
		
		/*$order=$this->model_account_order->getOrder($order_id);

		if (!empty($order) && $order['payment_method']=='Online Payment') {
			$this->session->data['error'] = $this->language->get('error_confirm');
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
		}
		
		if(empty($order)){
			$this->session->data['error'] = $this->language->get('error_confirm');
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
		}*/
		
		
		$data['account_destinations'] = $this->model_localisation_account_destination->getAccountDestinations();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_payment_confirmation->insertPaymentConfirmation($this->request->post);
			$this->model_checkout_order->addOrderHistory($order_id, '2', '', true);
			$this->response->redirect($this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL'));
			
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_order'),
			'href' => $this->url->link('account/order', '', 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL')
		);
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_total_payment'] = $this->language->get('text_total_payment');
		$data['text_date_payment'] = $this->language->get('text_date_payment');
		$data['text_method_payment'] = $this->language->get('text_method_payment');
		$data['text_bank_name'] = $this->language->get('text_bank_name');
		$data['text_account_name'] = $this->language->get('text_account_name');
		$data['text_account_number'] = $this->language->get('text_account_number');
		$data['text_branch'] = $this->language->get('text_branch');
		$data['text_account_destination'] = $this->language->get('text_account_destination');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['text_remarks'] = $this->language->get('text_remarks');
		$data['text_add_bank'] = $this->language->get('text_add_bank');
		$data['text_upload'] = $this->language->get('text_upload');
		
		$data['error_date_payment'] = $this->language->get('error_date_payment');
		$data['error_account_id'] = $this->language->get('error_account_id');
		$data['error_account_name'] = $this->language->get('error_account_name');
		$data['error_account_number'] = $this->language->get('error_account_number');
		$data['error_account_number_int'] = $this->language->get('error_account_number_int');
		$data['error_branch'] = $this->language->get('error_branch');
		$data['error_account_destination'] = $this->language->get('error_account_destination');
		$data['error_amount_int'] = $this->language->get('error_amount_int');
		$data['error_picture_confirmation'] = $this->language->get('error_picture_confirmation');
		
		$data['note_branch'] = $this->language->get('note_branch');
		$data['note_amount'] = $this->language->get('note_amount');
		$data['note_product'] = $this->language->get('note_product');
		$data['note_upload'] = $this->language->get('note_upload');
		$data['note_optional'] = $this->language->get('note_optional');
		
		$data['button_yes'] = $this->language->get('button_yes');
		$data['button_no'] = $this->language->get('button_no');
		$data['button_bank'] = $this->language->get('button_bank');
		
		$data['acc_destination'] = $this->language->get('acc_destination');
		
		$data['column_bank_name'] = $this->language->get('column_bank_name');
		$data['column_bank_code'] = $this->language->get('column_bank_code');
		$data['column_search'] = $this->language->get('column_search');
		$data['column_close'] = $this->language->get('column_close');
		
		/*pagination*/
		$accounts_per_page = 10;
		if ((int)$accounts_per_page == 0)
			$accounts_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$data['accounts'] = $this->model_localisation_account->getAccounts(
			// array(
				// 'account_id' => $account_id,
				// 'name' => $name,
				// 'code' => $code
			// ),
			array(
				'order_by' => 'account_id',
				'order_way' => 'ASC',
				'offset' => ($page - 1) * $accounts_per_page,
				'limit' => $accounts_per_page
			)
		);
		
		$total_comments = $this->model_localisation_account->getTotalAccounts(
			// array(
				// 'account_id' => $account_id,
			// )
		);
		$pagination = new Pagination();
		$pagination->total = $total_comments;
		$pagination->page = $page;
		$pagination->limit = $accounts_per_page; 
		$pagination->url = $this->url->link('payment/confirmation/paginationAccount', '&page={page}');
		$data['pagination'] = $pagination->render();
		
		/*------------------*/
		
		/*validation*/
		
		if (isset($this->error['date_payment'])) {
			$data['error_date_payment'] = $this->error['date_payment'];
		} else {
			$data['error_date_payment'] = '';
		}
		
		if (isset($this->error['account_id'])) {
			$data['error_account_id'] = $this->error['account_id'];
		} else {
			$data['error_account_id'] = '';
		}
		
		if (isset($this->error['account_name'])) {
			$data['error_account_name'] = $this->error['account_name'];
		} else {
			$data['error_account_name'] = '';
		}
		
		if (isset($this->error['account_number'])) {
			$data['error_account_number'] = $this->error['account_number'];
		} else {
			$data['error_account_number'] = '';
		}
		
		// if (isset($this->error['account_number_int'])) {
			// $data['error_account_number'] = $this->error['account_number_int'];
		// } else {
			// $data['error_account_number'] = '';
		// }
		
		if (isset($this->error['branch'])) {
			$data['error_branch'] = $this->error['branch'];
		} else {
			$data['error_branch'] = '';
		}
		
		if (isset($this->error['account_destination'])) {
			$data['error_account_destination'] = $this->error['account_destination'];
		} else {
			$data['error_account_destination'] = '';
		}
		
		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}
		
		// if (isset($this->error['amount_int'])) {
			// $data['error_amount'] = $this->error['amount_int'];
		// } else {
			// $data['error_amount'] = '';
		// }
		
		// if (isset($this->error['amount_match'])) {
			// $data['error_amount'] = $this->error['amount_match'];
		// } else {
			// $data['error_amount'] = '';
		// }
		
		// if (isset($this->error['picture_confirmation'])) {
			// $data['error_picture_confirmation'] = $this->error['picture_confirmation'];
		// } else {
			// $data['error_picture_confirmation'] = '';
		// }
		
		/*-----------------*/
		
		if (isset($this->error['picture_confirmation'])) {
			$data['error_picture_confirmations'] = $this->error['picture_confirmation'];
		} else {
			$data['error_picture_confirmations'] = '';
		}
		
		/*insert confirm payment*/
		$data['action'] = $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL');
		
		if (isset($this->request->post['input_order_id'])) {
			$data['input_order_id'] = $this->request->post['input_order_id'];
		} else {
			$data['input_order_id'] = '';
		}
		
		if (isset($this->request->post['input_date_payment'])) {
			$data['input_date_payment'] = $this->request->post['input_date_payment'];
		} else {
			$data['input_date_payment'] = '';
		}

		if (isset($this->request->post['input_method_payment'])) {
			$data['input_method_payment'] = $this->request->post['input_method_payment'];
		} else {
			$data['input_method_payment'] = '';
		}

		if (isset($this->request->post['id-account-bank'])) {
			$data['id-account-bank'] = $this->request->post['id-account-bank'];
		} else {
			$data['id-account-bank'] = '';
		}
		
		if (isset($this->request->post['input_account_name'])) {
			$data['input_account_name'] = $this->request->post['input_account_name'];
		} else {
			$data['input_account_name'] = '';
		}
		
		if (isset($this->request->post['input_account_number'])) {
			$data['input_account_number'] = $this->request->post['input_account_number'];
		} else {
			$data['input_account_number'] = '';
		}
		
		if (isset($this->request->post['input_branch'])) {
			$data['input_branch'] = $this->request->post['input_branch'];
		} else {
			$data['input_branch'] = '';
		}
		
		if (isset($this->request->post['input_account_destination'])) {
			$data['input_account_destination'] = $this->request->post['input_account_destination'];
		} else {
			$data['input_account_destination'] = '';
		}
		
		if (isset($this->request->post['input_amount'])) {
			$data['input_amount'] = $this->request->post['input_amount'];
		} else {
			$data['input_amount'] = '';
		}
		
		if (isset($this->request->post['picture_confirmation'])) {
			$data['picture_confirmation'] = $this->request->post['picture_confirmation'];
		} else {
			$data['picture_confirmation'] = '';
		}
		
		if (isset($this->request->post['input_remarks'])) {
			$data['input_remarks'] = $this->request->post['input_remarks'];
		} else {
			$data['input_remarks'] = '';
		}
		
		/*------------------*/
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/confirmation.tpl')) {
			//return $this->load->view($this->config->get('config_template') . '/template/payment/cod.tpl', $data);
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/confirmation.tpl', $data));
		} else {
			//return $this->load->view('default/template/payment/cod.tpl', $data);
			$this->response->setOutput($this->load->view('default/template/payment/confirmation.tpl', $data));
		}
	}
	
	public function validate() {
		if ((utf8_strlen(trim($this->request->post['input_date_payment'])) != 10)) {
			$this->error['date_payment'] = $this->language->get('error_date_payment');
		}

		if ((utf8_strlen(trim($this->request->post['id-account-bank'])) < 1) ) {
			$this->error['account_id'] = $this->language->get('error_account_id');
		}

		if ((utf8_strlen($this->request->post['input_account_name']) < 1) ) {
			$this->error['account_name'] = $this->language->get('error_account_name');
		}

		if ((utf8_strlen($this->request->post['input_account_number']) < 1 || !preg_match('/^[0-9]*$/', $this->request->post['input_account_number']))) {
			$this->error['account_number'] = $this->language->get('error_account_number');
		}
		
		if ((utf8_strlen($this->request->post['input_branch']) < 1)) {
			$this->error['branch'] = $this->language->get('error_branch');
		}
		
		if ((utf8_strlen($this->request->post['input_account_destination']) < 1) ) {
			$this->error['account_destination'] = $this->language->get('error_account_destination');
		}
		
		if ((utf8_strlen($this->request->post['input_amount']) < 1) || (floatval($this->request->post['input_amount']) != floatval($order_info['total'])) || !preg_match('/^[0-9]*$/', $this->request->post['input_amount'])) {
			$this->error['amount'] = $this->language->get('error_amount');
		}
		
		if ($_FILES["picture_confirmation"]["size"] > 1000000) {
			$this->error['picture_confirmation'] = $this->language->get('error_picture_confirmation_size');
		}
		
		$ext=pathinfo($_FILES["picture_confirmation"]["name"],PATHINFO_EXTENSION);
		if($ext!="PNG" && $ext=="png" && $ext=="JPG" && $ext=="jpg" && $ext=="JPEG" && $ext=="jpeg"){
			$this->error['picture_confirmation'] = $this->language->get('error_picture_confirmation_ext');
		}
		
		return !$this->error;
	}
	
}