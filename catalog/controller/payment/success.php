<?php
class ControllerPaymentSuccess extends Controller {
	public function index() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
			
		}
		
		$this->load->language('payment/success');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_order'),
			'href' => $this->url->link('account/order', '', 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL')
		);
		
		$this->load->model('payment/confirmation');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_first'] = $this->language->get('text_first');
		$data['text_second'] = $this->language->get('text_second');
		$data['text_third'] = $this->language->get('text_third');
		$data['text_fourth'] = sprintf($this->language->get('text_fourth'), $this->url->link('account/order'));
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/success.tpl')) {
				//return $this->load->view($this->config->get('config_template') . '/template/payment/cod.tpl', $data);
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/success.tpl', $data));
			} else {
				//return $this->load->view('default/template/payment/cod.tpl', $data);
				$this->response->setOutput($this->load->view('default/template/payment/success.tpl', $data));
			}
	}
}