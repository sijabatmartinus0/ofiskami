<?php
class ControllerPickupAccountDashboard extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		if (!$this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}
		
		$this->load->model('pickup/order');
		
		$this->load->language('pickup/account-dashboard');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('pickup/account-dashboard')
		);

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['pp_heading_nav'] = $this->language->get('pp_heading_nav');
		$data['pp_account_dashboard_nav'] = $this->language->get('pp_account_dashboard_nav');
		$data['pp_account_history_nav'] = $this->language->get('pp_account_history_nav');
		$data['pp_change_password'] = $this->language->get('pp_change_password');
		
		$data['text_search']=$this->language->get('text_search');
		$data['text_no_item']=$this->language->get('text_no_item');
		$data['text_md_edit_order']=$this->language->get('text_md_edit_order');
		
		$data['button_receive']=$this->language->get('button_receive');
		$data['button_pickup']=$this->language->get('button_pickup');
		$data['button_submit']=$this->language->get('button_submit');
		$data['button_cancel']=$this->language->get('button_cancel');
		
		$data['label_column_action']=$this->language->get('label_column_action');
		$data['label_column_status']=$this->language->get('label_column_status');
		$data['label_column_telephone']=$this->language->get('label_column_telephone');
		$data['label_column_recipient']=$this->language->get('label_column_recipient');
		$data['label_column_customer']=$this->language->get('label_column_customer');
		$data['label_column_order']=$this->language->get('label_column_order');
		
		$data['action']=$this->url->link('pickup/account-dashboard');
		
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$key = (isset($this->request->post['search'])) ? $this->request->post['search'] : '';
		
		$data['search']=$key;
		
		$order_per_page = $this->config->get('pp_order_perpage');
		if ((int)$order_per_page == 0)
			$order_per_page = 10;
		
		
		$data['orders'] = $this->model_pickup_order->getOrders(
			array(
				'key' => $key
			),
			array(
				'order_by' => 'data.date_added',
				'order_way' => 'ASC',
				'offset' => ($page - 1) * $order_per_page,
				'limit' => $order_per_page
			)
		);
		
		$total_orders = $this->model_pickup_order->getTotalOrders();
		
		$pagination = new Pagination();
		$pagination->total = $total_orders;
		$pagination->page = $page;
		$pagination->limit = $order_per_page; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('pickup/account-dashboard', '&page={page}');
		$data['pagination'] = $pagination->render();
		
		if(isset($this->request->get['success']) && $this->request->get['success']=='success'){
			$data['success']=$this->language->get('text_success');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/pickup/account-dashboard.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/pickup/account-dashboard.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/pickup/account-dashboard.tpl', $data));
		}
	}
	
	public function loadReceiveOrder(){
		if ($this->customer->isLogged() && $this->pickup->isPickupPoint()) {
			$order_detail_id = (isset($this->request->post['id'])) ? $this->request->post['id'] : 0;
		
			$data=array();
			
			$this->load->model('pickup/order');
			$this->load->model('account/order');
			
			$this->load->language('pickup/account-dashboard');
			
			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			
			$data['text_order_pickup'] = $this->language->get('text_order_pickup');
			$data['text_order_name'] = $this->language->get('text_order_name');
			$data['text_order_email'] = $this->language->get('text_order_email');
			$data['text_order_phone'] = $this->language->get('text_order_phone');
			$data['text_order_product'] = $this->language->get('text_order_product');
			$data['text_order_product_name'] = $this->language->get('text_order_product_name');
			$data['text_order_product_model'] = $this->language->get('text_order_product_model');
			$data['text_order_product_option'] = $this->language->get('text_order_product_option');
			
			
			$data['products'] = array();
		
			$data['order']=$this->model_pickup_order->getOrder($order_detail_id);
			
			$products = $this->model_pickup_order->getOrderProducts($order_detail_id);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($data['order']['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}
				
				//edit by arin
				$this->load->model('tool/image');
				$this->load->model('catalog/product');
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
				
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}


				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					//edit by arin
					'thumb'	   => $image,
					'href'	   => $this->url->link('product/product', 'product_id=' . $product['product_id'])					
				);
			}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/pickup/account-dashboard-receive.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/pickup/account-dashboard-receive.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/pickup/account-dashboard-receive.tpl', $data));
			}
		}
	}
	
	public function loadPickupOrder(){
		if ($this->customer->isLogged() && $this->pickup->isPickupPoint()) {
			$order_detail_id = (isset($this->request->post['id'])) ? $this->request->post['id'] : 0;
		
			$data=array();
			
			$this->load->model('pickup/order');
			$this->load->model('account/order');
			
			$this->load->language('pickup/account-dashboard');
			
			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			
			$data['text_order_pickup'] = $this->language->get('text_order_pickup');
			$data['text_order_name'] = $this->language->get('text_order_name');
			$data['text_order_email'] = $this->language->get('text_order_email');
			$data['text_order_phone'] = $this->language->get('text_order_phone');
			$data['text_order_product'] = $this->language->get('text_order_product');
			$data['text_order_product_name'] = $this->language->get('text_order_product_name');
			$data['text_order_product_model'] = $this->language->get('text_order_product_model');
			$data['text_order_product_option'] = $this->language->get('text_order_product_option');
			
			
			$data['products'] = array();
		
			$data['order']=$this->model_pickup_order->getOrder($order_detail_id);
			
			$products = $this->model_pickup_order->getOrderProducts($order_detail_id);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($data['order']['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}
				
				//edit by arin
				$this->load->model('tool/image');
				$this->load->model('catalog/product');
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
				
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}


				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					//edit by arin
					'thumb'	   => $image,
					'href'	   => $this->url->link('product/product', 'product_id=' . $product['product_id'])					
				);
			}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/pickup/account-dashboard-pickup.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/pickup/account-dashboard-pickup.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/pickup/account-dashboard-pickup.tpl', $data));
			}
		}
	}
	
	public function pickup(){
		$json=array();
		
		$this->load->model('pickup/order');

		$order_detail_id = (isset($this->request->post['order_detail_id'])) ? $this->request->post['order_detail_id'] : 0;
		
		$result=$this->model_pickup_order->getUpdatePickup($order_detail_id);
		
		if($result){
			$this->model_pickup_order->updatePickup($order_detail_id);

			$json['success']['url']=html_entity_decode($this->url->link('pickup/account-dashboard', 'success=success'));
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function receive(){
		$json=array();
		
		$this->load->model('pickup/order');

		$order_detail_id = (isset($this->request->post['order_detail_id'])) ? $this->request->post['order_detail_id'] : 0;
		
		$result=$this->model_pickup_order->getUpdateReceive($order_detail_id);
		
		if($result){
			$this->model_pickup_order->updateReceive($order_detail_id);

			$json['success']['url']=html_entity_decode($this->url->link('pickup/account-dashboard', 'success=success'));
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}

?>
