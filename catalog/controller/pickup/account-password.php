<?php
class ControllerPickupAccountPassword extends Controller {
	public function index() {
			$this->load->language('pickup/password');
			$this->document->setTitle($this->language->get('heading_title'));

			$data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
				array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', 'SSL'),
				),
				array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('pickup/account-password', '', 'SSL')
				)
			));
			

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_password'] = $this->language->get('text_password');

			$data['pp_heading_nav'] = $this->language->get('pp_heading_nav');
			$data['pp_account_dashboard_nav'] = $this->language->get('pp_account_dashboard_nav');
			$data['pp_account_history_nav'] = $this->language->get('pp_account_history_nav');
			$data['pp_change_password'] = $this->language->get('pp_change_password');
			
			$data['entry_password'] = $this->language->get('entry_password');
			$data['entry_confirm'] = $this->language->get('entry_confirm');
			$data['entry_old_pass'] = $this->language->get('entry_old_pass');
			
			$data['button_submit'] = $this->language->get('button_submit');

			if (isset($this->request->post['old_password'])) {
				$data['old_password'] = $this->request->post['old_password'];
			} else {
				$data['old_password'] = '';
			}
			
			if (isset($this->request->post['password'])) {
				$data['password'] = $this->request->post['password'];
			} else {
				$data['password'] = '';
			}

			if (isset($this->request->post['confirm'])) {
				$data['confirm'] = $this->request->post['confirm'];
			} else {
				$data['confirm'] = '';
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/pickup/account-password.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/pickup/account-password.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/pickup/account-password.tpl', $data));
			}
	}
	
	public function jxSubmit(){
		$this->load->language('pickup/password');
		$data = $this->request->post;
		$this->load->model('account/customer');
		
		$json = array();
		if ((utf8_strlen($data['password']) < 4) || (utf8_strlen($data['password']) > 20)) {
			$json['errors']['password'] = $this->language->get('error_password');
		}

		if ($data['confirm'] != $data['password']) {
			$json['errors']['confirm'] = $this->language->get('error_confirm');
		}
		
		$validate_old_pass = $this->model_account_customer->validateOldPassword($data['old_password']);
		$validate_same_pass = $this->model_account_customer->validateOldPassword($data['password']);
		
		if(!$validate_old_pass){
			$json['errors']['old_password'] = $this->language->get('error_old_match');
		}
		
		if($validate_same_pass){
			$json['errors']['password'] = $this->language->get('error_same_pass');
		}
		
		if (empty($json['errors'])) {

			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($this->customer->getEmail(), $data['password']);
			
			$json['redirect'] = $this->url->link('pickup/account-dashboard');
			$json['success'] = $this->language->get('text_success');
		}
		
		$this->response->setOutput(json_encode($json));
	}
}

?>
