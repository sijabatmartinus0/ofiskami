<?php
class ControllerSellerAccountRequestProduct extends ControllerSellerAccount {
	private $error = array();

	public function index() {

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-request-product', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/request_product');
		
		$this->document->setTitle($this->language->get('ms_account_request_product'));

		$this->data['heading_title'] = $this->language->get('ms_account_request_product');

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
				array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', 'SSL'),
				),
				array(
					'text' => $this->language->get('ms_account_request_product'),
					'href' => $this->url->link('seller/account-request-product', '', 'SSL')
				)
			));
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		$this->data['kiosks'] = $this->model_seller_request_product->getKiosks();
		
		$this->data['link_create'] = $this->url->link('seller/account-request-product/create', '', 'SSL');
		$this->data['list_request'] = $this->url->link('seller/account-request-product/listRequest', '', 'SSL');
		$this->data['search_request'] = $this->url->link('seller/account-request-product/searchRequest', '', 'SSL');
		$this->data['action_edit_request'] = $this->url->link('seller/account-request-product/edit', '', 'SSL');

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-request-product');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function listRequest(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-request-product/listRequest', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/request_product');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['requests'] = array();

		$request_total = $this->model_seller_request_product->getTotalRequest();
		
		$results = $this->model_seller_request_product->getRequests(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));
		

		foreach ($results as $result) {
			$this->data['requests'][] = array(
				'request_id'  	=> $result['request_id'],
				'kiosk'   		=> $result['pp_branch_name'],
				'kiosk_id' 		=> $result['kiosk_id'],
				'delivery_order'=> $result['delivery_order'],
				'sla_delivery'	=> $result['sla_delivery'],
				'status'       	=> $result['status'],
				'date_added'	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'date_modify'	=> date($this->language->get('date_format_short'), strtotime($result['date_modify'])),
				'date_received'	=> date($this->language->get('date_format_short'), strtotime($result['date_received'])),
				'href'       	=> $this->url->link('seller/account-request-product/detail', 'request_id=' . $result['request_id'] , 'SSL'),
				'print'       	=> $this->url->link('seller/account-request-product/printRequest', 'request_id=' . $result['request_id'] , 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $request_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-request-product/listRequest', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($request_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($request_total - $this->config->get('config_product_limit'))) ? $request_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $request_total, ceil($request_total / $this->config->get('config_product_limit')));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-request');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function searchRequest(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-request-product/listRequest', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/request_product');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		if (isset($this->request->get['kiosk_id'])) {
			$kiosk_id = $this->request->get['kiosk_id'];
		} else {
			$kiosk_id = "";
		}

		$this->data['requests'] = array();

		$request_total = $this->model_seller_request_product->getTotalSearchRequest($kiosk_id);
		
		$results = $this->model_seller_request_product->searchRequests(
			array(
				'kiosk_id'		=> $kiosk_id
			),
			($page - 1) * $this->config->get('config_product_limit'), 
			$this->config->get('config_product_limit')
		);
		
		foreach ($results as $result) {
			$this->data['requests'][] = array(
				'request_id'  	=> $result['request_id'],
				'kiosk'   		=> $result['pp_branch_name'],
				'kiosk_id' 		=> $result['kiosk_id'],
				'delivery_order'=> $result['delivery_order'],
				'sla_delivery'	=> $result['sla_delivery'],
				'status'       	=> $result['status'],
				'date_added'	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'date_modify'	=> date($this->language->get('date_format_short'), strtotime($result['date_modify'])),
				'date_received'	=> date($this->language->get('date_format_short'), strtotime($result['date_received'])),
				'href'       	=> $this->url->link('seller/account-request-product/detail', 'request_id=' . $result['request_id'] , 'SSL'),
				'print'       	=> $this->url->link('seller/account-request-product/printRequest', 'request_id=' . $result['request_id'] , 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $request_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-request-product/searchRequest', 'page={page}&kiosk_id='.$kiosk_id, 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($request_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($request_total - $this->config->get('config_product_limit'))) ? $request_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $request_total, ceil($request_total / $this->config->get('config_product_limit')));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-request');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function edit(){
		$json=array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if((utf8_strlen($this->request->post['input_delivery_order']) < 1) ){
				$json['error']['edit_request']=$this->language->get('error_delivery_order');
			}
			
			if(!isset($json['error'])){
				$this->load->model('seller/request_product');
				$this->model_seller_request_product->editRequest($this->request->post);
				$this->session->data['success'] = $this->language->get('text_success_edit_request');
				$this->session->data['error']='';
				$json['url']=$this->url->link('seller/account-request-product', '', 'SSL');
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function _initForm(){
		$this->load->model('seller/request_product');
		$this->data['kiosks'] = $this->model_seller_request_product->getKiosks();
	}
	
	public function create() {
		$this->_initForm();
		
		$this->document->setTitle($this->language->get('button_create_request'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', 'SSL'),
				),
			array(
				'text' => $this->language->get('ms_account_request_product'),
				'href' => $this->url->link('seller/account-request-product', '', 'SSL')
			),
			array(
				'text' => $this->language->get('button_create_request'),
				'href' => $this->url->link('seller/account-request-product/create', '', 'SSL'),
			)
		));
		
		$this->data['link_cancel'] = $this->url->link('seller/account-request-product', '', 'SSL');
		$this->data['action_add_request'] = $this->url->link('seller/account-request-product/save', '', 'SSL');
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-request-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function save(){
		$json=array();
		$data = $this->request->post;
		$this->load->model('seller/request_product');
		
		if (!$data['kiosk_id'] || $data['kiosk_id'] == null || $data['kiosk_id'] == "") {
			$json['error']['kiosk_id'] = $this->language->get('error_kiosk_id');
		}
		
		unset($data['product_request'][0]); // Remove sample row
		
		if (isset($data['product_request']) && is_array($data['product_request'])) {
			$product_requests = $data['product_request'];
			$x = 1;
			foreach ($product_requests as $product_request) {
				$is_minimum = $this->model_seller_request_product->isMinimumPassed($product_request['product_id'], $product_request['quantity']);
				if (!isset($product_request['product']) || $product_request['product'] == null || $product_request['product'] == "") {
					$json['error']['request'] = $this->language->get('error_product_request');
				}
				if(!isset($product_request['quantity']) || $product_request['quantity'] == null || $product_request['quantity'] == ""){
					$json['error']['request'] = $this->language->get('error_product_request');
				}
				if(!isset($product_request['price']) || $product_request['price'] == null || $product_request['price'] == ""){
					$json['error']['request'] = $this->language->get('error_product_request');
				}
				if($product_request['quantity'] && !$is_minimum){
					$json['error']['minimum'][$x] = $this->language->get('error_minimum');
				}
				$x++;
			}
		}
		
		if(!isset($json['error'])){
			$request_id = $this->model_seller_request_product->addRequest($data['kiosk_id']);
			foreach ($product_requests as $product_request) {
				$this->model_seller_request_product->addRequestDetail(
					array(
						'request_id'		=> $request_id,
						'product_id'		=> $product_request['product_id'],
						'quantity_request'	=> $product_request['quantity'],
						'quantity_actual'	=> $product_request['quantity'],
						'price'				=> $product_request['price']
					)
				);
			}
			$this->session->data['success'] = $this->language->get('text_success_add_request');
			$this->session->data['error']='';
			$json['url']=$this->url->link('seller/account-request-product', '', 'SSL');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function autoCompleteProduct() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('seller/request_product');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start' => 0,
				'limit' => 5
			);

			$results = $this->model_seller_request_product->getProductSeller($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'product_id' => $result['product_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function detail() {
		$this->load->model('seller/request_product');
		$this->load->model('tool/image');
		
		if (isset($this->request->get['request_id'])) {
			$request_id = $this->request->get['request_id'];
		} else {
			$request_id = "";
		}
		
		$this->document->setTitle($this->language->get('text_view_request'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', 'SSL'),
				),
			array(
				'text' => $this->language->get('ms_account_request_product'),
				'href' => $this->url->link('seller/account-request-product', '', 'SSL')
			),
			array(
				'text' => $this->language->get('text_view_request'),
				'href' => $this->url->link('seller/account-request-product/detail', 'request_id=' . $request_id, 'SSL'),
			)
		));
		
		$this->data['link_back'] = $this->url->link('seller/account-request-product', '', 'SSL');
		
		$request = $this->model_seller_request_product->getRequest($request_id);
		
		if ($request) {
			
			$this->data['request_id'] = $request['request_id'];
			$this->data['kiosk'] = $request['pp_branch_name'];
			$this->data['merchant'] = $this->MsLoader->MsSeller->getNickName();
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($request['date_added']));
			
			$this->data['details'] = array();
			
			$results = $this->model_seller_request_product->getRequestDetail($request_id);

			foreach ($results as $result) {
				if($result['image'] && file_exists(DIR_IMAGE . $result['image'])){
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				}else {
					$image = $this->MsLoader->MsFile->resizeImage('no_image.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				}
				
				if($result['status'] == MsProduct::STATUS_ON_REVIEW){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_ON_REVIEW);
				}else if($result['status'] == MsProduct::STATUS_ACTIVE){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_ACTIVE);
				}else if($result['status'] == MsProduct::STATUS_DISABLED){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_DISABLED);
				}else{
					$status = "";
				}
				
				$this->data['details'][] = array(
					'product_id'    => $result['product_id'],
					'name'     		=> $result['name'],
					'href'     		=> $this->url->link('product/product', 'product_id=' . $result['product_id'], 'SSL'),
					'quantity'     	=> $result['quantity_request'],
					'category'     	=> $this->model_seller_request_product->getCategoryProduct($result['product_id']),
					'image'     	=> $image,
					'status'     	=> $status,
					'price'     	=> $this->currency->format($result['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
				);
			}
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-request-detail');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function printRequest(){
		$this->load->model('seller/request_product');
		$this->load->model('tool/image');
		
		if (isset($this->request->get['request_id'])) {
			$request_id = $this->request->get['request_id'];
		} else {
			$request_id = "";
		}
		
		$this->data['heading_title'] = $this->language->get('ms_account_request_product');
		
		$request = $this->model_seller_request_product->getRequest($request_id);
		
		if ($request) {
			
			$this->data['request_id'] = $request['request_id'];
			$this->data['kiosk'] = $request['pp_branch_name'];
			$this->data['merchant'] = $this->MsLoader->MsSeller->getNickName();
			$this->data['merchant_company'] = $this->MsLoader->MsSeller->getCompany();
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($request['date_added']));
			
			$this->data['details'] = array();
			
			$results = $this->model_seller_request_product->getRequestDetail($request_id);

			foreach ($results as $result) {
				if($result['image'] && file_exists(DIR_IMAGE . $result['image'])){
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				}else {
					$image = $this->MsLoader->MsFile->resizeImage('no_image.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				}
				
				if($result['status'] == MsProduct::STATUS_ON_REVIEW){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_ON_REVIEW);
				}else if($result['status'] == MsProduct::STATUS_ACTIVE){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_ACTIVE);
				}else if($result['status'] == MsProduct::STATUS_DISABLED){
					$status = $this->language->get('ms_product_status_'.MsProduct::STATUS_DISABLED);
				}else{
					$status = "";
				}
				
				$this->data['details'][] = array(
					'product_id'    => $result['product_id'],
					'name'     		=> $result['name'],
					'href'     		=> $this->url->link('product/product', 'product_id=' . $result['product_id'], 'SSL'),
					'quantity'     	=> $result['quantity_request'],
					'category'     	=> $this->model_seller_request_product->getCategoryProduct($result['product_id']),
					'image'     	=> $image,
					'status'     	=> $status,
					'price'     	=> $this->currency->format($result['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
				);
			}
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('print_request');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
		
	}
}

?>