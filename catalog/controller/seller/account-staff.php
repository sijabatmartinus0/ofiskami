<?php

class ControllerSellerAccountStaff extends ControllerSellerAccount {
	public function getTableData() {
		$colMap = array(
			'staff_name' => '`name`',
			'staff_email' => '`email`',
			'staff_status' => '`status`',
			'date_created' => '`date_created`'
		);
		
		$sorts = array('staff_name', 'staff_email', 'staff_status', 'date_created');
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);

		$seller_id = $this->MsLoader->MsSeller->getSellerId();
		$staffs = $this->MsLoader->MsStaff->getStaffs(
			array(
				'seller_group' => $seller_id,
				'status' => array(MsStaff::STATUS_ENABLED, MsStaff::STATUS_DISABLED)
			),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);
		
		$total = isset($staffs[0]) ? $staffs[0]['total_rows'] : 0;

		$columns = array();
		foreach ($staffs as $staff) {
			// actions
			$actions = "";
			if ($staff['status'] == MsStaff::STATUS_ENABLED){
				$actions .= "<a href='" . $this->url->link('seller/account-staff/disabled', 'seller_id=' . $staff['seller_id'], 'SSL') ."' class='ms-button icon-only' title='" . $this->language->get('ms_unpublish') . "'><span class='fa fa-unlock'></span></a>";
			}else{
				$actions .= "<a href='" . $this->url->link('seller/account-staff/enabled', 'seller_id=' . $staff['seller_id'], 'SSL') ."' class='ms-button icon-only' title='" . $this->language->get('ms_publish') . "'><span class='fa fa-lock'></span></a>";
			}
				
			$actions .= "<a href='" . $this->url->link('seller/account-staff/update', 'seller_id=' . $staff['seller_id'], 'SSL') ."' class='ms-button icon-only' title='" . $this->language->get('ms_edit') . "'><span class='fa fa-pencil'></span></a>";
			$actions .= "<a href='" . $this->url->link('seller/account-staff/delete', 'seller_id=' . $staff['seller_id'], 'SSL') ."' class='ms-button icon-only' title='" . $this->language->get('ms_delete') . "'><span class='fa fa-trash'></span></a>";
			
			// product status
			$status = "";
			if ($staff['status'] == MsStaff::STATUS_ENABLED) { 
				$status = "<span class='active' style='color: #080;'>" . $this->language->get('ms_account_staff_status_' . $staff['status']) . "</td></span>";
			} else {
				$status = "<span class='inactive' style='color: #b00;'>" . $this->language->get('ms_account_staff_status_' . $staff['status']) . "</td></span>";
			}
			
			$columns[] = array_merge(
				$staff,
				array(
					'status'=>$status,
					'date_created' => date($this->language->get('date_format_short'), strtotime($staff['date_created'])),
					'actions' => $actions
				)
			);
		}
		
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}
	
	public function jxUpdateFile() {
		$json = array();
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}
		
		if (isset($this->request->post['file_id']) && isset($this->request->post['product_id'])) {
			$download_id = (int)substr($this->request->post['file_id'], strrpos($this->request->post['file_id'], '-')+1);
			$product_id = (int)$this->request->post['product_id'];
			$seller_id = $this->MsLoader->MsSeller->getSellerId();
			if  ($this->MsLoader->MsProduct->productOwnedBySeller($product_id,$seller_id) && $this->MsLoader->MsProduct->hasDownload($product_id,$download_id)) {
				$file = array_shift($_FILES);
				$errors = $this->MsLoader->MsFile->checkDownload($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileData = $this->MsLoader->MsFile->uploadDownload($file);
					$json['fileName'] = $fileData['fileName'];
					$json['fileMask'] = $fileData['fileMask'];
				}
			}
		}
			
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadSellerAvatar() {
		$json = array();
		$file = array();
		
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		foreach ($_FILES as $file) {
			$errors = $this->MsLoader->MsFile->checkImage($file);
			
			if ($errors) {
				$json['errors'] = array_merge($json['errors'], $errors);
			} else {
				$fileName = $this->MsLoader->MsFile->uploadImage($file);
				$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'));
				$json['files'][] = array(
					'name' => $fileName,
					'thumb' => $thumbUrl
				);
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadImages() {
		$json = array();
		$file = array();
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N images
		$msconf_images_limits = $this->config->get('msconf_images_limits');
		foreach ($_FILES as $file) {
			if ($msconf_images_limits[1] > 0 && $this->request->post['fileCount'] >= $msconf_images_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_image_maximum'),$msconf_images_limits[1]);
				$json['cancel'] = 1;
				$this->response->setOutput(json_encode($json));
				return;
			} else {
				$errors = $this->MsLoader->MsFile->checkImage($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileName = $this->MsLoader->MsFile->uploadImage($file);

					$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'));
					$json['files'][] = array(
						'name' => $fileName,
						'thumb' => $thumbUrl
					);
				}
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadDownloads() {
		$json = array();
		$file = array();
		
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N images
		$msconf_downloads_limits = $this->config->get('msconf_downloads_limits');
		foreach ($_FILES as $file) {
			if ($msconf_downloads_limits[1] > 0 && $this->request->post['fileCount'] >= $msconf_downloads_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_download_maximum'),$msconf_downloads_limits[1]);
				$json['cancel'] = 1;
				$this->response->setOutput(json_encode($json));
				return;
			} else {
				$errors = $this->MsLoader->MsFile->checkDownload($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileData = $this->MsLoader->MsFile->uploadDownload($file);

					$json['files'][] = array (
						'fileName' => $fileData['fileName'],
						'fileMask' => $fileData['fileMask'],
						'filePages' => isset($pages) ? $pages : ''
					);
				}
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxGetFee() {
		$data = $this->request->get;
		
		if (!isset($data['price']) && !is_numeric($data['price'])) 
			$data['price'] = 0;

		$rates = $this->MsLoader->MsCommission->calculateCommission(array('seller_id' => $this->MsLoader->MsSeller->getSellerId()));
		echo $this->currency->format((float)$rates[MsCommission::RATE_LISTING]['flat'] + ((float)$rates[MsCommission::RATE_LISTING]['percent'] * $data['price'] / 100), $this->config->get('config_currency'));
	}
	
	/*Custom by M*/
	public function jxGetProductAttribute(){
		$data = $this->request->get;	
		$product_attributes = array();
		$this->load->model('localisation/language');
		
		if (isset($this->request->get['category_id'])){
			$product_attributes['attributes'] = $this->MsLoader->MsProduct->getProductAttributesByCategory($data['category_id']);
			$product_attributes['languages'] = $this->model_localisation_language->getLanguages();
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($product_attributes));
	}
	
	public function jxSubmitStaff()
	{
		//ob_start();
		$data = $this->request->post;
		$this->load->model('account/customer');
		$staff_id = isset($this->request->post['staff_id']) ? (int)$this->request->post['staff_id'] : 0;
		$seller_id = $this->MsLoader->MsSeller->getSellerId();
		
		if  (!$this->MsLoader->MsStaff->staffOneGroupBySeller($staff_id, $seller_id)) {
    		$staff_id=0;
		}
		
		$staff = $data['staff'];
		
		$json = array();

		if ($staff){
			if ((utf8_strlen($staff['firstname']) < 1) || (utf8_strlen($staff['firstname']) > 32)) {
				$json['errors']['staff[firstname]'] = $this->language->get('error_firstname');
			}

			if ((utf8_strlen($staff['lastname']) < 1) || (utf8_strlen($staff['lastname']) > 32)) {
				$json['errors']['staff[lastname]'] = $this->language->get('error_lastname');
			}
			
			if ((utf8_strlen($staff['telephone']) < 3) || (utf8_strlen($staff['telephone']) > 32)) {
				$json['errors']['staff[telephone]'] = $this->language->get('error_telephone');
			}
			
			if (!$this->checkDate($staff['dob'])) {
				$json['errors']['staff[dob]'] = $this->language->get('error_dob');
			}else if(date($staff['dob']) > date("Y-m-d",strtotime('-15 years'))){
				$json['errors']['staff[dob]'] = $this->language->get('error_dob_value');
			}
			
			if ((utf8_strlen($staff['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $staff['email'])) {
				$json['errors']['staff[email]'] = $this->language->get('error_email');
			} else {
				if ($this->model_account_customer->getTotalCustomersByEmail($staff['email'],$staff_id)) {
					$json['errors']['staff[email]'] = $this->language->get('error_exists');
				}
			}

			if (empty($staff['nickname'])) {
				$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_empty');
			} else if (mb_strlen($staff['nickname']) < 4 || mb_strlen($staff['nickname']) > 128 ) {
				$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_length');
			} else if (($this->MsLoader->MsSeller->nicknameTakenUpdate($staff['nickname'],$staff_id)) ) {
				$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_taken');
			} else {
				switch($this->config->get('msconf_nickname_rules')) {
					case 1:
						// extended latin
						if(!preg_match("/^[a-zA-Z0-9_\-\s\x{00C0}-\x{017F}]+$/u", $staff['nickname'])) {
							$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_latin');
						}
						break;

					case 2:
						// utf8
						if(!preg_match("/((?:[\x01-\x7F]|[\xC0-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF7][\x80-\xBF]{3}){1,100})./x", $staff['nickname'])) {
							$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_utf8');
						}
						break;

					case 0:
					default:
						// alnum
						if(!preg_match("/^[a-zA-Z0-9_\-\s]+$/", $staff['nickname'])) {
							$json['errors']['staff[nickname]'] = $this->language->get('ms_error_sellerinfo_nickname_alphanumeric');
						}
						break;
				}
			}
		}
		// post-validation
		if (empty($json['errors'])) {
			if($staff_id==0){
				// Create buyer account
				$staff['fax'] = '';
				$staff['company'] = $this->MsLoader->MsSeller->getCompany();
				$staff['address_1'] = '';
				$staff['address_2'] = '';
				$staff['city'] = '';
				$staff['seller'] = 1;
				$staff['zone'] = $this->MsLoader->MsSeller->getZoneId();
				$staff['country'] = $this->MsLoader->MsSeller->getCountryId();
				$staff['city'] = $this->MsLoader->MsSeller->getCityId();
				$staff['district'] = $this->MsLoader->MsSeller->getDistrictId();
				$staff['subdistrict'] = $this->MsLoader->MsSeller->getSubdistrictId();
				$staff['postcode'] = $this->MsLoader->MsSeller->getPostcode();
				$staff['customer_group_id'] = $this->config->get('config_customer_group_id');
				$staff['hash'] = md5( rand(0,1000).time()); 
				$staff['code'] = $this->generate_random_password(5);
				$staff['password'] = MsStaff::PASSWORD_DEFAULT;
				$staff['seller_id'] = $this->MsLoader->MsStaff->addCustomerUser($staff);
				
				// Register seller
				$staff['status'] = MsSeller::STATUS_ACTIVE;
				$staff['approved'] = 1;
				$staff['address'] = $this->MsLoader->MsSeller->getAddress();
				$staff['account_id'] = $this->MsLoader->MsSeller->getAccountId();
				$staff['account_name'] = $this->MsLoader->MsSeller->getAccountName();
				$staff['account_number'] = $this->MsLoader->MsSeller->getAccountNumber();
				$staff['npwp_number'] = $this->MsLoader->MsSeller->getNPWPNumber();
				$staff['npwp_address'] = $this->MsLoader->MsSeller->getNPWPAddress();
				$staff['seller_group'] = $seller_id;
				$staff['seller_type'] = 0;
				$staff['premium_status'] = 2;

				$this->MsLoader->MsStaff->createUser($staff);
			}else{
				// Create buyer account
				$staff['fax'] = '';
				$staff['company'] = $this->MsLoader->MsSeller->getCompany();
				$staff['address_1'] = '';
				$staff['address_2'] = '';
				$staff['seller'] = 1;
				$staff['zone'] = $this->MsLoader->MsSeller->getZoneId();
				$staff['country'] = $this->MsLoader->MsSeller->getCountryId();
				$staff['city'] = $this->MsLoader->MsSeller->getCityId();
				$staff['district'] = $this->MsLoader->MsSeller->getDistrictId();
				$staff['subdistrict'] = $this->MsLoader->MsSeller->getSubdistrictId();
				$staff['postcode'] = $this->MsLoader->MsSeller->getPostcode();
				$staff['customer_group_id'] = $this->config->get('config_customer_group_id');
				$staff['hash'] = md5( rand(0,1000).time()); 
				$staff['code'] = $this->generate_random_password(5);
				$staff['customer_id'] = $staff_id;
				$this->MsLoader->MsStaff->editCustomerUser($staff);
				
				$staff['seller_id'] = $staff_id;
				
				// Register seller
				$staff['status'] = MsSeller::STATUS_ACTIVE;
				$staff['approved'] = 1;
				$staff['address'] = $this->MsLoader->MsSeller->getAddress();
				$staff['account_id'] = $this->MsLoader->MsSeller->getAccountId();
				$staff['account_name'] = $this->MsLoader->MsSeller->getAccountName();
				$staff['account_number'] = $this->MsLoader->MsSeller->getAccountNumber();
				$staff['npwp_number'] = $this->MsLoader->MsSeller->getNPWPNumber();
				$staff['npwp_address'] = $this->MsLoader->MsSeller->getNPWPAddress();
				$staff['seller_group'] = $seller_id;
				$staff['seller_type'] = 0;
				$staff['premium_status'] = 2;

				$this->MsLoader->MsStaff->editUser($staff);
			}
			$json['redirect'] = $this->url->link('seller/account-staff', '', 'SSL');
		}
		
		/*
		$output = ob_get_clean();
		if ($output) {
			$this->log->write('MMERCH PRODUCT FORM: ' . $output);
			if (!$this->session->data['success']) $json['fail'] = 1;
		}
		*/
		$this->response->setOutput(json_encode($json));
	}

	public function jxRenderOptions() {
		$this->data['options'] = $this->MsLoader->MsOption->getOptions();
		foreach ($this->data['options'] as &$option) {
			$option['values'] = $this->MsLoader->MsOption->getOptionValues($option['option_id']);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/multiseller/account-product-form-options.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/multiseller/account-product-form-options.tpl';
		} else {
			$this->template = 'default/template/multiseller/account-product-form-options.tpl';
		}

		$this->response->setOutput($this->load->view($this->template, $this->data));
	}
	
	public function jxRenderOptionValues() {
		$this->data['option'] = $this->MsLoader->MsOption->getOptions(
			array(
				'option_id' => 	$this->request->get['option_id'],
				'single' => 1
			)
		);
		
		$this->data['values'] = $this->MsLoader->MsOption->getOptionValues($this->request->get['option_id']);
		$this->data['option_index'] = 0;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/multiseller/account-product-form-options-values.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/multiseller/account-product-form-options-values.tpl';
		} else {
			$this->template = 'default/template/multiseller/account-product-form-options-values.tpl';
		}
	
		$this->response->setOutput($this->load->view($this->template, $this->data));
	}
	
	public function jxRenderProductOptions() {
		$this->load->model('catalog/product');
		$options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
		
		$output = '';
		if ($options) {
			$this->data['option_index'] = 0;
			foreach ($options as $o) {
				$this->data['option'] = $o;
				$this->data['product_option_values'] = $o['product_option_value'];
				$this->data['values'] = $this->MsLoader->MsOption->getOptionValues($o['option_id']);
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/multiseller/account-product-form-options-values.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/multiseller/account-product-form-options-values.tpl';
				} else {
					$this->template = 'default/template/multiseller/account-product-form-options-values.tpl';
				}
				
				$output .= $this->load->view($this->template, $this->data);
				$this->data['option_index']++;
			}
		}
	
		$this->response->setOutput($output);
	}	
	
	public function jxAutocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/manufacturer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start' => 0,
				'limit' => 5
			);

			$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'manufacturer_id' => $result['manufacturer_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function jxShippingCategories()
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $product_id = empty($this->request->post['product_id']) ? 0 : $this->request->post['product_id'];
        $seller_id  = $this->MsLoader->MsSeller->getSellerId();
        $product    = NULL;

        if(!empty($product_id))
        {
            if($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id))
            {
                $product = $this->MsLoader->MsProduct->getProduct($product_id);
            }
            else
                $product = NULL;
        }

        $this->data['product'] = $product;
        $this->data['product']['category_id'] = $this->MsLoader->MsProduct->getProductCategories($product_id);
        $this->data['product']['shipping'] = $this->request->post['type'];
        $this->data['categories'] = $this->MsLoader->MsProduct->getCategories();
        $this->data['msconf_allow_multiple_categories'] = $this->config->get('msconf_allow_multiple_categories');
        $this->data['msconf_enable_categories'] = $this->config->get('msconf_enable_categories');
        $this->data['msconf_physical_product_categories'] = $this->config->get('msconf_physical_product_categories');
        $this->data['msconf_digital_product_categories'] = $this->config->get('msconf_digital_product_categories');

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form-shipping-categories');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
    }

    public function index() {
		// Links
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		$this->data['link_create_user'] = $this->url->link('seller/account-staff/create', '', 'SSL');
		
		if((int)$this->MsLoader->MsSeller->getSellerType()==1 && (int)$this->MsLoader->MsSeller->getPremiumStatus()==2){
			
		
		// Title and friends
		$this->document->setTitle($this->language->get('ms_account_staff_heading'));		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_staff_breadcrumbs'),
				'href' => $this->url->link('seller/account-staff', '', 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-staff');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
		}else{
			$this->response->redirect($this->url->link('seller/account-noroles', '', 'SSL'));
		}
	}
	
	private function _initForm()
	{
		$this->load->model('localisation/language');

		$this->data['roles']=$this->MsLoader->MsStaff->getRoles();	
		
	}
	
	public function create() {
		$this->_initForm();
		$this->data['staff']['roles'] = array();
		$this->data['staff']['firstname']='';
		$this->data['staff']['lastname']='';
		$this->data['staff']['nickname']='';
		$this->data['staff']['email']='';
		$this->data['staff']['telephone']='';
		$this->data['staff']['dob']='';
		$this->data['staff_id']=0;
		$this->data['user'] = FALSE;
		$this->data['heading'] = $this->language->get('ms_account_newstaff_heading');
		$this->document->setTitle($this->language->get('ms_account_newstaff_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_account_staff_breadcrumbs'),
				'href' => $this->url->link('seller/account-staff', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_newstaff_breadcrumbs'),
				'href' => $this->url->link('seller/account-staff/create', '', 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-staff-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function update() {
		$staff_id = isset($this->request->get['seller_id']) ? (int)$this->request->get['seller_id'] : 0;
		$seller_id = $this->MsLoader->MsSeller->getSellerId();
		
		if  ($this->MsLoader->MsStaff->staffOneGroupBySeller($staff_id, $seller_id)) {
    		$staff = $this->MsLoader->MsStaff->getStaff($staff_id);
		} else {
			$staff = NULL;
		}

		if (!$staff)
			return $this->response->redirect($this->url->link('seller/account-staff', '', 'SSL'));
			
		
        $this->_initForm();
		$this->data['staff_id']=$staff_id;
		$this->data['staff']=$staff;
		$this->data['staff']['roles'] = unserialize($staff['roles']);
		//print_r($this->data['staff']['roles']);
		$this->data['heading'] = $this->language->get('ms_account_updatestaff_heading');
		$this->document->setTitle($this->language->get('ms_account_updatestaff_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_account_staff_breadcrumbs'),
				'href' => $this->url->link('seller/account-staff', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_updatestaff_breadcrumbs'),
				'href' => $this->url->link('seller/account-staff/update', '&seller_id='.$staff_id, 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-staff-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function delete() {
		$staff_id = (int)$this->request->get['seller_id'];
		$seller_id = (int)$this->MsLoader->MsSeller->getSellerId();
		
		if ($this->MsLoader->MsStaff->staffOneGroupBySeller($staff_id, $seller_id)) {
			$this->MsLoader->MsStaff->changeStatus($staff_id, MsStaff::STATUS_DELETED);
			$this->session->data['success'] = $this->language->get('ms_success_staff_deleted');
		}
		
		$this->response->redirect($this->url->link('seller/account-staff', '', 'SSL'));
	}
	
	public function enabled() {
		$staff_id = (int)$this->request->get['seller_id'];
		$seller_id = (int)$this->MsLoader->MsSeller->getSellerId();
		
		if ($this->MsLoader->MsStaff->staffOneGroupBySeller($staff_id, $seller_id)) {
			$this->MsLoader->MsStaff->changeStatus($staff_id, MsStaff::STATUS_ENABLED);
			$this->session->data['success'] = $this->language->get('ms_success_staff_enabled');
		}
		
		$this->response->redirect($this->url->link('seller/account-staff', '', 'SSL'));
	}	
	
	public function disabled() {
		$staff_id = (int)$this->request->get['seller_id'];
		$seller_id = (int)$this->MsLoader->MsSeller->getSellerId();
		
		if ($this->MsLoader->MsStaff->staffOneGroupBySeller($staff_id, $seller_id)) {
			$this->MsLoader->MsStaff->changeStatus($staff_id, MsStaff::STATUS_DISABLED);
			$this->session->data['success'] = $this->language->get('ms_success_staff_disabled');
		}
		
		$this->response->redirect($this->url->link('seller/account-staff', '', 'SSL'));
	}	
	
	public function download() {
		if (!$this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		if (isset($this->request->get['download_id'])) {
			$download_id = $this->request->get['download_id'];
		} else {
			$download_id = 0;
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (!$this->MsLoader->MsProduct->hasDownload($product_id,$download_id) || !$this->MsLoader->MsProduct->productOwnedBySeller($product_id,$this->MsLoader->MsSeller->getSellerId()))
			$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
			
		$download_info = $this->MsLoader->MsProduct->getDownload($download_id);
		
		if ($download_info) {
			$file = DIR_DOWNLOAD . $download_info['filename'];
			$mask = basename($download_info['mask']);

			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));
					
					readfile($file, 'rb');
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}
	}	

	public function comment() {
			
		$this->data['ms_account_productcomment_text_comment'] = $this->language->get('ms_account_productcomment_text_comment');
		$this->data['ms_account_productcomment_text_note'] = $this->language->get('ms_account_productcomment_text_note');
		$this->data['ms_account_productcomment_text_more'] = $this->language->get('ms_account_productcomment_text_more');
		$this->data['ms_account_productcomment_text_post_comment'] = $this->language->get('ms_account_productcomment_text_post_comment');
		$this->data['ms_account_productcomment_text_seller'] = $this->language->get('ms_account_productcomment_text_seller');
		$this->data['ms_account_productcomment_text_customer'] = $this->language->get('ms_account_productcomment_text_customer');
		$this->data['ms_account_productcomment_button_continue'] = $this->language->get('ms_account_productcomment_button_continue');
		$this->data['pc_wait'] = $this->language->get('pc_wait');
		$this->data['pc_no_comments_yet'] = $this->language->get('pc_no_comments_yet');
		
		$this->data['pcconf_maxlen'] = $this->config->get('pcconf_maxlen');
		
		$comments_per_page = $this->config->get('productcomments_pcconf_perpage');
		if ((int)$comments_per_page == 0)
			$comments_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$this->data['product'] = FALSE;
		$this->data['heading'] = $this->language->get('ms_account_productcomment_heading');
		$this->document->setTitle($this->language->get('ms_account_productcomment_heading'));

		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$seller_id = (int)$this->MsLoader->MsSeller->getSellerId();
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_account_products_breadcrumbs'),
				'href' => $this->url->link('seller/account-product', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_productcomment_breadcrumbs'),
				'href' => $this->url->link('seller/account-product/comment', '', 'SSL'),
			)
		));
		
		//$this->_initForm();
		if  ($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id)) {
    		$product = $this->MsLoader->MsProduct->getProduct($product_id);
		} else {
			$product = NULL;
		}

		if (!$product)
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		
		
		$this->load->model('module/productcomments');
		$this->data['comments'] = $this->model_module_productcomments->getCommentsPerProduct(
			array(
				'product_id' => $product_id,
				'displayed' => 1,
			),
			array(
				'order_by' => 'pc.create_time',
				'order_way' => 'DESC',
				'offset' => ($page - 1) * $comments_per_page,
				'limit' => $comments_per_page
			)
		);
		$this->data['product_id'] = $product_id;
		
		$this->document->addScript('catalog/view/javascript/ms-productcomments.js');
		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/productcomments.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/productcomments.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/productcomments.css');
		}
		
		$total_comments = $this->model_module_productcomments->getTotalCommentsPerCustomer(
			array(
				'displayed' => 1,
				'product_id' => $product_id
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_comments;
		$pagination->page = $page;
		$pagination->limit = $comments_per_page; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('seller/account-product/comment', '&page={page}' . '&product_id=' . $product_id);
		$this->data['pagination'] = $pagination->render();
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-comment');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function submitComment(){
		if (!isset($this->request->get['product_id']))
			return;
		
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$parent_id = isset($this->request->post['pcId']) ? (int)$this->request->post['pcId'] : 0;
		
		$comment['product_id'] = $product_id;
		
		$json = array();
		
		//if (is admin) {
			//admin logged in
		//} else 	
		if ($this->customer->isLogged()) {
			//customer logged in
			if  ($this->MsLoader->MsProduct->productOwnedBySeller($comment['product_id'], $this->MsLoader->MsSeller->getSellerId())) {
				$comment['customer_id'] = $this->MsLoader->MsSeller->getSellerId();
				$comment['parent_id'] = $parent_id;
				$comment['name'] = $this->customer->getFirstName();
				$comment['email'] = $this->customer->getEmail();
				$comment['flag'] = 0;
			} else {
				$comment['customer_id'] = 0;
				$comment['parent_id'] = 0;
				$comment['name'] = '';
				$comment['email'] = '';
				$comment['flag'] = 0;
			}			
		} else {
			// guest
			$comment['customer_id'] = 0;
			$comment['parent_id'] = 0;
			if (!$this->config->get('productcomments_pcconf_allow_guests')) {
				//$json['errors'][] = 'Not allowed to post';
				return;			
			} else {
	    		$comment['name'] = '';
	    		$comment['email'] = '';
			}
		}

	    $comment['comment'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcText'])));

		if (mb_strlen($comment['name'],'UTF-8') < 3 || mb_strlen($comment['name'],'UTF-8') > 25) {
			$json['errors'][] = sprintf($this->language->get('pc_error_name'), 3, 25);
		}

		if (!filter_var($comment['email'], FILTER_VALIDATE_EMAIL) || mb_strlen($comment['email'], 'UTF-8') > 128) {
			$json['errors'][] = $this->language->get('pc_error_email');
		}

		if (mb_strlen($comment['comment']) < 10) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_short'), 10);
		}

		if (mb_strlen($comment['comment']) > $this->config->get('productcomments_pcconf_maxlen') && $this->config->get('productcomments_pcconf_maxlen') > 0) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_long'), $this->config->get('productcomments_pcconf_maxlen'));
		}

		if (!$this->customer->isLogged() || ($this->customer->isLogged() && $this->config->get('productcomments_pcconf_enable_customer_captcha'))) {
			if (!isset($this->request->post['pcCaptcha']) || ($this->session->data['captcha'] != $this->request->post['pcCaptcha'])) {
				$json['errors'][] = $this->language->get('pc_error_captcha');
			}
		}
		
		$this->load->model('module/productcomments');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($json['errors'])) {
			$this->model_module_productcomments->addComment($comment);
			$json['success'] = $this->language->get('pc_success');
		}
		
		$json['comments'] = $this->model_module_productcomments->getCommentsPerProductDetail(
			array(
				'product_id' => $product_id,
				'parent_id' => $parent_id,
				'displayed' => 1,
			),
			array(
				'order_by' => 'pc.create_time',
				'order_way' => 'DESC'
			)
		);
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));
		}
	}
	public function viewComment(){
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$parent_id = isset($this->request->get['parent_id']) ? (int)$this->request->get['parent_id'] : 0;
		
		$this->load->model('module/productcomments');
		$json['comments'] = $this->model_module_productcomments->getCommentsPerProductDetailPerCustomer(
			array(
				'product_id' => $product_id,
				'parent_id' => $parent_id
			)
		);
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));
		}
	}
	private function generate_random_password($length = 10) {
			$alphabets = range('A','Z');
			$numbers = range('0','9');
			$additional_characters = array('_','.');
			$final_array = array_merge($alphabets,$numbers);
				 
			$password = '';
		  
			while($length--) {
			  $key = array_rand($final_array);
			  $password .= $final_array[$key];
			}
		  
			return $password;
	  }
	private function checkDate($date, $format = 'Y-m-d'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
  }
}
?>
