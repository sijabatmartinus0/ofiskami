<?php

class ControllerSellerAccountNoRoles extends ControllerSellerAccount {
	public function index() {
		
		$this->document->setTitle($this->language->get('ms_account_noroles_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-noroles');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
}

?>
