<?php
class ControllerSellerPassword extends Controller{

	public function index() {
		
		if(isset($this->request->get['code']) && $this->MsLoader->MsSeller->isNewSeller($this->request->get['code'])){
			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['wishlist']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['facebook']);
			unset($this->session->data['google']);
			unset($this->session->data['twitter']);
			
			$this->load->language('multiseller/password');
			$this->document->setTitle($this->language->get('heading_title'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('seller/password', '', 'SSL')
			);

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_password'] = $this->language->get('text_password');

			$data['entry_password'] = $this->language->get('entry_password');
			$data['entry_confirm'] = $this->language->get('entry_confirm');
			
			$data['code'] = $this->request->get['code'];
			$data['button_submit'] = $this->language->get('button_submit');

			if (isset($this->request->post['password'])) {
				$data['password'] = $this->request->post['password'];
			} else {
				$data['password'] = '';
			}

			if (isset($this->request->post['confirm'])) {
				$data['confirm'] = $this->request->post['confirm'];
			} else {
				$data['confirm'] = '';
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/multiseller/password.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/multiseller/password.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/multiseller/password.tpl', $data));
			}
		
		}else{
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

	}
	
	public function jxSubmit(){
		$this->load->language('multiseller/password');
		$data = $this->request->post;
		
		$json = array();
		if(isset($data['code']) && $customer = $this->MsLoader->MsSeller->isNewSeller($data['code'])){
			if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
				$json['errors']['password'] = $this->language->get('error_password');
			}

			if ($this->request->post['confirm'] != $this->request->post['password']) {
				$json['errors']['confirm'] = $this->language->get('error_confirm');
			}
		}else{
			$json['errors']['code'] = $this->language->get('error_code');
		}
		
		if (empty($json['errors'])) {
			
			$data = array_merge(
						$data,
						array(
							'customer_id' => $customer['customer_id'],
							'code' => $customer['change_password'],
						)
					);

			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($customer['email'], $data['password']);
			
			$this->MsLoader->MsSeller->updatePassword($data);
			$json['redirect'] = $this->url->link('account/login');
			$json['success'] = $this->language->get('text_success');
		}
		
		$this->response->setOutput(json_encode($json));
	}
}