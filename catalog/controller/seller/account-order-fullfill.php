<?php
class ControllerSellerAccountOrderFullfill extends ControllerSellerAccount {
	private $error = array();
	
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-order-fullfill', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->data['link_report_order_fullfill'] = $this->url->link('seller/account-order-fullfill/link_report_order_fullfill', '', 'SSL');
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');
		
		$this->document->setTitle($this->language->get('text_order_fullfill'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_order_fullfill'),
				'href' => $this->url->link('seller/account-order-fullfill', '', 'SSL'),
			)
		));
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->load->model('seller/account_order_fullfill');
		$this->data['order_statuses'] = $this->model_seller_account_order_fullfill->getOrderStatuses();
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		$this->data['search_order'] = $this->url->link('seller/account-order-fullfill/search_order', 'page=1', 'SSL');
		$this->data['list_order'] = $this->url->link('seller/account-order-fullfill/list_order', 'page=1', 'SSL');
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-fullfill');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function list_order(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-order-fullfill/list_order', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_order_fullfill');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['returns'] = array();

		$return_total = $this->model_seller_account_order_fullfill->getTotalOrders();
		//var_dump($return_total); die();
		$results = $this->model_seller_account_order_fullfill->getOrders(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));

		foreach ($results as $result) {
			$this->data['returns'][] = array(
				'receipt_no'      => $result['receipt_no'],
				'no_resi'      => $result['no_resi'],
				'kiosk_id' 	=> $result['kiosk_id'],
				'date_added'      => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'quantity'        => $result['quantity'],
				'sku'         => $result['sku'],
				'code'    => $result['code'],
				'price' => $this->currency->format($result['price'], $result['currency_code'], $result['currency_value']),
				'shipping_price' => $this->currency->format($result['shipping_price'], $result['currency_code'], $result['currency_value']),
				'commission_fee' => $result['commission_fee'],
				'name' => $result['name'],
				'href'       => $this->url->link('seller/account-order-fullfill/detail', 'receipt_no=' . $result['receipt_no'] , 'SSL')
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $return_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-order-fullfill/list_order', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($return_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($return_total - $this->config->get('config_product_limit'))) ? $return_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $return_total, ceil($return_total / $this->config->get('config_product_limit')));
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-fullfill-view');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function search_order(){
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');
		
		$this->document->setTitle($this->language->get('text_order_fullfill'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_order_fullfill'),
				'href' => $this->url->link('seller/account-order-fullfill', '', 'SSL'),
			)
		));
		
		$this->load->model('seller/account_order_fullfill');

		if (isset($this->request->get['search_invoice'])) {
			$data['search_invoice'] = $this->request->get['search_invoice'];
		} else {
			$data['search_invoice'] = '';
		}
		if (isset($this->request->get['search_status_id'])) {
            $data['search_status_id'] = $this->request->get['search_status_id'];
        } else {
            $data['search_status_id'] = '';
        }
		if (isset($this->request->get['search_start_date'])) {
			$data['search_start_date'] = $this->request->get['search_start_date'];
		} else {
			$data['search_start_date'] = '';
		}
		if (isset($this->request->get['search_end_date'])) {
			$data['search_end_date'] = $this->request->get['search_end_date'];
		} else {
			$data['search_end_date'] = '';
		}
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-order-fullfill/search_order', 'page=1&search_invoice='.$data['search_invoice']. '&search_status_id='.$data['search_status_id']. '&search_start_date='. $data['search_start_date']. '&search_end_date='. $data['search_end_date'], 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$this->data['returns'] = array();

		$return_total = $this->model_seller_account_order_fullfill->countSearchOrders(
			array(
				'search_invoice' 	=> $data['search_invoice'],
				'search_status_id'	=> $data['search_status_id'],
				'search_start_date' 	=> $data['search_start_date'],
				'search_end_date' 	=> $data['search_end_date']
			)
		);
		
		$results = $this->model_seller_account_order_fullfill->searchOrders(
			array(
				'search_invoice' 	=> $data['search_invoice'],
				'search_status_id'	=> $data['search_status_id'],
				'search_start_date' 	=> $data['search_start_date'],
				'search_end_date' 	=> $data['search_end_date']
			),
			($page - 1) * $this->config->get('config_product_limit'), 
			$this->config->get('config_product_limit')
		);
		foreach ($results as $result) {
			$this->data['returns'][] = array(
				'receipt_no'      => $result['receipt_no'],
				'no_resi'      => $result['no_resi'],
				'kiosk_id' 	=> $result['kiosk_id'],
				'date_added'      => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'quantity'        => $result['quantity'],
				'sku'         => $result['sku'],
				'code'    => $result['code'],
				'price' => $this->currency->format($result['price'], $result['currency_code'], $result['currency_value']),
				'shipping_price' => $this->currency->format($result['shipping_price'], $result['currency_code'], $result['currency_value']),
				'commission_fee' => $result['commission_fee'],
				'name' => $result['name'],
				'href'       => $this->url->link('seller/account-order-fullfill/detail', 'receipt_no=' . $result['receipt_no'], 'SSL')
			);
		}
		$pagination = new Pagination();
		$pagination->total = $return_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-order-fullfill/search_order', 'page={page}&search_invoice='.$data['search_invoice']. '&search_status_id='.$data['search_status_id']. '&search_start_date='. $data['search_start_date']. '&search_end_date='. $data['search_end_date'], 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($return_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($return_total - $this->config->get('config_product_limit'))) ? $return_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $return_total, ceil($return_total / $this->config->get('config_product_limit')));
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-fullfill-view');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	function link_report_order_fullfill(){
        $this->load->model('seller/account_order_fullfill');
            $data = $this->request->post;
           
            $filter_data = array(
                'search_invoice' => $data['search_invoice'],
				'search_start_date' => $data['search_start_date'],
				'search_end_date' => $data['search_end_date'],
				'search_status_id' => $data['search_status_id']
            );
            
            $results = $this->model_seller_account_order_fullfill->getReportOrderFullfill($filter_data);
            

            $this->model_seller_account_order_fullfill->exportToExcel($results);
     }
}

?>
