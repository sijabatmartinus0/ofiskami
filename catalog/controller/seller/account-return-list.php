<?php
class ControllerSellerAccountReturnList extends ControllerSellerAccount {
	private $error = array();
	
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');
		
		$this->document->setTitle($this->language->get('text_return_list'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_return_list'),
				'href' => $this->url->link('seller/account-return-list', '', 'SSL'),
			)
		));
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->load->model('seller/account_return_list');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		$this->data['search_return'] = $this->url->link('seller/account-return-list/search_return', 'page=1', 'SSL');
		$this->data['list_return'] = $this->url->link('seller/account-return-list/list_return', 'page=1', 'SSL');

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-return-list');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function list_return(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list/list_return', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_return_list');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['returns'] = array();

		$return_total = $this->model_seller_account_return_list->getTotalReturns();
		
		$results = $this->model_seller_account_return_list->getReturns(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));

		foreach ($results as $result) {
			$this->data['returns'][] = array(
				'return_id'  => $result['return_id'],
				'order_id'   => $result['order_id'],
				'invoice_no' => $result['invoice_no'],
				'product'	 => $result['product'],
				'model'	 	 => $result['model'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'href'       => $this->url->link('seller/account-return-list/detail', 'return_id=' . $result['return_id'] , 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $return_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-return-list/list_return', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($return_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($return_total - $this->config->get('config_product_limit'))) ? $return_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $return_total, ceil($return_total / $this->config->get('config_product_limit')));
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-return-view');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function detail(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->get['return_id'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		if (isset($this->error['error_discuss_comment'])) {
			$this->data['error_discuss_comment'] = $this->error['error_discuss_comment'];
		} else {
			$this->data['error_discuss_comment'] = '';
		}
		
		$this->data['link_back'] = $this->url->link('seller/account-return-list', '', 'SSL');
		$this->data['link_accept_return'] = $this->url->link('seller/account-return-list/accept_return', '', 'SSL');
		$this->data['link_edit_action'] = $this->url->link('seller/account-return-list/edit_action', '', 'SSL');
		$this->data['link_add_comment'] = $this->url->link('seller/account-return-list/add_comment', '', 'SSL');
		$this->data['list_return_comments'] = $this->url->link('seller/account-return-list/list_return_comments', '', 'SSL');
		$this->data['list_up_comments'] = $this->url->link('seller/account-return-list/list_up_comments', '', 'SSL');
		
		$this->document->setTitle($this->language->get('text_detail_return'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_return_list'),
				'href' => $this->url->link('seller/account-return-list', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_detail_return'),
				'href' => $this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->get['return_id'], 'SSL'),
			)
		));
		
		if (isset($this->request->get['return_id'])) {
			$return_id = $this->request->get['return_id'];
		} else {
			$return_id = 0;
		}
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->load->model('seller/account_return_list');
		$this->load->model('tool/image');
		
		$return_info = $this->model_seller_account_return_list->getReturn($return_id);
		
		if ($return_info) {
				
			if($return_info['image']){
				$image = $this->model_tool_image->resize($return_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				$popup_image = $this->model_tool_image->resize($return_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$image = '';
				$popup_image = '';
			}
			
			$this->data['return_id'] = $return_info['return_id'];
			$this->data['order_id'] = $return_info['order_id'];
			$this->data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($return_info['date_ordered']));
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($return_info['date_added']));
			$this->data['name'] = $return_info['firstname'] . ' ' . $return_info['lastname'];
			$this->data['email'] = $return_info['email'];
			$this->data['telephone'] = $return_info['telephone'];
			$this->data['product'] = $return_info['product'];
			$this->data['model'] = $return_info['model'];
			$this->data['quantity'] = $return_info['quantity'];
			$this->data['reason'] = $return_info['reason'];
			$this->data['opened'] = $return_info['opened'] ? $this->language->get('text_yes') : $this->language->get('text_no');
			$this->data['comment'] = nl2br($return_info['comment']);
			$this->data['action'] = $return_info['action'];
			$this->data['return_action_id'] = $return_info['return_action_id'];
			$this->data['invoice_no'] = $return_info['invoice_no'];
			$this->data['return_status_id'] = $return_info['return_status_id'];
			$this->data['is_action_changed'] = $return_info['is_action_changed'];
			$this->data['confirm_action_changed'] = $return_info['confirm_action_changed'];
			$this->data['sla'] = $return_info['sla'];
			$this->data['product_id'] = $return_info['product_id'];
			$this->data['thumb'] = $image;
			$this->data['popup_image'] = $popup_image;
			$this->data['href'] = $this->url->link('product/product', 'product_id=' . $return_info['product_id']);

			$this->data['histories'] = array();

			$results = $this->model_seller_account_return_list->getReturnHistories($this->request->get['return_id']);

			foreach ($results as $result) {
				$this->data['histories'][] = array(
					'date_added' => date('d/m/Y H:i', strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}

			$this->data['continue'] = $this->url->link('seller/account-return-list', $url, 'SSL');
		}
		
		$this->load->model('localisation/return_action');
		
		$this->data['return_actions'] = $this->model_localisation_return_action->getReturnActions();
		
		$this->data['return_comments']= $this->model_seller_account_return_list->listUpComments($return_info['return_id']);
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-return-detail');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function search_return(){
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');
		
		$this->document->setTitle($this->language->get('text_return_list'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('text_return_list'),
				'href' => $this->url->link('seller/account-return-list', '', 'SSL'),
			)
		));
		
		$this->load->model('seller/account_return_list');

		if (isset($this->request->get['search_invoice'])) {
			$data['search_invoice'] = $this->request->get['search_invoice'];
		} else {
			$data['search_invoice'] = '';
		}
		
		if (isset($this->request->get['search_customer'])) {
			$data['search_customer'] = $this->request->get['search_customer'];
		} else {
			$data['search_customer'] = '';
		}
		
		if (isset($this->request->get['search_product'])) {
			$data['search_product'] = $this->request->get['search_product'];
		} else {
			$data['search_product'] = '';
		}

		if (isset($this->request->get['search_status'])) {
			$data['search_status'] = $this->request->get['search_status'];
		} else {
			$data['search_status'] = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list/search_return', 'page=1&search_invoice='.$data['search_invoice']. '&search_customer='. $data['search_customer']. '&search_product='. $data['search_product']. '&search_status='.$data['search_status'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$this->data['returns'] = array();

		$return_total = $this->model_seller_account_return_list->countSearchReturns(
			array(
				'search_invoice' 	=> $data['search_invoice'],
				'search_customer' 	=> $data['search_customer'],
				'search_product' 	=> $data['search_product'],
				'search_status' 	=> $data['search_status']
			)
		);
		
		$results = $this->model_seller_account_return_list->searchReturns(
			array(
				'search_invoice' 	=> $data['search_invoice'],
				'search_customer' 	=> $data['search_customer'],
				'search_product' 	=> $data['search_product'],
				'search_status' 	=> $data['search_status']
			),
			($page - 1) * $this->config->get('config_product_limit'), 
			$this->config->get('config_product_limit')
		);

		foreach ($results as $result) {
			$this->data['returns'][] = array(
				'return_id'  => $result['return_id'],
				'order_id'   => $result['order_id'],
				'invoice_no' => $result['invoice_no'],
				'product'	 => $result['product'],
				'model'	 	 => $result['model'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'href'       => $this->url->link('seller/account-return-list/detail', 'return_id=' . $result['return_id'], 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $return_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-return-list/search_return', 'page={page}&search_invoice='.$data['search_invoice']. '&search_customer='. $data['search_customer']. '&search_product='. $data['search_product']. '&search_status='.$data['search_status'], 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($return_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($return_total - $this->config->get('config_product_limit'))) ? $return_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $return_total, ceil($return_total / $this->config->get('config_product_limit')));
		
		//status return
		$this->load->model('localisation/return_status');
		$return_statuses = $this->model_localisation_return_status->getReturnStatuses();
		
		foreach($return_statuses as $return_status){
			$this->data['status'][] = array(
				'return_status_id'  => $return_status['return_status_id'],
				'name'  		 	=> $return_status['name']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-return-view');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function accept_return(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_return_list');
		$this->load->model('account/return');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_seller_account_return_list->acceptReturn($this->request->post['input_return_id']);
			$this->model_account_return->sendNotification($this->request->post['input_return_id'], 2);
			
			$this->session->data['success'] = $this->language->get('success_accept_return');
			$this->response->redirect($this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->post['input_return_id'] , 'SSL'));
		}
		
		if (isset($this->request->post['input_return_id'])) {
			$this->data['input_return_id'] = $this->request->post['input_return_id'];
		} else {
			$this->data['input_return_id'] = '';
		}
	}
	
	public function edit_action(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_return_list');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_seller_account_return_list->editAction($this->request->post);
			$this->session->data['success'] = $this->language->get('success_change_action');
			$this->response->redirect($this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->post['input_return_id_action'] , 'SSL'));
		}
		
		if (isset($this->request->post['input_return_id_action'])) {
			$this->data['input_return_id_action'] = $this->request->post['input_return_id_action'];
		} else {
			$this->data['input_return_id_action'] = '';
		}
		
		if (isset($this->request->post['return_action_id'])) {
			$this->data['return_action_id'] = $this->request->post['return_action_id'];
		} else {
			$this->data['return_action_id'] = '';
		}
	}
	
	public function add_comment(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->get['discuss_return_id'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_return_list');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_seller_account_return_list->addReturnComment($this->request->post);
			
			$this->response->redirect($this->url->link('seller/account-return-list/detail', 'return_id=' . $this->request->post['discuss_return_id'] , 'SSL'));
		}
		
		if (isset($this->error['error_discuss_comment'])) {
			$this->data['error_discuss_comment'] = $this->error['error_discuss_comment'];
		} else {
			$this->data['error_discuss_comment'] = '';
		}
		
		if (isset($this->request->post['discuss_return_id'])) {
			$this->data['discuss_return_id'] = $this->request->post['discuss_return_id'];
		} else {
			$this->data['discuss_return_id'] = '';
		}
		
		if (isset($this->request->post['discuss_product_id'])) {
			$this->data['discuss_product_id'] = $this->request->post['discuss_product_id'];
		} else {
			$this->data['discuss_product_id'] = '';
		}
		
		if (isset($this->request->post['discuss_comment'])) {
			$this->data['discuss_comment'] = $this->request->post['discuss_comment'];
		} else {
			$this->data['discuss_comment'] = '';
		}
		
	}
	
	public function validate(){
		if ((utf8_strlen($this->request->post['discuss_comment']) < 2) ) {
			$this->error['error_discuss_comment'] = $this->language->get('error_discuss');
		}
		
		return !$this->error;
	}
	
	public function list_return_comments(){
		$this->load->model('seller/account_return_list');
		$this->data['return_comments'] = array();
		
		$return_comments = $this->model_seller_account_return_list->listReturnComments($this->request->get['return_id']);
		
		foreach($return_comments as $return_comment){
			$this->data['return_comments'][] = array(
				'return_id'  		=> $return_comment['return_id'],
				'seller_id'  		=> $return_comment['seller_id'],
				'product_id'  		=> $return_comment['product_id'],
				'comment'  			=> $return_comment['comment'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($return_comment['created_date'])),
				'flag'  			=> $return_comment['flag'],
				'firstname'  		=> $return_comment['firstname'],
				'nickname'  		=> $return_comment['nickname'],
				'created_by'  		=> $return_comment['created_by']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-return-comments');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function list_up_comments(){
		$this->load->model('seller/account_return_list');
		$this->data['return_comments'] = array();
		
		$return_comments = $this->model_seller_account_return_list->listUpComments($this->request->get['return_id']);
		
		foreach($return_comments as $return_comment){
			$this->data['return_comments'][] = array(
				'return_id'  		=> $return_comment['return_id'],
				'seller_id'  		=> $return_comment['seller_id'],
				'product_id'  		=> $return_comment['product_id'],
				'comment'  			=> $return_comment['comment'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($return_comment['created_date'])),
				'flag'  			=> $return_comment['flag'],
				'firstname'  		=> $return_comment['firstname'],
				'nickname'  		=> $return_comment['nickname'],
				'created_by'  		=> $return_comment['created_by']
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-return-comments');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
}

?>
