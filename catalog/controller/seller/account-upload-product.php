<?php
class ControllerSellerAccountUploadProduct extends ControllerSellerAccount {
	private $error = array();

	public function index() {
		$this->load->language('upload/product');

		$this->load->model('seller/seller');
		$this->load->model('seller/upload_product');
		
		$this->document->setTitle($this->language->get('ms_account_dashboard_upload_product'));

		$this->data['heading_title'] = $this->language->get('ms_account_dashboard_upload_product');

		$this->data['text_loading'] = $this->language->get('text_loading');

		$this->data['entry_upload'] = $this->language->get('entry_upload');
		$this->data['entry_overwrite'] = $this->language->get('entry_overwrite');
		$this->data['entry_progress'] = $this->language->get('entry_progress');
		$this->data['entry_choose_file'] = $this->language->get('entry_choose_file');
		$this->data['help_upload'] = $this->language->get('help_upload');
		$this->data['help_seller'] = $this->language->get('help_seller');
		$this->data['help_product'] = $this->language->get('help_product');

		$this->data['button_upload'] = $this->language->get('button_upload');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_seller'] = $this->language->get('text_seller');
		$this->data['text_choose'] = $this->language->get('text_choose');
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
				array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', 'SSL'),
				),
				array(
					'text' => $this->language->get('ms_account_dashboard_upload_product'),
					'href' => $this->url->link('seller/account-upload-product', '', 'SSL')
				)
			));

		$directories = glob(DIR_UPLOAD_PRODUCT . 'temp-*', GLOB_ONLYDIR);

		if ($directories) {
			$this->data['error_warning'] = $this->language->get('error_temporary');
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		$this->data['categories'] = $this->model_seller_seller->getCategories();
		$this->data['sellers'] = $this->model_seller_upload_product->getSellers();

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-upload-product');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function upload_file() {
		$json = array();
		
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				// $extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = array("xls", "xlsx");

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . md5(mt_rand());

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_UPLOAD_PRODUCT . $file); //pake enkripsi penamaan file nya
			// move_uploaded_file($this->request->files['file']['tmp_name'], DIR_UPLOAD_PRODUCT . $filename);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_success_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function uploadProcess(){
		// $this->validate(__FUNCTION__);
		$this->data = $this->request->post;
		
		$json = array();
		$this->load->language('upload/product');
		$this->load->model('seller/upload_product');
		$arr_ai = array();
		$arr_category = array();
		$arr_sku = array();
		$sku_duplicate = false;
		$arr_image = array();
		
		if (empty($this->data['category_id'])) {
			$json['errors']['category_id'][] = $this->language->get('error_category'); 
		} else if(!empty($this->data['category_id'])){
			/*validasi attribute group id*/
			$att_group_id = $this->model_seller_upload_product->getAttGroupId($this->data['category_id']);
			
			//if($att_group_id==0){
				//$json['errors']['error_common'][] = $this->language->get('error_agi') . '<br/>';
			//}else{
				
				/*get category path*/
				$category_paths = $this->model_seller_upload_product->getCategoryPath($this->data['category_id']);
				foreach($category_paths as $category_path){
					array_push($arr_category, $category_path['path_id']);
				}
				
				/*get attribute id*/
				$att_id = $this->model_seller_upload_product->getAttId($att_group_id);
				foreach($att_id as $ai){
					array_push($arr_ai, $ai['attribute_id']);
				}
				
				/*count attribute id*/
				$count_att_id = $this->model_seller_upload_product->countAttId($att_group_id);
				$arr_column_ai = array();
				
				if (empty($this->data['filename'])) {
					$json['errors']['error_common'][] = $this->language->get('error_upload'); 
				} else if(!empty($this->data['filename'])){
					/* validasi attribute name */
					$row_data = $this->model_seller_upload_product->readHeaderExcel($this->data['filename']);
					
					$temp_att_id = 0;
					for($y=1; $y < ($count_att_id*2); $y++){
						
						// $is_exist = false;
						$attr_name = str_replace("_", " ", ($row_data[0][(32+$y)]));
						
						if($y % 2 == 0){
							$language_id = 2;
						}else{
							$language_id = 1;
						}
						
						for($z=0; $z < count($arr_ai); $z++){
							if($temp_att_id != 0 && $language_id == 2){
								$check_attr = $this->model_seller_upload_product->getAttDescription($attr_name, $temp_att_id, $language_id);
								$temp_att_id = 0;
								$z = count($arr_ai);
								$arr_column_ai[] = ($y + 32).'-'.$check_attr.'-'.$language_id;
							} else if($temp_att_id != 0 && $language_id == 1){
								$check_attr = $this->model_seller_upload_product->getAttDescription($attr_name, $arr_ai[$z], $language_id);
								
								if($check_attr != 0){
									$temp_att_id = $check_attr;
									$z = count($arr_ai);
									$arr_column_ai[] = ($y + 32).'-'.$check_attr.'-'.$language_id;
								}
							} else{
								$check_attr = $this->model_seller_upload_product->getAttDescription($attr_name, $arr_ai[$z], $language_id);
								
								if($check_attr != 0){
									$temp_att_id = $check_attr;
									$z = count($arr_ai);
									$arr_column_ai[] = ($y + 32).'-'.$check_attr.'-'.$language_id;
								}
							}
						}
						
						if($check_attr == 0){
							$json['errors']['error_common'][] = $this->language->get('error_attr_name') . $this->model_seller_upload_product->getColumnExcel(32+$y) . '<br/>';
						}
					}
					
					/* validasi format file */
					$check_format = true;
//$this->model_seller_upload_product->validateExcel($this->data['filename'], ($count_att_id*2));
					if(!$check_format){
						$json['errors']['error_common'][] = $this->language->get('error_format') . '<br/>';
					}
				}
			//}
		}
		
		if (empty($this->data['filename'])) {
			$json['errors']['error_common'][] = $this->language->get('error_upload'); 
		} else if(!empty($this->data['filename'])){
			$row_data = $this->model_seller_upload_product->readExcel($this->data['filename']);
			
			for($x = 0; $x < count($row_data); $x++){
				/* validasi sku */
				if(empty($row_data[$x][10])){ 
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(10) . '<br/>';
				}else if(!empty($row_data[$x][10])){
					$check_sku = $this->model_seller_upload_product->checkSku($this->MsLoader->MsSeller->getSellerId() . $row_data[$x][10]);
				
					if(!$check_sku){
						$json['errors']['error_common'][] = $this->language->get('error_sku') . ($x+2) . '<br/>';
					}
					array_push($arr_sku, $row_data[$x][10]);
				}
				
				for($y = 0; $y <= count($arr_sku)-1; $y++){
					if(($y != $x) && ($arr_sku[$y] == $row_data[$x][10])){
						$sku_duplicate = true;
					}
				}
				
				if($sku_duplicate){
					$json['errors']['error_common'][] = $this->language->get('error_sku') . ($x+2) . '<br/>';
				}
				
				/*validasi manufacturer*/
				if(!empty($row_data[$x][16])){
					$manufacturer_id = $this->model_seller_upload_product->getManufacturerId($row_data[$x][16]);
					if(!$manufacturer_id){
						$json['errors']['error_common'][] = $this->language->get('error_manufacturer') . ($x+2) . '<br/>';
					}
				}else if(empty($row_data[$x][16])){
					$manufacturer_id = 0;
				}
				
				/*validasi cell value*/
				if(empty($row_data[$x][12])){ //p.quantity
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(12) . '<br/>';
				}else if(!empty($row_data[$x][12])){
					if(!is_numeric($row_data[$x][12])){	//quantity product
						$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(12) . '<br/>';
					}
				}
				
				if(empty($row_data[$x][17])){ //p.price
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(17) . '<br/>';
				}else if(!empty($row_data[$x][17])){
					if(!is_numeric($row_data[$x][17])){	//price product
						$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(17) . '<br/>';
					}
				}
				
				if(!empty($row_data[$x][18]) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row_data[$x][18])){ //date available product
					$json['errors']['error_common'][] = $this->language->get('error_date') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(18) . '<br/>';
				}
				
				if(empty($row_data[$x][19])){ //p.weight
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(19) . '<br/>';
				}else if(!empty($row_data[$x][19])){
					if(!is_numeric($row_data[$x][19])){ //weight product
						$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(19) . '<br/>';
					}
				}
				
				if(!empty($row_data[$x][23])){
					if(!is_numeric($row_data[$x][23])){ //minimum product
						$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(23) . '<br/>';
					}
				}
				
				if(!empty($row_data[$x][20]) && !is_numeric($row_data[$x][20])){ //length product
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(20) . '<br/>';
				}
				if(!empty($row_data[$x][21]) && !is_numeric($row_data[$x][21])){ //width product
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(21) . '<br/>';
				}
				if(!empty($row_data[$x][22]) && !is_numeric($row_data[$x][22])){ //height product
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(22) . '<br/>';
				}
				if(!empty($row_data[$x][24]) && !is_numeric($row_data[$x][24])){ //quantity discount
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(24) . '<br/>';
				}
				if(!empty($row_data[$x][25]) && !is_numeric($row_data[$x][25])){ //priority discount
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(25) . '<br/>';
				}
				if(!empty($row_data[$x][26]) && !is_numeric($row_data[$x][26])){ //price discount
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(26) . '<br/>';
				}
				if(!empty($row_data[$x][27]) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row_data[$x][27])){ //start date discount
					$json['errors']['error_common'][] = $this->language->get('error_date') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(27) . '<br/>';
				}
				if(!empty($row_data[$x][28]) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row_data[$x][28])){ //end date discount
					$json['errors']['error_common'][] = $this->language->get('error_date') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(28) . '<br/>';
				}
				if(!empty($row_data[$x][29]) && !is_numeric($row_data[$x][29])){ //priority special
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(29) . '<br/>';
				}
				if(!empty($row_data[$x][30]) && !is_numeric($row_data[$x][30])){ //price special
					$json['errors']['error_common'][] = $this->language->get('error_numeric') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(30) . '<br/>';
				}
				if(!empty($row_data[$x][31]) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row_data[$x][31])){ //start date discount
					$json['errors']['error_common'][] = $this->language->get('error_date') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(31) . '<br/>';
				}
				if(!empty($row_data[$x][32]) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row_data[$x][32])){ //start date discount
					$json['errors']['error_common'][] = $this->language->get('error_date') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(32) . '<br/>';
				}
				
				/*check empty value*/
				//product description
				if(empty($row_data[$x][0])){ //pd.name1
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(0) . '<br/>';
				}
				if(empty($row_data[$x][1])){ //pd.name2
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(1) . '<br/>';
				}
				if(empty($row_data[$x][2])){ //pd.description1
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(2) . '<br/>';
				}
				if(empty($row_data[$x][3])){ //pd.description2
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(3) . '<br/>';
				}
				if(empty($row_data[$x][4])){ //pd.tag
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(4) . '<br/>';
				}
				if(empty($row_data[$x][5])){ //pd.meta title
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(5) . '<br/>';
				}
				
				//product
				if(empty($row_data[$x][8])){ //p.model
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(8) . '<br/>';
				}
				if(empty($row_data[$x][9])){ //p.code
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(9) . '<br/>';
				}
				if(empty($row_data[$x][13])){ //p.image
					$json['errors']['error_common'][] = $this->language->get('error_blank') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(13) . '<br/>';
				}
				if(!empty($row_data[$x][13])){
					$arr_image = explode(".", $row_data[$x][13]);
					if(strtolower($arr_image[count($arr_image)-1]) != "jpg" && strtolower($arr_image[count($arr_image)-1]) != "jpeg" && strtolower($arr_image[count($arr_image)-1]) != "png"){
						$json['errors']['error_common'][] = $this->language->get('error_image') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(13) . '<br/>';
					}
				}
				unset($arr_image);
				
				if(!empty($row_data[$x][14])){
					$arr_image = explode(".", $row_data[$x][14]);
					if(strtolower($arr_image[count($arr_image)-1]) != "jpg" && strtolower($arr_image[count($arr_image)-1]) != "jpeg" && strtolower($arr_image[count($arr_image)-1]) != "png"){
						$json['errors']['error_common'][] = $this->language->get('error_image') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(14) . '<br/>';
					}
				}
				unset($arr_image);
				
				if(!empty($row_data[$x][15])){
					$arr_image = explode(".", $row_data[$x][15]);
					if(strtolower($arr_image[count($arr_image)-1]) != "jpg" && strtolower($arr_image[count($arr_image)-1]) != "jpeg" && strtolower($arr_image[count($arr_image)-1]) != "png"){
						$json['errors']['error_common'][] = $this->language->get('error_image') . ($x+2) . ' column ' . $this->model_seller_upload_product->getColumnExcel(15) . '<br/>';
					}
				}
				unset($arr_image);
			}
		}
		
		// if (empty($this->data['seller_id'])) {
			// $json['errors']['seller_id'][] = $this->language->get('error_seller'); 
		// }
		
		if (empty($json['errors']) && empty($json['error'])) {
			$row_data = $this->model_seller_upload_product->readExcel($this->data['filename']);
			for($x = 0; $x < count($row_data); $x++){
				
				$product_id = $this->model_seller_upload_product->saveUpload(
					array(
						'category_id' 		=> $this->data['category_id'],
						'ms.seller_id' 		=> $this->MsLoader->MsSeller->getSellerId(),
						
						//oc_product
						'p.model' 			=> $row_data[$x][8],
						'p.code' 			=> $row_data[$x][9],
						'p.sku' 			=> $this->MsLoader->MsSeller->getSellerId() . $row_data[$x][10],
						'p.location' 		=> $row_data[$x][11],
						'p.quantity' 		=> $row_data[$x][12],
						'p.image' 			=> 'sellers/'.$this->MsLoader->MsSeller->getSellerId().'/'.$row_data[$x][13],
						'p.manufacturer_id' => $manufacturer_id,
						'p.price' 			=> $row_data[$x][17],
						'p.date_available' 	=> $row_data[$x][18],
						'p.weight' 			=> $row_data[$x][19],
						'p.length' 			=> $row_data[$x][20],
						'p.width' 			=> $row_data[$x][21],
						'p.height' 			=> $row_data[$x][22],
						'p.minimum' 		=> $row_data[$x][23],
						
						//oc_product_description
						'pd.name1' 			=> $row_data[$x][0],
						'pd.name2' 			=> $row_data[$x][1],
						'pd.description1' 	=> $row_data[$x][2],
						'pd.description2' 	=> $row_data[$x][3],
						'pd.tag' 			=> $row_data[$x][4],
						'pd.meta_title' 	=> $row_data[$x][5],
						'pd.meta_description' => $row_data[$x][6],
						'pd.meta_keyword' 	=> $row_data[$x][7],
							
						//oc_product_image
						'pi.image2' 		=> 'sellers/'.$this->MsLoader->MsSeller->getSellerId().'/'.$row_data[$x][14],
						'pi.image3' 		=> 'sellers/'.$this->MsLoader->MsSeller->getSellerId().'/'.$row_data[$x][15],
						
						//oc_product_discount
						'pdi.quantity' 		=> $row_data[$x][24],
						'pdi.priority' 		=> $row_data[$x][25],
						'pdi.price' 		=> $row_data[$x][26],
						'pdi.date_start' 	=> $row_data[$x][27],
						'pdi.date_end' 		=> $row_data[$x][28],
						
						//oc_product_special
						'ps.priority' 		=> $row_data[$x][29],
						'ps.price' 			=> $row_data[$x][30],
						'ps.date_start' 	=> $row_data[$x][31],
						'ps.date_end' 		=> $row_data[$x][32]
					)
				);
				
				for($y=0; $y < count($arr_column_ai); $y++){
					$this->data_att = explode("-",$arr_column_ai[$y]);
					
					$this->model_seller_upload_product->saveUploadAttr(
						array(
							'pa.product_id'		=> $product_id,
							'pa.text'			=> $row_data[$x][$this->data_att[0]],
							'pa.attribute_id'	=> $this->data_att[1],
							'pa.language_id'	=> $this->data_att[2]
						)
					);
				}
				
				for($z=0; $z < count($arr_category); $z++){
					$this->model_seller_upload_product->saveCategoryPath(
						array(
							'product_id'	=> $product_id,
							'path_id'		=> $arr_category[$z]
						)
					);
				}
			}
			
			$this->session->data['success'] = $this->language->get('text_success_save');
			unset($arr_sku);
		}
		
		$this->response->setOutput(json_encode($json));
	}
}

?>
