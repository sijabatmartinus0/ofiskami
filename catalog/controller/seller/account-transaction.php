<?php

class ControllerSellerAccountTransaction extends ControllerSellerAccount {
	public function index() {
		if((int)$this->MsLoader->MsSeller->getSellerType()==1 && (int)$this->MsLoader->MsSeller->getPremiumStatus()==2){
		$seller_id = $this->MsLoader->MsSeller->getSellerId();
		
		$seller_balance = $this->MsLoader->MsBalance->getSellerBalance($seller_id);
		$pending_funds = $this->MsLoader->MsBalance->getReservedSellerFunds($seller_id);
		$waiting_funds = $this->MsLoader->MsBalance->getWaitingSellerFunds($seller_id, 14);
		$balance_formatted = $this->currency->format($seller_balance,$this->config->get('config_currency'));
		
		$balance_reserved_formatted = $pending_funds > 0 ? sprintf($this->language->get('ms_account_balance_reserved_formatted'), $this->currency->format($pending_funds)) . ', ' : '';
		$balance_reserved_formatted .= $waiting_funds > 0 ? sprintf($this->language->get('ms_account_balance_waiting_formatted'), $this->currency->format($waiting_funds)) . ', ' : ''; 
		$balance_reserved_formatted = ($balance_reserved_formatted == '' ? '' : '(' . substr($balance_reserved_formatted, 0, -2) . ')');

		$this->data['ms_balance_formatted'] = $balance_formatted;
		$this->data['ms_reserved_formatted'] = $balance_reserved_formatted;
		
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$earnings = $this->MsLoader->MsSeller->getTotalEarnings($seller_id);

		$this->data['earnings'] = $this->currency->format($earnings, $this->config->get('config_currency'));
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->data['link_list_payments'] = $this->url->link('seller/account-transaction/getPaymentData', 'page=1', 'SSL');
		$this->data['link_list_reports'] = $this->url->link('seller/account-transaction/getReportData', 'page=1', 'SSL');
		$this->data['link_list_balances'] = $this->url->link('seller/account-transaction/getBalanceData', 'page=1', 'SSL');
		$this->data['search_payment'] = $this->url->link('seller/account-transaction/searchPaymentData', 'page=1', 'SSL');
		$this->data['search_report'] = $this->url->link('seller/account-transaction/searchReportData', 'page=1', 'SSL');
		$this->data['search_balance'] = $this->url->link('seller/account-transaction/searchBalanceData', 'page=1', 'SSL');
		$this->data['link_report_payment'] = $this->url->link('seller/account-transaction/link_report_payment', '', 'SSL');
		$this->data['link_report_transaction'] = $this->url->link('seller/account-transaction/link_report_transaction', '', 'SSL');
		$this->data['link_report_balance'] = $this->url->link('seller/account-transaction/link_report_balance', '', 'SSL');
		
		$this->data['batchs'] = $this->MsLoader->MsPayment->getBatchs($seller_id);
		
		$this->document->setTitle($this->language->get('ms_account_transactions_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_transactions_breadcrumbs'),
				'href' => $this->url->link('seller/account-transaction', '', 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-transaction');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
		}else{
			$this->response->redirect($this->url->link('seller/account-noroles', '', 'SSL'));
		}
	}
	
	// public function getPaymentData() {
		// $colMap = array(
			// 'seller' => 'ms.nickname',
			// 'type' => 'payment_type',
			// 'description' => 'mpay.description',
			// 'date_created' => 'mpay.date_created',
			// 'date_paid' => 'mpay.date_paid'
		// );
		
		// $seller_id = $this->MsLoader->MsSeller->getSellerId();
		
		// $sorts = array('payment_type', 'payment_id', 'amount', 'payment_status', 'date_created');
		// $filters = array_diff(array_merge($sorts, array('description')), array('payment_status', 'type'));
	
		// list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		// $filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);
	
		// $results = $this->MsLoader->MsPayment->getPayments(
			// array(
				// 'seller_id' => $seller_id
			// ),
			// array(
				// 'order_by'  => $sortCol,
				// 'order_way' => $sortDir,
				// 'filters' => $filterParams,
				// 'offset' => $this->request->get['iDisplayStart'],
				// 'limit' => $this->request->get['iDisplayLength']
			// )
		// );
	
		// $total = isset($results[0]) ? $results[0]['total_rows'] : 0;
	
		// $columns = array();
		// foreach ($results as $result) {
			// if ($result['payment_status'] == MsPayment::STATUS_UNPAID && $result['payment_type'] == MsPayment::TYPE_SALE) {
				// $total--;
				// continue;
			// }
			
			// $columns[] = array_merge(
				// $result,
				// array(
					// 'payment_type' => ($result['payment_type'] == MsPayment::TYPE_SALE ? $this->language->get('ms_payment_type_' . $result['payment_type']) . ' (' . sprintf($this->language->get('ms_payment_order'), $result['order_id']) . ')' : $this->language->get('ms_payment_type_' . $result['payment_type'])),
					// 'amount' => $this->currency->format(abs($result['amount']),$result['currency_code']),
					// 'description' => (mb_strlen($result['mpay.description']) > 80 ? mb_substr($result['mpay.description'], 0, 80) . '...' : $result['mpay.description']),
					// 'payment_status' => $this->language->get('ms_payment_status_' . $result['payment_status']),
					// 'date_created' => date($this->language->get('date_format_short'), strtotime($result['mpay.date_created'])),
				// )
			// );
		// }
		
		// $this->response->setOutput(json_encode(array(
			// 'iTotalRecords' => $total,
			// 'iTotalDisplayRecords' => $total,
			// 'aaData' => $columns
		// )));
	// }
	
	public function getPaymentData() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-transaction#tab-payment', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['payments'] = array();
		$this->data['search_payment'] = $this->url->link('seller/account-transaction/searchPaymentData', 'page=1', 'SSL');
		$this->data['search_report'] = $this->url->link('seller/account-transaction/searchReportData', 'page=1', 'SSL');
		$this->data['search_balance'] = $this->url->link('seller/account-transaction/searchBalanceData', 'page=1', 'SSL');
		$this->data['link_report_payment'] = $this->url->link('seller/account-transaction/link_report_payment', '', 'SSL');
		$this->data['link_report_transaction'] = $this->url->link('seller/account-transaction/link_report_transaction', '', 'SSL');
		$this->data['link_report_balance'] = $this->url->link('seller/account-transaction/link_report_balance', '', 'SSL');
		
		$payment_total = $this->model_seller_account_transaction->countPaymentTransaction();
		
		$results = $this->model_seller_account_transaction->loadPaymentTransactions(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));

		foreach ($results as $result) {
			$this->data['payments'][] = array(
				'company'  			=> $result['company'],
				'firstname'   		=> $result['firstname'],
				'email' 			=> $result['email'],
				'payment_type'	 	=> $result['payment_type'],
				'payment_status'	=> $result['payment_status'],
				'amount'			=> $this->currency->format($result['amount'], $result['currency_code'], $result['currency_value']),
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($result['date_created'])),
				'date_paid' 		=> date($this->language->get('date_format_short'), strtotime($result['date_paid']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $payment_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-transaction/getPaymentData', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($payment_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($payment_total - $this->config->get('config_product_limit'))) ? $payment_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $payment_total, ceil($payment_total / $this->config->get('config_product_limit')));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-payments');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function searchPaymentData() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-transaction#tab-payment', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_payment'])) {
			$data['date_start_payment'] = $this->request->get['date_start_payment'];
		} else {
			$data['date_start_payment'] = '';
		}
		
		if (isset($this->request->get['date_end_payment'])) {
			$data['date_end_payment'] = $this->request->get['date_end_payment'];
			
		} else {
			$data['date_end_payment'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['payments'] = array();

		/*pagination*/
		$payment_per_page = 10;
		if ((int)$payment_per_page == 0)
			$payment_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$results = $this->model_seller_account_transaction->searchPaymentTransactions(
			array(
				'date_start_payment' 	=> $data['date_start_payment'],
				'date_end_payment' 		=> $data['date_end_payment']
			),
			array(
				'offset' => ($page - 1) * $payment_per_page,
				'limit' => $payment_per_page
			)
		);
		
		$total_payment = $this->model_seller_account_transaction->countSearchPaymentTransaction(
			array(
				'date_start_payment' 	=> $data['date_start_payment'],
				'date_end_payment' 		=> $data['date_end_payment']
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_payment;
		$pagination->page = $page;
		$pagination->limit = $payment_per_page;
		$pagination->url = $this->url->link('seller/account-transaction/searchPaymentData', 'page={page}&date_start_payment='.$data['date_start_payment'].'&date_end_payment='.$data['date_end_payment'], 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($results as $result) {
			$this->data['payments'][] = array(
				'company'  			=> $result['company'],
				'firstname'   		=> $result['firstname'],
				'email' 			=> $result['email'],
				'payment_type'	 	=> $result['payment_type'],
				'payment_status'	=> $result['payment_status'],
				'amount'			=> $this->currency->format($result['amount'], $result['currency_code'], $result['currency_value']),
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($result['date_created'])),
				'date_paid' 		=> date($this->language->get('date_format_short'), strtotime($result['date_paid']))
			);
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-payments');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
		
	public function getReportData() {
		$this->data['error']='';
		if (!isset($this->session->data['customer_id'])) {
			$this->data['error']=sprintf($this->language->get('error_session_timeout'),$this->url->link('account/login', '', 'SSL'));
		}else{
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['reports'] = array();
		$this->data['search_report'] = $this->url->link('seller/account-transaction/searchReportData', 'page=1', 'SSL');
		// $this->data['link_report_payment'] = $this->url->link('seller/account-transaction/link_report_payment', '', 'SSL');
		
		$report_total = $this->model_seller_account_transaction->countReportTransaction();
		
		$results = $this->model_seller_account_transaction->loadReportTransactions(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));

		foreach ($results as $detail) {
			$this->data['reports'][] = array(
				'summary_id' 			=> $detail['summary_id'],
				'batch_id' 				=> MsPayment::BATCH_PREFIX . $detail['seller_id'] . "-" . $detail['batch_id'],
				'order_id' 				=> $detail['order_id'],
				'order_detail_id' 		=> $detail['order_detail_id'],
				'invoice' 				=> $detail['invoice'],
				'kiosk_id' 				=> $detail['kiosk_id'],
				'kiosk_name' 			=> $detail['kiosk_name'],
				'seller_id' 			=> $detail['seller_id'],
				'nickname' 				=> $detail['nickname'],
				'tax_type' 			=> $detail['tax_type'],
				'payment_type' 			=> $detail['payment_type'],
				'transaction_date' 		=> date('d/m/Y H:i', strtotime($detail['transaction_date'])),
				'date_added' 			=> date('d/m/Y H:i', strtotime($detail['date_added'])),
				'product_id' 			=> $detail['product_id'],
				'quantity' 				=> $detail['quantity'],
				'sku' 					=> $detail['code'],
				'price' 				=> $this->currency->format($detail['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total' 				=> $this->currency->format($detail['total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 					=> $this->currency->format($detail['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'commission_base' 		=> $this->currency->format($detail['commission_base'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'shipping_fee' 			=> $this->currency->format($detail['shipping_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee'	=> $this->currency->format($detail['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission'		=> $this->currency->format($detail['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission_tax'	=> $this->currency->format($detail['store_commission_tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'vendor_balance'		=> $this->currency->format($detail['vendor_balance'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $report_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-transaction/getReportData', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($report_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($report_total - $this->config->get('config_product_limit'))) ? $report_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $report_total, ceil($report_total / $this->config->get('config_product_limit')));
		}
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-reports');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function searchReportData() {
		$this->data['error']='';
		if (!isset($this->session->data['customer_id'])) {
			$this->data['error']=sprintf($this->language->get('error_session_timeout'),$this->url->link('account/login', '', 'SSL'));
		}else{
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_report'])) {
			$data['date_start_report'] = $this->request->get['date_start_report'];
		} else {
			$data['date_start_report'] = '';
		}
		
		if (isset($this->request->get['date_end_report'])) {
			$data['date_end_report'] = $this->request->get['date_end_report'];
			
		} else {
			$data['date_end_report'] = '';
		}
		
		if (isset($this->request->get['batch'])) {
			$data['batch'] = $this->request->get['batch'];
			
		} else {
			$data['batch'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['reports'] = array();

		/*pagination*/
		$report_per_page = 10;
		if ((int)$report_per_page == 0)
			$report_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$results = $this->model_seller_account_transaction->searchReportTransactions(
			array(
				'date_start_report' 	=> $data['date_start_report'],
				'date_end_report' 		=> $data['date_end_report'],
				'batch' 		=> $data['batch']
			),
			array(
				'offset' => ($page - 1) * $report_per_page,
				'limit' => $report_per_page
			)
		);
		
		$total_report = $this->model_seller_account_transaction->countSearchReportTransaction(
			array(
				'date_start_report' 	=> $data['date_start_report'],
				'date_end_report' 		=> $data['date_end_report'],
				'batch' 		=> $data['batch']
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_report;
		$pagination->page = $page;
		$pagination->limit = $report_per_page;
		$pagination->url = $this->url->link('seller/account-transaction/searchReportData', 'page={page}&date_start_report='.$data['date_start_report'].'&date_end_report='.$data['date_end_report'], 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($results as $detail) {
			$this->data['reports'][] = array(
				'summary_id' 			=> $detail['summary_id'],
				'batch_id' 				=> MsPayment::BATCH_PREFIX . $detail['seller_id'] . "-" . $detail['batch_id'],
				'order_id' 				=> $detail['order_id'],
				'order_detail_id' 		=> $detail['order_detail_id'],
				'invoice' 				=> $detail['invoice'],
				'kiosk_id' 				=> $detail['kiosk_id'],
				'kiosk_name' 			=> $detail['kiosk_name'],
				'seller_id' 			=> $detail['seller_id'],
				'nickname' 				=> $detail['nickname'],
				'tax_type' 			=> $detail['tax_type'],
				'payment_type' 			=> $detail['payment_type'],
				'transaction_date' 		=> date('d/m/Y H:i', strtotime($detail['transaction_date'])),
				'date_added' 			=> date('d/m/Y H:i', strtotime($detail['date_added'])),
				'product_id' 			=> $detail['product_id'],
				'quantity' 				=> $detail['quantity'],
				'sku' 					=> $detail['code'],
				'price' 				=> $this->currency->format($detail['price'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'total' 				=> $this->currency->format($detail['total'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'tax' 					=> $this->currency->format($detail['tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'commission_base' 		=> $this->currency->format($detail['commission_base'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'shipping_fee' 			=> $this->currency->format($detail['shipping_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'online_payment_fee'	=> $this->currency->format($detail['online_payment_fee'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission'		=> $this->currency->format($detail['store_commission'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'store_commission_tax'	=> $this->currency->format($detail['store_commission_tax'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode())),
				'vendor_balance'		=> $this->currency->format($detail['vendor_balance'], $this->currency->getCode(), $this->currency->getValue($this->currency->getCode()))
			);
		}
		}
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-reports');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}	
	
	public function link_report_payment(){
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_payment'])) {
			$data['date_start_payment'] = $this->request->get['date_start_payment'];
		} else {
			$data['date_start_payment'] = '';
		}
		
		if (isset($this->request->get['date_end_payment'])) {
			$data['date_end_payment'] = $this->request->get['date_end_payment'];
			
		} else {
			$data['date_end_payment'] = '';
		}
		
		$this->model_seller_account_transaction->reportPaymentTransactions(
			array(
				'date_start_payment' 	=> $data['date_start_payment'],
				'date_end_payment' 		=> $data['date_end_payment']
			)
		);
	}	
	
	public function link_report_transaction(){
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_report'])) {
			$data['date_start_report'] = $this->request->get['date_start_report'];
		} else {
			$data['date_start_report'] = '';
		}
		
		if (isset($this->request->get['date_end_report'])) {
			$data['date_end_report'] = $this->request->get['date_end_report'];
			
		} else {
			$data['date_end_report'] = '';
		}
		
		if (isset($this->request->get['batch'])) {
			$data['batch'] = $this->request->get['batch'];
			
		} else {
			$data['batch'] = '';
		}
		
		$this->model_seller_account_transaction->reportTransactions(
			array(
				'date_start_report' 	=> $data['date_start_report'],
				'date_end_report' 		=> $data['date_end_report'],
				'batch' 		=> $data['batch']
			)
		);
	}	

	public function link_report_balance(){
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_balance'])) {
			$data['date_start_balance'] = $this->request->get['date_start_balance'];
		} else {
			$data['date_start_balance'] = '';
		}
		
		if (isset($this->request->get['date_end_balance'])) {
			$data['date_end_balance'] = $this->request->get['date_end_balance'];
			
		} else {
			$data['date_end_balance'] = '';
		}
		
		$this->model_seller_account_transaction->reportBalanceTransactions(
			array(
				'date_start_balance' 	=> $data['date_start_balance'],
				'date_end_balance' 		=> $data['date_end_balance']
			)
		);
	}	
	
	// public function getTransactionData() {
		// $seller_id = $this->MsLoader->MsSeller->getSellerId();
		
		// $colMap = array(
			// 'transaction_id' => 'balance_id',
			// 'seller' => '`nickname`',
			// 'description' => 'mb.description',
			// 'date_created' => 'mb.date_created'
		// );
		
		// $sorts = array('transaction_id', 'seller', 'amount', 'date_created');
		// $filters = array_merge($sorts, array('description'));
	
		// list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		// $filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);
	
		// $results = $this->MsLoader->MsBalance->getBalanceEntries(
			// array(
				// 'seller_id' => $seller_id
			// ),
			// array(
				// 'order_by'  => $sortCol,
				// 'order_way' => $sortDir,
				// 'filters' => $filterParams,
				// 'offset' => $this->request->get['iDisplayStart'],
				// 'limit' => $this->request->get['iDisplayLength']
			// )
		// );
	
		// $total = isset($results[0]) ? $results[0]['total_rows'] : 0;
	
		// $columns = array();
		// foreach ($results as $result) {
			// $columns[] = array_merge(
				// $result,
				// array(
					// 'transaction_id' => $result['balance_id'],
					// 'amount' => $this->currency->format($result['amount'], $this->config->get('config_currency')),
					// 'description' => (mb_strlen($result['mb.description']) > 80 ? mb_substr($result['mb.description'], 0, 80) . '...' : $result['mb.description']),
					// 'date_created' => date($this->language->get('date_format_short'), strtotime($result['mb.date_created'])),
				// )
			// );
		// }
	
		// $this->response->setOutput(json_encode(array(
				// 'iTotalRecords' => $total,
				// 'iTotalDisplayRecords' => $total,
				// 'aaData' => $columns
		// )));
	// }	
	
	public function getBalanceData() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-transaction#tab-balance', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['balances'] = array();
		$this->data['search_balance'] = $this->url->link('seller/account-transaction/searchBalanceData', 'page=1', 'SSL');
		
		$balance_total = $this->model_seller_account_transaction->countBalanceTransaction();
		
		$results = $this->model_seller_account_transaction->loadBalanceTransactions(($page - 1) * $this->config->get('config_product_limit'), $this->config->get('config_product_limit'));
		$no = 1;
		foreach ($results as $result) {
			$this->data['balances'][] = array(
				'no'  				=> $no,
				'invoice'  			=> $result['invoice'],
				'description'   	=> $result['description'],
				'tran_code' 		=> $result['tran_code'],
				'amount'			=> $this->currency->format($result['amount'], $result['currency_code'], $result['currency_value']),
				'balance'			=> $this->currency->format($result['balance'], $result['currency_code'], $result['currency_value']),
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($result['date_created']))
			);
			$no++;
		}

		$pagination = new Pagination();
		$pagination->total = $balance_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('seller/account-transaction/getBalanceData', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($balance_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($balance_total - $this->config->get('config_product_limit'))) ? $balance_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $balance_total, ceil($balance_total / $this->config->get('config_product_limit')));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-balances');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function searchBalanceData() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-transaction#tab-balance', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_transaction');
		
		if (isset($this->request->get['date_start_balance'])) {
			$data['date_start_balance'] = $this->request->get['date_start_balance'];
		} else {
			$data['date_start_balance'] = '';
		}
		
		if (isset($this->request->get['date_end_balance'])) {
			$data['date_end_balance'] = $this->request->get['date_end_balance'];
			
		} else {
			$data['date_end_balance'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['balances'] = array();

		/*pagination*/
		$balance_per_page = 10;
		if ((int)$balance_per_page == 0)
			$balance_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$results = $this->model_seller_account_transaction->searchBalanceTransactions(
			array(
				'date_start_balance' 	=> $data['date_start_balance'],
				'date_end_balance' 		=> $data['date_end_balance']
			),
			array(
				'offset' => ($page - 1) * $balance_per_page,
				'limit' => $balance_per_page
			)
		);
		
		$total_balance = $this->model_seller_account_transaction->countSearchBalanceTransaction(
			array(
				'date_start_balance' 	=> $data['date_start_balance'],
				'date_end_balance' 		=> $data['date_end_balance']
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_balance;
		$pagination->page = $page;
		$pagination->limit = $balance_per_page;
		$pagination->url = $this->url->link('seller/account-transaction/searchBalanceData', 'page={page}&date_start_balance='.$data['date_start_balance'].'&date_end_balance='.$data['date_end_balance'], 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		$no = 1;
		foreach ($results as $result) {
			$this->data['balances'][] = array(
				'no'  				=> $no,
				'invoice'  			=> $result['invoice'],
				'description'   	=> $result['description'],
				'tran_code' 		=> $result['tran_code'],
				'amount'			=> $this->currency->format($result['amount'], $result['currency_code'], $result['currency_value']),
				'balance'			=> $this->currency->format($result['balance'], $result['currency_code'], $result['currency_value']),
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($result['date_created']))
			);
			$no++;
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-balances');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
}

?>
