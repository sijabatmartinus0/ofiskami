<?php
class ControllerSellerAccountPendingOrder extends ControllerSellerAccount {
	private $error = array();
	
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');
		
		$this->document->setTitle($this->language->get('ms_account_dashboard_nav_pending_order'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_nav_pending_order'),
				'href' => $this->url->link('seller/account-pending-order', '', 'SSL'),
			)
		));
		
		$this->load->model('seller/account_pending_order');
		$this->load->model('tool/image');
		
		// $this->data['action_insert_receipt'] = $this->url->link('seller/account-pending-order/insert_receipt', '', 'SSL');
		// $this->data['action_edit_receipt'] = $this->url->link('seller/account-pending-order/edit_receipt', '', 'SSL');
		$this->data['action_insert_receipt'] = $this->url->link('seller/account-pending-order/insert_receipt_order', '', 'SSL');
		$this->data['action_edit_receipt'] = $this->url->link('seller/account-pending-order/edit_receipt_order', '', 'SSL');
		
		$this->data['list_order_release'] = $this->url->link('seller/account-pending-order/list_order_release', 'page=1', 'SSL');
		$this->data['action_accept_pickup'] = $this->url->link('seller/account-pending-order/accept_pickup', '', 'SSL');
		$this->data['list_pending_orders'] = $this->url->link('seller/account-pending-order/list_pending_orders', 'page=1', 'SSL');
		$this->data['search_order_release'] = $this->url->link('seller/account-pending-order/search_order_release', 'page=1', 'SSL');
		$this->data['search_account_shipping_status'] = $this->url->link('seller/account-pending-order/search_account_shipping_status', 'page=1', 'SSL');
		$this->data['account_shipping_status'] = $this->url->link('seller/account-pending-order/account_shipping_status', 'page=1', 'SSL');
		$this->data['view_detail'] = $this->url->link('seller/account-pending-order/detail_order_release', '', 'SSL');
		$this->data['print'] = $this->url->link('seller/account-pending-order/print_order', '', 'SSL');
		
		//list shippings
		$this->load->model('localisation/shipping');
		$this->data['logistics'] = $this->model_localisation_shipping->getShipping();
		
		// $this->data['error']=$this->session->data['error'];
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-pending-order');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function response_many(){
		$json=array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(isset($this->request->post['check_accept'])){
				$accepts=$this->request->post['check_accept'];
				$this->load->model('seller/account_pending_order');
				foreach($accepts as $accept){
					$this->data=explode("-", $accept);
					if(count($this->data)==2){
						$this->request->post['rbt_response']=1;
						$this->request->post['input_order_id']=$this->data[0];
						$this->request->post['input_order_detail_id']=$this->data[1];
						$this->model_seller_account_pending_order->insertResponseOrder($this->request->post);
					}
				}
				$this->session->data['success'] = $this->language->get('text_success_response');
				$this->session->data['error']='';
				$json['url']=$this->url->link('seller/account-pending-order#tab-confirm-shipping', '', 'SSL');
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function list_pending_orders(){
		$this->data['print'] = $this->url->link('seller/account-pending-order/print_order', '', 'SSL');

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order/list-pending-orders', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_pending_order');
		$this->load->model('account/order');
		$this->load->model('tool/image');
		
		$this->data['action'] = $this->url->link('seller/account-pending-order/list_pending_orders', '', 'SSL');
		$this->data['action_many'] = $this->url->link('seller/account-pending-order/response_many', '', 'SSL');
		
		$this->data['update_response_many'] = $this->url->link('seller/account-pending-order/response_many', '', 'SSL');
		/*View Pending Order*/
			
		/*header shipping*/
		
		$this->data['header_shippings'] = array();
		$this->data['products'] = array();
		
		
		/*pagination*/
		$orders_per_page = 5;
		if ((int)$orders_per_page == 0)
			$orders_per_page = 5;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$header_shippings = $this->model_seller_account_pending_order->headerShippingData(
			'3',
			array(
				'offset' => ($page - 1) * $orders_per_page,
				'limit' => $orders_per_page
			)
		);
		
		$total_order = $this->model_seller_account_pending_order->countPendingOrders();
		
		$pagination = new Pagination();
		$pagination->total = $total_order;
		$pagination->page = $page;
		$pagination->limit = $orders_per_page; 
		$pagination->url = $this->url->link('seller/account-pending-order/list_pending_orders', '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($header_shippings as $header_shipping) {
			/*data common shipping*/
			$common_shipping = $this->model_seller_account_pending_order->loadShippingData($header_shipping['order_id'], $header_shipping['invoice_prefix'].$header_shipping['invoice_no'], '3');
			
			if ($common_shipping['shipping_address_format']) {
				$format = $common_shipping['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $common_shipping['shipping_firstname'],
				'lastname'  => $common_shipping['shipping_lastname'],
				'company'   => $common_shipping['shipping_company'],
				'address_1' => $common_shipping['shipping_address_1'],
				'address_2' => $common_shipping['shipping_address_2'],
				'city'      => $common_shipping['shipping_city'],
				'postcode'  => $common_shipping['shipping_postcode'],
				'zone'      => $common_shipping['shipping_zone'],
				'zone_code' => $common_shipping['shipping_zone_code'],
				'country'   => $common_shipping['shipping_country']
			);
			
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}' . "\n" . "{pp_email}" . " (" . "{pp_telephone}" . ")";
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $common_shipping['pp_name'],
				'company' 		=> $common_shipping['company'],
				'pp_branch_name'=> $common_shipping['pp_branch_name'],
				'address'      	=> $common_shipping['address'],
				'subdistrict_pp'=> $common_shipping['subdistrict_pp'],
				'district_pp'   => $common_shipping['district_pp'],
				'city_pp' 		=> $common_shipping['city_pp'],
				'zone_pp'      	=> $common_shipping['zone_pp'],
				'postcode' 		=> $common_shipping['postcode'],
				'country_pp'  	=> $common_shipping['country_pp'],
				'pp_email'  	=> $common_shipping['pp_email'],
				'pp_telephone'  => $common_shipping['pp_telephone']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($header_shipping['order_detail_id']);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($header_shipping['order_detail_id']);
			$total_weight = $this->model_seller_account_pending_order->getTotalWeight($header_shipping['order_detail_id']);
			$weight = $this->model_seller_account_pending_order->getWeightClass($header_shipping['order_detail_id']);
			
			$this->data['header_shippings'][] = array(
				'buyer'    				=> $header_shipping['firstname']." ".$header_shipping['lastname'],
				'invoice_no'   			=> $header_shipping['invoice_prefix'].$header_shipping['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($header_shipping['date_added'])),
				'order_id'				=> $header_shipping['order_id'],
				'order_detail_id'		=> $header_shipping['order_detail_id'],
				'sla_response'			=> $header_shipping['sla_response'],
				'sla_shipping'			=> $header_shipping['sla_shipping'],
				'shipping_address'		=> $shipping_address,
				'shipping_price'		=> $this->currency->format($common_shipping['shipping_price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'shipping_insurance'	=> $common_shipping['shipping_insurance'],
				'shipping_service_name'	=> $common_shipping['shipping_service_name'],
				'shipping_name'			=> $common_shipping['shipping_name'],
				'shipping_id'			=> $common_shipping['shipping_id'],
				'pp_branch_name'		=> $common_shipping['pp_branch_name'],
				'address_pp'			=> $address_pp,
				'delivery_type'			=> $common_shipping['delivery_type'],
				'delivery_type_id'		=> $common_shipping['delivery_type_id'],
				'payment_method'		=> $common_shipping['payment_method'],
				'total_price'			=> $this->currency->format($header_shipping['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_qty'				=> $total_qty,
				'total_weight'			=> $total_weight,
				'weight'				=> $weight,
				'language_id'			=> $header_shipping['language_id']
			);
			
			/*data product*/
			$products = $this->model_seller_account_pending_order->loadProductData($header_shipping['order_id'], $header_shipping['order_detail_id']);
			$this->load->model('tool/image');
			foreach ($products as $product) {	
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
						
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
					
				$this->data['products'][] = array(
					'order_detail_id'		=> $product['order_detail_id'],
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'total'					=> $this->currency->format($product['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'price'					=> $this->currency->format($product['price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'comment'				=> $common_shipping['comment'],
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id'])	
				);
			}
		}
		
		// response order
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_seller_account_pending_order->insertResponseOrder($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success_response');
			$this->session->data['error']='';
			$this->response->redirect($this->url->link('seller/account-pending-order#tab-confirm-shipping', '', 'SSL'));
		}
		
		if (isset($this->request->post['input_order_detail_id'])) {
			$this->data['input_order_detail_id'] = $this->request->post['input_order_detail_id'];
		} else {
			$this->data['input_order_detail_id'] = '';
		}
		
		if (isset($this->request->post['rbt_response'])) {
			$this->data['rbt_response'] = $this->request->post['rbt_response'];
		} else {
			$this->data['rbt_response'] = '';
		}
		
		if (isset($this->request->post['rbt_reason'])) {
			$this->data['rbt_reason'] = $this->request->post['rbt_reason'];
		} else {
			$this->data['rbt_reason'] = '';
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-list-pending-order');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	/*public function response_many(){
		$this->load->model('seller/account_pending_order');
		
		if (isset($this->request->post['check_accept'])) {
			$this->data['check_accept'] = $this->request->post['check_accept'];
		} else {
			$this->data['check_accept'] = '';
		}
		
		$count = $this->data['check_accept'];
		
		for($x=0; $x < count($count); $x++){
			$order_info = explode("-", $count[$x]);
			$order_id = $order_info[0];
			$order_detail_id = $order_info[1];
			
			$this->model_seller_account_pending_order->insertResponseOrderMany($order_detail_id, $order_id);
		}
		
		$this->session->data['success'] = $this->language->get('text_success_response');
		$this->response->redirect($this->url->link('seller/account-pending-order', '', 'SSL'));
	}*/
	
	// public function insert_receipt_many(){
	// 	$json=array();
	// 	if ($this->request->server['REQUEST_METHOD'] == 'POST') {
	// 		if(isset($this->request->post['check_confirm'])){
	// 			$confirms=$this->request->post['check_confirm'];
	// 			$this->load->model('seller/account_pending_order');
				
	// 			foreach($confirms as $confirm){
	// 				$this->data=explode("-", $confirm);
	// 				if(count($this->data)==3){
	// 					if((int)$this->data[2]==1){
	// 						if(isset($this->request->post['input_receipt_number'.$this->data[1]]) && (utf8_strlen($this->request->post['input_receipt_number'.$this->data[1]]) < 1)){
	// 							$json['error'][]=array(
	// 							'id'=>$this->data[1],
	// 							'message'=>$this->language->get('error_receipt_number')
	// 							);
	// 						}
	// 					}
	// 				}
	// 			}
				
	// 			if(!isset($json['error'])){
	// 				foreach($confirms as $confirm){
	// 					$this->data=explode("-", $confirm);
	// 					if(count($this->data)==3){
	// 						if((int)$this->data[2]==1){
	// 							$input=array(
	// 								'order_detail_id_release'=>$this->data[1],
	// 								'rbt_change_delivery'=>0,
	// 								'input_delivery_agent'=>0,
	// 								'input_shipping_service'=>0,
	// 								'input_receipt_number'=>$this->request->post['input_receipt_number'.$this->data[1]],
	// 								'input_order_id_release'=>$this->data[0],
	// 								'input_order_detail_id_release'=>$this->data[1]
	// 							);
	// 							$this->model_seller_account_pending_order->insertReceiptNumber($input);
	// 						}else if((int)$this->data[2]==2){
	// 							$input=array(
	// 									'input_order_id_release'=>$this->data[0],
	// 									'input_order_detail_id_release'=>$this->data[1]
	// 								);
	// 							$this->model_seller_account_pending_order->acceptPickup($input);
	// 						}
	// 					}
	// 				}
	// 				$this->session->data['success'] = $this->language->get('text_success_confirm');
	// 				$this->session->data['error']='';
	// 				$json['url']=$this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL');
	// 			}
	// 		}
	// 	}
		
	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
	
	public function insert_receipt_many(){
		$json=array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
                    if(isset($this->request->post['check_confirm'])){
                            
				$confirms=$this->request->post['check_confirm'];
				$this->load->model('seller/account_pending_order');
				foreach($confirms as $confirm){
					$this->data=explode("-", $confirm);
					if(count($this->data)==3){
                                            
						if((int)$this->data[2]==1 || (int)$this->data[2]==2){
                                                    
							if(isset($this->request->post['input_receipt_number'.$this->data[1]]) && (utf8_strlen($this->request->post['input_receipt_number'.$this->data[1]]) < 1)){
								
                                                            $json['error'][]=array(
								'id'=>$this->data[1],
								'message'=>$this->language->get('error_receipt_number')
								);
							}
						}
					}
				}
				
				if(!isset($json['error'])){
                                
					foreach($confirms as $confirm){
						$this->data=explode("-", $confirm);
						if(count($this->data)==3){
							if((int)$this->data[2]==1){
								$input=array(
									'order_detail_id_release'=>$this->data[1],
									'rbt_change_delivery'=>0,
									'input_delivery_agent'=>0,
									'input_shipping_service'=>0,
									'input_receipt_number'=>$this->request->post['input_receipt_number'.$this->data[1]],
									'input_order_id_release'=>$this->data[0],
									'input_order_detail_id_release'=>$this->data[1]
								);
								$this->model_seller_account_pending_order->insertReceiptNumber($input);
							}else if((int)$this->data[2]==2){
								$input=array(
										'input_order_id_release'=>$this->data[0],
										'input_order_detail_id_release'=>$this->data[1],
                                                                                'input_receipt_number_pickup'=>$this->request->post['input_receipt_number'.$this->data[1]]
									);
								$this->model_seller_account_pending_order->acceptPickup($input);
							}
						}
					}
					$this->session->data['success'] = $this->language->get('text_success_confirm');
					$this->session->data['error']='';
					$json['url']=$this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL');
				}
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function insert_receipt_order(){
		$json=array();
		$this->load->model('seller/account_pending_order');
		
		if ($this->customer->isLogged()) {
				if((utf8_strlen($this->request->post['input_receipt_number']) < 1) ){
					$json['error']['input_receipt_number']=$this->language->get('error_receipt_number');
				}
				
				if(!isset($this->request->post['rbt_change_delivery'])){
					$this->request->post['rbt_change_delivery']=0;
				}
				if((int)$this->request->post['rbt_change_delivery']==1){
					if(utf8_strlen($this->request->post['input_shipping_service'])<1 || $this->request->post['input_shipping_service']==0){
						$json['error']['input_shipping_service']=$this->language->get('error_shipping_service');
					}
				}
				
				
				
				if(!isset($json['error'])){
					$this->model_seller_account_pending_order->insertReceiptNumber($this->request->post);
					$this->session->data['success'] = $this->language->get('text_success_confirm');
					$this->session->data['error']='';
					$json['url']=$this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL');
				}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function insert_receipt(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_pending_order');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			if(!isset($this->request->post['input_receipt_number'])){
				$this->request->post['input_receipt_number']=$this->request->post['input_receipt_number'.$this->request->post['input_order_detail_id_release']];
			}else{
				$this->request->post['input_receipt_number']=$this->request->post['input_receipt_number'];
			}
			$this->model_seller_account_pending_order->insertReceiptNumber($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success_confirm');
			$this->session->data['error']='';
			$this->response->redirect($this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL'));
		}
		
		if (isset($this->error['input_receipt_number'])) {
			$this->data['error_input_receipt_number'] = $this->error['input_receipt_number'];
		} else {
			$this->data['error_input_receipt_number'] = '';
		}
		
		if (isset($this->request->post['input_receipt_number'.$this->request->post['input_order_detail_id_release']])) {
			$this->data['input_receipt_number'.$this->request->post['input_order_detail_id_release']] = $this->request->post['input_receipt_number'.$this->request->post['input_order_detail_id_release']];
		} else {
			$this->data['input_receipt_number'.$this->request->post['input_order_detail_id_release']] = '';
		}
		if (isset($this->request->post['input_receipt_number'])) {
			$this->data['input_receipt_number'] = $this->request->post['input_receipt_number'];
		} else {
			$this->data['input_receipt_number'] = '';
		}
		
		if (isset($this->request->post['input_order_detail_id_release'])) {
			$this->data['input_order_detail_id_release'] = $this->request->post['input_order_detail_id_release'];
		} else {
			$this->data['input_order_detail_id_release'] = '';
		}
		
		if (isset($this->request->post['input_order_id_release'])) {
			$this->data['input_order_id_release'] = $this->request->post['input_order_id_release'];
		} else {
			$this->data['input_order_id_release'] = '';
		}
		
		if (isset($this->request->post['input_delivery_agent'])) {
			$this->data['input_delivery_agent'] = $this->request->post['input_delivery_agent'];
		} else {
			$this->data['input_delivery_agent'] = '';
		}
		
		if (isset($this->request->post['input_shipping_service'])) {
			$this->data['input_shipping_service'] = $this->request->post['input_shipping_service'];
		} else {
			$this->data['input_shipping_service'] = '';
		}
		
		if (isset($this->request->post['rbt_change_delivery'])) {
			$this->data['rbt_change_delivery'] = $this->request->post['rbt_change_delivery'];
		} else {
			$this->data['rbt_change_delivery'] = '';
		}
		if($this->error){
			$this->session->data['error'] = $this->error['input_receipt_number'];
			$this->response->redirect($this->url->link('seller/account-pending-order#tab-confirm-shipping', '', 'SSL'));
		}
	}
	
	public function edit_receipt_order(){
		$json=array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if((utf8_strlen($this->request->post['input_receipt_number']) < 1) ){
				$json['error']['edit_receipt_number']=$this->language->get('error_receipt_number');
			}
			
			if(!isset($json['error'])){
				$this->load->model('seller/account_pending_order');
				$this->model_seller_account_pending_order->insertReceiptNumber($this->request->post);
				$this->session->data['success'] = $this->language->get('text_success_edit');
				$this->session->data['error']='';
				$json['url']=$this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL');
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function edit_receipt(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-status-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_pending_order');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_seller_account_pending_order->insertReceiptNumber($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success_edit');
			$this->session->data['error']='';
			$this->response->redirect($this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL'));
		}
		
		if (isset($this->error['input_receipt_number'])) {
			$this->data['error_input_receipt_number'] = $this->error['input_receipt_number'];
		} else {
			$this->data['error_input_receipt_number'] = '';
		}
		
		if (isset($this->request->post['input_receipt_number'])) {
			$this->data['input_receipt_number'] = $this->request->post['input_receipt_number'];
		} else {
			$this->data['input_receipt_number'] = '';
		}
		
		if (isset($this->request->post['input_order_detail_id_release'])) {
			$this->data['input_order_detail_id_release'] = $this->request->post['input_order_detail_id_release'];
		} else {
			$this->data['input_order_detail_id_release'] = '';
		}
		
		if (isset($this->request->post['input_order_id_release'])) {
			$this->data['input_order_id_release'] = $this->request->post['input_order_id_release'];
		} else {
			$this->data['input_order_id_release'] = '';
		}
		
	}
	

	public function accept_pickup(){
            
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('seller/account_pending_order');
                
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
                    
		if((utf8_strlen($this->request->post['input_receipt_number_pickup']) < 1) ){
                    $json['error']['input_receipt_number']=$this->language->get('error_receipt_number');
				}
                                
                                
		if(!isset($json['error'])){
             $this->model_seller_account_pending_order->acceptPickup($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success_confirm');
			$this->session->data['error']='';
//			$this->response->redirect($this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL'));
                        $json['url']=$this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL');
                    }
                        
                    }
                
		if (isset($this->error['input_receipt_number'])) {
			$this->data['error_input_receipt_number'] = $this->error['input_receipt_number'];
		} else {
			$this->data['error_input_receipt_number'] = '';
		}
                
		if (isset($this->request->post['input_order_detail_id_release'])) {
			$this->data['input_order_detail_id_release'] = $this->request->post['input_order_detail_id_release'];
		} else {
			$this->data['input_order_detail_id_release'] = '';
		}
		
		if (isset($this->request->post['input_order_id_release'])) {
			$this->data['input_order_id_release'] = $this->request->post['input_order_id_release'];
		} else {
			$this->data['input_order_id_release'] = '';
		}
                
                
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	
	}
	// public function accept_pickup(){
	// 	if (!$this->customer->isLogged()) {
	// 		$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

	// 		$this->response->redirect($this->url->link('account/login', '', 'SSL'));
	// 	}
		
	// 	$this->load->model('seller/account_pending_order');
		
	// 	if ($this->request->server['REQUEST_METHOD'] == 'POST') {
	// 		$this->model_seller_account_pending_order->acceptPickup($this->request->post);
	// 		$this->session->data['success'] = $this->language->get('text_success_confirm');
	// 		$this->session->data['error']='';
	// 		$this->response->redirect($this->url->link('seller/account-pending-order#tab-status-shipping', '', 'SSL'));
	// 	}
		
	// 	if (isset($this->request->post['input_order_detail_id_release'])) {
	// 		$this->data['input_order_detail_id_release'] = $this->request->post['input_order_detail_id_release'];
	// 	} else {
	// 		$this->data['input_order_detail_id_release'] = '';
	// 	}
		
	// 	if (isset($this->request->post['input_order_id_release'])) {
	// 		$this->data['input_order_id_release'] = $this->request->post['input_order_id_release'];
	// 	} else {
	// 		$this->data['input_order_id_release'] = '';
	// 	}
	// }
	
	public function validate(){
		if(isset($this->request->post['input_receipt_number'.$this->request->post['input_order_detail_id_release']])){
			if((utf8_strlen($this->request->post['input_receipt_number'.$this->request->post['input_order_detail_id_release']]) < 1)){
				$this->error['input_receipt_number'] = $this->language->get('error_receipt_number');
			}
		}else{
			if ((utf8_strlen($this->request->post['input_receipt_number']) < 1) ) {
				$this->error['input_receipt_number'] = $this->language->get('error_receipt_number');
			}
		}
		return !$this->error;
	}
	
	public function shipping_service() {
		$json = array();

		$this->load->model('localisation/shipping');

		$shipping_service = $this->model_localisation_shipping->getShippingById($this->request->get['shipping_id']);

		if ($shipping_service) {
			$json = array(
				'shipping_id'   => $shipping_service['shipping_id'],
				'name'        	=> $shipping_service['name'],
				'services'		=> $this->model_localisation_shipping->getShippingServiceById($this->request->get['shipping_id'])
			);
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function list_order_release(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		/* List Order Release */
		$this->load->model('seller/account_pending_order');
		$this->data['confirmed_orders'] = array();
		$this->data['action_insert_receipt'] = $this->url->link('seller/account-pending-order/insert_receipt', '', 'SSL');
		$this->data['action_accept_pickup'] = $this->url->link('seller/account-pending-order/accept_pickup', '', 'SSL');
		$this->data['print'] = $this->url->link('seller/account-pending-order/print_order', '', 'SSL');
		
		/*pagination*/
		$orders_per_page = 10;
		if ((int)$orders_per_page == 0)
			$orders_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$confirmed_orders = $this->model_seller_account_pending_order->loadListOrderRelease(
			array(
				'offset' => ($page - 1) * $orders_per_page,
				'limit' => $orders_per_page
			)
		);
		
		$total_order = $this->model_seller_account_pending_order->countLoadListOrderRelease('18');
		
		$pagination = new Pagination();
		$pagination->total = $total_order;
		$pagination->page = $page;
		$pagination->limit = $orders_per_page; 
		$pagination->url = $this->url->link('seller/account-pending-order/list_order_release', '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($confirmed_orders as $confirmed_order) {
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($confirmed_order['order_detail_id']);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($confirmed_order['order_detail_id']);
			
			$this->data['confirmed_orders'][] = array(
				'buyer'    				=> $confirmed_order['firstname']." ".$confirmed_order['lastname'],
				'invoice_no'   			=> $confirmed_order['invoice_prefix'].$confirmed_order['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($confirmed_order['date_added'])),
				'order_id'				=> $confirmed_order['order_id'],
				'order_detail_id'		=> $confirmed_order['order_detail_id'],
				'sla_response'			=> $confirmed_order['sla_response'],
				'sla_shipping'			=> $confirmed_order['sla_shipping'],
				'receiver'				=> "a/n: <b>" . $confirmed_order['shipping_firstname'] . "</b><br/>" . $confirmed_order['shipping_address_1'] . ", " . $confirmed_order['shipping_district'] . " " . $confirmed_order['shipping_city'] . "<br/>" . $confirmed_order['shipping_zone'] ,
				'receiver_pickup'		=> "a/n: <b>" . $confirmed_order['pp_name'] . "</b><br/>" . $confirmed_order['pp_email'] . "<br/>" . $confirmed_order['pp_telephone'] ,
				'shipping_price'		=> $this->currency->format($confirmed_order['shipping_price'], $confirmed_order['currency_code'], $confirmed_order['currency_value']),
				'asal'	=> 'asal',
				'shipping_insurance'	=> $confirmed_order['shipping_insurance'],
				'shipping_service_name'	=> $confirmed_order['shipping_service_name'],
				'shipping_name'			=> $confirmed_order['shipping_name'],
				'shipping_id'			=> $confirmed_order['shipping_id'],
				'pp_branch_name'		=> $confirmed_order['pp_branch_name'],
				'delivery_type'			=> $confirmed_order['delivery_type'],
				'delivery_type_id'		=> $confirmed_order['delivery_type_id'],
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $confirmed_order['currency_code'], $confirmed_order['currency_value']),
				'total_qty'				=> $total_qty
				
			);
		}
		
		//list shippings
		$this->load->model('localisation/shipping');
		$this->data['logistics'] = $this->model_localisation_shipping->getShipping();
		
		$this->data['action_many'] = $this->url->link('seller/account-pending-order/insert_receipt_many', '', 'SSL');
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-release');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function search_order_release(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		/* List Order Release */
		$this->load->model('seller/account_pending_order');
		$this->data['confirmed_orders'] = array();
		$this->data['action_insert_receipt'] = $this->url->link('seller/account-pending-order/insert_receipt', '', 'SSL');
		$this->data['action_accept_pickup'] = $this->url->link('seller/account-pending-order/accept_pickup', '', 'SSL');
		$this->data['action_many'] = $this->url->link('seller/account-pending-order/response_many', '', 'SSL');
		$this->data['print'] = $this->url->link('seller/account-pending-order/print_order', '', 'SSL');
		
		if (isset($this->request->get['search_invoice'])) {
			$this->data['search_invoice'] = $this->request->get['search_invoice'];
		} else {
			$this->data['search_invoice'] = '';
		}
		
		if (isset($this->request->get['search_deadline'])) {
			$this->data['search_deadline'] = $this->request->get['search_deadline'];
		} else {
			$this->data['search_deadline'] = '';
		}
		
		if (isset($this->request->get['search_delivery_agent'])) {
			$this->data['search_delivery_agent'] = $this->request->get['search_delivery_agent'];
		} else {
			$this->data['search_delivery_agent'] = '';
		}
		
		/*pagination*/
		$orders_per_page = 10;
		if ((int)$orders_per_page == 0)
			$orders_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$confirmed_orders = $this->model_seller_account_pending_order->searchListOrderRelease(
			array(
				'invoice' 			=> $this->data['search_invoice'],
				'deadline' 			=> $this->data['search_deadline'],
				'delivery_agent' 	=> $this->data['search_delivery_agent']
			),
			array(
				'offset' => ($page - 1) * $orders_per_page,
				'limit' => $orders_per_page
			)
		);
		
		$total_order = $this->model_seller_account_pending_order->countSearchListOrderRelease(
			array(
				'invoice' 			=> $this->data['search_invoice'],
				'deadline' 			=> $this->data['search_deadline'],
				'delivery_agent' 	=> $this->data['search_delivery_agent']
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_order;
		$pagination->page = $page;
		$pagination->limit = $orders_per_page; 
		$pagination->url = $this->url->link('seller/account-pending-order/search_order_release', '&page={page}&search_invoice='.$this->data['search_invoice']. '&search_deadline=' . $this->data['search_deadline']. '&search_delivery_agent='. $this->data['search_delivery_agent'], 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($confirmed_orders as $confirmed_order) {
			
			$total_price = $this->model_seller_account_pending_order->totalPriceProductByOrderId($confirmed_order['order_id']);
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($confirmed_order['order_detail_id']);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($confirmed_order['order_detail_id']);
			
			$this->data['confirmed_orders'][] = array(
				'buyer'    				=> $confirmed_order['firstname']." ".$confirmed_order['lastname'],
				'invoice_no'   			=> $confirmed_order['invoice_prefix'].$confirmed_order['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($confirmed_order['date_added'])),
				'order_id'				=> $confirmed_order['order_id'],
				'order_detail_id'		=> $confirmed_order['order_detail_id'],
				'sla_response'			=> $confirmed_order['sla_response'],
				'sla_shipping'			=> $confirmed_order['sla_shipping'],
				'receiver'				=> "a/n: <b>" . $confirmed_order['shipping_firstname'] . "</b><br/>" . $confirmed_order['shipping_address_1'] . ", " . $confirmed_order['shipping_district'] . " " . $confirmed_order['shipping_city'] . "<br/>" . $confirmed_order['shipping_zone'] ,
				'receiver_pickup'		=> "a/n: <b>" . $confirmed_order['pp_name'] . "</b><br/>" . $confirmed_order['pp_email'] . "<br/>" . $confirmed_order['pp_telephone'] ,
				'shipping_price'		=> $this->currency->format($confirmed_order['shipping_price'], $confirmed_order['currency_code'], $confirmed_order['currency_value']),
				'shipping_insurance'	=> $confirmed_order['shipping_insurance'],
				'shipping_service_name'	=> $confirmed_order['shipping_service_name'],
				'shipping_name'			=> $confirmed_order['shipping_name'],
				'shipping_id'			=> $confirmed_order['shipping_id'],
				'pp_branch_name'		=> $confirmed_order['pp_branch_name'],
				'delivery_type'			=> $confirmed_order['delivery_type'],
				'delivery_type_id'		=> $confirmed_order['delivery_type_id'],
				'total_price'			=> $this->currency->format($total_price, $confirmed_order['currency_code'], $confirmed_order['currency_value']),
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $confirmed_order['currency_code'], $confirmed_order['currency_value']),
				'total_qty'				=> $total_qty
				
			);
		}
		
		//list shippings
		$this->load->model('localisation/shipping');
		$this->data['logistics'] = $this->model_localisation_shipping->getShipping();
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-release');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function detail_order_release(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-confirm-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->data['header_shippings'] = array();
		$this->data['products'] = array();
		
		if (isset($this->request->get['order_detail_id_release'])) {
			$this->data['order_detail_id_release'] = $this->request->get['order_detail_id_release'];
		} else {
			$this->data['order_detail_id_release'] = '';
		}
		
		if (isset($this->request->get['order_id_release'])) {
			$this->data['order_id_release'] = $this->request->get['order_id_release'];
		} else {
			$this->data['order_id_release'] = '';
		}
		
		$this->load->model('seller/account_pending_order');
		$this->load->model('account/order');
		$order_detail_id = $this->data['order_detail_id_release'];
		$order_id = $this->data['order_id_release'];
		
		$header_shippings = $this->model_seller_account_pending_order->headerShippingDataById($order_detail_id);
		
		foreach ($header_shippings as $header_shipping) {
			/*data common shipping*/
			$common_shipping = $this->model_seller_account_pending_order->loadShippingDataById($order_detail_id);
			
			if ($common_shipping['shipping_address_format']) {
				$format = $common_shipping['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $common_shipping['shipping_firstname'],
				'lastname'  => $common_shipping['shipping_lastname'],
				'company'   => $common_shipping['shipping_company'],
				'address_1' => $common_shipping['shipping_address_1'],
				'address_2' => $common_shipping['shipping_address_2'],
				'city'      => $common_shipping['shipping_city'],
				'postcode'  => $common_shipping['shipping_postcode'],
				'zone'      => $common_shipping['shipping_zone'],
				'zone_code' => $common_shipping['shipping_zone_code'],
				'country'   => $common_shipping['shipping_country']
			);
			
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}' . "\n" . "{pp_email}" . " (" . "{pp_telephone}" . ")";
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $common_shipping['pp_name'],
				'company' 		=> $common_shipping['company'],
				'pp_branch_name'=> $common_shipping['pp_branch_name'],
				'address'      	=> $common_shipping['address'],
				'subdistrict_pp'=> $common_shipping['subdistrict_pp'],
				'district_pp'   => $common_shipping['district_pp'],
				'city_pp' 		=> $common_shipping['city_pp'],
				'zone_pp'      	=> $common_shipping['zone_pp'],
				'postcode' 		=> $common_shipping['postcode'],
				'country_pp'  	=> $common_shipping['country_pp'],
				'pp_email'  	=> $common_shipping['pp_email'],
				'pp_telephone'  => $common_shipping['pp_telephone']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($order_detail_id);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($order_detail_id);
			$total_weight = $this->model_seller_account_pending_order->getTotalWeight($header_shipping['order_detail_id']);
			$weight = $this->model_seller_account_pending_order->getWeightClass($header_shipping['order_detail_id']);
			
			$this->data['header_shippings'][] = array(
				'buyer'    				=> $header_shipping['firstname']." ".$header_shipping['lastname'],
				'invoice_no'   			=> $header_shipping['invoice_prefix'].$header_shipping['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($header_shipping['date_added'])),
				'order_id'				=> $header_shipping['order_id'],
				'order_detail_id'		=> $header_shipping['order_detail_id'],
				'sla_response'			=> $header_shipping['sla_response'],
				'sla_shipping'			=> $header_shipping['sla_shipping'],
				'shipping_address'		=> $shipping_address,
				'shipping_price'		=> $this->currency->format($common_shipping['shipping_price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'shipping_insurance'	=> $common_shipping['shipping_insurance'],
				'shipping_service_name'	=> $common_shipping['shipping_service_name'],
				'shipping_name'			=> $common_shipping['shipping_name'],
				'shipping_id'			=> $common_shipping['shipping_id'],
				'pp_branch_name'		=> $common_shipping['pp_branch_name'],
				'address_pp'			=> $address_pp,
				'delivery_type'			=> $common_shipping['delivery_type'],
				'delivery_type_id'			=> $common_shipping['delivery_type_id'],
				'payment_method'		=> $common_shipping['payment_method'],
				'total_price'			=> $this->currency->format($header_shipping['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_qty'				=> $total_qty,
				'total_weight'			=> $total_weight,
				'weight'				=> $weight
			);
			
			/*data product*/
			$products = $this->model_seller_account_pending_order->loadProductData($order_id, $order_detail_id);
			$this->load->model('tool/image');
			foreach ($products as $product) {	
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
						
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
					
				$this->data['products'][] = array(
					'order_detail_id'		=> $product['order_detail_id'],
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'total'					=> $this->currency->format($product['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'price'					=> $this->currency->format($product['price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'comment'				=> $common_shipping['comment'],
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id'])	
				);
			}
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-release-detail');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function account_shipping_status(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-status-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->data['header_shippings'] = array();
		$this->data['products'] = array();
		
		$this->load->model('seller/account_pending_order');
		$this->load->model('account/order');
		
		/*pagination*/
		$orders_per_page = 5;
		if ((int)$orders_per_page == 0)
			$orders_per_page = 5;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$header_shippings = $this->model_seller_account_pending_order->headerShippingData(
			'19',
			array(
				'offset' => ($page - 1) * $orders_per_page,
				'limit' => $orders_per_page
			)
		);
		
		$total_order = $this->model_seller_account_pending_order->countLoadListOrderRelease('19');
		
		$pagination = new Pagination();
		$pagination->total = $total_order;
		$pagination->page = $page;
		$pagination->limit = $orders_per_page; 
		$pagination->url = $this->url->link('seller/account-pending-order/account_shipping_status', '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($header_shippings as $header_shipping) {
			/*data common shipping*/
			$common_shipping = $this->model_seller_account_pending_order->loadShippingData($header_shipping['order_id'], $header_shipping['invoice_prefix'].$header_shipping['invoice_no'], '19');
			
			if ($common_shipping['shipping_address_format']) {
				$format = $common_shipping['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $common_shipping['shipping_firstname'],
				'lastname'  => $common_shipping['shipping_lastname'],
				'company'   => $common_shipping['shipping_company'],
				'address_1' => $common_shipping['shipping_address_1'],
				'address_2' => $common_shipping['shipping_address_2'],
				'city'      => $common_shipping['shipping_city'],
				'postcode'  => $common_shipping['shipping_postcode'],
				'zone'      => $common_shipping['shipping_zone'],
				'zone_code' => $common_shipping['shipping_zone_code'],
				'country'   => $common_shipping['shipping_country']
			);
			
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}' . "\n" . "{pp_email}" . " (" . "{pp_telephone}" . ")";
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $common_shipping['pp_name'],
				'company' 		=> $common_shipping['company'],
				'pp_branch_name'=> $common_shipping['pp_branch_name'],
				'address'      	=> $common_shipping['address'],
				'subdistrict_pp'=> $common_shipping['subdistrict_pp'],
				'district_pp'   => $common_shipping['district_pp'],
				'city_pp' 		=> $common_shipping['city_pp'],
				'zone_pp'      	=> $common_shipping['zone_pp'],
				'postcode' 		=> $common_shipping['postcode'],
				'country_pp'  	=> $common_shipping['country_pp'],
				'pp_email'  	=> $common_shipping['pp_email'],
				'pp_telephone'  => $common_shipping['pp_telephone']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($header_shipping['order_detail_id']);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($header_shipping['order_detail_id']);
			$total_weight = $this->model_seller_account_pending_order->getTotalWeight($header_shipping['order_detail_id']);
			$weight = $this->model_seller_account_pending_order->getWeightClass($header_shipping['order_detail_id']);
			
			$this->data['header_shippings'][] = array(
				'buyer'    				=> $header_shipping['firstname']." ".$header_shipping['lastname'],
				'invoice_no'   			=> $header_shipping['invoice_prefix'].$header_shipping['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($header_shipping['date_added'])),
				'order_id'				=> $header_shipping['order_id'],
				'order_detail_id'		=> $header_shipping['order_detail_id'],
				'date_modified'			=> date('j F Y, H:i', strtotime($header_shipping['date_modified'])),
				'shipping_address'		=> $shipping_address,
				'shipping_price'		=> $this->currency->format($common_shipping['shipping_price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'shipping_insurance'	=> $common_shipping['shipping_insurance'],
				'shipping_service_name'	=> $common_shipping['shipping_service_name'],
				'shipping_name'			=> $common_shipping['shipping_name'],
				'shipping_id'			=> $common_shipping['shipping_id'],
				'pp_branch_name'		=> $common_shipping['pp_branch_name'],
				'address_pp'			=> $address_pp,
				'delivery_type'			=> $common_shipping['delivery_type'],
				'delivery_type_id'		=> $common_shipping['delivery_type_id'],
				'shipping_receipt_number'=> $common_shipping['shipping_receipt_number'],
				'payment_method'		=> $common_shipping['payment_method'],
				'total_price'			=> $this->currency->format($header_shipping['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_qty'				=> $total_qty,
				'total_weight'			=> $total_weight,
				'weight'				=> $weight
				
			);
			
			/*data product*/
			$products = $this->model_seller_account_pending_order->loadProductData($header_shipping['order_id'], $header_shipping['order_detail_id']);
			$this->load->model('tool/image');
			
			foreach ($products as $product) {	
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
						
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
					
				$this->data['products'][] = array(
					'order_detail_id'		=> $product['order_detail_id'],
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'total'					=> $this->currency->format($product['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'price'					=> $this->currency->format($product['price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'comment'				=> $common_shipping['comment'],
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id'])	
				);
			}
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-shipping-status');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function search_account_shipping_status(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('seller/account-pending-order#tab-status-shipping', '' , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->data['header_shippings'] = array();
		$this->data['products'] = array();
		
		if (isset($this->request->get['search_all'])) {
			$this->data['search_all'] = $this->request->get['search_all'];
		} else {
			$this->data['search_all'] = '';
		}
		
		$this->load->model('seller/account_pending_order');
		$this->load->model('account/order');
		
		/*pagination*/
		$orders_per_page = 5;
		if ((int)$orders_per_page == 0)
			$orders_per_page = 5;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$header_shippings = $this->model_seller_account_pending_order->searchAccountShippingStatus(
			array(
				'search_all' 		=> $this->data['search_all'],
			),
			array(
				'offset' => ($page - 1) * $orders_per_page,
				'limit' => $orders_per_page
			)
		);
		
		$total_order = $this->model_seller_account_pending_order->countSearchAccountShippingStatus(
			array(
				'search_all' 		=> $this->data['search_all'],
			)
		);
		
		$pagination = new Pagination();
		$pagination->total = $total_order;
		$pagination->page = $page;
		$pagination->limit = $orders_per_page; 
		$pagination->url = $this->url->link('seller/account-pending-order/search_account_shipping_status', '&page={page}&search_all=' . $this->data['search_all'], 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		foreach ($header_shippings as $header_shipping) {
			/*data common shipping*/
			$common_shipping = $this->model_seller_account_pending_order->loadShippingData($header_shipping['order_id'], $header_shipping['invoice_prefix'].$header_shipping['invoice_no'], '19');
			
			if ($common_shipping['shipping_address_format']) {
				$format = $common_shipping['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $common_shipping['shipping_firstname'],
				'lastname'  => $common_shipping['shipping_lastname'],
				'company'   => $common_shipping['shipping_company'],
				'address_1' => $common_shipping['shipping_address_1'],
				'address_2' => $common_shipping['shipping_address_2'],
				'city'      => $common_shipping['shipping_city'],
				'postcode'  => $common_shipping['shipping_postcode'],
				'zone'      => $common_shipping['shipping_zone'],
				'zone_code' => $common_shipping['shipping_zone_code'],
				'country'   => $common_shipping['shipping_country']
			);
			
			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}' . "\n" . "{pp_email}" . " (" . "{pp_telephone}" . ")";
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $common_shipping['pp_name'],
				'company' 		=> $common_shipping['company'],
				'pp_branch_name'=> $common_shipping['pp_branch_name'],
				'address'      	=> $common_shipping['address'],
				'subdistrict_pp'=> $common_shipping['subdistrict_pp'],
				'district_pp'   => $common_shipping['district_pp'],
				'city_pp' 		=> $common_shipping['city_pp'],
				'zone_pp'      	=> $common_shipping['zone_pp'],
				'postcode' 		=> $common_shipping['postcode'],
				'country_pp'  	=> $common_shipping['country_pp'],
				'pp_email'  	=> $common_shipping['pp_email'],
				'pp_telephone'  => $common_shipping['pp_telephone']
			);
			
			$address_pp = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$total_price_invoice = $this->model_seller_account_pending_order->totalPriceProductByInvoice($header_shipping['order_detail_id']);
			$total_qty = $this->model_seller_account_pending_order->totalQuantityProductByInvoice($header_shipping['order_detail_id']);
			$total_weight = $this->model_seller_account_pending_order->getTotalWeight($header_shipping['order_detail_id']);
			$weight = $this->model_seller_account_pending_order->getWeightClass($header_shipping['order_detail_id']);
			
			$this->data['header_shippings'][] = array(
				'buyer'    				=> $header_shipping['firstname']." ".$header_shipping['lastname'],
				'invoice_no'   			=> $header_shipping['invoice_prefix'].$header_shipping['invoice_no'],
				'date_added' 			=> date('j F Y', strtotime($header_shipping['date_added'])),
				'order_id'				=> $header_shipping['order_id'],
				'order_detail_id'		=> $header_shipping['order_detail_id'],
				'date_modified'			=> date('j F Y, H:i', strtotime($header_shipping['date_modified'])),
				'shipping_address'		=> $shipping_address,
				'shipping_price'		=> $this->currency->format($common_shipping['shipping_price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'shipping_insurance'	=> $common_shipping['shipping_insurance'],
				'shipping_service_name'	=> $common_shipping['shipping_service_name'],
				'shipping_name'			=> $common_shipping['shipping_name'],
				'shipping_id'			=> $common_shipping['shipping_id'],
				'pp_branch_name'		=> $common_shipping['pp_branch_name'],
				'address_pp'			=> $address_pp,
				'delivery_type'			=> $common_shipping['delivery_type'],
				'delivery_type_id'		=> $common_shipping['delivery_type_id'],
				'shipping_receipt_number'=> $common_shipping['shipping_receipt_number'],
				'payment_method'		=> $common_shipping['payment_method'],
				'total_price'			=> $this->currency->format($header_shipping['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_price_invoice'	=> $this->currency->format($total_price_invoice, $common_shipping['currency_code'], $common_shipping['currency_value']),
				'total_qty'				=> $total_qty,
				'total_weight'			=> $total_weight,
				'weight'				=> $weight
			);
			
			/*data product*/
			$products = $this->model_seller_account_pending_order->loadProductData($header_shipping['order_id'], $header_shipping['order_detail_id']);
			
			$this->load->model('tool/image');
			foreach ($products as $product) {	
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
						
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
					
				$this->data['products'][] = array(
					'order_detail_id'		=> $product['order_detail_id'],
					'product_name'    		=> $product['name'],
					'product_id'   			=> $product['product_id'],
					'quantity' 				=> $product['quantity'],
					'total'					=> $this->currency->format($product['total'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'price'					=> $this->currency->format($product['price'], $common_shipping['currency_code'], $common_shipping['currency_value']),
					'comment'				=> $common_shipping['comment'],
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id'])	
				);
			}
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-shipping-status');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function print_order(){
		$this->data['title'] = $this->language->get('ms_invoice');

		if ($this->request->server['HTTPS']) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}
		
		$this->data['direction'] = $this->language->get('direction');
		$this->data['lang'] = $this->language->get('code');
		
		$this->load->model('account/order');
		$this->load->model('seller/account_pending_order');
		$this->load->model('setting/setting');
		
		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		$order_info = $this->model_seller_account_pending_order->getOrderDetail($order_detail_id);
		
		if ($order_info) {
			
			if ($order_info['invoice_no']) {
				$this->data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$this->data['invoice_no'] = '';
			}
			
			$order_id = $order_info['order_id'];
			
			$this->data['qty'] = $this->model_account_order->getQuantity($order_detail_id);
			$this->data['order_id'] = $order_info['order_id'];
			$this->data['order_detail_id'] = $order_detail_id;
			$this->data['invoice'] = $order_info['invoice_prefix'].$order_info['invoice_no'];
			$this->data['delivery_type'] = $order_info['delivery_type'];
			$this->data['delivery_type_id'] = $order_info['delivery_type_id'];
			$delivery_type_id = $order_info['delivery_type_id'];
			$this->data['shipping_id'] = $order_info['shipping_id'];
			$this->data['shipping_name'] = $order_info['shipping_name'];
			$this->data['shipping_service_name'] = $order_info['shipping_service_name'];
			$this->data['shipping_insurance'] = $this->currency->getNumeric($order_info['shipping_insurance'], $order_info['currency_code'], $order_info['currency_value']);
			$this->data['shipping_price'] = $this->currency->getNumeric($order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			$this->data['pp_branch_id'] = $order_info['pp_branch_id'];
			$this->data['pp_branch_name'] = $order_info['pp_branch_name'];
			$this->data['shipping_receipt_number'] = $order_info['shipping_receipt_number'];
			$this->data['order_status_id'] = $order_info['order_status_id'];
			$this->data['email'] = $order_info['email'];
			$this->data['telephone'] = $order_info['telephone'];
			$this->data['store_name'] = $order_info['store_name'];
			$this->data['store_url'] = $order_info['store_url'];
			$this->data['total_price_invoice'] = $this->model_seller_account_pending_order->totalPriceProductByInvoice($order_detail_id);
			
			if($delivery_type_id != 3){
				$total_weight = $this->model_seller_account_pending_order->getTotalWeight($order_detail_id);
				$weight = $this->model_seller_account_pending_order->getWeightClass($order_detail_id);
				$this->data['total_weight'] = $total_weight;
				$this->data['weight'] = $weight;
			}
			
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$this->data['currency_code'] = $this->currency->getSymbolLeft($order_info['currency_code']);
			
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$this->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['payment_method'] = $order_info['payment_method'];
			$this->data['payment_name'] = $order_info['payment_firstname'] . $order_info['payment_lastname'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format =  '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . ", " . '{country}' ;
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country'],
				'telephone'   => $order_info['telephone']
			);

			$this->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			$this->data['shipping_name'] = $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'];
			$this->data['shipping_telp'] = $order_info['telephone'];
			
			//address pickup point
			
			$format_pp ='{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp} {postcode}' . "\n" . '{zone_pp}' . ", " . '{country_pp}' ;
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $order_info['pp_name'],
				'company' 		=> $order_info['company'],
				'pp_branch_name'=> $order_info['pp_branch_name'],
				'address'      	=> $order_info['address'],
				'subdistrict_pp'=> $order_info['subdistrict_pp'],
				'district_pp'   => $order_info['district_pp'],
				'city_pp' 		=> $order_info['city_pp'],
				'zone_pp'      	=> $order_info['zone_pp'],
				'postcode' 		=> $order_info['postcode'],
				'country_pp'  	=> $order_info['country_pp'],
				'pp_email'  	=> $order_info['pp_email'],
				'pp_telephone'  => $order_info['pp_telephone']
			);
			
			$this->data['address_pp'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			$this->data['pp_name'] = $order_info['pp_name'];
			$this->data['pp_telp'] = $order_info['pp_telephone'];
			
			$this->data['shipping_method'] = $order_info['shipping_method'];
			
			// Products
			$this->data['products'] = array();

			$products = $this->model_seller_account_pending_order->getProductOrder($order_id, $order_detail_id);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$this->data['products'][] = array(
					'product_name'     	=> $product['name'],
					'model'    			=> $product['model'],
					'option'   			=> $option_data,
					'sku'     			=> $product['sku'],
					'comment'     		=> $product['comment'],
					'href'	   			=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'nickname' 			=> $product['nickname'],
					'company' 			=> $product['company'],
					'quantity' 			=> $product['quantity'],
					'price'    			=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    			=> $this->currency->getNumeric($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			// Totals
			$subtotal = $this->model_account_order->getTotalPricePerInvoice($this->data['order_detail_id']);
			$this->data['subtotal']=$this->currency->getNumeric($subtotal);
			$this->data['total_price'] = $this->currency->getNumeric($subtotal+$order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			
			$this->data['text_subtotal'] = $this->language->get('text_subtotal');
			$this->data['text_total'] = $this->language->get('text_total');
			
			$this->data['comment'] = nl2br($order_info['comment']);
			
			$this->data['totals'] = array();
			
			$totals = $this->model_account_order->getOrderTotalsInvoice($order_id);
			
			foreach ($totals as $total) {
				$this->data['totals'][] = array(
					'title' 		=> $total['title'],
					'code' 			=> $total['code'],
					'value' 		=>$this->currency->getNumeric($total['value'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			//Seller
			$seller = $this->MsLoader->MsSeller->getSeller((int)$order_info['seller_id']);

			if (!$seller) {
				$this->data['seller'] = NULL;
			} else {
				$this->data['seller'] = array();
				
				$this->data['seller']['nickname'] = $seller['ms.nickname'];
				$this->data['seller']['seller_id'] = $seller['seller_id'];
				$this->data['seller']['company'] = $seller['ms.company'];
				$this->data['seller']['npwp'] = $seller['ms.npwp_number'];
				 
				$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
				
			}
			
			list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('print');
			$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
		}
	}
	
}

?>
