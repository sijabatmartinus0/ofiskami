<?php
class ControllerProductRentCar extends Controller {
	public function index() {
	
		$data['heading_title'] = "TRAC Rent Car";
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
		$this->document->setTitle("TRAC Rent Car");
		$data['logo']=$data['thumb'] = $this->model_tool_image->resize('trac.png', 140, 74);
		
		$data['breadcrumbs'][] = array(
			'text' => "TRAC Rent Car",
			'href' => $this->url->link('product/rentcar')
		);
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/rent_car.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/rent_car.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/rent_car.tpl', $data));
		}
	}
}
