<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');
		$this->load->model('product/product');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);


		if ($product_info) {
/*vqmod change*/
			$this->document->addScript('catalog/view/javascript/dialog-sellercontact.js');
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/multiseller.css');
			$data = array_merge($data, $this->load->language('multiseller/multiseller'));
			$this->load->model('localisation/country');
			$this->load->model('localisation/zone');
			$this->load->model('tool/image');

			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$seller = $this->MsLoader->MsSeller->getSeller($seller_id);

			if (!$seller) {
				$data['seller'] = NULL;
			} else {
				$data['seller'] = array();
				if (!empty($seller['ms.avatar'])) {
					$data['seller']['thumb'] = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
				} else {
					$data['seller']['thumb'] = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
				}
					
				$country = $this->model_localisation_country->getCountry($seller['ms.country_id']);
				
				if (!empty($country)) {
					$data['seller']['country'] = $country['name'];
				} else {
					$data['seller']['country'] = NULL;
				}
				
				$zone = $this->model_localisation_zone->getZone($seller['ms.zone_id']);
				
				if (!empty($zone)) {			
					$data['seller']['zone'] = $zone['name'];
				} else {
					$data['seller']['zone'] = NULL;
				}
				
				if (!empty($seller['ms.company'])) {
					$data['seller']['company'] = $seller['ms.company'];
				} else {
					$data['seller']['company'] = NULL;
				}
				
				if (!empty($seller['ms.website'])) {
					$data['seller']['website'] = $seller['ms.website'];
				} else {
					$data['seller']['website'] = NULL;
				}
				
				$data['seller']['nickname'] = $seller['ms.nickname'];
				$data['seller']['seller_id'] = $seller['seller_id'];
				 
				//$data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
				$data['seller']['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id']);	
				$data['seller']['total_sales'] = $this->MsLoader->MsSeller->getSalesForSeller($seller['seller_id']);
				$data['seller']['total_products'] = $this->MsLoader->MsProduct->getTotalProducts(array(
					'seller_id' => $seller['seller_id'],
					'product_status' => array(MsProduct::STATUS_ACTIVE)
				));

				$data['contactForm'] = $this->MsLoader->MsHelper->renderPmDialog($data);
			}

			$data['ms_product_attributes'] = $this->MsLoader->MsAttribute->getProductAttributes($this->request->get['product_id'], array('multilang' => 0, 'attribute_type'=> array(MsAttribute::TYPE_TEXT, MsAttribute::TYPE_TEXTAREA, MsAttribute::TYPE_DATE, MsAttribute::TYPE_DATETIME, MsAttribute::TYPE_TIME), 'mavd.language_id' => 0));
			$data['ms_product_attributes'] = array_merge($data['ms_product_attributes'], $this->MsLoader->MsAttribute->getProductAttributes($this->request->get['product_id'], (array())));
			/*--------------------------*/
			
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			$this->document->setTitle($product_info['meta_title']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['name'];

			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_sku'] = $this->language->get('text_sku');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			$data['text_loading'] = $this->language->get('text_loading');
			$data['text_modal_cart'] = $this->language->get('text_modal_cart');
			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');
			$data['entry_captcha'] = $this->language->get('entry_captcha');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_submit'] = $this->language->get('button_submit');
			
			$data['error_amount_0'] = $this->language->get('error_amount_0');
			
			$data['button_modal_cart'] = $this->language->get('button_modal_cart');
			
			//edit by arin
			$data['list_product_comments'] = $this->url->link('product/product/listProductComments', 'page=1', 'SSL');
			$data['list_comments_per_topic'] = $this->url->link('product/product/listCommentsPerTopic', '', 'SSL');
			$data['list_top_comments_per_topic'] = $this->url->link('product/product/listTopCommentsPerTopic', '', 'SSL');
			$data['button_join'] = $this->language->get('button_join');
			$data['link_join'] = $this->url->link('account/login', '', 'SSL');
			$data['text_start_topic'] = $this->language->get('text_start_topic');
				
			$this->load->model('catalog/review');

$this->load->model('module/productcomments');
				$this->language->load('module/productcomments');
				$data['tab_comments'] = sprintf($this->language->get('tab_comments'), $this->model_module_productcomments->getTotalComments(array(
					'product_id' => $this->request->get['product_id'],
					'displayed' => 1
				)));
				
				if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/productcomments.css')) {
					$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/productcomments.css');
				} else {
					$this->document->addStyle('catalog/view/theme/default/stylesheet/productcomments.css');
				}
			
			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);
			
			$total_comment = $this->model_product_product->countTopicComments(array("product_id"=>$this->request->get['product_id']));
			$data['tab_comment'] = sprintf($this->language->get('tab_comment'), $total_comment);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['code'] = $product_info['code'];
			$data['sku'] = $product_info['sku'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];

			if (true && $product_info['quantity'] <= 0) {
                    $data['stock_status'] = 'outofstock';
                }
                if (true && $product_info['quantity'] > 0) {
                    $data['stock_status'] = 'instock';
                }
                
			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
		
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}


				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			$data['label'] = $product_info['label'];
			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}
			
			/* edit by arin */
			$rate_success = $this->model_product_product->getRateSuccess($product_id);
			$data['rate_success'] = $rate_success['score'];
			$data['total_order_product'] = $rate_success['total_order'];

			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
                            } else {
                                    $server = $this->config->get('config_url');
                            }
                        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
                                    $data['logo'] = $server . 'image/icon/ofismoodbooster putih transparant.png';

                            } else {
                                    $data['logo'] = '';
                            }
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/product.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');
		$data['text_accuracy'] = $this->language->get('text_accuracy');
		$data['text_quality'] = $this->language->get('text_quality');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     		=> $result['author'],
				'text'       		=> nl2br($result['text']),
				'rating'     		=> (int)$result['rating'],
				'rating_accuracy'	=> (int)$result['rating_accuracy'],
				'date_added' 		=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/review.tpl', $data));
		}
	}

	public function getRecurringDescription() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
			}

			unset($this->session->data['captcha']);

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
public function getPrice() {
		$this->load->model('catalog/product');

		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->get['amount'])) {
			$amount = $this->request->get['amount'];
		} else {
			$amount = 1;
		}


		$product = $this->model_catalog_product->getProduct($product_id);
		

		$json = array();

		if ($product) {
			if (!$json) {
				$json['price'] = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
				$json['subtotal']=$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))*$amount);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function commentProduct(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('product/product', 'product_id=' . $this->request->post['product_id'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->language('product/product');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		$this->load->model('product/product');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateComment()) {
			$this->model_product_product->addProductComment($this->request->post);
			$this->response->redirect($this->url->link('product/product', 'product_id=' . $this->request->post['product_id'] , 'SSL'));
		}
		
		if (isset($this->error['error_comment_content'])) {
			$data['error_comment_content'] = $this->error['error_comment_content'];
		} else {
			$data['error_comment_content'] = '';
		}
		
		if (isset($this->request->post['product_id'])) {
			$data['product_id'] = $this->request->post['product_id'];
		} else {
			$data['product_id'] = '';
		}
		
		if (isset($this->request->post['comment'])) {
			$data['comment'] = $this->request->post['comment'];
		} else {
			$data['comment'] = '';
		}
	}
	
	public function validateComment(){
		if ((utf8_strlen($this->request->post['comment']) < 11) ) {
			return false;
		}else{
			return true;
		}
	}
	
	public function listProductComments(){
		$this->load->model('product/product');
		$data['product_comments'] = array();
		$data['topic_comments'] = array();
		
		/*pagination*/
		$topic_per_page = 5;
		if ((int)$topic_per_page == 0)
			$topic_per_page = 5;
		
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$topic_comments = $this->model_product_product->listTopicComments(
			array(
				"product_id"=>$this->request->get['product_id']
			),
			array(
				'offset' => ($page - 1) * $topic_per_page,
				'limit' => $topic_per_page
			)
		);
		
		$total_topic = $this->model_product_product->countTopicComments(array("product_id"=>$this->request->get['product_id']));
		
		$pagination = new Pagination();
		$pagination->total = $total_topic;
		$pagination->page = $page;
		$pagination->limit = $topic_per_page; 
		$pagination->url = $this->url->link('product/product/listProductComments', '&page={page}&product_id=' . $this->request->get['product_id'] , 'SSL');
		$data['pagination'] = $pagination->render();
		
		$this->load->language('product/product');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		$data['text_more'] = $this->language->get('text_more');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_start_topic'] = $this->language->get('text_start_topic');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_join'] = $this->language->get('button_join');
		$data['link_join'] = $this->url->link('account/login', '', 'SSL');
		$data['error_comment_content'] = $this->language->get('error_comment_content');
		$data['list_comments_per_topic'] = $this->url->link('product/product/listCommentsPerTopic', '', 'SSL');
		$data['list_top_comments_per_topic'] = $this->url->link('product/product/listTopCommentsPerTopic', '', 'SSL');
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('product/product', 'product_id='.$this->request->get['product_id'].'#tab-comments', 'SSL');
		}
		
		foreach($topic_comments as $topic_comment){
			$data['topic_comments'][] = array(
				'id_topic'  		=> $topic_comment['id_topic'],
				'customer_id'  		=> $topic_comment['customer_id'],
				'seller_id'  		=> $topic_comment['seller_id'],
				'product_id'  		=> $topic_comment['product_id'],
				'topic' 			=> $topic_comment['topic'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($topic_comment['created_date'])),
				'firstname' 		=> $topic_comment['firstname'],
				'email' 			=> $topic_comment['email'],
				'nickname' 			=> $topic_comment['nickname'],
				'product_comments'  => $this->model_product_product->loadTopCommentsPerTopic($topic_comment['id_topic'])
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product_comments.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product_comments.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('/template/product/product_comments.tpl', $data));
		}
	}
	
	public function listCommentsPerTopic(){
		$this->load->model('product/product');
		$data['product_comments'] = array();
		
		$product_comments = $this->model_product_product->loadCommentsPerTopic($this->request->get['id_topic']);
		
		$this->load->language('product/product');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_start_topic'] = $this->language->get('text_start_topic');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['error_comment_content'] = $this->language->get('error_comment_content');
		$data['list_comments_per_topic'] = $this->url->link('product/product/listCommentsPerTopic', '', 'SSL');
		$data['list_top_comments_per_topic'] = $this->url->link('product/product/listTopCommentsPerTopic', '', 'SSL');
		
		foreach($product_comments as $product_comment){
			$data['product_comments'][] = array(
				'id_comment'  		=> $product_comment['id_comment'],
				'id_topic'  		=> $product_comment['id_topic'],
				'customer_id'  		=> $product_comment['customer_id'],
				'seller_id'  		=> $product_comment['seller_id'],
				'product_id'  		=> $product_comment['product_id'],
				'comment' 			=> nl2br($product_comment['comment']),
				'topic' 			=> $product_comment['topic'],
				'created_by' 		=> $product_comment['created_by'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($product_comment['created_date'])),
				'firstname' 		=> $product_comment['firstname'],
				'email' 			=> $product_comment['email'],
				'nickname' 			=> $product_comment['nickname']
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/comment_category.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/comment_category.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('/template/product/comment_category.tpl', $data));
		}
	}
	
	public function listTopCommentsPerTopic(){
		$this->load->model('product/product');
		$data['product_comments'] = array();
		
		$product_comments = $this->model_product_product->loadTopCommentsPerTopic($this->request->get['id_topic']);
		
		$this->load->language('product/product');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_start_topic'] = $this->language->get('text_start_topic');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['error_comment_content'] = $this->language->get('error_comment_content');
		$data['list_comments_per_topic'] = $this->url->link('product/product/listCommentsPerTopic', '', 'SSL');
		
		foreach($product_comments as $product_comment){
			$data['product_comments'][] = array(
				'id_comment'  		=> $product_comment['id_comment'],
				'id_topic'  		=> $product_comment['id_topic'],
				'customer_id'  		=> $product_comment['customer_id'],
				'seller_id'  		=> $product_comment['seller_id'],
				'product_id'  		=> $product_comment['product_id'],
				'comment' 			=> nl2br($product_comment['comment']),
				'topic' 			=> $product_comment['topic'],
				'created_by' 		=> $product_comment['created_by'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($product_comment['created_date'])),
				'firstname' 		=> $product_comment['firstname'],
				'email' 			=> $product_comment['email'],
				'nickname' 			=> $product_comment['nickname']
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/comment_category.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/comment_category.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('/template/product/comment_category.tpl', $data));
		}
	}
	
	public function replyComment(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('product/product', 'product_id=' . $this->request->post['reply_product_id'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->language('product/product');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		$this->load->model('product/product');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateReply()) {
			$this->model_product_product->replyComment($this->request->post);
			$this->response->redirect($this->url->link('product/product', 'product_id=' . $this->request->post['reply_product_id'] , 'SSL'));
		}
		
		if (isset($this->error['error_comment_content'])) {
			$data['error_comment_content'] = $this->error['error_comment_content'];
		} else {
			$data['error_comment_content'] = '';
		}
		
		if (isset($this->request->post['reply_product_id'])) {
			$data['reply_product_id'] = $this->request->post['reply_product_id'];
		} else {
			$data['reply_product_id'] = '';
		}
		
		if (isset($this->request->post['reply_comment'])) {
			$data['reply_comment'] = $this->request->post['reply_comment'];
		} else {
			$data['reply_comment'] = '';
		}
		
		if (isset($this->request->post['reply_id_topic'])) {
			$data['reply_id_topic'] = $this->request->post['reply_id_topic'];
		} else {
			$data['reply_id_topic'] = '';
		}
	}
	
	public function validateReply(){
		if ((utf8_strlen($this->request->post['reply_comment']) < 11) ) {
			return false;
		}else{
			return true;
		}
	}
	/*
	public function testEmail(){
		$body='<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ofiskita Introduction</title>
<style>
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
</style>
</head>

<body bgcolor="#FFFFFF" style="position:relative;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;">
<div style="width: 100%; background-color: #FFFFFF; text-align: center; margin: 0; margin-bottom: 0px;">
<a href="http://www.ofiskita.com/" title="Ofiskita"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/6eab2b48-2957-4577-90fe-9fcb1dd4a670.png"></a>
</div>
<div style="width: 100%; background-color:#FFFFFF; text-align: center; margin: 0; margin-bottom: 0px;">
<img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/e5a62fee-4c18-4f1c-9109-435ee1f07084.png">
</div>
<!-- body -->
<table class="body-wrap" style="width: 100%;" bgcolor="#FFFFFF">
  <tr>
    <td></td>
    <td class="container" style=" clear: both !important;display: block !important;Margin: 0 auto !important;max-width: 600px !important;border: 0;padding: 20px;" bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content" style="display: block;margin: 0 auto;max-width: 600px;">
      <table>
        <tr>
          <td align="center">
			<p>Astragraphia dengan bangga memperkenalkan<br><span style="font-size:24px;">"OFISKITA"</span></p>
			<p>3 Fakta tentang OFISKITA</p>
			
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td valign="top" align="center" style="font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#000000;width:30%">
				  <img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/e5e44d49-aea8-496d-bd0a-ac211c470d4c.png" style="padding: 10px;">
				  <p style="line-height: 150%;">
					<strong>Inisiatif Baru</strong>
					<br>Dibangun akhir tahun 2015.
					<br>Merupakan e-marketplace yang fokus pada pemenuhan barang dan jasa untuk kebutuhan para profesional & pelaku bisnis
				  </p>
				</td>
				<td valign="top" align="center" style="font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#000000;width:30%">
				  <img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/563c4718-53a6-4920-aeba-ee9279b23e2e.png" style="padding: 10px;">
				  <p style="line-height: 150%;">
					<strong>Brick & Click</strong>
					<br>Kami dapat ditemukan di www.ofiskita.com & playstore (Click) serta Ofiskita Kiosk (Brick) yang tersebar di seluruh Indonesia
				  </p>
				</td>
				<td valign="top" align="center" style="font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#000000;width:30%">
				  <img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/762ded1c-147a-45ef-b478-276bae3f69b3.png" style="padding: 10px;">
				  <p style="line-height: 150%;">
					<strong>AG Banget!</strong>
					<br>Hasil karya kita sendiri.
					<br>Perpaduan antara AG Group dengan dukungan teknologi dari AGIT dan produk dari AXI serta kerja sama dengan Astra Group
					<br>Wow! AG bangga!
				  </p>
				</td>
              </tr>
            </table>
            
            <p style="line-height: 150%;padding:10px;background-color:#ec682e;color:#FFFFFF"><strong>Khusus untuk karyawan AG Group</strong>
			<br>Nantikan promo kami Senin besok ya!
			<br>Dapatkan keistimewaan dalam rangka Hari Pelanggan Nasional &#9786
			</p>
		  </td>
        </tr>
      </table>
      </div>
	  
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->
<div style="width: 100%;background-color:#efefef;color: #000000; line-height: 150%; font-size: 10px; height: 15%;">
 <table style="width: 100% !important;max-width: 600px;margin:auto;">
        <tr>
          <td>
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style="font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#000000;">
                  <p style="color:#878787;">Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
				  <p style="color:#9e9e9e;">Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja, atau meminta data pribadi anda. Ofiskita tidak pernah meminta password dan data pribadi melalui email, pesan pribadi, maupun channel lainnya.</p>
                </td>
              </tr>
            </table>
			<p></p>
		  </td>
        </tr>
      </table>
</div>
<div style="position: absolute;bottom: 80px;"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/95b5eb10-3ad5-4669-be57-b66f23fbece5.png" style="padding: 10px;"></div>
<div bgcolor="#ec682e" style="width: 100%;background-color:#ec682e;color:#FFFFFF; line-height: 150%; font-size: 10px; height: 15%;padding:10px">
 <table style="width: 100% !important;max-width: 300px;margin:auto">
        <tr>
          <td>
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style="font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#FFFFFF;">
                  Copyright © 2016 Ofiskita Powered by Astragraphia, All rights reserved.<br>
				  Astragraphia HO | Jl. Kramat Raya No. 43 Senen, Jakarta 10450<br>
				  <a href="mailto:support@ofiskita.com">support@ofiskita.com</a><br>
                </td>
              </tr>
            </table>
			<p></p>
		  </td>
        </tr>
      </table>
</div>
<div bgcolor="#b2360b" style="width: 100%; background-color:#b2360b; text-align: center; margin: 0; height: 15%; vertical-align: middle;padding:10px;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="30%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/052495a2-6e4f-4575-bf98-ef4f064459f5.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="30%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/3443b837-591c-4b97-a1b9-b2dd56430242.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="30%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/c841abe4-9a1b-47f2-b438-a5659168a452.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
</div>
</body>
</html>';
$mail = new Mail($this->config->get('config_mail'));
				//$mail->setTo('fanda.vionita@astragraphia.co.id');
				//$mail->setTo('roby.winarto@ag-it.com');
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject('Ofiskita Introduction');
				
				
				$mail->setHtml($body);
				$mail->send();
	}*/
}
