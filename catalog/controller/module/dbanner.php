<?php
class ControllerModuleDbanner extends Controller {
	public function index($setting) {

		$this->load->model('tool/image');

		$this->document->addScript('catalog/view/javascript/dbanner/jquery.lazyload.js');
		$this->document->addStyle('catalog/view/javascript/dbanner/animate.css');

		$data['title'] = $setting['title'];
		$data['effect'] = $setting['effect_type'];
		$margin_left = $setting['margin-left'] . "px";
		$margin_top = $setting['margin-top'] . "px";
		$margin_right = $setting['margin-right'] . "px";
		$margin_bottom = $setting['margin-bottom'] . "px";
		$data['padding'] = ($setting['padding'] == null ? 0 : $setting['padding']) . "px";

		$gridster = json_decode(stripslashes(html_entity_decode($setting['gridster'])));

		$widgets = array();

		/* transform in array and order by rows and cols */
		$array = array(); $data['maxRow'] = 0; $maxSize = 0;
		foreach ($gridster as $g) {
			$g = get_object_vars($g);
			$row = $g['row'];
			$col = $g['col'];

			$currentSize = $row + $g['size_y'];
			if($currentSize > $maxSize) {
				$maxSize = $currentSize;
			}

			$array[$row][$col] = $g;
		}

		/* correct maxRow */
		$data['maxRow'] = $maxSize - 1;

		/* order arrays */
		ksort($array);
		foreach ($array as $key => $value) {
			ksort($array[$key]);
		}

		$defaultHeight = 95;
		$defaultWidth = 100;

		$wPrevious = null; 
		for($i = 0; $i <= $data['maxRow']+1; $i++) {
			if (isset($array[$i])) {
				$wPrevious = null;
				foreach ($array[$i] as $key => $w) {
					// calculate offset of widget
					if($wPrevious != null) {
						// compare previus with current
						$w['offset'] = $w['col'] - ($wPrevious['col'] + $wPrevious['size_x']);
					} else {
						// first
						$w['offset'] = $w['col'] - 1;
					}

					$wPrevious = $array[$i][$key];

					// calucalte the height of widget
					$w['styleHeight'] = ($w['size_y'] * $defaultHeight) - (2 * $setting['padding']);
					$w['background_style'] = "min-height: " . $w['styleHeight'] . "px;";
					$w['image'] = $this->resize_image($w['img'], ($w['size_x'] * $defaultWidth), ($w['size_y'] * $defaultHeight));
					$w['background_style'] .= "background: url('" . $w['image'] ."') center no-repeat; background-size: 100%";
					
					// modal data
					$modalArray = array();
					parse_str($w['modal'], $modalArray);
					$lang_code = $this->session->data['language'];
					$w['link'] = (isset($modalArray['modal-link-'.$lang_code]) && ($modalArray['modal-link-'.$lang_code] != "")) ? $modalArray['modal-link-'.$lang_code] : "#";
					$w['title'] = isset($modalArray['modal-name-'.$lang_code]) ? $modalArray['modal-name-'.$lang_code] : "";

					// style class settings
					if($setting['mobile-layout']) {
						$offset_class = "col-sm-offset-" . $w['offset']; 
						$col_class = "col-xs-12 col-sm-";
					} else {
						$offset_class = "col-xs-offset-" . $w['offset']; 
						$col_class = "col-xs-";
					}
					$col_class .= $w['size_x'];
					$hidden_class = (!$modalArray['modal-show-mobile-'.$lang_code] && $setting['mobile-layout']) ? "hidden-xs" : "";
					$w['class'] = $offset_class . " " . $col_class . " " . $hidden_class;

					$array[$i][$key] = $w;
				}
			}
		}

		$data['widgets'] = $array;
		$data['margin'] = "padding: " . $margin_top . " " . $margin_right . " " . $margin_bottom . " " . $margin_left;
		$data['defaultHeight'] = $defaultHeight;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/dbanner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/dbanner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/dbanner.tpl', $data);
		}
	}

	private function resize_image($image, $size_x, $size_y) {
		if ($image) {    

			$oldsize = getimagesize(DIR_IMAGE.$image); 
             //get setting sizes
			$width = $oldsize[0];
			$height = $oldsize[1];

			$scale = max($size_x/$oldsize[0], $size_y/$oldsize[1]);

                   // Get the new dimensions
			$new_width  = ceil($scale*$oldsize[0]);
			$new_height = ceil($scale*$oldsize[1]); 

			$image = $this->model_tool_image->resize($image, $new_width, $new_height);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', $size_x, $size_y);
		}

		return $image;
	}
}