<?php
class ControllerModuleProductComments extends Controller {
	private $data=array();
	public function __construct($registry) {
		parent::__construct($registry);		
		//$data = array_merge($data, $this->load->language('module/productcomments'));
		$this->load->model('module/productcomments');
		$this->load->model('account/seller');
		$this->language->load('module/productcomments');
				
	}
	
	public function renderComments() {
		$data['pc_wait'] = $this->language->get('pc_wait');
		$data['pc_no_comments_yet'] = $this->language->get('pc_no_comments_yet');
		$data['pc_text_seller'] = $this->language->get('pc_text_seller');
		$data['pc_text_customer'] = $this->language->get('pc_text_customer');
		$data['pc_text_more'] = $this->language->get('pc_text_more');
		
		$product_id = $this->request->get['product_id'];
		$comments_per_page = $this->config->get('productcomments_pcconf_perpage');
		if ((int)$comments_per_page == 0)
			$comments_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		if ($this->customer->isLogged()) {
			$parent_id=$this->customer->getId();
		}else{
			$parent_id=0;
		}
		
		$data['product_id'] = $product_id;
		$data['pcComments'] = $this->model_module_productcomments->getCommentsPerProduct(
			array(
				'product_id' => $product_id,
				'parent_id' => $parent_id,
				'displayed' => 1,
			),
			array(
				'order_by' => 'pc.create_time',
				'order_way' => 'DESC',
				'offset' => ($page - 1) * $comments_per_page,
				'limit' => $comments_per_page
			)
		);
		
		$total_comments = $this->model_module_productcomments->getTotalCommentsPerCustomer(
			array(
				'displayed' => 1,
				'product_id' => $product_id,
				'parent_id' => $parent_id,
			)
		);
		$pagination = new Pagination();
		$pagination->total = $total_comments;
		$pagination->page = $page;
		$pagination->limit = $comments_per_page; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/productcomments/renderComments', '&page={page}' . '&product_id=' . $product_id);
		$data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/productcomments_comments.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/productcomments_comments.tpl';
		} else {
			$this->template = 'default/template/module/productcomments_comments.tpl';
		}			

		//$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
		$this->response->setOutput($this->load->view($this->template, $data));		
	}
	
	public function renderForm() {
		
		$data['pc_login_register'] = $this->language->get('pc_login_register');
		$data['pc_error_name'] = $this->language->get('pc_error_name');
		$data['pc_error_email'] = $this->language->get('pc_error_email');
		$data['pc_error_comment_short'] = $this->language->get('pc_error_comment_short');
		$data['pc_error_comment_long'] = $this->language->get('pc_error_comment_long');
		$data['pc_error_captcha'] = $this->language->get('pc_error_captcha');
		$data['tab_comments'] = $this->language->get('tab_comments');
		$data['text_post_comment'] = $this->language->get('text_post_comment');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_note'] = $this->language->get('text_note');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_join'] = $this->language->get('button_join');
		$data['pc_enter_captcha'] = $this->language->get('pc_enter_captcha');
		$data['pc_wait'] = $this->language->get('pc_wait');
		$data['pc_no_comments_yet'] = $this->language->get('pc_no_comments_yet');
		$data['pc_text_seller'] = $this->language->get('pc_text_seller');
		$data['pc_text_customer'] = $this->language->get('pc_text_customer');
		$data['pc_text_more'] = $this->language->get('pc_text_more');
		$data['pc_success'] = $this->language->get('pc_success');
		$data['pc_mail_subject'] = $this->language->get('pc_mail_subject');
		$data['pc_mail'] = $this->language->get('pc_mail');
		
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		if  (!$product_id) return;

		if  (!$this->customer->isLogged() && !$this->config->get('pcconf_allow_guests')) {
			$data['link']=$this->url->link('account/login', '', 'SSL');
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/productcomments_login.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/productcomments_login.tpl';
			} else {
				$this->template = 'default/template/module/productcomments_login.tpl';
			}
			$this->response->setOutput($this->load->view($this->template, $data));	
		} else {
		
			if  (!$this->model_module_productcomments->productOwnedBySeller($product_id, $this->customer->getId())) {
				$data['pcName'] = $this->customer->getFirstname();
				$data['pcEmail'] = $this->customer->getEmail();
				$data['pcLogged'] = $this->customer->isLogged();
				
				$data['pcconf_maxlen'] = $this->config->get('pcconf_maxlen');
				$data['pcconf_enforce_customer_data'] = $this->config->get('pcconf_enforce_customer_data');
				$data['product_id'] = $product_id;
				
				$parent_id=$this->customer->getId();
				$data['parent_id'] = $parent_id;
				
				$data['comments'] = $this->model_module_productcomments->getCommentsPerProductParent(
					array(
						'product_id' => $product_id,
						'parent_id' => $parent_id,
						'displayed' => 1,
					),
					array(
						'order_by' => 'pc.create_time',
						'order_way' => 'DESC'
					)
				);
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/productcomments_form.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/module/productcomments_form.tpl';
				} else {
					$this->template = 'default/template/module/productcomments_form.tpl';
				}			
				//$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));	
				$this->response->setOutput($this->load->view($this->template, $data));			
			}
		}
	}
	
	public function viewComment(){
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$parent_id = isset($this->request->get['parent_id']) ? (int)$this->request->get['parent_id'] : 0;
		
		$this->load->model('module/productcomments');
		$json['comments'] = $this->model_module_productcomments->getCommentsPerProductDetailPerCustomer(
			array(
				'product_id' => $product_id,
				'parent_id' => $parent_id
			)
		);
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));
		}
	}
	
	public function submitComment(){
		if (!isset($this->request->get['product_id']))
			return;
		
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$parent_id = isset($this->request->post['pcId']) ? (int)$this->request->post['pcId'] : 0;
		
		$comment['product_id'] = $product_id;
		
		$json = array();
		
		//if (is admin) {
			//admin logged in
		//} else 	
		if ($this->customer->isLogged()) {
			//customer logged in
			$comment['customer_id'] = $this->customer->getId();
			$comment['parent_id'] = $parent_id;
			$comment['name'] = $this->customer->getFirstName();
			$comment['email'] = $this->customer->getEmail();			
		} else {
			// guest
			$comment['customer_id'] = 0;
			$comment['parent_id'] = 0;
			if (!$this->config->get('productcomments_pcconf_allow_guests')) {
				//$json['errors'][] = 'Not allowed to post';
				return;			
			} else {
	    		$comment['name'] = '';
	    		$comment['email'] = '';
			}
		}

	    $comment['comment'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcText'])));

		if (mb_strlen($comment['name'],'UTF-8') < 3 || mb_strlen($comment['name'],'UTF-8') > 25) {
			$json['errors'][] = sprintf($this->language->get('pc_error_name'), 3, 25);
		}

		if (!filter_var($comment['email'], FILTER_VALIDATE_EMAIL) || mb_strlen($comment['email'], 'UTF-8') > 128) {
			$json['errors'][] = $this->language->get('pc_error_email');
		}

		if (mb_strlen($comment['comment']) < 10) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_short'), 10);
		}

		if (mb_strlen($comment['comment']) > $this->config->get('productcomments_pcconf_maxlen') && $this->config->get('productcomments_pcconf_maxlen') > 0) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_long'), $this->config->get('productcomments_pcconf_maxlen'));
		}

		if (!$this->customer->isLogged() || ($this->customer->isLogged() && $this->config->get('productcomments_pcconf_enable_customer_captcha'))) {
			if (!isset($this->request->post['pcCaptcha']) || ($this->session->data['captcha'] != $this->request->post['pcCaptcha'])) {
				$json['errors'][] = $this->language->get('pc_error_captcha');
			}
		}
		
		$this->load->model('module/productcomments');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($json['errors'])) {
			$this->model_module_productcomments->addComment($comment);
			$json['success'] = $this->language->get('pc_success');
		}
		
		$json['comments'] = $this->model_module_productcomments->getCommentsPerProductDetail(
			array(
				'product_id' => $product_id,
				'parent_id' => $parent_id,
				'displayed' => 1,
			),
			array(
				'order_by' => 'pc.create_time',
				'order_way' => 'DESC'
			)
		);
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));
		}
	}
	
	/*public function submitComment() {
		if (!isset($this->request->get['product_id']))
			return;
			
		$comment['product_id'] = $this->request->get['product_id'];
		
		$json = array();
		
		//if (is admin) {
			//admin logged in
		//} else 	
		if ($this->customer->isLogged()) {
			//customer logged in
			$comment['customer_id'] = $this->customer->getId();
			$comment['parent_id'] = $this->customer->getId();
			if  ($this->config->get('productcomments_pcconf_enforce_customer_data')) {
				$comment['name'] = $this->customer->getFirstName();
	    		$comment['email'] = $this->customer->getEmail();
			} else {
	    		$comment['name'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcName'])));
	    		$comment['email'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcEmail'])));
			}			
		} else {
			// guest
			$comment['customer_id'] = 0;
			$comment['parent_id'] = 0;
			if (!$this->config->get('productcomments_pcconf_allow_guests')) {
				//$json['errors'][] = 'Not allowed to post';
				return;			
			} else {
	    		$comment['name'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcName'])));
	    		$comment['email'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcEmail'])));
			}
		}

	    $comment['comment'] = trim(strip_tags(htmlspecialchars_decode($this->request->post['pcText'])));

		if (mb_strlen($comment['name'],'UTF-8') < 3 || mb_strlen($comment['name'],'UTF-8') > 25) {
			$json['errors'][] = sprintf($this->language->get('pc_error_name'), 3, 25);
		}

		if (!filter_var($comment['email'], FILTER_VALIDATE_EMAIL) || mb_strlen($comment['email'], 'UTF-8') > 128) {
			$json['errors'][] = $this->language->get('pc_error_email');
		}

		if (mb_strlen($comment['comment']) < 10) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_short'), 10);
		}

		if (mb_strlen($comment['comment']) > $this->config->get('productcomments_pcconf_maxlen') && $this->config->get('productcomments_pcconf_maxlen') > 0) {
			$json['errors'][] = sprintf($this->language->get('pc_error_comment_long'), $this->config->get('productcomments_pcconf_maxlen'));
		}

		if (!$this->customer->isLogged() || ($this->customer->isLogged() && $this->config->get('productcomments_pcconf_enable_customer_captcha'))) {
			if (!isset($this->request->post['pcCaptcha']) || ($this->session->data['captcha'] != $this->request->post['pcCaptcha'])) {
				$json['errors'][] = $this->language->get('pc_error_captcha');
			}
		}
		
		 
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($json['errors'])) {
			$this->model_module_productcomments->addComment($comment);
			$json['success'] = $this->language->get('pc_success');
		}
		
		if (strcmp(VERSION,'1.5.1.3') >= 0) {
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->library('json');
			$this->response->setOutput(Json::encode($json));
		}
	}	*/
}
?>