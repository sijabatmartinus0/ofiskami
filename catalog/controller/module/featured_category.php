<?php
class ControllerModuleFeaturedCategory extends Controller {
	public function index() {
		if((int)$this->config->get('featured_category_status')==1){
			$this->load->language('module/featured_category');

			$data['heading_title'] = $this->language->get('heading_title');
			$data['text_start_price'] = $this->language->get('text_start_price');
			
			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('tool/image');
			
			$featured_categories = $this->config->get('featured_category_data');
			
			usort($featured_categories, array($this, "cmp"));
			
			$data['featured_categories'] = array();

			foreach ($featured_categories as $featured_category) {
				if (is_file(DIR_IMAGE . $featured_category['image'])) {
					$thumb = $featured_category['image'];
				} else {
					$thumb = 'no_image.png';
				}
				
				
				$category_id = $featured_category['category_id'];
				$category = $this->model_catalog_category->getCategory($category_id);
				if(empty($category)){
					$category_name = "";
					$category_href = "#";
				}else{
					$category_name = $category['name'];
					$category_href = $this->url->link('product/category', 'path=' . $category_id);
				}
				
				//Get random product
				$products=array();
				$result_products = array();
				
				$filter_data = array(
					'filter_category_id' => $category_id,
					'sort' => "p.price",
					'order' => "ASC"
				);

				$results = $this->model_catalog_product->getProducts($filter_data);

				foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $this->config->get('featured_category_product_image_width'), $this->config->get('featured_category_product_image_height'));
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('featured_category_product_image_width'), $this->config->get('featured_category_product_image_height'));
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = (int)$result['rating'];
					} else {
						$rating = false;
					}

					$products[] = array(
						'product_id'  => $result['product_id'],
						'thumb'       => $image,
						'name'        => $result['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $result['rating'],
						'href'        => $this->url->link('product/product', 'path=' . $category_id . '&product_id=' . $result['product_id'])
					);
				}
				
				if(!empty($products)){
					$start_price = $products[0]['price'];
				}else{
					$start_price = $this->currency->format(0);
				}
				
				if(!empty($products)){
					shuffle($products); 
					$max=3;
					if(sizeof($products)<$max){
						$max=sizeof($products);
					}
					for($x=0;$x<$max;$x++){
						$result_products[] = $products[$x];
					}
				}
				
				$data['featured_categories'][] = array(
					'thumb'      	=> $this->model_tool_image->resize($thumb, $this->config->get('featured_category_banner_image_width'), $this->config->get('featured_category_banner_image_height')),
					'href' 			=> $category_href,
					'category' 		=> $category_name,
					'start_price'	=> $start_price,
					'products' 		=> $result_products
				);
			}
			
			if (!empty($data['featured_categories'])) {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured_category.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/featured_category.tpl', $data);
				} else {
					return $this->load->view('default/template/module/featured_category.tpl', $data);
				}
			}
		}
	}
	
	private function cmp($a, $b)
	{
		return strcmp($a['sort_order'], $b['sort_order']);
	}
}