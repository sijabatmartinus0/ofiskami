<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		
		$this->load->language('module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');


		
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');

		$this->load->model('tool/image');

//Tampilan Sale persen
		if (file_exists('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_' . $this->language->get('code') . '.js')) {
			$this->document->addScript('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_' . $this->language->get('code') . '.js');
			} else {
			$this->document->addScript('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_en.js');
			}
			$this->load->language('common/cosyone');
			$data['text_category_expire'] = $this->language->get('text_category_expire');
			$data['cosyone_category_thumb'] = $this->config->get('cosyone_category_thumb');
			$data['cosyone_grid_category'] = $this->config->get('cosyone_grid_category');
			$data['cosyone_category_refine'] = $this->config->get('cosyone_category_refine');
			$data['cosyone_category_per_row'] = $this->config->get('cosyone_category_per_row');
			$data['cosyone_rollover_effect'] = $this->config->get('cosyone_rollover_effect');
			$data['cosyone_percentage_sale_badge'] = $this->config->get('cosyone_percentage_sale_badge');
			$cosyone_quicklook = $this->config->get('cosyone_text_ql');
			if(empty($cosyone_quicklook[$this->language->get('code')])) {
				$data['cosyone_text_ql'] = false;
			} else if (isset($cosyone_quicklook[$this->language->get('code')])) {
				$data['cosyone_text_ql'] = html_entity_decode($cosyone_quicklook[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
			}
			$data['cosyone_brand'] = $this->config->get('cosyone_brand');
			$data['cosyone_product_countdown'] = $this->config->get('cosyone_product_countdown');
			$data['cosyone_product_hurry'] = $this->config->get('cosyone_product_hurry');
			$data['cosyone_default_view'] = $this->config->get('cosyone_default_view');
//Tampilan Sale persen

		$data['products'] = array();
		
		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
	

		$products = array_slice($setting['product'], 0, (int)$setting['limit']);
		
		foreach ($products as $product_id) {
			echo $a;
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($product_info['special_date_end']){
						date_default_timezone_set("Asia/Bangkok");
					$special_date_end = strtotime($product_info['special_date_end']) -time();
				}
				else {	
					$special_date_end = false;
				}

					if ($product_info['special_date_start']){
						date_default_timezone_set("Asia/Bangkok");
					$special_date_start = strtotime($product_info['special_date_start']) -time();
				}
				else {	
					$special_date_start = false;
				}


				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
				
				if ((float)$product_info['special']) {
				$sales_percantage = ((($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))/100));
				} else {
				$sales_percantage = false;
				}	
				$id= $this->model_catalog_category->getIdCategory($setting['name']);
				
				
				$data['category_name']= $setting['name'];
				$data['category_url']= $this->url->link('product/category ','path='.$id['category_id']);
				
				$data['products'][] = array(
					
					'product_id'  => $product_info['product_id'],
					'minimum'=>1,
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'special_date_end'=>$special_date_end,
					'special_date_start'=>$special_date_start,
					'sales_percantage' => number_format($sales_percantage, 0, ',', '.'),
					'tax'         => $taxi,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
	
		}

	 //echo '<pre>';
	 //	var_dump($data);
	 //	die();
			
		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				if($setting['name'] != "Office Supplies" && $setting['name'] != "Office Wear" && $setting['name'] != "Technology" &&	 $setting['name'] != "17an" ){
					return $this->load->view('default/template/module/featured_flash.tpl', $data);
				} 
				else {
				return $this->load->view('default/template/module/featured.tpl', $data);	
				}
				
			}
		}
	}
}
