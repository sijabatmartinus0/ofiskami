<?php

class ControllerModuleFeatured extends Controller {

    public function index($setting) {

        $this->load->language('module/featured');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');


        $this->load->language('common/cosyone');
        $data['text_category_expire'] = $this->language->get('text_category_expire');
        $data['cosyone_percentage_sale_badge'] = $this->config->get('cosyone_percentage_sale_badge');
        $cosyone_quicklook = $this->config->get('cosyone_text_ql');
        if (empty($cosyone_quicklook[$this->language->get('code')])) {
            $data['cosyone_text_ql'] = false;
        } else if (isset($cosyone_quicklook[$this->language->get('code')])) {
            $data['cosyone_text_ql'] = html_entity_decode($cosyone_quicklook[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
        }
        $data['cosyone_rollover_effect'] = $this->config->get('cosyone_rollover_effect');
        $data['cosyone_product_hurry'] = $this->config->get('cosyone_product_hurry');
        $data['cosyone_product_countdown'] = $this->config->get('cosyone_product_countdown');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

        if (!$setting['limit']) {
            $setting['limit'] = 4;
        }

        $products = array_slice($setting['product'], 0, (int) $setting['limit']);

        if (file_exists('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_' . $this->language->get('code') . '.js')) {
            $this->document->addScript('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_' . $this->language->get('code') . '.js');
        } else {
            $this->document->addScript('catalog/view/theme/ofisindi/js/countdown/jquery.countdown_en.js');
        }

        foreach ($products as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float) $product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ((float) $product_info['special']) {
                    $sales_percantage = ((($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) - ($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')))) / (($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) / 100));
                } else {
                    $sales_percantage = false;
                }

                if ((float) $product_info['special']) {
                    $special_info = $this->model_catalog_product->getSpecialPriceEnd($product_info['product_id']);
                    $special_date_end = strtotime($special_info['date_end']) - time();
                } else {
                    $special_date_end = false;
                }
                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

				$id= $this->model_catalog_category->getIdCategory($setting['name']);
				
				$data['category_name']= $setting['name'];
				$data['category_url']= $this->url->link('product/category ','path='.$id['category_id']);
				
                $data['products'][] = array(
                    'product_id' => $product_info['product_id'],
					'minimum'=>1,
                    'thumb' => $image,
                    'name' => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
            }
        }
//        echo '<pre>';
//            var_dump($data['products']);
//            die();
        if ($data['products']) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
            } else {
                return $this->load->view('default/template/module/featured.tpl', $data);
            }
        }
    }

}
