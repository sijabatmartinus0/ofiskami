<?php
class ControllerAccountOrder extends Controller {
	private $error = array();
	private $data=array();
	
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_invoice'] = $this->language->get('column_invoice');
		$data['heading_payment'] = $this->language->get('heading_payment');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_confirm_done'] = $this->language->get('button_confirm_done');
		$data['button_reorder'] = $this->language->get('button_reorder');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['orders'] = array();
		
		$this->load->model('account/order');
		$this->load->model('catalog/product');

		$order_total = $this->model_account_order->getTotalOrders();
		
		$results = $this->model_account_order->getOrderDetails(($page - 1) * 10, 10);
		
		foreach ($results as $result) {
			$order_type= array();
			
			$product_total = $this->model_account_order->getTotalOrderProductsByInvoice($result['order_id'], $result['order_detail_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);
			$count_merge_order_id = $this->model_account_order->getCountOrderIdFromDetails($result['order_id']);
			$count_merge_voucher = $this->model_account_order->getCountVoucherFromDetails($result['order_id']);
			
			$check_order = $this->model_account_order->checkOrder($result['order_id']);
			foreach($check_order as $co){
				array_push($order_type, $co['delivery_type_id']);
			}
			
			if(count($order_type) > 1 && in_array("3", $order_type)){
				$data['order_type'] = "mix";
			}else{
				$data['order_type'] =  "common";
			}
			
			$data['orders'][] = array(
				'order_id'   		=> $result['order_id'],
				'order_detail_id'   => $result['order_detail_id'],
				'name'       		=> $result['shipping_firstname'] . ' ' . $result['shipping_lastname'],
				'status'     		=> $result['status'],
				'payment_method'     		=> $result['payment_method'],
				'invoice_no'    	=> $result['invoice_prefix'] . $result['invoice_no'],
				'invoice'    		=> $result['invoice_no'],
				'invoice_link'		=> $this->url->link('account/order/invoice', 'order_id=' . $result['order_id'] . '&order_detail_id=' .$result['order_detail_id'] , 'SSL'),
				'date_added' 		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   		=> ($product_total + $voucher_total),
				'total'      		=> $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'href'       		=> $this->url->link('account/order/info', 'order_id=' .$result['order_id'] .'&order_detail_id=' . $result['order_detail_id'] , 'SSL'),
				'reorder'       		=> $this->url->link('account/order/reorder', 'order_id=' .$result['order_id'] .'&order_detail_id=' . $result['order_detail_id'] , 'SSL'),
				//edit by arin
				'confirm'    			=> $this->url->link('account/order/confirm', 'order_id=' . $result['order_id'], 'SSL'),
				'count_merge' 			=> $count_merge_order_id,
				'count_merge_voucher' 			=> $count_merge_voucher,
				'order_status_id' 		=> $result['order_status_id'],
				'delivery_type_id' 		=> $result['delivery_type_id'],
				'pp_name' 				=> $result['pp_name'],
				'to_name' 				=> $result['to_name'],
				'order_total' 			=> $this->currency->format($result['order_total'], $result['currency_code'], $result['currency_value']),
				'order_type'			=> $data['order_type']
			);
			
			unset($order_type);
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$data['continue'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/order_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/order_list.tpl', $data));
		}
	}

	public function info() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id='. $order_id . '&order_detail_id=' . $order_detail_id , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->model('account/order');
		$this->load->model('seller/account_pending_order');

		$order_info = $this->model_account_order->getOrderDetail($order_detail_id);

		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url . '&order_detail_id=' .$order_detail_id, 'SSL')
			);

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_receipt'] = $this->language->get('text_receipt');
			$data['text_seller'] = $this->language->get('text_seller');
			$data['text_complete_return'] = $this->language->get('text_complete_return');
			
			//Waybill
			$data['text_modal_waybill'] = $this->language->get('text_modal_waybill');
			$data['text_waybill_number'] = $this->language->get('text_waybill_number');
			$data['text_waybill_courier'] = $this->language->get('text_waybill_courier');
			$data['text_waybill_service'] = $this->language->get('text_waybill_service');
			$data['text_waybill_date'] = $this->language->get('text_waybill_date');
			$data['text_waybill_origin'] = $this->language->get('text_waybill_origin');
			$data['text_waybill_destination'] = $this->language->get('text_waybill_destination');
			$data['text_waybill_shipper'] = $this->language->get('text_waybill_shipper');
			$data['text_waybill_receiver'] = $this->language->get('text_waybill_receiver');
			$data['text_waybill_not_found'] = $this->language->get('text_waybill_not_found');

			$data['column_customer'] = $this->language->get('column_customer');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');
			//edit by arin
			$data['column_shipping'] = $this->language->get('column_shipping');
			$data['column_product_list'] = $this->language->get('column_product_list');
			$data['column_buyer'] = $this->language->get('column_buyer');
			$data['column_insurance'] = $this->language->get('column_insurance');
			$data['column_remarks'] = $this->language->get('column_remarks');

			$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_cancel'] = $this->language->get('button_cancel');
			
			$data['button_receive'] = $this->language->get('button_receive');
			$data['button_complete'] = $this->language->get('button_complete');
			
			$data['link_receive'] = $this->url->link('account/order/receiveOrder', '&order_id='.$order_id.'&order_detail_id='.$order_detail_id, 'SSL');
			$data['link_complete'] = $this->url->link('account/order/completeOrder', '&order_id='.$order_id.'&order_detail_id='.$order_detail_id, 'SSL');
			
			//voucher
			$data['column_code'] = $this->language->get('column_code');
			$data['column_message'] = $this->language->get('column_message');
			$data['column_description'] = $this->language->get('column_description');
			$data['text_receiver_name'] = $this->language->get('text_receiver_name');
			$data['text_receiver_email'] = $this->language->get('text_receiver_email');
			$data['text_voucher_data'] = $this->language->get('text_voucher_data');
			$data['text_voucher'] = $this->language->get('text_voucher');
			
			
			$data['invoice_link'] = $this->url->link('account/order/invoice', 'order_id=' . $order_id . '&order_detail_id=' .$order_detail_id , 'SSL');
			
			//Seller
			$seller = $this->MsLoader->MsSeller->getSeller((int)$order_info['seller_id']);

			if (!$seller) {
				$data['seller'] = NULL;
			} else {
				$data['seller'] = array();
				
				$data['seller']['nickname'] = $seller['ms.nickname'];
				$data['seller']['seller_id'] = $seller['seller_id'];
				 
				$data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
				
			}
			
			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_id'] = $this->request->get['order_id'];
			
			//edit by arin
			
			
			$data['order_detail_id'] = $this->model_account_order->getOrderDetailId($order_detail_id);
			$data['qty'] = $this->model_account_order->getQuantity($data['order_detail_id']);
			$data['delivery_type'] = $order_info['delivery_type'];
			$data['delivery_type_id'] = $order_info['delivery_type_id'];
			$data['shipping_id'] = $order_info['shipping_id'];
			$data['shipping_name'] = $order_info['shipping_name'];
			$data['shipping_service_name'] = $order_info['shipping_service_name'];
			$data['shipping_insurance'] = $this->currency->format($order_info['shipping_insurance'], $order_info['currency_code'], $order_info['currency_value']);
			$data['shipping_price'] = $this->currency->format($order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			$data['pp_branch_id'] = $order_info['pp_branch_id'];
			$data['pp_branch_name'] = $order_info['pp_branch_name'];
			$data['shipping_receipt_number'] = $order_info['shipping_receipt_number'];
			$data['order_status_id'] = $order_info['order_status_id'];
			
			if($data['delivery_type_id'] != 3){
				$total_weight = $this->model_seller_account_pending_order->getTotalWeight($order_detail_id);
				$weight = $this->model_seller_account_pending_order->getWeightClass($order_detail_id);
				$data['total_weight'] = $total_weight;
				$data['weight'] = $weight;
			}
			
			//voucher
			$data['to_name'] = $order_info['to_name'];
			$data['to_email'] = $order_info['to_email'];
			$data['code'] = $order_info['code'];
			$data['order_total'] = $order_info['order_total'];
			$data['description'] = $order_info['description'];
			$data['message'] = $order_info['message'];
			if($data['delivery_type_id'] == 3){
				$data['qty_voucher'] = $this->model_account_order->getTotalOrderVouchers($data['order_id']);
			}
			
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = "<b>" . '{firstname} {lastname}' . "</b>\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . "{telephone}";
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country'],
				'telephone'   => $order_info['telephone']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			//address pickup point
			
			$format_pp = "<b>" . '{pp_name}' . "</b>\n" . '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp}' . "\n" . '{zone_pp}' . "\n" . "{postcode}" . "\n" . '{country_pp}' . "\n" . "{pp_email}" . " (" . "{pp_telephone}" . ")";
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $order_info['pp_name'],
				'company' 		=> $order_info['company'],
				'pp_branch_name'=> $order_info['pp_branch_name'],
				'address'      	=> $order_info['address'],
				'subdistrict_pp'=> $order_info['subdistrict_pp'],
				'district_pp'   => $order_info['district_pp'],
				'city_pp' 		=> $order_info['city_pp'],
				'zone_pp'      	=> $order_info['zone_pp'],
				'postcode' 		=> $order_info['postcode'],
				'country_pp'  	=> $order_info['country_pp'],
				'pp_email'  	=> $order_info['pp_email'],
				'pp_telephone'  => $order_info['pp_telephone']
			);
			
			$data['address_pp'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			
			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProductDetails($this->request->get['order_id'], $data['order_detail_id']);
			
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], 'SSL');
				} else {
					$reorder = '';
				}
				
				//edit by arin
				$this->load->model('tool/image');
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
				
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
				
				$is_complete_return = $this->model_account_order->isCompleteReturn($data['order_detail_id'], $product['product_id']);

				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'] . '&order_detail_id=' . $order_detail_id, 'SSL'),
					
					//edit by arin
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'is_complete_return'	=> $is_complete_return['return_status_id']
				);
			}

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'code' 				=> $voucher['code'],
					'description' 		=> $voucher['description'],
					'message' 			=> $voucher['message'],
					'amount'      		=> $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['subtotal'] = $this->model_account_order->getTotalPricePerInvoice($data['order_detail_id']);
			$data['total_price'] = $this->currency->format($data['subtotal']+$order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			
			$data['comment'] = nl2br($order_info['comment']);

			// History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistoryDetails($this->request->get['order_id'], $data['order_detail_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' 	=> date('d/m/Y H:i', strtotime($result['date_added'])),
					'status'     	=> $result['status'],
					'description'   => $result['description'],
					'identity'   	=> $result['identity'],
					'order_status_id' => $result['order_status_id'],
					'comment'    	=> nl2br($result['comment'])
				);
			}

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/order_info.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/order_info.tpl', $data));
			}
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url . '&order_detail_id=' .$order_detail_id, 'SSL')
			);

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	/*public function reorder() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			if (isset($this->request->get['order_product_id'])) {
				$order_product_id = $this->request->get['order_product_id'];
			} else {
				$order_product_id = 0;
			}

			$order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

			if ($order_product_info) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

				if ($product_info) {
					$option_data = array();

					$order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
						}
					}

					$this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				} else {
					$this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
				}
			}
		}
		
		$this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id . '&order_detail_id=' .$order_detail_id));
	}*/
	
	public function confirm() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
			
		}
		
		$this->load->language('payment/confirmation');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
		
		$this->load->model('localisation/account_destination');
		$this->load->model('localisation/account');
		$this->load->model('payment/confirmation');
		$this->load->model('checkout/order');
		$this->load->model('account/order');
		
		$order=$this->model_account_order->getOrder($order_id);

		if (!empty($order) && $order['payment_method']=='Online Payment') {
			$this->session->data['error'] = $this->language->get('error_confirm');
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
		}
		
		if(empty($order)){
			$this->session->data['error'] = $this->language->get('error_confirm');
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
		}
		
		$data['account_destinations'] = $this->model_localisation_account_destination->getAccountDestinations();
		
		$data['payment_methods'] = $this->model_payment_confirmation->getPaymentMethods();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_payment_confirmation->insertPaymentConfirmation($this->request->post);
			$this->model_checkout_order->addOrderHistory($order_id, '2', '', true);
			
			$order_details = $this->model_payment_confirmation->getOrderDetailId($order_id);
			foreach ($order_details as $order_detail) {
				$data['order_details'][] = array(
					'order_detail_id'   => $order_detail['order_detail_id']
				);
			$this->model_payment_confirmation->insertDetailHistory($order_id, $order_detail['order_detail_id'], '', true);
			}
			
			$invoices = $this->model_account_order->getInvoiceConfirmationById($order_id);
			
			$this->response->redirect($this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL'));
		}
		
		if (isset($this->request->post['input_search'])) {
			$data['input_search'] = $this->request->post['input_search'];
		} else {
			$data['input_search'] = '';
		}
		
		$data['load_account'] = $this->url->link('account/order/load_account', 'page=1', 'SSL');
		$data['search_account'] = $this->url->link('account/order/search_account', 'page=1', 'SSL');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_total_payment'] = $this->language->get('text_total_payment');
		$data['text_date_payment'] = $this->language->get('text_date_payment');
		$data['text_method_payment'] = $this->language->get('text_method_payment');
		$data['text_bank_name'] = $this->language->get('text_bank_name');
		$data['text_account_name'] = $this->language->get('text_account_name');
		$data['text_account_number'] = $this->language->get('text_account_number');
		$data['text_branch'] = $this->language->get('text_branch');
		$data['text_account_destination'] = $this->language->get('text_account_destination');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['text_remarks'] = $this->language->get('text_remarks');
		$data['text_add_bank'] = $this->language->get('text_add_bank');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_choose'] = $this->language->get('text_choose');
		
		$data['error_date_payment'] = $this->language->get('error_date_payment');
		$data['error_account_id'] = $this->language->get('error_account_id');
		$data['error_account_name'] = $this->language->get('error_account_name');
		$data['error_account_number'] = $this->language->get('error_account_number');
		$data['error_account_number_int'] = $this->language->get('error_account_number_int');
		$data['error_branch'] = $this->language->get('error_branch');
		$data['error_account_destination'] = $this->language->get('error_account_destination');
		$data['error_amount_int'] = $this->language->get('error_amount_int');
		$data['error_picture_confirmation'] = $this->language->get('error_picture_confirmation');
		$data['error_date_format'] = $this->language->get('error_date_format');
		$data['error_date_limit'] = $this->language->get('error_date_limit');
		$data['error_date_order'] = $this->language->get('error_date_order');
		
		$data['note_branch'] = $this->language->get('note_branch');
		$data['note_amount'] = $this->language->get('note_amount');
		$data['note_product'] = $this->language->get('note_product');
		$data['note_upload'] = $this->language->get('note_upload');
		$data['note_optional'] = $this->language->get('note_optional');
		
		$data['button_yes'] = $this->language->get('button_yes');
		$data['button_no'] = $this->language->get('button_no');
		$data['button_bank'] = $this->language->get('button_bank');
		
		$data['acc_destination'] = $this->language->get('acc_destination');
		$data['pay_method'] = $this->language->get('pay_method');
		
		$data['column_bank_name'] = $this->language->get('column_bank_name');
		$data['column_bank_code'] = $this->language->get('column_bank_code');
		$data['column_search'] = $this->language->get('column_search');
		$data['column_close'] = $this->language->get('column_close');
		
		/*validation*/
		
		if (isset($this->error['date_payment'])) {
			$data['error_date_payment'] = $this->error['date_payment'];
		} else {
			$data['error_date_payment'] = '';
		}
		
		if (isset($this->error['account_id'])) {
			$data['error_account_id'] = $this->error['account_id'];
		} else {
			$data['error_account_id'] = '';
		}
		
		if (isset($this->error['account_name'])) {
			$data['error_account_name'] = $this->error['account_name'];
		} else {
			$data['error_account_name'] = '';
		}
		
		if (isset($this->error['account_number'])) {
			$data['error_account_number'] = $this->error['account_number'];
		} else {
			$data['error_account_number'] = '';
		}
		
		if (isset($this->error['branch'])) {
			$data['error_branch'] = $this->error['branch'];
		} else {
			$data['error_branch'] = '';
		}
		
		if (isset($this->error['account_destination'])) {
			$data['error_account_destination'] = $this->error['account_destination'];
		} else {
			$data['error_account_destination'] = '';
		}
		
		if (isset($this->error['payment_method'])) {
			$data['error_payment_method'] = $this->error['payment_method'];
		} else {
			$data['error_payment_method'] = '';
		}
		
		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}
		
		/*-----------------*/
		
		/*insert confirm payment*/
		$data['action'] = $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL');
		
		if (isset($this->request->post['input_order_id'])) {
			$data['input_order_id'] = $this->request->post['input_order_id'];
		} else {
			$data['input_order_id'] = '';
		}
		
		if (isset($this->request->post['input_date_payment'])) {
			$data['input_date_payment'] = $this->request->post['input_date_payment'];
		} else {
			$data['input_date_payment'] = '';
		}

		if (isset($this->request->post['input_method_payment'])) {
			$payment_method = explode('-', $this->request->post['input_method_payment']);
			$data['payment_method_id'] = $payment_method[0];
			$data['payment_method_name'] = $payment_method[1];
		} else {
			$data['payment_method_id'] = '';
			$data['payment_method_name'] = '';
		}

		if (isset($this->request->post['id-account-bank'])) {
			$data['id-account-bank'] = $this->request->post['id-account-bank'];
		} else {
			$data['id-account-bank'] = '';
		}
		
		if (isset($this->request->post['input_account_name'])) {
			$data['input_account_name'] = $this->request->post['input_account_name'];
		} else {
			$data['input_account_name'] = '';
		}
		
		if (isset($this->request->post['input_account_number'])) {
			$data['input_account_number'] = $this->request->post['input_account_number'];
		} else {
			$data['input_account_number'] = '';
		}
		
		if (isset($this->request->post['input_branch'])) {
			$data['input_branch'] = $this->request->post['input_branch'];
		} else {
			$data['input_branch'] = '';
		}
		
		if (isset($this->request->post['input_account_destination'])) {
			$data['input_account_destination'] = $this->request->post['input_account_destination'];
		} else {
			$data['input_account_destination'] = '';
		}
		
		if (isset($this->request->post['input_amount'])) {
			$data['input_amount'] = $this->request->post['input_amount'];
		} else {
			$data['input_amount'] = '';
		}
		
		if (isset($this->request->post['picture_confirmation'])) {
			$data['picture_confirmation'] = $this->request->post['picture_confirmation'];
		} else {
			$data['picture_confirmation'] = '';
		}
		
		if (isset($this->request->post['input_remarks'])) {
			$data['input_remarks'] = $this->request->post['input_remarks'];
		} else {
			$data['input_remarks'] = '';
		}
		
		/*------------------*/

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);
		$is_confirmed = $this->model_account_order->isConfirmed($order_id); 

		if ($is_confirmed) {
			$this->load->language('payment/confirmation');
			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL')
			);

			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}
			
			$invoices = $this->model_account_order->getInvoiceConfirmationById($order_id);
		
			foreach ($invoices as $invoice) {
			
				$data['invoices'][] = array(
					'invoice_no'   		=> $invoice['invoice_prefix'].$invoice['invoice_no']
				);
			}
			
			if ($order_info['order_id']) {
				$data['order_id'] = $order_id;
			} else {
				$data['order_id'] = '';
			}
			
			$total_per_invoice = $this->model_account_order->getTotalConfirmationbyId($order_id);
			
			if ($order_info['total']) {
				$data['total'] = $this->currency->format($total_per_invoice, $order_info['currency_code'], $order_info['currency_value']);
			
			} else {
				$data['total'] = '';
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/confirmation.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/confirmation.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/payment/confirmation.tpl', $data));
			}
		} else {
			$this->load->language('error/not_found');
			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order/confirm', 'order_id=' . $order_id, 'SSL')
			);


			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
	
	public function validate() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		$this->load->model('account/order');
		$this->load->language('payment/confirmation');
		
		$date_added = $this->model_account_order->getDateOrder($order_id);
		
		//check payment date
		$order_date = date("Y-m-d", strtotime($date_added));
		$payment_date = $this->request->post['input_date_payment'];
		$diff=date_diff(date_create($order_date),date_create($payment_date));
		
		$total_per_invoice = $this->model_account_order->getTotalConfirmationbyId($order_id);
		
		if ((utf8_strlen(trim($this->request->post['input_date_payment'])) != 10)) {
			$this->error['date_payment'] = $this->language->get('error_date_payment');
		}
		
		if (!empty($this->request->post['input_date_payment']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$this->request->post['input_date_payment'])) {
			$this->error['date_payment'] = $this->language->get('error_date_format');
		}
		
		if($diff->format("%R%a") != "+0" && $diff->format("%R%a") != "+1"){
			$this->error['date_payment'] = $this->language->get('error_payment_date');
		}

		if ((utf8_strlen(trim($this->request->post['id-account-bank'])) < 1) ) {
			$this->error['account_id'] = $this->language->get('error_account_id');
		}

		if ((utf8_strlen($this->request->post['input_account_name']) < 1) ) {
			$this->error['account_name'] = $this->language->get('error_account_name');
		}

		if ((utf8_strlen($this->request->post['input_account_number']) < 1 || !preg_match('/^[0-9]*$/', $this->request->post['input_account_number']))) {
			$this->error['account_number'] = $this->language->get('error_account_number');
		}
		
		if ((int)$this->request->post['id-account-bank'] != 1 && (utf8_strlen($this->request->post['input_branch']) < 1)) {
			$this->error['branch'] = $this->language->get('error_branch');
		}
		
		if ((utf8_strlen($this->request->post['input_account_destination']) < 1) ) {
			$this->error['account_destination'] = $this->language->get('error_account_destination');
		}
		
		if ((utf8_strlen($this->request->post['input_method_payment']) < 2) ) {
			$this->error['payment_method'] = $this->language->get('error_payment_method');
		}
		
		if ((utf8_strlen($this->request->post['input_amount']) < 1) || (floatval($this->request->post['input_amount']) != floatval($total_per_invoice)) || !preg_match('/^[0-9]*$/', $this->request->post['input_amount'])) {
			$this->error['amount'] = $this->language->get('error_amount');
		}
		
		return !$this->error;
	}
	
	public function success() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
			
		}
		
		$this->load->language('payment/success');
		
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('account/order');
		
		// $order_info = $this->model_account_order->getOrder($order_id);
		$total_per_invoice = $this->model_account_order->getTotalConfirmationbyId($order_id);
		
		$this->load->model('payment/confirmation');
		
		$data['total_payment'] = $total_per_invoice;
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->load->language('payment/success');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_success'] = $this->language->get('heading_success');
		$data['text_first'] = $this->language->get('text_first');
		$data['text_second'] = $this->language->get('text_second');
		$data['text_third'] = $this->language->get('text_third');
		$data['text_fourth'] = sprintf($this->language->get('text_fourth'), $this->url->link('account/order', '' , 'SSL'));
		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);
		$is_success = $this->model_account_order->isSuccess($order_id); 

		if ($is_success) {
			$this->load->language('payment/success');
			$this->document->setTitle($this->language->get('text_order'));
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);
			
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_success'),
				'href' => $this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL')
			);
		
			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}
			
			if ($order_info['total']) {
				$data['total'] = $this->currency->format($total_per_invoice, $order_info['currency_code'], $order_info['currency_value']);
			
			} else {
				$data['total'] = '';
			}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/success.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/success.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/payment/success.tpl', $data));
			}
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_success'),
				'href' => $this->url->link('account/order/success', 'order_id=' . $order_id, 'SSL')
			);

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
	
	public function load_account(){
		$this->load->language('payment/confirmation');
		$data['column_bank_name'] = $this->language->get('column_bank_name');
		$data['column_bank_code'] = $this->language->get('column_bank_code');
		$data['column_search'] = $this->language->get('column_search');
		$data['column_close'] = $this->language->get('column_close');
		
		$this->load->model('localisation/account');
		/*pagination*/
		$accounts_per_page = 10;
		if ((int)$accounts_per_page == 0)
			$accounts_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$data['accounts'] = $this->model_localisation_account->getAccounts(
			array(
				'order_by' => 'account_id',
				'order_way' => 'ASC',
				'offset' => ($page - 1) * $accounts_per_page,
				'limit' => $accounts_per_page
			)
		);
		
		$total_accounts = $this->model_localisation_account->getTotalAccounts();
		
		$pagination = new Pagination();
		$pagination->total = $total_accounts;
		$pagination->page = $page;
		$pagination->limit = $accounts_per_page; 
		$pagination->url = $this->url->link('account/order/load_account', '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/account_bank.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/account_bank.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/payment/account_bank.tpl', $data));
		}
	}
	
	public function search_account(){
		$this->load->language('payment/confirmation');
		$data['column_bank_name'] = $this->language->get('column_bank_name');
		$data['column_bank_code'] = $this->language->get('column_bank_code');
		$data['column_search'] = $this->language->get('column_search');
		$data['column_close'] = $this->language->get('column_close');
		$data['text_empty'] = $this->language->get('text_empty');
		
		if (isset($this->request->get['input_search'])) {
			$data['input_search'] = $this->request->get['input_search'];
		} else {
			$data['input_search'] = '';
		}
		
		$this->load->model('localisation/account');
		$account_name = $data['input_search'];
		
		/*pagination*/
		$accounts_per_page = 10;
		if ((int)$accounts_per_page == 0)
			$accounts_per_page = 10;
			
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		
		$data['accounts'] = $this->model_localisation_account->searchAccount(
			$account_name,
			array(
				'order_by' => 'account_id',
				'order_way' => 'ASC',
				'offset' => ($page - 1) * $accounts_per_page,
				'limit' => $accounts_per_page
			)
		);
		
		$total_accounts_search = $this->model_localisation_account->totalSearchAccount($account_name);
		
		$pagination = new Pagination();
		$pagination->total = $total_accounts_search;
		$pagination->page = $page;
		$pagination->limit = $accounts_per_page; 
		$pagination->url = $this->url->link('account/order/search_account', '&page={page}&input_search='.$data['input_search'], 'SSL');
		$data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/account_bank.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/account_bank.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/payment/account_bank.tpl', $data));
		}
	}
	
	public function receiveOrder(){
		$this->load->language('account/order');
		$this->load->model('account/order');
		
		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		$this->model_account_order->receiveOrder($order_detail_id);
		
		$this->session->data['success']=$this->language->get('text_success_receive');
		$this->response->redirect($this->url->link('account/order/info', 'order_id='. $order_id . '&order_detail_id=' . $order_detail_id , 'SSL'));
	}
	
	public function completeOrder(){
		$this->load->language('account/order');
		$this->load->model('account/order');
		
		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		$this->model_account_order->completeOrder($order_detail_id);
		
		$this->session->data['success']=$this->language->get('text_success_complete');
		$this->response->redirect($this->url->link('account/review', '', 'SSL'));
	}
	/*
	public function reorder(){
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$this->load->model('account/address');
		$this->load->model('account/seller');
		$this->load->model('localisation/city');
		$this->load->model('shipping/shipping');
							
		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		$order_info = $this->model_account_order->getOrderDetail($order_detail_id);
		echo json_encode($order_info);
		if(!empty($order_info)){
			$products = $this->model_account_order->getOrderProductDetails($order_id, $order_detail_id);
			
			foreach($products as $product){
			
			}
			
			foreach($products as $product){
					if (isset($this->request->post['product_id'])) {
						$product_id = (int)$this->request->post['product_id'];
					} else {
						$product_id = 0;
					}

					$product_info = $this->model_catalog_product->getProduct($product_id);
					
					
					if (isset($this->request->post['product_amount'])) {
						$product_amount=(int)$this->request->post['product_amount'];
					} else {
						$product_amount = 1;
					}
					if (isset($this->request->post['option'])) {
						$options = array_filter($this->request->post['option']);
					} else {
						$options = array();
					}
					if ($product_info) {
						//CHECK STOCK
						$stockOption=$this->model_catalog_product->checkStockOption($product_id,$options,$product_amount);
						if(((int)$product_info['quantity']-$product_amount)>=0 && $stockOption==true){
							//CHECK DELIVERY TYPE
							$shipping=array();
							$delivery_type=(int)$this->request->post['delivery_type'];
							
							//Get Seller Data
							$seller=$this->model_account_address->getCheckoutSellerAddress(
								array(
									"product_id" => $product_id
									)
								);
							if($delivery_type==1){
								//CHECK ADDRESS STATUS
								$address_id=0;
								$new_address=(int)$this->request->post['new_address'];
								if($new_address==0){
									if ($this->request->post['shipping_address']== '') {
										$json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
									}else{
										$address_id=$this->request->post['shipping_address'];
									}
								}else{
									//add_firstname
									//add_lastname
									//add_company
									//add_address_1
									//add_address_2
									//add_country_id
									//add_zone_id
									//add_city_id
									//add_district_id
									//add_subdistrict_id
									//add_postcode
									
									if ((utf8_strlen(trim($this->request->post['add_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['add_firstname'])) > 32)) {
										$json['error']['delivery']['add_firstname'] = $this->language->get('error_add_firstname');
									}

									if ((utf8_strlen(trim($this->request->post['add_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['add_lastname'])) > 32)) {
										$json['error']['delivery']['add_lastname'] = $this->language->get('error_add_lastname');
									}

									if ((utf8_strlen(trim($this->request->post['add_address_1'])) < 3) || (utf8_strlen(trim($this->request->post['add_address_1'])) > 128)) {
										$json['error']['delivery']['add_address_1'] = $this->language->get('error_add_address_1');
									}

									if (!isset($this->request->post['add_country_id'])  || $this->request->post['add_country_id'] == '') {
										$json['error']['delivery']['add_country_id'] = $this->language->get('error_add_country_id');
									}

									if (!isset($this->request->post['add_zone_id']) || $this->request->post['add_zone_id'] == '') {
										$json['error']['delivery']['add_zone_id'] = $this->language->get('error_add_zone_id');
									}
									
									if (!isset($this->request->post['add_city_id']) || $this->request->post['add_city_id'] == '') {
										$json['error']['delivery']['add_city_id'] = $this->language->get('error_add_city_id');
									}
									
									if (!isset($this->request->post['add_district_id']) || $this->request->post['add_district_id'] == '') {
										$json['error']['delivery']['add_district_id'] = $this->language->get('error_add_district_id');
									}
									
									if (!isset($this->request->post['add_subdistrict_id']) || $this->request->post['add_subdistrict_id'] == '') {
										$json['error']['delivery']['add_subdistrict_id'] = $this->language->get('error_add_subdistrict_id');
									}
									
									if (!isset($this->request->post['add_postcode']) || $this->request->post['add_postcode'] == '') {
										$json['error']['delivery']['add_postcode'] = $this->language->get('error_add_postcode');
									}
									
									if(!isset($json['error']['delivery'])){
										$address_id=$this->model_account_address->addAddress(
											array(
												"firstname"=>$this->request->post['add_firstname'],
												"lastname"=>$this->request->post['add_lastname'],
												"company"=>$this->request->post['add_company'],
												"address_1"=>$this->request->post['add_address_1'],
												"address_2"=>$this->request->post['add_address_2'],
												"country_id"=>$this->request->post['add_country_id'],
												"zone_id"=>$this->request->post['add_zone_id'],
												"city_id"=>$this->request->post['add_city_id'],
												"district_id"=>$this->request->post['add_district_id'],
												"subdistrict_id"=>$this->request->post['add_subdistrict_id'],
												"postcode"=>$this->request->post['add_postcode']
											)
										);
									}
								}
								
								if ($this->request->post['shipping_id']== '') {
									$json['error']['delivery']['shipping_id'] = $this->language->get('error_shipping_id');
								}
								
								if ($this->request->post['shipping_service_id']== '') {
									$json['error']['delivery']['shipping_service_id'] = $this->language->get('error_shipping_service_id');
								}
								
								if ($this->request->post['insurance']== '') {
									$json['error']['delivery']['insurance'] = $this->language->get('error_insurance');
								}
										
								if(!isset($json['error']['delivery'])){
									//CHECK VALID SHIPPING
									$customer=$this->model_account_address->getCheckoutCustomerAddress(
										array(
											"customer_id" => $this->customer->getId(),
											"address_id" => $address_id
											)
										);
										
									if(!empty($customer) && !empty($seller)){
										//Use API Raja Ongkir
										$this->session->data['payment_address_id']=$address_id;
										$DESTINATION=$this->model_localisation_city->getCity($customer['city_id'])['ro_id'];			
										$ORIGIN=$this->model_localisation_city->getCity($seller['city_id'])['ro_id'];
										$SELLER_ID=$seller['seller_id'];
										$CART_WEIGHT=$this->cart->getWeightShipping(
											array(
												'seller_id'=>$SELLER_ID,
												'shipping_address'=>$address_id,
												'shipping_service_id'=>$this->request->post['shipping_service_id']
											)
										);
										$WEIGHT=$CART_WEIGHT+(int)ceil($this->weight->convert($product_info['weight'], $product_info['weight_class_id'], $this->config->get('config_weight_class_id'))*$product_amount);
										
										
										$logisticRO=$this->rajaongkir->getServiceRajaOngkir($ORIGIN,$DESTINATION,$WEIGHT);

										if(!empty($logisticRO)){
											$shipping_code=$this->model_shipping_shipping->getShippingCode($this->request->post['shipping_id']);
											$shipping_service=$this->model_shipping_shipping->getShippingService($this->request->post['shipping_service_id']);
											$price=$this->getPriceShippingRO($logisticRO,$shipping_code,$shipping_service);
											if($price>=0){
												$shipping['delivery']['price']=$price;
												$shipping['delivery']['shipping_address']=$address_id;
												$shipping['delivery']['seller_id']=$SELLER_ID;
												$shipping['delivery']['origin']=$seller['city_id'];
												$shipping['delivery']['destination']=$customer['city_id'];
												$shipping['delivery']['origin_ro']=$ORIGIN;
												$shipping['delivery']['destination_ro']=$DESTINATION;
											}else{
												$json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
											}
										}else{
											$json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
										}
									}else{
										$json['error']['delivery']['shipping_address'] = $this->language->get('error_shipping_address');
									}
								}
								
								if(!isset($json['error']['delivery'])){
									$shipping['delivery_type'] = $delivery_type;
									$shipping['delivery']['shipping_id'] = $this->request->post['shipping_id'];
									$shipping['delivery']['shipping_service_id'] = $this->request->post['shipping_service_id'];
									$shipping['delivery']['insurance'] = $this->request->post['insurance'];
								}
								
							}else if($delivery_type==2){
								//CHECK PICKUP PERSON STATUS
								
								if(isset($this->request->post['pickup_person_status'])){
									$pickup_person_status=(int)$this->request->post['pickup_person_status'];
								}else{
									$pickup_person_status=0;
								}
								if($pickup_person_status==1){
									if ((utf8_strlen(trim($this->request->post['pickup_person_name'])) < 1) || (utf8_strlen(trim($this->request->post['pickup_person_name'])) > 32)) {
										$json['error']['pickup']['name'] = $this->language->get('error_pickup_person_name');
									}
									if ((utf8_strlen($this->request->post['pickup_person_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['pickup_person_email'])) {
										$json['error']['pickup']['email'] = $this->language->get('error_pickup_person_email');
									}

									if ((utf8_strlen($this->request->post['pickup_person_phone']) < 3) || (utf8_strlen($this->request->post['pickup_person_phone']) > 32)) {
										$json['error']['pickup']['phone'] = $this->language->get('error_pickup_person_phone');
									}
									
									if(!isset($json['error']['pickup_person'])){
										$shipping['pickup']['name'] = $this->request->post['pickup_person_name'];
										$shipping['pickup']['email'] = $this->request->post['pickup_person_email'];
										$shipping['pickup']['phone'] = $this->request->post['pickup_person_phone'];
									}
								}
								
								if ($this->request->post['pp_branch_id']== '') {
									$json['error']['pickup']['branch'] = $this->language->get('error_pickup_branch');
								}
								
								if(!isset($json['error']['pickup'])){
									$shipping['delivery_type'] = $delivery_type;
									$shipping['pickup']['seller_id'] = $seller['seller_id'];
									$shipping['pickup']['branch'] = $this->request->post['pp_branch_id'];
									$shipping['pickup']['status'] = $pickup_person_status;
								}
								
							}else{
								$json['error']['delivery_type']=$this->language->get('error_delivery_type');
							}
						}else{
							$json['error']['amount']=$this->language->get('error_amount');
						}
						
						if(!isset($json['error'])){
							//SET SHIPPING
							$shipping['information']=$this->request->post['product_information'];

							if (isset($this->request->post['option'])) {
								$option = array_filter($this->request->post['option']);
							} else {
								$option = array();
							}

							$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

							foreach ($product_options as $product_option) {
								if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
									$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
								}
							}

							if (isset($this->request->post['recurring_id'])) {
								$recurring_id = $this->request->post['recurring_id'];
							} else {
								$recurring_id = 0;
							}

							$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

							if ($recurrings) {
								$recurring_ids = array();

								foreach ($recurrings as $recurring) {
									$recurring_ids[] = $recurring['recurring_id'];
								}

								if (!in_array($recurring_id, $recurring_ids)) {
									$json['error']['recurring'] = $this->language->get('error_recurring_required');
								}
							}

							if (!$json) {
								$this->cart->add($this->request->post['product_id'], $product_amount, $option, $recurring_id, $shipping);


								$this->load->model('tool/image');
								$json['image'] = Journal2Utils::resizeImage($this->model_tool_image, $product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
							
								$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

								unset($this->session->data['shipping_method']);
								unset($this->session->data['shipping_methods']);
								unset($this->session->data['payment_method']);
								unset($this->session->data['payment_methods']);

								// Totals
								$this->load->model('extension/extension');

								$total_data = array();
								$total = 0;
								$taxes = $this->cart->getTaxes();

								// Display prices
								if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
									$sort_order = array();

									$results = $this->model_extension_extension->getExtensions('total');

									foreach ($results as $key => $value) {
										$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
									}

									array_multisort($sort_order, SORT_ASC, $results);

									foreach ($results as $result) {
										if ($this->config->get($result['code'] . '_status')) {
											$this->load->model('total/' . $result['code']);

											$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
										}
									}

									$sort_order = array();

									foreach ($total_data as $key => $value) {
										$sort_order[$key] = $value['sort_order'];
									}

									array_multisort($sort_order, SORT_ASC, $total_data);
								}

								$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
							} else {
								$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
							}
							
						}
					}
			}	
		}
	}*/
	
	public function invoice(){
		$this->load->language('account/order');

		if (isset($this->request->get['order_detail_id'])) {
			$order_detail_id = $this->request->get['order_detail_id'];
		} else {
			$order_detail_id = 0;
		}
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id='. $order_id . '&order_detail_id=' . $order_detail_id , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->model('account/order');
		$this->load->model('seller/account_pending_order');

		$order_info = $this->model_account_order->getOrderDetail($order_detail_id);
		
		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_receipt'] = $this->language->get('text_receipt');
			$data['text_seller'] = $this->language->get('text_seller');
			$data['text_seller_invoice'] = $this->language->get('text_seller_invoice');
			$data['text_print'] = $this->language->get('text_print');
			$data['text_complete_return'] = $this->language->get('text_complete_return');
			$data['text_invoice_info'] = $this->language->get('text_invoice_info');
			$data['text_delivery_info'] = $this->language->get('text_delivery_info');
			$data['text_order_info'] = $this->language->get('text_order_info');
			$data['text_note_invoice'] = $this->language->get('text_note_invoice');
			$data['text_note_desc'] = $this->language->get('text_note_desc');

			$data['column_customer'] = $this->language->get('column_customer');
			$data['column_buyer'] = $this->language->get('column_buyer');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');
			$data['column_invoice'] = $this->language->get('column_invoice');
			$data['column_order_id'] = $this->language->get('column_order_id');
			$data['column_payment_method'] = $this->language->get('column_payment_method');
			$data['column_shipping_method'] = $this->language->get('column_shipping_method');
			$data['column_receiver'] = $this->language->get('column_receiver');
			$data['column_telp'] = $this->language->get('column_telp');
			$data['column_voucher_amount'] = $this->language->get('column_voucher_amount');
			
			
			//edit by arin
			$data['column_shipping'] = $this->language->get('column_shipping');
			$data['column_product_list'] = $this->language->get('column_product_list');
			$data['column_buyer'] = $this->language->get('column_buyer');
			$data['column_insurance'] = $this->language->get('column_insurance');
			$data['column_remarks'] = $this->language->get('column_remarks');
			$data['column_voucher'] = $this->language->get('column_voucher');
			$data['text_voucher_info'] = $this->language->get('text_voucher_info');
			$data['column_name_voucher'] = $this->language->get('column_name_voucher');
			$data['column_email_voucher'] = $this->language->get('column_email_voucher');
			$data['column_date_order'] = $this->language->get('column_date_order');
			$data['column_npwp'] = $this->language->get('column_npwp');
			
			//voucher
			$data['column_code'] = $this->language->get('column_code');
			$data['column_message'] = $this->language->get('column_message');
			$data['column_description'] = $this->language->get('column_description');
			$data['text_receiver_name'] = $this->language->get('text_receiver_name');
			$data['text_receiver_email'] = $this->language->get('text_receiver_email');
			$data['text_voucher_data'] = $this->language->get('text_voucher_data');
			$data['text_voucher'] = $this->language->get('text_voucher');
			$data['column_sku'] = $this->language->get('column_sku');
			
			$data['text_subtotal'] = $this->language->get('text_subtotal');
			$data['text_total'] = $this->language->get('text_total');

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_id'] = $this->request->get['order_id'];
			
			//edit by arin
			
			
			$data['order_detail_id'] = $this->model_account_order->getOrderDetailId($order_detail_id);
			$data['qty'] = $this->model_account_order->getQuantity($data['order_detail_id']);
			$data['delivery_type'] = $order_info['delivery_type'];
			$data['delivery_type_id'] = $order_info['delivery_type_id'];
			$data['shipping_id'] = $order_info['shipping_id'];
			$data['shipping_name'] = $order_info['shipping_name'];
			$data['shipping_service_name'] = $order_info['shipping_service_name'];
			$data['currency_code'] = $this->currency->getSymbolLeft($order_info['currency_code']);
			
			$data['shipping_insurance'] = $this->currency->getNumeric($order_info['shipping_insurance'], $order_info['currency_code'], $order_info['currency_value']);
			
			$data['shipping_price'] = $this->currency->getNumeric($order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			$data['pp_branch_id'] = $order_info['pp_branch_id'];
			$data['pp_branch_name'] = $order_info['pp_branch_name'];
			$data['shipping_receipt_number'] = $order_info['shipping_receipt_number'];
			$data['order_status_id'] = $order_info['order_status_id'];
			
			if($data['delivery_type_id'] != 3){
				$total_weight = $this->model_seller_account_pending_order->getTotalWeight($order_detail_id);
				$weight = $this->model_seller_account_pending_order->getWeightClass($order_detail_id);
				$data['total_weight'] = $total_weight;
				$data['weight'] = $weight;
			}
			
			//voucher
			$data['to_name'] = $order_info['to_name'];
			$data['to_email'] = $order_info['to_email'];
			$data['from_name'] = $order_info['from_name'];
			$data['from_email'] = $order_info['from_email'];
			$data['code'] = $order_info['code'];
			$data['order_total'] = $order_info['order_total'];
			$data['description'] = $order_info['description'];
			$data['message'] = $order_info['message'];
			if($data['delivery_type_id'] == 3){
				$data['qty_voucher'] = $this->model_account_order->getTotalOrderVouchers($data['order_id']);
			}
			
			$data['voucher_price'] = $this->model_account_order->getTotalPriceVoucher($data['order_id']);
			$data['total_price_voucher'] = $this->currency->getNumeric($data['voucher_price']+$order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];
			$data['payment_name'] = $order_info['payment_firstname'] . $order_info['payment_lastname'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . ", " . '{country}' ;
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}',
				'{telephone}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country'],
				'telephone'   => $order_info['telephone']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			$data['shipping_name'] = $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'];
			$data['shipping_telp'] = $order_info['telephone'];
			
			//address pickup point
			
			$format_pp = '{company} - {pp_branch_name}' . "\n" . '{address}, {subdistrict_pp}' . "\n" . '{district_pp} {city_pp} {postcode}' . "\n" . '{zone_pp}' . ", " . '{country_pp}' ;
			
			
			$find_pp = array(
				'{pp_name}',
				'{company}',
				'{pp_branch_name}',
				'{address}',
				'{subdistrict_pp}',
				'{district_pp}',
				'{city_pp}',
				'{zone_pp}',
				'{postcode}',
				'{country_pp}',
				'{pp_email}',
				'{pp_telephone}'
			);

			$replace_pp = array(
				'pp_name'		=> $order_info['pp_name'],
				'company' 		=> $order_info['company'],
				'pp_branch_name'=> $order_info['pp_branch_name'],
				'address'      	=> $order_info['address'],
				'subdistrict_pp'=> $order_info['subdistrict_pp'],
				'district_pp'   => $order_info['district_pp'],
				'city_pp' 		=> $order_info['city_pp'],
				'zone_pp'      	=> $order_info['zone_pp'],
				'postcode' 		=> $order_info['postcode'],
				'country_pp'  	=> $order_info['country_pp'],
				'pp_email'  	=> $order_info['pp_email'],
				'pp_telephone'  => $order_info['pp_telephone']
			);
			
			$data['address_pp'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find_pp, $replace_pp, $format_pp))));
			$data['pp_name'] = $order_info['pp_name'];
			$data['pp_telp'] = $order_info['pp_telephone'];
			
			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProductDetails($this->request->get['order_id'], $data['order_detail_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], 'SSL');
				} else {
					$reorder = '';
				}
				
				//edit by arin
				$this->load->model('tool/image');
				$product_image = $this->model_catalog_product->getProduct($product['product_id']);
				
				if($product_image['image']){
					$image = $this->model_tool_image->resize($product_image['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}
				
				$is_complete_return = $this->model_account_order->isCompleteReturn($data['order_detail_id'], $product['product_id']);

				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'sku'      => $product['sku'],
					'comment'  => $product['comment'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->getNumeric($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'] . '&order_detail_id=' . $order_detail_id, 'SSL'),
					
					//edit by arin
					'thumb'	   				=> $image,
					'href'	   				=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'is_complete_return'	=> $is_complete_return['return_status_id'],
					'total_normal' 			=> $this->currency->format($product['total_normal']),
					'payment_total' 		=> $this->currency->format($product['payment_total']),
					'discount_merchant' 	=> $product['discount_merchant'],
					'discount_ofiskita' 	=> $product['discount_ofiskita']
				);
			}

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'to_name' 			=> $voucher['to_name'],
					'to_email' 			=> $voucher['to_email'],
					'code' 				=> $voucher['code'],
					'description' 		=> $voucher['description'],
					'message' 			=> $voucher['message'],
					'amount'      		=> $this->currency->getNumeric($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					'price'      		=> $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$subtotal = $this->model_account_order->getTotalPricePerInvoice($data['order_detail_id']);
			$data['subtotal']=$this->currency->getNumeric($subtotal);
			$data['total_price'] = $this->currency->getNumeric($subtotal+$order_info['shipping_price'], $order_info['currency_code'], $order_info['currency_value']);
			
			$data['comment'] = nl2br($order_info['comment']);
			
			$data['totals'] = array();
			
			$totals = $this->model_account_order->getOrderTotalsInvoice($this->request->get['order_id']);
			
			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' 		=> $total['title'],
					'code' 			=> $total['code'],
					'value' 		=>$this->currency->getNumeric($total['value'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistoryDetails($this->request->get['order_id'], $data['order_detail_id']);
			
			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' 	=> date('d/m/Y H:i', strtotime($result['date_added'])),
					'status'     	=> $result['status'],
					'description'   => $result['description'],
					'identity'   	=> $result['identity'],
					'order_status_id' => $result['order_status_id'],
					//'payment_total' => $result['payment_total'],
					//'total_normal' => $result['total_normal'],
					//'discount_merchant' => $result['discount_merchant'],
					//'discount_ofiskita' => $result['discount_ofiskita'],
					'comment'    	=> nl2br($result['comment'])
				);
			}
			
			//Seller
			$seller = $this->MsLoader->MsSeller->getSeller((int)$order_info['seller_id']);

			if (!$seller) {
				$data['seller'] = NULL;
			} else {
				$data['seller'] = array();
				
				$data['seller']['nickname'] = $seller['ms.nickname'];
				$data['seller']['seller_id'] = $seller['seller_id'];
				$data['seller']['company'] = $seller['ms.company'];
				$data['seller']['npwp'] = $seller['ms.npwp_number'];
				 
				$data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
				
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_invoice.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/order_invoice.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/order_invoice.tpl', $data));
			}
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] .'&order_detail_id=' .$order_detail_id, 'SSL')
			);

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
	
	public function waybill(){
		$result=array();
		if ($this->customer->isLogged()) {
			if (isset($this->request->get['order_detail_id'])) {
				$order_detail_id = $this->request->get['order_detail_id'];
			} else {
				$order_detail_id = 0;
			}
			
			$this->load->model('account/order');
			$this->load->model('shipping/shipping');

			$order_info = $this->model_account_order->getOrderDetail($order_detail_id);
			
			if($order_info){
				if($order_info['shipping_id']!='' && $order_info['shipping_id']!=NULL && (int)$order_info['shipping_id']!=0){
					$shipping_code = $this->model_shipping_shipping->getShippingCode($order_info['shipping_id']);
					$waybill_info = $this->rajaongkir->checkWaybillRajaOngkir($order_info['shipping_receipt_number'],$shipping_code);
					
					if(!empty($waybill_info)){
						$result['waybill']=$waybill_info;
					}
				}
			}
		}
		
		//Set Response
		if(!empty($result)){
			$result['status']=array(
				"code"=>200,
				"description"=> "OK"
			);
		}else{
			$result['status']=array(
				"code"=>500,
				"description"=> "ERROR"
			);
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($result));
	}
}