<?php
class ControllerAccountReview extends Controller {
	public function index() {		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
		
		$this->load->language('account/review');
                $this->load->language('account/account');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_review'),
			'href' => $this->url->link('account/review', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['heading_title'] = $this->language->get('text_review');
                $data['text_my_account'] = $this->language->get('text_my_account');

		$data['text_display'] = $this->language->get('text_display');
		$data['text_all'] = $this->language->get('text_all');
		$data['text_unread'] = $this->language->get('text_unread');
		$data['text_search'] = $this->language->get('text_search');
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/review.tpl', $data));
		}
	}
	
	public function loadReview(){
		$this->load->language('account/review');
                $this->load->language('account/account');
		$this->load->model('account/review');
		$this->load->model('tool/image');
                 $data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_seller'] = $this->language->get('text_seller');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_no_review'] = $this->language->get('text_no_review');
		$data['entry_review'] = $this->language->get('entry_review');
		$data['text_note'] = $this->language->get('text_note');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['entry_rating'] = $this->language->get('entry_rating');
		$data['entry_accuracy'] = $this->language->get('entry_accuracy');
		$data['entry_bad'] = $this->language->get('entry_bad');
		$data['entry_good'] = $this->language->get('entry_good');
		$data['button_hide'] = $this->language->get('button_hide');
		$data['button_show'] = $this->language->get('button_show');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['reviews']=array();
		$page = (isset($this->request->get['page'])) ? (int)$this->request->get['page'] : 1;
		if ($this->customer->isLogged() && !$this->pickup->isPickupPoint() && !$this->MsLoader->MsSeller->isSeller()) {
			if(isset($this->request->get['key'])){
				$data['key']=$this->request->get['key'];
				$data['search']="";
				$data_search=array(
					'key'=>$this->request->get['key'],
					'customer_id'=> $this->customer->getId()
				);
				if(isset($this->request->post['search'])){
					$data_search = array_merge($data_search, array("search" => $this->request->post['search']));
					$data['search']=$this->request->post['search'];
				}
				$reviews=$this->model_account_review->getReview($data_search);
				if(!empty($reviews)){
					$result=array();
					foreach($reviews as $review){
						if($review['avatar']){
							$review['avatar']=$this->model_tool_image->resize($review['avatar'], 50, 50);
						}else{
							$review['avatar']=$this->model_tool_image->resize('ms_no_image.jpg', 50, 50);
						}
						
						if($review['product_image']){
							$review['product_image']=$this->model_tool_image->resize($review['product_image'], 50, 50);
						}else{
							$review['product_image']=$this->model_tool_image->resize('no_image.png', 50, 50);
						}
						
						$result[$review['order_detail_id']]['order_detail_id']=$review['order_detail_id'];
						$result[$review['order_detail_id']]['order_id']=$review['order_id'];
						$result[$review['order_detail_id']]['invoice']=$review['invoice'];
						$result[$review['order_detail_id']]['seller']=$review['seller'];
						$result[$review['order_detail_id']]['avatar']=$review['avatar'];
						$result[$review['order_detail_id']]['date_added']=date('d/m/Y H:i', strtotime($review['date_added']));
						$result[$review['order_detail_id']]['review'][]=array(
							'product_name'=>$review['product_name'],
							'product_href'=>$this->url->link('product/product', 'product_id='.$review['product_id'], 'SSL'),
							'product_image'=>$review['product_image'],
							'product_id'=>$review['product_id'],
							'review_id'=>$review['review_id'],
							'review_author'=>$review['review_author'],
							'review_rating'=>$review['review_rating'],
							'review_text'=>$review['review_text'],
							'review_accuracy'=>$review['review_accuracy'],
							'review_date_added'=>date('d/m/Y H:i', strtotime($review['review_date_added']))
						);
					}
					/*$temp_order_detail_id=array();
					foreach($result as $res){
						$temp_order_detail_id[]=$res['order_detail_id'];
					}
					
					$temp_data=array();
					for($i;$i<count($temp_order_detail_id);$i++){
						
					}*/
					
					$pagination = new Pagination();
					$pagination->total = count($result);
					$pagination->page = $page;
					$pagination->limit = 5; 
					$pagination->text = $this->language->get('text_pagination');
					$pagination->url = $this->url->link('account/review/loadReview', '&page={page}' . (isset($this->request->get['key']) ? "&key=".$this->request->get['key'] : ''));
					$data['pagination'] = $pagination->render();
					
					$data['reviews']=array_slice($result, ($page-1)*5, 5);
					
				}
				
			}
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/review_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/review_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/review_list.tpl', $data));
		}
	}
	
	public function write(){
		$this->load->language('account/review');

		$json = array();
		
		$this->load->model('account/review');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}
			
			if (empty($this->request->post['rating_accuracy']) || $this->request->post['rating_accuracy'] < 0 || $this->request->post['rating_accuracy'] > 5) {
				$json['error'] = $this->language->get('error_rating_accuracy');
			}
			
			if($this->model_account_review->checkReview($this->request->post)){
				$json['error'] = $this->language->get('error_review');
			}
			
			if (!isset($json['error'])) {
				$json = $this->model_account_review->addReview($this->request->post);
				$json = array_merge($json, array("success" => $this->language->get('text_success')));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}