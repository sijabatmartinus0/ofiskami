<?php
class ControllerAccountActivate extends Controller {
	private $error = array();
	// private $data = array();
	
	public function index() {
		
		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
		
		if((int)$this->customer->getVerified()==1){
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}
		$this->load->language('account/activate');
		
		if(isset($this->request->get['code'])){
			if ($this->customer->isLogged()) {
					$this->event->trigger('pre.customer.logout');

					$this->customer->logout();
					$this->cart->clear();

					unset($this->session->data['wishlist']);
					unset($this->session->data['shipping_address']);
					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_address']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
					unset($this->session->data['comment']);
					unset($this->session->data['order_id']);
					unset($this->session->data['coupon']);
					unset($this->session->data['reward']);
					unset($this->session->data['voucher']);
					unset($this->session->data['vouchers']);

					$this->event->trigger('post.customer.logout');
					
				}
				
			$this->load->model('account/customer');
			$result = $this->model_account_customer->emailVerification($this->request->get['code']);
			$data['text_email']= $this->language->get('text_email');
			if($result['response']==true){
				
				$this->model_account_customer->deleteLoginAttempts($result['customer']['email']);
				
				$this->customer->login($result['customer']['email'], "", true);

				unset($this->session->data['guest']);
				
				$data['text_success_email_verification']= $this->language->get('text_success_email_verification');
			}else{
				$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

				$this->response->redirect($this->url->link('account/login', '', 'SSL'));
			}
		}
		
		/*if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}*/
		
		if(isset($this->request->get['mail'])){
			if((int)$this->request->get['mail']==0 && $this->customer->isLogged()){
				$this->load->model('account/customer');
				$this->model_account_customer->sendActivationMail($this->customer->getId());
				$data['text_success_email_verification']= $this->language->get('text_resend_email');
			}/*else if((int)$this->request->get['mail']==1 && isset($this->request->post['email'])){
				$this->load->model('account/customer');
				$this->model_account_customer->sendActivationMailByEmail($this->request->post['email']);
				$data['text_success_email_verification']= $this->language->get('text_email');
			}else{
				$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

				$this->response->redirect($this->url->link('account/login', '', 'SSL'));
			}*/
		}
		

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_activate'),
			'href' => $this->url->link('account/activate')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($this->config->get('config_customer_group_id'));

		if ($customer_group_info && !$customer_group_info['approval']) {
			$data['text_message'] = sprintf($this->language->get('text_message'), $this->customer->getEmail(),$this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_approval'), $this->config->get('config_name'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_shop'] = $this->language->get('button_shop');

		if ($this->cart->hasProducts()) {
			$data['continue'] = $this->url->link('checkout/cart');
		} else {
			$data['continue'] = $this->url->link('common/home', '', 'SSL');
		}
		
		$data['text_progress'] =  $this->language->get('text_progress');

		/*Verified Status 0:Not Verified, 1:Email Verified, 2:Phone Number Verified*/
		if((int)$this->customer->getVerified()==0){
			$data['text_progress_message']=sprintf($this->language->get('text_message_email'), $this->url->link('account/activate').'&mail=0');
			$data['text_progress_percent']=60;
		}else if((int)$this->customer->getVerified()==1){
			$data['text_progress_message']=sprintf($this->language->get('text_message_complete'),$this->url->link('account/activate/phone'));
			$data['text_progress_percent']=100;
		}else if((int)$this->customer->getVerified()==2){
			$data['text_progress_message']=sprintf($this->language->get('text_message_complete'), $this->url->link('common/home'));
			$data['text_progress_percent']=100;	
		}else{
			/*Back to login*/
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/activate.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/activate.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/activate.tpl', $data));
		}
	}
	public function phone(){
		/*$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
		$this->response->redirect($this->url->link('account/login', '', 'SSL'));*/
		// $mail = new Mail($this->config->get('config_mail'));
		// $mail->setTo("rizki0022@gmail.com");
		// $mail->setFrom("coeagit.dev@gmail.com");
		// $mail->setSender("COE AGIT");
		// $mail->setSubject("Coba emmail oepncart");
		
		// $mail->setHtml($this->load->view('/default/template/mail/register.tpl'));
		// $mail->send();
		$this->load->language('account/activate');
		$this->load->model('account/activate');
		
		$this->document->setTitle($this->language->get('text_verification_phone'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_activate'),
			'href' => $this->url->link('account/activate')
		);

		

		$data['heading_title'] = $this->language->get('text_verification_phone');
		
		$data['text_verification_phone'] = $this->language->get('text_verification_phone');
		$data['text_registered_number'] = $this->language->get('text_registered_number');
		$data['text_ver_code'] = $this->language->get('text_ver_code');
		$data['text_note1'] = $this->language->get('text_note1');
		$data['text_note2'] = $this->language->get('text_note2');
		$data['text_note3'] = $this->language->get('text_note3');
		$data['text_retry_send'] = $this->language->get('text_retry_send');
		$data['text_progress'] =  $this->language->get('text_progress');
		$data['error_is_verified'] =  $this->language->get('error_is_verified');
		$data['phone']=$this->customer->getTelephone();
		
		$data['button_send_verifivation'] = $this->language->get('button_send_verifivation');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_skip'] = $this->language->get('button_skip');
		
		$data['link_skip'] = $this->url->link('account/account', '', 'SSL');
		$data['text_edit_account'] = sprintf($this->language->get('text_edit_account'),$this->url->link('account/edit', '', 'SSL'));
		$data['action_verify'] = $this->url->link('account/activate/phone', '', 'SSL');
		
		$data['text_modal_success'] =$this->language->get('text_modal_success');
		$data['text_modal_success_message'] =$this->language->get('text_modal_success_message');
		$data['button_ok'] =$this->language->get('button_ok');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		/*
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate($this->request->post['verification_code'])) {
			$this->model_account_activate->setVerified($this->request->post['verification_code']);
			
			// $this->response->redirect($this->url->link('account/activate/phone', '', 'SSL'));
		}
		*/
		
		$data['verified'] = $this->model_account_activate->isVerifiedPhone();
		$verified = $this->model_account_activate->isVerifiedPhone();
		
		if($verified==0){
			$data['text_progress_message']=sprintf($this->language->get('text_message_email'), $this->url->link('account/activate').'&mail=0');
			$data['text_progress_percent']=60;
		}else if($verified==1){
			$data['text_progress_message']=sprintf($this->language->get('text_message_phone'),$this->url->link('account/activate/phone'));
			$data['text_progress_percent']=100;
		}else if($verified==2){
			$data['text_progress_message']=sprintf($this->language->get('text_message_complete'), $this->url->link('common/home'));
			$data['text_progress_percent']=100;	
		}else{
			/*Back to login*/
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		
		// /*validation*/
		if (isset($this->error['is_verified'])) {
			$data['error_is_verified'] = $this->error['is_verified'];
		} else {
			$data['error_is_verified'] = '';
		}
		
		if (isset($this->error['is_empty'])) {
			$data['error_empty'] = $this->error['is_empty'];
		} else {
			$data['error_empty'] = '';
		}
		
		if (isset($this->request->post['verification_code'])) {
			$data['verification_code'] = $this->request->post['verification_code'];
		} else {
			$data['verification_code'] = '';
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/activate_phone.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/activate_phone.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/activate_phone.tpl', $data));
		}
	}
	
	public function validate($code){
		$this->load->language('account/activate');
		$this->load->model('account/activate');
		
		if (!$this->model_account_activate->isVerified($code)) {
			$this->error['is_verified'] = $this->language->get('error_is_verified');
		}
		
		if (empty($code)) {
			$this->error['is_empty'] = $this->language->get('error_empty');
		}
		
		return !$this->error;
	}
	
	public function sendVerificationCode(){
		$this->load->language('account/activate');
		$this->load->model('account/activate');
		
		$json=array();
		if((int)$this->customer->getVerified()==1){
			$message=sprintf($this->language->get('text_message_verification'),$this->customer->getCode());
			$this->zenziva->getServiceZenziva($this->customer->getTelephone(),$message);
			$json['success']=$this->language->get('text_success_send_verification_code');
		}else{
			$json['error']=$this->language->get('error_empty_registered_number');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
