<?php
class ControllerAccountAccount extends Controller {
	public function index() {

		
		/*vqmod change*/
		$data['ms_seller_created'] = $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId());
		$data = array_merge($this->load->language('multiseller/multiseller'), $data);
		/*----------------------*/
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
		
		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['password'] = $this->url->link('account/password', '', 'SSL');
		$data['address'] = $this->url->link('account/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['review'] = $this->url->link('account/review', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['return'] = $this->url->link('account/return', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['recurring'] = $this->url->link('account/recurring', '', 'SSL');
		
		$data['text_activate'] = $this->language->get('text_activate');
		$data['activate'] = $this->url->link('account/activate', '', 'SSL');

		
		$this->load->model('account/order');
		$this->load->model('account/return');
		$this->load->model('account/review');
		
		$data['notification_order'] = $this->model_account_order->getCountNotificationOrder($this->customer->getId());
		$data['notification_review'] = $this->model_account_review->getCountNotificationReview($this->customer->getId());
		$data['notification_reward'] = sprintf($this->language->get('text_reward_point'),(int)$this->customer->getRewardPoints());
		$data['text_name'] = $this->language->get('text_name');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_phone'] = $this->language->get('text_phone');
		$data['text_dob'] = $this->language->get('text_dob');
		$data['text_account_bank'] = $this->language->get('text_account_bank');
		$data['text_account_name'] = $this->language->get('text_account_name');
		$data['text_account_number'] = $this->language->get('text_account_number');
		$data['text_balance'] = $this->language->get('text_balance');
		
		$data['name'] = $this->customer->getFirstName()." ".$this->customer->getLastName();
		$data['email'] = $this->customer->getEmail();
		$data['phone'] = $this->customer->getTelephone();
		$data['dob'] = date_format(date_create($this->customer->getDOB()),"d/m/Y");
		$data['account'] = $this->customer->getAccount();
		$data['account_name'] = $this->customer->getAccountName();
		$data['account_number'] = $this->customer->getAccountNumber();
		$data['balance'] = $this->currency->format($this->balance->getCustomerBalance($this->customer->getId()));
		$data['button_edit'] = $this->language->get('button_edit');
		
		$data['text_list_order'] = $this->language->get('text_list_order');
		$data['text_list_return'] = $this->language->get('text_list_return');
		
		$data['text_column_invoice_order'] = $this->language->get('text_column_invoice_order');
		$data['text_column_status_order'] = $this->language->get('text_column_status_order');
		$data['text_column_action_order'] = $this->language->get('text_column_action_order');
		$data['text_column_invoice_return'] = $this->language->get('text_column_invoice_return');
		$data['text_column_product_return'] = $this->language->get('text_column_product_return');
		$data['text_column_status_return'] = $this->language->get('text_column_status_return');
		$data['text_column_action_return'] = $this->language->get('text_column_action_return');
		$data['text_view_more'] = $this->language->get('text_view_more');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		
		$orders = $this->model_account_order->getOrdersDashboard($this->customer->getId(),5);
		$data['orders']=array();
		foreach($orders as $order){
			$data['orders'][]=array(
				"invoice"=>$order['invoice'],
				"status"=>$order['status'],
				"link"=>$this->url->link('account/order/info', '&order_id='.$order['order_id'].'&order_detail_id='.$order['order_detail_id'], 'SSL')
			);
		}
		
		$returns = $this->model_account_return->getReturnsDashboard($this->customer->getId(),5);
		$data['returns']=array();
		foreach($returns as $return){
			$data['returns'][]=array(
				"invoice"=>$return['invoice'],
				"product"=>$return['product'],
				"status"=>$return['status'],
				"link"=>$this->url->link('account/return/info', '&return_id='.$return['return_id'], 'SSL')
			);
		}
		
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function zone() {
		$json = array();

		$this->load->model('localisation/zone');

		$zone_info = $this->model_localisation_zone->getZone($this->request->get['zone_id']);

		if ($zone_info) {
			$this->load->model('localisation/city');

			$json = array(
				'zone_id'        => $zone_info['zone_id'],
				'name'              => $zone_info['name'],
				'city'              => $this->model_localisation_city->getCitiesByZoneId($this->request->get['zone_id']),
				'status'            => $zone_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function city() {
		$json = array();

		$this->load->model('localisation/city');

		$city_info = $this->model_localisation_city->getCity($this->request->get['city_id']);

		if ($city_info) {
			$this->load->model('localisation/district');

			$json = array(
				'city_id'        => $city_info['city_id'],
				'name'              => $city_info['name'],
				'district'              => $this->model_localisation_district->getDistrictsByCityId($this->request->get['city_id']),
				'status'            => $city_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function district() {
		$json = array();

		$this->load->model('localisation/district');

		$city_info = $this->model_localisation_district->getDistrict($this->request->get['district_id']);

		if ($city_info) {
			$this->load->model('localisation/subdistrict');

			$json = array(
				'district_id'        => $city_info['district_id'],
				'name'              => $city_info['name'],
				'subdistrict'              => $this->model_localisation_subdistrict->getSubdistrictsByDistrictId($this->request->get['district_id']),
				'status'            => $city_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function subdistrict() {
		$json = array();

		$this->load->model('localisation/subdistrict');

		$city_info = $this->model_localisation_subdistrict->getSubdistrict($this->request->get['subdistrict_id']);

		if ($city_info) {

			$json = array(
				'subdistrict_id'        => $city_info['subdistrict_id'],
				'name'              => $city_info['name'],
				'postcode'              => $city_info['code'],
				'status'            => $city_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function pp_branch_city() {
		$json = array();

		$this->load->model('localisation/city');

		$city_info = $this->model_localisation_city->getCity($this->request->get['city_id']);

		if ($city_info) {
			$this->load->model('localisation/pp_branch');

			$json = array(
				'city_id'        => $city_info['city_id'],
				'name'              => $city_info['name'],
				'pp_branch'              => $this->model_localisation_pp_branch->getPpBranchs(array('city_id'=>$this->request->get['city_id'])),
				'status'            => $city_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function pp_branch() {
		$json = array();

		$this->load->model('localisation/pp_branch');

		$branch_info = $this->model_localisation_pp_branch->getPpBranch($this->request->get['pp_branch_id']);

		if ($branch_info) {
			$json = $branch_info;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function countrykiosk() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountryKiosk($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesKioskByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function zonekiosk() {
		$json = array();

		$this->load->model('localisation/zone');

		$zone_info = $this->model_localisation_zone->getZoneKiosk($this->request->get['zone_id']);

		if ($zone_info) {
			$this->load->model('localisation/city');

			$json = array(
				'zone_id'        => $zone_info['zone_id'],
				'name'              => $zone_info['name'],
				'city'              => $this->model_localisation_city->getCitiesKioskByZoneId($this->request->get['zone_id']),
				'status'            => $zone_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}