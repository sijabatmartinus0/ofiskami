<?php

class ControllerAccountRegisterMerchent extends Controller {

    private $error = array();

    public function index() {
        if ($this->pickup->isPickupPoint()) {
            $this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
        }

        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', 'SSL'));
        }

        $this->load->language('account/register_merchent');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->model('account/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $nama = $this->request->post['nama'];
            $badan_usaha = $this->request->post['badan_usaha'];
            $email = $this->request->post['email'];
            $no_telp = $this->request->post['telephone'];
            $kategori_produk = $this->request->post['kategori_produk'];
            $website_toko = $this->request->post['website_toko'];
            $npwp = $this->request->post['npwp'];
            $state = $this->request->post['state'];
            $data = array(
                'nama' => $nama,
                'badan_usaha' => $badan_usaha,
                'email' => $email,
                'no_telp' => $no_telp,
                'kategori_produk' => $kategori_produk,
                'website_toko' => $website_toko,
                'npwp' => $npwp,
                'state' => $state
            );
//            echo '<pre>';
//            var_dump($data);
//            die();
            $this->model_account_customer->addMerchant($data);
//            $this->model_account_customer->addMerchant($this->request->post);
            $this->response->redirect($this->url->link('common/home'));
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/register_merchent', '', 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');

//		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
        $data['text_your_details'] = $this->language->get('text_your_details');
        $data['text_your_address'] = $this->language->get('text_your_address');
        $data['text_personal'] = $this->language->get('text_personal');
        $data['text_corporate'] = $this->language->get('text_corporate');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $data['entry_nama'] = $this->language->get('entry_nama');

        $data['entry_badan_usaha'] = $this->language->get('entry_badan_usaha');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_kategori_produk'] = $this->language->get('entry_kategori_produk');
        $data['entry_website_toko'] = $this->language->get('entry_website_toko');
        $data['entry_npwp'] = $this->language->get('entry_npwp');


        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_upload'] = $this->language->get('button_upload');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['nama'])) {
            $data['error_nama'] = $this->error['nama'];
        } else {
            $data['error_nama'] = '';
        }

        if (isset($this->error['badan_usaha'])) {
            $data['error_badan_usaha'] = $this->error['badan_usaha'];
        } else {
            $data['error_badan_usaha'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['kategori_produk'])) {
            $data['error_kategori_produk'] = $this->error['kategori_produk'];
        } else {
            $data['error_kategori_produk'] = '';
        }

        if (isset($this->error['website_toko'])) {
            $data['error_website_toko'] = $this->error['website_toko'];
        } else {
            $data['error_website_toko'] = '';
        }

        if (isset($this->request->post['nama'])) {
            $data['nama'] = $this->request->post['nama'];
        } else {
            $data['nama'] = '';
        }
        if (isset($this->request->post['badan_usaha'])) {
            $data['badan_usaha'] = $this->request->post['badan_usaha'];
        } else {
            $data['badan_usaha'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } else {
            $data['telephone'] = '';
        }

        if (isset($this->request->post['kategori_produk'])) {
            $data['kategori_produk'] = $this->request->post['kategori_produk'];
        } else {
            $data['kategori_produk'] = '';
        }

        if (isset($this->request->post['website_toko'])) {
            $data['website_toko'] = $this->request->post['website_toko'];
        } else {
            $data['website_toko'] = '';
        }

        if (isset($this->request->post['npwp'])) {
            $data['npwp'] = $this->request->post['npwp'];
        } else {
            $data['npwp'] = '';
        }

        if (isset($this->request->post['state'])) {
            $data['state'] = $this->request->post['state'];
        } else {
            $data['state'] = '';
        }
        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register_merchent.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/register_merchent.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/register_merchent.tpl', $data));
        }
    }

    public function validate() {
        if ((utf8_strlen(trim($this->request->post['nama'])) < 1) || (utf8_strlen(trim($this->request->post['nama'])) > 32)) {
            $this->error['nama'] = $this->language->get('error_nama');
        }

        if ((utf8_strlen(trim($this->request->post['badan_usaha'])) < 1) || (utf8_strlen(trim($this->request->post['badan_usaha'])) > 32)) {
            $this->error['badan_usaha'] = $this->language->get('error_badan_usaha');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if ((utf8_strlen(trim($this->request->post['kategori_produk'])) < 1) || (utf8_strlen(trim($this->request->post['kategori_produk'])) > 32)) {
            $this->error['badan_usaha'] = $this->language->get('error_kategori_produk');
        }
        if ((utf8_strlen(trim($this->request->post['website_toko'])) < 1) || (utf8_strlen(trim($this->request->post['website_toko'])) > 32)) {
            $this->error['website_toko'] = $this->language->get('error_website_toko');
        }

        return !$this->error;
    }

    public function customfield() {
        $json = array();

        $this->load->model('account/custom_field');

        // Customer Group
        if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->get['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required' => $custom_field['required']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
