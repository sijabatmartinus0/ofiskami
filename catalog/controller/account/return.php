<?php
class ControllerAccountReturn extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}

		$this->load->language('account/return');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/return', $url, 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_return_id'] = $this->language->get('column_return_id');
		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_customer'] = $this->language->get('column_customer');

		$data['button_view'] = $this->language->get('button_view');
		$data['button_continue'] = $this->language->get('button_continue');

		$this->load->model('account/return');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['returns'] = array();

		$return_total = $this->model_account_return->getTotalReturns();

		$results = $this->model_account_return->getReturns(($page - 1) * 10, 10);

		foreach ($results as $result) {
			$data['returns'][] = array(
				'return_id'  => $result['return_id'],
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'href'       => $this->url->link('account/return/info', 'return_id=' . $result['return_id'] . $url, 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $return_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_product_limit');
		$pagination->url = $this->url->link('account/return', 'page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($return_total) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($return_total - $this->config->get('config_product_limit'))) ? $return_total : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $return_total, ceil($return_total / $this->config->get('config_product_limit')));

		$data['continue'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/return_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/return_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/return_list.tpl', $data));
		}
	}

	public function info() {
		$this->load->language('account/return');
		
		$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
		
		$data['link_complete_return'] = $this->url->link('account/return/complete_return', '', 'SSL');
		$data['link_confirm_action_changed'] = $this->url->link('account/return/confirm_action', '', 'SSL');
		$data['list_return_comments'] = $this->url->link('account/return/list_return_comments', '', 'SSL');
		$data['link_add_comment'] = $this->url->link('account/return/add_comment', '', 'SSL');
		$data['list_up_comments'] = $this->url->link('account/return/list_up_comments', '', 'SSL');
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->request->get['return_id'])) {
			$return_id = $this->request->get['return_id'];
		} else {
			$return_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return/info', 'return_id=' . $return_id, 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}

		$this->load->model('account/return');
		$this->load->model('tool/image');

		$return_info = $this->model_account_return->getReturn($return_id);
		
		if ($return_info) {
			$this->document->setTitle($this->language->get('text_return'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/return', $url, 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_return'),
				'href' => $this->url->link('account/return/info', 'return_id=' . $this->request->get['return_id'] . $url, 'SSL')
			);

			$data['heading_title'] = $this->language->get('text_return');

			$data['text_return_detail'] = $this->language->get('text_return_detail');
			$data['text_return_id'] = $this->language->get('text_return_id');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_ordered'] = $this->language->get('text_date_ordered');
			$data['text_customer'] = $this->language->get('text_customer');
			$data['text_email'] = $this->language->get('text_email');
			$data['text_telephone'] = $this->language->get('text_telephone');
			$data['text_status'] = $this->language->get('text_status');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_product'] = $this->language->get('text_product');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_invoice'] = $this->language->get('text_invoice');
			$data['text_complete_return'] = $this->language->get('text_complete_return');
			$data['text_confirm'] = $this->language->get('text_confirm');
			$data['text_discuss'] = $this->language->get('text_discuss');
			$data['text_view_more'] = $this->language->get('text_view_more');
			
			$data['warning_action_changed'] = $this->language->get('warning_action_changed');

			$data['column_product'] = $this->language->get('column_product');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_opened'] = $this->language->get('column_opened');
			$data['column_reason'] = $this->language->get('column_reason');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');
			$data['column_invoice'] = $this->language->get('column_invoice');
			$data['column_customer'] = $this->language->get('column_customer');
			$data['column_merchant'] = $this->language->get('column_merchant');
			$data['column_phone'] = $this->language->get('column_phone');

			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_submit'] = $this->language->get('button_submit');
			$data['success_complete_return'] = $this->language->get('success_complete_return');
			$data['error_discuss'] = $this->language->get('error_discuss');
			
			if($return_info['image']){
				$image = $this->model_tool_image->resize($return_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				$data['ori_image'] = $this->model_tool_image->resize($return_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$image = '';
				$data['ori_image'] = '';
			}
			
			if ($return_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($return_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			$data['return_id'] = $return_info['return_id'];
			$data['order_id'] = $return_info['order_id'];
			$data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($return_info['date_ordered']));
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($return_info['date_added']));
			$data['name'] = $return_info['firstname'] . ' ' . $return_info['lastname'];
			$data['email'] = $return_info['email'];
			$data['telephone'] = $return_info['telephone'];
			$data['product'] = $return_info['product'];
			$data['model'] = $return_info['model'];
			$data['quantity'] = $return_info['quantity'];
			$data['reason'] = $return_info['reason'];
			$data['opened'] = $return_info['opened'] ? $this->language->get('text_yes') : $this->language->get('text_no');
			$data['comment'] = nl2br($return_info['comment']);
			$data['action'] = $return_info['action'];
			$data['invoice_no'] = $return_info['invoice_no'];
			$data['return_status_id'] = $return_info['return_status_id'];
			$data['is_action_changed'] = $return_info['is_action_changed'];
			$data['product_id'] = $return_info['product_id'];
			$data['confirm_action_changed'] = $return_info['confirm_action_changed'];
			$data['sla'] = $return_info['sla'];
			$data['thumb'] = $image;
			$data['href'] = $this->url->link('product/product', 'product_id=' . $return_info['product_id']);

			$data['histories'] = array();

			$results = $this->model_account_return->getReturnHistories($this->request->get['return_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date('d/m/Y H:i', strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}

			$data['continue'] = $this->url->link('account/return', $url, 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->load->model('seller/account_return_list');
			$data['return_comments']= $this->model_seller_account_return_list->listUpComments($return_info['return_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/return_info.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/return_info.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/return_info.tpl', $data));
			}
		} else {
			$this->document->setTitle($this->language->get('text_return'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/return', '', 'SSL')
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_return'),
				'href' => $this->url->link('account/return/info', 'return_id=' . $return_id . $url, 'SSL')
			);

			$data['heading_title'] = $this->language->get('text_return');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('account/return', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function add() {
		$this->load->language('account/return');

		$this->load->model('account/return');

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			unset($this->session->data['captcha']);

			$return_id = $this->model_account_return->addReturn($this->request->post);
			$this->model_account_return->sendNotification($return_id, 1);

			// Add to activity log
			$this->load->model('account/activity');

			if ($this->customer->isLogged()) {
				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
					'return_id'   => $return_id
				);

				$this->model_account_activity->addActivity('return_account', $activity_data);
			} else {
				$activity_data = array(
					'name'      => $this->request->post['firstname'] . ' ' . $this->request->post['lastname'],
					'return_id' => $return_id
				);

				$this->model_account_activity->addActivity('return_guest', $activity_data);
			}

			$this->response->redirect($this->url->link('account/return/success', '', 'SSL'));
		}

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);
		

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		//edit by arin
		$data['text_return_action'] = $this->language->get('text_return_action');
		$data['text_upload_return'] = $this->language->get('text_upload_return');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_order'] = $this->language->get('text_order');
		$data['error_return_action'] = $this->language->get('error_return_action');
		$data['warning_file_type'] = $this->language->get('warning_file_type');

		$data['entry_order_id'] = $this->language->get('entry_order_id');
		$data['entry_date_ordered'] = $this->language->get('entry_date_ordered');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_reason'] = $this->language->get('entry_reason');
		$data['entry_opened'] = $this->language->get('entry_opened');
		$data['entry_fault_detail'] = $this->language->get('entry_fault_detail');
		$data['entry_captcha'] = $this->language->get('entry_captcha');
		$data['error_quantity'] = $this->language->get('error_quantity');
		$data['error_quantity_limit'] = $this->language->get('error_quantity_limit');

		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['order_id'])) {
			$data['error_order_id'] = $this->error['order_id'];
		} else {
			$data['error_order_id'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['product'])) {
			$data['error_product'] = $this->error['product'];
		} else {
			$data['error_product'] = '';
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}
		
		if (isset($this->error['quantity'])) {
			$data['error_quantity'] = $this->error['quantity'];
		} else {
			$data['error_quantity'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = '';
		}

		if (isset($this->error['comment'])) {
			$data['error_comment'] = $this->error['comment'];
		} else {
			$data['error_comment'] = '';
		}
		
		if (isset($this->error['return_action'])) {
			$data['error_return_action'] = $this->error['return_action'];
		} else {
			$data['error_return_action'] = '';
		}
		
		if (isset($this->error['picture_return'])) {
			$data['error_picture_return'] = $this->error['picture_return'];
		} else {
			$data['error_picture_return'] = '';
		}
		
		if (isset($this->error['captcha'])) {
			$data['error_captcha'] = $this->error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}

		$data['action'] = $this->url->link('account/return/add', 'order_id=' . $this->request->get['order_id'] . '&product_id=' . $this->request->get['product_id'] . '&order_detail_id=' . $this->request->get['order_detail_id'], 'SSL');

		$this->load->model('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_info = $this->model_account_order->getOrder($this->request->get['order_id']);
		}

		$this->load->model('catalog/product');

		if (isset($this->request->get['product_id'])) {
			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
		}

		if (isset($this->request->get['product_id'])) {
			$data['product_id'] = $this->request->get['product_id'];
		} else {
			$data['product_id'] = '';
		}
		
		if (isset($this->request->get['order_detail_id'])) {
			$data['order_detail_id'] = $this->request->get['order_detail_id'];
		} else {
			$data['order_detail_id'] = '';
		}
		
		if (isset($this->request->post['input_product_id'])) {
			$data['input_product_id'] = $this->request->post['input_product_id'];
		} else {
			$data['input_product_id'] = '';
		}
		
		if (isset($this->request->post['input_order_detail_id'])) {
			$data['input_order_detail_id'] = $this->request->post['input_order_detail_id'];
		} else {
			$data['input_order_detail_id'] = '';
		}
		
		if (isset($this->request->post['order_id'])) {
			$data['order_id'] = $this->request->post['order_id'];
		} elseif (!empty($order_info)) {
			$data['order_id'] = $order_info['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if (isset($this->request->post['date_ordered'])) {
			$data['date_ordered'] = $this->request->post['date_ordered'];
		} elseif (!empty($order_info)) {
			$data['date_ordered'] = date('Y-m-d', strtotime($order_info['date_added']));
		} else {
			$data['date_ordered'] = '';
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($order_info)) {
			$data['firstname'] = $order_info['firstname'];
		} else {
			$data['firstname'] = $this->customer->getFirstName();
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} elseif (!empty($order_info)) {
			$data['lastname'] = $order_info['lastname'];
		} else {
			$data['lastname'] = $this->customer->getLastName();
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($order_info)) {
			$data['email'] = $order_info['email'];
		} else {
			$data['email'] = $this->customer->getEmail();
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($order_info)) {
			$data['telephone'] = $order_info['telephone'];
		} else {
			$data['telephone'] = $this->customer->getTelephone();
		}

		if (isset($this->request->post['product'])) {
			$data['product'] = $this->request->post['product'];
		} elseif (!empty($product_info)) {
			$data['product'] = $product_info['name'];
		} else {
			$data['product'] = '';
		}

		if (isset($this->request->post['model'])) {
			$data['model'] = $this->request->post['model'];
		} elseif (!empty($product_info)) {
			$data['model'] = $product_info['model'];
		} else {
			$data['model'] = '';
		}

		if (isset($this->request->post['quantity'])) {
			$data['quantity'] = $this->request->post['quantity'];
		} else {
			$data['quantity'] = 1;
		}

		if (isset($this->request->post['opened'])) {
			$data['opened'] = $this->request->post['opened'];
		} else {
			$data['opened'] = false;
		}

		if (isset($this->request->post['return_reason_id'])) {
			$data['return_reason_id'] = $this->request->post['return_reason_id'];
		} else {
			$data['return_reason_id'] = '';
		}
		
		if (isset($this->request->post['return_action_id'])) {
			$data['return_action_id'] = $this->request->post['return_action_id'];
		} else {
			$data['return_action_id'] = '';
		}
		
		if (isset($_FILES['picture_return']['tmp_name'])) {
			$data['picture_return'] = $_FILES['picture_return']['tmp_name'];
		} else {
			$data['picture_return'] = '';
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_order'),
			'href' => $this->url->link('account/order/info', 'order_id=' . $data['order_id'] . '&order_detail_id=' . $data['order_detail_id'] . '&product_id=' . $data['product_id'], 'SSL')
		);
		
		$this->load->model('localisation/return_reason');

		$data['return_reasons'] = $this->model_localisation_return_reason->getReturnReasons();
		
		$this->load->model('localisation/return_action');
		
		$data['return_actions'] = $this->model_localisation_return_action->getReturnActions();

		if (isset($this->request->post['comment'])) {
			$data['comment'] = $this->request->post['comment'];
		} else {
			$data['comment'] = '';
		}

		if ($this->config->get('config_return_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_return_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_return_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		$data['back'] = $this->url->link('account/order/info', 'order_id='.$data['order_id'].'&order_detail_id='.$data['order_detail_id'], 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/return_form.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/return_form.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/return_form.tpl', $data));
		}
	}

	public function success() {
		$this->load->language('account/return');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/return', '', 'SSL')
		);
		
		// edit by arin
		if ($this->customer->isLogged()) {
			$data['is_logged_in'] = false;
		} else {
			
			$data['is_logged_in'] = true;
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_message');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	protected function validate() {
		$this->load->model("account/return");
		
		$qty = (int)$this->model_account_return->getQuantityProduct($this->request->get['product_id'], $this->request->get['order_detail_id']);
		
		if (!$this->request->post['order_id']) {
			$this->error['order_id'] = $this->language->get('error_order_id');
		}
		
		if ((int)$this->request->post['quantity'] == 0 || !$this->request->post['quantity']) {
			$this->error['quantity'] = $this->language->get('error_quantity');
		}
		
		if ((int)$this->request->post['quantity'] > $qty) {
			$this->error['quantity'] = $this->language->get('error_quantity_limit');
		}

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen($this->request->post['product']) < 1) || (utf8_strlen($this->request->post['product']) > 255)) {
			$this->error['product'] = $this->language->get('error_product');
		}

		if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
			$this->error['model'] = $this->language->get('error_model');
		}

		if (empty($this->request->post['return_reason_id'])) {
			$this->error['reason'] = $this->language->get('error_reason');
		}
		
		if (!empty($this->request->post['return_reason_id'])) {
			if (((int)$this->request->post['return_reason_id'] == 4 || (int)$this->request->post['return_reason_id'] == 5) && empty($this->request->post['comment'])) {
				$this->error['comment'] = $this->language->get('error_comment');
			}
		}
		
		if (empty($_FILES['picture_return']['tmp_name'])) {
			$this->error['picture_return'] = $this->language->get('error_picture_return');
		}
		
		if (empty($this->request->post['return_action_id'])) {
			$this->error['return_action'] = $this->language->get('error_return_action');
		}

		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$this->error['captcha'] = $this->language->get('error_captcha');
		}

		if ($this->config->get('config_return_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_return_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return !$this->error;
	}
	
	public function complete_return(){
		$this->load->language('account/return');
		$data['success'] = $this->language->get('success_complete_return');
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return/complete_return', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('account/return');
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_account_return->completeReturn($this->request->post['input_return_id']);
			$this->model_account_return->sendNotification($this->request->post['input_return_id'], 3);
			
			$this->session->data['success'] = $this->language->get('success_complete_return');
			$this->response->redirect($this->url->link('account/return/info', 'return_id=' . $this->request->post['input_return_id'] , 'SSL'));
		}
		
		if (isset($this->request->post['input_return_id'])) {
			$data['input_return_id'] = $this->request->post['input_return_id'];
		} else {
			$data['input_return_id'] = '';
		}
	}
	
	public function confirm_action(){
		$this->load->language('account/return');
		$data['success'] = $this->language->get('success_confirm_action');
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return/complete_return', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
		$this->load->model('account/return');
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_account_return->confirmAction($this->request->post['input_return_id_action']);
			$this->session->data['success'] = $this->language->get('success_confirm_action');
			$this->response->redirect($this->url->link('account/return/info', 'return_id=' . $this->request->post['input_return_id_action'] , 'SSL'));
		}
		
		if (isset($this->request->post['input_return_id_action'])) {
			$data['input_return_id_action'] = $this->request->post['input_return_id_action'];
		} else {
			$data['input_return_id_action'] = '';
		}
	}
	
	public function add_comment(){
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/return/info', 'return_id=' . $this->request->get['discuss_return_id'] , 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->load->language('account/return');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		$this->load->model('seller/account_return_list');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate_comment()) {
			$this->model_seller_account_return_list->addReturnComment($this->request->post);
			$this->response->redirect($this->url->link('account/return/info', 'return_id=' . $this->request->post['discuss_return_id'] , 'SSL'));
		}
		
		if (isset($this->error['error_discuss_comment'])) {
			$data['error_discuss_comment'] = $this->error['error_discuss_comment'];
		} else {
			$data['error_discuss_comment'] = '';
		}
		
		if (isset($this->request->post['discuss_return_id'])) {
			$data['discuss_return_id'] = $this->request->post['discuss_return_id'];
		} else {
			$data['discuss_return_id'] = '';
		}
		
		if (isset($this->request->post['discuss_product_id'])) {
			$data['discuss_product_id'] = $this->request->post['discuss_product_id'];
		} else {
			$data['discuss_product_id'] = '';
		}
		
		if (isset($this->request->post['discuss_comment'])) {
			$data['discuss_comment'] = $this->request->post['discuss_comment'];
		} else {
			$data['discuss_comment'] = '';
		}
	}
	
	public function validate_comment(){
		if ((utf8_strlen($this->request->post['discuss_comment']) < 2) ) {
			$this->error['error_discuss_comment'] = $this->language->get('error_discuss');
		}
		
		return !$this->error;
	}
	
	
	public function list_return_comments(){
		$this->load->model('seller/account_return_list');
		
		$this->load->language('account/return');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		$data['return_comments'] = array();
		$return_comments = $this->model_seller_account_return_list->listReturnComments($this->request->get['return_id']);
		
		foreach($return_comments as $return_comment){
			$data['return_comments'][] = array(
				'return_id'  		=> $return_comment['return_id'],
				'seller_id'  		=> $return_comment['seller_id'],
				'product_id'  		=> $return_comment['product_id'],
				'comment'  			=> $return_comment['comment'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($return_comment['created_date'])),
				'flag'  			=> $return_comment['flag'],
				'firstname'  		=> $return_comment['firstname'],
				'nickname'  		=> $return_comment['nickname'],
				'created_by'  		=> $return_comment['created_by']
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/return_comments.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/return_comments.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('/template/account/return_comments.tpl', $data));
		}
		
	}
	
	public function list_up_comments(){
		$this->load->model('seller/account_return_list');
		$data['return_comments'] = array();
		
		$return_comments = $this->model_seller_account_return_list->listUpComments($this->request->get['return_id']);
		
		$this->load->language('account/return');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_merchant'] = $this->language->get('column_merchant');
		$data['text_no_data'] = $this->language->get('text_no_data');
		
		foreach($return_comments as $return_comment){
			$data['return_comments'][] = array(
				'return_id'  		=> $return_comment['return_id'],
				'seller_id'  		=> $return_comment['seller_id'],
				'product_id'  		=> $return_comment['product_id'],
				'comment'  			=> $return_comment['comment'],
				'created_date'  	=> date('d/m/Y H:i', strtotime($return_comment['created_date'])),
				'flag'  			=> $return_comment['flag'],
				'firstname'  		=> $return_comment['firstname'],
				'nickname'  		=> $return_comment['nickname'],
				'created_by'  		=> $return_comment['created_by']
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/return_comments.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/return_comments.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('/template/account/return_comments.tpl', $data));
		}
	}
}
