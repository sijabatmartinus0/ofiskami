<?php
class ControllerAccountLogin extends Controller {
	private $error = array();

	public function index() {
		$this->load->model('account/customer');
		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->event->trigger('pre.customer.login');

			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['wishlist']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->event->trigger('post.customer.login');
				
				if ($this->pickup->isPickupPointUser($this->customer->getId())) {
					$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
				}
				
				if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
					$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
				} else {
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
				}
			}
		}
		
		if ($this->customer->isLogged()) {
		
			if ($this->pickup->isPickupPointUser($this->customer->getId())) {
				$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
			}
			
				if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
					$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
				} else {
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
				}

		}

		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));
		

		  if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

                $nama = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
                $email = $this->request->post['email'];
                $telephone = $this->customer->getTelephone();
				 $id = $this->customer->getId();
                $data_api = [
                    "nama" => $nama,
                    "email" => $email,
                    "telephone" => $telephone,
					"id" => $id,
                ];
                $data = json_encode($data_api);
                $curl = curl_init();

                // Set SSL if required
                if (substr(HTTPS_CATALOG_LOCAL, 0, 5) == 'https') {
                    curl_setopt($curl, CURLOPT_PORT, 443);
                }

                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLINFO_HEADER_OUT, true);
                curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, 'https://www.ofiskita.com/workingspace/session.php');
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, ($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
                );

                $json = curl_exec($curl);

                if (curl_errno($curl)) {
                    print curl_error($curl);
                }

                if (!$json) {
                    $this->error['warning'] = sprintf($this->language->get('error_curl'), curl_error($curl), curl_errno($curl));
                } else {
                    $response = json_decode($json, true);
                    curl_close($curl);
                }
            }

	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

                $nama = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
                $email = $this->request->post['email'];
                $telephone = $this->customer->getTelephone();
				 $id = $this->customer->getId();
                $data_api = [
                    "nama" => $nama,
                    "email" => $email,
                    "telephone" => $telephone,
					"id" => $id,
                ];
                $data = json_encode($data_api);
                $curl = curl_init();

                // Set SSL if required
                if (substr(HTTPS_CATALOG_LOCAL, 0, 5) == 'https') {
                    curl_setopt($curl, CURLOPT_PORT, 443);
                }

                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLINFO_HEADER_OUT, true);
                curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, 'https://www.ofiskita.com/ceria/session.php');
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, ($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
                );

                $json = curl_exec($curl);

                if (curl_errno($curl)) {
                    print curl_error($curl);
                }

                if (!$json) {
                    $this->error['warning'] = sprintf($this->language->get('error_curl'), curl_error($curl), curl_errno($curl));
                } else {
                    $response = json_decode($json, true);
                    curl_close($curl);
                }
            }
        	if (!empty($this->request->get['jenis'])) {

			if ($response['message'] == "sampe") {

		                $this->response->redirect('http://www.ofiskita.com/workingspace/index.php?id='.$id);
            		}
        	}
		if (!empty($this->request->get['ceria'])) {
	        	if ($response['message'] == "sampe") {
		        	$this->response->redirect('http://www.ofiskita.com/ceria/index.php?id='.$id);
            		}
        	}		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);

			if ($this->pickup->isPickupPointUser($this->customer->getId())) {
            			    if (isset($this->session->data['url'])) {
                    			$this->response->redirect($this->url->link(substr($this->session->data['url'], 40), '', 'SSL'));
              			  } else {
                   			 $this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
        			        }
          			  }

				
			if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
          		      		if (isset($this->session->data['url'])) {
                		          $this->response->redirect($this->url->link(substr($this->session->data['url'], 40), '', 'SSL'));
              				  } else {
          			          $this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
             					 }
           									 } else {
               				 if (isset($this->session->data['url'])) {
                   			 //$this->response->redirect($this->url->link(substr($this->session->data['url'], 40), '', 'SSL'));
					 $this->response->redirect($this->session->data['url']);
       				         } else {
                    $this->response->redirect($this->url->link('common/home', '', 'SSL'));
                }
            }
			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect'])){// && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			}else if(isset($this->session->data['redirect'])){
				$this->response->redirect(str_replace('&amp;', '&', $this->session->data['redirect']));
				unset($this->session->data['redirect']);
			} else {
				
				if((int)$this->customer->getVerified()==0){
					$this->response->redirect($this->url->link('account/activate', '', 'SSL'));
				}
				
				$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_new_customer'] = $this->language->get('text_new_customer');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_register_account'] = $this->language->get('text_register_account');
		$data['text_returning_customer'] = $this->language->get('text_returning_customer');
		$data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
		$data['text_forgotten'] = $this->language->get('text_forgotten');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_password'] = $this->language->get('entry_password');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_login'] = $this->language->get('button_login');

		$data['facebook']=FACEBOOK_REDIRECT_URI;
		$data['google'] = GOOGLE_REDIRECT_URI;
		$data['twitter'] = TWITTER_REDIRECT_URI;
		
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		//$data['action'] = $this->url->link('account/login', '', 'SSL');
		if (!empty($this->request->get['jenis'])) {
         	   $data['action'] = $this->url->link('account/login&jenis=wp', '', 'SSL');
        	} elseif (!empty($this->request->get['ceria'])) {
            		$data['action'] = $this->url->link('account/login&ceria=wp', '', 'SSL');
        	}else {
        	    	$data['action'] = $this->url->link('account/login', '', 'SSL');
        	}
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');

		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
			$data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
			$data['redirect'] = $this->session->data['redirect'];

			unset($this->session->data['redirect']);
		} else {
			$data['redirect'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/login.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/login.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/login.tpl', $data));
		}
	}

	protected function validate() {
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);
				
		if ($login_info && ($login_info['total'] > $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}
		
		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}
		
		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');
			
				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			}			
		}
		
		return !$this->error;
	}
	
	public function twitter(){
	$this->load->language('account/login');
		if(isset($this->session->data['twitter'])){
			
			if ($this->customer->isLogged()) {
		
				if ($this->pickup->isPickupPointUser($this->customer->getId())) {
					$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
				}
				
				if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
					$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
				} else {
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
				}
			}
			
			$this->load->model('account/customer');
			
			if(isset($this->session->data['twitter']['email'])){
				$femail=$this->session->data['twitter']['email'];
			}else{
				$femail='';
			}
			
			if(isset($this->session->data['twitter']['id'])){
				$fid=$this->session->data['twitter']['id'];
			}else{
				$fid='';
			}
			
			$customer=$this->model_account_customer->getCustomerByTwitterId($fid);
			
			if(!empty($customer)){
				if((int)$customer['login_type']==4 && $customer['twitter_id']==$fid){
					$this->customer->login($customer['email'], '', true);
					
					unset($this->session->data['guest']);

					// Default Shipping Address
					$this->load->model('account/address');

					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
					);

					$this->model_account_activity->addActivity('login', $activity_data);

					if((int)$this->customer->getVerified()==0){
						$this->response->redirect($this->url->link('account/activate', '', 'SSL'));
					}
					
					if (isset($this->request->post['redirect'])){
						$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
					} else {
						$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					}
					
					$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					
				}else{
					$this->error['warning'] = $this->language->get('error_used_email');
					$this->response->redirect($this->url->link('account/login', '', 'SSL'));
				}
			}else{
				unset($this->session->data['facebook']);
				
				unset($this->session->data['google']);
				
				$this->response->redirect($this->url->link('account/register-account-twitter', '', 'SSL'));
			}
		}else{
			$this->error['warning'] = $this->language->get('error_api');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
	}
	
	public function facebook(){
	$this->load->language('account/login');
		if(isset($this->session->data['facebook'])){
			
			if ($this->customer->isLogged()) {
		
				if ($this->pickup->isPickupPointUser($this->customer->getId())) {
					$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
				}
				
				if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
					$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
				} else {
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
				}
			}
			
			$this->load->model('account/customer');
			
			if(isset($this->session->data['facebook']['email'])){
				$femail=$this->session->data['facebook']['email'];
			}else{
				$femail='';
			}
			
			if(isset($this->session->data['facebook']['id'])){
				$fid=$this->session->data['facebook']['id'];
			}else{
				$fid='';
			}
			
			$customer=$this->model_account_customer->getCustomerByFbId($fid);
			
			if(!empty($customer)){
				if((int)$customer['login_type']==2 && $customer['facebook_id']==$fid){
					$this->customer->login($customer['email'], '', true);
					
					unset($this->session->data['guest']);

					// Default Shipping Address
					$this->load->model('account/address');

					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
					);

					$this->model_account_activity->addActivity('login', $activity_data);

					if((int)$this->customer->getVerified()==0){
						$this->response->redirect($this->url->link('account/activate', '', 'SSL'));
					}
					
					if (isset($this->request->post['redirect'])){
						$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
					} else {
						$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					}
					
					$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					
				}else{
					$this->error['warning'] = $this->language->get('error_used_email');
					$this->response->redirect($this->url->link('account/login', '', 'SSL'));
				}
			}else{
				unset($this->session->data['twitter']);
				
				unset($this->session->data['google']);
				
				$this->response->redirect($this->url->link('account/register-account-facebook', '', 'SSL'));
			}
		}else{
			$this->error['warning'] = $this->language->get('error_api');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
	}
	
	public function google(){
	$this->load->language('account/login');
		if(isset($this->session->data['google'])){
			
			if ($this->customer->isLogged()) {
		
				if ($this->pickup->isPickupPointUser($this->customer->getId())) {
					$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
				}
				
				if ($this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
					$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
				} else {
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
				}
			}
			
			$this->load->model('account/customer');
			
			if(isset($this->session->data['google']['email'])){
				$femail=$this->session->data['google']['email'];
			}else{
				$femail='';
			}
			
			if(isset($this->session->data['google']['id'])){
				$fid=$this->session->data['google']['id'];
			}else{
				$fid='';
			}
			
			$customer=$this->model_account_customer->getCustomerByEmail($femail);
			
			if(!empty($customer)){
				if((int)$customer['login_type']==3 && $customer['google_id']==$fid){
					$this->customer->login($femail, '', true);
					
					unset($this->session->data['guest']);

					// Default Shipping Address
					$this->load->model('account/address');

					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
					);

					$this->model_account_activity->addActivity('login', $activity_data);

					if((int)$this->customer->getVerified()==0){
						$this->response->redirect($this->url->link('account/activate', '', 'SSL'));
					}
					
					if (isset($this->request->post['redirect'])){
						$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
					} else {
						$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					}
					
					$this->response->redirect($this->url->link('common/home', '', 'SSL'));
					
				}else{
					$this->error['warning'] = $this->language->get('error_used_email');
					$this->response->redirect($this->url->link('account/login', '', 'SSL'));
				}
			}else{
				unset($this->session->data['twitter']);
				
				unset($this->session->data['facebook']);
				
				$this->response->redirect($this->url->link('account/register-account-google', '', 'SSL'));
			}
		}else{
			$this->error['warning'] = $this->language->get('error_api');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
	}
}
