<?php

class ControllerAccountWithdrawal extends Controller {
	private $error = array();

	public function index() {
		$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		
		$this->load->language('account/withdrawal');
                $this->load->language('account/account');
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		if ($this->pickup->isPickupPoint()) {
			$this->response->redirect($this->url->link('pickup/account-dashboard', '', 'SSL'));
		}
		
		if ($this->MsLoader->MsSeller->isSeller()) {
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->payment->createPayment(array(
					'user_id' => $this->customer->getId(),
					'customer_id' => $this->customer->getId(),
					'payment_type' => Payment::TYPE_PAYOUT_REQUEST,
					'payment_status' => Payment::STATUS_UNPAID,
					'payment_method' => Payment::METHOD_TRANSFER,
					'payment_data' => $this->customer->getFirstName()." ".$this->customer->getLastName()."(".$this->customer->getAccount()."-".$this->customer->getAccountNumber().")",
					'amount' => $this->request->post['withdraw_amount'],
					'currency_id' => $this->currency->getId($this->config->get('config_currency')),
					'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
					'description' => sprintf($this->language->get('text_description'), $this->language->get('text_method_transfer'), $this->customer->getFirstName()." ".$this->customer->getLastName()."(".$this->customer->getAccount()."-".$this->customer->getAccountNumber().")", date("d/m/Y")),
				)
			);
					$description=sprintf($this->language->get('text_description'), $this->language->get('text_method_transfer'), $this->customer->getFirstName()." ".$this->customer->getLastName()."(".$this->customer->getAccount()."-".$this->customer->getAccountNumber().")", date("d/m/Y"));
					// Send Email to Customer
					$this->load->language('mail/customer_withdrawal');
					$email=array();
					$email['text_customer'] = $this->language->get('text_customer');
					$email['text_withdrawal'] = $this->language->get('text_withdrawal');
					$email['text_amount'] = $this->language->get('text_amount');
					$email['text_date_added'] = $this->language->get('text_date_added');
					$email['text_description'] = $this->language->get('text_description');
					$email['text_withdrawal_link'] = $this->language->get('text_withdrawal_link');
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status');
					$email['text_withdrawal_request'] = $this->language->get('text_withdrawal_request');
					$email['text_footer'] = $this->language->get('text_footer');
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

					$email['store_url']=HTTP_SERVER;
					$email['title']=$subject;
					$email['store_name']=$this->config->get('config_name');
					$email['logo']=HTTP_SERVER . 'image/' . $this->config->get('config_logo');
					$email['withdrawal_link']=HTTP_SERVER.'index.php?route=account/transaction';
					$email['customer']=$this->customer->getFirstName()." ".$this->customer->getLastName();
					$email['withdrawal']=$this->language->get('text_withdrawal_request');
					$email['date_added']=date('d/m/Y');
					$email['amount']=$this->currency->format($this->request->post['withdraw_amount']);
					$email['description']=$description;
					
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/customer_withdrawal.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/customer_withdrawal.tpl', $email);
					} else {
						$html = $this->load->view('default/template/mail/customer_withdrawal.tpl', $email);
					}
	
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($this->customer->getEmail());
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
					// Send Email to Admin
					
					$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$email['text_withdrawal_status'] = $this->language->get('text_withdrawal_status_admin');
					
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/customer_withdrawal_admin.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/customer_withdrawal_admin.tpl', $email);
					} else {
						$html = $this->load->view('default/template/mail/customer_withdrawal_admin.tpl', $email);
					}
					
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->customer->getEmail());
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml($html);
					$mail->send();
			
			$this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('withdrawal', $activity_data);
		}
		
		$this->load->language('account/withdrawal');
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->error['withdraw_amount'])) {
			$data['error_withdraw_amount'] = $this->error['withdraw_amount'];
		} else {
			$data['error_withdraw_amount'] = '';
		}
		
		if (isset($this->request->post['withdraw_amount'])) {
			$data['withdraw_amount'] = $this->request->post['withdraw_amount'];
		}  else {
			$data['withdraw_amount'] = '';
		}
		
		$data['text_heading'] = $this->language->get('text_heading');
                $data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_deposit_fee'] = $this->language->get('text_deposit_fee');
		$data['text_balance'] = $this->language->get('text_balance');
		$data['text_balance_available'] = $this->language->get('text_balance_available');
		$data['text_minimum'] = $this->language->get('text_minimum');
		$data['text_balance_reserved_formatted'] = $this->language->get('text_balance_reserved_formatted');
		$data['text_balance_waiting_formatted'] = $this->language->get('text_balance_waiting_formatted');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['text_amount_note'] = $this->language->get('text_amount_note');
		$data['text_method'] = $this->language->get('text_method');
		$data['text_method_note'] = $this->language->get('text_method_note');
		$data['text_method_paypal'] = $this->language->get('text_method_paypal');
		$data['text_method_transfer'] = $this->language->get('text_method_transfer');
		$data['text_all'] = $this->language->get('text_all');
		$data['text_minimum_not_reached'] = $this->language->get('text_minimum_not_reached');
		$data['text_no_funds'] = $this->language->get('text_no_funds');
		$data['text_no_paypal'] = $this->language->get('text_no_paypal');
		$data['text_empty_account'] = sprintf($this->language->get('text_empty_account'),$this->url->link('account/edit', '', 'SSL'));
		
		if(!empty($this->customer->getAccountId()) && $this->customer->getAccountId()!='' && !empty($this->customer->getAccountName()) && $this->customer->getAccountName()!='' && !empty($this->customer->getAccountNumber()) && $this->customer->getAccountNumber()!=''){
			$data['no_account'] =false;
		}else{
			$data['no_account'] =true;
		}
		
		
		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_back'] = $this->language->get('button_back');
		
		$customer_id = $this->customer->getId();
		
		$customer_balance = $this->balance->getCustomerBalance($customer_id);
		$pending_funds = $this->balance->getReservedCustomerFunds($customer_id);
		$waiting_funds = $this->balance->getWaitingCustomerFunds($customer_id);
		$available_balance = $this->balance->getAvailableCustomerFunds($customer_id);
		
		
		$balance_formatted = $this->currency->format($customer_balance,$this->config->get('config_currency'));
		$balance_reserved_formatted = $pending_funds > 0 ? sprintf($this->language->get('text_balance_reserved_formatted'), $this->currency->format($pending_funds)) . ', ' : '';
		$balance_reserved_formatted .= $waiting_funds > 0 ? sprintf($this->language->get('text_balance_waiting_formatted'), $this->currency->format($waiting_funds)) . ', ' : ''; 
		$balance_reserved_formatted = ($balance_reserved_formatted == '' ? '' : '(' . substr($balance_reserved_formatted, 0, -2) . ')'); 
		
		$data['text_balance_formatted'] = $balance_formatted;
		$data['text_reserved_formatted'] = $balance_reserved_formatted;
		
		$data['balance'] = $customer_balance;
		$data['balance_formatted'] = $this->currency->format($this->data['balance'],$this->config->get('config_currency'));
		
		$data['balance_available'] = $available_balance;
		$data['balance_available_formatted'] = $this->currency->format($available_balance, $this->config->get('config_currency'));
		
		$data['msconf_allow_partial_withdrawal'] = $this->config->get('msconf_allow_partial_withdrawal');
		$data['currency_code'] = $this->config->get('config_currency');
		
		if ($available_balance - $this->config->get('msconf_minimum_withdrawal_amount') >= 0) {
			$data['withdrawal_minimum_reached'] = TRUE;
		} else {
			$data['withdrawal_minimum_reached'] = FALSE;
		}
		
		

		$data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_breadcrumbs'),
			'href' => $this->url->link('account/withdrawal', '', 'SSL')
		);
		
		$data['action'] = $this->url->link('account/withdrawal', '', 'SSL');
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/withdrawal.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/withdrawal.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/withdrawal.tpl', $data));
		}
	}
	
	protected function validate() {
		
		$balance = $this->balance->getAvailableCustomerFunds($this->customer->getId());

		$json = array();
		
		if (!isset($this->request->post['withdraw_amount'])) {
			$this->request->post['withdraw_amount'] = $balance;
		}
		
		if (preg_match("/[^0-9.]/",$this->request->post['withdraw_amount']) || (float)$this->request->post['withdraw_amount'] <= 0) {
			$this->error['withdraw_amount'] = $this->language->get('ms_error_withdraw_amount');
		} else {
			$this->request->post['withdraw_amount'] = (float)$this->request->post['withdraw_amount'];
			if ($this->request->post['withdraw_amount'] > $balance) {
				$this->error['withdraw_amount'] = $this->language->get('ms_error_withdraw_balance');
			} else if ($this->request->post['withdraw_amount'] < $this->config->get('msconf_minimum_withdrawal_amount')) {
				$this->error['withdraw_amount'] = $this->language->get('ms_error_withdraw_minimum');
			}
		}
		
		return !$this->error;
	}
}

?>
