$(function() {
	$(document).delegate("#tab-comments .links a", 'click', function() {
		var page = $(this).attr('href').match(/\d*$/);
		$('#tab-comments .pcComments').load($(this).attr('href'));
		return false;
	});

	$(document).delegate('.pcViewBtn:not(.disabled)', 'click', function() {
		var parent_id=$(this).siblings('input').val();
		var jqSelect=$(this).parent();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=module/productcomments/viewComment&product_id='+pc_product_id+'&parent_id='+parent_id,
			beforeSend: function() {
				jqSelect.siblings('.success,.warning').remove();
				$(this).addClass('disabled');
				jqSelect.siblings('.comment-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> ' + pc_wait + '</div>');
			},		
			complete: function() {
				$(this).removeClass('disabled');
				jqSelect.siblings('.attention').remove();
			},
			success: function(jsonData) {
				if (!$.isEmptyObject(jsonData.errors)) {
					var errors = '';
					jQuery.each(jsonData.errors, function(index, item) {
					    errors += '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + item + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
					});
					jqSelect.siblings('.comment-title').after('<div class="warning"><ul>' + errors + '</ul></div>');
				} else {
					var html='';
					jQuery.each(jsonData.comments, function(index, comment) {
					    html+='<div class="content">';
						html+='<div class="comment-header">';
						html+='<span class="comment-name">';
						html+='<h4>';
						html+=comment.name;
						if(comment.customer_id*1==comment.parent_id*1){ html+='<span class="badge green">'+ pc_customer +'</span>';} else { html+='<span class="badge">'+ pc_seller +'</span>';}
						html+='<small class="muted"><i>'+ comment.create_time +'</i></small>';
						html+='</h4>';
						html+='</span>';
						html+='</div>';
						html+='<div class="comment-content">';
						html+=comment.comment;
						html+='</div>';
						html+='</div>';
						html+='<hr>';
					});
					html+='</br>';
					jqSelect.empty().html(html);
					}
				}
			});
	});
	
	$('#pcText[maxlength]').keyup(function(){
		var limit = parseInt($(this).attr('maxlength'));
		if($(this).val().length > limit){
			$(this).val($(this).val().substr(0, limit));
		}
	});
});
