$(function() {
	$(document).delegate('.pcViewBtn:not(.disabled)', 'click', function() {
		var parent_id=$(this).siblings('input').val();
		var jqSelect=$(this).parent();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=seller/account-product/viewComment&product_id='+pc_product_id+'&parent_id='+parent_id,
			beforeSend: function() {
				jqSelect.siblings('.success,.warning').remove();
				$(this).addClass('disabled');
				jqSelect.siblings('.comment-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> ' + pc_wait + '</div>');
			},		
			complete: function() {
				$(this).removeClass('disabled');
				jqSelect.siblings('.attention').remove();
			},
			success: function(jsonData) {
				if (!$.isEmptyObject(jsonData.errors)) {
					var errors = '';
					jQuery.each(jsonData.errors, function(index, item) {
					    errors += '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + item + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
					});
					jqSelect.siblings('.comment-title').after('<div class="warning"><ul>' + errors + '</ul></div>');
				} else {
					var html='';
					jQuery.each(jsonData.comments, function(index, comment) {
					    html+='<div class="content">';
						html+='<div class="comment-header">';
						html+='<span class="comment-name">';
						html+='<h4>';
						html+=comment.name;
						if(comment.customer_id*1==comment.parent_id*1){ html+='<span class="badge green">'+ pc_customer +'</span>';} else { html+='<span class="badge">'+ pc_seller +'</span>';}
						html+='<small class="muted"><i>'+ comment.create_time +'</i></small>';
						if(comment.flag*1==1) { html+='<span style="color:#ff0000;margin-left:3px;font-size:10px">New</span>';};
						html+='</h4>';
						html+='</span>';
						html+='</div>';
						html+='<div class="comment-content">';
						html+=comment.comment;
						html+='</div>';
						html+='</div>';
						html+='<hr>';
					});
					html+='</br>';
					jqSelect.empty().html(html);
					}
				}
			});
	});
	$(document).delegate('.pcSubmitBtn:not(.disabled)', 'click', function() {
		var jqSelect=$(this).parent();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=seller/account-product/submitComment&product_id='+pc_product_id,
			data: jqSelect.siblings(".comment-form").find(".pcForm").serialize(),
			beforeSend: function() {
				jqSelect.siblings('.success,.warning').remove();
				$(this).addClass('disabled');
				jqSelect.siblings('.comment-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> ' + pc_wait + '</div>');
			},		
			complete: function() {
				$(this).removeClass('disabled');
				jqSelect.siblings('.attention').remove();
			},
			success: function(jsonData) {
				if (!$.isEmptyObject(jsonData.errors)) {
					var errors = '';
					jQuery.each(jsonData.errors, function(index, item) {
					    errors += '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + item + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
					});
					jqSelect.siblings('.comment-title').after('<div class="warning"><ul>' + errors + '</ul></div>');
				} else {
					jqSelect.siblings('.comment-title').after('<div class="alert alert-success success"><i class="fa fa-check-circle"></i> Success:' + jsonData.success + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					$(".pcText").val('');
					var html='',parent_id=0;
					jQuery.each(jsonData.comments.comments, function(index, comment) {
					    html+='<div class="content">';
						html+='<div class="comment-header">';
						html+='<span class="comment-name">';
						html+='<h4>';
						html+=comment.name;
						if(comment.customer_id*1==comment.parent_id*1){ html+='<span class="badge green">'+ pc_customer +'</span>';} else { html+='<span class="badge">'+ pc_seller +'</span>';}
						html+='<small class="muted"><i>'+ comment.create_time +'</i></small>';
						if(comment.flag*1==1) { html+='<span style="color:#ff0000;margin-left:3px;font-size:10px">New</span>';};
						html+='</h4>';
						html+='</span>';
						html+='</div>';
						html+='<div class="comment-content">';
						html+=comment.comment;
						html+='</div>';
						html+='</div>';
						html+='<hr>';
						parent_id=parseInt(comment.parent_id);
					});
					html+='</br>';
					if(jsonData.comments.total*1>2) { html+='<input type="hidden" name="pcIdView" value="'+ parent_id +'"><a class="pcViewBtn icon-only"><span>'+ pc_text_view +'</span><i style="margin-left: 5px; font-size: 16px" data-icon="&#xe1b0"></i></a>';};
					html+='</br>';
					jqSelect.siblings('.content-comments').empty().html(html);
					}
				}
			});	
	});
	
	$('.pcText[maxlength]').keyup(function(){
		var limit = parseInt($(this).attr('maxlength'));
		if($(this).val().length > limit){
			$(this).val($(this).val().substr(0, limit));
		}
	});
});
