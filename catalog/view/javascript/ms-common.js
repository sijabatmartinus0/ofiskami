function ms_addToCart(product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;

	$.ajax({
                url: 'index.php?route=checkout/cart/checkOption',
				type: 'post',
				data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
				dataType: 'json',
                beforeSend: function() {
					$('.alert, .text-danger').remove();
					$('.form-group').removeClass('has-error');
					$('#button-cart').button('loading');
                },
                complete: function() {
					$('#button-cart').button('reset');
                },
                success: function(json) {
					if (json['error']) {
					
					}else if(json['success']){
						$.ajax({
							url: 'index.php?route=checkout/cart/loadCart&amount='+quantity+'&product_id='+product_id,
							type: 'post',
							dataType: "html",
							beforeSend: function() {
								$('#button-cart').button('loading');
							},
							complete: function() {
								$('#button-cart').button('reset');
							},
							success: function(json) {
								$('#modal-cart-content').empty().html(json);
								$('#cart-modal').modal('show');
							}
						});
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
}
