function submitdata(){
	if (typeof	( $("#sgopayment").find('span.o-payment.o-payment-checked').find('input[type=radio]').val()) === 'undefined'){
		alert("Please Select Payment Method");
		$('#checkout-confirm-button').button('reset');
		$('#content .quickcheckout-content').html('');
	}else{
		
		var pos = $("#sgopayment").find('span.o-payment.o-payment-checked').find('input[type=radio]').val(); 
		var posLength = pos.length;
		var n = pos.indexOf(":");
		var bankCode = pos.substr(0,n);
		var productCode = pos.substr(n+1,posLength);
		
	 	 var data = {
							key : $('#sgopaymentid').val(),
							paymentId : $('#cartid').val(),
							paymentAmount : $('#paymentamount').val(),
							backUrl :$('#back_url').val(),
							bankCode : bankCode,
							bankProduct: productCode
						},
						sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
					if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
					SGOSignature.receiveForm();
					
				}
}