<!-- custom  nanang -->
<script>
  $(document).ready(function(){
      $( "#newsletter" ).click(function() {
        $.ajax({
          url: 'index.php?route=module/newsletter/validate',
          type: 'post',
          data: $('#email'),
          dataType: 'json',
          beforeSend: function() {
            $('#newsletter').prop('disabled', true);
            $('#newsletter').after('<i class="fa fa-spinner"></i>');
          },  
          complete: function() {
            $('#newsletter').prop('disabled', false);
            $('.fa-spinner').remove();
          },    
          success: function(json) {
            if (json['error']) {
              alert(json['error']['warning']);
            } else {
              alert(json['success']);
              
              $('#email').val('');
            }
          }
        }); 
      });

      $('#email').on('keydown', function(e) {
        if (e.keyCode == 13) {
          $('#button-newsletter').trigger('click');
        }
      });
  });
</script>

<div class="ofisindi_footer">
  <!-- <div class="clearfix footer_margin"></div> -->
  <?php echo $footer_modules ?>

  <div id="footer">
  	<div class="column">
       <div class="of-box-heading heading">NEWSLETTER</div> <!-- INDIGITAL CUSTOM DESIGN NOT WORK -->
         <div class="box newsletter">
            <div class="box-content">
            <p class="contrast_font intro">Get exclusive deals you will not find anywhere else straight to your inbox!</p>
	           <input type="text" value="" placeholder="Enter your mail Address" name="email" id="email" class="form-control">
                 <a class="button btn-red" id="newsletter"><span>Subscribe / Unsubscribe</span></a>
            </div>
          </div>

        <!-- <div class="of-box-heading heading"><?php //echo $text_information; ?></div> -->
        <!--  <ul class="footer_list">  INDIGITAL CUSTOM -->
        <!-- <?php //if ($informations) { ?> -->
        <!-- <?php //foreach ($informations as $information) { ?> -->
        <!-- <li><i class="fa fa-caret-right"></i><a href="<?php //echo $information['href']; ?>"><?php //echo $information['title']; ?></a></li> -->
        <!-- <?php //} ?> -->
        <!-- <?php //} ?> -->
        <!-- <li><i class="fa fa-caret-right"></i><a href="<?php //echo $contact; ?>"><?php //echo $text_contact; ?></a></li> -->
        <!-- </ul> -->
    	</div><!--
    -->

    <div class="column">
    	<div class="of-box-heading heading">
      		<?php //echo $cosyone_footer_custom_block_title; ?>
      		OFISKITA
     	</div>
     	<div class="custom_block">
      		<?php //echo $cosyone_footer_custom_block; ?>
      		<ul class="footer_list">
      			<li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=4">Tentang Kami</a></li>
      			<li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=3">Kebijakan Privasi</a></li>
      			<li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=5">Syarat & Ketentuan</a></li>
      		</ul>
      	</div>
    </div><!--
    -->

    <div class="column">
        <div class="of-box-heading heading">BELANJA</div>
        <ul class="footer_list">
	        <li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=10">Panduan Belanja</a></li>
	        <li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=8">Status Pemesanan</a></li>
	        <li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=6">Info Pengiriman</a></li>
	        <li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=7">Info Pengembalian</a></li>
	        <li><i class="fa fa-caret-right"></i><a href="#">Hubungi Kami</a></li>
	        <li><i class="fa fa-caret-right"></i><a href="#">Bantuan Pelanggan</a></li>
	    </ul>
    </div>

    <!-- <div class="column">
        <div class="of-box-heading heading">AVAILABLE IN</div> --> <!-- INDIGITAL CUSTOM STATIC -->
         <!-- <div class="custom_block">
           <a href="#">
             <img src="image/apps/playstore.png" alt="ofiskita android app" class="available-app">
           </a>
           <a href="#">
             <img src="image/apps/AppStore.png" alt="ofiskita ios app" class="available-app">
           </a>
         </div> -->
      <!-- <div class="of-box-heading heading">  <?php //echo $text_extra; ?></div> -->
      <!-- <ul class="footer_list">
        <li><i class="fa fa-caret-right"></i><a href="<?php //echo $manufacturer; ?>"><?php //echo $text_manufacturer; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php //echo $voucher; ?>"><?php //echo $text_voucher; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php //echo $affiliate; ?>"><?php //echo $text_affiliate; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php //echo $special; ?>"><?php //echo $text_special; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php //echo $sitemap; ?>"><?php //echo $text_sitemap; ?></a></li>
      </ul> -->
    <!-- </div> -->

    <!---->
    <div class="column">
      <div class="of-box-heading heading"><?php //echo $text_account; ?>MERCHANT</div>
      <ul class="footer_list">
        <!-- <li><i class="fa fa-caret-right"></i><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li> -->
        <li><i class="fa fa-caret-right"></i><a href="http://www.ofiskita.com/index.php?route=information/information&information_id=9">Berjualan di OFISKITA</a></li>
      </ul>
    </div>
  </div> <!-- #footer ends --> 

   <div class="bottom_line"> <a class="scroll_top icon tablet_hide"><i class="fa fa-angle-up"></i></a>
    <div id="powered"><?php echo $powered; ?></div>
     <div id="footer_payment_icon">
      <img src="image/payment/bca.png " alt="" class="img-footer"/>  
      <img src="image/payment/digipay.png" alt="" class="img-footer"/>
      <img src="image/payment/mandiri.png" alt="" class="img-footer"/>
      <img src="image/payment/permata.png" alt="" class="img-footer"/>
      </div>
     <div class="clearfix"></div>
    </div>
  <!--
  OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
  Please donate via PayPal to donate@opencart.com
  //-->
  </div>  <!-- .container ends -->
</div>
</div>  <!-- .outer_container ends -->
<script type="text/javascript" src="catalog/view/theme/ofisindi_hut/js/jquery.cookie.js"></script>

<script type="text/javascript" src="catalog/view/theme/ofisindi_hut/js/colorbox/jquery.colorbox-min.js"></script>
<link href="catalog/view/theme/ofisindi_hut/js/colorbox/custom_colorbox.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="catalog/view/theme/ofisindi_hut/js/quickview.js"></script>
<?php if($cosyone_use_retina) { ?>
<script type="text/javascript" src="catalog/view/theme/ofisindi_hut/js/retina.min.js"></script>
<?php } ?>
<?php echo $live_search; ?>
<?php echo $cosyone_cookie; ?>
</body></html>
