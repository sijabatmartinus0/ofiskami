<style>
.shortcuts_wrapper{
  margin-top: 4px;
}
.mobile_menu_trigger.up_to_tablet {
    padding-top: 13px;
}
.logmod {
  display: block;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 1;
  z-index: 1;
  margin-bottom:10px;
}
.logmod::after {
  clear: both;
  content: "";
  display: table;
}
.logmod__wrapper {
  display: block;
  background: #FFF;
  position: relative;
  max-width: 400px;
  margin: 0px auto;
}
.logmod__close {
  display: block;
  position: absolute;
  right: 50%;
  background: url("http://imgh.us/close_white.svg") no-repeat scroll 0% 0% transparent;
  text-indent: 100%;
  white-space: nowrap;
  overflow: hidden;
  cursor: pointer;
  top: -72px;
  margin-right: -24px;
  width: 48px;
  height: 48px;
}
.logmod__container {
  overflow: hidden;
  width: 100%;
}
.logmod__container::after {
  clear: both;
  content: "";
  display: table;
}
.logmod__tab {
  position: relative;
  width: 100%;
  height: 0;
  overflow: hidden;
  opacity: 0;
  visibility: hidden;
}
.logmod__tab-wrapper {
  width: 100%;
  height: auto;
  overflow: hidden;
}
.logmod__tab.show {
  opacity: 1;
  height: 100%;
  visibility: visible;
}
.logmod__tabs {
  list-style: none;
  padding: 0;
  margin: 0;
}
.logmod__tabs::after {
  clear: both;
  content: "";
  display: table;
}
.logmod__tabs li.current a {
  background: #FFF;
  color: #333;
}
.logmod__tabs li a {
  width: 50%;
  position: relative;
  float: left;
  text-align: center;
  background: #D2D8D8;
  line-height: 72px;
  height: 72px;
  text-decoration: none;
  color: #809191;
  text-transform: uppercase;
  font-weight: 700;
  font-size: 16px;
  cursor: pointer;
}
.logmod__tabs li a:focus {
  outline: dotted 1px;
}
.logmod__heading {
  text-align: center;
  padding: 5px 0 5px 0;
}
.logmod__heading-subtitle {
  display: block;
  font-weight: 400;
  font-size: 15px;
  color: #888;
  line-height: 48px;
}
.logmod__form {
  border-top: 1px solid #e5e5e5;
}
.logmod__alter {
  display: block;
  position: relative;
  margin-top: 7px;
}
.logmod__alter::after {
  clear: both;
  content: "";
  display: table;
}
.logmod__alter .connect:last-child {
  border-radius: 0 0 4px 4px;
}

.connect {
  overflow: hidden;
  position: relative;
  display: block;
  width: 100%;
  height: 52px;
  line-height: 52px;
  text-decoration: none;
}
.connect::after {
  clear: both;
  content: "";
  display: table;
}
.connect:focus, .connect:hover, .connect:visited {
  color: #FFF;
  text-decoration: none;
}
.connect__icon {
    vertical-align: middle;
    float: left;
    width: 50px;
    text-align: center;
    font-size: 22px;
    height: 52px;
    padding-top: 15px;
}
.connect__context {
  vertical-align: middle;
  text-align: center;
}
.connect.facebook {
  background: #3b5998;
  color: #FFF;
}
.connect.facebook a {
  color: #FFF;
}
.connect.facebook .connect__icon {
  background: #283d68;
}
.connect.googleplus {
  background: #dd4b39;
  color: #FFF;
}
.connect.googleplus a {
  color: #FFF;
}
.connect.googleplus .connect__icon {
  background: #b52f1f;
}

.connect.twitter {
  background: #73b4ff;
  color: #FFF;
}
.connect.twitter a {
  color: #FFF;
}
.connect.twitter .connect__icon {
  background: #4099ff;
}

.connect.ofiskita {
  background: #E7E7E7;
  color: #565656;
}
.connect.ofiskita a {
  color: #565656;
}
.connect.ofiskita .connect__icon {
  background: #565656;
  color:#FFF;
}


.simform {
  position: relative;
}
.simform__actions {
  padding: 15px;
  font-size: 14px;
}
.simform__actions::after {
  clear: both;
  content: "";
  display: table;
}
.simform__actions .sumbit {
  height: 28px;
  float: left;
  margin-left:10px;
  color: #FFF;
  font-weight: 700;
  font-size: 16px;
  background: #EE682D;
  margin-top: 7px;
}
.simform__actions .sumbit::after {
  clear: both;
  content: "";
  display: table;
}
.simform__actions-sidetext {
  display: inline-block;
  float: right;
  padding: 0 10px;
  margin: 9px 0 0 0;
  color: #8C979E;
  text-align: center;
  line-height: 24px;
}
.simform__actions-sidetext::after {
  clear: both;
  content: "";
  display: table;
}

.sminputs::after {
  clear: both;
  content: "";
  display: table;
}
.sminputs .input {
  display: block;
  position: relative;
  width: 50%;
  height: 71px;
  padding: 11px 24px;
  border-bottom: none;
  float: left;
  background-color: #FFF;
  border-radius: 0;
  box-sizing: border-box;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
.sminputs .input.active {
  background: #eee;
}
.sminputs .input.active .hide-password {
  background: #eee;
}
.sminputs .input.full {
  width: 100%;
  height:auto;
}
.sminputs .input label {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  display: block;
  width: 100%;
  text-transform: uppercase;
  letter-spacing: 1px;
  font-weight: 700;
  font-size: 12px;
  cursor: pointer;
  line-height: 24px;
}
.sminputs .input input {
  postion: relative;
  display: inline-block;
  height: 38px;
  font-size: 15px;
  line-height: 19.2px;
  color: #555;
  border-radius: 0px;
  vertical-align: middle;
  box-shadow: none;
  box-sizing: border-box;
  width: 100%;
  border: 1px solid #e9e9e9;
  padding: 6px 8px;
  cursor: pointer;
  background-color: transparent !important;
  color: rgba(75, 89, 102, 0.85);
}
.sminputs .hide-password {
  display: inline-block;
  position: absolute;
  right: 0;
  top: 0;
  padding: 0 15px;
  border-left: 1px solid #e4e4e4;
  font-size: 14px;
  background: #FFF;
  overflow: hidden;
  color: #444;
  cursor: pointer;
  margin-top: 12px;
  line-height: 48px;
}

.btnlogin, .simform__actions .sumbit {
  display: inline-block;
  line-height: normal;
  white-space: nowrap;
  vertical-align: middle;
  text-align: center;
  cursor: pointer;
  box-sizing: border-box;
  border-top: 0px solid #EE682D;
  border-bottom: 3px solid #EE682D;
  border-left: 0px solid #EE682D;
  border-right: 0px solid #EE682D;
  outline: none;
  outline-offset: 0;
  font-weight: 400;
  box-shadow: none;
  height:38px;
  min-width: 90px;
  //padding: 5px 7px;
}
.btn.full, .simform__actions .full.sumbit {
  width: 100%;
}
.btn.lg, .simform__actions .lg.sumbit {
  min-width: 125px;
  padding: 17px 14px;
  font-size: 22px;
  line-height: 1.3;
}
.btn.sm, .simform__actions .sm.sumbit {
  min-width: 65px;
  padding: 4px 12px;
  font-size: 14px;
}
.btn.xs, .simform__actions .xs.sumbit {
  min-width: 45px;
  padding: 2px 10px;
  font-size: 10px;
  line-height: 1.5;
}
.btn.circle, .simform__actions .circle.sumbit {
  overflow: hidden;
  width: 56px;
  height: 56px;
  min-width: 56px;
  line-height: 1;
  padding: 0;
  border-radius: 50%;
}
.btn.circle.lg, .simform__actions .circle.lg.sumbit {
  width: 78px;
  height: 78px;
  min-width: 78px;
}
.btn.circle.sm, .simform__actions .circle.sm.sumbit {
  width: 40px;
  height: 40px;
  min-width: 40px;
}
.btn.circle.xs, .simform__actions .circle.xs.sumbit {
  width: 30px;
  height: 30px;
  min-width: 30px;
}
.btn:focus, .simform__actions .sumbit:focus, .btn:active, .simform__actions .sumbit:active, .btn.active, .simform__actions .active.sumbit, .btn:active:focus, .simform__actions .sumbit:active:focus, .btn.active:focus, .simform__actions .active.sumbit:focus {
  outline: 0;
  outline-offset: 0;
  box-shadow: none;
}
.btn.red, .simform__actions .red.sumbit {
  background: #f44336;
  color: #FFF;
}
.btn.red:active, .simform__actions .red.sumbit:active, .btn.red:focus, .simform__actions .red.sumbit:focus {
  background-color: #ef1d0d;
}
.btn.red:hover, .simform__actions .red.sumbit:hover {
  background-color: #f32c1e;
}
.btn.pink, .simform__actions .pink.sumbit {
  background: #E91E63;
  color: #FFF;
}
.btn.pink:active, .simform__actions .pink.sumbit:active, .btn.pink:focus, .simform__actions .pink.sumbit:focus {
  background-color: #c61350;
}
.btn.pink:hover, .simform__actions .pink.sumbit:hover {
  background-color: #d81557;
}
.btn.purple, .simform__actions .purple.sumbit {
  background: #9C27B0;
  color: #FFF;
}
.btn.purple:active, .simform__actions .purple.sumbit:active, .btn.purple:focus, .simform__actions .purple.sumbit:focus {
  background-color: #7b1f8a;
}
.btn.purple:hover, .simform__actions .purple.sumbit:hover {
  background-color: #89229b;
}
.btn.deep-purple, .simform__actions .deep-purple.sumbit {
  background: #673AB7;
  color: #FFF;
}
.btn.deep-purple:active, .simform__actions .deep-purple.sumbit:active, .btn.deep-purple:focus, .simform__actions .deep-purple.sumbit:focus {
  background-color: #532f94;
}
.btn.deep-purple:hover, .simform__actions .deep-purple.sumbit:hover {
  background-color: #5c34a4;
}
.btn.indigo, .simform__actions .indigo.sumbit {
  background: #3F51B5;
  color: #FFF;
}
.btn.indigo:active, .simform__actions .indigo.sumbit:active, .btn.indigo:focus, .simform__actions .indigo.sumbit:focus {
  background-color: #334293;
}
.btn.indigo:hover, .simform__actions .indigo.sumbit:hover {
  background-color: #3849a2;
}
.btn.blue, .simform__actions .blue.sumbit {
  background: #2196F3;
  color: #FFF;
}
.btn.blue:active, .simform__actions .blue.sumbit:active, .btn.blue:focus, .simform__actions .blue.sumbit:focus {
  background-color: #0c7fda;
}
.btn.blue:hover, .simform__actions .blue.sumbit:hover {
  background-color: #0d8aee;
}
.btn.light-blue, .simform__actions .light-blue.sumbit {
  background: #03A9F4;
  color: #FFF;
}
.btn.light-blue:active, .simform__actions .light-blue.sumbit:active, .btn.light-blue:focus, .simform__actions .light-blue.sumbit:focus {
  background-color: #028ac7;
}
.btn.light-blue:hover, .simform__actions .light-blue.sumbit:hover {
  background-color: #0398db;
}
.btn.cyan, .simform__actions .cyan.sumbit {
  background: #00BCD4;
  color: #FFF;
}
.btn.cyan:active, .simform__actions .cyan.sumbit:active, .btn.cyan:focus, .simform__actions .cyan.sumbit:focus {
  background-color: #0093a6;
}
.btn.cyan:hover, .simform__actions .cyan.sumbit:hover {
  background-color: #00a5bb;
}
.btn.teal, .simform__actions .teal.sumbit {
  background: #009688;
  color: #FFF;
}
.btn.teal:active, .simform__actions .teal.sumbit:active, .btn.teal:focus, .simform__actions .teal.sumbit:focus {
  background-color: #00685e;
}
.btn.teal:hover, .simform__actions .teal.sumbit:hover {
  background-color: #007d71;
}
.btn.green, .simform__actions .green.sumbit {
  background: #4CAF50;
  color: #FFF;
}
.btn.green:active, .simform__actions .green.sumbit:active, .btn.green:focus, .simform__actions .green.sumbit:focus {
  background-color: #3e8f41;
}
.btn.green:hover, .simform__actions .green.sumbit:hover {
  background-color: #449d48;
}
.btn.light-green, .simform__actions .light-green.sumbit {
  background: #8BC34A;
  color: #FFF;
}
.btn.light-green:active, .simform__actions .light-green.sumbit:active, .btn.light-green:focus, .simform__actions .light-green.sumbit:focus {
  background-color: #74a838;
}
.btn.light-green:hover, .simform__actions .light-green.sumbit:hover {
  background-color: #7eb73d;
}
.btn.lime, .simform__actions .lime.sumbit {
  background: #CDDC39;
  color: #FFF;
}
.btn.lime:active, .simform__actions .lime.sumbit:active, .btn.lime:focus, .simform__actions .lime.sumbit:focus {
  background-color: #b6c423;
}
.btn.lime:hover, .simform__actions .lime.sumbit:hover {
  background-color: #c6d626;
}
.btn.yellow, .simform__actions .yellow.sumbit {
  background: #FFEB3B;
  color: #FFF;
}
.btn.yellow:active, .simform__actions .yellow.sumbit:active, .btn.yellow:focus, .simform__actions .yellow.sumbit:focus {
  background-color: #ffe60d;
}
.btn.yellow:hover, .simform__actions .yellow.sumbit:hover {
  background-color: #ffe821;
}
.btn.amber, .simform__actions .amber.sumbit {
  background: #FFC107;
  color: #FFF;
}
.btn.amber:active, .simform__actions .amber.sumbit:active, .btn.amber:focus, .simform__actions .amber.sumbit:focus {
  background-color: #d8a200;
}
.btn.amber:hover, .simform__actions .amber.sumbit:hover {
  background-color: #ecb100;
}
.btn.orange, .simform__actions .orange.sumbit {
  background: #FF9800;
  color: #FFF;
}
.btn.orange:active, .simform__actions .orange.sumbit:active, .btn.orange:focus, .simform__actions .orange.sumbit:focus {
  background-color: #d17d00;
}
.btn.orange:hover, .simform__actions .orange.sumbit:hover {
  background-color: #e68900;
}
.btn.deep-orange, .simform__actions .deep-orange.sumbit {
  background: #FF5722;
  color: #FFF;
}
.btn.deep-orange:active, .simform__actions .deep-orange.sumbit:active, .btn.deep-orange:focus, .simform__actions .deep-orange.sumbit:focus {
  background-color: #f33a00;
}
.btn.deep-orange:hover, .simform__actions .deep-orange.sumbit:hover {
  background-color: #ff4408;
}
.btn.brown, .simform__actions .brown.sumbit {
  background: #795548;
  color: #FFF;
}
.btn.brown:active, .simform__actions .brown.sumbit:active, .btn.brown:focus, .simform__actions .brown.sumbit:focus {
  background-color: #5c4137;
}
.btn.brown:hover, .simform__actions .brown.sumbit:hover {
  background-color: #694a3e;
}
.btn.grey, .simform__actions .grey.sumbit {
  background: #9E9E9E;
  color: #FFF;
}
.btn.grey:active, .simform__actions .grey.sumbit:active, .btn.grey:focus, .simform__actions .grey.sumbit:focus {
  background-color: #878787;
}
.btn.grey:hover, .simform__actions .grey.sumbit:hover {
  background-color: #919191;
}
.btn.blue-grey, .simform__actions .blue-grey.sumbit {
  background: #607D8B;
  color: #FFF;
}
.btn.blue-grey:active, .simform__actions .blue-grey.sumbit:active, .btn.blue-grey:focus, .simform__actions .blue-grey.sumbit:focus {
  background-color: #4d6570;
}
.btn.blue-grey:hover, .simform__actions .blue-grey.sumbit:hover {
  background-color: #566f7c;
}

.special {
  color: blue;
  position: relative;
  text-decoration: none;
}
.special:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 1px;
  bottom: 0px;
  left: 0;
  background: #f00;
  visibility: hidden;
}

.bold {
	font-weight:bold;
}
</style>
<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>  
      <div class="logmod">
		  <div class="logmod__wrapper">
		  <div class="panel panel-default">
			  <div class="panel-body">
				  <h1 class="text-center bold">Sign In</h1>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="sminputs">
                  <div class="input full">
                    <input class="string optional" maxlength="255" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" type="text" size="50" />
                  </div>
                </div>
                <div class="sminputs">
                  <div class="input full">
                    <input class="string optional" maxlength="255" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" type="password" size="50" />
                  </div>
                </div>
                <div class="sminputs">
					<div class="simform__actions">
					  <input class="sumbit" name="commit" type="submit" value="<?php echo $button_login; ?>" />
					  <span class="simform__actions-sidetext"><a class="special" role="link" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></span>
					  <?php if ($redirect) { ?>
					  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
					  <?php } ?>
					</div> 
				</div> 
              </form>
              <div class="simform__actions" style="vertical-align: middle;">
				<div class="col-md-6">
				  <b>Or login with</b>
				</div>
				  <div class="pull-right">
					<a href="<?php echo $facebook;?>"><button class="btn sm circle connect facebook"><i class="fa fa-facebook"></i></button></a>
					<a href="<?php echo $twitter;?>"><button class="btn sm circle connect twitter"><i class="fa fa-twitter"></i></button></a>
					<a href="<?php echo $google;?>"><button class="btn sm circle connect googleplus"><i class="fa fa-google-plus"></i></button></a>
				  </div>
				</div>
			  </div>
			  <div class="panel-footer" style="background-color:#EE682D">
					<a href="<?php echo $register; ?>" class="connect googleplus" style="background-color:#EE682D">
					  <div class="connect__context">
						<span><strong style="text-transform:uppercase">create an account</strong></span>
					  </div>
					</a>
				</div>
			</div>
		</div>
		</div>
		<div class="clearfix"></div>
	  
      <?php echo $content_bottom; ?>
    </div>
	  <?php //echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>
