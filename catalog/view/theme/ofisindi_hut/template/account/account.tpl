<?php echo $header; ?>
<style>
.form-group{
	border-bottom:rgb(245,245,245) 1px solid;
	background:rgb(253,253,253);
}
.form-group .label-right{
	text-align: right;
}
.form-group .label-left{
	text-align: left;
	font-weight:bold;
	width:70% !important;
}
.notification-container{
	width:100%;
	//height:30px;
	text-align:right;
	margin-bottom:10px;
}
.notification{
	float:left;
	
	height:30px;
	background:#eaeaea;
	margin:5px;
	display: block;
	position:absolute;
	text-align:center;
	padding-top:5px;
	border-radius: 5px;
}
.order{
	right:60px;
	width:30px;
        top :0px;
}
.reward-point{
	right:100px;
	font-weight:bold;
	width:auto;
	padding-left:10px;
	padding-right:10px;
        top:0px;
}
.review{
	right:20px;
	width:30px;
        top :0px;
}
span.badge{
	position: absolute;
    top: -5px;
    right: -10px;
}
.btn-sm{
	padding:3px 8px;
}

.activate{
  margin-bottom:20px;

}

.more{
  margin-bottom:20px;
}

</style>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $text_my_account; ?></h1>
      <div class="notification-container">
		<a data-tooltip="Review" class="notification   review" href="<?php echo $review;?>"><i class="fa fa-thumbs-o-up fa-lg"></i><span class="badge"><?php echo $notification_review; ?></span></a>
		<a data-tooltip="Order History" class="notification  order" href="<?php echo $order;?>"><i class="fa fa-th-list fa-lg"></i><span class="badge"><?php echo $notification_order; ?></span></a>
		<a class="notification  reward-point" href="<?php echo $reward;?>"><i class="fa fa-smile-o fa-lg"></i><?php echo $notification_reward; ?></a>
	</div>
	<div class="xl-100">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_name;?></td>
              <td><?php echo $text_email;?></td>
              <td><?php echo $text_phone;?></td>
              <td><?php echo $text_dob;?></td>
              <td><?php echo $text_account_bank;?></td>
              <td><?php echo $text_account_name;?></td>
              <td><?php echo $text_account_number;?></td>
              <td><?php echo $text_balance;?></td>
            </tr>
          </thead>
          <tbody>
               <tr>
              <td><?php echo $name;?> <a style="margin-left:10px; margin-top: 10px;" class="btn btn-default" href="<?php echo $edit;?>"><i class="fa fa-pencil"></i>&nbsp;<?php echo $button_edit;?></a></td>
              <td><?php echo $email;?></td>
              <td><?php echo $phone;?></td>
              <td><?php echo $dob;?></td>
              <td><?php echo $account;?></td>
              <td><?php echo $account_name;?></td>
              <td><?php echo $account_number;?></td>
              <td><?php echo $balance;?></td>
            </tr>
          </tbody>
        </table>
      </div>           

		<?php if($this->customer->getVerified()!=2){ ?>

                     <div class="activate">
                       <a class="btn btn-default" href="<?php echo $activate;?>"><?php echo $text_activate;?></a>
                     </div>

		<?php }?>
		
	</div>
	<div class="">
	<div class="panel panel-default col-lg-6">
  <div class="panel-heading">
    <h2 class="panel-title"><i class="fa fa-th-list"></i> <?php echo $text_list_order;?></h2>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-hover list" style="margin-bottom: 10px;">
      <thead>
        <tr>
          <td class="text-right"><?php echo $text_column_invoice_order;?></td>
          <td><?php echo $text_column_status_order;?></td>
          <td class="text-right"><?php echo $text_column_action_order;?></td>
        </tr>
      </thead>
      <tbody>
		<?php if(!empty($orders)){?>
		<?php foreach($orders as $order_data){?>
        <tr>
          <td class="text-right"><?php echo $order_data['invoice'];?></td>
          <td><?php echo $order_data['status'];?></td>
          <td class="text-right"><a href="<?php echo $order_data['link']; ?>" data-toggle="tooltip" title="<?php echo $text_view;?>" class="btn btn-info btn-sm" data-original-title="<?php echo $text_view;?>"><i class="fa fa-eye"></i></a></td>
        </tr>
		 <?php }?>
		 <?php }else{?>
		 <tr><td colspan="3"><?php echo $text_no_data;?></td></tr>
		 <?php }?>
        </tbody>
    </table>
	<div class="more">
          <a class="btn btn-default" href="<?php echo $order;?>"><?php echo $text_view_more;?></a>
        </div>
  </div>
</div>
<div class="panel panel-default col-lg-6">
  <div class="panel-heading">
    <h2 class="panel-title"><i class="fa fa-mail-reply"></i>  <?php echo $text_list_return;?></h2>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-hover list" style="margin-bottom: 10px;">
      <thead>
        <tr>
          <td class="text-right"><?php echo $text_column_invoice_return;?></td>
          <td><?php echo $text_column_product_return;?></td>
		   <td><?php echo $text_column_status_return;?></td>
          <td class="text-right"><?php echo $text_column_action_return;?></td>
        </tr>
      </thead>
      <tbody>
          <?php if(!empty($returns)){?>
		<?php foreach($returns as $return_data){?>
        <tr>
          <td class="text-right"><?php echo $return_data['invoice'];?></td>
		  <td><?php echo $return_data['product'];?></td>
          <td><?php echo $return_data['status'];?></td>
          <td class="text-right"><a href="<?php echo $return_data['link']; ?>" data-toggle="tooltip" title="<?php echo $text_view;?>" class="btn btn-info btn-sm" data-original-title="<?php echo $text_view;?>"><i class="fa fa-eye"></i></a></td>
        </tr>
		 <?php }?>
		 <?php }else{?>
		 <tr><td colspan="4"><?php echo $text_no_data;?></td></tr>
		 <?php }?>
       </tbody>
    </table>
	<div class="more">
          <a class="btn btn-default" href="<?php echo $return;?>"><?php echo $text_view_more;?></a>
        </div>
  </div>
</div>
</div>
      <?php echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<?php echo $footer; ?>		