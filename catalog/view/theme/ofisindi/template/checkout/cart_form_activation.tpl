<strong><?php echo $text_md_cart_activation_message ?></strong> <a href="<?php echo $link ?>"><?php echo $button_activation ?></a>
<script type="text/javascript">
	$("#md-button-submit").empty().html('<?php echo $button_activation ?>');
	$("#md-button-submit").unbind('click');
	$('#md-button-submit').on('click', function(){
		location='<?php echo $link ?>';
	});
</script>