<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php echo $title; ?></title>
<!-- <title>Ofiskita</title> -->
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/grid/<?php echo $cosyone_max_width; ?>.css" />
<!--<link href="catalog/view/javascript/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />-->
<!--<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->


<!--https font awesome-->

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/FontAwesome.otf" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/fontawesome-webfont.eot" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/fontawesome-webfont.svg" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/fontawesome-webfont.ttf" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/fontawesome-webfont.woff" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/fonts/fontawesome-webfont.woff2" rel="stylesheet" type="text/css" />



<?php if($cosyone_use_responsive == 'enabled'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/responsive.css" />
<?php } ?>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php if ($direction == 'rtl') { ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/rtl.css" />
<?php } ?>
<!-- INDIGITAL CSS -->
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/indigital_custom.css" />


<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="catalog/view/theme/ofisindi/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/ofisindi/js/ofisindi_common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!-- <script src="http://code.jquery.com/jquery-1.9.0.js"></script> -->
<!-- <script src="http://code.jquery.com/jquery-migrate-1.3.0.js"></script> -->
<!--[nos 8]>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/ofisindi/stylesheet/ie8.css" />
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
<script type="text/javascript">
// $( document ).ajaxSuccess(function( event, xhr, settings ) {
	// console.log(settings.url);
  // if ( settings.url == "<?php echo $this->url->link('account/login', '', 'SSL'); ?>" ) {
    // console.log( "Triggered ajaxSuccess handler. The Ajax response was: " +
      // xhr.responseText );
  // }
// });
</script>
<script type="text/javascript">
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) confirm("Not Support Browser Internet Explorer"),close();
  var isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) confirm("Not Support Browser Internet Explorer"),close();
</script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php echo $google_analytics; ?>
<?php echo $cosyone_styles; ?>
</head>
<body class="<?php echo $class; ?>">
	<div class="outer_container <?php echo $cosyone_default_product_style; ?> <?php if($cosyone_use_custom){ echo $cosyone_container_layout; } ?> <?php echo $cosyone_use_breadcrumb; ?>">
		<div class="header_wrapper 
			<?php echo $cosyone_menu_sticky; ?> 
			<?php echo $cosyone_menu_border; ?> 
			<?php echo $cosyone_header_style; ?>">
			<div class="header_top_line_wrapper">
				<div class="header_top_line container">
					<?php if($cosyone_header_style == 'header1'){ ?>
    				<?php } else { ?>
				    <div class="drop_downs_wrapper">
				    <?php echo $header_login; ?>
				  	<?php echo $currency; ?>
				    </div>
    				<?php } ?>
					<?php
					(string)$url ="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					(string)$urls = "https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					if(isset($this->request->get['information_id'])){
						$information_id=$this->request->get['information_id'];
					}else{
						$information_id=0;
					}
					?>
  					<!-- <div class="promo_message"><?php echo $cosyone_top_promo_message; ?></div> -->
				   	<div class="promo_message">
<div class="mnr_a"><a href="#">OFISKITA</a></div>
				    	<!--<div class="mnr_a"><a href="#">POD</a></div>-->
				<!-- 	<div class="mnr_a <?php if($url == $link_workingspace OR $urls == $link_workingspace){ echo 'active'; } ?>"><a href="<?php echo $link_workingspace; ?>" class="<?php if($url == $link_workingspace OR $urls == $link_workingspace){ echo 'active'; } ?>">WORKING SPACE</a></div> -->
				    	 <div class="mnr_a <?php if($url == $link_workingspace OR $urls == $link_workingspace OR (int)$information_id==16){ echo 'active'; } ?> " ><a href="<?php echo $link_workingspace; ?>" class="<?php if($url == $link_workingspace OR $urls == $link_workingspace OR (int)$information_id==16){ echo 'active'; } ?> ">WORKING SPACE</a></div>
              <div class="mnr_a <?php if($url == $link_ceria OR $urls == $link_ceria OR (int)$information_id==11){ echo 'active'; } ?> " ><a href="<?php echo $link_ceria; ?>" class="<?php if($url == $link_ceria OR $urls == $link_ceria OR (int)$information_id==11){ echo 'active'; } ?> ">CERIA</a></div>
				    	<div class="mnr_a <?php if($url == $link_kiosk OR $urls == $link_kiosk OR (int)$information_id==14){ echo 'active'; } ?> " ><a href="<?php echo $link_kiosk; ?>" class="<?php if($url == $link_kiosk OR $urls == $link_kiosk OR (int)$information_id==14){ echo 'active'; } ?> ">KIOSK</a></div>
				  	</div>
 					<div class="links contrast_font language hidden-xs">
						<a href="<?php echo $register_merchant; ?>" class=""><i class="fa fa-user"></i><?php echo $text_register_merchant; ?></a>
					  	<!-- <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a> -->
					  	<!-- <a href="<?php //echo $wishlist; ?>" id="wishlist-total"><?php //echo $text_wishlist; ?></a>
					  	<a href="<?php //echo $shopping_cart; ?>"><?php //echo $text_shopping_cart; ?></a>
					  	<a href="<?php //echo $checkout; ?>"><?php //echo $language; ?></a> -->
					  	<?php echo $language; ?>
  					</div>
  					<div class="clearfix"></div>
				</div>
			</div>
			<div class="header header-sticky">  
				<div class="header_main hidden-xs hidden-sm">
  					<div class="header_right"> 
  						<?php if ($logo) { ?>
					  	<div class="logo">
					    	<a href="<?php echo $home; ?>">
					      		<img class="img_logo" src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" style=""/>
					    	</a>
					  	</div>
  						<?php } ?>
					   	<span id="fullhead">
						   	<?php if($cosyone_header_style == 'header3') { ?>
						   	<?php } else { ?>
						   	<?php if($cosyone_header_search == 'enabled') { ?>
						   	<?php echo $search; ?>
						   	<?php } ?>
						   	<?php } ?>
   							<div class="mobile_clear"></div>
	    					<!-- Position for header login, lang, curr, in the header main -->
	  						<?php if($cosyone_header_style == 'header1'){ ?>
	   
						     	<!--<a href="<?php //echo $wishlist; ?>" class="btn btn-default btn-header" id="wishlist-total">
						          <i class="fa fa-star"></i>-->
						      	<?php //echo $text_wishlist; ?></a> <!-- INDIGITAL CUSTOM -->
                    <?php if (!$logged) { ?>
                      <div class="reg">
                        <a href="<?php echo $register;?>" class="button register" style=""><i class="fa fa-user"></i> <?php echo $text_register; ?></a>
                      </div>
                    <?php } else { ?>
                    <?php } ?>
						      	<?php echo $header_login; ?> 
						      	
    							<?php //echo $currency; ?>
  						</span>
  						<div class="collapse" id="collapseTopMenu">
  							<?php echo $search; ?>

						   	<!--<a href="<?php //echo $wishlist; ?>" class="btn btn-default btn-header" id="wishlist-total">
						        <i class="fa fa-star"></i>-->
						    <?php //echo $text_wishlist; ?></a> <!-- INDIGITAL CUSTOM -->
						    <?php echo $header_login; ?>
						    <?php if (!$logged) { ?>
	      						<a href="<?php echo $register;?>" class="button register" ><?php echo $text_register; ?></a>
	    					<?php } else { ?>
	    					<?php } ?>
						    <!-- <div class="hidden-sm">
						      <?php echo $language; ?> 
						    </div>  -->
	    					<?php //echo $currency; ?>
  						</div>
    				<?php } ?>
  					<?php if($cosyone_header_style == 'header2') { ?>
	    			<div class="shortcuts_wrapper">

			       	<?php echo $header_wishlist_compare; ?>
			        <?php if ($cosyone_header_cart) { echo $cart; } ?>
	        		</div>
    			<?php } else { ?>
    			<?php } ?>
  				<?php if($cosyone_header_style == 'header3'){ ?>
    			<?php } else { ?>
      			</div> <!-- header_right ends -->
			</div> <!-- header ends -->
    		<?php } ?>

<div class="menu_wrapper">
<div class="container menu_border" style="background: #9a0034;"></div>
<div class="container menu_holder">
<div id="menu">
<?php if($cosyone_header_style == 'header2') { ?>
    	<?php } else { ?>
        <div class="shortcuts_wrapper">
        <?php if($cosyone_header_style == 'header3') { ?>
         <div class="search-holder">
         <?php echo $search; ?>
         </div>
         <?php } ?>

        <?php echo $header_wishlist_compare; ?>
        <?php if ($this->customer->isLogged()){ if ($cosyone_header_cart) { echo $cart; }} ?>  
           <a  id="btn1" class="btn btn-primary btn1 hide-top-menu ">
            <i class="fa fa-search"></i>
          </a> 
          <!-- </div> -->
          <?php if (!$logged) { ?>
            <a href="<?php echo $login; ?>" class="btn btn-primary" id="hide-top-menu" type="button" 
            data-original-title="<?php echo $text_login; ?>" data-placement="bottom" data-toggle="tooltip" >
              <i class="fa fa-user"></i>
            </a>
          <?php } else { ?>
            <a href="<?php echo $login; ?>" class="btn btn-primary" id="hide-top-menu" type="button"
             data-original-title="<?php echo $text_my_account; ?>" data-placement="bottom" data-toggle="tooltip">
              <i class="fa fa-user"></i>
            </a>
          <?php } ?> 
          <?php if ($logged) { ?>
            <a href="<?php echo $logout_link; ?>" class="btn btn-primary" id="hide-top-menu" type="button" style="padding: 0px" data-original-title="<?php echo $text_logout; ?>" data-placement="bottom" data-toggle="tooltip">
              <i class="fa fa-sign-out"></i>
            </a>
          <?php } ?>
        </div>
 <?php } ?>
<a class="mobile_menu_trigger up_to_tablet"><i class="fa fa-bars"></i> <?php //echo $cosyone_text_mobile_menu; ?></a>
  <ul class="only_desktop">
	<li class="home only_desktop <?php echo $cosyone_show_home_icon; ?>"><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
       <?php if ($categories) { ?>
       <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['category_1_id'] == $category_1_id) { ?>
		<li class="col<?php echo $category_1['column']; ?> current"><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?><i class="fa fa-sort-desc"></i></a>
	  <?php }
	  else { 

           if($category_1['name'] == "Flash Sale" || $category_1['name'] == "Bayar 72% Saja")  { ?>
       
          
           <li class="col<?php echo $category_1['column']; ?>"><a href="<?php echo $category_1['href']; ?>" >

           <?php echo $category_1['name']; ?> 
           &nbsp;<span style="    font-size: 8px;
            color: #fff;
            background-color: #f8011e;
            padding: 2px 5px;
            /*display: inline-block;*/
            /*margin-left: 10px;*/
            vertical-align: middle;
            position: relative;
            top: -15px;
            /*right: -38px;*/
            border-radius: 2px;
            font-weight: 400;"> HOT &nbsp;</span>  


           <i class="fa fa-sort-desc"></i></a>
           
           <?php  
           }
         else { ?>
 
         <li class="col<?php echo $category_1['column']; ?>"><a href="<?php echo $category_1['href']; ?>" >

          <?php echo $category_1['name'];?> 


         <i class="fa fa-sort-desc"></i></a>
        
 <?php  } }  ?>
          <?php if ($category_1['children']) { ?>
          <div class="menu_drop_down" style="width: <?php echo ((($category_1['column']) * (195)) + (10)); ?>px">
          <div class="wrapper">
          <ul><?php foreach ($category_1['children'] as $category_2) { ?>
          <li class="column level2">
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?><i class="fa fa-caret-right"></i></a>
            <?php if($cosyone_menu_mega_second_thumb == 'enabled' && $category_2['thumb']) { ?>
          <a href="<?php echo $category_2['href']; ?>" class="sub_thumb"><img src="<?php echo $category_2['thumb']; ?>" alt="<?php echo $category_2['name']; ?>"/></a>
          <?php } ?>
              <?php if ($category_2['children']) { ?>
              <div class="third">
              <ul>
               <?php foreach ($category_2['children'] as $category_3) { ?>
               <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
               <?php } ?>
              </ul>
              </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          </div><!-- wrapper ends -->
          </div>
          <?php } ?>
        </li>
        <?php } ?>
       
<?php } ?>
        <?php if($cosyone_custom_menu_block == 'enabled'){ ?>
		<li class="withsubs custom_block"><a><?php echo $cosyone_custom_menu_block_title; ?><i class="fa fa-sort-desc"></i></a>
        <div class="menu_drop_down" style="width:<?php echo $cosyone_menu_block_width; ?>px">
        <?php echo $cosyone_menu_custom_block_content; ?>
        </div></li>
		<?php } ?>
        <?php if($cosyone_custom_menu_title1){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url1 ?>"><?php echo $cosyone_custom_menu_title1; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title2){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url2 ?>"><?php echo $cosyone_custom_menu_title2; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title3){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url3; ?>"><?php echo $cosyone_custom_menu_title3; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title4){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url4; ?>"><?php echo $cosyone_custom_menu_title4; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title5){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url5; ?>"><?php echo $cosyone_custom_menu_title5; ?></a></li>
        <?php } ?>
      </ul>

      <?php if ($logo) { ?>
     <div class="logo1"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
  <?php } ?>

    </div> <!-- menu_holder ends -->
</div> <!-- menu ends -->
</div> <!-- menu_wrapper ends -->

 <div id="cari" style="background-color: #9A0034;">
	<div class="container">
	  	<div class="row">
	    	<div class="col-md-12">
	      <div id="search1">
	        <div class="container">
	            <div class="input-group">
	              <input type="text" class="search_input"  name="search" placeholder="<?php echo $text_search; ?>" style="width: 100%">
	              <span class="input-group-addon button-search" style="cursor: pointer;"><i class="fa fa-search"></i></span>
	            </div>
	        </div>
	      </div>
	      <!-- <div id="search1" style="display:none;padding:20px 0"><input type="text" class="search_input" placeholder="Search" style="width: 100%"></div> -->
	    	</div>
	  </div>
	</div>
</div>



<?php if($cosyone_header_style == 'header3'){ ?>
      </div> <!-- header_right ends -->
	</div> <!-- header ends -->
    <?php } else { ?><?php } ?>
<div class="clearfix"></div>
<div class="mobile_menu_wrapper">

<div class="mobile_menu" style="overflow: scroll;height: 200px;">

	<ul>
    <?php if ($categories) { ?>
       <?php foreach ($categories as $category_1) { ?>
         <li><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
          <?php if ($category_1['children']) { ?>
          <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
          <ul>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li>
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
              <?php if ($category_2['children']) { ?>
              <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
              <ul>
                <?php foreach ($category_2['children'] as $category_3) { ?>
                <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
        <?php } ?>
        <?php if($cosyone_custom_menu_title1){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url1; ?>"><?php echo $cosyone_custom_menu_title1; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title2){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url2; ?>"><?php echo $cosyone_custom_menu_title2; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title3){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url3; ?>"><?php echo $cosyone_custom_menu_title3; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title4){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url4; ?>"><?php echo $cosyone_custom_menu_title4; ?></a></li>
        <?php } ?>
        <?php if($cosyone_custom_menu_title5){ ?>
		<li><a href="<?php echo $cosyone_custom_menu_url5; ?>"><?php echo $cosyone_custom_menu_title5; ?></a></li>
        <?php } ?>
      </ul>


</div>
</div>
</div> <!-- header_wrapper ends -->
</div> <!-- inner conainer ends -->



<!-- <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div id="search1">
        <div class="container">
            <div class="input-group">
              <input type="text" class="search_input" placeholder="<?php echo $text_search; ?>" style="width: 100%">
              <span class="input-group-addon" style="cursor: pointer;"><i class="fa fa-search"></i></span>
            </div>
        </div>
      </div> -->
      <!-- <div id="search1" style="display:none;padding:20px 0"><input type="text" class="search_input" placeholder="Search" style="width: 100%"></div> -->
  <!--   </div>
  </div>
</div> -->

<div class="breadcrumb_wrapper"></div>
<div id="notification" class="container"></div>


<script type='text/javascript'>
  	$(document).ready(function(){
    	$("#btn1").click(function(){
      		$("#search1").slideToggle(5);
    	});
  	});
</script>

<script type="text/javascript">
  $(document).ready(function () {
          $('[data-toggle="tooltip"]').tooltip();
        }); 
</script>

	
