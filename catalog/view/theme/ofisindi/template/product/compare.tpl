<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
	   <?php if ($products) { ?>
		<?php foreach (array_keys($products) as $category_id) { ?>
		  <?php $productscomp=array(); $productscomp =$products[$category_id]; $category=$categories[$category_id];//print_r($this->session->data['compare']);//unset($_SESSION['compare']); ?>
		  <?php if ($productscomp) { ?>
		  <table class="table table-bordered compare-info">
			<thead>
			  <tr>
				<td colspan="<?php echo count($productscomp) + 1; ?>" style="background: rgb(154, 0, 52);color: #fff;" class="compare-product"><strong><?php echo $category['name']; ?></strong></td>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td><?php echo $text_name; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td class="name"><a href="<?php echo $productscomp[$product['product_id']]['href']; ?>"><strong><?php echo $productscomp[$product['product_id']]['name']; ?></strong></a></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_image; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td class="text-center"><?php if ($productscomp[$product['product_id']]['thumb']) { ?>
				  <img src="<?php echo $productscomp[$product['product_id']]['thumb']; ?>" alt="<?php echo $productscomp[$product['product_id']]['name']; ?>" title="<?php echo $productscomp[$product['product_id']]['name']; ?>" class="img-thumbnail" />
				  <?php } ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_price; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php if ($productscomp[$product['product_id']]['price']) { ?>
				  <?php if (!$productscomp[$product['product_id']]['special']) { ?>
				  <?php echo $productscomp[$product['product_id']]['price']; ?>
				  <?php } else { ?>
				  <span class="price-old"><?php echo $productscomp[$product['product_id']]['price']; ?> </span> <span class="price-new"> <?php echo $productscomp[$product['product_id']]['special']; ?> </span>
				  <?php } ?>
				  <?php } ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_model; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php echo $productscomp[$product['product_id']]['model']; ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_manufacturer; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php echo $productscomp[$product['product_id']]['manufacturer']; ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_availability; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php echo $productscomp[$product['product_id']]['availability']; ?></td>
				<?php } ?>
			  </tr>
			  <?php if ($review_status) { ?>
			  <tr>
				<td><?php echo $text_rating; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td class="rating"><?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($productscomp[$product['product_id']]['rating'] < $i) { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				  <br />
				  <?php echo $productscomp[$product['product_id']]['reviews']; ?></td>
				<?php } ?>
			  </tr>
			  <?php } ?>
			  <tr>
				<td><?php echo $text_summary; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td class="description"><?php echo $productscomp[$product['product_id']]['description']; ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_weight; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php echo $productscomp[$product['product_id']]['weight']; ?></td>
				<?php } ?>
			  </tr>
			  <tr>
				<td><?php echo $text_dimension; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<td><?php echo $productscomp[$product['product_id']]['length']; ?> x <?php echo $productscomp[$product['product_id']]['width']; ?> x <?php echo $productscomp[$product['product_id']]['height']; ?></td>
				<?php } ?>
			  </tr>
			</tbody>
			<?php if (in_array($category_id, array_keys($attribute_groups))) {?>
			<?php foreach ($attribute_groups[$category_id] as $attribute_group) {?>
			
			<thead>
			  <tr>
				<td class="compare-attribute" colspan="<?php echo count($productscomp) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong></td>
			  </tr>
			</thead>
			<?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
			<tbody>
			  <tr>
				<td><?php echo $attribute['name']; ?></td>
				<?php foreach ($productscomp as $product) { ?>
				<?php if (isset($productscomp[$product['product_id']]['attribute'][$key])) { ?>
				<td><?php echo $productscomp[$product['product_id']]['attribute'][$key]; ?></td>
				<?php } else { ?>
				<td></td>
				<?php } ?>
				<?php } ?>
			  </tr>
			</tbody>
			<?php } ?>
			<?php } ?>
			<?php } ?>
			<tr>
			  <td></td>
			  <?php foreach ($productscomp as $product) { ?>
			  <td>
				<div class="cart <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
				  <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="btn btn-danger btn-block button hint--top compare-add-to-cart" data-hint="<?php echo $button_cart; ?>"><i class="button-left-icon"></i><span class="button-cart-text"><?php echo $button_cart; ?></span><i class="button-right-icon"></i></a>
					<a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block button compare-remove"><?php echo $button_remove; ?></a>
				</div>
			  </td>
			  <?php } ?>
			</tr>
		  </table>
		  <?php } else { ?>
		  <p><?php echo $text_empty; ?></p>
		  <div class="buttons">
			<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default button"><?php echo $button_continue; ?></a></div>
		  </div>
		  <?php } ?>
		  <?php } ?>
	  <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<!-- Modal -->
<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="min-width: 80%;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-cart-label"><?php echo $text_modal_cart ?></h4>
      </div>
      <div class="modal-body">
        <div id="modal-cart-content">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary button btn-block" id="md-button-submit" style="display: block;width: 100%;"><i class="fa fa-shopping-cart"></i> <?php echo $button_modal_cart ?></button>
      </div>
    </div>
  </div>
 </div>
<?php echo $footer; ?>