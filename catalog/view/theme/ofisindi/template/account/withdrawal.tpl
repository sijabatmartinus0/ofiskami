<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $text_heading; ?></h1>
		
	<form id="withdrawal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"  class="ms-form">
	
    <fieldset>
	<p class="col-sm-12"><?php echo $text_balance; ?> <b class="label-number"><?php echo $text_balance_formatted; ?></b> <span style="color: gray"><?php echo $text_reserved_formatted; ?></span></p>
		<p class="col-sm-12"><?php echo $text_balance_available; ?> <b class="label-number"><?php echo $balance_available_formatted; ?></b></p>
		<p class="col-sm-12"><?php echo $text_minimum; ?> <b class="label-number"><?php echo $this->currency->format($this->config->get('msconf_minimum_withdrawal_amount'),$this->config->get('config_currency')); ?></b></p>
		
		<?php if ($balance_available <= 0) { ?>
			<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i><?php echo $text_no_funds; ?></div>
		<?php } else if (!$withdrawal_minimum_reached) { ?>
			<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i><?php echo $text_minimum_not_reached; ?></div>
		<?php } else if ($no_account) { ?>
			<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i><?php echo $text_empty_account; ?></div>
		<?php } ?>
			<?php if (!$withdrawal_minimum_reached || $balance_available <= 0 || $no_account) { ?>
			<div class="ms-overlay"></div>
			<?php } ?>
			
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $text_amount; ?></label>
					<div class="col-sm-8">
						<?php if ($msconf_allow_partial_withdrawal) { ?>
						<p>
							<input type="radio" class="form-inline" name="withdraw_all" value="0" checked="checked" />
							<input type="text" class="form-control number" style="width: 100px; display: inline" name="withdraw_amount" value="<?php echo $this->currency->format($this->config->get('msconf_minimum_withdrawal_amount'),$this->config->get('config_currency'), '', FALSE); ?>" />
							<?php echo $currency_code; ?>
						</p>
						<?php } ?>
						
						<p>
							<input type="radio" name="withdraw_all" value="1" <?php if (!$msconf_allow_partial_withdrawal) { ?>checked="checked"<?php } ?> />
							<span><?php echo $text_all; ?> (<?php echo $balance_available_formatted; ?>)</span>
						</p>
						<p class="ms-note"><?php echo $text_amount_note; ?></p>
						<?php if ($error_withdraw_amount) { ?>
						  <div class="text-danger"><?php echo $error_withdraw_amount; ?></div>
						<?php } ?>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $text_method; ?></label>
					<div class="col-sm-8">
						<p>
							<input type="radio" name="withdraw_method" value="transfer" checked="checked" />
							<span><?php echo $text_method_transfer; ?></span>
						</p>
						<p class="ms-note"><?php echo $text_method_note; ?></p>
						<p class="error" id="error_withdraw_method"></p>
					</div>
				</div>
		</fieldset>
		

		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
			<?php if ($withdrawal_minimum_reached) { ?>
			<div class="pull-right"><button type="submit" class="btn btn-primary button" id="submit-request"><span><?php echo $button_submit; ?></span></button></div>
			<?php } ?>
		</div>
	</form>
      <?php echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$("body").delegate(".number", "keypress", function(e){
		 var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		  }
	});
$('input[name="withdraw_all"]').change(function() {
		if ($(this).val() == 1) {
			$('input[name="withdraw_amount"]').attr('disabled','disabled');
		} else {
			$('input[name="withdraw_amount"]').removeAttr('disabled');
		}
	});
//--></script> 
<?php echo $footer; ?>