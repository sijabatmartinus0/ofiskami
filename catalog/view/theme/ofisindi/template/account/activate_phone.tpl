<?php echo $header; ?>
<div id="container" class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php //echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	<div class="well">
	<h3><?php echo $text_progress; ?></h3>
	<div class="progress">
	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $text_progress_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $text_progress_percent; ?>%">
		<?php echo $text_progress_percent; ?>% Complete
	  </div>
	</div>
	<?php echo $text_progress_message; ?>
	</div>
	<?php echo $content_top; ?>
	
		<?php echo $content_bottom; ?></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="modal-success-label"data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-success-label"><?php echo $text_modal_success ?></h4>
      </div>
      <div class="modal-body">
        <?php echo $text_modal_success_message;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary button" data-dismiss="modal"><?php echo $button_ok ?></button>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript">
	  $(function() {
		$("#button-send,#button-resend").click(function() {
			var button = $(this);
			$.ajax({
				type: "POST",
				dataType: "json",
				url: $('base').attr('href') + 'index.php?route=account/activate/sendVerificationCode',
				data: $("form#form_phone_verification").serialize(),
				beforeSend: function() {
					$("#button-resend").hide();
					$("#button-send").hide();
					$('.error-content-activate').empty();
				},
				complete: function(jqXHR, textStatus) {
					$("#button-resend").hide();
				},
				success: function(jsonData) {
					if (!jQuery.isEmptyObject(jsonData.error)) {
						$('.error-content-activate').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+jsonData.error+'</div>');
					} else {
						$('#success-modal').modal('show');
						$('.error-content-activate').empty().html('<div class="alert alert-success success" style="margin-bottom:20px;width:auto">'+jsonData.success+'</div>');
					}
				}
			});
		});
		$("body").delegate(".number", "keypress", function(e){
			 var charCode = (e.which) ? e.which : e.keyCode;
			  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			  }
		});
	});
	  </script>
<?php echo $footer; ?>