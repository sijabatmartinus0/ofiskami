<?php echo $header; ?>
<div id="container" class="container j-container success-page">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php //echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	<div class="well">
	<h3><?php echo $text_progress; ?></h3>
	<div class="progress">
	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $text_progress_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $text_progress_percent; ?>%">
		<?php echo $text_progress_percent; ?>% Complete
	  </div>
	</div>
	<?php echo $text_progress_message; ?>
	<div class="buttons">
        <div class="text-center"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_shop; ?></a></div>
    </div>
	</div>
	<?php if (isset($text_success_email_verification)) { ?>
	  <div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $text_success_email_verification; ?></div>
	<?php } ?>
	<?php if (isset($text_error_email_verification)) { ?>
	  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $text_error_email_verification; ?></div>
	<?php } ?>
	<?php echo $content_top; ?>
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <!--<div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>-->
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>