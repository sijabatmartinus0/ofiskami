<?php echo $header; ?>
<div class="container" id="container-reg">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger">
        <i class="fa fa-exclamation-circle"></i> 
        <?php echo $error_warning; ?>
    </div>
    <?php } ?>
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset id="account">
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-nama"><?php echo $entry_nama; ?></label>
                        <div class="col-sm-9">
                            <input type="text" name="nama" value="<?php echo $nama; ?>" placeholder="<?php echo $entry_nama; ?>" id="input-nama" class="form-control" />
                            <?php if ($error_nama) { ?>
                            <div class="text-danger">
                                <?php echo $error_nama; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-badan_usaha"><?php echo $entry_badan_usaha; ?></label>
                        <div class="col-sm-9">
                            <?php if ($badan_usaha) { ?>
                            <label class="radio-inline">
                                <input type="radio" name="badan_usaha" value="Personal" checked="checked" />
                                <?php echo $text_personal; ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="badan_usaha" value="Corporate" />
                                <?php echo $text_corporate; ?>
                            </label>
                            <?php } else { ?>
                            <label class="radio-inline">
                                <input type="radio" name="badan_usaha" value="Personal" checked="checked" />
                                <?php echo $text_personal; ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="badan_usaha" value="Corporate" />
                                <?php echo $text_corporate; ?>
                            </label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <div class="col-sm-9">
                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            <?php if ($error_email) { ?>
                            <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                        <div class="col-sm-9">
                            <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control number" />
                            <?php if ($error_telephone) { ?>
                            <div class="text-danger"><?php echo $error_telephone; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-kategori_produk"><?php echo $entry_kategori_produk ?></label>
                        <div class="col-sm-9">
                            <input type="text" name="kategori_produk" value="<?php echo $kategori_produk; ?>" placeholder="<?php echo $entry_kategori_produk; ?>" id="input-kategori_produk" class="form-control" />
                            <?php if ($error_kategori_produk) { ?>
                            <div class="text-danger"><?php echo $error_kategori_produk; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-website_toko"><?php echo $entry_website_toko; ?></label>
                        <div class="col-sm-9">
                            <input type="text" name="website_toko" value="<?php echo $website_toko; ?>" placeholder="<?php echo $entry_website_toko; ?>" id="input-website_toko" class="form-control" />
                            <?php if ($error_website_toko) { ?>
                            <div class="text-danger"><?php echo $error_website_toko; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_npwp; ?></label>
                        <div class="col-sm-9">
                            <?php if ($npwp) { ?>
                            <label class="radio-inline">
                                <input type="radio" name="npwp" value="Yes" checked="checked" />
                                <?php echo $text_yes; ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="npwp" value="No" />
                                <?php echo $text_no; ?>
                            </label>
                            <?php } else { ?>
                            <label class="radio-inline">
                                <input type="radio" name="npwp" value="Yes" checked="checked" />
                                <?php echo $text_yes; ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="npwp" value="No" />
                                <?php echo $text_no; ?>
                            </label>
                            <?php } ?>
                        </div>
                    </div>
                    <input type="hidden" name="state" value="Request" />
                </fieldset>
                <br/>
                <div class="buttons">
                    <div class="pull-right">
                        <input type="submit" value="SUBMIT" class="btn btn-primary button" data-toggle="modal" data-target="#confirm-submit" />
                    </div>
                </div>
            </form>
            <?php echo $content_bottom; ?>
        </div>
        <!-- <?php echo $column_right; ?> -->
    </div>
</div> 
<!-- Modal -->
<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="min-width: 40%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terima kasih</h4>
            </div>
            <div class="modal-body">
                <h5>Terima kasih telah mengisi form pendaftaran merchant. Admin kami akan segera menghubungi anda.</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary button btn-block" id="close-submit">OK</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"><!--
// Sort the custom fields
    $(function () {
        $('#close-submit').click(function () {
            $('#confirm-submit').modal('hide');
        });
    });

    $('#account .form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
            $('#account .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#account .form-group').length) {
            $('#account .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#account .form-group').length) {
            $('#account .form-group:first').before(this);
        }
    });

    $('#address .form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
            $('#address .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#address .form-group').length) {
            $('#address .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#address .form-group').length) {
            $('#address .form-group:first').before(this);
        }
    });

    $('input[name=\'customer_group_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
            dataType: 'json',
            success: function (json) {
                $('.custom-field').hide();
                $('.custom-field').removeClass('required');

                for (i = 0; i < json.length; i++) {
                    custom_field = json[i];

                    $('#custom-field' + custom_field['custom_field_id']).show();

                    if (custom_field['required']) {
                        $('#custom-field' + custom_field['custom_field_id']).addClass('required');
                    }
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
//--></script> 
<script type="text/javascript"><!--
var t;
    $(document).on(
            'DOMMouseScroll mousewheel scroll',
            'body',
            function () {
                console.log('scrl')
                window.clearTimeout(t);
                t = window.setTimeout(function () {
                    $('.date').data("DateTimePicker").hide();
                }, 100);
            }
    );
    $(function () {

        var dateInput = $('.date');
        dateInput.parent().css("position", "relative");
        dateInput.datetimepicker({
            pickTime: false,
        });

        dateInput.on('dp.show', function () {
            var datepicker = $('body').find('.bootstrap-datetimepicker-widget:last');
            if (datepicker.hasClass('bottom')) {
                var top = $(this).offset().top + $(this).outerHeight();
                var right = $(this).offset().right;
                datepicker.css({
                    'top': top + 'px',
                    'bottom': 'auto',
                    'right': right + 'px'
                });
            } else if (datepicker.hasClass('top')) {
                var top = $(this).offset().top - datepicker.outerHeight();
                var right = $(this).offset().right;
                datepicker.css({
                    'top': top + 'px',
                    'bottom': 'auto',
                    'right': right + 'px'
                });
            }
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });
    })


    $("body").delegate(".number", "keypress", function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });

//--></script> 
<?php echo $footer; ?>
