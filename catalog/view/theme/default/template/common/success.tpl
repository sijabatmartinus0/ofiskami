<?php echo $header; ?>
<style>
.box-success{
    text-align: center;
    background-color: #dedede;
    width: 300px;
    margin: auto;
	padding: 10px;
}
</style>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
	  <?php if (isset($is_logged_in)){ ?>
	  <?php if ($is_logged_in){ ?>
		  <div class="box-success">
			<?php echo $text_total_price; ?><br/>
			<p style="color:#f24537; font-size: 16px; font-weight: bold; margin-top: 10px;"><?php echo (isset($total)? $total : '')?></p>
		  </div>
		  <?php echo (isset($text_message_transfer)? $text_message_transfer : ''); ?>
		  <div class="buttons">
			<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_confirm_payment; ?></a></div>
		  </div>
	  <?php } else if(!$is_logged_in){?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
	  <?php } ?>
	  <?php }else{ ?>
	  <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
	  <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<?php echo $footer; ?>