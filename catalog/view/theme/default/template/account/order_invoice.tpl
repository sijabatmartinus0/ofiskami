<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen,print" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
</head>
<style>
/* Print styling */

@media print {

[class*="col-sm-"] {
	float: left;
}

[class*="col-xs-"] {
	float: left;
}

.col-sm-12, .col-xs-12 { 
	width:100% !important;
}

.col-sm-11, .col-xs-11 { 
	width:91.66666667% !important;
}

.col-sm-10, .col-xs-10 { 
	width:83.33333333% !important;
}

.col-sm-9, .col-xs-9 { 
	width:75% !important;
}

.col-sm-8, .col-xs-8 { 
	width:66.66666667% !important;
}

.col-sm-7, .col-xs-7 { 
	width:58.33333333% !important;
}

.col-sm-6, .col-xs-6 { 
	width:50% !important;
}

.col-sm-5, .col-xs-5 { 
	width:41.66666667% !important;
}

.col-sm-4, .col-xs-4 { 
	width:33.33333333% !important;
}

.col-sm-3, .col-xs-3 { 
	width:25% !important;
}

.col-sm-2, .col-xs-2 { 
	width:16.66666667% !important;
}

.col-sm-1, .col-xs-1 { 
	width:8.33333333% !important;
}
  
.col-sm-1,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11,
.col-xs-12 {
float: left !important;
}

body {
	margin: 0;
	padding 0 !important;
	min-width: 768px;
}

.container {
	width: auto;
	min-width: 750px;
}

body {
	font-size: 10px;
}

a[href]:after {
	content: none;
}

.noprint, 
div.alert, 
header, 
.group-media, 
.btn, 
.footer, 
form, 
#comments, 
.nav, 
ul.links.list-inline,
ul.action-links {
	display:none !important;
}

}
</style>
<body>
<div class="container">
    <div style="page-break-after: always;">
	<div class="container-fluid">
	<div class="col-lg-10"><h1><?php echo $seller['nickname']; ?></h1></div>
	<div class="col-lg-2">
	<div class="pull-right"><a style="margin-top:20px;" onclick="window.print();" class="btn btn-info"><?php echo $text_print; ?><i class="fa fa-print"></i></a></div>
	</div>
	</div>
	<table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left" colspan="2"><i class="fa fa-info" style="padding: 3px;"></i><?php echo $text_order_detail; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
		  <td style="display: none;"><?php echo $order_detail_id; ?></td>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
              <b><?php echo $text_seller; ?></b> <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>
			</td>
            <td class="text-left"><?php if ($payment_method) { ?>
              <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
              <?php } ?></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <?php if ($shipping_address) { ?>
				<?php if ($delivery_type_id == 1) { ?>
					<td class="text-left" rowspan="6"><?php echo $text_shipping_address; ?> (<i class="fa fa-truck" style="padding: 3px;"></i> <?php echo $shipping_name; ?> - <?php echo $shipping_service_name; ?> )</td>
				<?php } else if ($delivery_type_id == 2) {?>
					<td class="text-left" rowspan="6"><?php echo $text_shipping_address; ?> (<i class="fa fa-truck" style="padding: 3px;"></i> <?php echo $delivery_type; ?> - <?php echo $pp_branch_name; ?> )</td>
				<?php } ?>
            <?php } ?>
			<td class="text-left" style="width: 25%;"><?php echo $column_quantity; ?></td>
			<td class="text-left" style="width: 25%;"><?php echo $column_shipping; ?></td>
          </tr>
        </thead>
        <tbody>
		
			<tr>
				<?php if ($delivery_type_id == 1) { ?>
					<td class="text-left" rowspan="6" style="width:50%; text-align: left;"><?php echo $shipping_address; ?></td>
				<?php } else if ($delivery_type_id == 2) { ?>
					<td class="text-left" rowspan="6" style="width:50%; text-align: left;"><?php echo $address_pp; ?></td>
				<?php } ?>
				  <td class="text-right" ><?php echo $qty; ?> (<?php echo sprintf('%0.2f', $total_weight); ?> <?php echo $weight; ?>)</td>
				<td class="text-right" ><?php echo $shipping_price; ?></td>
			</tr>
			<tr>
				<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" >Terima Sebagian</td>
				<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" ><?php echo $column_insurance?></td>
			</tr>
			<tr>
				<td class="text-left" >Tidak</td>
				<td class="text-left" ><?php echo $shipping_insurance; ?></td>
			</tr>
		
        </tbody>
      </table>
	  
	  <div class="table-responsive">
		<table class="table table-bordered table-hover list">
			<thead>
				<tr>
				<?php if($order_status_id == 21){ ?>
					<td colspan="5"><i class="fa fa-th-list" style="padding: 3px;"></i><?php echo $column_product_list; ?></td>
				<?php } else { ?>
					<td colspan="4"><i class="fa fa-th-list" style="padding: 3px;"></i><?php echo $column_product_list; ?></td>
				<?php } ?>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($products as $product) { ?>
				<tr>
					<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail"/></td>
					<td style="width: 40%; text-align: left;"><a
                            href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
					<td style="width: 25%; text-align: left; height:10px;" class="bold"><?php echo $column_remarks; ?></td>
					<td style="width: 25%; text-align: left; height:10px;" class="bold"><?php echo $column_price; ?></td>
					<?php if($order_status_id == 21){ ?>
					<?php if($product['is_complete_return'] == 3 || $product['is_complete_return'] == null) { ?>
					<td class="text-right" style="white-space: nowrap;" rowspan="2">
						<a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a>
					</td>
					<?php } else { ?>
					<td class="text-right" style="white-space: nowrap;" rowspan="2">
						<a data-toggle="tooltip" title="<?php echo $text_complete_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a>
					</td>
					<?php } ?>
					
					<?php } ?>
				</tr>
				<tr>
					<td style="text-align: left;"><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?></td>
					<td style="text-align: left;">-</td>
					<td style="text-align: left;"><?php echo $product['total']; ?></td>
					
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>
			<!--Total-->
            <tr>
				<td colspan="3" class="bold" style="font-size: 16px; text-align: center;"><?php echo $column_total; ?></td>
				<td colspan="2" class="bold" style="font-size: 16px;"><?php echo $total_price; ?></td>
            </tr>
          </tfoot>
		</table>
	  </div>
      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
</div>
</div>
</body>
</html>