<?php echo $header; ?>
<div id="container" class="container j-container register-seller">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="alert alert-danger warning main"></div>

  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $ms_account_register_seller; ?></h1>
      <?php echo $content_top; ?>
      <div class="alert alert-danger information"><?php echo $text_account_already; ?></div>
	  <br>
<div class="content">
      <form id="seller-form" class="form-horizontal">
        <fieldset id="account">
		<h2 class="secondary-title"><?php echo $ms_account_register_details; ?></h2>
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="seller[firstname]" placeholder="<?php echo $entry_firstname; ?>" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="seller[lastname]" placeholder="<?php echo $entry_lastname; ?>" class="form-control" />
            </div>
          </div>

		  <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_nickname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="seller[nickname]" placeholder="<?php echo $ms_account_sellerinfo_nickname_note; ?>" class="form-control" />
            </div>
          </div>
		  
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="email" name="seller[email]" placeholder="<?php echo $entry_email; ?>" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
              <input type="text" name="seller[telephone]" placeholder="<?php echo $entry_telephone; ?>" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-dob"><?php echo $entry_dob; ?></label>
            <div class="col-sm-10">
			<div class="input-group date">
                <input type="text" name="seller[dob]" placeholder="<?php echo $entry_dob; ?>" data-date-format="YYYY-MM-DD" data-date-maxdate="<?php echo date("Y-m-d",strtotime('-15 years'))?>" data-date-format="YYYY-MM-DD" data-date-defaultDate="<?php echo date("Y-m-d",strtotime('-15 years'))?>" id="input-dob" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="seller[password]" placeholder="<?php echo $entry_password; ?>" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="seller[password_confirm]" placeholder="<?php echo $entry_confirm; ?>" class="form-control" />
            </div>
          </div>
        </fieldset>
		<div class="buttons">
          <div class="pull-right">
		  <?php if (isset($seller_terms)) { ?>
			<?php echo $seller_terms; ?>
			<input type="checkbox" name="seller[terms]" value="1" />
			<?php } ?>
            &nbsp;
            <a class="btn btn-primary button" id="ms-submit-button" value="<?php echo $button_continue; ?>"><span><?php echo $button_continue; ?></span></a>
          </div>
        </div>
      </form>
</div>
      <?php echo $content_bottom; ?></div>
   </div>
</div>

<script>
$('.date').datetimepicker({
	pickTime: false
});
	var msGlobals = {
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>'
	};
</script>
<?php echo $footer; ?>