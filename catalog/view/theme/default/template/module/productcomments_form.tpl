	<div class="panel panel-default">
			<div class="panel-collapse collapse in">
			  <div class="panel-body">
			  <div class="content-comments">
			  <?php if(!empty($comments)){ ?>
			  <?php foreach ($comments['comments'] as $q) { ?>
				<div class="content">
					<div class="comment-header">
						<span class="comment-name">
							<h4>
							<?php echo htmlspecialchars($q['name']); ?>
							<?php if((int)$q['customer_id']==(int)$q['parent_id']){ ?><span class="badge green"><?php echo $pc_text_customer; ?></span><?php } else { ?><span class="badge"><?php echo $pc_text_seller; ?></span><?php }?>
							<small class="muted"><i><?php echo date('d/m/Y',$q['create_time']); ?></i></small>
							</h4>
						</span>
					</div>
					<div class="comment-content">
						<?php echo nl2br($q['comment']); ?>
					</div>
				</div>
				<hr>
				<?php } ?>
				</br>
				<?php if((int)$comments['total']>2) { ?><input type="hidden" name="pcIdView" value="<?php echo $parent_id; ?>"><a class="pcViewBtn icon-only"><span><?php echo $pc_text_more; ?></span><i style="margin-left: 5px; font-size: 16px" data-icon="&#xe1b0"></i></a><?php }?>
				</br>
				<?php } ?>
				</div>
				<div class="comment-title"></div>
				</br>
				<div class="comment-form">
				<form class="pcForm">
					<input type="hidden" name="pcId" value="<?php echo $parent_id; ?>">
					<textarea class="pcText" name="pcText" cols="40" rows="8" style="width: 100%;" <?php if ($pcconf_maxlen > 0) echo "maxlength='$pcconf_maxlen'" ?> placeholder="<?php echo $text_comment; ?>"></textarea>
				 </form>
				</div>
				</br>
				<div class="right-side"><a id="pcSubmitBtn" class="btn btn-default button pcSubmitBtn"><span><?php echo $button_continue; ?></span></a></div>
			  </div>
			</div>
		  </div>

