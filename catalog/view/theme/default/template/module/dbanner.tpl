<?php if($title) { ?>
  <h3><?php echo $title; ?></h3>
<?php } ?>
<div class="row dbanner" style="<?php echo $margin; ?>">
   <?php for($i = 1; $i <= $maxRow; $i++) { ?>
      <?php if (!isset($widgets[$i])) { ?>
         <div class='row col-sm-12' style='height: <?php echo $defaultHeight; ?>px;'></div>
      <?php } else { ?>
         <div class="row col-sm-12" style="height: <?php echo $defaultHeight; ?>px">
         <?php for($j = 1; $j <= 12; $j++) { ?>
            <?php if(isset($widgets[$i][$j])) { ?>
               <?php $w = $widgets[$i][$j]; ?>
               <div class="<?php echo $w['class']; ?>" style="padding: <?php echo $padding; ?>">
                  <div class="dbanner-inner-box lazy animated <?php echo $effect; ?>" style="<?php echo $w['background_style']; ?>" data-original="<?php echo $w['image'] ?>">
                    <div class="dbanner-inner-box-url" style="min-height: <?php echo $w['styleHeight'] . 'px'; ?>">
                      <a class="dbanner-inner-box-text" href="<?php echo $w['link']; ?>">
                        <?php if ($w['title'] != null) { ?>
                          <span><?php echo $w['title']; ?></span>
                        <?php } ?>
                      </a>
                    </div>
                  </div>
               </div>
            <?php } ?>
         <?php } ?>
         </div>
      <?php } ?>
   <?php } ?>
</div>
<style type="text/css">

  .dbanner-inner-box {
     background-size: cover;
  }
  .dbanner-inner-box-url {
    display: table;    
    text-align: center;
    width: 100%;
  }
  .dbanner-inner-box-text {
    display: table-cell;
    vertical-align: middle;
    width: 100%;
    text-align: center;
    position: relative;
    z-index: 10;
  }
  .dbanner-inner-box-text span {
    padding: 10px;
    font-size: 20px;
    color: #fff;
  }
  .dbanner-inner-box-text:hover {
    background: rgba(0,0,0,0.5);
  }
  .row.dbanner, .row.dbanner .row {
    margin: 0;
  }
  .row.dbanner .row { 
    padding: 0;
  }
  .animated {
    -webkit-animation-fill-mode: none;
    animation-fill-mode: none;
  }
</style>
<script type="text/javascript">
$(function() {
    $("div.lazy").lazyload({
      effect: "fadeIn"
    });
});
</script>