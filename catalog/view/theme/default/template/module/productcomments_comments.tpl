<?php if (!empty($pcComments)) {?>
	  <?php foreach(array_keys($pcComments) as $key) { ?>
		<?php $comment=$pcComments[$key];?>
		<div class="panel panel-default">
			<div class="panel-collapse collapse in">
			  <div class="panel-body">
			  <div class="content-comments">
			  <?php foreach ($pcComments[$key]['comments'] as $q) { ?>
				<div class="content">
					<div class="comment-header">
						<span class="comment-name">
							<h4>
							<?php echo htmlspecialchars($q['name']); ?>
							<?php if((int)$q['customer_id']==(int)$q['parent_id']){ ?><span class="badge green"><?php echo $pc_text_customer; ?></span><?php } else { ?><span class="badge"><?php echo $pc_text_seller; ?></span><?php }?>
							<small class="muted"><i><?php echo date('d/m/Y',$q['create_time']); ?></i></small>
							</h4>
						</span>
					</div>
					<div class="comment-content">
						<?php echo nl2br($q['comment']); ?>
					</div>
				</div>
				<hr>
				<?php } ?>
				</br>
				<?php if((int)$pcComments[$key]['total']>2) { ?><input type="hidden" name="pcIdView" value="<?php echo $key; ?>"><a class="pcViewBtn icon-only"><span><?php echo $pc_text_more; ?></span><i style="margin-left: 5px; font-size: 16px" data-icon="&#xe1b0"></i></a><?php }?>
			  </div>
			  <div class="comment-title"></div>
			</div>
		  </div>
		  </div>
	 <?php } ?>
	 <div class="pagination"><?php echo $pagination; ?></div>
	 <?php } else { ?>
		<div class="content">
		<div class="panel panel-default">
		  <div class="panel-heading">
		  <h4 class="panel-title">
				<a>
				  <?php echo $pc_no_comments_yet; ?>
				</a>
			  </h4>
		</div>
		</div>
		</div>
	<?php } ?>
<script type="text/javascript">
	var pc_product_id = <?php echo $product_id; ?>;
	var pc_wait = '<?php echo $pc_wait; ?>';
	var pc_seller = '<?php echo $pc_text_seller; ?>';
	var pc_customer = '<?php echo $pc_text_customer; ?>';
	var pc_text_view = '<?php echo $pc_text_more; ?>';
</script>