<style>
.col:not(:first-child),.col:not(:last-child) {
  padding-right:10px;
  padding-left:10px;
}
.col{
  padding-bottom:10px;
}
.category-content{
	width:100%;
	border:1px solid rgba(0,0,0,.1);
	background:#fff;
}
.product-content{
	width: 40px;
    height: 40px;
    border: 1px solid #fff;
    background: #fff;
    display: block;
    float: left;
    margin: 5px;
	-moz-box-shadow:    1px 1px 1px 1px rgba(0,0,0,.3);
	-webkit-box-shadow: 1px 1px 1px 1px rgba(0,0,0,.3);
	box-shadow:         1px 1px 1px 1px rgba(0,0,0,.3);
}
.detail-category{
	padding:5px;
	background:rgba(0,0,0,.1);
}
.text_start_price{
	font-size:10px;
}
.start_price{
	font-size:14px;
	font-weight:bold;
}
</style>
<div class="box">
<div class="box-heading">
<?php echo $heading_title; ?>
</div>
<div class="row">
  <?php foreach ($featured_categories as $featured_category) { ?>
	<div class="col-md-4 col" style="padding:10px;">
		<div class="category-content">
			<div class="image-category" style="width:100%">
				<a href="<?php echo $featured_category['href']; ?>"><img src="<?php echo $featured_category['thumb']; ?>" alt="" title="" style="width:100%" /></a>
			</div>
			<div class="detail-category" style="width:100%">
				<div class="row">
					<div class="col-md-6">
						<div class="">
							<h5><a href="<?php echo $featured_category['href']; ?>"><?php echo $featured_category['category']; ?></a></h5>
						</div>
						<div class="">
							<span class="text_start_price"><?php echo $text_start_price; ?></span>
							<span class="start_price"><?php echo $featured_category['start_price']; ?></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="pull-right">
							<?php foreach ($featured_category['products'] as $product) { ?>
							<span class="product-content">
								<a href="<?php echo $product['href']; ?>" ><img src="<?php echo $product['thumb']; ?>" alt="" title="" style="width:40px" /></a>
							</span>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <?php } ?>
</div>
</div>
