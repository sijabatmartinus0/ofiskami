
<?php if($category_name == "17an") { ?>
  <style>
button.accordion {
background-color: #9A0034;
    color: white;
    cursor: pointer;
    padding: 10px;
    width: 100%;
    border: none;
    text-align: center;
    outline: none;
    font-size: 18px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
   background-color: #9A0034;
  
}

button.accordion:after {
    content: '\002B';
    color: white;

    font-weight: bold;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2212";
}

div.panel1 {
    padding: 0 18px;
  background: url("http://www.ofiskita.com/catalog/view/theme/ofisindi/image/custom/pap.jpg")no-repeat scroll 0% 0% / 100% 100% transparent;
    color :white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    border: none;
    font-size: 15px;
    font-weight: 400;
    line-height: 1.5;
}


img.sticky-icon {
    height: 35px;
    width: 35px;
    margin:2.5px;
    vertical-align: middle;
    padding: 2px;
}
.sticky label {
    vertical-align: middle;
} 
.container-sticky{ 
    margin:2px 
}
</style>

<!--<h2>Accordion with symbols</h2>
<p>In this example we have added a "plus" sign to each button. When the user clicks on the button, the "plus" sign is replaced with a "minus" sign.</p>
-->
<button class="accordion">BACA SYARAT DAN KETENTUAN</button>
<div class="panel1">
<ol start="1" type="1">
  <p>
    <li>Promo Merdeka, Bayar 72%  Saja berlangsung  hingga 31 Agustus 2017 </li>
    <li>Gratis biaya kirim dengan kode voucher : MERDEKA72</li>
    <li>Gratis biaya kirim untuk produk yang berpartisipasi dalam promo Bayar 72% Saja, dan total transaksi minimum Rp.100.000,-</li>
    <li>Produk promo yang telah dibeli tidak dapat dikembalikan, kecuali jika ada kerusakan saat proses pengiriman..</li>
    <li>Kuantitas Produk yang dijual dalam Promo merdeka terbatas.</li>
    <li>Segera lakukan konfirmasi pembayaran dalam waktu 4 jam, atau order akan otomatis expired.</li>
    <li>Promo produk dalam Bayar 72% Saja dapat digabungkan dengan kode voucher lain..</li>
    <li>Produk yang dibeli akan dikirimkan langsung oleh pihak merchant.</li>
    <li>Customer wajib melakukan konfirmasi pembayaran secepatnya	.</li>
    <li>Pihak Merchant berhak membatalkan pesanan jika stock produk kosong.</li>
    <li>Pihak Ofiskita berhak membatalkan pesanan jika dirasa terjadi pelanggaran dalam proses pembelian.</li></p>
</ol>

</div>


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>
<?php }
?>
<?php if($category_name != "17an") { ?>
<span style="float: right;">
    <a href="<?php echo $category_url."?sort=ps.spec&order=DESC"; ?>">
        <h4 style="text-align: right; border-bottom: 1px solid #9a0034;">
            View all
        </h4>

        <!-- <input type="submit" class="button" value="view all" style="" />  -->
    </a>


</span>
<br>
<br>
<?php } ?>
<div class="row" style=" margin-top: 10px;">

    <?php foreach ($products as $product) { ?>


    <div class="col-md-3 " style="height: 400px; position: auto;">

        <div class="product-thumb transition">
            <!--<div class="image" style="">
                <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                </a>
            </div>-->
            <div class="image">
                <!--<a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                </a>-->
		<?php if ($product['label'] == 1 ) { ?>
                <div style="position:absolute;z-index:2;top:30%;width:70%;height:50%;border-radius:50%;line-height:50px;color:#ffffff;text-align:center;font-size:12px;">
                    <?php if ($logo) { ?>
                    <img src="<?php echo $logo; ?>" class="img-responsive" style="border-radius: 0%;" />
                    <?php } else { ?>
                    <h1></h1>
                    <?php } ?>
                </div>

                <?php } ?>
                <?php if ($product['special'] && $cosyone_percentage_sale_badge == 'enabled') { ?>
                <div class="sale_badge">-<?php echo $product['sales_percantage']; ?>%</div>
                <?php } ?>
                <?php if ($product['thumb_hover'] && $cosyone_rollover_effect == 'enabled') { ?>
                <div class="image_hover">
                    <a href="<?php echo $product['href']; ?>">
                        <img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" />
                    </a>
                </div>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                <?php } elseif ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                <?php } ?>
            </div>
            <div class="caption" style="height :120px;">
                <!-- <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4> -->
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

                <?php if ($product['rating']) { ?>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                </p>
                <?php } ?>
            </div>

            <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-star"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>


            </div>


            <!-- Modal -->
        </div>
    </div>
    <?php } ?>


</div>


<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="min-width: 80%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-cart-label"><?php echo $text_modal_cart ?></h4>
            </div>
            <div class="modal-body">
                <div id="modal-cart-content">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary button btn-block" id="md-button-submit" style="display: block;width: 100%;"><i class="fa fa-shopping-cart"></i> <?php echo $button_modal_cart ?></button>
            </div>
        </div>
    </div>
</div>
