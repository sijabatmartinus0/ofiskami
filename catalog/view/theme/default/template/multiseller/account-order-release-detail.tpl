<?php foreach($header_shippings as $header_shipping){ ?>
				<table class="table table-bordered table-hover list">
				<thead>
					<tr>
						<td class="text-left" rowspan="6" colspan="2"><?php echo $column_destination; ?></td>
						<td class="text-left" style="width: 25%;"><?php echo $column_quantity; ?></td>
						<td class="text-left" style="width: 25%;"><?php echo $column_shipping_price; ?></td>
				  </tr>
				</thead>
				<tbody>
				
					<tr>
						<td class="text-left" rowspan="6" colspan="2" style="width:50%; text-align: left;">
						<?php if($header_shipping['delivery_type_id'] == 1) { ?>
							<?php echo $header_shipping['shipping_address']; ?>
						<?php } else if($header_shipping['delivery_type_id'] == 2) { ?>
							<?php echo $header_shipping['address_pp']; ?>
						<?php } ?>
						</td>
						<td class="text-right" style="text-align: left;"><?php echo $header_shipping['total_qty']; ?> (<?php echo sprintf('%0.2f', $header_shipping['total_weight']); ?> <?php echo $header_shipping['weight']; ?>)</td>
						<td class="text-right" style="text-align: left;">
						<?php if($header_shipping['delivery_type_id'] == 1) { ?>
						<span class="highlight bold"><?php echo $header_shipping['shipping_name']; ?> - <?php echo $header_shipping['shipping_service_name']; ?></span>
						<?php } else if($header_shipping['delivery_type_id'] == 2){ ?>
						<span class="highlight bold"><?php echo $header_shipping['delivery_type']; ?> - <?php echo $header_shipping['pp_branch_name']; ?></span>
						<?php } ?>
						<br class="wide"/>
						<br class="wide"/>
						<?php echo $header_shipping['shipping_price']; ?>
						</td>
					</tr>
					<tr>
						<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" >Terima Sebagian</td>
						<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" ><?php echo $column_insurance?></td>
					</tr>
					<tr>
						<td class="text-left" style="text-align: left;">Tidak</td>
						<td class="text-left" style="text-align: left;"><?php echo $header_shipping['shipping_insurance']; ?></td>
					</tr>
				
				</tbody>
				
				
				<!-- Product -->
				<div class="table-responsive">
					
						<tbody>
						<?php 
							foreach ($products as $product) { 
								if ($product['order_detail_id'] == $header_shipping['order_detail_id']){
							?>
							<tr>
								<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['product_name']; ?>" title="<?php echo $product['product_name']; ?>" class="img-thumbnail"/></td>
								<td style="width: 40%; text-align: left;">
									<a href="<?php echo $product['href']; ?>"><?php echo $product['product_name']; ?></a>
								</td>
								<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_remarks; ?></td>
								<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_price; ?></td>
							</tr>
							<tr>
								<td style="text-align: left;"><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?></td>
								<td style="text-align: left;">-</td>
								<td style="text-align: left;"><?php echo $product['total']; ?></td>
							</tr>
						<?php 
								}
							}
						?>
						<tr>
							<td colspan="4">
							<div style="float:left; display:inline">
								<?php echo $column_payment_method; ?><?php echo $header_shipping['payment_method']; ?>
								(<?php echo $header_shipping['total_price']; ?>)
							</div>
							<div style="float:right; display:inline; font-size: 16px;" class="bold">
								<a data-toggle="tooltip" title="<?php echo $text_tooltip; ?>" style="color: black !important;"><i class="fa fa-question-circle"></i></a> <?php echo $column_total; ?><a class="color-response"><?php echo $header_shipping['total_price_invoice']; ?></a>
							</div>
							</td>
						</tr>
						</tbody>
				 </div>
				 </table>	
				  <?php } ?>