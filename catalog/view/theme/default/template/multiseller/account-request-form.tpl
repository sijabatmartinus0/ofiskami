<?php echo $header; ?>
<style>
.chart {
  position: relative;
  display: inline-block;
  width: 150px;
  height: 150px;
  margin:auto;
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
}
.chart canvas {
  position: absolute;
  top: 0;
  left: 0;
}
.percent {
    display: inline-block;
    line-height: 150px;
    z-index: 2;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					<li>
					<a href="<?php echo $this->url->link('seller/account-request-product', '', 'SSL'); ?>">
						<?php echo $ms_account_request_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-create-request"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $text_create_request; ?></h1>
		<form id="form_add_request" method="post">
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $text_kiosk; ?></label>
			<div class="col-sm-4">
				<select class="form-control" value="" id="kiosk_id" name="kiosk_id">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($kiosks as $kiosk){ ?>
						<option value="<?php echo $kiosk['pp_branch_id']; ?>"><?php echo $kiosk['pp_branch_name']; ?></option>
					<?php } ?>
				</select>
				<div class="error"></div>
			</div>
		</div>
		<br/>
		<br/>
		<br/>
		<p class="error" id="error_request"></p>
		<div class="error"></div>
		<table class="list table table-bordered table-hover">
				<thead>
				<tr>
					<td style="width:400px;"><span class="required">*</span><?php echo $ms_account_products_product; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $column_quantity; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $column_price; ?></td>
					<td style="width:100px;"><?php echo $ms_account_staff_action; ?></td>
				</tr>
				</thead>

				<tbody>

				<!-- sample row -->
				<tr class="ffSample">
					<td style="width:400px; vertical-align: top;"><input style="width:400px;" type="text" class="form-control inline" name="product_request[0][product]" value="" size="2" /><input style="width:400px;" type="hidden" class="form-control inline" name="product_request[0][product_id]" value="" size="2" /></td>
					<td style="width:150px; vertical-align: top;"><input class="number" style="width:150px;" type="text" class="form-control inline" name="product_request[0][quantity]" value="" /><p class="error_quantity"></p></td>
					<td style="width:150px; vertical-align: top;">
						<input style="width:150px;" class="number" type="text" class="form-control inline" name="product_request[0][price]" value="" />
					</td>
					<td style="width:100px; vertical-align: top;"><a class="btn btn-default button delete" title="<?php echo $ms_delete; ?>"><i class="fa fa-trash"></i></a></td>
				</tr>
				<tr>
					<td style="width:400px; vertical-align: top;"><input style="width:400px;" type="text" class="form-control inline" name="product_request[1][product]" value="" size="2" /><input style="width:400px;" type="hidden" class="form-control inline" name="product_request[1][product_id]" value="" size="2" /></td>
					<td style="width:150px; vertical-align: top;"><input class="number" style="width:150px;" type="text" class="form-control inline" name="product_request[1][quantity]" value="" /><p class="error_quantity"></p></td>
					<td style="width:150px; vertical-align: top;">
						<input style="width:150px;" class="number" type="text" class="form-control inline" name="product_request[1][price]" value="" />
					</td>
					<td style="width:100px; vertical-align: top;"></td>
				</tr>
				</tbody>

				<tfoot>
				<tr>
				<td colspan="5" class="text-center"><a class="btn btn-primary ffClone pull-right"><i class="fa fa-plus"></i></a></td>
				</tr>
				</tfoot>
			</table>
		</form>
		<div class="buttons">
			<div class="pull-right">
			<a href="<?php echo $link_cancel; ?>" class="btn btn-default button"><span><?php echo $ms_button_cancel; ?></span></a>
			<a id="button-submit" class="btn btn-default button"><span><?php echo $ms_button_submit; ?></span></a>
			</div>
		</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<script type="text/javascript">
$(function() {
	$('body').delegate("a.delete", "click", function() {
		$(this).parents('tr').remove();
	});
	
	$('body').delegate("a.ffClone", "click", function() {
		var lastRow = $(this).closest('table').find('tbody tr:last input:last').attr('name');
		if (typeof lastRow == "undefined") {
			var newRowNum = 2;
		} else {
			var newRowNum = parseInt(lastRow.match(/[0-9]+/)) + 1;
		}

		var newRow = $(this).closest('table').find('tbody tr.ffSample').clone();
		newRow.find('input,select').attr('name', function(i,name) {
			return name.replace('[0]','[' + newRowNum + ']');
		});
		
		$(this).closest('table').find('tbody').append(newRow.removeAttr('class'));
		
		$('input[name=\'product_request['+newRowNum+'][product]\']').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: $('base').attr('href') + 'index.php?route=seller/account-request-product/autoCompleteProduct&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['product_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'product_request['+newRowNum+'][product]\']').val(item.label);
				$('input[name=\'product_request['+newRowNum+'][product_id]\']').val(item.value);
			}
		});
	});
	
	$("#button-submit").on('click', function(){
		$('#kiosk_id').siblings('.error').html("");
		$('#error_request').siblings('.error').html("");
		$('.error_quantity').html("");
		
		$.post("<?php echo $action_add_request; ?>", $('#form_add_request input[type=\'text\'], #form_add_request input[type=\'hidden\'], #form_add_request select'), function(data)
		{
			if(data['error']){
				
				if(data['error']['request']){
					$('#error_request').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['request']+"</strong>");
				}
				if(data['error']['kiosk_id']){
					$('#kiosk_id').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['kiosk_id']+"</strong>");
				}
				
				var arr = Object.keys(data['error']['minimum']).map(function (key) {return data['error']['minimum'][key]});
				for(x=1; x <= arr.length; x++){
					if(data['error']['minimum'][x]){
						$('input[name=\'product_request['+x+'][quantity]\']').siblings('.error_quantity').html("<br><span style='color:#FF0000; font-size: 12px;'>"+data['error']['minimum'][x]+"</span>");
					}else{
						$('input[name=\'product_request['+x+'][quantity]\']').siblings('.error_quantity').html("");
					}
				}
			}else if(data['url']){
				location=data['url'];
			}
		});
	});
	
	$('input[name=\'product_request[1][product]\']').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: $('base').attr('href') + 'index.php?route=seller/account-request-product/autoCompleteProduct&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['product_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'product_request[1][product]\']').val(item.label);
				$('input[name=\'product_request[1][product_id]\']').val(item.value);
			}
		});
});
</script>
<?php echo $footer; ?>
	