<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-transaction">
      <h1 class="heading-title"><?php echo $ms_account_withdraw_heading; ?></h1>
      <?php echo $content_top; ?>
	  
	    <p><?php echo $ms_account_withdraw_balance; ?> <b class="label-number"><?php echo $ms_account_balance_formatted; ?></b> <span style="color: gray"><?php echo $ms_account_reserved_formatted; ?></span></p>
		<p><?php echo $ms_account_withdraw_deposit_fee; ?> <b class="label-number"><?php echo $deposit_fee_formatted; ?></b></p>
		<p><?php echo $ms_account_withdraw_balance_available; ?> <b class="label-number"><?php echo $balance_available_formatted; ?></b></p>
		<p><?php echo $ms_account_withdraw_minimum; ?> <b class="label-number"><?php echo $this->currency->format($this->config->get('msconf_minimum_withdrawal_amount'),$this->config->get('config_currency')); ?></b></p>
		
		<?php if ($balance_available <= 0) { ?>
			<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i><?php echo $ms_account_withdraw_no_funds; ?></div>
		<?php } else if (!$withdrawal_minimum_reached) { ?>
			<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i><?php echo $ms_account_withdraw_minimum_not_reached; ?></div>
		<?php } ?>
		
	<form id="ms-withdrawal" class="ms-form">
    <div class="content">
			<?php if (!$withdrawal_minimum_reached || $balance_available <= 0) { ?>
			<div class="ms-overlay"></div>
			<?php } ?>
			
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $ms_account_withdraw_amount; ?></label>
					<div class="col-sm-10">
						<?php if ($msconf_allow_partial_withdrawal) { ?>
						<p>
							<input type="radio" class="form-inline" name="withdraw_all" value="0" checked="checked" />
							<input type="text" class="form-control number" style="width: 100px; display: inline" name="withdraw_amount" value="<?php echo $this->currency->format($this->config->get('msconf_minimum_withdrawal_amount'),$this->config->get('config_currency'), '', FALSE); ?>" />
							<?php echo $currency_code; ?>
						</p>
						<?php } ?>
						
						<p>
							<input type="radio" name="withdraw_all" value="1" <?php if (!$msconf_allow_partial_withdrawal) { ?>checked="checked"<?php } ?> />
							<span><?php echo $ms_account_withdraw_all; ?> (<?php echo $balance_available_formatted; ?>)</span>
						</p>
						<p class="ms-note"><?php echo $ms_account_withdraw_amount_note; ?></p>
						<p class="error" id="error_withdraw_amount"></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $ms_account_withdraw_method; ?></label>
					<div class="col-sm-10">
						<p>
							<input type="radio" name="withdraw_method" value="transfer" checked="checked" />
							<span><?php echo $ms_account_withdraw_method_transfer; ?></span>
						</p>
						<p class="ms-note"><?php echo $ms_account_withdraw_method_note; ?></p>
						<p class="error" id="error_withdraw_method"></p>
					</div>
				</div>
			</table>
		</div>
		

		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
			<?php if ($withdrawal_minimum_reached) { ?>
			<div class="pull-right"><a class="btn btn-primary button" id="ms-submit-request"><span><?php echo $ms_button_submit_request; ?></span></a></div>
			<?php } ?>
		</div>
	</form>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<?php echo $footer; ?>