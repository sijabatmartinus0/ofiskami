<?php echo $header; ?>
<style>
.input-group {
    position: relative;
    display: table;
    border-collapse: separate;
}
.input-group-addon:first-child {
    border-right: 0;
}
.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn-group:not(:last-child)>.btn, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: 1px solid #ccc;
	width: 75px !important;
}
.input-group-addon, .input-group-btn {
    width: 1%;
    white-space: nowrap;
    vertical-align: middle;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
}
#tab-data input{
	width:400px;
}
#tab-data select{
	width:400px;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading; ?></h1>
      <?php echo $content_top; ?>
<div class="content">
      <form id="staff-form" class="form-horizontal">
        <fieldset id="account">
		<h2 class="secondary-title"><?php echo $ms_account_register_details; ?></h2>
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
				<input type="hidden" name="staff_id" value="<?php echo $staff_id;?>" />
              <input type="text" name="staff[firstname]" placeholder="<?php echo $entry_firstname; ?>" class="form-control" value="<?php echo $staff['firstname'];?>" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="staff[lastname]" placeholder="<?php echo $entry_lastname; ?>" class="form-control" value="<?php echo $staff['lastname'];?>"/>
            </div>
          </div>

		  <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_nickname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="staff[nickname]" placeholder="<?php echo $ms_account_sellerinfo_nickname_note; ?>" class="form-control" value="<?php echo $staff['nickname'];?>" />
            </div>
          </div>
		  
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="email" name="staff[email]" placeholder="<?php echo $entry_email; ?>" class="form-control" value="<?php echo $staff['email'];?>" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
              <input class="number form-control" type="text" name="staff[telephone]" placeholder="<?php echo $entry_telephone; ?>" class="form-control" value="<?php echo $staff['telephone'];?>" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-dob"><?php echo $entry_dob; ?></label>
            <div class="col-sm-10">
			<div class="input-group date">
                <input type="text" name="staff[dob]" placeholder="<?php echo $entry_dob; ?>" data-date-format="YYYY-MM-DD" data-date-maxdate="<?php echo date("Y-m-d",strtotime('-15 years'))?>" data-date-format="YYYY-MM-DD" data-date-defaultDate="<?php echo date("Y-m-d",strtotime('-15 years'))?>" id="input-dob" class="form-control" value="<?php echo $staff['dob'];?>" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
			</div>
		<div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $ms_account_staff_role; ?></label>
            <div class="col-sm-10">
              <div>
                <?php foreach ($roles as $role) { ?>
                <div class="checkbox" style="width:100%;display:block">
                  <label style="width:100%;display:inline-block;min-height: 17px;padding-left: 20px;margin-bottom: 0;font-weight: normal;cursor: pointer;">
                    <?php if (in_array($role['role_id'], $staff['roles'])) { ?>
                    <input type="checkbox" name="staff[roles][]" value="<?php echo $role['role_id']; ?>" checked="checked" />
                    <?php echo $role['role']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="staff[roles][]" value="<?php echo $role['role_id']; ?>" />
                    <?php echo $role['role']; ?>
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $ms_account_staff_roles_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $ms_account_staff_roles_unselect_all; ?></a><p class="ms-note"><?php echo $ms_account_staff_roles_note; ?></p></div>		  
		  </div>
        </fieldset>
		<div class="buttons">
          <div class="pull-right">
            <a class="btn btn-primary button" id="ms-submit-button" value="<?php echo $button_continue; ?>"><span><?php echo $button_continue; ?></span></a>
          </div>
        </div>
      </form>
</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<?php $timestamp = time(); ?>
<script>
$('.date').datetimepicker({
	pickTime: false
});
	var msGlobals = {
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>'
	};
	$("#ms-submit-button").click(function() {
			var button = $(this);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $('base').attr('href') + 'index.php?route=seller/account-staff/jxSubmitStaff',
            data: $("form#staff-form").serialize(),
            beforeSend: function() {
                button.hide();
                $('p.error').remove();
                $('.warning.main').hide();
                $('.form-group').removeClass("has-error");
            },
            complete: function(jqXHR, textStatus) {
				button.show();
                if (textStatus != 'success') {
                    button.show();
                    $(".warning.main").text(msGlobals.formError).show();
                    window.scrollTo(0,0);
                }
            },
            success: function(jsonData) {
                if (!jQuery.isEmptyObject(jsonData.errors)) {
                    $('#ms-submit-button').show().prev('span.wait').remove();
                    $('.error').text('');

                    for (error in jsonData.errors) {
                        if ($('#error_' + error).length > 0) {
                            $('#error_' + error).text(jsonData.errors[error]);
                            $('#error_' + error).parents('.form-group').addClass('has-error');
                        } else if ($('[name="'+error+'"]').length > 0) {
                            $('[name="' + error + '"]').parents('.form-group').addClass('has-error');
                            $('[name="' + error + '"]').parents('div:first').append('<p class="error">' + jsonData.errors[error] + '</p>');
                        } else $(".warning.main").append("<p>" + jsonData.errors[error] + "</p>").show();
                    }
                    window.scrollTo(0,0);
                } else {
                    window.location = jsonData.redirect;
                }
            }
        });
		});
		$("body").delegate(".number", "keypress", function(e){
		 var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		  }
	});
</script>
<?php echo $footer; ?>