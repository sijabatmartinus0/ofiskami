<?php echo $header; ?>
<style>
.chart {
  position: relative;
  display: inline-block;
  width: 150px;
  height: 150px;
  margin:auto;
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
}
.chart canvas {
  position: absolute;
  top: 0;
  left: 0;
}
.percent {
    display: inline-block;
    line-height: 150px;
    z-index: 2;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if (isset($success) && ($success)) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
				<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					<li>
					<a href="<?php echo $this->url->link('seller/account-request-product', '', 'SSL'); ?>">
						<?php echo $ms_account_request_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-request-product"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $ms_list_request_product; ?></h1>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $text_kiosk; ?></label>
			<div class="col-sm-4">
				<select class="form-control" value="" id="kiosk_id" name="kiosk_id" >
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($kiosks as $kiosk){ ?>
						<option value="<?php echo $kiosk['pp_branch_id']; ?>"><?php echo $kiosk['pp_branch_name']; ?></option>
					<?php } ?>
				</select>
			</div>
				<div class="pull-right"><a href="<?php echo $link_create; ?>" class="btn btn-default button"><span><?php echo $button_create_request; ?></span></a></div>
		</div>
		<br/>
		<br/>
		<br/>
		<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
		<div id="list-request">
		
		</div>
      <?php echo $content_bottom; ?></div>
    </div>
	
	<div class="modal fade" id="modal_edit_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h1><?php echo $text_edit_request; ?></h1>
				</div>
				<div class="modal-body" >
				<form action="<?php echo $action_edit_request; ?>" id="form_edit_request" method="post">
					<input type="hidden" id="input_request_id" name="input_request_id">
					<label><?php echo $column_delivery_order; ?></label><br>
					<input type="text" name="input_delivery_order" id="input_delivery_order" placeholder="<?php echo $column_delivery_order; ?>" style="width: 550px;">
					<div class="error"></div>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" style="font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;"><?php echo $button_cancel; ?></button>
					<button type="button" class="btn btn-primary button" style="line-height:33px;font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;" id="button_edit_request"><?php echo $button_save; ?></button>

				</div>
				</form>
		</div>
	</div>
	</div>
	
</div>
<script type="text/javascript">
$(function(){
		$('.pcAttention').show();
		$('#list-request').load('<?php echo $list_request; ?>',function(){$('.pcAttention').hide();});
		
		$( document ).ajaxComplete(function() {
		$('.list-requests.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-request').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	}); 
	
	document.getElementById('kiosk_id').onchange = function () {
		$('.pcAttention').show();
		$('#list-request').load('<?php echo $search_request; ?>&kiosk_id=' +$('#kiosk_id').val(),function(){$('.pcAttention').hide();});
	}
	
	$( document ).ajaxComplete(function() {
		$(".btn.btn-primary.edit-request").on('click', function(){
			$('#input_request_id').val($(this).data('requestid'));
		});
	});
		
	
	$("#button_edit_request").on('click', function(){
		$.post("<?php echo $action_edit_request; ?>", $('#form_edit_request input[type=\'hidden\'], #form_edit_request input[type=\'text\']'), function(data)
		{
			if(data['error']){
				if(data['error']['edit_request']){
					$('#input_delivery_order').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['edit_request']+"</strong>");
				}
			}else if(data['url']){
				location=data['url'];
			}
		});
	});
});
</script>
<?php echo $footer; ?>
	