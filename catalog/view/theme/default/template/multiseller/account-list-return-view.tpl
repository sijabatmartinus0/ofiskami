<?php if ($returns) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-right bold"><?php echo $column_return_id; ?></td>
            <td class="text-right bold"><?php echo $column_order_id; ?></td>
            <td class="text-right bold"><?php echo $column_invoice; ?></td>
            <td class="text-left bold"><?php echo $column_customer; ?></td>
            <td class="text-left bold"><?php echo $column_product; ?></td>
            <td class="text-left bold"><?php echo $column_status; ?></td>
            <td class="text-left bold"><?php echo $column_date_added; ?></td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($returns as $return) { ?>
          <tr>
            <td class="text-right">#<?php echo $return['return_id']; ?></td>
			<td class="text-right">#<?php echo $return['order_id']; ?></td>
            <td class="text-right"><?php echo $return['invoice_no']; ?></td>
            <td class="text-left"><?php echo $return['name']; ?></td>
            <td class="text-left"><?php echo $return['product']; ?> (<?php echo $return['model']; ?>)</td>
            <td class="text-left"><?php echo $return['status']; ?></td>
            <td class="text-left"><?php echo $return['date_added']; ?></td>
            <td>
			<a href="<?php echo $return['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
			</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="list-returns pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
	   <p><?php echo $text_no_data_global; ?></p>
	  <?php } ?>