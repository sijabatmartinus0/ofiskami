<?php echo $header; ?>
<style>
.caption p{
  text-align:left;
  margin-left:10px;
}
</style>
<div class="container j-container ms-catalog-seller-products" style="max-width: 1280px;position: relative;
    margin: 0 auto;background-attachment: scroll;">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $ms_catalog_sellers_heading; ?></h1>
      <?php echo $content_top; ?>
      <?php if (isset($sellers) && $sellers) { ?>
<div class="product-filter">
    <div class="display"> 
    <a id="grid_view_icon"><i class="fa fa-th"></i></a><a id="list_view_icon"><i class="fa fa-list"></i></a>
    </div>
    <div class="sort"><?php echo $text_sort; ?>
      <select id="input-sort" onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
              
    </div>
  </div>
  <div id="main" class="product-<?php echo $cosyone_default_view; ?>">
  <div class="grid_holder">
     <?php foreach ($sellers as $seller) { ?><!--
    --><div class="item contrast_font product-layout">
       <div class="image">
      <a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" title="<?php echo $seller['nickname']; ?>" alt="<?php echo $seller['nickname']; ?>" /></a> 
        </div><!-- image ends -->
      <div class="information_wrapper">
      <div class="left">
      <h4><a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a></h4>

        <?php if ($seller['country']) { ?>
          <p class="country">
            <b><?php echo $ms_catalog_sellers_country; ?></b>
            <img class="country-flag" src="<?php echo $seller['country_flag']; ?>" alt="<?php echo $seller['country']; ?>" title="<?php echo $seller['country']; ?>" /> <span class="country-name"><?php echo $seller['country']; ?></span>
          </p>
        <?php } ?>

        <?php if ($seller['company']) { ?>
          <p class="company"><b><?php echo $ms_catalog_sellers_company; ?></b> <?php echo $seller['company']; ?></p>
        <?php } ?>

        <?php if ($seller['website']) { ?>
          <p class="website"><b><?php echo $ms_catalog_sellers_website; ?></b> <?php echo $seller['website']; ?></p>
        <?php } ?>

        <p class="totalsales"><b><?php echo $ms_catalog_sellers_totalsales; ?></b> <?php echo $seller['total_sales']; ?></p>
        <p class="totalproducts"><b><?php echo $ms_catalog_sellers_totalproducts; ?></b> <?php echo $seller['total_products']; ?></p>
        
        <p class="totalsales"><b><?php echo $ms_catalog_sellers_totalsales; ?></b> <?php echo $seller['total_sales']; ?></p>
        <p class="totalproducts"><b><?php echo $ms_catalog_sellers_totalproducts; ?></b> <?php echo $seller['total_products']; ?></p>
      </div>
      <div class="description main_font"><?php echo $seller['description']; ?></div>

      </div>
    </div><!--
    --><?php } ?>
    </div>
    </div>
  
  <div class="pagination_holder row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
  
      <?php } else { ?>
      <p><?php echo $ms_catalog_seller_products_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript">
$(function() {
  var pv = $.cookie('product_view');
  if (pv == 'g') {
    $('#main').removeClass();
    $('#main').addClass('product-grid');
    $('#list_view_icon').removeClass();
    $('#grid_view_icon').addClass('active');
  } else if (pv == 'l') {
    $('#main').removeClass();
    $('#main').addClass('product-list');
    $('#grid_view_icon').removeClass();
    $('#list_view_icon').addClass('active');
  } else {
    $('#<?php echo $cosyone_default_view?>_view_icon').addClass('active');
  }
});
$(document).ready(function() {
  $('#grid_view_icon').click(function() {
    $(this).addClass('active');
    $('#list_view_icon').removeClass();
    $('#main').fadeOut(300, function() {
      $(this).removeClass();
      $(this).addClass('product-grid').fadeIn(300);
      $.cookie('product_view', 'g');
    });
    return false;
  });
  $('#list_view_icon').click(function() {
    $(this).addClass('active');
    $('#grid_view_icon').removeClass();
    $('#main').fadeOut(300, function() {
      $(this).removeClass();
      $(this).addClass('product-list').fadeIn(300);
      $.cookie('product_view', 'l');
    });
    return false;
  });
});
</script>
<?php echo $footer; ?>