<?php echo $header; ?>
<style>
.note-popover .popover .popover-content .dropdown-menu, .note-toolbar .dropdown-menu {
    min-width: 90px;
}
ul.dropdown-menu {
	margin:0px !important;
}
.dropdown-menu {
    position: absolute !important;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    list-style: none;
    font-size: 12px;
    text-align: left;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 3px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    background-clip: padding-box;
}
.note-popover .popover .popover-content .note-color .dropdown-menu .btn-group .note-palette-title, .note-toolbar .note-color .dropdown-menu .btn-group .note-palette-title {
    margin: 2px 7px;
    font-size: 12px;
    text-align: center;
    border-bottom: 1px solid #eee;
}
.open > .dropdown-menu {
    display: block;
	position:absolute;
}
.note-toolbar>.btn-group {
    margin-top: 5px;
    margin-right: 5px;
    margin-left: 0;
}
.note-editor .note-toolbar {
    background-color: #f5f5f5;
    border-bottom: 1px solid #a9a9a9;
}
.note-popover .popover .popover-content, .note-toolbar {
    padding: 0 0 5px 5px;
    margin: 0;
}
.btn-toolbar .btn-group, .btn-toolbar .input-group {
    float: left;
}
.btn-group, .btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
}

.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
}
.btn-group > .btn:last-child:not(:first-child), .btn-group > .dropdown-toggle:not(:first-child) {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}
.btn-group > .btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
}
.btn-default {
    border-color: #FFFFFF;
}

.caret {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px solid;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
}
.btn .caret {
    margin-left: 0;
}
.btn-group > .btn:hover, .btn-group-vertical > .btn:hover, .btn-group > .btn:focus, .btn-group-vertical > .btn:focus, .btn-group > .btn:active, .btn-group-vertical > .btn:active, .btn-group > .btn.active, .btn-group-vertical > .btn.active {
    z-index: 2;
}
.btn-group .btn + .btn, .btn-group .btn + .btn-group, .btn-group .btn-group + .btn, .btn-group .btn-group + .btn-group {
    margin-left: -1px;
}
.btn-sm, .btn-group-sm > .btn {
    padding: 4px 9px;
    font-size: 11px;
    line-height: 1.5;
    border-radius: 2px;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if (isset($error_warning) && $error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
   <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

    <?php if (isset($statustext) && ($statustext)) { ?>
        <div class="alert alert-<?php echo $statusclass; ?>"><?php echo $statustext; ?></div>
    <?php } ?>

  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
  <?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $ms_account_sellerinfo_heading; ?></h1>
      <?php echo $content_top; ?>
      <div class="content">
      <form id="ms-sellerinfo" class="ms-form form-horizontal">
		<input type="hidden" name="action" id="ms_action" />
		<!-- todo status check update -->
		<?php if ($seller['ms.seller_status'] == MsSeller::STATUS_DISABLED || $seller['ms.seller_status'] == MsSeller::STATUS_DELETED) { ?>
		<div class="ms-overlay"></div>
		<?php } ?>
		<fieldset id="profile">
			<h2 class="secondary-title"><?php echo $ms_account_register_profiles; ?></h2>
			<?php if (!$this->config->get('msconf_change_seller_nickname') && !empty($seller['ms.nickname'])) { ?>
				<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_nickname; ?></label>
				<div style="line-height: 30px;font-size:20px">
					<b><?php echo $seller['ms.nickname']; ?></b>
				</div>
			<?php } else { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_nickname; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  name="seller[nickname]" value="<?php echo $seller['ms.nickname']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_nickname_note; ?></p>
				</div>
			<?php } ?>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_description; ?></label>
			<div class="col-sm-10">
				<!-- todo strip tags if rte disabled -->
				<textarea name="seller[description]" id="seller_textarea" class="form-control <?php echo $this->config->get('msconf_enable_rte') ? 'ckeditor' : ''; ?>"><?php echo $this->config->get('msconf_enable_rte') ? htmlspecialchars_decode($seller['ms.description']) : strip_tags(htmlspecialchars_decode($seller['ms.description'])); ?></textarea>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_description_note; ?></p>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_company; ?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control"  name="seller[company]" value="<?php echo $seller['ms.company']; ?>" />
				<p class="ms-note"><?php echo $ms_account_sellerinfo_company_note; ?></p>
			</div>
		</div>
		
		<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account_address; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  name="seller[address]" value="<?php echo $seller['ms.address']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_account_address_note; ?></p>
				</div>
			</div>
			
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_country; ?></label>
			<div class="col-sm-10">
				<select name="seller[country]" class="form-control">
					<option value="" selected="selected"><?php echo $ms_account_sellerinfo_country_dont_display; ?></option>
					<?php foreach ($countries as $country) { ?>
					<option value="<?php echo $country['country_id']; ?>" <?php if ($seller['ms.country_id'] == $country['country_id'] || $country_id == $country['country_id']) { ?>selected="selected"<?php } ?>><?php echo $country['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_country_note; ?></p>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_zone; ?></label>
			<div class="col-sm-10">
				<select name="seller[zone]" class="form-control">
				<?php foreach ($zones as $zone) { ?>
					<option value="<?php echo $zone['zone_id']; ?>" <?php if ($seller['ms.zone_id'] == $zone['zone_id'] || $zone_id == $zone['zone_id']) { ?>selected="selected"<?php } ?>><?php echo $zone['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_zone_note; ?></p>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_city; ?></label>
			<div class="col-sm-10">
				<select name="seller[city]" class="form-control">
				<?php foreach ($cities as $city) { ?>
					<option value="<?php echo $city['city_id']; ?>" <?php if ($seller['ms.city_id'] == $city['city_id'] || $city_id == $city['city_id']) { ?>selected="selected"<?php } ?>><?php echo $city['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_city_note; ?></p>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_district; ?></label>
			<div class="col-sm-10">
				<select name="seller[district]" class="form-control">
				<?php foreach ($districts as $district) { ?>
					<option value="<?php echo $district['district_id']; ?>" <?php if ($seller['ms.district_id'] == $district['district_id'] || $district_id == $district['district_id']) { ?>selected="selected"<?php } ?>><?php echo $district['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_district_note; ?></p>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_subdistrict; ?></label>
			<div class="col-sm-10">
				<select name="seller[subdistrict]" class="form-control">
				<?php foreach ($subdistricts as $subdistrict) { ?>
					<option value="<?php echo $subdistrict['subdistrict_id']; ?>" <?php if ($seller['ms.subdistrict_id'] == $subdistrict['subdistrict_id'] || $subdistrict_id == $subdistrict['subdistrict_id']) { ?>selected="selected"<?php } ?>><?php echo $subdistrict['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_subdistrict_note; ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_postcode; ?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control"  name="seller[postcode]" value="<?php echo empty($seller['ms.postcode'])?'':$seller['ms.postcode']; ?>" readonly="readonly" />
				<p class="ms-note"><?php echo $ms_account_sellerinfo_postcode_note; ?></p>
			</div>
		</div>
		
		<div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_logistic; ?></label>
            <div class="col-sm-10 input-logistic">
              <div style="height: 60px; overflow: auto;">
                <?php foreach ($logistics as $logistic) { ?>
                <div class="checkbox">
                  <label style="display:block;min-height: 17px;padding-left: 20px;margin-bottom: 0;font-weight: normal;cursor: pointer;">
                    <?php if (in_array($logistic['shipping_id'], $seller_logistic)) { ?>
                    <input type="checkbox" name="seller[logistic][]" value="<?php echo $logistic['shipping_id']; ?>" checked="checked" />
                    <?php echo $logistic['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="seller[logistic][]" value="<?php echo $logistic['shipping_id']; ?>" />
                    <?php echo $logistic['name']; ?>
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $ms_account_sellerinfo_logistic_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $ms_account_sellerinfo_logistic_unselect_all; ?></a><p class="ms-note"><?php echo $ms_account_sellerinfo_logistic_note; ?></p></div>
			  
		  </div>
		  
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_avatar; ?></label>
			<div class="col-sm-10">
				<!--<input type="file" name="ms-file-selleravatar" id="ms-file-selleravatar" />-->
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-selleravatar" id="ms-file-selleravatar" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $ms_account_sellerinfo_avatar_note; ?></p>
				<p class="error" id="error_sellerinfo_avatar"></p>

				<div id="sellerinfo_avatar_files">
				<?php if (!empty($seller['avatar'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[avatar_name]" value="<?php echo $seller['avatar']['name']; ?>" />
						<img src="<?php echo $seller['avatar']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>

		<?php if ($this->config->get('msconf_enable_seller_banner')) { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_banner; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
					<a name="ms-file-sellerbanner" id="ms-file-sellerbanner" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				</div>

				<p class="ms-note"><?php echo $ms_account_sellerinfo_banner_note; ?></p>
				<p class="error" id="error_sellerinfo_banner"></p>

				<div id="sellerinfo_banner_files">
				<?php if (!empty($seller['banner'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[banner_name]" value="<?php echo $seller['banner']['name']; ?>" />
						<img src="<?php echo $seller['banner']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($ms_account_sellerinfo_terms_note) { ?>
		<div class="form-group required">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_terms; ?></label>
			<div class="col-sm-10">
				<p style="margin-bottom: 0">
					<input type="checkbox" name="seller[terms]" value="1" />
					<?php echo $ms_account_sellerinfo_terms_note; ?>
				</p>
			</div>
		</div>
		<?php } ?>

		<?php if ((!isset($seller['seller_id']) || $seller['ms.seller_status'] == MsSeller::STATUS_INCOMPLETE) && $seller_validation != MsSeller::MS_SELLER_VALIDATION_NONE) { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_reviewer_message; ?></label>
			<div class="col-sm-10">
				<textarea name="seller[reviewer_message]" id="message_textarea" class="form-control"></textarea>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_reviewer_message_note; ?></p>
			</div>
		</div>
		<?php } ?>
		
		<!--<div class="form-group required">
			<label class="col-sm-2 control-label"><?php echo $text_tax_type; ?></label>
			<div class="col-sm-10">
				<select name="seller[tax_type]" class="form-control">
				<?php if(isset($seller['ms.tax_type']) && $seller['ms.tax_type'] == $text_tax_type_pkp){ ?>
					<option value="<?php echo $text_tax_type_pkp; ?>" selected="selected"><?php echo $text_tax_type_pkp; ?></option>
					<option value="<?php echo $text_tax_type_non_pkp_val; ?>"><?php echo $text_tax_type_non_pkp; ?>
				<?php }else if(isset($seller['ms.tax_type']) && $seller['ms.tax_type'] == $text_tax_type_non_pkp_val){ ?>
					<option value="<?php echo $text_tax_type_pkp; ?>"><?php echo $text_tax_type_pkp; ?></option>
					<option value="<?php echo $text_tax_type_non_pkp_val; ?>" selected="selected"><?php echo $text_tax_type_non_pkp; ?>
				<?php }else{ ?>
					<option value="<?php echo $text_tax_type_pkp; ?>" selected="selected"><?php echo $text_tax_type_pkp; ?></option>
					<option value="<?php echo $text_tax_type_non_pkp_val; ?>"><?php echo $text_tax_type_non_pkp; ?>
				<?php } ?>
				</option>
				</select>
			</div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label"><?php echo $text_business_type; ?></label>
			<div class="col-sm-10">
				<select name="seller[business_type]" class="form-control" id="business_type">
				<?php if(isset($seller['ms.business_type']) && $seller['ms.business_type'] == $text_business_type_1){ ?>
					<option value="<?php echo $text_business_type_1; ?>" selected="selected"><?php echo $text_business_type_1; ?></option>
					<option value="<?php echo $text_business_type_2; ?>"><?php echo $text_business_type_2; ?></option>
				<?php }else if(isset($seller['ms.business_type']) && $seller['ms.business_type'] == $text_business_type_2){ ?>
					<option value="<?php echo $text_business_type_1; ?>"><?php echo $text_business_type_1; ?></option>
					<option value="<?php echo $text_business_type_2; ?>" selected="selected"><?php echo $text_business_type_2; ?></option>
				<?php }else { ?>
					<option value="<?php echo $text_business_type_1; ?>" selected="selected"><?php echo $text_business_type_1; ?></option>
					<option value="<?php echo $text_business_type_2; ?>"><?php echo $text_business_type_2; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>-->
		
        </fieldset>
		<!--<fieldset id="attachment">
		<h2 class="secondary-title"><?php echo $text_attachment; ?></h2>
		
		<div class="form-group npwp-individu">
			<label class="col-sm-2 control-label"><?php echo $text_npwp; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-npwp-individu" id="ms-file-seller-npwp-individu" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_npwp; ?></p>
				<p class="error" id="error_sellerinfo_npwp_individu"></p>

				<div id="sellerinfo_npwp_individu_files">
				<?php if (!empty($seller['npwp_individu'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[npwp_individu_files]" value="<?php echo $seller['npwp_individu']['name']; ?>" />
						<img src="<?php echo $seller['npwp_individu']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group ktp">
			<label class="col-sm-2 control-label"><?php echo $text_ktp; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-ktp" id="ms-file-seller-ktp" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_ktp; ?></p>
				<p class="error" id="error_sellerinfo_ktp"></p>

				<div id="sellerinfo_ktp_files">
				<?php if (!empty($seller['ktp'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[ktp_files]" value="<?php echo $seller['ktp']['name']; ?>" />
						<img src="<?php echo $seller['ktp']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group tabungan-individu">
			<label class="col-sm-2 control-label"><?php echo $text_book_account; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-tabungan-individu" id="ms-file-seller-tabungan-individu" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_book_account; ?></p>
				<p class="error" id="error_sellerinfo_tabungan_individu"></p>

				<div id="sellerinfo_tabungan_individu_files">
				<?php if (!empty($seller['tabungan_individu'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[tabungan_individu_files]" value="<?php echo $seller['tabungan_individu']['name']; ?>" />
						<img src="<?php echo $seller['tabungan_individu']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group npwp-bisnis">
			<label class="col-sm-2 control-label"><?php echo $text_npwp; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-npwp-bisnis" id="ms-file-seller-npwp-bisnis" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_npwp; ?></p>
				<p class="error" id="error_sellerinfo_npwp_bisnis"></p>

				<div id="sellerinfo_npwp_bisnis_files">
				<?php if (!empty($seller['npwp_bisnis'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[npwp_bisnis_files]" value="<?php echo $seller['npwp_bisnis']['name']; ?>" />
						<img src="<?php echo $seller['npwp_bisnis']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group siup">
			<label class="col-sm-2 control-label"><?php echo $text_siup; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-siup" id="ms-file-seller-siup" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_siup; ?></p>
				<p class="error" id="error_sellerinfo_siup"></p>

				<div id="sellerinfo_siup_files">
				<?php if (!empty($seller['siup'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[siup_files]" value="<?php echo $seller['siup']['name']; ?>" />
						<img src="<?php echo $seller['siup']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group tdp">
			<label class="col-sm-2 control-label"><?php echo $text_tdp; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-tdp" id="ms-file-seller-tdp" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_tdp; ?></p>
				<p class="error" id="error_sellerinfo_tdp"></p>

				<div id="sellerinfo_tdp_files">
				<?php if (!empty($seller['tdp'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[tdp_files]" value="<?php echo $seller['tdp']['name']; ?>" />
						<img src="<?php echo $seller['tdp']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group apbu">
			<label class="col-sm-2 control-label"><?php echo $text_apbu; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-apbu" id="ms-file-seller-apbu" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_apbu; ?></p>
				<p class="error" id="error_sellerinfo_apbu"></p>

				<div id="sellerinfo_apbu_files">
				<?php if (!empty($seller['apbu'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[apbu_files]" value="<?php echo $seller['apbu']['name']; ?>" />
						<img src="<?php echo $seller['apbu']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group ktp-direksi">
			<label class="col-sm-2 control-label"><?php echo $text_ktp_direksi; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-ktp-direksi" id="ms-file-seller-ktp-direksi" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_ktp_direksi; ?></p>
				<p class="error" id="error_sellerinfo_ktp_direksi"></p>

				<div id="sellerinfo_ktp_direksi_files">
				<?php if (!empty($seller['ktp_direksi'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[ktp_direksi_files]" value="<?php echo $seller['ktp_direksi']['name']; ?>" />
						<img src="<?php echo $seller['ktp_direksi']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group domisili">
			<label class="col-sm-2 control-label"><?php echo $text_domisili; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-domisili" id="ms-file-seller-domisili" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_domisili; ?></p>
				<p class="error" id="error_sellerinfo_domisili"></p>

				<div id="sellerinfo_domisili_files">
				<?php if (!empty($seller['domisili'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[domisili_files]" value="<?php echo $seller['domisili']['name']; ?>" />
						<img src="<?php echo $seller['domisili']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="form-group tabungan-bisnis">
			<label class="col-sm-2 control-label"><?php echo $text_book_account; ?></label>
			<div class="col-sm-10">
				<div class="buttons">
				<?php if ($this->config->get('msconf_avatars_for_sellers') != 2) { ?>
					<a name="ms-file-seller-tabungan-bisnis" id="ms-file-seller-tabungan-bisnis" class="btn btn-primary"><span><?php echo $ms_button_select_image; ?></span></a>
				<?php } ?>
				</div>

				<p class="ms-note"><?php echo $note_book_account; ?></p>
				<p class="error" id="error_sellerinfo_tabungan_bisnis"></p>

				<div id="sellerinfo_tabungan_bisnis_files">
				<?php if (!empty($seller['tabungan_bisnis'])) { ?>
					<div class="ms-image">
						<input type="hidden" name="seller[tabungan_bisnis_files]" value="<?php echo $seller['tabungan_bisnis']['name']; ?>" />
						<img src="<?php echo $seller['tabungan_bisnis']['thumb']; ?>" />
						<span class="ms-remove"></span>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		
		</fieldset>-->
		<fieldset id="payment">
			<h2 class="secondary-title"><?php echo $ms_account_register_payment; ?></h2>
			
			<div class="form-group required">
			<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account; ?></label>
			<div class="col-sm-10">
				<select name="seller[account]" class="form-control">
					<option value="" selected="selected"><?php echo $ms_account_sellerinfo_account_select; ?></option>
					<?php foreach ($accounts as $account) { ?>
					<option value="<?php echo $account['account_id']; ?>" <?php if ($seller['ms.account_id'] == $account['account_id'] || $account_id == $account['account_id']) { ?>selected="selected"<?php } ?>><?php echo $account['name']; ?></option>
					<?php } ?>
				</select>
				<p class="ms-note"><?php echo $ms_account_sellerinfo_account_note; ?></p>
			</div>
		</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account_name; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  name="seller[account_name]" value="<?php echo $seller['ms.account_name']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_account_name_note; ?></p>
				</div>
			</div>
			
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account_number; ?></label>
				<div class="col-sm-10">
					<input type="text" class="number form-control"  name="seller[account_number]" value="<?php echo $seller['ms.account_number']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_account_number_note; ?></p>
				</div>
			</div>
			
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account_npwp_number; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  name="seller[npwp_number]" value="<?php echo $seller['ms.npwp_number']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_account_npwp_number_note; ?></p>
				</div>
			</div>
			
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_sellerinfo_account_npwp_address; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  name="seller[npwp_address]" value="<?php echo $seller['ms.npwp_address']; ?>" />
					<p class="ms-note"><?php echo $ms_account_sellerinfo_account_npwp_address_note; ?></p>
				</div>
			</div>
			
		</fieldset>
        <br/>
        </form>

		<?php if (isset($group_commissions) && $group_commissions[MsCommission::RATE_SIGNUP]['flat'] > 0) { ?>
			<p class="alert alert-warning ms-commission">
				<?php echo sprintf($this->language->get('ms_account_sellerinfo_fee_flat'),$this->currency->format($group_commissions[MsCommission::RATE_SIGNUP]['flat'], $this->config->get('config_currency')), $this->config->get('config_name')); ?>
				<?php echo $ms_commission_payment_type; ?>
			</p>

			<?php if(isset($payment_form)) { ?><div class="ms-payment-form"><?php echo $payment_form; ?></div><?php } ?>
		<?php } ?>
		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
			<?php if ($seller['ms.seller_status'] != MsSeller::STATUS_DISABLED && $seller['ms.seller_status'] != MsSeller::STATUS_DELETED) { ?>
			<div class="pull-right"><a class="btn btn-primary button" id="ms-submit-button"><span><?php echo $ms_button_save; ?></span></a></div>
			<?php } ?>
		</div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<?php $timestamp = time(); ?>
<script type="text/javascript">
	var msGlobals = {
		timestamp: '<?php echo $timestamp; ?>',
		token : '<?php echo md5($salt . $timestamp); ?>',
		session_id: '<?php echo session_id(); ?>',
		uploadError: '<?php echo htmlspecialchars($ms_error_file_upload_error, ENT_QUOTES, "UTF-8"); ?>',
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
		config_enable_rte: '<?php echo $this->config->get('msconf_enable_rte'); ?>',
		zone_id: '<?php echo $seller['ms.zone_id'] ?>',
		zoneSelectError: '<?php echo htmlspecialchars($ms_account_sellerinfo_zone_select, ENT_QUOTES, "UTF-8"); ?>',
		zoneNotSelectedError: '<?php echo htmlspecialchars($ms_account_sellerinfo_zone_not_selected, ENT_QUOTES, "UTF-8"); ?>',
		city_id: '<?php echo $seller['ms.city_id'] ?>',
		citySelectError: '<?php echo htmlspecialchars($ms_account_sellerinfo_city_select, ENT_QUOTES, "UTF-8"); ?>',
		cityNotSelectedError: '<?php echo htmlspecialchars($ms_account_sellerinfo_city_not_selected, ENT_QUOTES, "UTF-8"); ?>',
		district_id: '<?php echo $seller['ms.district_id'] ?>',
		districtSelectError: '<?php echo htmlspecialchars($ms_account_sellerinfo_district_select, ENT_QUOTES, "UTF-8"); ?>',
		districtNotSelectedError: '<?php echo htmlspecialchars($ms_account_sellerinfo_district_not_selected, ENT_QUOTES, "UTF-8"); ?>',
		subdistrict_id: '<?php echo $seller['ms.subdistrict_id'] ?>',
		subdistrictSelectError: '<?php echo htmlspecialchars($ms_account_sellerinfo_subdistrict_select, ENT_QUOTES, "UTF-8"); ?>',
		subdistrictNotSelectedError: '<?php echo htmlspecialchars($ms_account_sellerinfo_subdistrict_not_selected, ENT_QUOTES, "UTF-8"); ?>'
	};
	
$('input[name=\'seller[npwp_number]\']').keypress(function (e){
	var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});
	
$(document).ready(function() {
	var value = $('#business_type').val();
	if(value=="INDIVIDUAL"){
		$('.form-group.npwp-individu').show();
		$('.form-group.ktp').show();
		$('.form-group.tabungan-individu').show();
			
		$('.form-group.npwp-bisnis').hide();
		$('.form-group.siup').hide();
		$('.form-group.tdp').hide();
		$('.form-group.apbu').hide();
		$('.form-group.ktp-direksi').hide();
		$('.form-group.domisili').hide();
		$('.form-group.tabungan-bisnis').hide();		
	}else if(value=="BUSINESS"){
		$('.form-group.npwp-individu').hide();
		$('.form-group.ktp').hide();
		$('.form-group.tabungan-individu').hide();
			
		$('.form-group.npwp-bisnis').show();
		$('.form-group.siup').show();
		$('.form-group.tdp').show();
		$('.form-group.apbu').show();
		$('.form-group.ktp-direksi').show();
		$('.form-group.domisili').show();
		$('.form-group.tabungan-bisnis').show();
	}
			
	$('#business_type').change(function(){
		var value = $('#business_type').val();
		if(value=="INDIVIDUAL"){
			$('.form-group.npwp-individu').show();
			$('.form-group.ktp').show();
			$('.form-group.tabungan-individu').show();
			
			$('.form-group.npwp-bisnis').hide();
			$('.form-group.siup').hide();
			$('.form-group.tdp').hide();
			$('.form-group.apbu').hide();
			$('.form-group.ktp-direksi').hide();
			$('.form-group.domisili').hide();
			$('.form-group.tabungan-bisnis').hide();
			
		}else if(value=="BUSINESS"){
			$('.form-group.npwp-individu').hide();
			$('.form-group.ktp').hide();
			$('.form-group.tabungan-individu').hide();
			
			$('.form-group.npwp-bisnis').show();
			$('.form-group.siup').show();
			$('.form-group.tdp').show();
			$('.form-group.apbu').show();
			$('.form-group.ktp-direksi').show();
			$('.form-group.domisili').show();
			$('.form-group.tabungan-bisnis').show();
		}
	});
});
</script>

<?php echo $footer; ?>