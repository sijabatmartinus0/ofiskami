<?php echo $header; ?>
<style>
.chart {
  position: relative;
  display: inline-block;
  width: 150px;
  height: 150px;
  margin:auto;
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
}
.chart canvas {
  position: absolute;
  top: 0;
  left: 0;
}
.percent {
    display: inline-block;
    line-height: 150px;
    z-index: 2;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-upload-product"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $ms_account_dashboard_upload_product; ?></h1>
	
	<form class="form-horizontal" id="upload-product">
	<h2 class="secondary-title"><?php echo $help_upload; ?></h2>
		<div class="form-group required">
			<label class="col-sm-3 control-label"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $text_category; ?></span></label>
			<div class="col-sm-8">
				<select class="form-control" value="" id="category" name="category_id">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($categories as $category){ ?>
						<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-3 control-label" for="button-upload"><span data-toggle="tooltip" title="<?php echo $help_upload; ?>"><?php echo $entry_upload; ?></span></label>
			<div class="col-sm-8">
				<div class="input-group">
					<input type="text" name="filename" value="" placeholder="<?php echo $entry_choose_file; ?>" id="input-filename" class="form-control" readonly />
					<span class="input-group-btn">
					<button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> </button>
					</span> 
				</div>
				
				<br/>
				<div class="pull-right">
					<button type="button" id="button-continue" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-check"></i> <?php echo $button_continue; ?></button>
				</div>
				<br/>
				<br/>
				<br/>
				<input type="hidden" name="error_common" value="" class="form-control" />
			</div>
			
		</div>
		
	</form>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
	<script type="text/javascript">
	$('#button-upload').on('click', function() {
		$('#form-upload').remove();
		
		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

		$('#form-upload input[name=\'file\']').trigger('click');
		
		timer = setInterval(function() {
			if ($('#form-upload input[name=\'file\']').val() != '') {
				clearInterval(timer);		
				
				$.ajax({
					url: 'index.php?route=seller/account-upload-product/upload_file',
					type: 'post',		
					dataType: 'json',
					data: new FormData($('#form-upload')[0]),
					cache: false,
					contentType: false,
					processData: false,		
					beforeSend: function() {
						$('#button-upload').button('loading');
					},
					complete: function() {
						$('#button-upload').button('reset');
					},	
					success: function(json) {
						if (json['error']) {
							alert(json['error']);
						}
									
						if (json['success']) {
							alert(json['success']);
							
							$('input[name=\'filename\']').attr('value', json['filename']);
							// $('input[name=\'mask\']').attr('value', json['mask']);
						}
					},			
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
		}, 500);
	});

	$("#button-continue").click(function() {
		var button = $(this);
		var id = $(this).attr('id');
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=seller/account-upload-product/uploadProcess',
			data: $('#upload-product').serialize(),
			beforeSend: function() {
				$('div.text-danger').remove();
				$('.alert-danger').hide().find('i').text('');
			},
			complete: function(jqXHR, textStatus) {
				button.show().prev('span.wait').remove();
				$('.alert-danger').hide().find('i').text('');
			},
			error: function(jqXHR, textStatus, errorThrown) {
			   $('.alert-danger').show().find('i').text(textStatus);
			},
			success: function(jsonData) {
				if (!jQuery.isEmptyObject(jsonData.errors)) {
					for (error in jsonData.errors) {
						$('[name="'+error+'"]').after('<div class="text-danger">' + jsonData.errors[error].join('') + '</div>');
					}
					window.scrollTo(0,0);
				} else {
					window.location = 'index.php?route=seller/account-upload-product';
				}
			}
		});
	});
	</script>
<?php echo $footer; ?>
	