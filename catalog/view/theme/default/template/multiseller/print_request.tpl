<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen,print" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
</head>
<style>
/* Print styling */

@media print {

[class*="col-sm-"] {
	float: left;
}

[class*="col-xs-"] {
	float: left;
}

.col-sm-12, .col-xs-12 { 
	width:100% !important;
}

.col-sm-11, .col-xs-11 { 
	width:91.66666667% !important;
}

.col-sm-10, .col-xs-10 { 
	width:83.33333333% !important;
}

.col-sm-9, .col-xs-9 { 
	width:75% !important;
}

.col-sm-8, .col-xs-8 { 
	width:66.66666667% !important;
}

.col-sm-7, .col-xs-7 { 
	width:58.33333333% !important;
}

.col-sm-6, .col-xs-6 { 
	width:50% !important;
}

.col-sm-5, .col-xs-5 { 
	width:41.66666667% !important;
}

.col-sm-4, .col-xs-4 { 
	width:33.33333333% !important;
}

.col-sm-3, .col-xs-3 { 
	width:25% !important;
}

.col-sm-2, .col-xs-2 { 
	width:16.66666667% !important;
}

.col-sm-1, .col-xs-1 { 
	width:8.33333333% !important;
}
  
.col-sm-1,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11,
.col-xs-12 {
float: left !important;
}

body {
	margin: 0;
	padding 0 !important;
	min-width: 768px;
}

.container {
	width: auto;
	min-width: 750px;
}

body {
	font-size: 10px;
}

a[href]:after {
	content: none;
}

.noprint, 
div.alert, 
header, 
.group-media, 
.btn, 
.footer, 
form, 
#comments, 
.nav, 
ul.links.list-inline,
ul.action-links {
	display:none !important;
}

}
</style>
<body>
<div class="container">
    <div style="page-break-after: always;">
	<div class="container-fluid">
	<div class="col-lg-10"><div class="pull-left"><a style="margin-top:20px;" onclick="window.print();" class="btn btn-info"><?php echo $button_print; ?><i class="fa fa-print"></i></a></div></div>
	
	</div>
	<img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/1f554a3d-25d6-4c30-bdbd-9c26c9f74fb5.png" width="150" class="pull-right" style="margin-top: -40px;">
	<br>
	<br>
	<br>
	<p style="background-color: #990033; padding: 5px; color: #fff; font-weight: bold; font-size: 14px; text-align: right; margin: 0 !important;" ><?php echo strtoupper($heading_title); ?></p>
	<p style="background-color: #FF6600; padding: 5px; color: #fff; font-weight: bold; font-size: 12px;"><?php echo $merchant; ?><?php if($merchant_company){ ?> (<?php echo $merchant_company; ?>) <?php } ?></p>
	
	<table border="0" width="60%">
		<tr>
			<td width="30%" style="font-weight: bold; padding: 5px;"><?php echo $column_request; ?></td>
			<td width="5%" style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;">#<?php echo $request_id; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $text_kiosk; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $kiosk; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $ms_account_productcomment_text_seller; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $merchant; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $column_date_added; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $date_added; ?></td>
		</tr>
	</table>
	<br/>
	<br/>
	
	<p style="background-color: #990033; padding: 5px; color: #fff; font-weight: bold; font-size: 12px;"><?php echo strtoupper($ms_list_request_product); ?> </p>
	<?php if ($details) { ?>
		<table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td width="10%" class="text-center" style="font-weight: bold;"><?php echo $ms_account_products_image; ?></td>
            <td class="text-center" style="font-weight: bold;"><?php echo $ms_account_products_product; ?></td>
            <td class="text-center" style="font-weight: bold;"><?php echo $ms_account_product_category; ?></td>
            <td class="text-center" style="font-weight: bold;"><?php echo $column_quantity; ?></td>
            <td class="text-center" style="font-weight: bold;"><?php echo $column_price; ?></td>
            <td class="text-center" style="font-weight: bold;"><?php echo $column_status; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($details as $detail) { ?>
          <tr>
            <td class="text-center" valign="center"><img src="<?php echo $detail['image']; ?>" alt="<?php echo $detail['name']; ?>" title="<?php echo $detail['name']; ?>" class="img-thumbnail"/></td>
            <td class="text-center" style="vertical-align: middle;"><?php echo $detail['name']; ?></td>
            <td class="text-center" style="vertical-align: middle;"><?php echo $detail['category']; ?></td>
            <td class="text-center" style="vertical-align: middle;"><?php echo $detail['quantity']; ?></td>
            <td class="text-center" style="vertical-align: middle;"><?php echo $detail['price']; ?></td>
            <td class="text-center" style="vertical-align: middle;"><?php echo $detail['status']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php }  ?>
	</div>
</div>
</body>
</html>