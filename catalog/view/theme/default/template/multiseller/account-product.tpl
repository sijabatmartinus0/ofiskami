<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $ms_account_products_heading; ?></h1>
      <?php echo $content_top; ?>
	  
	 <!-- <div class="table-responsive">
	<table class="list table table-bordered table-hover" id="list-products">
	<thead>
	<tr>
		<td><?php echo $ms_account_products_image; ?></td>
		<td><?php echo $ms_account_products_product; ?></td>
		<td><?php echo $ms_account_product_price; ?></td>
		<td><?php echo $ms_account_products_sales; ?></td>
		<td><?php echo $ms_account_products_earnings; ?></td>
		<td><?php echo $ms_account_products_status; ?></td>
		<td><?php echo $ms_account_products_date; ?></td>
		<td><?php echo $ms_account_products_listing_until; ?></td>
		<td class="large"><?php echo $ms_account_products_action; ?></td>
	</tr>
	</thead>
	<tbody></tbody>
	</table>
	</div>-->
	<div class="well" style="background-color: #F5F5F5;">
		<div class="row">
			<div class="col-sm-6">
			<div class="form-group">
				<label class="bold"><?php echo $column_product; ?></label>
				<input name="search_name" id="search_name" value="" placeholder="<?php echo $column_product; ?>" class="form-control" type="text">
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				<label class="bold"><?php echo $ms_product_sku; ?></label>
				<input name="search_sku" id="search_sku" value="" placeholder="<?php echo $ms_product_sku; ?>" class="form-control" type="text">
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				<label class="bold"><?php echo $column_date_added; ?></label>
				<div class="input-group date" >
					<input name="search_date_added" value="" placeholder="<?php echo "YYYY-MM-DD"; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="search_date_added">
					<span class="input-group-btn" style="display: table-cell !important; ">
					<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				<label class="bold" for="input-order-id"><?php echo $column_status; ?></label>
				<select class="combo-status form-control" value="" id="search_status">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
				<option value="<?php echo MsProduct::STATUS_ACTIVE; ?>"><?php echo $this->language->get('ms_product_status_' . MsProduct::STATUS_ACTIVE); ?></option>
				<option value="<?php echo MsProduct::STATUS_INACTIVE; ?>"><?php echo $this->language->get('ms_product_status_' . MsProduct::STATUS_INACTIVE); ?></option>
				<option value="<?php echo MsProduct::STATUS_DISABLED; ?>"><?php echo $this->language->get('ms_product_status_' . MsProduct::STATUS_DISABLED); ?></option>
				<option value="<?php echo MsProduct::STATUS_UNPAID; ?>"><?php echo $this->language->get('ms_product_status_' . MsProduct::STATUS_UNPAID); ?></option>
				</select>
			   
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				<label class="bold" for="input-order-id"><?php echo $text_category; ?></label>
				<select class="combo-status form-control" value="" id="search_category">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
				<?php foreach($categories as $category){ ?>
					<option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
				<?php } ?>
				</select>
			   
			</div>
			</div>
			<div class="col-sm-6">
				<button type="button" id="button_filter" class="btn btn-primary pull-right button" style="margin-top: 15px;"><i class="fa fa-search"></i> <?php echo $text_search; ?></button>
			</div>
		</div>
			
	</div>
	<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
	<div id="list-product">
	
	</div>

		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
			<div class="pull-right"><a href="<?php echo $link_create_product; ?>" class="btn btn-primary button"><?php echo $ms_create_product; ?></a></div>
		</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<script>
	$(function() {
		// $('#list-products').dataTable( {
			// "sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-product/getTableData",
			// "aoColumns": [
				// { "mData": "image" },
				// { "mData": "product_name" },
				// { "mData": "product_price" },
				// { "mData": "number_sold" },
				// { "mData": "product_earnings" },
				// { "mData": "product_status" },
				// { "mData": "date_created" },
				// { "mData": "list_until" },
				// { "mData": "actions", "bSortable": false, "sClass": "text-right" }
			// ]
		// });
		$('.date').datetimepicker({
			pickTime: false,
			maxDate: new Date()
		});
		
		$('.pcAttention').show();
		$('#list-product').load('<?php echo $list_product; ?>',function(){$('.pcAttention').hide();});
		
		$("#button_filter").on('click', function(){
			$('.pcAttention').show();
			$('#list-product').load('<?php echo $search_product; ?>&search_name=' +encodeURIComponent($('#search_name').val()) + '&search_sku='+$('#search_sku').val() + '&search_date_added='+$('#search_date_added').val() + '&search_status='+$('#search_status').val() + '&search_category='+$('#search_category').val(),function(){$('.pcAttention').hide();});
		});
		
		$( document ).ajaxComplete(function() {
			$('.list-products.pagination>ul>li>a').click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-name-asc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-name-desc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-price-asc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-price-desc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-date-asc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
			$("#sort-date-desc").click(function(event){
				event.preventDefault();
				$('.pcAttention').show();
				$('#list-product').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
			});
		}); 
		
		$(document).on('click', '.ms-button-delete', function() {
			if (!confirm('<?php echo $ms_account_products_confirmdelete; ?>')) return false;
		});
	});
</script>
<?php echo $footer; ?>