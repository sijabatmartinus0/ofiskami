<?php if(!empty($header_shippings)){ ?>
<div>
	<a role="button" id="link_all" data-toggle="collapse" href="#collapse_all_status" aria-expanded="true" aria-controls="collapseTwo" class="response-button"><span id="text_collapse_all"><?php echo $button_hide_all; ?> <i id="all" class="fa fa-chevron-circle-up all_collapse"></i></span> </a>
	<br class="wide">
	<br class="wide">
	<br class="wide">
	
	<div id="collapse_all_status" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	<?php foreach($header_shippings as $header_shipping){ ?>
	<!-- Header shipping data -->
	<table style="background-color: #F7F9FA; padding: 5px;" width="100%" class="shadow">
		<tr>
			<td width="20%" style="vertical-align: top; margin:auto;">
				<?php echo $text_buyer; ?>:
				<br class="wide-small">
				<span class="color-response bold">
				<?php echo $header_shipping['buyer']; ?>
				</span>
			</td>
			<td width="80%">
				<span class="bold" style="font-size: 16px;"> 
				<?php echo $header_shipping['total_price_invoice']; ?>
				</span>
				<span class="color-response bold">
				(#<?php echo $header_shipping['order_id']; ?> <?php echo $header_shipping['invoice_no']; ?>)
				</span>
				<br class="wide">
				<?php echo $text_buy_date; ?>
				<span class="bold"><?php echo $header_shipping['date_added']; ?></span>
				<br class="wide">
				
				<div class="box-response" style="background-color:#E4EFF2 !important;">
					<?php echo $header_shipping['date_modified']; ?>
					<br class="wide-small">
					<?php if($header_shipping['delivery_type_id'] == 1) {
							echo $text_receipt_number; ?>: <?php echo $header_shipping['shipping_receipt_number']; 
						} else {
							echo $header_shipping['delivery_type']; ?>: <?php echo $header_shipping['pp_branch_name']; 
						}
					?>
				</div>
				
				<br class="wide">
				<?php if($header_shipping['delivery_type_id'] == 1) { ?>
					<button type="button" class="btn btn-primary button-primary link_modal_shipping" data-target="#modal_shipping_status" data-toggle="modal" data-target="#modal_shipping_status" data-toggle="modal" data-detailid="<?php echo $header_shipping['order_detail_id']; ?>" data-orderid="<?php echo $header_shipping['order_id']; ?>">
						<i class="fa fa-pencil" id="edit_receipt"></i> <?php echo $text_edit_receipt; ?>
					</button>	
					<button type="button" class="btn btn-primary button-primary">
						<i class="fa fa-location-arrow" id="edit_receipt"> </i> <?php echo $text_track; ?>
					</button>	
				<?php } ?>
				<br class="wide">
				<a role="button" data-toggle="collapse" href="#<?php echo $header_shipping['order_detail_id']; ?>" aria-expanded="false" aria-controls="collapseTwo" class="response-button pull-right"><span id="text_collapse<?php echo $header_shipping['order_detail_id']; ?>"><?php echo $button_show; ?> <i class="fa fa-chevron-circle-down some_collapse" ></i></span></a>
				<br class="wide">
			</td>
		</tr>	
	</table>
	
	<!-- Common shipping data -->
	<div id="<?php echo $header_shipping['order_detail_id']; ?>" class="panel-collapse collapse some" role="tabpanel" aria-labelledby="headingOne">
	<div class="panel-body">
	<table class="table table-bordered table-hover list">
	<thead>
	<tr>
		<td class="text-left" rowspan="6" colspan="2"><?php echo $column_destination; ?></td>
		<td class="text-left" style="width: 25%;"><?php echo $column_quantity; ?></td>
		<td class="text-left" style="width: 25%;"><?php echo $column_shipping_price; ?></td>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td class="text-left" rowspan="6" colspan="2" style="width:50%; text-align: left;">
		<?php if($header_shipping['delivery_type_id'] == 1) { 
				echo $header_shipping['shipping_address']; 
			  } else if($header_shipping['delivery_type_id'] == 2) { 
				echo $header_shipping['address_pp'];
			} ?>
		</td>
		<td class="text-right" style="text-align: left;"><?php echo $header_shipping['total_qty']; ?> (<?php echo sprintf('%0.2f', $header_shipping['total_weight']); ?> <?php echo $header_shipping['weight']; ?>)</td>
		<td class="text-right" style="text-align: left;">
		<?php if($header_shipping['delivery_type_id'] == 1) { ?>
			<span class="highlight bold"><?php echo $header_shipping['shipping_name']; ?> - <?php echo $header_shipping['shipping_service_name']; ?></span>
		<?php } else if($header_shipping['delivery_type_id'] == 2) { ?>
			<span class="highlight bold"><?php echo $header_shipping['delivery_type']; ?> - <?php echo $header_shipping['pp_branch_name']; ?></span>
		<?php } ?>
		<br style="margin-bottom: 5px;"/>
		<?php echo $header_shipping['shipping_price']; ?>
		</td>
	</tr>
	<tr>
		<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" >Terima Sebagian</td>
		<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" ><?php echo $column_insurance?></td>
	</tr>
	<tr>
		<td class="text-left" style="text-align: left;">Tidak</td>
		<td class="text-left" style="text-align: left;"><?php echo $header_shipping['shipping_insurance']; ?></td>
	</tr>
	</tbody>
				
				
	<!-- Product -->
	<div class="table-responsive">
	<tbody>
	<?php 
		foreach ($products as $product) { 
			if ($product['order_detail_id'] == $header_shipping['order_detail_id']){
	?>
	<tr>
		<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['product_name']; ?>" title="<?php echo $product['product_name']; ?>" class="img-thumbnail"/></td>
		<td style="width: 40%; text-align: left;">
			<a href="<?php echo $product['href']; ?>"><?php echo $product['product_name']; ?></a>
		</td>
		<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_remarks; ?></td>
		<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_price; ?></td>
	</tr>
	<tr>
		<td style="text-align: left;"><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?></td>
		<td style="text-align: left;">-</td>
		<td style="text-align: left;"><?php echo $product['total']; ?></td>
		</tr>
	<?php 
		}
	}
	?>
	<script type="text/javascript">
			$('#<?php echo $header_shipping['order_detail_id']; ?>')
			.on('shown.bs.collapse', function () {
					 $('span[id=\'text_collapse<?php echo $header_shipping['order_detail_id']; ?>\']').html("<?php echo $button_hide; ?> <i class=\'fa fa-chevron-circle-up all_collapse\' ></i>");
			}).on('hidden.bs.collapse', function () {
					 $('span[id=\'text_collapse<?php echo $header_shipping['order_detail_id']; ?>\']').html("<?php echo $button_show; ?> <i class=\'fa fa-chevron-circle-down all_collapse\' ></i>");
			});
	</script>
	<tr>
		<td colspan="4">
		<div style="float:left; display:inline">
			<?php echo $column_payment_method; ?><?php echo $header_shipping['payment_method']; ?>
			(<?php echo $header_shipping['total_price']; ?>)
		</div>
		<div style="float:right; display:inline; font-size: 16px;" class="bold">
			<a data-toggle="tooltip" title="<?php echo $text_tooltip; ?>" style="color: black !important;"><i class="fa fa-question-circle"></i></a> <?php echo $column_total; ?><a class="color-response"><?php echo $header_shipping['total_price_invoice']; ?></a>
		</div>
		</td>
	</tr>
	</tbody>
	</div>
	</table>
	</div>	 
	</div>
	<br/>
				
	<?php } ?>
	</div>
</div>
<script type="text/javascript">
$('#collapse_all_status')
	.on('shown.bs.collapse', function () {
		 $('span[id=\'text_collapse_all\']').html("<?php echo $button_hide_all; ?> <i class=\'fa fa-chevron-circle-up some_collapse\' ></i>");
	}).on('hidden.bs.collapse', function () {
		$('span[id=\'text_collapse_all\']').html("<?php echo $button_show_all; ?> <i class=\'fa fa-chevron-circle-down some_collapse\' ></i>");
	});
</script>
	<div class="shipping-status pagination" style="width:100%;"><?php echo $pagination; ?></div>
	<?php } else { ?>
	<?php echo $text_no_data_global; ?>
	<?php } ?>
	
