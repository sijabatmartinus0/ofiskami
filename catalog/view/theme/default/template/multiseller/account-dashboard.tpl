<?php echo $header; ?>
<style>
.chart {
  position: relative;
  display: inline-block;
  width: 150px;
  height: 150px;
  margin:auto;
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
}
.chart canvas {
  position: absolute;
  top: 0;
  left: 0;
}
.percent {
    display: inline-block;
    line-height: 150px;
    z-index: 2;
}
.form-group{
	border-bottom:rgb(245,245,245) 1px solid;
	background:rgb(253,253,253);
}
.form-group .label-right{
	text-align: right;
}
.form-group .label-left{
	text-align: left;
	font-weight:bold;
	width:70% !important;
}
.notification-container{
	width:100%;
	//height:30px;
	text-align:right;
	margin-bottom:10px;
}
.notification{
	float:left;
	
	height:30px;
	background:#eaeaea;
	margin:5px;
	display: block;
	position:absolute;
	text-align:center;
	padding-top:5px;
	border-radius: 5px;
}
.order{
	right:60px;
	width:30px;
        top :0px;
}
.return{
	right:20px;
	width:30px;
        top :0px;
}
span.badge{
	position: absolute;
    top: -5px;
    right: -10px;
}
.btn-sm{
	padding:3px 8px;
}

.activate{
  margin-bottom:20px;

}

</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>

					<!--
					<li>
					<a href="<?php echo $this->url->link('seller/account-request-product', '', 'SSL'); ?>">
						<?php echo $ms_account_request_product; ?>
					</a>
					</li>
					-->
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $ms_account_dashboard_heading; ?></h1>
	<div class="notification-container">
		<a data-tooltip="Return" class="notification   return" href="<?php echo $link_return_order;?>"><i class="fa fa-reply fa-lg"></i><span class="badge"><?php echo $seller['return_order']; ?></span></a>
		<a data-tooltip="New Order" class="notification  order" href="<?php echo $link_pending_order;?>"><i class="fa fa-th-list fa-lg"></i><span class="badge"><?php echo $seller['pending_order']; ?></span></a>
	</div>
	<?php if((int)$seller['seller_type']==0 && (int)$seller['premium_status']==0 ){ ?>
		<div class="well">
			<?php echo $link_request_premium; ?>
		</div>
	<?php } ?>
	<?php if((int)$seller['seller_type']==0 && (int)$seller['premium_status']==1 ){ ?>
		<div class="well">
			<?php echo $link_premium; ?>
		</div>
	<?php } ?>
    <div class="row">
		<div class="refine-image col-md-4">
			<a href="<?php echo $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']); ?>">
				<img style="display: block" width="100" height="100" src="<?php echo $seller['avatar']; ?>" alt="<?php echo $seller['ms.nickname']; ?>">
				<span class="refine-category-name" style="height: 15px;  font-weight: bold;"><?php echo $seller['ms.nickname']; ?></span>
			</a>
		</div>
	
		<div class="overview product-options col-md-4">
			<div class="xl-50">
				<h3><?php echo $ms_account_dashboard_overview; ?></h3>
				<ul class="list-unstyled description">
					<li><span><?php echo $ms_date_created; ?>:</span> <span class="label-number"><?php echo $seller['date_created']; ?></span></li>
					<li><span><?php echo $ms_account_withdraw_deposit_fee; ?></span> <span class="label-number"><?php echo $seller['balance']; ?></span></li>
				</ul>
			</div>
		</div>

		<div class="overview col-md-4">
			<h3><?php echo $ms_account_dashboard_stats; ?></h3>
			<ul class="list-unstyled description" >
				<li><span><?php echo $ms_account_dashboard_total_sales; ?>:</span> <span class="label-number"><?php echo $seller['total_sales']; ?></span></li>
				<li><span><?php echo $ms_account_dashboard_total_earnings; ?>:</span> <span class="label-number"><?php echo $seller['total_earnings']; ?></span></li>
				<li><span><?php echo $ms_account_dashboard_earnings_month; ?>:</span> <span class="label-number"><?php echo $seller['earnings_month']; ?></span></li>
			</ul>
		</div>
		</div>
<div class="row">
		<div class="overview col-md-6">
			<div class="xl-50">
				<div class="title-chart">Order Complete</div>
		 		<span class="chart order-chart" data-percent="<?php echo $order_percent;?>"><span class="percent"></span></span>
			</div>
		</div>
		<div class="col-md-6">
			<div class="xl-50">
				<div class="title-chart">Review</div>
				<span class="chart review-chart" data-percent="<?php echo $review_percent;?>"><span class="percent"></span></span>
			</div>
		</div>
	
    </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
	<script type="text/javascript">
	$(function() {
		$('.order-chart').easyPieChart({
			easing: 'easeOutBounce',
			barColor: 'rgb(238, 28, 37)',
			trackColor: 'rgb(204, 204, 204)',
			scaleColor: 'rgb(36, 85, 145)',
			scaleLength: 7,
			lineCap: 'round',
			lineWidth: 5,
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent)+"%");
			}
		});
		
		$('.review-chart').easyPieChart({
			easing: 'easeOutBounce',
			barColor: 'rgb(238, 28, 37)',
			trackColor: 'rgb(204, 204, 204)',
			scaleColor: 'rgb(36, 85, 145)',
			scaleLength: 7,
			lineCap: 'round',
			lineWidth: 5,
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent)+"%");
			}
		});
		
	});
	</script>
<?php echo $footer; ?>
	