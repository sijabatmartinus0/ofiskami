<?php if ($payments) { ?>
      <table class="table table-bordered table-hover list">
        <thead >
          <tr>
            <td class="bold" style="text-align: center;"><?php echo $ms_account_sellerinfo_company; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_requester; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_email2; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_pay_type; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_pay_status; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $ms_amount; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_date_created; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_date_paid; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($payments as $payment) { ?>
          <tr>
            <td class="text-right"><?php echo $payment['company']; ?></td>
			<td class="text-right"><?php echo $payment['firstname']; ?></td>
            <td class="text-right"><?php echo $payment['email']; ?></td>
            <td class="text-left"><?php echo $payment['payment_type']; ?></td>
            <td class="text-left"><?php echo $payment['payment_status']; ?></td>
            <td class="text-left"><?php echo $payment['amount']; ?></td>
            <td class="text-left"><?php echo $payment['date_created']; ?></td>
			<?php if($payment['date_paid'] == '01/01/1970'){ ?>
				<td class="text-left">-</td>
            <?php } else { ?>
				<td class="text-left"><?php echo $payment['date_paid']; ?></td>
			<?php } ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="list-payments pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
	<p><?php echo $text_no_data_global; ?></p>
<?php } ?>