<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
	<div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-dashboard order-list"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $text_detail_return; ?></h1>
	
	<?php if($return_status_id == 1) { ?>
	<div class="pull-left">
		<span class="highlight-red bold" style="background-color: #D60D3C ; color: #FFFFFF; font-weight: bold;"><?php echo $sla; ?><?php echo $text_sla_return; ?></span>
	</div>
	<?php } ?>
	
	<?php if(($return_status_id == 1 && $is_action_changed == 0 && $confirm_action_changed == 0) || ($return_status_id == 1 && $is_action_changed == 1 && $confirm_action_changed == 1) ) {?>
		<div class="pull-right" style="margin-bottom: 10px;" >
		<a href="javascript:;" onclick="document.getElementById('form_accept_return').submit();" data-toggle="tooltip" title="<?php echo $text_accept_return; ?>" class="btn btn-primary"><i class="fa fa-check-square-o" ></i></a>
		</div>
	<?php } else if($return_status_id == 1 && $is_action_changed == 1 && $confirm_action_changed == 0){ ?>
		<div class="pull-right" style="margin-bottom: 10px;" >
		<a data-toggle="tooltip" title="<?php echo $text_accept_return; ?>" class="btn btn-primary"><i class="fa fa-check-square-o" ></i></a>
		</div>
	<?php } ?>
	
	<br class="wide"/>
	<br class="wide"/>
	<br class="wide"/>
	
	<form method="post" action="<?php echo $link_accept_return; ?>" id="form_accept_return">
	<input type="hidden" name="input_return_id" value="<?php echo $return_id; ?>" />
	</form>
	<table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" colspan="3"><?php echo $text_info_return; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 30%;">
				<b><?php echo $text_return_id; ?></b> #<?php echo $return_id; ?><br />
				<b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?>
			</td>
            <td class="text-left" style="width: 30%;">
				<b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
				<b><?php echo $text_date_ordered; ?></b> <?php echo $date_ordered; ?><br />
				<b><?php echo $column_invoice; ?></b> <?php echo $invoice_no; ?>
			</td>
			<td class="text-left" style="width: 40%;">
				<b><?php echo $column_customer; ?>:</b> <?php echo $name; ?><br />
				<b><?php echo $ms_account_sellerinfo_phone; ?>:</b> <?php echo $telephone; ?><br />
				<b><?php echo $text_email; ?></b> <?php echo $email; ?>
			</td>
          </tr>
        </tbody>
    </table>
	 
	<table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" colspan="3"><?php echo $text_product_info; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 15%;">
				<a data-target="#myModal" data-toggle="modal" ><img src="<?php echo $thumb; ?>" alt="<?php echo $product; ?>" title="<?php echo $product; ?>" class="img-thumbnail"/></a>
			</td>
            <td class="text-left" style="text-align: left; vertical-align: top;">
				<b><?php echo $column_product; ?>:</b> <a href="<?php echo $href; ?>"><?php echo $product; ?></a><br/>
				<b><?php echo $column_model; ?>:</b> <?php echo $model; ?>  <br/>
				<b><?php echo $column_quantity; ?>:</b> <?php echo $quantity; ?> 
			</td>
			<td class="text-left" style="text-align: left; left; vertical-align: top;">
				<b><?php echo $column_opened; ?>:</b> <?php echo $opened; ?>  <br/>
				<b><?php echo $column_reason; ?>:</b> <?php echo $reason; ?> <br/>
				<?php if($comment){ ?>
					<b><?php echo $column_comment; ?>:</b> <?php echo $comment; ?>  <br/>
				<?php } else {?>
					<b><?php echo $column_comment; ?>:</b> - <br/>
				<?php } ?>
				<b><?php echo $column_action; ?>:</b> <br/>
				<form method="post" action="<?php echo $link_edit_action; ?>" id="form_edit_action">
				<input type="hidden" name="input_return_id_action" value="<?php echo $return_id; ?>" />
				<div class="row" > 
				<div class="col-sm-4">
				<?php if ($return_status_id == 3 || ($is_action_changed == 1 && $confirm_action_changed == 0)) { ?>
					<select class="form-control" disabled value="" id="return_action" style="width: 200px;" name="return_action_id">
				<?php } else if (($is_action_changed == 1 && $confirm_action_changed == 1) || ($is_action_changed == 0 && $confirm_action_changed == 0)) { ?>
					<select class="form-control" value="" id="return_action" style="width: 200px;" name="return_action_id">
				<?php } ?>
				<?php foreach ($return_actions as $return_action) { ?>
				  <?php if ($return_action['return_action_id'] == $return_action_id) { ?>
				  <option selected value="<?php echo $return_action['return_action_id']; ?>"><?php echo $return_action['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $return_action['return_action_id']; ?>"><?php echo $return_action['name']; ?></option>
				  <?php  } ?>
				<?php  } ?>
				</select>
				</div>
				<div class="col-sm-4">
				<?php if ($return_status_id == 3 || ($is_action_changed == 1 && $confirm_action_changed == 0)){ ?>
					<a style="height: 35px; margin-left: 50px;" data-toggle="tooltip" title="<?php echo $text_change_action; ?>" class="btn btn-primary"><i class="fa fa-refresh" ></i></a>
				<?php } else if (($is_action_changed == 1 && $confirm_action_changed == 1) || ($is_action_changed == 0 && $confirm_action_changed == 0)) { ?>
					<a style="height: 35px; margin-left: 50px;" href="javascript:;" onclick="document.getElementById('form_edit_action').submit();" data-toggle="tooltip" title="<?php echo $text_change_action; ?>" class="btn btn-primary"><i class="fa fa-refresh" ></i></a>
				<?php } ?>
				</div>
				</div>
				</form>
				<br/>
				<?php if ($is_action_changed == 1 && $confirm_action_changed == 0) { ?>
				  <div class="alert alert-danger warning">
				  <i class="fa fa-exclamation-circle"></i> <?php echo $warning_user_confirm; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				  </div>
				<?php } else if ($is_action_changed == 1 && $confirm_action_changed == 1) { ?>
				  <div class="alert alert-success success">
				  <i class="fa fa-check-circle"></i> <?php echo $success_user_confirm; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				  </div>
				<?php } ?>
			</td>
          </tr>
        </tbody>
    </table>
	
<h1 class="heading-title"><?php echo $text_discuss; ?></h1>

<div class="panel panel-default">
	<div class="panel-collapse collapse in">
		<div class="panel-body">
		<div id="discuss_return">
		
		</div>
		<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
		<?php if(count($return_comments) == 2) { ?>
			<a id="view_more" class="pcViewBtn icon-only" style="cursor: pointer;"><span ><?php echo $text_view_more; ?></span><i style="margin-left: 5px; font-size: 16px" class="fa fa-comments"></i></a>
		<?php } ?>
	
		<div class="comment-title"></div>
		<br/>
		<?php if($return_status_id !=3 ){?>
		<div class="comment-form">
			<form class="pcForm" id="form_return_comments" method="post" >
				<input type="hidden" name="discuss_return_id" id="discuss_return_id" value="<?php echo $return_id; ?>">
				<input type="hidden" name="discuss_product_id" id="discuss_product_id" value="<?php echo $product_id; ?>">
				<textarea class="pcText" name="discuss_comment" id="discuss_comment" cols="40" rows="8" style="width: 100%;" placeholder="<?php $column_comment; ?>"></textarea>
				<div id="error_discuss" class="text-danger"><?php echo $error_discuss; ?></div>
			</form>
		</div>
		<br>
		<div class="right-side">
			<a id="pcSubmitBtn" class="btn btn-default button pcSubmitBtn"><span><?php echo $ms_button_submit; ?></span></a>
		</div>
		<?php }?>
		</div>
	</div>
</div>

	
	<?php if ($histories) { ?>
      <h1 class="heading-title"><?php echo $text_history; ?></h1>
      <table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" style="width: 20%;"><?php echo $column_date_added; ?></td>
            <td class="text-left bold"><?php echo $column_status; ?></td>
			
          </tr>
        </thead>
        <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><i class="fa fa-clock-o" style="padding: 3px; padding: 5px;"></i> <?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
			
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4><?php echo $product; ?></h4>
			</div>
			<div class="modal-body" >
				<img src="<?php echo $popup_image; ?>" alt="<?php echo $product; ?>" title="<?php echo $product; ?>" class="img-thumbnail"/>
			</div>
	    </div>
	  </div>
	</div>
</div>

<script type="text/javascript">
$(function(){
	$('.pcAttention').show();
	$('#view_more').hide();
	$('#error_discuss').hide();
	$('#discuss_return').load('<?php echo $list_up_comments; ?>&return_id=' +<?php echo $return_id; ?>,function(){$('.pcAttention').hide();$('#view_more').show();});
	
	$("#view_more").on('click', function(){
		$('.pcAttention').show();
		$('#discuss_return').load('<?php echo $list_return_comments; ?>&return_id=' +<?php echo $return_id; ?>,function(){$('.pcAttention').hide();});
		$('#view_more').hide();
	});
	
	$(document).ready(function(){
		<?php if($return_status_id !=3 ){?>
		$("#pcSubmitBtn").click(function(){
			var comment = $("#discuss_comment").val();
			var return_id = $("#discuss_return_id").val();
			var product_id = $("#discuss_product_id").val();
			
			$('.pcAttention').show();
			 $.ajax({
				url: 'index.php?route=seller/account-return-list/add_comment',
				type: "POST",
				cache: false,
				data: 
				{ 
					discuss_comment: comment, 
					discuss_return_id: return_id, 
					discuss_product_id: product_id
				},
				success : function(html){
					if($("#discuss_comment").val().length < 2){
						$('#error_discuss').show();
						$('.pcAttention').hide();
					}else{
						$('#error_discuss').hide();
						$('.pcAttention').hide();
						loadComments();
						$("#discuss_comment").val("");
						// setInterval (loadComments, 3000);
					}
				}
			 })
		});
		<?php }?>
		
		function loadComments(){
			$.ajax({
				url : "index.php?route=seller/account-return-list/list_return_comments&return_id=" + <?php echo $return_id ?>,
				cache : false,
				success : function(html){
					$('#discuss_return').load('<?php echo $list_return_comments; ?>&return_id=' +<?php echo $return_id; ?>);
					$('#view_more').hide();
				},
			});
		}
		
	 }); 
	
});
</script>

<?php echo $footer; ?>