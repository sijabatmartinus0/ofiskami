<?php echo $header; ?>
<style>
.note-popover .popover .popover-content .dropdown-menu, .note-toolbar .dropdown-menu {
    min-width: 90px;
}
ul.dropdown-menu {
	margin:0px !important;
}
.dropdown-menu {
    position: absolute !important;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    list-style: none;
    font-size: 12px;
    text-align: left;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 3px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    background-clip: padding-box;
}
.note-popover .popover .popover-content .note-color .dropdown-menu .btn-group .note-palette-title, .note-toolbar .note-color .dropdown-menu .btn-group .note-palette-title {
    margin: 2px 7px;
    font-size: 12px;
    text-align: center;
    border-bottom: 1px solid #eee;
}
.open > .dropdown-menu {
    display: block;
	position:absolute;
}
.note-toolbar>.btn-group {
    margin-top: 5px;
    margin-right: 5px;
    margin-left: 0;
}
.note-editor .note-toolbar {
    background-color: #f5f5f5;
    border-bottom: 1px solid #a9a9a9;
}
.note-popover .popover .popover-content, .note-toolbar {
    padding: 0 0 5px 5px;
    margin: 0;
}
.btn-toolbar .btn-group, .btn-toolbar .input-group {
    float: left;
}
.btn-group, .btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
}

.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
}
.btn-group > .btn:last-child:not(:first-child), .btn-group > .dropdown-toggle:not(:first-child) {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}
.btn-group > .btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
}
.btn-default {
    border-color: #FFFFFF;
}

.caret {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px solid;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
}
.btn .caret {
    margin-left: 0;
}
.btn-group > .btn:hover, .btn-group-vertical > .btn:hover, .btn-group > .btn:focus, .btn-group-vertical > .btn:focus, .btn-group > .btn:active, .btn-group-vertical > .btn:active, .btn-group > .btn.active, .btn-group-vertical > .btn.active {
    z-index: 2;
}
.btn-group .btn + .btn, .btn-group .btn + .btn-group, .btn-group .btn-group + .btn, .btn-group .btn-group + .btn-group {
    margin-left: -1px;
}
.btn-sm, .btn-group-sm > .btn {
    padding: 4px 9px;
    font-size: 11px;
    line-height: 1.5;
    border-radius: 2px;
}


/*#ms-new-product .form-control{
	height: auto !important;
    padding: 3px 5px !important;
	margin-top: 5px !important;
}*/
.input-group {
    position: relative;
    display: table;
    border-collapse: separate;
}
.input-group-addon:first-child {
    border-right: 0;
}
.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn-group:not(:last-child)>.btn, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: 1px solid #ccc;
	width: 75px !important;
}
.input-group-addon, .input-group-btn {
    width: 1%;
    white-space: nowrap;
    vertical-align: middle;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
}
#tab-data input{
	width:400px;
}
#tab-data select{
	width:400px;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
  <?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading; ?></h1>
      <?php echo $content_top; ?>
	  
	  <form id="ms-new-product" class="form-horizontal ms-account-product-form" method="post" enctype="multipart/form-data">
		<input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>" />
		<input type="hidden" name="action" id="ms_action" />
		<input type="hidden" name="list_until" value="<?php echo isset($list_until) ? $list_until : '' ?>" />
		<ul id="tabs" class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $ms_account_product_tab_general; ?></a></li>

			<?php
			$data_tab_fields = array('model', 'sku', 'upc', 'ean', 'jan', 'isbn', 'mpn', 'manufacturer', 'taxClass', 'subtract', 'stockStatus', 'dateAvailable');
			$intersection_fields = array_intersect($data_tab_fields, $this->config->get('msconf_product_included_fields'));
     		if (!empty($intersection_fields)) { ?>
     		<li><a href="#tab-data" data-toggle="tab"><?php echo $ms_account_product_tab_data; ?></a></li>
     		<?php } ?>

			<li><a href="#tab-options" data-toggle="tab"><?php echo $ms_account_product_tab_options; ?></a></li>

			<?php if ($this->config->get('msconf_allow_specials')) { ?>
     		<li><a href="#tab-specials" data-toggle="tab"><?php echo $ms_account_product_tab_specials; ?></a></li>
     		<?php } ?>

     		<?php if ($this->config->get('msconf_allow_discounts')) { ?>
     		<li><a href="#tab-discounts" data-toggle="tab"><?php echo $ms_account_product_tab_discounts; ?></a></li>
     		<?php } ?>
		</ul>

     	<!-- general tab -->
		<div class="tab-content ms-product">
     	<div id="tab-general" class="tab-content tab-pane active">
     		<?php if (count($languages) > 1) { ?>
			<?php $first = key($languages); ?>
			<ul class="nav nav-tabs" id="tabs">
				<?php foreach ($languages as $k => $language) { ?>
				<li <?php if ($k == $first) { ?> class="active" <?php } ?>><a data-toggle="tab" href="#language<?php echo $language['language_id']; ?>"><img src="image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
				<?php } ?>
			</ul>
			<?php } ?>
			<div class="tab-content">
			<!--<div class="tab-content">-->
			<?php
			reset($languages); $first = key($languages);
			foreach ($languages as $k => $language) {
				$langId = $language['language_id'];
				?>
				
				<div class="tab-content ms-language-div tab-pane <?php if ($k == $first) { echo 'active'; } ?>" id="language<?php echo $langId; ?>">
					<fieldset>
					<h2 class="secondary-title" ><?php echo $ms_account_product_name_description; ?></h2>
					<div class="form-group <?php if ($k == $first) { echo 'required'; } ?>">
						<label class="col-sm-2 control-label"><?php echo $ms_account_product_name; ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="languages[<?php echo $langId; ?>][product_name]" value="<?php echo $product['languages'][$langId]['name']; ?>" />
							<p class="ms-note"><?php echo $ms_account_product_name_note; ?></p>
							<p class="error" id="error_product_name_<?php echo $langId; ?>"></p>
						</div>
					</div>

					<div class="form-group <?php if ($k == $first) { echo 'required'; } ?>">
						<label class="col-sm-2 control-label"><?php echo $ms_account_product_description; ?></label>
						<div class="col-sm-10">
							<!-- todo strip tags if rte disabled -->
							<textarea name="languages[<?php echo $langId; ?>][product_description]" class="form-control <?php echo $this->config->get('msconf_enable_rte') ? 'ckeditor' : ''; ?>"><?php echo $this->config->get('msconf_enable_rte') ? htmlspecialchars_decode($product['languages'][$langId]['description']) : strip_tags(htmlspecialchars_decode($product['languages'][$langId]['description'])); ?></textarea>
							<p class="ms-note"><?php echo $ms_account_product_description_note; ?></p>
							<p class="error" id="error_product_description_<?php echo $langId; ?>"></p>
						</div>
					</div>

                    <?php if (in_array('metaDescription', $this->config->get('msconf_product_included_fields'))) { ?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $ms_account_product_meta_description; ?></label>
						<div class="col-sm-10">
							<!-- todo strip tags if rte disabled -->
							<textarea class="form-control"  name="languages[<?php echo $langId; ?>][product_meta_description]"><?php echo strip_tags(htmlspecialchars_decode($product['languages'][$langId]['meta_description'])); ?></textarea>
							<p class="ms-note"><?php echo $ms_account_product_meta_description_note; ?></p>
							<p class="error" id="error_product_meta_description_<?php echo $langId; ?>"></p>
						</div>
					</div>
                    <?php } ?>

                    <?php if (in_array('metaKeywords', $this->config->get('msconf_product_included_fields'))) { ?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $ms_account_product_meta_keyword; ?></label>
						<div class="col-sm-10">
							<!-- todo strip tags if rte disabled -->
							<textarea class="form-control"  name="languages[<?php echo $langId; ?>][product_meta_keyword]"><?php echo strip_tags(htmlspecialchars_decode($product['languages'][$langId]['meta_keyword'])); ?></textarea>
							<p class="ms-note"><?php echo $ms_account_product_meta_keyword_note; ?></p>
							<p class="error" id="error_product_meta_keyword_<?php echo $langId; ?>"></p>
						</div>
					</div>
                    <?php } ?>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $ms_account_product_tags; ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="languages[<?php echo $langId; ?>][product_tags]" value="<?php echo $product['languages'][$langId]['tags']; ?>" />
							<p class="ms-note"><?php echo $ms_account_product_tags_note; ?></p>
							<p class="error" id="error_product_tags_<?php echo $langId; ?>"></p>
						</div>
					</div>

					<?php if (isset($multilang_attributes) && !empty($multilang_attributes)) { ?>
					<?php foreach ($multilang_attributes as &$attr) { ?>
					<div class="form-group <?php if ($attr['required'] && $k == $first) { echo 'required'; } ?>">
						<label class="col-sm-2 control-label"><?php echo $attr['mad.name']; ?></label>
						<div class="col-sm-10">
							<?php if ($attr['attribute_type'] == MsAttribute::TYPE_TEXT) { ?>
								<input type="text" class="form-control" name="languages[<?php echo $langId; ?>][product_attributes][<?php echo $attr['attribute_id']; ?>][value]" value="<?php echo isset($multilang_attribute_values[$attr['attribute_id']][$langId]) ? $multilang_attribute_values[$attr['attribute_id']][$langId]['value'] : '' ?>" />
								<input type="hidden" name="languages[<?php echo $langId; ?>][product_attributes][<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo $multilang_attribute_values[$attr['attribute_id']][$langId]['value_id']; } ?>" />
							<?php } ?>

							<?php if ($attr['attribute_type'] == MsAttribute::TYPE_TEXTAREA) { ?>
								<textarea class="form-control"  name="languages[<?php echo $langId; ?>][product_attributes][<?php echo $attr['attribute_id']; ?>][value]"><?php echo isset($multilang_attribute_values[$attr['attribute_id']][$langId]) ? $multilang_attribute_values[$attr['attribute_id']][$langId]['value'] : '' ?></textarea>
								<input type="hidden" name="languages[<?php echo $langId; ?>][product_attributes][<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo $multilang_attribute_values[$attr['attribute_id']][$langId]['value_id']; } ?>" />
							<?php } ?>
							<p class="ms-note"><?php echo $attr['description']; ?></p>
							<p class="error"></p>
						</div>
					</div>
					<?php } ?>
					<?php } ?>
					</fieldset>
				</div>
			<?php } ?>
			</div>
			<!--</div>-->
			<fieldset>
            	<h2 class="secondary-title" ><?php echo $ms_account_product_price_attributes; ?></h2>

				<div class="form-group required">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_price; ?>(<span class="vertical-align: auto"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>)</label>
					<div class="col-sm-10">
						<input type="text" class="form-control number inline" name="product_price" value="<?php echo $product['price']; ?>" <?php if (isset($seller['commissions']) && $seller['commissions'][MsCommission::RATE_LISTING]['percent'] > 0) { ?>class="ms-price-dynamic"<?php } ?> />
						<span class="vertical-align: auto"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
						<p class="ms-note"><?php echo $ms_account_product_price_note; ?></p>
						<p class="error" id="error_product_price"></p>
					</div>
				</div>

				<?php if ($this->config->get('msconf_enable_shipping') == 2) { ?>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_enable_shipping; ?></label>
					<div class="col-sm-10">
						<input type="radio" name="product_enable_shipping" value="1" <?php if($product['shipping'] == 1) { ?> checked="checked" <?php } ?>/>
						<?php echo $text_yes; ?>
						<input type="radio" name="product_enable_shipping" value="0" <?php if($product['shipping'] == 0) { ?> checked="checked" <?php } ?>/>
						<?php echo $text_no; ?>
						<p class="ms-note"><?php echo $ms_account_product_enable_shipping_note; ?></p>
						<p class="error" id="error_product_enable_shipping"></p>
					</div>
				</div>
				<?php } ?>

				<div class="form-group required" <?php if (!$enable_quantities) { ?>style="display: none"<?php } ?>>
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_quantity; ?></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="product_quantity" value="<?php echo $product['quantity']; ?>" />
						<p class="ms-note"><?php echo $ms_account_product_quantity_note; ?></p>
						<p class="error" id="error_product_quantity"></p>
					</div>
				</div>

				<?php if (isset($normal_attributes) && !empty($normal_attributes)) { ?>
				<?php foreach ($normal_attributes as $attr) { ?>
				<div class="form-group <?php if ($attr['required']) { echo 'required'; } ?>">
					<label class="col-sm-2 control-label"><?php echo $attr['name']; ?></label>
					<div class="col-sm-10">
						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_SELECT) { ?>
							<select class="form-control" name="product_attributes[<?php echo $attr['attribute_id']; ?>]">
							<option value=""><?php echo $text_select; ?></option>
							<?php foreach ($attr['values'] as $attr_value) { ?>
							<option value="<?php echo $attr_value['attribute_value_id']; ?>" <?php if (isset($normal_attribute_values[$attr['attribute_id']]) && array_key_exists($attr_value['attribute_value_id'], $normal_attribute_values[$attr['attribute_id']])) { ?>selected="selected"<?php } ?>><?php echo $attr_value['name']; ?></option>
							<?php } ?>
							</select>
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_RADIO) { ?>
						<?php foreach ($attr['values'] as $attr_value) { ?>
							<input type="radio" name="product_attributes[<?php echo $attr['attribute_id']; ?>]" value="<?php echo $attr_value['attribute_value_id']; ?>" <?php if (isset($normal_attribute_values[$attr['attribute_id']]) && array_key_exists($attr_value['attribute_value_id'], $normal_attribute_values[$attr['attribute_id']])) { ?>checked="checked"<?php } ?> />
							<label><?php echo $attr_value['name']; ?></label>
							<br />
						<?php } ?>
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_IMAGE) { ?>
						<?php foreach ($attr['values'] as $attr_value) { ?>
							<input type="radio" name="product_attributes[<?php echo $attr['attribute_id']; ?>]" value="<?php echo $attr_value['attribute_value_id']; ?>" <?php if (isset($normal_attribute_values[$attr['attribute_id']]) && array_key_exists($attr_value['attribute_value_id'], $normal_attribute_values[$attr['attribute_id']])) { ?>checked="checked"<?php } ?> style="vertical-align: middle"/>
							<label><?php echo $attr_value['name']; ?></label>
							<img src="<?php echo $attr_value['image']; ?>" style="vertical-align: middle; padding: 1px; border: 1px solid #DDDDDD; margin-bottom: 10px" />
							<br />
						<?php } ?>
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_CHECKBOX) { ?>
						<?php foreach ($attr['values'] as $attr_value) { ?>
							<input type="checkbox" name="product_attributes[<?php echo $attr['attribute_id']; ?>][]" value="<?php echo $attr_value['attribute_value_id']; ?>" <?php if (isset($normal_attribute_values[$attr['attribute_id']]) && array_key_exists($attr_value['attribute_value_id'], $normal_attribute_values[$attr['attribute_id']])) { ?>checked="checked"<?php } ?> />
							<label><?php echo $attr_value['name']; ?></label>
							<br />
						<?php } ?>
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_TEXT) { ?>
							<input type="text" class="form-control" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo current(reset($normal_attribute_values[$attr['attribute_id']])); } ?>" />
							<input type="hidden" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo key($normal_attribute_values[$attr['attribute_id']]); } ?>" />
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_TEXTAREA) { ?>
							<textarea class="form-control"  name="product_attributes[<?php echo $attr['attribute_id']; ?>][value]" cols="40" rows="5"><?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo current(reset($normal_attribute_values[$attr['attribute_id']])); } ?></textarea>
							<input type="hidden" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo key($normal_attribute_values[$attr['attribute_id']]); } ?>" />
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_DATE) { ?>
							<div class="input-group date">
							<input type="text" class="form-control inline" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo current(reset($normal_attribute_values[$attr['attribute_id']])); } ?>" data-date-format="YYYY-MM-DD" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<input type="hidden" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo key($normal_attribute_values[$attr['attribute_id']]); } ?>" />
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_DATETIME) { ?>
							<div class="input-group datetime">
							<input type="text" class="form-control inline" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo current(reset($normal_attribute_values[$attr['attribute_id']])); } ?>" data-date-format="YYYY-MM-DD HH:mm" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<input type="hidden" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo key($normal_attribute_values[$attr['attribute_id']]); } ?>" />
						<?php } ?>

						<?php if ($attr['attribute_type'] == MsAttribute::TYPE_TIME) { ?>
							<div class="input-group time">
							<input type="text" class="form-control inline" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo current(reset($normal_attribute_values[$attr['attribute_id']])); } ?>" data-date-format="HH:mm" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<input type="hidden" name="product_attributes[<?php echo $attr['attribute_id']; ?>][value_id]" value="<?php if (isset($normal_attribute_values[$attr['attribute_id']])) { echo key($normal_attribute_values[$attr['attribute_id']]); } ?>" />
						<?php } ?>

						<p class="ms-note"><?php echo $attr['description']; ?></p>
						<p class="error"></p>
					</div>
				</div>
				<?php } ?>
				<?php } ?>
				<div class="form-group required">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_category; ?></label>
					<div class="col-sm-10" id="product_category_block">
						<?php if (!$msconf_allow_multiple_categories) { ?>

						<select class="form-control" name="product_category">
							<option value=""><?php echo ''; ?></option>
							<?php foreach ($categories as $category) { ?>
                                <?php if($msconf_enable_categories && $this->config->get('msconf_enable_shipping') == 2) { ?>
                                    <?php if($product['shipping'] == 1 || $product['shipping'] == NULL) { ?>
                                        <?php if(in_array($category['category_id'],$msconf_physical_product_categories)) { ?>
                                            <option value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>selected="selected"<?php } ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>><?php echo $category['name']; ?></option>
                                    <?php }} else { ?>
                                        <?php if(in_array($category['category_id'],$msconf_digital_product_categories)) { ?>
                                            <option value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>selected="selected"<?php } ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>><?php echo $category['name']; ?></option>
                                    <?php }} ?>
                                <?php } else { ?>
                                <option value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>selected="selected"<?php } ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>><?php echo $category['name']; ?></option>
							<?php }} ?>
						</select>

						<?php } else { ?>

						<div class="scrollbox">
						<?php $class = 'odd'; ?>
						<?php foreach ($categories as $category) { ?>
                            <?php if($msconf_enable_categories && $this->config->get('msconf_enable_shipping') == 2) { ?>
                                <?php if($product['shipping'] == 1 || $product['shipping'] == NULL) { ?>
                                    <?php if(in_array($category['category_id'],$msconf_physical_product_categories)) { ?>
                                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                        <div class="<?php echo $class; ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>">
                                            <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>checked="checked"<?php } ?> <?php if ($category['disabled']) { ?>disabled="disabled"<?php } ?>/>
                                            <?php echo $category['name']; ?>
                                        </div>
                                <?php }} else { ?>
                                     <?php if(in_array($category['category_id'],$msconf_digital_product_categories)) { ?>
                                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                        <div class="<?php echo $class; ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>">
                                            <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>checked="checked"<?php } ?> <?php if ($category['disabled']) { ?>disabled="disabled"<?php } ?>/>
                                            <?php echo $category['name']; ?>
                                        </div>
                                <?php }} ?>
                            <?php } else { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div class="<?php echo $class; ?> <?php echo ($category['disabled'] ? 'disabled' : ''); ?>">
                                    <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" <?php if (in_array($category['category_id'], explode(',',$product['category_id'])) && !$category['disabled']) { ?>checked="checked"<?php } ?> <?php if ($category['disabled']) { ?>disabled="disabled"<?php } ?>/>
                                    <?php echo $category['name']; ?>
                                </div>
                            <?php }} ?>
						</div>

						<?php } ?>

						<p class="ms-note"><?php echo $ms_account_product_category_note; ?></p>
						<p class="error" id="error_product_category"></p>
					</div>
				</div>
				<div class="table-responsive">
                <table id="attribute" class="list table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Attribute</td>
                      <td class="text-left">Value</td>
                    </tr>
                  </thead>
                  <tbody class="attribute-product">
				  <?php $attribute_row = 0; ?>
				  <?php if(!empty($product['product_attributes'])) { ?>
                    <?php foreach ($product['product_attributes'] as $product_attribute) { ?>
                    <tr id="attribute-row<?php echo $attribute_row; ?>">
                      <td class="text-left" style="width: 40%;"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" readonly="readonly" />
                        <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                      <td class="text-left">
						<table class="list table table-bordered table-hover dataTable">
						<tbody>
							<?php foreach ($languages as $language) { ?>
								<tr>
									<td><img src="image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
									<td><textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea></td>
								</tr>
							<?php } ?>
						</tbody>
						</table>
					   </td>
                    </tr>
                    <?php $attribute_row++; ?>
                    <?php } ?>
					<?php } else { ?>
					<tr><td colspan=2>No Attribute</td></tr>
					<?php } ?>
                  </tbody>
                </table>
              </div>
			</fieldset>

			<fieldset>
				<h2 class="secondary-title"><?php echo $ms_account_product_files; ?></h2>

				<div class="form-group <?php if ($msconf_images_limits[0] > 0) { echo 'required'; } ?>">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_image; ?></label>
					<div class="col-sm-10">
						<!--<input type="file" name="ms-file-addimages" id="ms-file-addimages" />-->
						<a name="ms-file-addimages" id="ms-file-addimages" class="btn btn-primary"><span><?php echo $ms_button_select_images; ?></span></a>
						<p class="ms-note"><?php echo $ms_account_product_image_note; ?></p>
						<div class="error" id="error_product_image"></div>

						<div class="image progress"></div>

						<div class="product_image_files">
						<?php if (isset($product['images'])) { ?>
						<?php $i = 0; ?>
						<?php foreach ($product['images'] as $image) { ?>
							<div class="ms-image">
								<input type="hidden" name="product_images[]" value="<?php echo $image['name']; ?>" />
								<img src="<?php echo $image['thumb']; ?>" />
								<span class="ms-remove"></span>
							</div>
						<?php $i++; ?>
						<?php } ?>
						<?php } ?>
						</div>
					</div>
				</div>

				<div class="form-group <?php if ($msconf_downloads_limits[0] > 0) { echo 'required'; } ?>">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_download; ?></label>
					<div class="col-sm-10">
						<!--<input type="file" name="ms-file-addfiles" id="ms-file-addfiles" />-->
						<a name="ms-file-addfiles" id="ms-file-addfiles" class="btn btn-primary"><span><?php echo $ms_button_select_files; ?></span></a>
						<p class="ms-note"><?php echo $ms_account_product_download_note; ?></p>
						<div class="error" id="error_product_download"></div>
						<div class="download progress"></div>
						<div class="product_download_files">
						<?php if (isset($product['downloads'])) { ?>
						<?php $i = 0; ?>
						<?php foreach ($product['downloads'] as $download) { ?>
							<div class="ms-download">
								<input type="hidden" name="product_downloads[<?php echo $i; ?>][download_id]" value="<?php echo isset($clone) ? '' : $download['id']; ?>" />
								<input type="hidden" name="product_downloads[<?php echo $i; ?>][filename]" value="<?php echo (isset($clone)) ? $download['src'] : ''; ?>" />
								<span class="ms-download-name"><?php echo $download['name']; ?></span>
								<div class="ms-buttons">
									<a href="<?php echo $download['href']; ?>" class="ms-button-download" title="<?php echo $ms_download; ?>"></a>
										<!--<input id="ms-update-<?php echo $download['id']; ?>" name="ms-update-<?php echo $download['id']; ?>" class="ms-file-updatedownload" type="file" multiple="false" />-->
									<a id="ms-update-<?php echo $download['id']; ?>" name="ms-update-<?php echo $download['id']; ?>" class="ms-file-updatedownload ms-button-update" title="<?php echo $ms_update; ?>"></a>
									<a class="ms-button-delete" title="<?php echo $ms_delete; ?>"></a>
								</div>
							</div>
						<?php $i++; ?>
						<?php } ?>
						<?php } ?>
						</div>
					</div>
				</div>
			</fieldset>

			<?php if ($seller['ms.product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_APPROVAL) { ?>
			<fieldset>
				<h2 class="secondary-title"><?php echo $ms_account_product_message_reviewer; ?></h2>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $ms_account_product_message; ?></label>
					<div class="col-sm-10">
						<textarea class="form-control"  name="product_message"></textarea>
						<p class="ms-note"><?php echo $ms_account_product_message_note; ?></p>
						<p class="error" id="error_product_message"></p>
					</div>
				</div>
			</fieldset>
			<?php } ?>
		</div>

        <!-- data tab -->
        <div id="tab-data" class="tab-pane tab-content">
		<h2 class="secondary-title">Data</h2>
			<?php if (in_array('model', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_model; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_model" value="<?php echo $product['model']; ?>" />
					<p class="error" id="error_product_model"></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('sku', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_sku; ?></label>
				<div class="col-sm-10">
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1"><?php echo $sku_prefix; ?></span>
				  <input style="width:325px" type="text" class="form-control" name="product_sku" value="<?php echo $product['sku']; ?>" />
				</div>
					<p class="error" id="error_product_sku"></p>
					<p class="ms-note"><?php echo $ms_account_product_sku_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<!--Product Code-->
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_code; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_code" value="<?php echo $product['code']; ?>" />
					<p class="error" id="error_product_code; ?>"></p>
				</div>
			</div>
			<!--END Product Code-->
			<?php if (in_array('upc', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_upc; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_upc" value="<?php echo $product['upc']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_upc_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('ean', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_ean; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_ean" value="<?php echo $product['ean']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_ean_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('jan', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_jan; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_jan" value="<?php echo $product['jan']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_jan_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('isbn', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_isbn; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_isbn" value="<?php echo $product['isbn']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_isbn_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('mpn', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_mpn; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_mpn" value="<?php echo $product['mpn']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_mpn_note; ?></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('manufacturer', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_manufacturer; ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="product_manufacturer" value="<?php echo $product['manufacturer'] ?>" />
					<input type="hidden" name="product_manufacturer_id" value="<?php echo $product['manufacturer_id']; ?>" />
					<p class="ms-note"><?php echo $ms_account_product_manufacturer_note; ?></p>
					<p class="error" id="error_product_manufacturer"></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('taxClass', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_tax_class; ?></label>
				<div class="col-sm-10">
					<select class="form-control" name="product_tax_class_id">
						<option value="0"><?php echo $text_select; ?></option>
						<?php foreach ($tax_classes as $tax_class) { ?>
						<?php if ($tax_class['tax_class_id'] == $product['tax_class_id']) { ?>
						<option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
					<p class="error" id="error_product_tax_class_id"></p>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('subtract', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_subtract; ?></label>
				<div class="col-sm-10">
					<select class="form-control" name="product_subtract">
						<?php if ($product['subtract']) { ?>
						<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						<option value="0"><?php echo $text_no; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_yes; ?></option>
						<option value="0" selected="selected"><?php echo $text_no; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('stockStatus', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_stock_status; ?></label>
				<div class="col-sm-10">
					<select class="form-control" name="product_stock_status_id">
						<?php foreach ($stock_statuses as $stock_status) { ?>
						<?php if ($stock_status['stock_status_id'] == $product['stock_status_id']) { ?>
						<option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<?php } ?>
			<?php if (in_array('dateAvailable', $this->config->get('msconf_product_included_fields'))) { ?>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $ms_account_product_date_available; ?></label>
				<div class="col-sm-10">
				<div class="input-group date">
					<input type="text" class="form-control" name="product_date_available" value="<?php echo $date_available; ?>" class="date" data-date-format="YYYY-MM-DD" style="width:361px" readonly="readonly" />
					<span class="input-group-btn">
					<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
					</span></div>
					<p class="error" id="error_product_date_available"></p>
				</div>
			</div>
			<?php } ?>
			<div class="form-group required">
                <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_dimension; ?></label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-4">
                      <input type="text" name="length" value="<?php echo $product['length']; ?>" placeholder="<?php echo $entry_length; ?>" id="input-length" class="form-control" />
                    <p class="error" id="error_length"></p>
					</div>
					</div>
					<div class="row">
                    <div class="col-sm-4">
                      <input type="text" name="width" value="<?php echo $product['width']; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
                    <p class="error" id="error_width"></p>
					</div>
					</div>
					<div class="row">
                    <div class="col-sm-4">
                      <input type="text" name="height" value="<?php echo $product['height']; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
                    <p class="error" id="error_height"></p>
					</div>
					</div>
                  </div>
                </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-length-class"><?php echo $entry_length_class; ?></label>
                <div class="col-sm-10">
                  <select name="length_class_id" id="input-length-class" class="form-control">
                    <?php foreach ($length_classes as $length_class) { ?>
                    <?php if ($length_class['length_class_id'] == $product['length_class_id']) { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-weight"><?php echo $entry_weight; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="weight" value="<?php echo $product['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-weight" class="form-control" />
                <p class="error" id="error_weight"></p>
				</div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $entry_weight_class; ?></label>
                <div class="col-sm-10">
                  <select name="weight_class_id" id="input-weight-class" class="form-control">
                    <?php foreach ($weight_classes as $weight_class) { ?>
                    <?php if ($weight_class['weight_class_id'] == $product['weight_class_id']) { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
        </div>

		<!-- options tab -->
		<div id="tab-options" class="tab-pane tab-content"></div>

		<!-- specials tab -->
		<?php if ($this->config->get('msconf_allow_specials')) { ?>
		<div id="tab-specials" class="tab-pane tab-content">
			<h2 class="secondary-title"><?php echo $ms_account_product_tab_specials; ?></h2>
			<p class="error" id="error_specials"></p>

			<table class="list table table-bordered table-hover">
				<thead>
				<tr>
					<td style="width:100px;"><span class="required">*</span><?php echo $ms_account_product_priority; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_price; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_date_start; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_date_end; ?></td>
					<td style="width:100px;"></td>
				</tr>
				</thead>

				<tbody>

				<!-- sample row -->
				<tr class="ffSample">
					<td style="width:100px;"><input class="number" style="width:100px;" type="text" class="form-control inline" name="product_specials[0][priority]" value="" size="2" /></td>
					<td style="width:150px;"><input class="number" style="width:150px;" type="text" class="form-control inline" name="product_specials[0][price]" value="" /></td>
					<td style="width:150px;">
						<div class="input-group date_start" style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_specials[0][date_start]" value="" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td style="width:150px;">
						<div class="input-group date_end"style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_specials[0][date_end]" value="" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td style="width:100px;"><a style="width:32px;height:32px;" class="btn ms-button-delete" title="<?php echo $ms_delete; ?>"></a></td>
				</tr>

				<?php if (isset($product['specials'])) { ?>
				<?php $special_row = 1; ?>
				<?php foreach ($product['specials'] as $product_special) { ?>
				<tr>
					<td style="width:100px;"><input class="number" type="text" class="form-control inline" name="product_specials[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" size="2" /></td>
					<td style="width:150px;"><input class="number" type="text" class="form-control inline" name="product_specials[<?php echo $special_row; ?>][price]" value="<?php echo $this->MsLoader->MsHelper->uniformDecimalPoint((int)$product_special['price']); ?>" /></td>
					<td style="width:150px;">
						<div class="input-group date_start" style="width:150px;">
						<input type="text" class="form-control inline" name="product_specials[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td style="width:150px;">
						<div class="input-group date_end" style="width:150px;">
						<input type="text" class="form-control inline" name="product_specials[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" data-date-format="YYYY-MM-DD" readonly="readonly"/>
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td style="width:100px;"><a style="width:32px;height:32px;" class="btn ms-button-delete" title="<?php echo $ms_delete; ?>"></a></td>
				</tr>
				<?php $special_row++; ?>
				<?php } ?>
				<?php } ?>
				</tbody>

				<tfoot>
				<tr>
				<td colspan="5" class="text-center"><a class="btn btn-primary ffClone pull-right"><?php echo $ms_button_add_special; ?></a></td>
				</tr>
				</tfoot>
			</table>
		</div>
		<?php } ?>

		<!-- Quantity Discounts tab -->
		<?php if ($this->config->get('msconf_allow_discounts')) { ?>
		<div id="tab-discounts" class="tab-pane tab-content">
			<h2 class="secondary-title"><?php echo $ms_account_product_tab_discounts; ?></h2>
			<p class="error" id="error_quantity_discounts"></p>
			
			<table class="list table table-bordered table-hover">
				<thead>
				<tr>
					<td style="width:75px;"><span class="required">*</span><?php echo $ms_account_product_priority; ?></td>
					<td style="width:75px;"><span class="required">*</span><?php echo $ms_account_product_quantity; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_price; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_date_start; ?></td>
					<td style="width:150px;"><span class="required">*</span><?php echo $ms_account_product_date_end; ?></td>
					<td style="width:75px;"></td>
				</tr>
				</thead>
				
				<tbody>				
				
				<!-- sample row -->
				<tr class="ffSample">				
					<td><input class="number" style="width:75px;" type="text" class="form-control inline" name="product_discounts[0][priority]" value="" size="2" /></td>
					<td><input class="number" style="width:75px;" type="text" class="form-control inline" name="product_discounts[0][quantity]" value="" size="2" /></td>
					<td><input class="number" style="width:150px;" type="text" class="form-control inline" name="product_discounts[0][price]" value="" /></td>
					<td>
						<div class="input-group date_start" style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_discounts[0][date_start]" value="" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td>
						<div class="input-group date_end" style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_discounts[0][date_end]" value="" data-date-format="YYYY-MM-DD" readonly="readonly"/>
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td><a style="width:32px;height:32px;" class="btn ms-button-delete" title="<?php echo $ms_delete; ?>"></a></td>
				</tr>
				
				<?php if (isset($product['discounts'])) { ?>
				<?php $discount_row = 1; ?>
				<?php foreach ($product['discounts'] as $product_discount) { ?>
				<tr>
					<td><input class="number" style="width:75px;" type="text" class="form-control inline" name="product_discounts[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" size="2" /></td>
					<td><input class="number" style="width:75px;" type="text" class="form-control inline" name="product_discounts[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" size="2" /></td>
					<td><input class="number" style="width:150px;" type="text" class="form-control inline" name="product_discounts[<?php echo $discount_row; ?>][price]" value="<?php echo $this->MsLoader->MsHelper->uniformDecimalPoint((int)$product_discount['price']); ?>" /></td>
					<td>
						<div class="input-group date_start" style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_discounts[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td>
						<div class="input-group date_end" style="width:150px;">
						<input style="width:150px;" type="text" class="form-control inline" name="product_discounts[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" data-date-format="YYYY-MM-DD" readonly="readonly" />
						<span class="input-group-btn">
                			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
					</td>
					<td><a style="width:32px;height:32px" class="btn ms-button-delete" title="<?php echo $ms_delete; ?>"></a></td>
				</tr>
				<?php $discount_row++; ?>
				<?php } ?>
				<?php } ?>
				</tbody>

				<tfoot>
				<tr>
					<td colspan="6" class="text-center"><a class="btn btn-primary ffClone pull-right"><?php echo $ms_button_add_discount; ?></a></td>
				</tr>
				</tfoot>
			</table>
		</div>
		<?php } ?>
		</div>
	</form>
		
		<?php if (isset($seller['commissions']) && ($seller['commissions'][MsCommission::RATE_LISTING]['percent'] > 0 || $seller['commissions'][MsCommission::RATE_LISTING]['flat'] > 0)) { ?>
			<?php if ($seller['commissions'][MsCommission::RATE_LISTING]['percent'] > 0) { ?>
			<p class="alert alert-warning ms-commission">
				<?php echo sprintf($this->language->get('ms_account_product_listing_percent'),$this->currency->format($seller['commissions'][MsCommission::RATE_LISTING]['flat'], $this->config->get('config_currency'))); ?>
				<?php echo $ms_commission_payment_type; ?>
			</p>
			<?php } else if ($seller['commissions'][MsCommission::RATE_LISTING]['flat'] > 0) { ?>
			<p class="alert alert-warning ms-commission">
				<?php echo sprintf($this->language->get('ms_account_product_listing_flat'),$this->currency->format($seller['commissions'][MsCommission::RATE_LISTING]['flat'], $this->config->get('config_currency'))); ?>
				<?php echo $ms_commission_payment_type; ?>
			</p>
			<?php } ?>
			
			<?php if(isset($payment_form)) { ?><div class="ms-payment-form"><?php echo $payment_form; ?></div><?php } ?>
		<?php } ?>
		
		<?php if (isset($list_until) && $list_until != NULL) { ?>
			<p class="alert alert-warning">
				<?php echo sprintf($this->language->get('ms_account_product_listing_until'), date($this->language->get('date_format_short'), strtotime($list_until))); ?>
			</p>
		<?php } ?>


		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
			<?php if ($seller['ms.seller_status'] != MsSeller::STATUS_DISABLED && $seller['ms.seller_status'] != MsSeller::STATUS_DELETED && $seller['ms.seller_status'] != MsSeller::STATUS_INCOMPLETE) { ?>
			<div class="pull-right"><a class="btn btn-primary button" id="ms-submit-button"><span><?php echo $ms_button_submit; ?></span></a></div>
			<?php } ?>	
		</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<?php $timestamp = time(); ?>
<script>
	var msGlobals = {
		timestamp: '<?php echo $timestamp; ?>',
		token : '<?php echo md5($salt . $timestamp); ?>',
		session_id: '<?php echo session_id(); ?>',
		product_id: '<?php echo $product['product_id']; ?>',
		text_delete: '<?php echo htmlspecialchars($ms_delete, ENT_QUOTES, "UTF-8"); ?>',
		text_none: '<?php echo htmlspecialchars($ms_none, ENT_QUOTES, "UTF-8"); ?>',
		uploadError: '<?php echo htmlspecialchars($ms_error_file_upload_error, ENT_QUOTES, "UTF-8"); ?>',
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
		formNotice: '<?php echo htmlspecialchars($ms_error_form_notice, ENT_QUOTES, "UTF-8"); ?>',
		config_enable_rte: '<?php echo $this->config->get('msconf_enable_rte'); ?>',
		config_enable_quantities: '<?php echo $this->config->get('msconf_enable_quantities'); ?>'
	};
	
	$('input[name=\'product_quantity\'],input[name=\'length\'],input[name=\'width\'],input[name=\'height\'],input[name=\'weight\']').keypress(function (e){
		  var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46) {
			return false;
		  }
	});
	$("body").delegate(".date_start", "focusin", function(){
		var ds=$(this);
		$(this).datetimepicker({
			pickTime: false,
			minDate: new Date()
		}).on("dp.change", function (e) {
			$('.date_end').datetimepicker({
				useCurrent: false, //Important! See issue #1075
				pickTime: false,
				minDate: new Date()
			});
			ds.parents('tr').find('.date_end').data("DateTimePicker").setMinDate(e.date);
		});
	});
	$("body").delegate(".date_end", "focusin", function(){
		$(this).datetimepicker({
			useCurrent: false, //Important! See issue #1075
			pickTime: false,
			minDate: new Date()
		});
	});
	
	$('.date').datetimepicker({
		pickTime: false
	});
</script>
<?php echo $footer; ?>