<?php echo $header; ?>
<style>
.chart {
  position: relative;
  display: inline-block;
  width: 150px;
  height: 150px;
  margin:auto;
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
}
.chart canvas {
  position: absolute;
  top: 0;
  left: 0;
}
.percent {
    display: inline-block;
    line-height: 150px;
    z-index: 2;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					<li>
					<a href="<?php echo $this->url->link('seller/account-request-product', '', 'SSL'); ?>">
						<?php echo $ms_account_request_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-request-product"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $text_view_request; ?></h1>
		<table border="0" width="60%">
		<tr>
			<td width="30%" style="font-weight: bold; padding: 5px;"><?php echo $column_request; ?></td>
			<td width="5%" style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;">#<?php echo $request_id; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $text_kiosk; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $kiosk; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $ms_account_productcomment_text_seller; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $merchant; ?></td>
		</tr>
		<tr>
			<td style="font-weight: bold; padding: 5px;"><?php echo $column_date_added; ?></td>
			<td style="font-weight: bold; padding: 5px;">:</td>
			<td style="padding: 5px;"><?php echo $date_added; ?></td>
		</tr>
		</table>
		<br/>
		<br/>
		<?php if ($details) { ?>
		<table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td width="10%" class="text-right bold"><?php echo $ms_account_products_image; ?></td>
            <td class="text-right bold"><?php echo $ms_account_products_product; ?></td>
            <td class="text-right bold"><?php echo $ms_account_product_category; ?></td>
            <td class="text-left bold"><?php echo $column_quantity; ?></td>
            <td class="text-left bold"><?php echo $column_price; ?></td>
            <td class="text-left bold"><?php echo $column_status; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($details as $detail) { ?>
          <tr>
            <td class="text-right"><img src="<?php echo $detail['image']; ?>" alt="<?php echo $detail['name']; ?>" title="<?php echo $detail['name']; ?>" class="img-thumbnail"/></td>
            <td class="text-right"><a href="<?php echo $detail['href']; ?>"><?php echo $detail['name']; ?></a></td>
            <td class="text-right"><?php echo $detail['category']; ?></td>
            <td class="text-left"><?php echo $detail['quantity']; ?></td>
            <td class="text-left"><?php echo $detail['price']; ?></td>
            <td class="text-left"><?php echo $detail['status']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } else { ?>
	   <p><?php echo $text_no_data_global; ?></p>
	  <?php } ?>
		
		<div class="pull-right"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<script type="text/javascript">
$(function(){
		$('.pcAttention').show();
		$('#list-request').load('<?php echo $list_request; ?>',function(){$('.pcAttention').hide();});
		
		$( document ).ajaxComplete(function() {
		$('.list-requests.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-request').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	}); 
	
	document.getElementById('kiosk_id').onchange = function () {
		// selGate = this.options[this.selectedIndex].text
		// document.getElementById('kiosk_id').innerHTML = selGate;
		$('.pcAttention').show();
		$('#list-request').load('<?php echo $search_request; ?>&kiosk_id=' +$('#kiosk_id').val(),function(){$('.pcAttention').hide();});
	}
	
	// $("#button_filter").on('click', function(){
		// $('.pcAttention').show();
		// $('#list-request').load('<?php echo $search_request; ?>&kiosk_id=' +$('#kiosk_id').val(),function(){$('.pcAttention').hide();});
	// });
});
</script>
<?php echo $footer; ?>
	