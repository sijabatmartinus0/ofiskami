<?php if(!empty($header_shippings)){ ?>
	<div style="padding: 5px">
				<input type="checkbox" name="check_accept_all" onClick="toggle(this)" />
				<div style="display: inline; padding: 22px">
				<a id="response-receive"><button type="button" class="btn btn-primary button" >
					<i class="fa fa-check-circle" id="accept_all"></i> <?php echo $button_accept_all; ?>
				</button></a>
				</div>
				<a role="button" data-toggle="collapse" href="#collapseAll" aria-expanded="true" aria-controls="collapseOne" class="response-button"><span id="pending_collapse_all"><?php echo $button_hide_all; ?> <i class="fa fa-chevron-circle-up"></i></span>
				</a>
				<br class="wide">
			</div>
			<div id="collapseAll" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			
			<?php 
				$x = 0;
				foreach($header_shippings as $header_shipping){ ?>
				<!-- Header shipping data -->
				
				<table style="background-color: #E8E8E8; padding: 5px;" width="100%">
					<tr>
					<form action="<?php echo $update_response_many; ?>" method="post" enctype="multipart/form-data" id="update_response_many">
						<td width="5%" style="vertical-align: top; margin:auto;">
							<input type="checkbox" name="check_accept[]" value="<?php echo $header_shipping['order_id']; ?>-<?php echo $header_shipping['order_detail_id']; ?>"></td>
					</form>
						<td width="95%">
							<div class="bold">
								<?php echo $text_buyer; ?>
								<span class="color-response"><?php echo $header_shipping['buyer']; ?> 
								(#<?php echo $header_shipping['order_id']; ?> <?php echo $header_shipping['invoice_no']; ?>)</span>
								<a data-orderdetailid="<?php echo $header_shipping['order_detail_id']; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_print; ?>" class="btn btn-primary response-button link_print pull-right"><i class="fa fa-print"></i></a>
							</div>
							<br class="wide"/>
							<?php echo $text_buy_date; ?> <span class="bold"><?php echo $header_shipping['date_added']; ?></span> <i class="fa fa-arrows-h"></i>
							<?php echo $text_deadline_response; ?> 
							<span class="highlight bold" style="background-color: #D60D3C !important; color: #FFFFFF;">
							<?php echo $header_shipping['sla_response']; ?><?php echo $text_deadline_days; ?>
							</span>
							<br class="wide"/>
							<br class="wide"/>
						<div style="background-color: #FAFAFA; padding: 10px; border: 1px solid #F6F6F6; width: auto; height: 80px;	margin-right: 15px;">
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form_response<?php echo $x; ?>" >
							<?php echo $text_response; ?>
							<div style="display: inline; margin-left: 80px;">
							<input type="hidden" name="input_order_detail_id" value="<?php echo $header_shipping['order_detail_id']; ?>" />
							<input type="hidden" name="input_order_id" value="<?php echo $header_shipping['order_id']; ?>" />
							<input type="radio" name="rbt_response" value="1" checked="checked" id="<?php echo $x; ?>A" ><?php echo $text_accept; ?>
							<input type="radio" name="rbt_response" value="0" id="<?php echo $x; ?>B" ><?php echo $text_decline; ?>
							</div>
							<div style="float: right; display: inline;">
							<input id="btn_response" type="submit" class="btn btn-primary button pull-right" value="<?php echo $button_response; ?>">
							</div>
							<div style="display: none;" id="<?php echo $x; ?>C">
							<label><?php echo $text_decline_reason; ?></label>
							<div style="margin-left: 25px; display: inline;">
								<?php if ((int)$this->config->get('config_language_id') == 1){ ?>
									<input type="radio" name="rbt_reason" value="<?php echo MsOrderData::REJECT_REASON_A1; ?>" checked="checked" ><?php echo MsOrderData::REJECT_REASON_A1; ?>
									<input type="radio" name="rbt_reason" value="<?php echo MsOrderData::REJECT_REASON_B1; ?>"><?php echo MsOrderData::REJECT_REASON_B1; ?>	
								<?php } else { ?>
									<input type="radio" name="rbt_reason" value="<?php echo MsOrderData::REJECT_REASON_A2; ?>" checked="checked" ><?php echo MsOrderData::REJECT_REASON_A2; ?>
									<input type="radio" name="rbt_reason" value="<?php echo MsOrderData::REJECT_REASON_B2; ?>"><?php echo MsOrderData::REJECT_REASON_B2; ?>	
								<?php } ?>
							</div>
							</div>
						</form>
						</div>
						<br class="wide"/>
						<a role="button" data-toggle="collapse" href="#<?php echo $header_shipping['order_detail_id']; ?>" aria-expanded="true" aria-controls="collapseOne" class="response-button pull-right"><span id="pending_collapse<?php echo $header_shipping['order_detail_id']; ?>"><?php echo $button_show; ?> <i class="fa fa-chevron-circle-down"></i></span></a>
						</td>
					</tr>	
					
				</table>
				<hr class="dashed">
				<br/>
				
				<!-- Common shipping data -->
				<div id="<?php echo $header_shipping['order_detail_id']; ?>" class="panel-collapse collapse pending" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
				<table class="table table-bordered table-hover list">
				<thead>
					<tr>
						<td class="text-left" rowspan="6" colspan="2"><?php echo $column_destination; ?></td>
						<td class="text-left" style="width: 25%;"><?php echo $column_quantity; ?></td>
						<td class="text-left" style="width: 25%;"><?php echo $column_shipping_price; ?></td>
				  </tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left" rowspan="6" colspan="2" style="width:50%; text-align: left;">
						<?php if($header_shipping['delivery_type_id'] == 1) { ?>
							<?php echo $header_shipping['shipping_address']; ?>
						<?php } else if($header_shipping['delivery_type_id'] == 2) { ?>
							<?php echo $header_shipping['address_pp']; ?>
						<?php } ?>
						</td>
						<td class="text-right" style="text-align: left;"><?php echo $header_shipping['total_qty']; ?> (<?php echo sprintf('%0.2f', $header_shipping['total_weight']); ?> <?php echo $header_shipping['weight']; ?>)</td>
						<td class="text-right" style="text-align: left;">
						<?php if($header_shipping['delivery_type_id'] == 1) { ?>
						<span class="highlight bold"><?php echo $header_shipping['shipping_name']; ?> - <?php echo $header_shipping['shipping_service_name']; ?></span>
						<?php } else if($header_shipping['delivery_type_id'] == 2) { ?>
						<span class="highlight bold"><?php echo $header_shipping['delivery_type']; ?> - <?php echo $header_shipping['pp_branch_name']; ?></span>
						<?php } ?>
						<br class="wide"/>
						<br class="wide"/>
						<?php echo $header_shipping['shipping_price']; ?>
						</td>
					</tr>
					<tr>
						<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" >Terima Sebagian</td>
						<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" ><?php echo $column_insurance?></td>
					</tr>
					<tr>
						<td class="text-left" style="text-align: left;">Tidak</td>
						<td class="text-left" style="text-align: left;"><?php echo $header_shipping['shipping_insurance']; ?></td>
					</tr>
				</tbody>
				
				
				<!-- Product -->
				<div class="table-responsive">
					
						<tbody>
						<?php 
							foreach ($products as $product) { 
								if ($product['order_detail_id'] == $header_shipping['order_detail_id']){
							?>
							<tr>
								<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['product_name']; ?>" title="<?php echo $product['product_name']; ?>" class="img-thumbnail"/></td>
								<td style="width: 40%; text-align: left;">
									<a href="<?php echo $product['href']; ?>"><?php echo $product['product_name']; ?></a>
								</td>
								<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_remarks; ?></td>
								<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px"><?php echo $column_price; ?></td>
							</tr>
							<tr>
								<td style="text-align: left;"><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?></td>
								<td style="text-align: left;">-</td>
								<td style="text-align: left;"><?php echo $product['total']; ?></td>
							</tr>
						<?php 
								}
							}
						?>
						<tr>
							<td colspan="4">
							<div style="float:left; display:inline">
								<?php echo $column_payment_method; ?><?php echo $header_shipping['payment_method']; ?>
								(<?php echo $header_shipping['total_price']; ?>)
							</div>
							<div style="float:right; display:inline; font-size: 16px;" class="bold">
								<a data-toggle="tooltip" title="<?php echo $text_tooltip; ?>" style="color: black !important;"><i class="fa fa-question-circle"></i></a> <?php echo $column_total; ?><a class="color-response"><?php echo $header_shipping['total_price_invoice']; ?></a>
							</div>
							</td>
						</tr>
						</tbody>
				 </div>
				 </table>
				</div>
				</div>
				
				<script type="text/javascript">
					$('#<?php echo $header_shipping['order_detail_id']; ?>')
					.on('shown.bs.collapse', function () {
							 $('span[id=\'pending_collapse<?php echo $header_shipping['order_detail_id']; ?>\']').html("<?php echo $button_hide; ?> <i class=\'fa fa-chevron-circle-up all_collapse\' ></i>");
					}).on('hidden.bs.collapse', function () {
							 $('span[id=\'pending_collapse<?php echo $header_shipping['order_detail_id']; ?>\']').html("<?php echo $button_show; ?> <i class=\'fa fa-chevron-circle-down all_collapse\' ></i>");
					});
				</script>
				
			<?php 
				$x++;
			} 
			?>
			</div>
			<div class="pending-orders pagination" style="width:100%;"><?php echo $pagination; ?></div>
			<script type="text/javascript">
				$('#response-receive').on('click', function(){
					if(confirm('<?php echo $text_confirm_accept; ?>')){
						$.post("<?php echo $action_many; ?>", $('#collapseAll input[type=\'checkbox\']:checked'), function(data)
						{
							if(data['url']){
								location=data['url'];
								location.reload();
							}
						});
					}
				});
				
				$('#collapseAll')
				.on('shown.bs.collapse', function () {
						 $('span[id=\'pending_collapse_all\']').html("<?php echo $button_hide_all; ?> <i class=\'fa fa-chevron-circle-up some_collapse\' ></i>");
				}).on('hidden.bs.collapse', function () {
						 $('span[id=\'pending_collapse_all\']').html("<?php echo $button_show_all; ?> <i class=\'fa fa-chevron-circle-down some_collapse\' ></i>");
				});
			
				<?php 
				$x = 0;
				foreach($header_shippings as $header_shipping){ ?>
				$(document).ready(function(){ 
					$("#<?php echo $x ?>B").click(function() {
						$("#<?php echo $x ?>C").show(500);
					}); 
					$("#<?php echo $x ?>A").click(function() {
						$("#<?php echo $x ?>C").hide(500);
					}); 
					 
				});	
				
				$('#form_response<?php echo $x; ?>').submit(function() {
					if(document.getElementById("<?php echo $x ?>B").checked == true){
						var c = confirm("<?php echo $text_confirm_decline; ?>");
						return c;
					}
				});
				
				<?php 
						$x++;
					} 
				?>
			</script>
			<?php } else { ?>
				<?php echo $text_no_data_pending; ?>
			<?php } ?>