<?php if ($requests) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-right bold"><?php echo $column_request; ?></td>
            <td class="text-right bold"><?php echo $column_kiosk; ?></td>
            <td class="text-right bold"><?php echo $column_delivery_order; ?></td>
            <td class="text-left bold"><?php echo $column_date_added; ?></td>
            <td class="text-left bold"><?php echo $ms_account_staff_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($requests as $request) { ?>
          <tr>
            <td class="text-right">#<?php echo $request['request_id']; ?></td>
            <td class="text-right"><?php echo $request['kiosk']; ?></td>
            <td class="text-left">
				<?php 
				if($request['delivery_order']){
					echo $request['delivery_order'];
				}else{
					echo "-";
				} 
				?>
			</td>
            <td class="text-left"><?php echo $request['date_added']; ?></td>
            <td>
			<?php if((int)$request['status'] == 0){ ?>
				<a data-target="#modal_edit_request" data-toggle="modal" data-requestid="<?php echo $request['request_id']; ?>" title="<?php echo $button_edit; ?>" class="btn btn-primary edit-request"><i class="fa fa-pencil"></i></a>
			<?php } ?>
			<a href="<?php echo $request['print']; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_print; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
			<a href="<?php echo $request['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
			</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="list-requests pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
	   <p><?php echo $text_no_data_global; ?></p>
	  <?php } ?>