<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $ms_account_transactions_heading; ?></h1>
      <?php echo $content_top; ?>
	
	<div class="product-tab">
	
	<ul id="tabs" class="nav nav-tabs htabs">
		<li class="active"><a href="#tab-balance" data-toggle="tab"><?php echo $ms_account_transactions_records; ?></a></li>
		<!--<li><a href="#tab-payment" data-toggle="tab"><?php echo $ms_payment_payments; ?></a></li>-->
		<li><a href="#tab-report" data-toggle="tab"><?php echo $text_report; ?></a></li>
	</ul>
	
	<div class="tab-content">
		
	<div class="tab-pane tab-content active" id="tab-balance">
		<span style="width:300px; display: inline-block;"><?php echo $ms_account_transactions_balance; ?></span> : <b class="label-number"><?php echo $ms_balance_formatted; ?></b> <span style="color:grey"><?php echo $ms_reserved_formatted; ?></span><br />
		<span style="width:300px; display: inline-block;"><?php echo $ms_account_transactions_earnings; ?></span> : <b class="label-number"><?php echo $earnings; ?></b>
	 
		  <!-- BALANCE RECORDS -->
		<div class="content my-records">
		<div class="row">
			<div class="col-sm-9">
			<div class="form-group">
				<label class="bold"><?php echo $text_transaction_date; ?></label>
				<div class="row">
				<div class="col-sm-5">
				<div class="input-group date" >
					<input name="input_date_start_balance" value="" placeholder="<?php echo $text_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_start_balance">
					<span class="input-group-btn" style="display: table-cell !important; ">
					<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
				</div>
				<div class="col-sm-1" style="line-height:34px;padding-left:5px;padding-right:5px;" >
				<?php echo $text_to; ?>
				</div>
				<div class="col-sm-5">
					<div class="input-group date" >
						<input name="input_date_end_balance" value="" placeholder="<?php echo $text_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_end_balance">
						<span class="input-group-btn" style="display: table-cell !important; ">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
				</div>
			</div>
			</div>
			<div class="col-sm-3">
			<label class="bold"> </label>
				<div class="input-group">
					<a data-toggle="tooltip" title="<?php echo $text_search; ?>" id="btn_search_balance" class="btn btn-danger btn-primary" style="margin-right: 5px;"><i class="fa fa-search"></i></a> 
					<a id="download_report_balance" data-toggle="tooltip" title="<?php echo $ms_download; ?>" class="btn btn-info btn-primary"><i class="fa fa-download"></i></a>
				</div>
			</div>
		</div>
		<br class="wide" />
		<br class="wide" />
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
			<div id="list-balances">
			
			</div>
		</div>
	</div>
	
	<!--
	<div class="tab-pane tab-content" id="tab-payment">
		<h2 class="secondary-title"><?php echo $ms_payment_payments; ?></h2>
		<div class="content my-payments">
		<div class="row">
			<div class="col-sm-9">
			<div class="form-group">
				<label class="bold"><?php echo $text_date_paid; ?></label>
				<div class="row">
				<div class="col-sm-5">
				<div class="input-group date" >
					<input name="input_date_start_payment" value="" placeholder="<?php echo $text_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_start_payment">
					<span class="input-group-btn" style="display: table-cell !important; ">
					<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
				</div>
				<div class="col-sm-1" style="line-height:34px;padding-left:5px;padding-right:5px;" >
				<?php echo $text_to; ?>
				</div>
				<div class="col-sm-5">
					<div class="input-group date" >
						<input name="input_date_end_payment" value="" placeholder="<?php echo $text_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_end_payment">
						<span class="input-group-btn" style="display: table-cell !important; ">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
				</div>
			</div>
			</div>
			<div class="col-sm-3">
			<label class="bold"> </label>
				<div class="input-group">
					<a data-toggle="tooltip" title="<?php echo $text_search; ?>" id="btn_search_payment" class="btn btn-danger btn-primary" style="margin-right: 5px;"><i class="fa fa-search"></i></a>
					<a  id="download_report_payment" data-toggle="tooltip" title="<?php echo $ms_download; ?>" class="btn btn-info btn-primary"><i class="fa fa-download"></i></a>
				</div>
			</div>
		</div>
		<br class="wide" />
		<br class="wide" />
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
			<div id="list-payments">
			
			</div>
		</div>

	</div>
	-->

	<div class="tab-pane tab-content" id="tab-report">
		<div class="row">
			<div class="col-sm-12">
			<div class="form-group">
				<label class="bold"><?php echo $text_date_modified; ?></label>
				<div class="row">
				<div class="col-sm-5">
				<div class="input-group date">
					<input name="input_date_start_report" value="" placeholder="<?php echo $text_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_start_report">
					<span class="input-group-btn" style="display: table-cell !important; ">
					<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
				</div>
				<div class="col-sm-1" style="line-height:34px;padding-left:5px;padding-right:5px;" >
				<?php echo $text_to; ?>
				</div>
				<div class="col-sm-5">
					<div class="input-group date">
						<input name="input_date_end_report" value="" placeholder="<?php echo $text_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control date" type="text" id="date_end_report">
						<span class="input-group-btn" style="display: table-cell !important; ">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
				</div>
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
			<label class="bold"><?php echo $text_batch; ?></label>
				<div class="col-sm-12" style="padding-left:0px">
					<select name="batch" id="batch" class="form-control">
						<option value="" selected="selected"><?php echo $text_batch_select; ?></option>
						<?php foreach ($batchs as $batch) { ?>
						<option value="<?php echo $batch['batch_id']; ?>"><?php echo $batch['batch']; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			</div>
			<div class="col-sm-3">
			<label class="bold"> </label>
				<div class="input-group">
					<a id="btn_search_report" data-toggle="tooltip" title="<?php echo $text_search; ?>" class="btn btn-danger btn-primary" style="margin-right: 5px;"><i class="fa fa-search"></i></a>
					<a id="download_report_transaction" data-toggle="tooltip" title="<?php echo $ms_download; ?>" class="btn btn-info btn-primary"><i class="fa fa-download"></i></a>
				</div>
			</div>
		</div>
		<br class="wide" />
		<br class="wide" />
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
			<div id="list-reports">
			
			</div>
	</div>
	</div>
	</div>
		<div class="buttons">
			<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><span><?php echo $button_back; ?></span></a></div>
		</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<script>
/*keep the selected tab on page load*/
 $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
	$(window).scrollTop(0);
});
var hash = window.location.hash;
$('#tabs a[href="' + hash + '"]').tab('show');

$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});

$(function() {
	
	$('.pcAttention').show();
	$('#list-payments').load('<?php echo $link_list_payments; ?>',function(){$('.pcAttention').hide();});
	$('#list-reports').load('<?php echo $link_list_reports; ?>',function(){$('.pcAttention').hide();});
	$('#list-balances').load('<?php echo $link_list_balances; ?>',function(){$('.pcAttention').hide();});
	
	$('#date_start_balance').datetimepicker({
		pickTime: false,
		maxDate: new Date()
	}).on("dp.change", function (e) {
		$('#date_end_balance').data("DateTimePicker").setMinDate(e.date);
    });
    $('#date_end_balance').datetimepicker({
		useCurrent: false, //Important! See issue #1075
		pickTime: false,
		maxDate: new Date()
    });
	
	$('#date_start_report').datetimepicker({
		pickTime: false,
		maxDate: new Date()
	}).on("dp.change", function (e) {
		$('#date_end_report').data("DateTimePicker").setMinDate(e.date);
    });
    $('#date_end_report').datetimepicker({
		useCurrent: false, //Important! See issue #1075
		pickTime: false,
		maxDate: new Date()
    });
	
	$('#date_start_payment').datetimepicker({
		pickTime: false,
		maxDate: new Date()
	}).on("dp.change", function (e) {
		$('#date_end_payment').data("DateTimePicker").setMinDate(e.date);
    });
    $('#date_end_payment').datetimepicker({
		useCurrent: false, //Important! See issue #1075
		pickTime: false,
		maxDate: new Date()
    });
	
	// $('#list-transactions').dataTable( {
		// "sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-transaction/getTransactionData",
		// "aoColumns": [
			// { "mData": "transaction_id" },
			// { "mData": "amount" },
			// { "mData": "description", "bSortable": false },
			// { "mData": "date_created" }
		// ],
        // "aaSorting":  [[3,'desc']]
	// });

	// $('#list-payments').dataTable( {
		// "sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-transaction/getPaymentData",
		// "aoColumns": [
			// { "mData": "payment_id" },
			// { "mData": "payment_type" },
			// { "mData": "amount" },
			// { "mData": "description", "bSortable": false },
			// { "mData": "payment_status" },
			// { "mData": "date_created" },
		// ],
        // "aaSorting":  [[5,'desc']]
	// });
	
	$( document ).ajaxComplete(function() {
		$('.list-payments.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-payments').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
		$('.list-reports.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-reports').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
		$('.list-balances.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-balances').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	});
	
	var valDate = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
	
	$("#btn_search_payment").on('click', function(){
		if (($('#date_start_payment').val() != '' && !$('#date_start_payment').val().match(valDate)) || ($('#date_end_payment').val() != '' && !$('#date_end_payment').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
		}else if($('#date_start_payment').val() == '' && $('#date_end_payment').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else if($('#date_start_payment').val() != '' && $('#date_end_payment').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else{
			$('.pcAttention').show();
			$('#list-payments').load('<?php echo $search_payment; ?>&date_start_payment='+$('#date_start_payment').val() + '&date_end_payment='+$('#date_end_payment').val() ,function(){$('.pcAttention').hide();});
		}
		
	});
	
	$("#btn_search_report").on('click', function(){
		if (($('#date_start_report').val() != '' && !$('#date_start_report').val().match(valDate)) || ($('#date_end_report').val() != '' && !$('#date_end_report').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
		}else if($('#date_start_report').val() != '' && $('#date_end_report').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else if($('#date_start_report').val() == '' && $('#date_end_report').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else{
			$('.pcAttention').show();
			$('#list-reports').load('<?php echo $search_report; ?>&date_start_report='+$('#date_start_report').val() + '&date_end_report='+$('#date_end_report').val()+ '&batch='+$('#batch').val() ,function(){$('.pcAttention').hide();});
		}
	});
	
	$("#btn_search_balance").on('click', function(){
		if (($('#date_start_balance').val() != '' && !$('#date_start_balance').val().match(valDate)) || ($('#date_end_balance').val() != '' && !$('#date_end_balance').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
		}else if($('#date_start_balance').val() != '' && $('#date_end_balance').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else if($('#date_start_balance').val() == '' && $('#date_end_balance').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
		}else{
			$('.pcAttention').show();
			$('#list-balances').load('<?php echo $search_balance; ?>&date_start_balance='+$('#date_start_balance').val() + '&date_end_balance='+$('#date_end_balance').val() ,function(){$('.pcAttention').hide();});
		}
	});
});
$(document).ready(function(){
	var valDate = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
	
    $('#download_report_payment').click(function(){
		if (($('#date_start_payment').val() != '' && !$('#date_start_payment').val().match(valDate)) || ($('#date_end_payment').val() != '' && !$('#date_end_payment').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
			$("#download_report_payment").removeAttr("href");
		}else if($('#date_start_payment').val() != '' && $('#date_end_payment').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_payment").removeAttr("href");
		}else if($('#date_start_payment').val() == '' && $('#date_end_payment').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_payment").removeAttr("href");
		}else{
			$('#download_report_payment').attr('href', '<?php echo $link_report_payment; ?>&date_start_payment='+$('#date_start_payment').val() + '&date_end_payment='+$('#date_end_payment').val());
		}
    });
	$('#download_report_transaction').click(function(){
		if (($('#date_start_report').val() != '' && !$('#date_start_report').val().match(valDate)) || ($('#date_end_report').val() != '' && !$('#date_end_report').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
			$("#download_report_transaction").removeAttr("href");
		}else if($('#date_start_report').val() != '' && $('#date_end_report').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_transaction").removeAttr("href");
		}else if($('#date_start_report').val() == '' && $('#date_end_report').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_transaction").removeAttr("href");
		}else{
			$('#download_report_transaction').attr('href', '<?php echo $link_report_transaction; ?>&date_start_report='+$('#date_start_report').val() + '&date_end_report='+$('#date_end_report').val()+ '&batch='+$('#batch').val());
		}
    });
	$('#download_report_balance').click(function(){
		if (($('#date_start_balance').val() != '' && !$('#date_start_balance').val().match(valDate)) || ($('#date_end_balance').val() != '' && !$('#date_end_balance').val().match(valDate))){
			alert("<?php echo $ms_warning_date_format; ?>");
			$("#download_report_balance").removeAttr("href");
		}else if($('#date_start_balance').val() != '' && $('#date_end_balance').val() == ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_balance").removeAttr("href");
		}else if($('#date_start_balance').val() == '' && $('#date_end_balance').val() != ''){
			alert("<?php echo $ms_warning_date_picker; ?>");
			$("#download_report_balance").removeAttr("href");
		}else{
			$('#download_report_balance').attr('href', '<?php echo $link_report_balance; ?>&date_start_balance='+$('#date_start_balance').val() + '&date_end_balance='+$('#date_end_balance').val());
		}
    });
});

$('input[name=\'input_date_end_payment\'],input[name=\'input_date_start_payment\'],input[name=\'input_date_start_balance\'],input[name=\'input_date_end_balance\'],input[name=\'input_date_start_report\'],input[name=\'input_date_end_report\']').keypress(function (e){
	var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});
</script>

<?php echo $footer; ?>
