<?php if ($products) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
			<td><?php echo $ms_account_products_image; ?></td>
			<td><?php echo $ms_account_products_product; ?> <a id="sort-name-asc" href="<?php echo $sort_product."&sort=pd.name&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-name-desc" href="<?php echo $sort_product."&sort=pd.name&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
			<td> Diskon Merchant <a id="sort-price-asc" href="<?php echo $sort_product."&sort=p.price&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-price-desc" href="<?php echo $sort_product."&sort=p.price&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
      <td>Diskon OK <a id="sort-price-asc" href="<?php echo $sort_product."&sort=p.price&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-price-desc" href="<?php echo $sort_product."&sort=p.price&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
      <td><?php echo $ms_account_product_price; ?> <a id="sort-price-asc" href="<?php echo $sort_product."&sort=p.price&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-price-desc" href="<?php echo $sort_product."&sort=p.price&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
			<td><?php echo $ms_account_products_sales; ?> <a id="sort-sales-asc" href="<?php echo $sort_product."&sort=mp.number_sold&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-sales-desc" href="<?php echo $sort_product."&sort=mp.number_sold&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
			<td><?php echo $ms_account_products_status; ?></td>
			<td><?php echo $ms_account_products_date; ?> <a id="sort-date-asc" href="<?php echo $sort_product."&sort=p.date_added&direction=asc"; ?>"><span class="fa fa-caret-up"></span></a> <a id="sort-date-desc" href="<?php echo $sort_product."&sort=p.date_added&direction=desc"; ?>"><span class="fa fa-caret-down"></span></a></td>
			<td class="large"><?php echo $ms_account_products_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="text-right"><?php echo $product['image']; ?></td>
			      <td class="text-right"><?php echo $product['name']; ?></td>
            <td class="text-right"><?php echo $product['specialmerchant']; ?></td>
            <td class="text-right"><?php echo $product['specialok']; ?></td>
            <td class="text-right"><?php echo $product['price']; ?></td>
            <td class="text-left"><?php echo $product['number_sold']; ?></td>
            <!-- <td class="text-left"><?php echo $product['product_earnings']; ?></td> -->
            <td class="text-left"><?php echo $product['product_status']; ?></td>
            <td class="text-left"><?php echo $product['date_created']; ?></td>
            <!-- <td class="text-left"><?php echo $product['list_until']; ?></td> -->
            <td class="text-left"><?php echo $product['actions']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
	  <div class="pull-right"><?php echo $ms_catalog_seller_profile_totalproducts ?> <?php echo $product_total; ?></div>
      <div class="list-products pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
	   <p><?php echo $text_no_data_global; ?></p>
	  <?php } ?>