<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="alert alert-danger warning main" style="display:none"><div id="error"></div></div>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <div class="content">
      <form method="post" enctype="multipart/form-data" class="form-horizontal" id="ms-sellerinfo">
        <fieldset>
          <h2 class="secondary-title"><?php echo $text_password; ?></h2>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
			  <input type="hidden" name="code" value="<?php echo $code; ?>"/>
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
            </div>
          </div>
        </fieldset>
        
      </form>
	  <div class="buttons">
          <div class="pull-right">
            <button class="btn btn-primary button" id="ms-submit-button" ><?php echo $button_submit; ?></button>
          </div>
        </div>
	  <script type="text/javascript">
	  $(function() {
		$("#ms-submit-button").click(function() {
			$('.success').remove();
			var button = $(this);
			var id = $(this).attr('id');

			$.ajax({
				type: "POST",
				dataType: "json",
				url: $('base').attr('href') + 'index.php?route=seller/password/jxSubmit',
				data: $("form#ms-sellerinfo").serialize(),
				beforeSend: function() {
					button.hide();
					$('p.error').remove();
				},
				complete: function(jqXHR, textStatus) {
					if (textStatus != 'success') {
						button.show().prev('span.wait').remove();
						window.scrollTo(0,0);
					}
				},
				success: function(jsonData) {
					if (!jQuery.isEmptyObject(jsonData.errors)) {
						$('#ms-submit-button').show().prev('span.wait').remove();
						var html='';
						var error;
						for (error in jsonData.errors) {
							html += "<p>"+jsonData.errors[error]+"</p>";
						}
						$('#error').empty().html(html);
						$('.warning').show();
					} else {
						window.location = jsonData.redirect;
					}
				}
			});
		});
	});
	  </script>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?> 