<?php if ($reports) { ?>
	  <div style="width:100%;overflow-x: scroll;">
      <table class="table table-bordered table-hover list">
        <thead >
          <tr>
            <td style="text-align: center;min-width:200px;"><?php echo $column_batch; ?></td>
            <td style="text-align: center;min-width:250px"><?php echo $column_order_no; ?></td>
            <td style="text-align: center;min-width:100px"><?php echo $column_kiosk_id; ?></td>
            <td style="text-align: center;min-width:150px"><?php echo $column_transaction_date; ?></td>
            <td style="text-align: center;min-width:50px"><?php echo $column_quantity; ?></td>
            <td style="text-align: center;min-width:200px"><?php echo $column_sku; ?></td>
            <td style="text-align: center;min-width:150px"><?php echo $column_total_report; ?></td>
            <td style="text-align: center;min-width:150px"><?php echo $column_shipping; ?></td>
            <td style="text-align: center;min-width:150px"><?php echo $column_online_payment; ?></td>
            <td style="text-align: center;min-width:150px"><?php echo $column_vendor_balance; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($reports as $report) { ?>
		  <tr>
					<td class="text-center" style="text-align:left;padding-left:5px;"><?php echo $report['batch_id']; ?></td>
					<td class="text-center" style="text-align:left;padding-left:5px;"><?php echo $report['invoice']; ?></td>
					<td class="text-center" class="text-center"><?php if($report['kiosk_name']){ echo $report['kiosk_name']; }else{ echo "-";} ?></td>
					<td class="text-center"><?php echo $report['transaction_date']; ?></td>
					<td class="text-center"><?php echo $report['quantity']; ?></td>
					<td class="text-center"><?php echo $report['sku']; ?></td>
					<td class="text-right" style="text-align:right;padding-right:5px;"><?php echo $report['total']; ?></td>
					<td class="text-right" style="text-align:right;padding-right:5px;"><?php echo $report['shipping_fee']; ?></td>
					<td class="text-right" style="text-align:right;padding-right:5px;"><?php echo $report['online_payment_fee']; ?></td>
					<td class="text-right" style="text-align:right;padding-right:5px;"><?php echo $report['vendor_balance']; ?></td>
				</tr>
          <?php } ?>
        </tbody>
      </table>
	  </div>
      <div class="list-reports pagination"><?php echo $pagination; ?></div>
<?php }elseif($error!=''){ ?>
	<p><?php echo $error; ?></p>
<?php } else { ?>
	<p><?php echo $text_no_data_global; ?></p>
<?php } ?>