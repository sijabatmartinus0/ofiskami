<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if (isset($success) && ($success)) { ?>
		<div class="alert alert-success success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <?php if (isset($error_warning) && $error_warning) { ?>
  	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading; ?></h1>
	  <?php echo $content_top; ?>
	   <div id="list_product_comments">
				
				</div>
				<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<?php $timestamp = time(); ?>
<script>
	var msGlobals = {
		timestamp: '<?php echo $timestamp; ?>',
		/*token : '<?php echo md5($salt . $timestamp); ?>',*/
		session_id: '<?php echo session_id(); ?>',
		product_id: '<?php echo $product['product_id']; ?>',
		text_delete: '<?php echo htmlspecialchars($ms_delete, ENT_QUOTES, "UTF-8"); ?>',
		text_none: '<?php echo htmlspecialchars($ms_none, ENT_QUOTES, "UTF-8"); ?>',
		uploadError: '<?php echo htmlspecialchars($ms_error_file_upload_error, ENT_QUOTES, "UTF-8"); ?>',
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
		formNotice: '<?php echo htmlspecialchars($ms_error_form_notice, ENT_QUOTES, "UTF-8"); ?>',
		config_enable_rte: '<?php echo $this->config->get('msconf_enable_rte'); ?>',
		config_enable_quantities: '<?php echo $this->config->get('msconf_enable_quantities'); ?>'
	};
</script>
<script type="text/javascript">	
$(function(){
	$('.pcAttention').show();
	$('#error_discuss').hide();
	$('#list_product_comments').load('<?php echo $list_product_comments; ?>&product_id=<?php echo $product_id; ?><?php echo (isset($topic_id)?"&topic_id=".$topic_id : ""); ?>',function(){$('.pcAttention').hide();});
	
	$(document).ready(function(){
		$("#pcSubmitBtn").click(function(){
		var comment = $("#comment_content").val();
		var product_id = $("#comment_product_id").val();
			
			$('.pcAttention').show();
			 $.ajax({
				url: 'index.php?route=seller/account-product/commentProduct',
				type: "POST",
				cache: false,
				data: 
				{ 
					comment: comment, 
					product_id: product_id
				},
				success : function(html){
					console.log(222222222222222222222222222222);
					if($("#comment_content").val().length < 10){
						$('#error_discuss').show();
						$('.pcAttention').hide();
					}else{
						$('#error_discuss').hide();
						$('.pcAttention').hide();
						loadTopics();
						$("#comment_content").val("");
					}
				}
			})
		});
		
		function loadTopics(){
			$('#list_product_comments').load('<?php echo $list_product_comments; ?>&product_id=<?php echo $product_id; ?><?php echo (isset($topic_id)?"&topic_id=".$topic_id : ""); ?>');
		}
		
		$('.thumbnails').magnificPopup({
			type:'image',
			delegate: 'a',
			gallery: {
				enabled:true
			}
		});
		
		$("#new_topic").click(function() {
			$("#comment_form").show(500);
		}); 
	}); 
	
	$( document ).ajaxComplete(function() {
		$('.product-comments.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list_product_comments').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	});
});
</script>
<?php echo $footer; ?>