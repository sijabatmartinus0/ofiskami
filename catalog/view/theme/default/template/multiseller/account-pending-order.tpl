<?php echo $header; ?>

<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if (isset($success) && ($success)) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
 
  <?php if (isset($error) && ($error)) { ?>
  <div class="alert alert-warning warning"><i class="fa fa-check-circle"></i> <?php echo $error; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>
			
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $ms_account_dashboard_nav_pending_order; ?></h1>
	<div class="product-tab">
		<ul id="tabs" class="nav nav-tabs htabs">
			<li class="active"><a href="#tab-new-order" data-toggle="tab"><?php echo $tab_new_order; ?></a></li>
			<li><a href="#tab-confirm-shipping" data-toggle="tab"><?php echo $tab_confirm_shipping; ?></a></li>
			<li><a href="#tab-status-shipping" data-toggle="tab"><?php echo $tab_status_shipping; ?></a></li>
		</ul>
	
		<div class="tab-content">
			<div class="tab-pane tab-content active" id="tab-new-order">
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
			<div id="list_pending_orders">
			
			</div>
			</div>
			
			<div class="tab-pane tab-content" id="tab-confirm-shipping">
			<div class="well" style="background-color: #F5F5F5;">
			<div class="row">
			<div class="col-sm-3" style="width: 250px;">
			<div class="form-group">
				<label><?php echo $text_search_invoice; ?></label>
				<input type="text" placeholder="<?php echo $text_search_invoice; ?>" value="" name="search_invoice" id="search_invoice" class="form-control"/>
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				<label><?php echo $text_deadline; ?></label>
				<select class="form-control" value="" id="search_deadline" style="width: 200px;">
					<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php for($deadline = $this->config->get('config_sla_response'); $deadline >= 0; $deadline--) {?>
						<option value="<?php echo $deadline; ?>"><?php echo $deadline; ?><?php echo $text_days; ?></option>
					<?php } ?>
				</select>
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				<label><?php echo $column_delivery; ?></label>
				<select class="form-control" value="" id="search_delivery_agent" style="width: 200px;">
					<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
					<?php foreach($logistics as $logistic){ ?>
					<option value="<?php echo $logistic['shipping_id']; ?>"><?php echo $logistic['name']; ?></option>
					<?php } ?>
				</select>
			</div>
			</div>
			<div class="col-sm-1">
			<div class="form-group">
				<button class="btn btn-primary pull-right button" type="button" style="margin-top: 25px;" id="button_search"><span class="fa fa-search"></button>
			</div>
			</div>
			</div>
			
			</div>
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
			<div id="list_order_release">
			
			</div>
			</div>
			<div class="tab-pane tab-content" id="tab-status-shipping">
				<div class="row">
					<div class="col-sm-3">
					<div class="form-group">
						<button class="btn btn-primary pull-right button pull-left" type="button" style="margin-top: 5px;" id="button_search_all"><span class="fa fa-search">
						</button>
					</div>
					</div>
					<div class="col-sm-3">
					<div class="form-group" style="margin-left: -210px;">
						<input name="search_all" id="search_all" value="" placeholder="<?php echo $text_search_all; ?>"  class="form-control input-lg" type="text" style="width:80%; margin: 4px 0px 0px 45px;">
					</div>
					</div>
				</div>
				
				<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
				<br class="wide">
				<div id="account_shipping_status">
				
				</div>
			</div>
		</div>
	</div>
    <div class="buttons clearfix">
		<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a></div>
    </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; right: 150px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h1><?php echo $text_detail_order; ?></h1>
			</div>
			<div class="modal-body" >
			<div class="pcAttention2" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>	
			<div id="account_order_release_detail">
				
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" ><?php echo $ms_sellercontact_close; ?></button>
		</div>
	</div>
</div>
</div>
				
<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h1><?php echo $tab_confirm_shipping; ?></h1>
			</div>
			<div class="modal-body" >
			<form action="<?php echo $action_insert_receipt; ?>" id="form_save_receipt" method="post" enctype="multipart/form-data" >
			<input type="hidden" id="order_detail_id_modal" name="input_order_detail_id_release">
			<input type="hidden" id="order_id_modal" name="input_order_id_release">
			<?php echo $text_change_delivery; ?>
			<div class="distance">
			<input type="radio" name="rbt_change_delivery" value="1"><?php echo $text_yes; ?>
			<input type="radio" name="rbt_change_delivery" value="0" checked="checked"><?php echo $text_no; ?>	
			<br class="wide">
			</div>
			<div style="display: none;" id="1">
			<label class="same-distance"><?php echo $column_delivery; ?></label>
			<select id="combobox-month" value="" name="input_delivery_agent" class="form-control">
				<option value="">--<?php echo $text_choose; ?>--</option>
				<?php foreach($logistics as $logistic){ ?>
				<option value="<?php echo $logistic['shipping_id']; ?>-<?php echo $logistic['name']; ?>"><?php echo $logistic['name']; ?></option>
				<?php } ?>
			</select>
			<br class="wide">
			<label class="same-distance"><?php echo $text_shipping_service; ?></label>
			<select id="input_shipping_service" name="input_shipping_service" class="form-control">
				
			</select><div class="error"></div>
			<br class="wide">
			</div>
			<br class="wide">
			<?php echo $text_receipt_number; ?><br>
			<input class="form-control" type="text" id="input_receipt_number" name="input_receipt_number" placeholder="<?php echo $text_delivery_code; ?>" style="width: 550px;"><div class="error"></div>
			<!--<?php if ($error_input_receipt_number) { ?>
				<div class="text-danger"><?php echo $error_receipt_number; ?></div>
			<?php } ?>-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;"><?php echo $text_no; ?></button>
				<button type="button" class="btn btn-primary" style="font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;" id="button_save_receipt"><?php echo $text_yes; ?></button>
			</div>
			</form>
		</div>
    </div>
</div>

<div class="modal fade" id="modal_shipping_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h1><?php echo $text_edit_receipt; ?></h1>
			</div>
			<div class="modal-body" >
			<form action="<?php echo $action_edit_receipt; ?>" id="form_edit_receipt" method="post">
				<input type="hidden" id="order_detail_id_shipping" name="input_order_detail_id_release">
				<input type="hidden" id="order_id_shipping" name="input_order_id_release">
				<label><?php echo $text_receipt_number; ?></label><br>
				<input type="text" name="input_receipt_number" id="md_edit_receipt_number" placeholder="<?php echo $text_delivery_code; ?>" style="width: 550px;">
				<div class="error"></div>
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;"><?php echo $button_cancel; ?></button>
				<button type="button" class="btn btn-primary button" style="line-height:33px;font-weight: 700; font-family: 'Roboto'; font-style: normal;font-size: 14px;" id="button_edit_receipt"><?php echo $button_save; ?></button>

			</div>
			</form>
	</div>
</div>
</div>

<script type="text/javascript">
/*keep the selected tab on page load*/
 $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
	$(window).scrollTop(0);
});
var hash = window.location.hash;
$('#tabs a[href="' + hash + '"]').tab('show');

$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});

function toggle(source) {
  checkboxes = document.getElementsByName('check_accept[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}

function toggle2(source) {
  checkboxes = document.getElementsByName('check_confirm[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}

$(function(){
	$('.pcAttention').show();
	
	$('#list_pending_orders').load('<?php echo $list_pending_orders; ?>',function(){$('.pcAttention').hide();});
	$('#list_order_release').load('<?php echo $list_order_release; ?>',function(){$('.pcAttention').hide();});
	$('#account_shipping_status').load('<?php echo $account_shipping_status; ?>',function(){$('.pcAttention').hide();});
	
		
	$( document ).ajaxComplete(function() {
		$(".btn.btn-primary.response-button.link_print").on('click', function(){
			$('.btn.btn-primary.response-button.link_print').attr('href', '<?php echo $print; ?>&order_detail_id='+$(this).data('orderdetailid'));
		});
		
		$(".link_modal_confirm").on('click', function(){
			$('#order_detail_id_modal').val($(this).data('detailid'));
			$('#order_id_modal').val($(this).data('orderid'));
		});

		$(".btn.btn-primary.button-primary.link_modal_shipping").on('click', function(){
			$('#order_detail_id_shipping').val($(this).data('detailid'));
			$('#order_id_shipping').val($(this).data('orderid'));
		});
		
		$(".view_detail").on('click', function(){
			$('.pcAttention2').show();
			$('#account_order_release_detail').empty().load('<?php echo $view_detail; ?>&order_id_release=' + $(this).data('orderid') + '&order_detail_id_release=' + $(this).data('detailid'),function(){$('.pcAttention2').hide();});
		});
		
		$('.pending-orders.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list_pending_orders').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
		
		$('.order-release.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list_order_release').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
		
		$('.shipping-status.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#account_shipping_status').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	});
	
	$("#button_search_all").on('click', function(){
		$('.pcAttention').show();
		$('#account_shipping_status').load('<?php echo $search_account_shipping_status; ?>&search_all='+$('#search_all').val() ,function(){$('.pcAttention').hide();});
	});
	
	$("#button_search").on('click', function(){
		$('.pcAttention').show();
		$('#list_order_release').load('<?php echo $search_order_release; ?>&search_invoice='+$('#search_invoice').val()+'&search_delivery_agent='+$('#search_delivery_agent').val()+'&search_deadline='+$('#search_deadline').val(), function(){$('.pcAttention').hide();});
	});
	
	$("#button_save_receipt").on('click', function(){
		$('.error').empty();
		$.post("<?php echo $action_insert_receipt; ?>", $('#form_save_receipt input[type=\'text\'], #form_save_receipt input[type=\'hidden\'], #form_save_receipt input[type=\'radio\']:checked, #form_save_receipt input[type=\'checkbox\']:checked, #form_save_receipt select'), function(data)
		{
			if(data['error']){
				if(data['error']['input_receipt_number']){
					$('#input_receipt_number').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['input_receipt_number']+"</strong>");
				}
				if(data['error']['input_shipping_service']){
					$('#input_shipping_service').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['input_shipping_service']+"</strong>");
				}
			}else if(data['url']){
				location=data['url'];
				location.reload();
			}
		});
	});
	
	$("#button_edit_receipt").on('click', function(){
		$.post("<?php echo $action_edit_receipt; ?>", $('#form_edit_receipt input[type=\'text\'], #form_edit_receipt input[type=\'hidden\'], #form_edit_receipt input[type=\'radio\']:checked, #form_edit_receipt input[type=\'checkbox\']:checked, #form_edit_receipt select'), function(data)
		{
			if(data['error']){
				if(data['error']['edit_receipt_number']){
					$('#md_edit_receipt_number').siblings('.error').html("<br><strong style='color:#FF0000'>"+data['error']['edit_receipt_number']+"</strong>");
				}
			}else if(data['url']){
				location=data['url'];
				location.reload();
			}
		});
	});
});
$("#search_all").keypress(function (e) {
	if (e.keyCode == 13) {
		$('.pcAttention').show();
		$('#account_shipping_status').load('<?php echo $search_account_shipping_status; ?>&search_all='+$('#search_all').val() ,function(){$('.pcAttention').hide();});
	}
});

$(document).ready(function(){ 
    $("input[name='rbt_change_delivery']").click(function() {
        var test = $(this).val();
        $("#1").hide(500);
        $("#"+test).show(500);
    }); 
	 
});

$('select[name=\'input_delivery_agent\']').on('change', function() {
	var temp = new Array();
	temp = ($(this).val()).split("-");
	$.ajax({
		url: 'index.php?route=seller/account-pending-order/shipping_service&shipping_id=' + temp[0],
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'input_delivery_agent\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			
			html = '';
			
			if (json['services'] && json['services'] != '') {
				for (i = 0; i < json['services'].length; i++) {
					html += '<option value="' + json['services'][i]['shipping_service_id'] + '-' + json['services'][i]['name'] + '"';
				
					html += '>' + json['services'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected">--</option>';
			}
			
			$('select[name=\'input_shipping_service\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'search_invoice\']').keypress(function (e){
	var charCode = (e.which) ? e.which : e.keyCode;
		if ((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97)) {
			return false;
		}
	});
	
</script>

<?php echo $footer; ?>
