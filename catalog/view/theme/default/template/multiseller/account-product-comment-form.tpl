<?php if($topic_comments) {
?>
<div class="content-comments">
<?php $y=0; 
foreach($topic_comments as $topic_comment){ 
?>
<div class="panel panel-default">
<div class="panel-collapse collapse in">
<div class="panel-body">
<?php if(count($topic_comment['product_comments']) == 2) { ?>
	<div class="content" style="background-color: beige;">
<?php }else{ ?>
	<div class="content">
<?php } ?>
		<div class="comment-header">
			<span class="comment-name">
				<h4>
				<?php 
					echo $topic_comment['firstname'];
				?>
				<span class="badge green">
					<?php echo $column_customer; ?>
				</span>
			</span>
				<small class="muted"><i><?php echo $topic_comment['created_date'] ?></i></small>
				</h4>
			</span>
		</div>
		<div class="comment-content"><?php echo $topic_comment['topic'] ?></div>
	</div>
	<?php if(count($topic_comment['product_comments']) == 2) { ?>
		<a id="view_more<?php echo $y; ?>" class="pcViewBtn icon-only" style="cursor: pointer;"><span ><?php echo $text_more; ?></span><i style="margin-left: 5px; font-size: 16px" class="fa fa-comments"></i></a>
	<?php } ?>
	<div id="list_comments_category<?php echo $y; ?>">
	
	</div>
	<?php if((int)$this->MsLoader->MsSeller->getSellerId() == (int)$topic_comment['seller_id']){ ?>
		<div id="pcAttention<?php echo $y; ?>" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
		<div class="comment-form">
			<form class="pcForm" method="post" >
				<input type="hidden" id="id_topic<?php echo $y; ?>" name="id_topic" value="<?php echo $topic_comment['id_topic']; ?>">
				<input type="hidden" id="product_id<?php echo $y; ?>" name="product_id" value="<?php echo $topic_comment['product_id']; ?>">
				<textarea class="pcText" id="reply_content<?php echo $y; ?>" name="reply_content" cols="40" rows="8" style="width: 100%;" placeholder="<?php $text_comment; ?>"></textarea>
				<div id="error_reply<?php echo $y; ?>" class="text-danger"><?php echo $error_comment_content; ?></div>
			</form>
			
			<br class="wide"/>
			<div class="right-side">
				<a id="submitReply<?php echo $y; ?>" class="btn btn-primary pull-right button"><span><?php echo $button_submit; ?></span></a>
			</div>
		</div>
		<?php } ?>
</div>
</div>
</div>
<hr />
<script type="text/javascript">
// $(function(){
	$('#error_reply<?php echo $y; ?>').hide();
	$('#list_comments_category<?php echo $y; ?>').load('<?php echo $list_top_comments_per_topic; ?>&id_topic=' +<?php echo $topic_comment['id_topic']; ?>+'&product_id='+<?php echo $product_id; ?>);
	
	// $("#view_more<?php echo $y; ?>").on('click', function(){
		// $('#pcAttention<?php echo $y; ?>').show();
		// $('#list_comments_category<?php echo $y; ?>').load('<?php echo $list_comments_per_topic; ?>&id_topic=' +<?php echo $topic_comment['id_topic']; ?>+'&product_id='+<?php echo $product_id; ?>,function(){$('#pcAttention<?php echo $y; ?>').hide();});
		// $('#view_more<?php echo $y; ?>').hide();
	// });
	
	// $(document).ready(function(){
		$("#view_more<?php echo $y; ?>").on('click', function(){
		$('#pcAttention<?php echo $y; ?>').show();
		$('#list_comments_category<?php echo $y; ?>').load('<?php echo $list_comments_per_topic; ?>&id_topic=' +<?php echo $topic_comment['id_topic']; ?>+'&product_id='+<?php echo $product_id; ?>,function(){$('#pcAttention<?php echo $y; ?>').hide();});
		$('#view_more<?php echo $y; ?>').hide();
	});
		
		
		$("#submitReply<?php echo $y; ?>").click(function(){
			var reply_comment = $("#reply_content<?php echo $y; ?>").val();
			var reply_id_topic = $("#id_topic<?php echo $y; ?>").val();
			var reply_product_id = $("#product_id<?php echo $y; ?>").val();
							
			$('#pcAttention<?php echo $y; ?>').show();
			$.ajax({
				url: 'index.php?route=seller/account-product/replyComment',
				type: "POST",
				cache: false,
				data: 
				{ 
					reply_comment: reply_comment, 
					reply_id_topic: reply_id_topic,
					reply_product_id: reply_product_id
				},
				success : function(html){
					if($("#reply_content<?php echo $y; ?>").val().length < 10){
						$('#error_reply<?php echo $y; ?>').show();
						$('#pcAttention<?php echo $y; ?>').hide();
					}else{
						$('#error_reply<?php echo $y; ?>').hide();
						$('#pcAttention<?php echo $y; ?>').hide();
						// loadComments();
						$('#list_comments_category<?php echo $y; ?>').load('<?php echo $list_comments_per_topic; ?>&id_topic=' + <?php echo $topic_comment['id_topic'] ?>+'&product_id='+<?php echo $product_id; ?>);
						$("#reply_content<?php echo $y; ?>").val("");
					}
				}
			})
		});
					
		// function loadComments(){
			// $('#list_comments_category<?php echo $y; ?>').load('<?php echo $list_comments_per_topic; ?>&id_topic=' + <?php echo $topic_comment['id_topic'] ?>+'&product_id='+<?php echo $product_id; ?>);
		// }
	// }); 
// });				
</script>
<?php 
$y++;
}
?>
<br />
</div>
<div class="product-comments pagination" style="width:100%;"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="panel-heading">
	<h4 class="panel-title">
	<a><?php echo $text_no_data; ?></a>
	</h4>
</div>
<?php } ?>