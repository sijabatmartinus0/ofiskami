<?php if ($balances) {  ?>
      <table class="table table-bordered table-hover list">
        <thead >
          <tr>
            <td class="bold" style="text-align: center;"><?php echo $text_number; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $column_invoice; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_transaction_date; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $ms_description; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_transaction_code; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $ms_amount; ?></td>
            <td class="bold" style="text-align: center;"><?php echo $text_balance; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($balances as $balance) { ?>
          <tr>
            <td class="text-right"><?php echo $balance['no']; ?></td>
			<td class="text-right"><?php echo $balance['invoice']; ?></td>
            <td class="text-right"><?php echo $balance['date_created']; ?></td>
            <td class="text-left" style="text-align: left;"><?php echo $balance['description']; ?></td>
            <td class="text-left"><?php echo $balance['tran_code']; ?></td>
            <td class="text-left"><?php echo $balance['amount']; ?></td>
            <td class="text-left"><?php echo $balance['balance']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="list-balances pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
	<p><?php echo $text_no_data_global; ?></p>
<?php } ?>