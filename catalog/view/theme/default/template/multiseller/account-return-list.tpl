<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $ms_account_dashboard_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_profile; ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->url->link('seller/account-password', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_password; ?>
						</a>
					</li>
					<li>
					<a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_product; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-upload-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_upload_product; ?>
					</a>
					</li>

					<li>
					<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_products; ?>
					</a>
					</li>
			
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_balance; ?>
					</a>
					</li>
					
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li>
					<a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_payout; ?>
					</a>
					</li>
					<?php } ?>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>">
						<?php echo $ms_account_stats; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-pending-order', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_pending_order; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('seller/account-return-list', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_return_list; ?>
					</a>
					</li>
					
					<li>
						<a href="<?php echo $this->url->link('seller/account-staff', '', 'SSL'); ?>">
						<?php echo $ms_account_dashboard_nav_staff; ?>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> ms-account-dashboard order-list"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $text_return_list; ?></h1>
	<div class="well" style="background-color: #F5F5F5;">
		<div class="row">
			<div class="col-sm-3">
			<div class="form-group">
				<label class="bold" for="input-return-id"><?php echo $column_invoice; ?></label>
				<input name="search_invoice" id="search_invoice" value="" placeholder="<?php echo $column_invoice; ?>" class="form-control" type="text">
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				<label class="bold" for="input-order-id"><?php echo $column_customer; ?></label>
				<input name="search_customer" id="search_customer" value="" placeholder="<?php echo $column_customer; ?>" class="form-control" type="text">
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				<label class="bold" for="input-return-id"><?php echo $column_product; ?></label>
				<input name="search_product" id="search_product" value="" placeholder="<?php echo $column_product; ?>" class="form-control" type="text">
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				<label class="bold" for="input-order-id"><?php echo $column_status; ?></label>
				<select class="combo-status form-control" value="" id="search_status">
				<option value="" selected="selected">--<?php echo $text_choose; ?>--</option>
				<?php foreach($status as $return_status){ ?>
					<option value="<?php echo $return_status['return_status_id']; ?>"><?php echo $return_status['name']; ?></option>
				<?php } ?>
				</select>
			   
			</div>
			<button type="button" id="button_filter" class="btn btn-primary pull-right button" style="margin-top: 15px;"><i class="fa fa-search"></i> <?php echo $text_search; ?></button>
			</div>
		</div>
			
	</div>
	<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
	<div id="list-return">
	
	</div>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a></div>
      </div>
      <?php echo $content_bottom; ?>
	  
	  </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$('.pcAttention').show();
	$('#list-return').load('<?php echo $list_return; ?>',function(){$('.pcAttention').hide();});
	
	$( document ).ajaxComplete(function() {
		$('.list-returns.pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#list-return').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	}); 
	
	$("#button_filter").on('click', function(){
		$('.pcAttention').show();
		$('#list-return').load('<?php echo $search_return; ?>&search_invoice=' +$('#search_invoice').val() + '&search_customer='+$('#search_customer').val() + '&search_product='+$('#search_product').val() + '&search_status='+$('#search_status').val(),function(){$('.pcAttention').hide();});
	});
});

$("#search_invoice").keypress(function (e) {
	if (e.keyCode == 13) {
		$('.pcAttention').show();
		$('#list-return').load('<?php echo $search_return; ?>&search_invoice=' +$('#search_invoice').val() + '&search_customer='+$('#search_customer').val() + '&search_product='+$('#search_product').val() + '&search_status='+$('#search_status').val(),function(){$('.pcAttention').hide();});
	}
});

$("#search_customer").keypress(function (e) {
	if (e.keyCode == 13) {
		$('.pcAttention').show();
		$('#list-return').load('<?php echo $search_return; ?>&search_invoice=' +$('#search_invoice').val() + '&search_customer='+$('#search_customer').val() + '&search_product='+$('#search_product').val() + '&search_status='+$('#search_status').val(),function(){$('.pcAttention').hide();});
	}
});

$("#search_product").keypress(function (e) {
	if (e.keyCode == 13) {
		$('.pcAttention').show();
		$('#list-return').load('<?php echo $search_return; ?>&search_invoice=' +$('#search_invoice').val() + '&search_customer='+$('#search_customer').val() + '&search_product='+$('#search_product').val() + '&search_status='+$('#search_status').val(),function(){$('.pcAttention').hide();});
	}
});

</script>

<?php echo $footer; ?>
