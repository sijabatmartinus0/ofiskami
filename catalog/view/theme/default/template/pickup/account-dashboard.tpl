<?php echo $header; ?>
<style>
/* enable absolute positioning */
.inner-addon { 
    position: relative;
    padding-right: 31px;
}

/* style icon */
.inner-addon .fa {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}

/* align icon */
.left-addon .fa  { left:  0px;}
.right-addon .fa { right: 0px;}

/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if (isset($success)) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
	  <div id="column-right" class="col-sm-3 hidden-xs side-column">
		<div class="box side-category side-category-right side-category-accordion">
			<div class="box-heading"><?php echo $pp_heading_nav; ?></div>
			<div class="box-category">
				<ul class="list-unstyled">
					<li>
						<a href="<?php echo $this->url->link('pickup/account-dashboard', '', 'SSL'); ?>">
						<?php echo $pp_account_dashboard_nav; ?>
						</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('pickup/account-history', '', 'SSL'); ?>">
						<?php echo $pp_account_history_nav; ?>
					</a>
					</li>
					
					<li>
					<a href="<?php echo $this->url->link('pickup/account-password', '', 'SSL'); ?>">
						<?php echo $pp_change_password; ?>
					</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php $column_right=true; ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <h1 class="heading-title"><?php echo $heading_title; ?></h1>
	<div class="row">
	
		<form class="form-horizontal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<div class="pull-right">
				<div class="inner-addon left-addon">
					<i class="fa fa-search"></i>
					<input type="text" name="search" class="form-control" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
				</div>
				</div>
				<div class="pull-left">
					<div></div>
				</div>
			</div>
		</form>
	</div>
	  <div class="table-responsive">
        <table class="table table-bordered table-hover list">
          <thead>
            <tr>
              <td class="text-right"><?php echo $label_column_order; ?></td>
			  <td class="text-right"><?php echo $label_column_customer; ?></td>
              <td class="text-left"><?php echo $label_column_recipient; ?></td>
              <td class="text-left"><?php echo $label_column_telephone; ?></td>
              <td class="text-right"><?php echo $label_column_status; ?></td>
              <td class="text-right"><?php echo $label_column_action; ?></td>
            </tr>
          </thead>
          <tbody>
			<?php if(!empty($orders)){?>
				<?php foreach($orders as $order){?>
					<tr>
					  <td class="text-right"><?php echo $order['invoice']; ?></td>
					  <td class="text-right"><?php echo $order['customer']; ?></td>
					  <td class="text-left"><?php echo $order['recipient']; ?></td>
					  <td class="text-left"><?php echo $order['telephone']; ?></td>
					  <td class="text-right"><?php echo $order['status']; ?></td>
					  <td class="text-right">
					  <?php if((int)$order['order_status_id']==19){?>
					  <input type="button" style="width:120px" value="<?php echo $button_receive; ?>" class="btn btn-primary button" onclick="receiveOrder('<?php echo $order['order_detail_id']; ?>')" />
					  <?php } else {?>
					  <input type="button" style="width:120px" value="<?php echo $button_pickup; ?>" class="btn btn-primary button" onclick="pickupOrder('<?php echo $order['order_detail_id']; ?>')" />
					  <?php }?>
					  </td>
					</tr>
				<?php }?>
			<?php } else {?>
            <tr><td colspan="6"><?php echo $text_no_item; ?></td></tr>
			<?php }?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-edit-order" tabindex="-1" role="dialog" aria-labelledby="modal-pickup-label"data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $text_md_edit_order; ?></h4>
      </div>
      <div class="modal-body">
        <div id="modal-order-content">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
        <button type="button" id="md-button-save-order" class="btn btn-primary"><?php echo $button_submit;?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$(function() {
	window.receiveOrder = function receiveOrder(id){
		$.ajax({
			url: 'index.php?route=pickup/account-dashboard/loadReceiveOrder',
			type: 'post',
			data: {id:id},
			dataType: "html",
			beforeSend: function() {
				//$('#button-cart').button('loading');
			},
			complete: function() {
				//$('#button-cart').button('reset');
			},
			success: function(json) {
				$('#modal-order-content').empty().html(json);
				$('#modal-edit-order').modal('show');
			}
		});
	}
	window.pickupOrder = function pickupOrder(id){
		$.ajax({
			url: 'index.php?route=pickup/account-dashboard/loadPickupOrder',
			type: 'post',
			data: {id:id},
			dataType: "html",
			beforeSend: function() {
				//$('#button-cart').button('loading');
			},
			complete: function() {
				//$('#button-cart').button('reset');
			},
			success: function(json) {
				$('#modal-order-content').empty().html(json);
				$('#modal-edit-order').modal('show');
			}
		});
	}
});
//--></script>
<?php echo $footer; ?>
