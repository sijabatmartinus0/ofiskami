<div class="md-error-content"></div>
<div id="content" class="row md-content md-pickup" style="padding: 0px 20px 0 20px !important;">
    <table class="table table-bordered table-hover list">
        <thead>
            <tr>
                <td colspan="2"><i class="fa fa-info" style="padding: 3px;"></i>
                    <?php echo $text_order_detail; ?>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width: 50%;">
				<input type="hidden" name="order_detail_id" value="<?php echo $order['order_detail_id']; ?>" >
                    <?php if ($order['invoice']) { ?>
                        <b><?php echo $text_invoice_no; ?></b>
                        <?php echo $order['invoice']; ?>
                            <br />
                            <?php } ?>
                                <b><?php echo $text_order_id; ?></b> #
                                <?php echo $order['order_id']; ?>
                                    <br />
                                    <b><?php echo $text_date_added; ?></b>
                                    <?php echo $order['date_added']; ?>
                </td>
                <td>
                    <?php if ($order['payment_method']) { ?>
                        <b><?php echo $text_payment_method; ?></b>
                        <?php echo $order['payment_method']; ?>
                            <br />
                            <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
	<table class="table table-bordered table-hover list">
        <thead>
            <tr>
                <td class="text-left" colspan="2"><i class="fa fa-user" style="padding: 3px;"></i>
                    <?php echo $text_order_pickup; ?>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width: 50%;">
                        <b><?php echo $text_order_name; ?></b>
                        <?php echo $order['recipient']; ?>
                        <br />
                        <b><?php echo $text_order_email; ?></b>
                        <?php echo $order['email']; ?>
                        <br />
                        <b><?php echo $text_order_phone; ?></b>
                        <?php echo $order['telephone']; ?>
                </td>
            </tr>
        </tbody>
    </table>
	
	<table class="table table-bordered table-hover list">
			<thead>
				<tr>
					<td colspan="4"><i class="fa fa-th-list" style="padding: 3px;"></i><?php echo $text_order_product; ?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($products as $product) { ?>
				<tr>
					<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail"/></td>
					<td style="width: 40%; text-align: left;">
						<?php echo $text_order_product_name; ?>
					</td>
					<td style="width: 25%;"><?php echo $text_order_product_model; ?></td>
					<td style="width: 25%;"><?php echo $text_order_product_option; ?></td>
				</tr>
				<tr>
					<td style="width: 40%; text-align: left;">
						<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
						<br/>
						<?php echo $product['quantity']; ?> item
					</td>
					<td style="text-align: left;"><?php echo $product['model']; ?></td>
					<td style="text-align: left;">
						<?php if(!empty($product['option'])){ ?>
							<?php foreach($product['option'] as $option){?>
								<br/>
								<?php echo $option; ?>
							<?php } ?>
						<?php } else{ ?>
						--
						<?php } ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
</div>
<script type="">
$("#md-button-save-order").unbind('click');
		$('#md-button-save-order').on('click', function(){
			$.ajax({
                url: 'index.php?route=pickup/account-dashboard/pickup',
				type: 'post',
				data:$('.md-pickup input[type=\'hidden\']'),
                dataType: 'json',
                beforeSend: function() {
					$('.alert').remove();
					$('#md-button-save-order').button('loading');
                },
                complete: function() {
                    $('.fa-spin').remove();
					$('#md-button-save-order').button('reset');
                },
                success: function(json) {
					if (json['error']) {
						var html="";
						if(json['error']){
							html+='<i class="fa fa-exclamation-circle"></i>'+json['error']+'<br>';
						}
						$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>');
					}
					if (json['success']) {
						$('#modal-edit-order').modal('hide');
						location=json['success']['url'];
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
</script>