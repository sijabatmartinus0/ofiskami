<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin:0; padding:0;">
<div style="width: 680px;">
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; margin-bottom: 20px;">
<a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/25184501-a8ee-4ae5-8b8b-c915e6c66374.png" style="padding: 10px;"></a>
</div>
  
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_update_order_status; ?> <b><?php echo $order_status; ?></b></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_update_link; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_link_direct; ?></p>
 
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;" colspan="2"><?php echo $text_order_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
		  <b><?php echo $text_order_id; ?></b> <?php echo $order_id; ?><br />
		  <b><?php echo $text_order_detail_id; ?></b> <?php echo $order_detail_id; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
          <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
        </td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
		  <b><?php echo $text_email; ?></b> <?php echo $email; ?><br />
          <b><?php echo $text_telephone; ?></b> <?php echo $telephone; ?><br />
          <b><?php echo $text_order_status; ?></b> <?php echo $order_status; ?><br />
		  <?php if($order_status_id == 19 && $delivery_type_id == 1){ ?>
		  <b><?php echo $text_receipt_number; ?></b> <?php echo $shipping_receipt_number; ?><br />
		  <?php } ?>
		  <?php if($order_status_id == 23){ ?>
		  <b><?php echo $text_decline_reason; ?></b> <?php echo $reason; ?><br />
		  <?php } ?>
		  </td>
      </tr>
    </tbody>
  </table>
  
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <?php if ($delivery_type_id == 1) { ?>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_shipping_address; ?> (<?php echo $shipping_name; ?> - <?php echo $shipping_service_name; ?>)</td>
        <?php } else if($delivery_type_id == 2){ ?>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_shipping_address; ?> (<?php echo $delivery_type; ?> - <?php echo $pp_branch_name; ?>)</td>
		<?php } ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php if ($delivery_type_id == 1) { ?>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $shipping_address; ?></td>
        <?php }else if($delivery_type_id == 2){ ?>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $address_pp; ?></td>
		<?php } ?>
      </tr>
    </tbody>
  </table>
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_product; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_model; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_quantity; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_price; ?></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_total; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $product['product_name']; ?> <br/> by: <?php echo $product['nickname']; ?> (<?php echo $product['company']; ?>)</td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $product['model']; ?></td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['quantity']; ?></td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['price']; ?></td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4"><b><?php echo $text_shipping_price; ?>:</b></td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $shipping_price; ?></td>
      </tr>
	  <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4"><b><?php echo $text_total; ?>:</b></td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $total_price_invoice; ?></td>
      </tr>
    </tfoot>
  </table>
  <!--<p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>-->
  <br>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; height: 15%; vertical-align: middle;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="25%">
			<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
<div style="background-color: #990033; border-bottom: 1px solid #841719; padding: 9px 18px; color: #EEEEEE; line-height: 150%; font-size: 10px; height: 15%;">
			<em>Copyright � 2016 Ofiskita Powered by Astragraphia, All rights reserved.</em><br>
			<br>
			<strong>Address:</strong><br>
			Jl. Kramat Raya No. 43<br>
			Senen, Jakarta<br>
			10450
		</div>
</div>
</div>
</body>
</html>
