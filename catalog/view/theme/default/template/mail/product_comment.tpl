<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin:0; padding:0;">
<div style="width: 680px;">
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; margin-bottom: 20px;">
<a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/25184501-a8ee-4ae5-8b8b-c915e6c66374.png" style="padding: 10px;"></a>
</div>
  
	<p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_new_comment; ?></p>
  
  <table style="border-collapse: collapse; width: 100%; margin-bottom: 20px;">
    <tbody>
	<?php foreach($product_comments as $product_comment){ ?>
      <tr style="border-bottom:1pt solid #ccc;">
        <td>
		<div style="display: inline; font-weight: bold; font-size: 11px; text-transform: none; color: #333745;">
			<?php 
				if ($product_comment['created_by'] == $product_comment['seller_id']){
					echo $product_comment['nickname'];
				}else{
					echo $product_comment['firstname'];
				}
			?>
		</div>
		<?php if ($product_comment['created_by'] == $product_comment['seller_id']){ ?>
			<div style="margin-left: 3px; display: inline-block; min-width: 10px; padding: 3px 7px; font-size: 10px;	font-weight: 700; line-height: 1; color: #FFF; text-align: center; white-space: nowrap; vertical-align: middle;	border-radius: 10px; background-color: #5CB85C;">		
			<?php echo $text_merchant; ?> 
			</div>
		<?php } else { ?>
			<div style="margin-left: 3px; display: inline-block; min-width: 10px; padding: 3px 7px; font-size: 10px;	font-weight: 700; line-height: 1; color: #FFF; text-align: center; white-space: nowrap; vertical-align: middle;	border-radius: 10px; background-color: #E69100;">		
				<?php echo $text_customer; ?>
			</div>
		<?php } ?>
		<div style="display: inline; margin-left: 3px; font-size: 10px;	color: #999; position: relative; font-style: italic; font-weight: lighter; vertical-align: middle;">
			 <?php echo $product_comment['created_date']; ?>
		</div>
		<br/>
		<div style="font-size: 11px; text-transform: none; color: #333745;">
			 <?php echo $product_comment['comment']; ?>
		</div>
        </td>
      </tr>
	<?php } ?>
    </tbody>
  </table>
	<p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_link; ?></p>
	<p style="margin-top: 0px; margin-bottom: 20px;">
	<?php 
		echo $link;
	?>
	</p>
	<br>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; height: 15%; vertical-align: middle;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="25%">
			<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
<div style="background-color: #990033; border-bottom: 1px solid #841719; padding: 9px 18px; color: #EEEEEE; line-height: 150%; font-size: 10px; height: 15%;">
			<em>Copyright � 2016 Ofiskita Powered by Astragraphia, All rights reserved.</em><br>
			<br>
			<strong>Address:</strong><br>
			Jl. Kramat Raya No. 43<br>
			Senen, Jakarta<br>
			10450
		</div>
</div>
</div>
</body>
</html>