<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $title; ?></title>
<style>
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
</style>
</head>

<body bgcolor="#FFFFFF" style="position:relative;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;">
<div style="width: 100%; background-color: #FFFFFF; text-align: center; margin: 0; margin-bottom: 20px;">
<a href="http://www.ofiskita.com/" title="Ofiskita"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/6eab2b48-2957-4577-90fe-9fcb1dd4a670.png" style="padding: 10px;"></a>
</div>
<div style="width: 100%; background-color: #FFFFFF; text-align: center; margin: 0; margin-bottom: 20px;">
<img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/e5a62fee-4c18-4f1c-9109-435ee1f07084.png" style="padding: 10px;">
</div>
<!-- body -->
<table class="body-wrap" style="padding: 20px;width: 100%;" bgcolor="#FFFFFF">
  <tr>
    <td></td>
    <td class="container" style=" clear: both !important;display: block !important;Margin: 0 auto !important;max-width: 600px !important;border: 0;padding: 20px;" bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content" style="display: block;margin: 0 auto;max-width: 600px;">
      <table style="width: 100%;">
        <tr>
          <td align="center">
			<p style="font-size:18px;margin:0px;"><strong>Selamat bergabung di Ofiskita</strong></p>
			<p style="font-size:18px;margin:0px;">Hi <?php echo $name; ?>,</p>
			<p style="font-size:18px;margin:0px;">Akun anda sudah aktif</p>
			<br>
			<p style="font-size:18px;margin:0px;">Anda mendaftar dengan email :</p>
			<p style="font-size:18px;color: -webkit-link;"><?php echo $email; ?></p>
			<br>
			<!-- button -->
            <table style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style="background-color: #ec682e;font-size: 14px;text-align: center;vertical-align: top; ">
                  <a href="http://www.ofiskita.com/" style="display:block;background-color: #ec682e;border: solid 1px #ec682e;border-width: 10px 20px;color: #ffffff;cursor: pointer;font-weight: bold;line-height: 20px;text-decoration: none;">Mulai belanja</a>
                </td>
              </tr>
            </table>
            <!-- /button -->
           </td>
        </tr>
      </table>
      </div>
	  
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->
<table cellpadding="0" cellspacing="0" border="0" height="270" width="100%">
  <tr>
    <td background="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/7d3bacca-adb8-4fda-89ff-dd64caa77d63.png" bgcolor="#EFEFEF" valign="top">
      <!--[if gte mso 15]>
      <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:270px;">
        <v:fill type="tile" src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/7d3bacca-adb8-4fda-89ff-dd64caa77d63.png" color="#EFEFEF" />
        <v:textbox inset="0,0,0,0">
      <![endif]-->
      <div>
<table style="height:270px;width: 100% !important;background:url('https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/7d3bacca-adb8-4fda-89ff-dd64caa77d63.png') repeat-x #efefef;" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="top;" style="width: 150px;" >
			<img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/051f78f9-3396-4612-88de-8eec9831bd19.png">
		  </td>
		  <td valign="top" style="width: 60% !important;min-width: 300px;">
			<table class="info" style="width: 100% !important;" height="136" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style="background-color: #efefef;font-size: 14px; text-align: center;vertical-align: top;color:#000000;">
                  <p style="color:#878787;">Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
				  <p style="color:#9e9e9e;">Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja, atau meminta data pribadi anda. Ofiskita tidak pernah meminta password dan data pribadi melalui email, pesan pribadi, maupun channel lainnya.</p>
                </td>
              </tr>
            </table>
			<table width="100%" height="85" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" style="background-color: #ec682e;">
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style="font-size: 14px; text-align: center;vertical-align: top;color:#FFFFFF;">
                  Copyright � 2016 Ofiskita Powered by Astragraphia, All rights reserved.<br>
				  Astragraphia HO | Jl. Kramat Raya No. 43 Senen, Jakarta 10450<br>
				  <a href="mailto:support@ofiskita.com">support@ofiskita.com</a><br>
                </td>
              </tr>
            </table>
			</td>
				</tr>
			</table>
			<table width="100%" height="49" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" style="background-color: #b2360b;line-height:1px;" align="center">
			<table style="margin:auto" height="100%" width="20%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="30%">
						<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/052495a2-6e4f-4575-bf98-ef4f064459f5.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
					<td width="30%">
						<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/3443b837-591c-4b97-a1b9-b2dd56430242.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
					<td width="30%">
						<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://gallery.mailchimp.com/209dd85ec8823d12b9256756b/images/c841abe4-9a1b-47f2-b438-a5659168a452.png" style="display:block;" height="24" width="24" class=""></a></center>
					</td>
				</tr>
			</table>
			</td>
				</tr>
			</table>
		  </td>
		  <td valign="top;" style="width: 20% !important;max-width: 150px;">
			
		  </td>
        </tr>
      </table>
	        </div>
      <!--[if gte mso 15]>
        </v:textbox>
      </v:rect>
      <![endif]-->
    </td>
  </tr>
</table>
</body>
</html>