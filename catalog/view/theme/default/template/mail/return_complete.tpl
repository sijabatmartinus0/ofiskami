<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin:0; padding:0;">
<div style="width: 680px;">
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; margin-bottom: 20px;">
<a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/25184501-a8ee-4ae5-8b8b-c915e6c66374.png" style="padding: 10px;"></a>
</div>
  
	<?php if($status_id == 1){ ?>
		<p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_new_received; ?></p>
	<?php } else if($status_id == 3){ ?>
		<p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_merchant_complete; ?> <b><?php echo $status; ?></b></p>
	<?php } ?>
  
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_merchant_link; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $link_merchant; ?></p>
 
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;" colspan="3"><?php echo $text_new_return_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; 	vertical-align: top;">
		  <b><?php echo $text_new_return_id; ?></b> <?php echo $return_id; ?><br />
		  <b><?php echo $text_date_returned; ?></b> <?php echo $date_added; ?>
        </td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; vertical-align: top;">
		  <b><?php echo $text_new_order_id; ?></b> <?php echo $order_id; ?><br />
		  <b><?php echo $text_date_added; ?></b> <?php echo $date_ordered; ?><br />
		  <b><?php echo $text_invoice; ?></b> <?php echo $invoice_no; ?>
        </td>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
		  <b><?php echo $text_customer; ?></b> <?php echo $firstname; ?> <?php echo $lastname; ?><br />
		  <b><?php echo $text_email; ?></b> <?php echo $email; ?><br />
          <b><?php echo $text_telephone; ?></b> <?php echo $telephone; ?><br />
          <b><?php echo $text_ret_status; ?></b> <?php echo $status; ?>
		  </td>
      </tr>
    </tbody>
  </table>
  
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td colspan="2" style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_information; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
		<b><?php echo $text_prod_name; ?></b> <?php echo $product; ?><br />
		<b><?php echo $text_prod_model; ?></b> <?php echo $model; ?><br />
		<b><?php echo $text_prod_qty; ?></b> <?php echo $quantity; ?><br />
		<b><?php echo $text_prod_open; ?></b> <?php echo $opened; ?>
		</td>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px; vertical-align: top;">
		<b><?php echo $text_ret_reason; ?></b> <?php echo $reason; ?><br />
		<b><?php echo $text_ret_comment; ?></b> <?php echo $comment; ?><br />
		<b><?php echo $text_ret_action; ?></b> <?php echo $action; ?>
		</td>
      </tr>
    </tbody>
  </table>
  <br>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; height: 15%; vertical-align: middle;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="25%">
			<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
<div style="background-color: #990033; border-bottom: 1px solid #841719; padding: 9px 18px; color: #EEEEEE; line-height: 150%; font-size: 10px; height: 15%;">
			<em>Copyright � 2016 Ofiskita Powered by Astragraphia, All rights reserved.</em><br>
			<br>
			<strong>Address:</strong><br>
			Jl. Kramat Raya No. 43<br>
			Senen, Jakarta<br>
			10450
		</div>
</div>
</div>
</body>
</html>
