<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $title;?></title>
<style>
/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
</style>
</head>

<body bgcolor="#f6f6f6" style='font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;'>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; margin-bottom: 20px;">
<a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/25184501-a8ee-4ae5-8b8b-c915e6c66374.png" style="padding: 10px;"></a>
</div>
<!-- body -->
<table class="body-wrap" style="padding: 20px;width: 100%;" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" style=' clear: both !important;display: block !important;Margin: 0 auto !important;max-width: 600px !important;border: 1px solid #f0f0f0;padding: 20px;' bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content" style='display: block;margin: 0 auto;max-width: 600px;border-bottom:1px solid #cecece;'>
      <table>
        <tr>
          <td>
			<h2><?php echo $header;?></h2>
            <p>Hi <?php echo $pp_name;?>,</p>
            <p><?php echo $message_receive;?></p>
			<br/>
            <p><strong><?php echo $pp_branch_name;?></strong></p>
			<p><?php echo $pp_address;?></p>
			<br/>
            <p><?php echo $message_link_receive;?></p>
		  </td>
        </tr>
      </table>
      </div>
	  <div class="content-info" style='display: block;margin: 10px auto;max-width: 600px;border-bottom:1px solid #cecece;'>
      <table>
        <tr>
          <td>
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style='background-color: #e14169; font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#fff;'>
                  <p><?php echo $message_warning;?></p>
                </td>
              </tr>
            </table>
			<p></p>
		  </td>
        </tr>
      </table>
      </div>
	  
	  <div class="content-copyright" style='display: block;margin: 10px auto;max-width: 600px;font-size:10px !important;'>
      <table>
        <tr>
          <td>
			2015@Ofiskita.com
		  </td>
        </tr>
      </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->
<br>
<div style="width: 100%; background-color: #990033; text-align: center; margin: 0; height: 15%; vertical-align: middle;">
<center>
	<table style="left: 50%; top: 50%;" height="100%" width="20%">
	<tr>
		<td width="25%">
			<center><a href="http://ofiskita.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="https://twitter.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://www.facebook.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
		<td width="25%">
			<center><a href="http://instagram.com/ofiskita" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a></center>
		</td>
	</tr>
	</table>
</center>
<div style="background-color: #990033; border-bottom: 1px solid #841719; padding: 9px 18px; color: #EEEEEE; line-height: 150%; font-size: 10px; height: 15%;">
			<em>Copyright � 2016 Ofiskita Powered by Astragraphia, All rights reserved.</em><br>
			<br>
			<strong>Address:</strong><br>
			Jl. Kramat Raya No. 43<br>
			Senen, Jakarta<br>
			10450
		</div>
</div>
</body>
</html>