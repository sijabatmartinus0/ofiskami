  
<div class="checkout-content checkout-payment-methods">
<?php if ($error_warning) { ?>
    <div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    </div>
<?php } ?>
<?php if ($keperluans) { ?>
<form action="quick_checkout.php" method="POST">
    <h2 class="secondary-title" style="font-weight: bold;">Tipe Keperluan</h2>
        <div class="radio">
            <label>
                    <input type="radio" name="keperluan" id="PT" value="Perusahaan"/> Perusahaan <br>
                    <input type="radio" name="keperluan" id="PR" value="Pribadi" /> Pribadi <br>
                    <p id="warning" style="color: red"> Wajib Diisi</p>
                <?php } ?>
                <?php echo $payment_method['title']; ?>
                <?php if (isset($payment_method['terms']) && $payment_method['terms']) { ?>
                    (<?php echo $payment_method['terms']; ?>)
                <?php } ?>
            </label>
        </div>
</form>
</div>
