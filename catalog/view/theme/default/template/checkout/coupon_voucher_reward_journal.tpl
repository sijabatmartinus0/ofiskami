<div class="checkout-content coupon-voucher">
    <h2 class="secondary-title"><?php echo $text_title_voucher; ?></h2>
    <?php if ($coupon_status): ?>
    <div class="panel-body checkout-coupon col-lg-12">
        <div class="input-group">
            <input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon;?>" id="input-coupon" class="form-control" />
            <span class="input-group-btn">
                <input type="button" value="<?php echo $button_coupon;?>" id="button-coupon" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary button" />
				<input type="button" value="<?php echo $button_cancel;?>" id="button-coupon-cancel" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary button" style="display: none;" />
			</span>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($voucher_status): ?>
    <!--<div class="panel-body checkout-voucher col-lg-6">
        <div class="input-group">
            <input type="text" name="voucher" value="<?php echo $voucher; ?>" placeholder="<?php echo $entry_voucher;?>" id="input-voucher" class="form-control" />
            <span class="input-group-btn">
                <input type="button" value="<?php echo $button_voucher;?>" id="button-voucher" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary button" />
				<input type="button" value="<?php echo $button_cancel;?>" id="button-voucher-cancel" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary button" style="display: none;" />
            </span>
        </div>
    </div>-->
    <?php endif; ?>
    <?php if ($reward_status): ?>
        <div class="panel-body checkout-reward">
            <label class="col-sm-2 control-label" for="input-reward">'Enter reward points'</label>
            <div class="input-group">
                <input type="text" name="reward" value="<?php echo $reward; ?>" placeholder="'Enter reward points'" id="input-reward" class="form-control" />
            <span class="input-group-btn">
                <input type="button" value="<?php echo $button_reward;?>" id="button-reward" data-loading-text="<?php echo $text_loading; ?>"  class="btn-primary button" />
            </span>
            </div>
        </div>
    <?php endif; ?>
</div>
