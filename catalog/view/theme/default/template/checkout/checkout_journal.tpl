<?php echo $header; ?>
<style>
 #payment-confirm-button .buttons {
    display: none !important;
    cursor: not-allowed !important;
}
.quickcheckout-block{
	height:100%;
	width:100%;
	position:fixed;
	top:0;
	left:0;
	background:#e3e3e3;
	opacity: 0.6;
    filter: alpha(opacity=60);
	z-index:99999;
}
.quickcheckout-loading{
	position: relative;
    top: 50%;
	margin:auto;
}
</style>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="one-page-checkout <?php echo $class; ?>"><?php echo $content_top; ?>
	<div class="quickcheckout-content"></div>
	        <h1 class="heading-title"><?php echo $heading_title;?></h1>
            <div class="checkout col-lg-12">
                <div class="left">
                    
                </div>
                <div class="right">
                    <section class="section-left col-lg-4">
						<div class="spw">
							<?php echo $payment_methods; ?>
                        </div>
                        <!-- <div class="spw">
                            <?php echo $keperluans; ?>
                        </div> -->
                    </section>
                    <section class="section-right col-lg-8">
                        <?php echo $coupon_voucher_reward; ?>
                    </section>
                    <?php

                    ?>
                </div>
			</div>
				<div class="checkout col-lg-12">
				<?php echo $cart; ?>
                        <div class="checkout-content confirm-section">
                            <div>
                                <h2 class="secondary-title"><?php echo $text_comments ?></h2>
                                <label class="col-lg-12">
                                    <textarea name="comment" rows="8" class="col-lg-12 form-control"><?php echo $comment; ?></textarea>
                                </label>
                            </div>
                            <?php if ($entry_newsletter): ?>
                            <div class="checkbox check-newsletter">
                                <label for="newsletter">
                                    <input type="checkbox" name="newsletter" value="1" id="newsletter" />
                                    <?php echo $entry_newsletter; ?>
                                </label>
                            </div>
                            <?php endif; ?>

                            <?php if ($text_privacy): ?>
                            <div class="radio check-privacy">
                                <label>
                                    <input type="checkbox" name="privacy" value="1" />
                                    <?php echo $text_privacy; ?>
                                </label>
                            </div>
                            <?php endif; ?>

                            <!---- Modal -->
                    <div class="modal fade" id="modal-remove-cart" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="modal-cart-label"><?php echo $text_md_remove; ?></h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" id="keyCartRemove" />
                                    <?php echo "Anda tidak memenuhi syarat dan ketentuan. Silakan cek kembali Cart Anda." ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="ok" class="btn btn-default" data-dismiss="modal"><?php echo "Kembali ke Cart"; ?></button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!---- Modal -->
                            <?php if ($text_agree): ?>
                            <div class="radio check-terms">
                                <label>
                                    <input type="checkbox" name="agree" value="1" />
                                    <?php echo $text_agree; ?>
                                </label>
                            </div>
                            <?php endif; ?>
                            <div class="confirm-order">
                                <button id="checkout-confirm-button" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button confirm-button"><?php echo $button_confirm; ?></button>
                            </div>
                        </div>
            </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>
    $(document).delegate('input[name="payment_method"]', 'change', function() {
        $(document).trigger('checkout_payment_changed', this.value);
    });
	
	$(document).delegate('.checkout .confirm-button', 'click', function () {
		var data = { };

        $('.checkout input[type="text"], .checkout input[type="password"], .checkout select, .checkout input:checked, .heckout textarea[name="comment"]').each(function () {
            data[$(this).attr('name')] = $(this).val();
        });
        //console.log(data);
        $.ajax({
            url: 'index.php?route=checkout/quick_checkout/confirm',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
            },
            success: function(json) {
                $('.text-danger').remove();
                $('.has-error').removeClass('has-error');
                if(!json['promo']){
                    triggerLoadingOff();
                    $('#modal-remove-cart').modal('show');
                    $(document).delegate('.modal-footer', 'click', function (){
                        location = json['redirects'];
                    });
                    
                }
                else{
                if (json['errors']) {
                    $.each(json['errors'], function (k, v) {
                        if ($.inArray(k, ['payment_country', 'payment_zone', 'shipping_country', 'shipping_zone']) !== -1) {
                            k += '_id';
                        } else if (k.indexOf('custom_field') === 0) {
                            k = k.replace('custom_field', '');
                            k = 'custom_field[' + k + ']';
                        } else if (k.indexOf('payment_custom_field') === 0) {
                            k = k.replace('payment_custom_field', '');
                            k = 'payment_custom_field[' + k + ']';
                        } else if (k.indexOf('shipping_custom_field') === 0) {
                            k = k.replace('shipping_custom_field', '');
                            k = 'shipping_custom_field[' + k + ']';
                        }
                        var $element = $('.checkout [name="' + k + '"]');
                        $element.closest('.form-group').addClass('has-error');
                        $element.after('<div class="text-danger">' + v + '</div>');
                    });
                    triggerLoadingOff();
                } else if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    var $btn = $('#payment-confirm-button input[type="button"], #payment-confirm-button input[type="submit"], #payment-confirm-button .pull-right a, #payment-confirm-button .right a').first();
                    if ($btn.attr('href')) {
                        location = $btn.attr('href');
                    } else {
                        $btn.trigger('click');
                    }
                }
            }
        },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
	
	$(document).on('checkout_payment_changed', function (e, value) {
        $.ajax({
            url: 'index.php?route=checkout/quick_checkout/save',
            type: 'post',
            data: {
                payment_method: value
            },
            dataType: 'json',
            success: function(data) {
                console.log(1);
				$(document).trigger('checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
			console.log(2);
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
	
	$(document).on('checkout_reload_cart', function () {
        $.ajax({
            url: 'index.php?route=checkout/quick_checkout/cart',
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                triggerLoadingOn();
                $('.checkout-cart').addClass('checkout-loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('.checkout-cart').removeClass('checkout-loading');
            },
            success: function(html) {
                $('.checkout-cart').replaceWith(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
	
	$(document).delegate('#button-voucher', 'click', function() {
		$('#button-voucher').hide();
		$('#button-voucher-cancel').show();
        $.ajax({
            url: 'index.php?route=checkout/voucher/voucher',
            type: 'post',
            data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
				$('.alert').remove();
                $('#button-voucher-cancel').button('loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('#button-voucher-cancel').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                   $('.breadcrumb').after('<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                } else {
                    $('#cart ul').load('index.php?route=common/cart/info ul li');

                    $(document).trigger('checkout_reload_payment');
                }
            }
        });
    });
	
	$(document).delegate('#button-voucher-cancel', 'click', function() {
		$('#button-voucher').show();
		$('#button-voucher-cancel').hide();
		$('input[name=\'voucher\']').val('');
        $.ajax({
            url: 'index.php?route=checkout/voucher/unsetVoucher',
            type: 'post',
            data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
				$('.alert').remove();
                $('#button-voucher-cancel').button('loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('#button-voucher-cancel').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                   $('.breadcrumb').after('<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                } else {
                    $('#cart ul').load('index.php?route=common/cart/info ul li');

                    $(document).trigger('checkout_reload_payment');
                }
            }
        });
    });

    $(document).delegate('#button-coupon', 'click', function() {
		$('#button-coupon').hide();
		$('#button-coupon-cancel').show();
        $.ajax({
			url: 'index.php?route=checkout/coupon/coupon',
            type: 'post',
            data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
				$('.alert').remove();
                $('#button-coupon-cancel').button('loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('#button-coupon-cancel').button('reset');
            },
            success: function(json) {
                if (json['error']) {
					$('.breadcrumb').after('<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                } else {
                    $('#cart ul').load('index.php?route=common/cart/info ul li');

                    $(document).trigger('checkout_reload_payment');
                }
            }
        });
    });
	
	$(document).delegate('#button-coupon-cancel', 'click', function() {
		$('#button-coupon').show();
		$('#button-coupon-cancel').hide();
		$('input[name=\'coupon\']').val('');
        $.ajax({
            url: 'index.php?route=checkout/coupon/unsetCoupon',
            type: 'post',
            data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
				$('.alert').remove();
                $('#button-coupon-cancel').button('loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('#button-coupon-cancel').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                   $('.breadcrumb').after('<div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                } else {
                    $('#cart ul').load('index.php?route=common/cart/info ul li');

                    $(document).trigger('checkout_reload_payment');
                }
            }
        });
    });

    $(document).delegate('#button-reward', 'click', function() {
        $.ajax({
            url: 'index.php?route=checkout/reward/reward',
            type: 'post',
            data: 'reward=' + encodeURIComponent($('input[name=\'reward\']').val()),
            dataType: 'json',
            beforeSend: function() {
                triggerLoadingOn();
                $('#button-reward').button('loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('#button-reward').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                    alert(json['error']);
                } else {
                    $('#cart ul').load('index.php?route=common/cart/info ul li');

                    $(document).trigger('checkout_reload_payment');
                }
            }
        });
    });
	
	$(document).on('checkout_reload_payment', function () {
        $.ajax({
            url: 'index.php?route=checkout/quick_checkout/payment',
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                triggerLoadingOn();
                $('.checkout-payment-methods').addClass('checkout-loading');
            },
            complete: function() {
                triggerLoadingOff();
                $('.checkout-payment-methods').removeClass('checkout-loading');
            },
            success: function(html) {
                $('.checkout-payment-methods').replaceWith(html);
                $(document).trigger('checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
	
	 var ajax_calls = 0;

    function triggerLoadingOn() {
        ajax_calls++;
        if (ajax_calls === 1) {
            $('#checkout-confirm-button').button('loading');
			$('#content .quickcheckout-content').html('<div class="text-center quickcheckout-block"><div class="quickcheckout-loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div></div>');
            //$('#checkout-confirm-button, .checkout-register, .checkout-payment-form, .checkout-shipping-form').addClass('checkout-loading');
        }
    }

    function triggerLoadingOff() {
        ajax_calls--;
        if (ajax_calls === 0) {
            $('#checkout-confirm-button').button('reset');
			$('#content .quickcheckout-content').html('');
            //$('#checkout-confirm-button, .checkout-register, .checkout-payment-form, .checkout-shipping-form').removeClass('checkout-loading');
        }
    }
</script>
<?php echo $footer; ?>
