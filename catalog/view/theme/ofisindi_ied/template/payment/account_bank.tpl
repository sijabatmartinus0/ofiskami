<?php if (!empty($accounts)) { ?>
	<table class="table table-striped">
	<tr>
		<td class="col-lg-1">
			<?php echo ""; ?>
		</td>
		<td class="col-lg-8">
			<?php echo $column_bank_name; ?>
		</td>
		<td class="col-lg-3">
			<?php echo $column_bank_code; ?>
		</td>
	</tr>
	<?php foreach ($accounts as $account) { ?>
	<tr>
		<td>
			<input type="radio" name="account_id" value="<?php echo $account['account_id']; ?>-<?php echo $account['name']; ?>" class="rbt_account"/>
		</td>
		<td>
			<?php echo $account['name']; ?>
		</td>
		<td>
			<?php echo $account['code']; ?>
		</td>
	</tr>
	<?php } ?>
		
	</table>
		
	<br/>
<?php } else { ?>
	<h3><?php echo $text_empty; ?></h3>

<?php } ?>
	<div class="acc pagination" style="width:90%;"><?php echo $pagination; ?></div>
	<br/>