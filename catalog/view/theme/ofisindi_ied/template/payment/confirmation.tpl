	<?php echo $header; ?>
	<div id="container" class="container j-container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
		<?php if ($success) { ?>
			<div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="row"><?php echo $column_left; ?>
			<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
			<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
			<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
			<?php } ?>
			<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
				<h1 class="heading-title"><?php echo $heading_title; ?></h1>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" style="padding-left:10px">
				<fieldset id="invoice">
					<div class="form-group" style="display: none;">
					<label class="col-sm-2 control-label" for="input-invoice-number"><?php echo $text_invoice; ?></label>
						<div class="col-sm-3">
						  <label class="" for="input-invoice-number">
							<?php foreach ($invoices as $invoice) {?>
								<?php echo $invoice['invoice_no']; ?><br/>
							<?php } ?>
						  </label>
						  <input type="hidden" name="input_order_id" value="<?php echo $order_id; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-2 control-label" for="input-total-payment"><?php echo $text_total_payment; ?></label>
						<div class="col-sm-3">
						  <label class="control-label bold" for="input-total-payment"><?php echo $total; ?></label>
						</div>
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-date-payment"><?php echo $text_date_payment; ?></label>
						<div class="col-sm-3">
							<div class="input-group date">
							  <input name="input_date_payment" value="<?php echo $input_date_payment; ?>" placeholder="<?php echo $text_date_payment; ?>" data-date-format="YYYY-MM-DD" id="input-date-payment" class="form-control" type="text" >
							  <span class="input-group-btn" style="display: table-cell !important;">
							  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
							  </span>
							</div>
							 <?php if ($error_date_payment) { ?>
								<div class="text-danger"><?php echo $error_date_payment; ?></div>
							  <?php } ?>
						</div>
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-method-payment"><?php echo $text_method_payment; ?></label>
						<div class="col-sm-3">
							<select id="combobox-payment" name="input_method_payment" id="method_payment" class="form-control">
							<option value="-" >--<?php echo $acc_destination; ?>--</option>
							<?php foreach($payment_methods as $payment_method){ ?>
								<?php if ($payment_method['payment_method_id'] == $payment_method_id) { ?>
									<option selected value="<?php echo $payment_method['payment_method_id']; ?>-<?php echo $payment_method['name']; ?>"><?php echo $payment_method['name']; ?></option>
								<?php } else { ?>
									<option value="<?php echo $payment_method['payment_method_id']; ?>-<?php echo $payment_method['name']; ?>"><?php echo $payment_method['name']; ?></option>
								<?php } ?> 
							<?php } ?>
							</select>
							<?php if ($error_payment_method) { ?>
								<div class="text-danger"><?php echo $error_payment_method; ?></div>
							<?php } ?>
						</div>
					</div>
					<hr style="border-bottom: 1px dashed #ccc; margin:15px 0px 20px 0;"/>
				</fieldset>
				<fieldset id="bank">
					<h1 class="heading-title" style="margin-left:-10px;"><?php echo $text_add_bank; ?></h1>
					<div class="form-group required" style="margin-bottom:15px;">
					<label class="col-sm-2 control-label" for="input-bank-name"><?php echo $text_bank_name; ?></label>
						<div class="col-sm-3">
							<span class="input-group-btn">
							<input type="button" data-toggle="modal" data-target="#myModal" style="width:436px; margin:-5px 0px 0px 5px;" value="<?php echo $button_bank; ?>" name="button-bank" id="button-bank" data-loading-text="" class="button btn btn-danger" />
							<input type="hidden" name="id-account-bank" value="" id="id-account-bank" class="form-control" />
							</span>
							<?php if ($error_account_id) { ?>
								<div class="text-danger"><?php echo $error_account_id; ?></div>
							<?php } ?>
						</div>
						
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-account-name"><?php echo $text_account_name; ?></label>
						<div class="col-sm-3">
							<input width="436px" type="text" name="input_account_name" value="<?php echo $input_account_name; ?>" placeholder="<?php echo $text_account_name ?>" id="input-account-name" class="form-control" />
							<?php if ($error_account_name) { ?>
								<div class="text-danger"><?php echo $error_account_name; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-account-number"><?php echo $text_account_number; ?></label>
						<div class="col-sm-3">
						  <input width="436px" type="text" name="input_account_number" value="<?php echo $input_account_number; ?>" placeholder="<?php echo $text_account_number ?>" id="input-account-number" class="form-control number" />
							<?php if ($error_account_number) { ?>
								<div class="text-danger"><?php echo $error_account_number; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-branch"><?php echo $text_branch; ?></label>
						<div class="col-sm-3">
						  <input type="text" name="input_branch" value="<?php echo $input_branch; ?>" placeholder="<?php echo $text_branch ?>" id="input-branch" class="form-control" />
							<?php if ($error_branch) { ?>
								<div class="text-danger"><?php echo $error_branch; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-2 control-label" for="input-branch"></label>
						<div class="col-sm-3" id="grey">
							<?php echo $note_branch; ?>
						</div>
					</div>
					<hr style="border-bottom: 1px dashed #ccc; margin:15px 0px 20px 0;"/>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-account-destination"><?php echo $text_account_destination; ?></label>
						<div class="col-sm-3">
						<select id="combobox-payment" value="<?php echo $input_account_destination; ?>" name="input_account_destination" id="input-account-destination" class="form-control">
							<option value="" >--<?php echo $acc_destination; ?>--</option>
							<?php foreach ($account_destinations as $account_destination) { ?>
							<?php if ($account_destination['account_destination_id'] == $input_account_destination) { ?>
							<option selected value="<?php echo $account_destination['account_destination_id']; ?>"><?php echo $account_destination['name']; ?> - <?php echo $account_destination['account_number']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $account_destination['account_destination_id']; ?>"><?php echo $account_destination['name']; ?> - <?php echo $account_destination['account_number']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
						<?php if ($error_account_destination) { ?>
							<div class="text-danger"><?php echo $error_account_destination; ?></div>
						<?php } ?>
						</div>
					</div>
					<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-amount"><?php echo $text_amount; ?></label>
						<div class="col-sm-3">
						  <input type="text" name="input_amount" value="<?php echo $input_amount; ?>" placeholder="<?php echo $text_amount ?>" id="input-amount" class="number form-control" />
							<?php if ($error_amount) { ?>
								<div class="text-danger"><?php echo $error_amount; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-2 control-label" for="note-amount"></label>
						<div class="col-sm-3" id="grey">
							<?php echo $note_amount; ?>
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-2 control-label" for="upload-file"><?php echo $text_upload; ?></label>
						<div class="col-sm-3">
							
							<input id="uploadFile" type="text" name="input_file" placeholder="<?php $text_choose; ?>" disabled="disabled" class="form-control"/>
							<div class="fileUpload btn btn-primary">
							<span><?php echo $text_upload; ?></span>
							<input type="file" name="picture_confirmation" id="uploadBtn" class="upload">
							</div>
							
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-2 control-label" for="input-remarks"><?php echo $text_remarks; ?></label>
						<div class="col-sm-3">
						  <input type="text" name="input_remarks" value="<?php echo $input_remarks; ?>" placeholder="<?php echo $text_remarks ?>" id="input-remarks" class="form-control" />
						</div>
					</div>
				</fieldset>
				
				<br class="wide"/>
				<label id="grey" for="note-product"><?php echo $note_product; ?></label>
				<br class="wide"/>
				<label id="grey" for="note-upload"><?php echo $note_upload; ?></label>
				<br class="wide"/>
				<!--<div class="form-group required">
					<label id="grey" for="note-optional"><?php echo $note_optional; ?></label>	
				</div>-->
				
				<div class="btn-block pull-right" style="max-width: 400px; margin-bottom:30px;">
					<span class="input-group-btn">
						<input type="reset" style="margin-left:5px; font-weight: bold;" value="<?php echo $button_no; ?>" id="button-bank" data-loading-text="" class="btn btn-primary button-primary" />
						<input type="submit" style="width:60px; margin-left:5px; font-weight: bold;" value="<?php echo $button_yes; ?>" id="button-bank" data-loading-text="" class="btn btn-danger button-danger" />
						
					</span>
				</div>
				</form>
			<?php echo $content_bottom; ?></div>
			<?php //echo $column_right; ?>
		</div>
		
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4><?php echo $heading_title; ?></h4>
			</div>
			<div class="modal-body" >
			<div class="row">
			<div class="col-lg-12">
				<div class="input-group">
					<input autocomplete2="off" name="input_search" id="input_search" value="" placeholder="<?php echo $column_search; ?>" autocomplete="off" class="form-control input-lg" type="text">
					<span class="input-group-btn" id="button_search_account">
						<button type="button" class="btn btn-default "><span class="fa fa-search"></span></button>
					</span>
				</div>
			</div>
				
			<hr/>
			<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>	
			</div>
				<div id="account_modal">
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" ><?php echo $column_close; ?></button>
			</div>
	    </div>
	  </div>
	</div>
	
	<script type="text/javascript">
	    $(function(){
			$('#account_modal').empty().load('<?php echo $load_account; ?>',function(){$('.pcAttention').hide();});
			
			$( document ).ajaxComplete(function() {
				$(".rbt_account").on('click',function(){
					var temp = new Array();
					temp = ($(this).val()).split("-");
				
					$('#button-bank').val(temp[1]);
					$('#id-account-bank').val(temp[0]);
					$('#myModal').modal('hide');
				})
				;
				
				$('.acc.pagination>ul>li>a').click(function(event){
					event.preventDefault();
					$('.pcAttention').show();
					$('#account_modal').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
				});
				
				$("#button-bank").on('click', function(){
					event.preventDefault();
					$('.pcAttention').show();
					$('#account_modal').empty().load('<?php echo $load_account; ?>',function(){$('.pcAttention').hide();});
					$('#input_search').val("");
				});
				
				$("#button_search_account").on('click', function(){
					$('.pcAttention').show();
					$('#account_modal').load('<?php echo $search_account; ?>&input_search='+$('#input_search').val(),function(){$('.pcAttention').hide();});
				});
				
				$("#input_search").keypress(function (e) {
					if (e.keyCode == 13) {
						$('.pcAttention').show();
						$('#account_modal').load('<?php echo $search_account; ?>&input_search='+$('#input_search').val(),function(){$('.pcAttention').hide();});
					}
				});
				
			});  
	    });
		
		$('.date').datetimepicker({
			pickTime: false
			// minDate: new Date()
		});
		
		document.getElementById("uploadBtn").onchange = function () {
			document.getElementById("uploadFile").value = this.value;
		};
		
		$(document).ready(function(){ 
			$('input[type="file"]').change(function () {
				var ext = this.value.match(/\.(.+)$/)[1];
				switch (ext) {
					case 'jpg':
					case 'jpeg':
					case 'png':
					case 'pdf':
					case 'JPG':
					case 'JPEG':
					case 'PNG':
					case 'PDF':
						break;
					default:
						alert('<?php echo $error_picture_confirmation; ?>');
						this.value = '';
						$('#uploadFile').val(this.value);
				}
			});
		});
		$("body").delegate(".number", "keypress", function(e){
		 var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		  }
	});
	
	</script>
	<?php echo $footer; ?>