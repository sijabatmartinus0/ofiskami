<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen,print" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
</head>
<style>
/* Print styling */

@media print {

[class*="col-sm-"] {
	float: left;
}

[class*="col-xs-"] {
	float: left;
}

.col-sm-12, .col-xs-12 { 
	width:100% !important;
}

.col-sm-11, .col-xs-11 { 
	width:91.66666667% !important;
}

.col-sm-10, .col-xs-10 { 
	width:83.33333333% !important;
}

.col-sm-9, .col-xs-9 { 
	width:75% !important;
}

.col-sm-8, .col-xs-8 { 
	width:66.66666667% !important;
}

.col-sm-7, .col-xs-7 { 
	width:58.33333333% !important;
}

.col-sm-6, .col-xs-6 { 
	width:50% !important;
}

.col-sm-5, .col-xs-5 { 
	width:41.66666667% !important;
}

.col-sm-4, .col-xs-4 { 
	width:33.33333333% !important;
}

.col-sm-3, .col-xs-3 { 
	width:25% !important;
}

.col-sm-2, .col-xs-2 { 
	width:16.66666667% !important;
}

.col-sm-1, .col-xs-1 { 
	width:8.33333333% !important;
}
  
.col-sm-1,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11,
.col-xs-12 {
float: left !important;
}

body {
	margin: 0;
	padding 0 !important;
	min-width: 768px;
}

.container {
	width: auto;
	min-width: 750px;
}

body {
	font-size: 10px;
}

a[href]:after {
	content: none;
}

.noprint, 
div.alert, 
header, 
.group-media, 
.btn, 
.footer, 
form, 
#comments, 
.nav, 
ul.links.list-inline,
ul.action-links {
	display:none !important;
}

}
</style>
<body>
<div class="container">
    <div style="page-break-after: always;">
	<div class="container-fluid">
	<div class="col-lg-10"><div class="pull-left"><a style="margin-top:20px;" onclick="window.print();" class="btn btn-info"><?php echo $text_print; ?><i class="fa fa-print"></i></a></div></div>
	
	</div>
	<img src="https://gallery.mailchimp.com/f61bf18107bddcf2a43f94ed3/images/1f554a3d-25d6-4c30-bdbd-9c26c9f74fb5.png" width="150" class="pull-right" style="margin-top: -40px;">
	<br>
	<br>
	<br>
	<p style="background-color: #990033; padding: 5px; color: #fff; font-weight: bold; font-size: 14px; margin: 0 !important;"><?php echo $text_seller_invoice; ?>: <?php if($delivery_type_id == 3){ ?>OFISKITA<?php }else if($delivery_type_id == 1 || $delivery_type_id == 2){ ?><?php echo $seller['nickname'] . " "; if($seller['company']) { echo "(" . $seller['company'] . ")";} ?><?php } ?></p>
	<p style="background-color: #FF6600; padding: 5px; color: #fff; font-weight: bold; font-size: 12px; text-align: right;" ><?php echo $text_invoice_info; ?></p>
	
	<table width="50%">
	<tr>
		<td width="30%"><b><?php echo $column_npwp; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<?php if($delivery_type_id == 3){ ?>
			<td width="65%"><?php echo $this->config->get('config_npwp_store'); ?><td>
		<?php }else if($delivery_type_id == 1 || $delivery_type_id == 2){ ?>
			<td width="65%"><?php echo $seller['npwp']; ?><td>
		<?php } ?>
	</tr>
	<?php if ($invoice_no) { ?>
	<tr>
		<td width="30%"><b><?php echo $column_invoice; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<td width="65%"><?php echo $invoice_no; ?><td>
	</tr>
	<?php } ?>
	<tr>
		<td width="30%"><b><?php echo $column_order_id; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<td width="65%">#<?php echo $order_id; ?><td>
	</tr>
	<tr>
		<td width="30%"><b><?php echo $column_date_order; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<td width="65%"><?php echo $date_added; ?><td>
	</tr>
	<?php if ($payment_method) { ?>
	<tr>
		<td width="30%"><b><?php echo $column_payment_method; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<td width="65%"><?php echo $payment_method; ?><td>
	</tr>
	<?php } ?>
	<?php if ($shipping_method) { ?>
	<tr>
		<td width="30%"><b><?php echo $column_shipping_method; ?></b><td>
		<td width="5px" style="text-align: right;">:<td>
		<td width="65%"><?php echo $shipping_method; ?><td>
	</tr>
	<?php } ?>
	</table>
	<br>
	<?php if($delivery_type_id == 3){ ?>
	<p style="background-color: #FF6600; padding: 5px; color: #fff; font-weight: bold; font-size: 12px; text-align: right;" ><?php echo $text_voucher_info; ?></p>
	<br>
	<h4 class="heading-title"><?php echo $column_total; ?>: <?php echo $qty_voucher; ?> <?php echo $text_voucher; ?></h4>
		<table width="100%">
			<thead style="font-weight: bold; border: 1px solid #E0E0E0; height: 40px;">
				<tr>
					<td style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="20%"><?php echo $column_name_voucher; ?></td>
					<td style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="15%"><?php echo $column_email_voucher; ?></td>
					<?php if($order_status_id < 3){ ?>
						<td style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="20%"><?php echo $column_voucher_amount; ?></td>
					<?php }else if($order_status_id >= 3){ ?>
						<td style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="20%"><?php echo $column_code; ?></td>
					<?php } ?>
					<td style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="25%"><?php echo $column_description; ?></td>
					<td colspan="2" style="text-align: center; border: 1px solid rgb(224, 224, 224); " width="20%"><?php echo $column_price; ?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($vouchers as $voucher) { ?>
				<tr>
					<td style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><?php echo $voucher['to_name']; ?> </td>
					<td style="border: 1px solid rgb(224, 224, 224); text-align: center;" class="text-center"><?php echo $voucher['to_email']; ?> </td>
					<?php if($order_status_id < 3){ ?>
						<td style="border: 1px solid rgb(224, 224, 224); text-align: center;" class="text-center"><?php echo $voucher['amount']; ?> </td>
					<?php }else if($order_status_id >= 3){ ?>
						<td style="border: 1px solid rgb(224, 224, 224); text-align: center;" class="text-center"><?php echo $voucher['code']; ?> </td>
					<?php } ?>
					<td style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><?php echo $voucher['description']; ?> </td>
					<td style="text-align: right; padding-right: 5px;" width="12%" class="text-right"><?php echo $currency_code; ?> </td>
					<td style="border-right: 1px solid rgb(224, 224, 224); border-bottom: 1px solid rgb(224, 224, 224);  text-align: right; padding-right: 5px;" class="text-right"><?php echo $voucher['amount']; ?> </td>
				</tr>
			<?php } ?>
				<tr>
					<td style="border: 1px solid rgb(224, 224, 224); text-align: right; padding-left: 5px;" class="text-right" colspan="4"><?php echo '<b>' . $column_total . ' <sup>' . $text_note_invoice . '</sup></b>'; ?></td>
					<td style="border-bottom: 1px solid rgb(224, 224, 224); border-top: 1px solid rgb(224, 224, 224); padding-right: 5px;"  width="12%" class="text-right"><?php echo $currency_code; ?> </td>
					<td style="border-right: 1px solid rgb(224, 224, 224); border-bottom: 1px solid rgb(224, 224, 224); padding-right: 5px;"  class="text-right"><?php echo $total_price_voucher; ?></td>
				</tr>
			</tbody>
		</table>
	<?php } else if($delivery_type_id == 1 || $delivery_type_id == 2){ ?>
	<p style="background-color: #FF6600; padding: 5px; color: #fff; font-weight: bold; font-size: 12px; text-align: right;" ><?php echo $text_delivery_info; ?></p>
	<table width="100%">
		<thead style="border: 1px solid #E0E0E0; height: 40px;">
		<tr>
			<?php if ($delivery_type_id == 1) { ?>
				<td class="text-left" colspan="3" width="60%" style="padding-left: 5px;"><b><?php echo $text_shipping_address; ?> (<?php echo $shipping_name; ?> - <?php echo $shipping_service_name; ?> )</b></td>
			<?php } else if ($delivery_type_id == 2) {?>
				<td class="text-left" colspan="3" width="60%" style="padding-left: 5px;"><b><?php echo $text_shipping_address; ?> (<?php echo $delivery_type; ?> - <?php echo $pp_branch_name; ?> )</b></td>
			<?php } ?>
			<td width="20%" style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><b><?php echo $column_quantity; ?></b></td>
			<td width="20%" style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;" colspan="2"><b><?php echo $column_shipping; ?></b></td>
		</tr>
        </thead>
		<tbody>
			<tr style="border-left: 1px solid rgb(224, 224, 224);">
				<?php if ($delivery_type_id == 1) { ?>
					<td width="20%" style="padding-left: 5px;"><b><?php echo $column_receiver; ?></b></td>
					<td width="5">:</td>
					<td><?php echo $shipping_name; ?></td>
				<?php } else if ($delivery_type_id == 2) {?>
					<td width="20%" style="padding-left: 5px;"><b><?php echo $column_receiver; ?></b></td>
					<td width="5">:</td>
					<td><?php echo $pp_name; ?></td>
				<?php } ?>
					<td rowspan="5" style="border: 1px solid rgb(224, 224, 224); text-align: center;"><?php echo $qty; ?> (<?php echo sprintf('%0.2f', $total_weight); ?> <?php echo $weight; ?>)</td>
					<td width="12%" rowspan="2" style="text-align: right; padding-right: 5px;"><?php echo $currency_code; ?></td>
					<td rowspan="2" style="border-right: 1px solid rgb(224, 224, 224); text-align: right; padding-right: 5px;"><?php echo $shipping_price; ?></td>
			</tr>
			<tr style="border-left: 1px solid rgb(224, 224, 224);">
				<td style="padding-left: 5px;"><b><?php echo $text_shipping_address; ?></b></td>
				<td width="5">:</td>
			</tr>
			<tr style="border-left: 1px solid rgb(224, 224, 224);">
			<?php if ($delivery_type_id == 1) { ?>
				<td colspan="3" rowspan="2"><p style="margin-left: 40px;"><?php echo $shipping_address?></p></td>
			<?php } else if ($delivery_type_id == 2) {?>
				<td colspan="3" rowspan="2"><p style="margin-left: 40px;"><?php echo $address_pp?></p></td>
			<?php } ?>
				<td style="border: 1px solid rgb(224, 224, 224);  height: 40px; text-align: left; padding-left: 5px;" colspan="2"><b><?php echo $column_insurance?></b></td>
			</tr>
			<tr style="border-left: 1px solid rgb(224, 224, 224);">
				<td rowspan="2" style="text-align: right; padding-right: 5px;"><?php echo $currency_code; ?></td>
				<td rowspan="2" style="border-right: 1px solid rgb(224, 224, 224); text-align: right; padding-right: 5px;"><?php echo $shipping_insurance?></td>
			</tr>
			<tr style="border-left: 1px solid rgb(224, 224, 224); border-bottom: 1px solid rgb(224, 224, 224);">
				<td style="padding-left: 5px; "><b><?php echo $column_telp; ?></b></td>
				<td width="5">:</td>
				<?php if ($delivery_type_id == 1) { ?>
					<td><?php echo $shipping_telp; ?></td>
				<?php } else if ($delivery_type_id == 2) {?>
					<td><?php echo $pp_telp; ?></td>
				<?php } ?>
			</tr>
        </tbody>
	</table>
	
	<br>
	<p style="background-color: #FF6600; padding: 5px; color: #fff; font-weight: bold; font-size: 12px; text-align: right;" ><?php echo $text_order_info; ?></p>
	
	<table width="100%">
		<thead style="border: 1px solid #E0E0E0; height: 40px;">
		<tr>
			<td colspan="2" width="60%" style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><b><?php echo $column_product_list; ?></b></td>
			<td width="20%" style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><b><?php echo $column_remarks; ?></b></td>
			<td width="20%" colspan="2" style="border: 1px solid rgb(224, 224, 224); text-align: left; padding-left: 5px;"><b><?php echo $column_price; ?></b></td>
		</tr>
        </thead>
		<tbody>
		<?php foreach ($products as $product) { ?>
			<tr style="border: 1px solid #E0E0E0; height: 40px;">
			<td width="10%"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail"/></td>
			<td style="width: 40%; text-align: left; vertical-align: top; padding-left: 5px; border-right: 1px solid rgb(224, 224, 224);">
				<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
				<br>
				<?php echo $column_sku; ?>: <?php echo $product['sku']; ?>
				<br>
				<?php echo $column_quantity; ?>: <?php echo $product['quantity']; ?> x @ <?php echo $product['price']; ?>
			</td>
			<td style="width: 30%; text-align: right; padding-right: 5px; border-right: 1px solid rgb(224, 224, 224);">
				<?php if($product['comment']){ echo $product['comment']; }else{ echo "-"; }?>
			</td>
			<td width="12%" style="text-align: right; padding-right: 5px;"><?php echo $currency_code; ?></td>
			<td style="border-right: 1px solid rgb(224, 224, 224); text-align: right; padding-right: 5px;"><?php echo $product['total']; ?></td>
			</tr>
		<?php } ?>
		<?php foreach ($totals as $total) { ?>
			<tr style="border: 1px solid #E0E0E0; height: 40px;">
				<td colspan="3" style="width: 40%; text-align: right; padding-right: 5px; border-right: 1px solid rgb(224, 224, 224);">
					<?php 
						if($total['code']=='total'){
							echo '<b>' . $total['title'] . '<sup>'. $text_note_invoice . '</sup></b>' ;
						}else{
							echo $total['title'];
						}  
					?>
				</td>
				<td style="text-align: right; padding-right: 5px;"><?php echo $currency_code; ?></td>
				<td style="border-right: 1px solid rgb(224, 224, 224); text-align: right; padding-right: 5px;">
					<?php 
						if($total['code']=='total'){
							echo '<b>' . $total['value'] . '</b>'; 
						}else{
							echo $total['value'];
						}
					?>
				</td>			
			</tr>
		<?php } ?>
		</tbody>
	</table>
	
	<?php } ?>
	
	<?php if ($comment) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
	  <br>
	  <?php echo '<p style="color: #990033; font-weight: bold;"><sup>' . $text_note_invoice .  '</sup> ' . $text_note_desc . '</p>'; ?>
	  <br>
	  <br>
	</div>
</div>
</body>
</html>