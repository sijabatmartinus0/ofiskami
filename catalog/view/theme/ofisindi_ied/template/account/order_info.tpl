<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      
      <?php if($delivery_type_id == 3){ ?>
		<table class="table table-bordered table-hover list">
			<thead>
			  <tr>
				<td style="text-align: center;"><?php echo $text_order_detail; ?>
				</td>
				<td style="text-align: center;"><?php echo $column_customer; ?></td>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td style="text-align: left; ">
					<b><?php echo $text_invoice_no; ?></b> <a href="<?php echo $invoice_link; ?>" target="_blank"><?php echo $invoice_no; ?></a><br />
					<b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
					<b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
					<b><?php echo $text_seller; ?></b> <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a><br />
					<b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?>
				</td>
				<td style="text-align: left; vertical-align: top;">
					<b><?php echo $text_receiver_name; ?></b> <?php echo $to_name; ?><br />
					<b><?php echo $text_receiver_email; ?></b> <?php echo $to_email; ?>
				</td>
			  </tr>
			</tbody>
		  </table>
		
		<h1 class="heading-title"><?php echo $text_voucher_data; ?> - <?php echo $column_total; ?>: <?php echo $qty_voucher; ?> <?php echo $text_voucher; ?></h1>
		<div class="table-responsive">
		<table class="table table-bordered table-hover list">
			<thead>
				<tr>
					<td style="text-align: center;"><?php echo $column_code; ?></td>
					<td style="text-align: center;"><?php echo $column_description; ?></td>
					<td style="text-align: center;"><?php echo $column_message; ?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($vouchers as $voucher) { ?>
				<tr>
					<td><?php echo $voucher['code']; ?> </td>
					<td><?php echo $voucher['description']; ?> </td>
					<td><?php echo $voucher['message']; ?> </td>
				</tr>
			<?php } ?>
		</table>
		</div>
		
		<?php if ($histories) { ?>
		<h3><i class="fa fa-history" style="padding: 3px;"></i><?php echo $text_history; ?></h3>
		<br/>
		<table style="border-collapse: collapse;">
		<?php foreach ($histories as $history) { ?>
			<tr>
				<td class="text-left bold" width="15%" style="text-align: right; background-color: #4C82C5; color: #FFFFFF;  padding: 5px;"><?php echo $history['identity']; ?></td>
				<td class="text-left" style="padding-left: 10px; padding: 5px;"><?php echo $history['description']; ?></td>
			</tr>
			<tr style="border-bottom:1pt solid #ccc;">
				<td class="text-left"><i class="fa fa-clock-o" style="padding: 3px; padding: 5px;"></i><?php echo $history['date_added']; ?></td>
				<td class="text-left" style="padding-left: 10px; padding: 5px;"><?php echo $history['status']; ?></td>
			</tr>
			  <?php } ?>
		</table>
		<?php } ?>
		<br/><br/>
	  <?php } else if($delivery_type_id == 1 || $delivery_type_id == 2){ ?>
	  
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left" colspan="2"><i class="fa fa-info" style="padding: 3px;"></i><?php echo $text_order_detail; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
		  <td style="display: none;"><?php echo $order_detail_id; ?></td>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <a href="<?php echo $invoice_link; ?>" target="_blank"><?php echo $invoice_no; ?></a><br />
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
              <b><?php echo $text_seller; ?></b> <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>
			  <?php if ($order_status_id==19) { ?>
				<div class="pull-right"><a href="<?php echo $link_receive; ?>" class="btn btn-primary button"><?php echo $button_receive;?></a></div>
			  <?php } ?>
			  <?php if ($order_status_id==21) { ?>
				<div class="pull-right"><a href="<?php echo $link_complete; ?>" class="btn btn-primary button"><?php echo $button_complete;?></a></div>
			  <?php } ?>
			</td>
            <td class="text-left"><?php if ($payment_method) { ?>
              <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
              <?php } ?></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <?php if ($shipping_address) { ?>
				<?php if ($delivery_type_id == 1) { ?>
					<td class="text-left" rowspan="6"><?php echo $text_shipping_address; ?> (<i class="fa fa-truck" style="padding: 3px;"></i> <?php echo $shipping_name; ?> - <?php echo $shipping_service_name; ?> )</td>
				<?php } else if ($delivery_type_id == 2) {?>
					<td class="text-left" rowspan="6"><?php echo $text_shipping_address; ?> (<i class="fa fa-truck" style="padding: 3px;"></i> <?php echo $delivery_type; ?> - <?php echo $pp_branch_name; ?> )</td>
				<?php } ?>
            <?php } ?>
			<td class="text-left" style="width: 25%;"><?php echo $column_quantity; ?></td>
			<td class="text-left" style="width: 25%;"><?php echo $column_shipping; ?></td>
          </tr>
        </thead>
        <tbody>
		
			<tr>
				<?php if ($delivery_type_id == 1) { ?>
					<td class="text-left" rowspan="6" style="width:50%; text-align: left;"><?php echo $shipping_address; ?></td>
				<?php } else if ($delivery_type_id == 2) { ?>
					<td class="text-left" rowspan="6" style="width:50%; text-align: left;"><?php echo $address_pp; ?></td>
				<?php } ?>
				  <td class="text-right" ><?php echo $qty; ?> (<?php echo sprintf('%0.2f', $total_weight); ?> <?php echo $weight; ?>)</td>
				<td class="text-right" ><?php echo $shipping_price; ?></td>
			</tr>
			<tr>
				<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" >Terima Sebagian</td>
				<td style="background-color: #A9B8C0; text-align: left; color: #F4F4F4; padding: 3px;" height="8px" class="text-left" ><?php echo $column_insurance?></td>
			</tr>
			<tr>
				<td class="text-left" >Tidak</td>
				<td class="text-left" ><?php echo $shipping_insurance; ?></td>
			</tr>
		
        </tbody>
      </table>
	  
	  <div class="table-responsive">
		<table class="table table-bordered table-hover list">
			<thead>
				<tr>
				<?php if($order_status_id == 21 || $order_status_id == 24){ ?>
					<td colspan="5"><i class="fa fa-th-list" style="padding: 3px;"></i><?php echo $column_product_list; ?></td>
				<?php } else { ?>
					<td colspan="4"><i class="fa fa-th-list" style="padding: 3px;"></i><?php echo $column_product_list; ?></td>
				<?php } ?>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($products as $product) { ?>
				<tr>
					<td rowspan="2" style="width: 10%;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail"/></td>
					<td style="width: 40%; text-align: left;"><a
                            href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
					<td style="width: 25%; text-align: left; height:10px;" class="bold"><?php echo $column_remarks; ?></td>
					<td style="width: 25%; text-align: left; height:10px;" class="bold"><?php echo $column_price; ?></td>
					<?php if($order_status_id == 21 || $order_status_id == 24){ ?>
					<?php if($product['is_complete_return'] == 3 || $product['is_complete_return'] == null) { ?>
					<td class="text-right" style="white-space: nowrap;" rowspan="2">
						<a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a>
					</td>
					<?php } else { ?>
					<td class="text-right" style="white-space: nowrap;" rowspan="2">
						<a data-toggle="tooltip" title="<?php echo $text_complete_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a>
					</td>
					<?php } ?>
					
					<?php } ?>
				</tr>
				<tr>
					<td style="text-align: left;"><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?></td>
					<td style="text-align: left;">-</td>
					<td style="text-align: left;"><?php echo $product['total']; ?></td>
					
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>
			<!--Total-->
            <tr>
				<td colspan="3" class="bold" style="font-size: 16px; text-align: center;"><?php echo $column_total; ?></td>
				<td colspan="2" class="bold" style="font-size: 16px;"><?php echo $total_price; ?></td>
            </tr>
          </tfoot>
		</table>
	  </div>
      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
	  
		<?php if ($histories) { ?>
		<h3><i class="fa fa-history" style="padding: 3px;"></i><?php echo $text_history; ?></h3>
		<br/>
		<table style="border-collapse: collapse;">
		<?php foreach ($histories as $history) { ?>
			<tr>
				<td class="text-left bold" width="15%" style="text-align: right; background-color: #4C82C5; color: #FFFFFF;  padding: 5px;"><?php echo $history['identity']; ?></td>
				<?php if($history['order_status_id'] == 19) { ?>
					<td class="text-left" style="padding-left: 10px; padding: 5px;"><?php echo $history['description']; ?><?php  echo $text_receipt; ?> <a class="btn btn-default" role="button" onclick="checkWaybill()"><?php echo $shipping_receipt_number; ?></a></td>
				<?php } else { ?>
					<td class="text-left" style="padding-left: 10px; padding: 5px;"><?php echo $history['description']; ?></td>
				<?php } ?>
			</tr>
			<tr style="border-bottom:1pt solid #ccc;">
				<td class="text-left"><i class="fa fa-clock-o" style="padding: 3px; padding: 5px;"></i><?php echo $history['date_added']; ?></td>
				<td class="text-left" style="padding-left: 10px; padding: 5px;">
				<?php echo $history['status']; ?>
				<?php if($history['order_status_id'] == 23) { ?>
					: <?php echo $history['comment']; ?>
				<?php } ?>
				</td>
			</tr>
			  <?php } ?>
		</table>
		<?php } ?>
		<br/><br/>
	<?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-waybill" tabindex="-1" role="dialog" aria-labelledby="modal-waybill-label"data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-waybill-label"><?php echo $text_modal_waybill;?></h4>
      </div>
      <div class="modal-body">
        <div id="modal-waybill-content">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$(function() {
	window.checkWaybill = function checkWaybill(){
		$.ajax({
			url: 'index.php?route=account/order/waybill&order_detail_id=<?php echo $order_detail_id; ?>',
			type: 'get',
			beforeSend: function() {
				//$('#button-cart').button('loading');
			},
			complete: function() {
				//$('#button-cart').button('reset');
			},
			success: function(json) {
				var html='';
				if(json['status']['code']==200){
					var waybill=json['waybill']['summary'];
					html='<table class="table table-bordered table-hover list">'+
							'<tbody>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_number; ?></td>'+
								'<td colspan="3" class="text-left">'+waybill['waybill_number']+'</td>'+
							  '</tr>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_courier; ?></td>'+
								'<td colspan="3" class="text-left">'+waybill['courier_name']+'</td>'+
							  '</tr>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_service; ?></td>'+
								'<td colspan="3" class="text-left">'+waybill['service_code']+'</td>'+
							  '</tr>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_date; ?></td>'+
								'<td colspan="3" class="text-left">'+waybill['waybill_date']+'</td>'+
							  '</tr>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_origin; ?></td>'+
								'<td class="text-left">'+waybill['origin']+'</td>'+
								'<td class="text-left"><?php echo $text_waybill_destination; ?></td>'+
								'<td class="text-left">'+waybill['destination']+'</td>'+
							  '</tr>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_shipper; ?></td>'+
								'<td class="text-left">'+waybill['shipper_name']+'</td>'+
								'<td class="text-left"><?php echo $text_waybill_receiver; ?></td>'+
								'<td class="text-left">'+waybill['receiver_name']+'</td>'+
							  '</tr>'+
							'</tbody>'+
						  '</table>';
				}else if(json['status']['code']==500){
					html='<table class="table table-striped">'+
							'<thead>'+
							  '<tr>'+
								'<td class="text-left"><?php echo $text_waybill_not_found; ?></td>'+
							  '</tr>'+
							'</thead>'+
						  '</table>';
				}
				$('#modal-waybill-content').empty().html(html);
				$('#modal-waybill').modal('show');
				
			}
		});
	}
});
//--></script>
<?php echo $footer; ?>