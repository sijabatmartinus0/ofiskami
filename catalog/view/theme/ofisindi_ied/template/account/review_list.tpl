<style>
.panel-heading-review {
    padding: 10px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
    transition: all 0.2s;
}
.image-review{
	width:50px;
	height:50px;
	margin:10px;
	float:left;
}
.seller-review{
	float:left;
}

.image-review-product{
	width:50px;
	height:50px;
	margin:10px;
	float:left;
}
.seller-review-product{
	width:85%;
	float:left;
}

.seller-review-name{
	margin-top:20px;
}
.seller-review-date{
}
.review-toggle{
	margin-bottom:10px;
}
#review .fa-star {
    color: #f1c40f;
	cursor:pointer;
}
#review .fa-star + .fa-star-o {
    color: #f1c40f;
	cursor:pointer;
}
#review .fa-star-o {
    color: #f1c40f;
	cursor:pointer;
}
.fa-stack{
    width: auto;
    height: auto;
    line-height: 100%;
    padding: 0 8px;
    display: inline-block;
    margin-bottom: 20px;
}
</style>
<?php if(!empty($reviews)){ ?>
<?php foreach($reviews as $review){ ?>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading-review"><h5 class="muted">#<?php echo $review['invoice'];?></h5></div>
  <div class="panel-body">
	<div class="row review-toggle" role="button" data-toggle="collapse" href="#<?php echo $review['order_detail_id'];?>" aria-expanded="true" aria-controls="collapseOne">
	<div class="col-lg-12">
	<div class="image-review">
	<img style="border:1px solid #F7F7F7" src="<?php echo $review['avatar'];?>" alt="<?php echo $review['seller'];?>" title="<?php echo $review['seller'];?>" class="img-thumbnail">
	</div>
	<div class="seller-review">
		<div class="seller-review-name">
		<h4><?php echo $review['seller'];?><span class="badge red"><?php echo $text_seller;?></span></h4>
		</div>
		<div class="seller-review-date">
			<h5><small class="muted"><i><?php echo $review['date_added'];?></i></small></h5>
		</div>
	</div>
	</div>
	<div class="col-lg-12">
	<div class="pull-right">
		<a class="response-button collapsed"><span class="pending_collapse">Show <i class="fa fa-chevron-circle-down all_collapse"></i></span></a>
	</div>
	</div>
	</div>
	<div id="<?php echo $review['order_detail_id'];?>" class="panel-collapse review collapse" role="tabpanel" aria-labelledby="headingOne" style="height: auto;">
		<div class="panel-body" style="background:#F7F7F7">
		<?php foreach($review['review'] as $review_product){?>
		<div class="panel panel-default">
			<div class="panel-body">
			<div class="image-review-product">
				<img style="border:1px solid #F7F7F7" src="<?php echo $review_product['product_image'];?>" alt="<?php echo $review_product['product_name'];?>" title="<?php echo $review_product['product_name'];?>" class="img-thumbnail">
			</div>
			<div class="seller-review-product">
			<div style="border-bottom:1px solid #EBEBEB;width:100%;line-height:20px" class="form-group required">
			<a style="font-size:13px" href="<?php echo $review_product['product_href']; ?>"><?php echo $review_product['product_name']; ?></a>
			</div>
			<div class="review-product-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>">
			<?php if($review_product['review_id']==''){?>
			<form id="form-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>">
				<input type="hidden" name="order_id" value="<?php echo $review['order_id'];?>" />
				<input type="hidden" name="order_detail_id" value="<?php echo $review['order_detail_id'];?>" />
				<input type="hidden" name="product_id" value="<?php echo $review_product['product_id'];?>" />
				<div id="review-note-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>"></div>
				<div class="form-group required">
				<div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_accuracy; ?></label>
					<div>
                    <div class="stars-accuracy starrr" data-rating='0'></div>
					<input type="hidden" name="rating_accuracy" id="rating_accuracy" class="rating_accuracy"/>
					</div>
                </div>
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
					<div>
                    <div class="stars-existing starrr" data-rating='0'></div>
					<input type="hidden" name="rating" id="rating" class="rating"/>
					</div>
                </div>
				<div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                  </div>
                </div>
			</form>
				<div class="buttons col-sm-12">
                  <div class="pull-left">
                    <button type="button" id="button-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button"><?php echo $button_submit; ?></button>
                  </div>
                </div>
				</div>
                <script type="text/javascript">
				$('#button-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>').on('click', function() {
					$.ajax({
						url: 'index.php?route=account/review/write',
						type: 'post',
						dataType: 'json',
						data: $("#form-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>").serialize(),
						beforeSend: function() {
							$('.alert-success, .alert-danger').remove();
							$('#button-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>').button('loading');
						},
						complete: function() {
							$('#button-review-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>').button('reset');
						},
						success: function(json) {
							if (json['error']) {
								$('#review-note-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>').after('<div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
							}
							
							if (json['success']) {
								var html=
									"<div style=\"margin-top:10px;\">"+
									"<h4>"+json['author']+"<span class=\"badge green\"><?php echo $text_customer;?></span></h4>" +
									"<p>"+json['text']+"</p>"+
									"<h4><?php echo $entry_rating;?></h4>";
								for (var i = 1; i <= 5; i++) {
									if (parseInt(json['rating']) < i) {
										html+="<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
									}else{
										html+="<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
									}
								}
								html+="</div><div style=\"margin-top:10px;\">"+
									  "<h4><?php echo $entry_accuracy;?></h4>"
								for (var i = 1; i <= 5; i++) {
									if (parseInt(json['rating_accuracy']) < i) {
										html+="<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
									}else{
										html+="<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
									}
								}								html+="</div><div><h5><small class=\"muted\"><i>"+json['date_added']+"</i></small></h5></div>";
								$('.review-product-<?php echo $review['order_detail_id'];?>-<?php echo $review_product['product_id'];?>').empty().html(html);
							}
						}
					});
				});
				</script>
				<?php } else {?>
					<div style="margin-top:10px;">
					<h4><?php echo $review_product['review_author'];?><span class="badge green"><?php echo $text_customer;?></span></h4>
					<p><?php echo $review_product['review_text']; ?></p>
					<h4><?php echo $entry_rating;?></h4>
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($review_product['review_rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?></div>
					  <div style="margin-top:10px;">
					  <h4><?php echo $entry_accuracy;?></h4>
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($review_product['review_accuracy'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>
					<div><h5><small class="muted"><i><?php echo $review_product['review_date_added']; ?></i></small></h5></div>
				<?php }?>
				</div>
			</div>
		</div>
		</div>
		<?php } ?>
		</div>
	</div>
  </div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php }else{ ?>
<div class="panel panel-default">
				  <div class="panel-heading">
					<h4 class="panel-title">
						<?php echo $text_no_review; ?>
					</h4>
				</div>
				</div>
<?php } ?>
<script type="text/javascript"><!--
var key="<?php echo $key;?>";
var search="<?php echo $search;?>";
$('.panel-collapse.collapse.review')
			.on('shown.bs.collapse', function () {
					 $(this).parent().find(".pending_collapse").html("<?php echo $button_hide; ?> <i class=\'fa fa-chevron-circle-up all_collapse\' ></i>");
			}).on('hidden.bs.collapse', function () {
					 $(this).parent().find(".pending_collapse").html("<?php echo $button_show; ?> <i class=\'fa fa-chevron-circle-down all_collapse\' ></i>");
			});
var __slice = [].slice;

(function($, window) {
    var Starrr;

    Starrr = (function() {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function(e, value) {}
        };

        function Starrr($el, options) {
            var i, _, _ref,
                _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                _ = _ref[i];
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'i', function(e) {
                return _this.syncRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function() {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'i', function(e) {
                return _this.setRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);
        }

        Starrr.prototype.createStars = function() {
            var _i, _ref, _results;

            _results = [];
            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<i class='fa fa-star-o'></i>"));
            }
            return _results;
        };

        Starrr.prototype.setRating = function(rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function(rating) {
            var i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('i').eq(i).removeClass('fa-star-o').addClass('fa-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('i').eq(i).removeClass('fa-star').addClass('fa-star-o');
                }
            }
            if (!rating) {
                return this.$el.find('i').removeClass('fa-star').addClass('fa-star-o');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function() {
            var args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function() {
                var data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

$(function() {
    return $(".starrr").starrr();
});

$( document ).ready(function() {
  
  $('.stars-existing').on('starrr:change', function(e, value){
	$(this).siblings().val(value);
    $('#count-existing').html(value);
  });
  $('.stars-accuracy').on('starrr:change', function(e, value){
	$(this).siblings().val(value);
    $('#count-existing').html(value);
  });
});
//--></script> 