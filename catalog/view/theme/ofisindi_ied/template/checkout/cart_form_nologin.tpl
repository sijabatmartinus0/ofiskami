<strong><?php echo $text_md_cart_nologin_message ?></strong> <a href="<?php echo $link ?>"><?php echo $button_login ?></a>
<script type="text/javascript">
	$("#md-button-submit").empty().html('<?php echo $button_login ?>');
	$("#md-button-submit").unbind('click');
	$('#md-button-submit').on('click', function(){
		location='<?php echo $link ?>';
	});
</script>