<style>
.md-cart .form-control{
	height: auto !important;
    padding: 3px 5px !important;
	margin-top: 5px !important;
}
</style>
<div class="md-error-content"></div>
<div id="content" class="row md-content md-cart" style="padding: 0px 20px 0 20px !important;">
    <div class="xl-40 col-lg-4">
		<div style="margin-bottom:10px">
            <?php echo $text_md_cart_product_name ?>
        </div>
        <div style="margin-bottom:10px">
            <a href="<?php echo $product_info['link']; ?>">
                <?php echo $product_info['product_name'];?>
            </a>
        </div>
		<div style="margin-bottom:10px" class="product_option">
            <?php echo $product_info['option']; ?>
        </div>
	</div>
	<div class="xl-30 col-lg-5">
        <div class="">
            <table>
                <tr>
                    <td style="width:50%" valign="top">
                        <div class="form-group">
                            <label class="col-sm-2 control-label text-center">
                                <?php echo $text_md_cart_product_amount; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control inline number text-center" name="product_amount" id="product_amount" value="<?php echo $product_info['product_amount'] ?>" style="width:50% !important;" />
                            </div>
                        </div>
                    </td>
                    <td style="width:50%" valign="top">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?php echo $text_md_cart_product_price; ?>
                            </label>
                            <div class="col-sm-12 product-price" id="product-price" style="font-weight:bold">
                                <?php echo $product_info['product_price'] ?>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="xl-30 col-lg-3">
        <div class="form-group">
            <label class="col-sm-12 control-label">
                <?php echo $text_md_cart_product_information; ?>
            </label>
            <div class="col-sm-12">
                <textarea class="form-control" name="product_information" style="resize: none;"><?php echo $product_info['product_information'] ?></textarea>
            </div>
        </div>
    </div>
    <div style="clear:both;margin-bottom:5px"></div>
    <hr>
    <div style="clear:both;margin-bottom:5px"></div>
    <div class="">
        <ul id="tabs" class="nav nav-tabs htabs" style="margin-top: 5px;">
            <li class="active md-tab-delivery"><a href="#md-tab-delivery" data-toggle="tab"><i class="fa fa-truck"></i> <?php echo $button_md_cart_logistic; ?></a></li>
            <li class="md-tab-pickup"><a href="#md-tab-pickup" data-toggle="tab"><i class="fa fa-cube"></i> <?php echo $button_md_cart_pick_up_point; ?></a></li>
        </ul>
        <div class="tabs-content tab-content">
            <div class="tab-pane tab-content active" style="background-color: rgb(228, 228, 228);padding:0px 15px 15px 15px;margin-bottom: 10px;" id="md-tab-delivery">
                <input type="hidden" name="delivery_type" id="delivery_type" value="1" />
				<input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>" />
				<input type="hidden" name="new_address" id="new_address" value="0" />
                <div class="address-new" style="display:none;">
				<div class="buttons" style="margin:10px 0px 0px 0px;padding:0;background:#fff;border-radius:4px;">
                    <div class="pull-left">
                        <button class="btn btn-default cancel-address-button" style="background:none;color:#000;border:none;font-size:12px;">
                        <i class="fa fa-times" style="font-size:12px;color:#000;padding-bottom: 2px;"></i>&nbsp;<?php echo $button_md_cart_cancel_address; ?></button>
                    </div>
                </div>
				<div class="well" style="background:#ffffff;margin:0;padding:0px 15px 15px 15px;">
				<div class="row">
				<div class="form-horizontal">
				<div class="xl-45" style="padding:10px;">
			<div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_alias; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_alias" placeholder="<?php echo $entry_alias; ?>" id="input-alias" class="form-control" />
            </div>
          </div>
				<div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_firstname" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_lastname" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_company" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_address_1" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="add_address_2" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
            </div>
          </div>
		  </div>
		  <div class="xl-45" style="padding:10px;">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            <div class="col-sm-10">
              <select name="add_country_id" id="input-country" class="form-control" style="padding: 7px;margin:0" readonly="readonly">
              <option value="">
                                    <?php echo $text_select; ?>
                                </option>
                                <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_id) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected">
                                            <?php echo $country['name']; ?>
                                        </option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>">
                                                <?php echo $country['name']; ?>
                                            </option>
                                            <?php } ?>
                                                <?php } ?>
			  </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
            <div class="col-sm-10">
              <select name="add_zone_id" id="input-zone" class="form-control" style="padding: 7px;margin:0">
              <option value=""><?php echo $text_select; ?></option>
			  </select>
            </div>
          </div>
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
			  <select name="add_city_id" id="input-city" class="form-control" style="padding: 7px;margin:0">
              <option value=""><?php echo $text_select; ?></option>
			  </select>
            </div>
          </div>
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-district"><?php echo $entry_district; ?></label>
            <div class="col-sm-10">
			  <select name="add_district_id" id="input-district" class="form-control" style="padding: 7px;margin:0">
              <option value=""><?php echo $text_select; ?></option>
			  </select>
            </div>
          </div>
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-subdistrict"><?php echo $entry_subdistrict; ?></label>
            <div class="col-sm-10">
			  <select name="add_subdistrict_id" id="input-subdistrict" class="form-control" style="padding: 7px;margin:0">
              <option value=""><?php echo $text_select; ?></option>
			  </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
            <div class="col-sm-10">
              <input style="width: 90%;" type="text" name="add_postcode" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" readonly="readonly" />
            </div>
          </div>
		  </div>
				</div>
				</div>
				</div>
				</div>
				<div class="address-existing">
				<div class="buttons" style="margin:10px 0px 0px 0px;padding:0;background:#fff;border-radius:4px;">
                    <div class="pull-right">
                        <select class="form-control col-lg-3" style="background:none;border:none;float:left" id="shipping_address" name="shipping_address">
                            <?php foreach($addresses as $address){?>
                                <option value="<?php echo $address['id'];?>">
                                    <?php echo $address['name'];?>
                                </option>
                                <?php }?>
                        </select>
                        <button class="btn btn-default add-address-button col-lg-3" style="background:none;color:#000;border:none;font-size:12px;float:left">
                            <?php echo $button_md_cart_add_address; ?>&nbsp;<i class="fa fa-plus" style="font-size:12px;color:#000"></i></button>
                    </div>
                </div>
                <div class="well" style="background:#ffffff;margin:0;padding:0px 15px 15px 15px;">
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" style="width:90%" id="address_name">
                                    <?php echo (isset($addresses[0]['name'])?$addresses[0]['name']:''); ?>
                                </label>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" style="width:90%" id="address_detail">
                                    <?php echo (isset($addresses[0]['detail'])?$addresses[0]['detail']:''); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
				</div>
            </div>
            <div class="tab-pane tab-content" style="background-color: rgb(228, 228, 228);padding:0px 15px 15px 15px;" id="md-tab-pickup">
                <div class="row">
                    <div class="form-horizontal xl-60 col-lg-6">
                        <div class="form-group required" style="margin-top:10px">
                            <label class="col-sm-4 control-label" for="search-pp-country">
                                <?php echo $text_md_cart_search_pp_country; ?>
                            </label>
                            <select name="country_id" id="search-pp-country" class="xl-60 col-sm-4">
                                <option value="">
                                    <?php echo $text_select; ?>
                                </option>
                                <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_id) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected">
                                            <?php echo $country['name']; ?>
                                        </option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>">
                                                <?php echo $country['name']; ?>
                                            </option>
                                            <?php } ?>
                                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group required" style="">
                            <label class="col-sm-4 control-label" for="search-pp-zone">
                                <?php echo $text_md_cart_search_pp_zone; ?>
                            </label>
                            <select name="zone_id" id="search-pp-zone" class="xl-60 col-sm-4">
                            <option value=""><?php echo $text_select; ?></option>
							</select>
                        </div>
                        <div class="form-group required" style="">
                            <label class="col-sm-4 control-label" for="search-pp-city">
                                <?php echo $text_md_cart_search_pp_city; ?>
                            </label>
                            <select name="city_id" id="search-pp-city" class="xl-60 col-sm-4">
                            <option value=""><?php echo $text_select; ?></option>
							</select>
                        </div>
                        <div class="form-group required" style="">
                            <label class="col-sm-4 control-label" for="search-pp-branch">
                                <?php echo $text_md_cart_search_pp_branch; ?>
                            </label>
                            <select name="pp_branch_id" id="search-pp-branch" class="xl-60 col-sm-4">
                            <option value=""><?php echo $text_select; ?></option>
							</select>
                        </div>
                    </div>
                    <div class="form-horizontal xl-40 col-lg-6">
                        <div class="form-group" style="margin-top:5px;line-height:30px">
                            <input type="checkbox" name="pickup_person_status" class="pickup_person_status" id="pickup_person_status" value="1"/><?php echo $text_md_cart_pickup_person_status ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="pickup_person_name">
                                <?php echo $text_md_cart_pickup_person_name; ?>
                            </label>
                            <input type="text" style="width: auto;" class="xl-60 col-lg-6 form-control inline pickup_person name" name="pickup_person_name" id="pickup_person_name" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="pickup_person_email">
                                <?php echo $text_md_cart_pickup_person_email; ?>
                            </label>
                            <input type="text" style="width: auto;" class="form-control inline pickup_person xl-60 col-lg-6" name="pickup_person_email" id="pickup_person_email" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="pickup_person_phone">
                                <?php echo $text_md_cart_pickup_person_phone; ?>
                            </label>
                            <input type="text" style="width: auto;" class="form-control inline pickup_person xl-60 col-lg-6 number" name="pickup_person_phone" id="pickup_person_phone" disabled="disabled" />
                        </div>
                    </div>
                    <div style="clear:both;margin-bottom:5px"></div>
                    <div class="well" style="background:#ffffff;padding: 5px 15px 5px 15px;">
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label class="col-sm-2 control-label">
                                        <?php echo $text_md_cart_info_pp_name; ?>
                                    </label>
                                    <label class="col-sm-2 form-control-static" style="width:60%">:&nbsp;<span class="pp_branch_name"></span></label>
                                </div>
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label class="col-sm-2 control-label">
                                        <?php echo $text_md_cart_info_pp_company; ?>
                                    </label>
                                    <label class="col-sm-2 form-control-static" style="width:60%">:&nbsp;<span class="pp_branch_company"></span></label>
                                </div>
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label class="col-sm-2 control-label">
                                        <?php echo $text_md_cart_info_pp_address; ?>
                                    </label>
                                    <label class="col-sm-2 form-control-static" style="width:60%">:&nbsp;<span class="pp_branch_address"></span>
                                        <br>&nbsp;&nbsp;<span class="pp_branch_location"></span></label>
                                </div>
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label class="col-sm-2 control-label">
                                        <?php echo $text_md_cart_info_pp_telephone; ?>
                                    </label>
                                    <label class="col-sm-2 form-control-static" style="width:60%">:&nbsp;<span class="pp_branch_telephone"></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div style="display: table;" class="xl-100 col-lg-12">
	  <div style="display: table-row;" class="xl-100 col-lg-12">
		<div style="display: table-cell;" class="xl-25 col-lg-3">
		  <div>
		  <?php echo $text_md_cart_info_shipping_name; ?>
		  </div>
		  <div class="delivery shipping">
                    <select name="shipping_id" id="shipping_id" class="form-control">
                    </select>
                </div>
		</div>
		<div style="display: table-cell;" class="xl-25 col-lg-3">
		<div>
		  <?php echo $text_md_cart_info_shipping_service_name; ?>
		  </div>
		  <div class="delivery shipping-service">
                    <select name="shipping_service_id" id="shipping_service_id" class="form-control">
                    </select>
                </div>
		</div>
		<div style="display: table-cell;" class="xl-25 col-lg-3">
		<div>
		  <?php echo $text_md_cart_info_shipping_price; ?>
		  </div>
		  <div class="delivery shipping-price" style="font-weight:bold">
                </div>
		</div>
		<div style="display: table-cell;" class="xl-25 col-lg-3">
		<div>
		  <?php echo $text_md_cart_info_total; ?>
		  </div>
		  <div class="delivery subtotal" style="font-weight:bold">
                </div>
		</div>
	  </div>
	</div>
</div>
<script type="text/javascript">
    <!--
    $(function() {
        var shipping_data = new Array();
        var product_price = <?php echo $product_info['price'];?>;
		$('.cancel-address-button').on('click', function() {
            $('input[name=\'new_address\']').val(0);
			$('.address-new').hide();
			$('.address-existing').show();
        });
		$('.add-address-button').on('click', function() {
            $('input[name=\'new_address\']').val(1);
			$('.address-new').show();
			$('.address-existing').hide();
        });
		$('#pickup_person_status').on('change', function() {
			$(".pickup_person").prop("disabled", !this.checked); 
		});
        $('li.md-tab-delivery').on('click', function() {
			if($('input[name=\'new_address\']').val()==0){
				$('select[name=\'shipping_address\']').trigger('change');
				$('input[name=\'new_address\']').val(0);
				$('.address-new').hide();
				$('.address-existing').show();
			}else{
				$('input[name=\'new_address\']').val(1);
				$('.address-new').show();
				$('.address-existing').hide();
				$('select[name=\'add_zone_id\']').val("");
				$('select[name=\'add_city_id\']').val("");
				$('select[name=\'add_district_id\']').val("");
				$('select[name=\'add_subdistrict_id\']').val("");
			}
            $('input[name=\'delivery_type\']').val(1);
        });
        $('li.md-tab-pickup').on('click', function() {
            $('select[name=\'shipping_id\']').empty().html("<option>-</option>");
            $('select[name=\'shipping_service_id\']').empty().html("<option>-</option>");
            $('.shipping-price').empty().html('-');
            $('.subtotal').empty().html($('#product-price').html());
            $('input[name=\'delivery_type\']').val(2);
        });
		$("#md-button-submit").unbind('click');
		$('#md-button-submit').on('click', function(){
			$.ajax({
                url: 'index.php?route=checkout/cart/addCart',
				type: 'post',
				data:$('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, .md-cart input[type=\'text\'], .md-cart input[type=\'hidden\'], .md-cart input[type=\'checkbox\']:checked, .md-cart select, .md-cart textarea' ),
                dataType: 'json',
                beforeSend: function() {
					$('.alert').remove();
					$('#md-button-submit').button('loading');
                },
                complete: function() {
                    $('.fa-spin').remove();
					$('#md-button-submit').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();
					$('.form-group').removeClass('has-error');

					if (json['error']) {
						if (json['error']['option']) {
							for (i in json['error']['option']) {
								var element = $('#input-option' + i.replace('_', '-'));
								
								if (element.parent().hasClass('input-group')) {
									element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
								} else {
									element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
								}
							}
							$('#cart-modal').modal('hide');
						}
						
						if (json['error']['recurring']) {
							$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
						}
						
						$('.text-danger').parent().addClass('has-error');
						
						if (json['error']['amount']){
							var html="";
							if(json['error']['amount']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['amount']+'<br>';
							}
							
							$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>');
						}
						if (json['error']['delivery']) {
							var html="";
							if(json['error']['delivery']['shipping_address']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['shipping_address']+'<br>';
							}
							if(json['error']['delivery']['add_alias']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_alias']+'<br>';
							}
							if(json['error']['delivery']['add_firstname']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_firstname']+'<br>';
							}
							if(json['error']['delivery']['add_lastname']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_lastname']+'<br>';
							}
							if(json['error']['delivery']['add_address_1']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_address_1']+'<br>';
							}
							if(json['error']['delivery']['add_country_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_country_id']+'<br>';
							}
							if(json['error']['delivery']['add_zone_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_zone_id']+'<br>';
							}
							if(json['error']['delivery']['add_city_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_city_id']+'<br>';
							}
							if(json['error']['delivery']['add_district_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_district_id']+'<br>';
							}
							if(json['error']['delivery']['add_subdistrict_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_subdistrict_id']+'<br>';
							}
							if(json['error']['delivery']['add_postcode']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['add_postcode']+'<br>';
							}
							if(json['error']['delivery']['shipping_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['shipping_id']+'<br>';
							}
							if(json['error']['delivery']['shipping_service_id']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['delivery']['shipping_service_id']+'<br>';
							}
							
							$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>');
						}
						if (json['error']['pickup']) {
							var html="";
							if(json['error']['pickup']['name']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['pickup']['name'] +'<br>';
							}
							if(json['error']['pickup']['email']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['pickup']['email'] +'<br>';
							}
							if(json['error']['pickup']['phone']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['pickup']['phone'] +'<br>';
							}
							if(json['error']['pickup']['branch']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['pickup']['branch'] +'<br>';
							}
							$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>')
						}
					}
					
					if (json['success']) {
						
						$('#cart-modal').modal('hide');
						
						$.colorbox({
						html:'<div class="cart_notification"><div class="product"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom"><a class="btn btn-default" href="' + json['link_cart'] + '">' + json['text_cart'] + '</a> ' + '<a class="btn btn-primary" href="' + json['link_checkout'] + '">' + json['text_checkout'] + '</a></div></div>',
						className: "notification",
						initialHeight:50,
						initialWidth:50,
						width:"90%",
						maxWidth:400,
						height:"90%",
						maxHeight:200
						});
						
						$('#cart').load('index.php?route=common/cart/info #cart'); //Added
						
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
		$('select[name=\'shipping_address\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=checkout/cart/shipping&new_address=' +$('input[name=\'new_address\']').val()+ '&city_id=' +$('select[name=\'add_city_id\']').val()+ '&amount=' + $('input[name=\'product_amount\']').val() + '&address_id=' + this.value + '&product_id=<?php echo $product_id;?>',
                dataType: 'json',
				type: 'post',
				data:$('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea' ),
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'shipping_address\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('select[name=\'shipping_id\']').empty().html("<option>-</option>");
                    $('select[name=\'shipping_service_id\']').empty().html("<option>-</option>");
                    $('.shipping-price').empty().html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('.subtotal').empty().html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('#product-price').empty().html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
					
                    if (parseInt(json['status']['code']) == 200) {
                        if (json['address']) {
                            $("#address_name").empty().html(json['address']['name']);
                            $("#address_detail").empty().html(json['address']['detail']);
                        }
                        shipping_data = json['shipping'];
                        var html = "";
                        $.each(shipping_data, function(index, item) {
                            html += "<option value=" + item.id + ">" + item.name + "</option>";
                        });
                        $('select[name=\'shipping_id\']').empty().html(html);
                        $('select[name=\'shipping_service_id\']').empty().html("<option>-</option>");

                        $('select[name=\'shipping_id\']').trigger('change');
                    } else {
						var reload_html = "</br><a id=\"md-cart-reload\" style=\"font-size:10px;\"><i class=\"fa fa-refresh\" style=\"font-size:10px;\"></i> <?php echo $text_reload;?></a>";
						$('.shipping-price').empty().html(reload_html);
						$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto"><i class="fa fa-exclamation-circle"></i> '+json['status']['error_log'] +'</div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
		$('input[name=\'product_amount\']').keypress(function (e){
		  var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		  }
		});
        $('input[name=\'product_amount\']').on('change', function() {
			var delivery_type=parseInt($('input[name=\'delivery_type\']').val());
			var new_address=parseInt($('input[name=\'new_address\']').val());
			if(delivery_type==1 && new_address==0){
				$('select[name=\'shipping_address\']').trigger('change');
			}else{
                $.ajax({
					url: 'index.php?route=product/product/getPrice&amount=' + this.value + '&product_id=<?php echo $product_id;?>',
					dataType: 'json',
					beforeSend: function() {
						$('.alert').remove();
						$('select[name=\'shipping_id\']').empty().html("<option>-</option>");
						$('select[name=\'shipping_service_id\']').empty().html("<option>-</option>");
						$('.shipping-price').empty().html('-');
						$('.subtotal').empty().html('<i class="fa fa-circle-o-notch fa-spin"></i>');
						$('#product-price').empty().html('<i class="fa fa-circle-o-notch fa-spin"></i>');
					},
					complete: function() {
						$('.fa-spin').remove();
					},
					success: function(json) {
						if (json) {
							$('.subtotal').empty().html(json['subtotal']);
							$('#product-price').empty().html(json['price']);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
        });
		
		<?php if(!empty($addresses)){?>
			$('input[name=\'product_amount\']').trigger('change');
		<?php }else{?>
			$('.add-address-button').trigger('click');
		<?php }?>
        $('select[name=\'shipping_id\']').on('change', function() {
            var html = "";
            $.each(shipping_data[this.value]['service'], function(index, item) {
                html += "<option value=" + item.id + ">" + item.name + "</option>";
            });

            $('select[name=\'shipping_service_id\']').empty().html(html);

            $('select[name=\'shipping_service_id\']').trigger('change');
        });

        $('select[name=\'shipping_service_id\']').on('change', function() {
            var html = "<option value=0>NO</option>";
			var reload_html = "</br><a id=\"md-cart-reload\" style=\"font-size:10px;\"><i class=\"fa fa-refresh\" style=\"font-size:10px;\"></i> <?php echo $text_reload;?></a>";
            var id = parseInt(this.value);
            $.each(shipping_data[$('select[name=\'shipping_id\']').val()]['service'], function(index, item) {
                if (parseInt(item.id) == id) {
                    $('.shipping-price').empty().html(item.shipping_price+reload_html);
                    $('.subtotal').empty().html(item.subtotal);
                    $('#product-price').empty().html(item.product_price);
                }
            });
        });
		
		$("body").delegate("#md-cart-reload", "click", function(e){
			$('select[name=\'shipping_address\']').trigger('change');
		});
		
        $('select[name=\'country_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/countrykiosk&country_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'zone_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $('select[name=\'country_id\']').trigger('change');

        $('select[name=\'zone_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/zonekiosk&zone_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['city'] && json['city'] != '') {
                        for (i = 0; i < json['city'].length; i++) {
                            html += '<option value="' + json['city'][i]['city_id'] + '"';

                            if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['city'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'city_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // $('select[name=\'zone_id\']').trigger('change');

        $('select[name=\'city_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/pp_branch_city&city_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['pp_branch'] && json['pp_branch'] != '') {
                        for (i = 0; i < json['pp_branch'].length; i++) {
                            html += '<option value="' + json['pp_branch'][i]['pp_branch_id'] + '"';

                            if (json['pp_branch'][i]['pp_branch_id'] == '<?php echo $pp_branch_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['pp_branch'][i]['pp_branch_name'] + '</option>';
                        }
                    } else {
                        html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'pp_branch_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // $('select[name=\'city_id\']').trigger('change');


        $('select[name=\'pp_branch_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/pp_branch&pp_branch_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'pp_branch_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    if (json['pp_branch_name'] != null) {
                        $('.pp_branch_name').empty().html(json['pp_branch_name']);
                        $('.pp_branch_company').empty().html(json['company']);
                        $('.pp_branch_address').empty().html(json['address']);
                        $('.pp_branch_location').empty().html(json['city'] + "," + json['zone'] + "," + json['country'] + "," + json['postcode']);
                        $('.pp_branch_telephone').empty().html(json['telephone']);
                    } else {
                        $('.pp_branch_name').empty();
                        $('.pp_branch_company').empty();
                        $('.pp_branch_address').empty();
                        $('.pp_branch_location').empty();
                        $('.pp_branch_telephone').empty();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // $('select[name=\'pp_branch_id\']').trigger('change');

		$('select[name=\'add_country_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/country&country_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'add_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'add_postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'add_postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'add_zone_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $('select[name=\'add_country_id\']').trigger('change');

        $('select[name=\'add_zone_id\']').on('change', function() {
            $.ajax({
                url: 'index.php?route=account/account/zone&zone_id=' + this.value,
                dataType: 'json',
                beforeSend: function() {
				$('.alert').remove();
                    $('select[name=\'add_zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                complete: function() {
                    $('.fa-spin').remove();
                },
                success: function(json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'add_postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'add_postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['city'] && json['city'] != '') {
                        for (i = 0; i < json['city'].length; i++) {
                            html += '<option value="' + json['city'][i]['city_id'] + '"';

                            if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['city'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'add_city_id\']').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // $('select[name=\'add_zone_id\']').trigger('change');

        $('select[name=\'add_city_id\']').on('change', function() {
			if(this.value>0){
				$('select[name=\'shipping_address\']').trigger('change');
			}
			$.ajax({
				url: 'index.php?route=account/account/city&city_id=' + this.value,
				dataType: 'json',
				beforeSend: function() {
					$('select[name=\'add_city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
				},
				complete: function() {
					$('.fa-spin').remove();
				},
				success: function(json) {
					if (json['postcode_required'] == '1') {
						$('input[name=\'add_postcode\']').parent().parent().addClass('required');
					} else {
						$('input[name=\'add_postcode\']').parent().parent().removeClass('required');
					}
					
					html = '<option value=""><?php echo $text_select; ?></option>';
					
					if (json['district'] && json['district'] != '') {
						for (i = 0; i < json['district'].length; i++) {
							html += '<option value="' + json['district'][i]['district_id'] + '"';
							
							if (json['district'][i]['district_id'] == '<?php echo $district_id; ?>') {
								html += ' selected="selected"';
							}
						
							html += '>' + json['district'][i]['name'] + '</option>';
						}
					} else {
						html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
					}
					
					$('select[name=\'add_district_id\']').html(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		// $('select[name=\'add_city_id\']').trigger('change');

		$('select[name=\'add_district_id\']').on('change', function() {
			$.ajax({
				url: 'index.php?route=account/account/district&district_id=' + this.value,
				dataType: 'json',
				beforeSend: function() {
					$('select[name=\'add_district_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
				},
				complete: function() {
					$('.fa-spin').remove();
				},
				success: function(json) {
					if (json['postcode_required'] == '1') {
						$('input[name=\'add_postcode\']').parent().parent().addClass('required');
					} else {
						$('input[name=\'add_postcode\']').parent().parent().removeClass('required');
					}
					
					html = '<option value=""><?php echo $text_select; ?></option>';
					
					if (json['subdistrict'] && json['subdistrict'] != '') {
						for (i = 0; i < json['subdistrict'].length; i++) {
							html += '<option value="' + json['subdistrict'][i]['subdistrict_id'] + '"';
							
							if (json['subdistrict'][i]['subdistrict_id'] == '<?php echo $subdistrict_id; ?>') {
								html += ' selected="selected"';
							}
						
							html += '>' + json['subdistrict'][i]['name'] + '</option>';
						}
					} else {
						html += '<option value="" selected="selected"><?php echo $text_none; ?></option>';
					}
					
					$('select[name=\'add_subdistrict_id\']').html(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		// $('select[name=\'add_district_id\']').trigger('change');

		$('select[name=\'add_subdistrict_id\']').on('change', function() {
			$.ajax({
				url: 'index.php?route=account/account/subdistrict&subdistrict_id=' + this.value,
				dataType: 'json',
				beforeSend: function() {
					$('select[name=\'add_subdistrict_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
				},
				complete: function() {
					$('.fa-spin').remove();
				},
				success: function(json) {
					if (json['postcode_required'] == '1') {
						$('input[name=\'add_postcode\']').parent().parent().addClass('required');
					} else {
						$('input[name=\'add_postcode\']').parent().parent().removeClass('required');
					}
					
					html = '<option value=""><?php echo $text_select; ?></option>';
					
					if (json['postcode'] && json['postcode'] != '') {
						$('input[name=\'add_postcode\']').val(json['postcode']);
					} else {
						$('input[name=\'add_postcode\']').val('');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		//$('select[name=\'add_subdistrict_id\']').trigger('change');

    });
    //-->
</script>
