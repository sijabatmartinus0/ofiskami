<?php echo $header; ?>
<div id="container" class="container j-container">
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
<?php if((int)$this->customer->getNewCustomer()==0 && $this->customer->isLogged()) { $this->customer->existingCustomer();?>
<div class="info-block" id="info-block">
<p class="triangle-header triangle-border top" id="welcome-text-step1">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</br>
	<span class="triangle-content">
		<span class="triangle-content-header">Selamat datang di Ofiskita.com</span>
		</br>
		<span style="display: block;margin-bottom:5px;">Anda berada di halaman utama, ini adalah menu navigasi atas :</span>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>(+62) 021 300 61222</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melihat info pengiriman</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melakukan klaim pengembalian barang</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melakukan login/menuju halaman pelanggan</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melakukan pendaftaran/menuju halaman pelanggan</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melihat history pemesanan</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i></span><span>Akses untuk melihat wishlist</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><img width="16" height="11" src="<?php echo HTTP_SERVER;?>image/flags/gb.png" alt="English"></span><span>Akses untuk mengganti format bahasa</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><span class="currency-symbol">Rp</span></span><span>Akses untuk mengganti format harga</span>
		</br>
		<button type="button" id="button-next-welcome-step1" class="button pull-right"><span class="button-next-welcome-step1">Berikut</span></button>
	</span>
</p>
<p class="triangle-search triangle-border top" id="welcome-text-step2">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</br>
	<span class="triangle-content">
		<span class="triangle-content-header">Selamat datang di Ofiskita.com</span>
		</br>
		<span style="display: block;margin-bottom:5px;">Anda berada di halaman utama, ini adalah menu pencarian :</span>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><span style="width:16px;display: inline-block;background:rgb(214, 26, 34);padding:3px;"><i style="margin-right: 5px; font-size: 16px;color:#fff" data-icon=""></i></span></span><span>Akses untuk mencari barang</span>
		<br>
		<span style="width:30px;display: inline-block;margin-bottom:5px;"><span style="width:16px;display: inline-block;background:rgb(214, 26, 34);padding:3px;"><i style="margin-right: 5px; font-size: 16px;color:#fff" data-icon=""></i></span></span><span>Akses untuk melihat info pengiriman</span>
		</br>
		<button type="button" id="button-next-welcome-step2" class="button pull-right"><span class="button-next-welcome-step1">Berikut</span></button>
	</span>
</p>
</div>
<style>
.info-block {
	width:1500px;height:1000px;z-index:999998;position:fixed;top:0;
}
.info-overflow{
	overflow:hidden;
}
/* ============================================================================================================================
== BUBBLE WITH A BORDER AND TRIANGLE
** ============================================================================================================================ */

/* THE SPEECH BUBBLE
------------------------------------------------------------------------------------------------------------------------------- */
.triangle-content-header {
    display: block;
    font-size: 20px;
    font-weight: bold;
}
.triangle-header {
	top:45px;
	display:none;
}
.triangle-search {
	top:60px;
	left:365px;
	display:none;
}
.triangle-content {
	margin-top:10px;
}
.triangle-border {
  background:#fff;
  padding:10px;
  margin:1em 10px 3em;
  border:5px solid rgb(0, 0, 0);
  border:5px solid rgba(0, 0, 0, 0.5);
  -moz-background-clip: padding;  
  -webkit-background-clip: padding;  
  background-clip: padding-box; 
  color:#333;
  /* css3 */
  -webkit-border-radius:10px;
  -moz-border-radius:10px;
  border-radius:10px;
  box-shadow: 0 0 6px #B2B2B2;
  position:absolute;
  z-index:999999;
}

/* Variant : for left positioned triangle
------------------------------------------ */

.triangle-border.left {
  margin-left:30px;
}

/* Variant : for right positioned triangle
------------------------------------------ */

.triangle-border.right {
  margin-right:30px;
}

/* THE TRIANGLE
------------------------------------------------------------------------------------------------------------------------------- */

.triangle-border:before {
  content:"";
  position:absolute;
  bottom:-20px; /* value = - border-top-width - border-bottom-width */
  left:40px; /* controls horizontal position */
  border-width:20px 20px 0;
  border-style:solid;
  border-color:rgb(0, 0, 0) transparent;
  border-color:rgba(0, 0, 0, 0.5) transparent;
  -webkit-background-clip: padding-box; /* for Safari */
  background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
  /* reduce the damage in FF3.0 */
  display:block;
  width:0;
}

/* creates the smaller  triangle */
.triangle-border:after {
  content:"";
  position:absolute;
  bottom:-13px; /* value = - border-top-width - border-bottom-width */
  left:47px; /* value = (:before left) + (:before border-left) - (:after border-left) */
  border-width:13px 13px 0;
  border-style:solid;
  border-color:#fff transparent;
  /* reduce the damage in FF3.0 */
  display:block;
  width:0;
}

/* Variant : top
------------------------------------------ */

/* creates the larger triangle */
.triangle-border.top:before {
  top:-20px; /* value = - border-top-width - border-bottom-width */
  bottom:auto;
  left:40px;
  right:auto; /* controls horizontal position */
  border-width:0 20px 20px;
}

/* creates the smaller  triangle */
.triangle-border.top:after {
  top:-13px; /* value = - border-top-width - border-bottom-width */
  bottom:auto;
  left:47px;
  right:auto; /* value = (:before right) + (:before border-right) - (:after border-right) */
  border-width:0 13px 13px;
}

/* Variant : left
------------------------------------------ */

/* creates the larger triangle */
.triangle-border.left:before {
  top:10px; /* controls vertical position */
  bottom:auto;
  left:-30px; /* value = - border-left-width - border-right-width */
  border-width:15px 30px 15px 0;
  border-color:transparent #5a8f00;
}

/* creates the smaller  triangle */
.triangle-border.left:after {
  top:16px; /* value = (:before top) + (:before border-top) - (:after border-top) */
  bottom:auto;
  left:-21px; /* value = - border-left-width - border-right-width */
  border-width:9px 21px 9px 0;
  border-color:transparent #fff;
}

/* Variant : right
------------------------------------------ */

/* creates the larger triangle */
.triangle-border.right:before {
  top:10px; /* controls vertical position */
  bottom:auto;
  left:auto;
  right:-30px; /* value = - border-left-width - border-right-width */
  border-width:15px 0 15px 30px;
  border-color:transparent rgb(239, 29, 38);
}

/* creates the smaller  triangle */
.triangle-border.right:after {
  top:16px; /* value = (:before top) + (:before border-top) - (:after border-top) */
  bottom:auto;
  left:auto;
  right:-21px; /* value = - border-left-width - border-right-width */
  border-width:9px 0 9px 21px;
  border-color:transparent #fff;
}

</style>
<script type="text/javascript">
$(function() {
    $('body').addClass("info-overflow");
	$('html, body').animate({scrollTop: 0}, 700);
	$('#welcome-text-step1').fadeTo(1,0).fadeTo(750,0).fadeTo(500,1);
});

$(document).ready(function()
{
	var subject = new Array(); 
	$("#info-block > p").each(function( index ) {
		subject[index]=$( this ).attr("class");
	});
    $("#info-block").mouseup(function(e)
    {
		var parent_subject=e.target;
        if(subject.indexOf($(parent_subject).attr("class"))==-1 && !$("#info-block > p").has(e.target).length)
        {
			$('.triangle-border').fadeOut();
			$('body').removeClass("info-overflow");
			$('#info-block').hide();
        }
    });
	$("#button-next-welcome-step1").on('click', function() {
		$('#welcome-text-step1').fadeOut();
		$('#welcome-text-step2').fadeTo(1,0).fadeTo(750,0).fadeTo(500,1);
		$('body,html').animate({ scrollTop: 100 }, "slow");
	});
	$("#button-next-welcome-step2").on('click', function() {
		$('#welcome-text-step2').fadeOut();
		$('body').removeClass("info-overflow");
		$('#info-block').hide();
	});
});
</script>
<?php } ?>