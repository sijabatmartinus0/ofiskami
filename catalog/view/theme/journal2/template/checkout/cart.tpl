<?php echo $header; ?>
<style>
.cart-info tbody td{
    border: thin solid  rgb(228, 228, 228);
}
.text-cart{
	margin:5px;
}
.text-right{
	text-align:right !important;
}
.text-left{
	text-align:left !important;
}
.text-normal{
	font-weight:normal !important;
}
.shipping-detail{
	background-color:#ece7e7 !important;
}
.text-remove{
	position:absolute;
	top:0;
	right:0;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($attention) { ?>
  <div class="alert alert-info information"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> sc-page">
      <h1 class="heading-title"><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1>
      <?php echo $content_top; ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive cart-info">
          
		  <?php 
		  if(isset($cart) && !empty($cart)){
			foreach ($cart as $itemCart) {
			if($itemCart['shipping']['delivery_type']==1){
				$shipping=$itemCart['shipping']['delivery'];
			}else{
				$shipping=$itemCart['shipping']['pickup'];
			}
				
		  ?>
		  <table class="table table-bordered">
            <thead>
			<tr>
				<td class="text-left name" colspan="6"><?php echo $column_seller; ?>&nbsp;<span class='ms-by-seller'><a href="<?php echo $shipping['seller']['link']; ?>"><?php echo $shipping['seller']['nickname']; ?></a></td>
			</tr>
            </thead>
            <tbody>
			<?php 
				$subtotal=0;
				$quantity=0;
				$weight=0;
				$key=array();
				foreach($itemCart['products'] as $product){
				$subtotal+=$product['total_count'];
				$quantity+=$product['quantity'];
				$weight+=$product['weight_count'];
				array_push($key,$product['key']);
				?>
              <tr>
                <td colspan="3" class="text-left name" ><?php if ($product['thumb']) { ?>
                  <div style="float:left">
				  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?>
				  </div>
				  <div style="float:left">
					  <div class="text-cart">
					  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
					  </div>
					  <?php if (!$product['stock']) { ?>
					  <div class="text-cart">
					  <span class="text-danger">***</span>
					  </div>
					  <?php } ?>
					  <?php if ($product['option']) { ?>
					  <?php foreach ($product['option'] as $option) { ?>
					  <div style="">
					  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
					  </div>
					  <?php } ?>
					  <?php } ?>
					  <?php if ($product['reward']) { ?>
					  <div class="text-cart">
					  <small><?php echo $product['reward']; ?></small>
					  </div>
					  <?php } ?>
					  <?php if ($product['recurring']) { ?>
					  <div class="text-cart">
					  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
					  </div>
					  <?php } ?>
					  <div class="text-cart">
					  <span><?php echo $product['quantity']; ?>&nbsp;item&nbsp;(<?php echo $product['weight']; ?>)&nbsp;x <?php echo $product['price']; ?></span>
						</div>
						<div class="text-cart">
						<span><a class="text-normal" onclick="editQuantity('<?php echo $product['key']; ?>')"><i class="fa fa-pencil"></i>&nbsp;edit</a></span>
						</div>
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_product_price; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $product['total']; ?>
					</div>
					</td>
					<td valign="top"  width="20%" colspan="2" class="text-left price">
					<div class="text-cart" style="position:relative">
					<?php echo $column_remark; ?>
					<span><a class="text-normal text-remove" onclick="cartRemove('<?php echo $product['key']; ?>');"><i class="fa fa-trash-o"></i>&nbsp;<?php echo $button_remove; ?></a></span>
					</div>
					<div class="text-cart text-normal">
					<?php echo $product['information']; ?>
					</div>
					</td>
				</tr>
				
				<?php }?>
                <tr class="shipping-detail">
					<td valign="top" class="text-left price">
						<div class="text-cart">
							<?php echo $column_address; ?>&nbsp;<span><a class="text-normal" onclick='editAddress.apply(this,new Array(<?php echo json_encode($key); ?>));'><i class="fa fa-pencil"></i>&nbsp;edit</a></span>
						</div>
						<?php if(isset($shipping['shipping'])) {?>
						<div class="text-cart text-normal">
							<?php echo $shipping['shipping']; ?>-<?php echo $shipping['shipping_service']; ?>
						</div>
						<div class="text-cart">
							<?php echo $shipping['shipping_address']['detail']; ?>
							<br>
							<?php echo $shipping['shipping_address']['telephone']; ?>
						</div>
						<?php }?>
						<?php if(isset($shipping['branch'])) {?>
						<div class="text-cart text-normal">
							<?php echo $shipping['branch']['name']; ?>-<?php echo $shipping['branch']['company']; ?>
						</div>
						<div class="text-cart">
							<?php echo $shipping['branch']['detail']; ?>
							<br>
							<?php echo $shipping['branch']['telephone']; ?>
						</div>
						<?php }?>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
						<?php echo $column_insurance; ?>
					</div>
					<div class="text-cart text-normal">
						<!--<select name="insurance" id="insurance" class="form-control">
						<option value=""><?php echo $text_insurance_no; ?></option>
						<?php if ($text_insurance_no == $insurance) { ?>
						<option value="<?php echo $text_insurance_no; ?>" selected="selected"><?php echo $text_insurance_no; ?></option>
						<?php } else { ?>
						<option value="<?php echo $text_insurance_yes; ?>"><?php echo $text_insurance_yes; ?></option>
						<?php } ?>
				  </select>	-->
				  --
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_total_unit; ?>
					</div>
					<div class="text-cart text-normal">
						<?php echo $quantity; ?>&nbsp;item&nbsp;(<?php echo $this->weight->format($weight,1); ?>)
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_subtotal; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $this->currency->format($subtotal); ?>
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_insurance_charge; ?>
					</div>
					<div class="text-cart">
					--
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_shipping; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo (isset($shipping['price'])?$shipping['price']:"--"); ?>
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="text-left name">
					<span><a class="text-normal" onclick='cartRemoveAll(<?php echo json_encode($key); ?>);'><i class="fa fa-trash-o"></i>&nbsp;<?php echo $text_delete_all; ?></a></span>
					</td>
					<td colspan="2" class="text-right total">
						<span class="text-normal"><?php echo $text_per_invoice; ?></span>
						<span><?php echo $this->currency->format((isset($shipping['price_count'])?$shipping['price_count']:0)+$subtotal);?></span>
						</td>
				</tr>
              </table>
			  <br/><br/>
              <?php } ?>
			   <?php } ?>
			<?php if (isset($vouchers) && !empty($vouchers)) { ?>
			<table class="table table-bordered">
			   <thead>
			<tr>
				<td class="text-left name" colspan="6"><?php echo $text_voucher;?></td>
			</tr>
            </thead>
            <tbody>
              <?php foreach ($vouchers as $vouchers) { ?>
			  <tr>
                <td colspan="3" class="text-left name" >
                  <?php echo $vouchers['description']; ?>
				</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_product_price; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $vouchers['amount']; ?>
					</div>
					</td>
					<td valign="top"  width="20%" colspan="2" class="text-left price">
					<div class="text-cart">
					<?php echo $column_remark; ?>
					</div>
					<div class="text-cart">
					<span><a class="text-normal" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-trash-o"></i>&nbsp;<?php echo $button_remove; ?></a></span>
					</div>
					</td>
				</tr>
			 
              <?php } ?>
			   </tbody>
			   </table>
			   <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
	  <br>

      <div class="action-area">       
        <div class="row">
          <div class="col-sm-4 col-sm-offset-8 cart-total">
            <table class="table table-bordered" id="total">
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td class="text-right right"><strong><?php echo $total['title']; ?>:</strong></td>
                <td class="text-right right"><?php echo $total['text']; ?></td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
        <div class="buttons">
          <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default button"><?php echo $button_shopping; ?></a></div>
          <div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-primary button"><?php echo $button_checkout; ?></a></div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-remove-cart-all" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-cart-label"><?php echo $text_md_remove_all; ?></h4>
      </div>
      <div class="modal-body">
	  <input type="hidden" id="keyCartRemoveAll" />
        <?php echo $text_remove_all_product_confirmation;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
        <button type="button" class="btn btn-primary" onclick="doCartRemoveAll()"><?php echo $button_ok;?></button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-remove-cart" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-cart-label"><?php echo $text_md_remove; ?></h4>
      </div>
      <div class="modal-body">
	  <input type="hidden" id="keyCartRemove" />
        <?php echo $text_remove_product_confirmation;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
        <button type="button" class="btn btn-primary" onclick="doCartRemove()"><?php echo $button_ok;?></button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-edit-cart" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-cart-label"><?php echo $text_md_edit_cart; ?></h4>
      </div>
      <div class="modal-body">
        <div id="modal-edit-cart-content">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
        <button type="button" id="md-button-save-edit-cart" class="btn btn-primary"><?php echo $button_save;?></button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-edit-address" tabindex="-1" role="dialog" aria-labelledby="modal-cart-label"data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-cart-label"><?php echo $text_md_edit_address; ?></h4>
      </div>
      <div class="modal-body">
        <div id="modal-edit-address-content">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel;?></button>
        <button type="button" id="md-button-save-edit-address" class="btn btn-primary"><?php echo $button_save;?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$(function() {
	window.cartRemove = function cartRemove(key){
		$("#keyCartRemove").val(key);
		$('#modal-remove-cart').modal('show');
	}
	window.doCartRemove = function doCartRemove(){
		cart.remove($("#keyCartRemove").val());
	}
	window.cartRemoveAll = function cartRemoveAll(key){
		$("#keyCartRemoveAll").val(key);
		$('#modal-remove-cart-all').modal('show');
	}
	window.doCartRemoveAll = function doCartRemoveAll(key){
		cart.remove.apply(this,new Array($("#keyCartRemoveAll").val()));
	}
	window.editQuantity = function editQuantity(key){
		$.ajax({
			url: 'index.php?route=checkout/cart/loadEditCart',
			type: 'post',
			data: {key:key},
			dataType: "html",
			beforeSend: function() {
				//$('#button-cart').button('loading');
			},
			complete: function() {
				//$('#button-cart').button('reset');
			},
			success: function(json) {
				$('#modal-edit-cart-content').empty().html(json);
				$('#modal-edit-cart').modal('show');
			}
		});
	}
	window.editAddress = function editAddress(key){
		$.ajax({
			url: 'index.php?route=checkout/cart/loadEditAddress',
			type: 'post',
			data: {key:key},
			dataType: "html",
			beforeSend: function() {
				//$('#button-cart').button('loading');
			},
			complete: function() {
				//$('#button-cart').button('reset');
			},
			success: function(json) {
				$('#modal-edit-address-content').empty().html(json);
				$('#modal-edit-address').modal('show');
			}
		});
	}
});
//--></script>
<?php echo $footer; ?> 