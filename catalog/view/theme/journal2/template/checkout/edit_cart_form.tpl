<div class="md-error-content"></div>
<div id="content" class="row md-content md-cart" style="padding: 0px 20px 0 20px !important;">
<div class="content">
    <div class="form-group" style="margin-bottom:10px">
		<label class="col-sm-2 control-label">
			<?php echo $text_md_cart_product_name; ?>
		</label>
		<div class="col-sm-10">
			<input type="hidden" name="key" value="<?php echo $product['key'];?>"/>
			<input type="text" name="product_name" value="<?php echo $product['name'];?>" id="product_name" class="form-control" readonly="readonly" />
		</div>
	</div>
	<div class="form-group" style="margin-bottom:10px">
		<label class="col-sm-2 control-label">
			<?php echo $text_md_cart_product_amount; ?>
		</label>
		<div class="col-sm-10" style="width:25%">
			<input type="text" name="product_amount" value="<?php echo $product['quantity'];?>" id="product_amount" class="form-control"/>
		</div>
	</div>
	<div class="form-group" style="margin-bottom:10px">
		<label class="col-sm-2 control-label">
			<?php echo $text_md_cart_product_information; ?>
		</label>
		<div class="col-sm-10">
			<textarea class="form-control" name="product_information">
				<?php echo $product['shipping']['information'];?>
			</textarea>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
$('input[name=\'product_amount\']').keypress(function (e){
		  var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		  }
		});
$("#md-button-save-edit-cart").unbind('click');
		$('#md-button-save-edit-cart').on('click', function(){
			$.ajax({
                url: 'index.php?route=checkout/cart/editCart',
				type: 'post',
				data:$('.md-cart input[type=\'text\'], .md-cart input[type=\'hidden\'], .md-cart textarea' ),
                dataType: 'json',
                beforeSend: function() {
					$('.alert').remove();
					$('#md-button-save-edit-cart').button('loading');
                },
                complete: function() {
                    $('.fa-spin').remove();
					$('#md-button-save-edit-cart').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();
					$('.form-group').removeClass('has-error');
					if (json['error']) {
						// ERROR SHIPPING
						if (json['error']['amount']){
							var html="";
							if(json['error']['amount']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['amount']+'<br>';
							}
							
							$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>');
						}
						if (json['error']['api']){
							var html="";
							if(json['error']['api']){
								html+='<i class="fa fa-exclamation-circle"></i>'+json['error']['api']+'<br>';
							}
							
							$('.md-error-content').empty().html('<div class="alert alert-danger warning" style="margin-bottom:20px;width:auto">'+html+'</div>');
						}
						
					}
					if ('success' in json){
							console.log(json['success']['url']);
							location=json['success']['url'];
						}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
</script>