<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> order-list">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover list">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
			  <td class="text-right"><?php echo $column_invoice; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-left"><?php echo $column_customer; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td></td>
			  <td width="10%"></td>
            </tr>
          </thead>
          <tbody>
            <?php 
				$order_id = null; $order_id2 = null;
				foreach ($orders as $order) { 
					if ($order_id != $order['order_id']){ ?>
				<tr>
				<td class="text-right" rowspan="<?php echo $order['count_merge']; ?>">#<?php echo $order['order_id']; ?></td>
			<?php
					$order_id = $order['order_id'];
					$order_id2 = $order_id;
					foreach($orders as $order2){
						if($order_id == $order2['order_id']){ ?>
						  <td class="text-left" style="display: none;"><?php echo $order2['order_detail_id']; ?></td>
						  <td class="text-left"><?php echo $order2['invoice_no']; ?></td>
						  <td class="text-left"><?php echo $order2['status']; ?></td>
						  <td class="text-left"><?php echo $order2['date_added']; ?></td>
						  <td class="text-right"><?php echo $order2['products']; ?></td>
						  <?php if($order2['delivery_type_id'] == 1){ ?>
							<td class="text-left"><?php echo $order2['name']; ?></td>
							 <td class="text-right"><?php echo $order2['total']; ?></td>
						  <?php }else if($order2['delivery_type_id'] == 2){ ?>
							<td class="text-left"><?php echo $order2['pp_name']; ?></td>
							 <td class="text-right"><?php echo $order2['total']; ?></td>
						  <?php }else if($order2['delivery_type_id'] == 3){ ?>
							<td class="text-left"><?php echo $order2['to_name']; ?></td>
							 <td class="text-right"><?php echo $order2['total']; ?></td>
						  <?php } ?>
						  <td class="text-right">
						  <a href="<?php echo $order2['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info btn-primary"><i class="fa fa-eye"></i></a>
						  </td>
					<?php
						
						if($order_id2 == $order2['order_id']){
					?>
						  <td class ="text-right" rowspan="<?php echo $order['count_merge']; ?>">
						  <?php if ($order2['order_status_id'] == 1){?>
							<a href="<?php echo $order['confirm']; ?>" data-toggle="tooltip" title="<?php echo $button_confirm; ?>" class="btn btn-info btn-danger"><i class="fa  fa-check-circle"></i></a>
						  <?php } else if($order2['order_status_id'] != 1 && $order2['delivery_type_id'] == 3) {?>
							<a data-toggle="tooltip" title="<?php echo $button_confirm_done; ?>" class="btn btn-info btn-danger"><i class="fa  fa-check-circle"></i></a>
						  <?php } else {?>
							<a data-toggle="tooltip" title="<?php echo $button_confirm_done; ?>" class="btn btn-info btn-danger"><i class="fa  fa-check-circle"></i></a>
						  <?php }?>
						  </td>
					<?php 
							$order_id2 = null;
						} 
					?>
						<tr>				
			<?php		
									
						}
					}
					}	
				}
			?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>