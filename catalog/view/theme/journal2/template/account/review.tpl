<?php echo $header; ?>
<style>
.button-search-review i {
    margin-left: 8px;
    margin-top: 6px;
    display: block;
}
.button-search-review{
    width: 33px;
    height: 33px;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> order-list">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
	  <div class="xl-100">
		  <div class="xl-50">
			<div class="pull-left">
				<span><?php echo $text_display; ?></span>
				<span class="review-button" id="review-all"><a><?php echo $text_all; ?></a></span> | 
				<span class="review-button" id="review-unread"><a><?php echo $text_unread; ?></a></span>
			</div>
		  </div>
		  <div class="xl-50">
			<div class="pull-right">
				<form id="form-review-search">
				<input type="text" placeholder="<?php echo $text_search; ?>" value="" name="search" id="search" style="width: 250px;float:left">
				<div class="button-search button-search-review" style="position:relative"><button type="button"><i></i></button></div>
				</form>
			</div>
		  </div>
	  </div>
	  <div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
	  <div class="xl-100" id="review" style="margin-top:20px;"></div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<script type="text/javascript"><!--
$(function(){
var key='all';
	$('#review').load('index.php?route=account/review/loadReview&key=all');
	$('#review-all').on('click', function() {
			key='all';
			$.ajax({
                url: 'index.php?route=account/review/loadReview&key=all',
				type: 'post',
				dataType: "html",
				data: $("#form-review-search").serialize(),
				beforeSend: function() {
					$('.review-button').css("color", "#FF0000");
					$('.review-button').css("font-weight", "normal");
					$('.pcAttention').show();
                },
                complete: function() {
					$('#review-all').css("color", "#000000");
					$('#review-all').css("font-weight", "bold");
					$('.pcAttention').hide();
                },
                success: function(json) {
					$('#review').empty().html(json);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
	$('#review-unread').on('click', function() {
			key='unread';
			$.ajax({
                url: 'index.php?route=account/review/loadReview&key=unread',
				type: 'post',
				dataType: "html",
				data: $("#form-review-search").serialize(),
				beforeSend: function() {
					$('.review-button').css("color", "#FF0000");
					$('.review-button').css("font-weight", "normal");
					$('.pcAttention').show();
                },
                complete: function() {
					$('#review-unread').css("color", "#000000");
					$('#review-unread').css("font-weight", "bold");
					$('.pcAttention').hide();
                },
                success: function(json) {
					$('#review').empty().html(json);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
	$('#form-review-search').on('keyup keypress', function(e) {
		  var code = e.keyCode || e.which;
		  if (code == 13) { 
			e.preventDefault();
			$.ajax({
                url: 'index.php?route=account/review/loadReview&key='+key,
				type: 'post',
				dataType: "html",
				data: $("#form-review-search").serialize(),
				beforeSend: function() {
					$('.pcAttention').show();
                },
                complete: function() {
					$('.pcAttention').hide();
                },
                success: function(json) {
					$('#review').empty().html(json);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
			return false;
		  }
		});
	$('.button-search-review').on('click', function() {
			$.ajax({
                url: 'index.php?route=account/review/loadReview&key='+key,
				type: 'post',
				dataType: "html",
				data: $("#form-review-search").serialize(),
				beforeSend: function() {
					$('.pcAttention').show();
                },
                complete: function() {
					$('.pcAttention').hide();
                },
                success: function(json) {
					$('#review').empty().html(json);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
		});
		
	$(document).ajaxComplete(function() {
		$('#review .pagination>ul>li>a').click(function(event){
			event.preventDefault();
			$('.pcAttention').show();
			$('#review').empty().load($(this).attr('href'),function(){$('.pcAttention').hide();});
		});
	});
});
//--></script> 
<?php echo $footer; ?>
