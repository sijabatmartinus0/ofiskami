<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <?php if ($success) { ?>
	<div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> order-list">
    <h1 class="heading-title"><?php echo $heading_title; ?></h1>
	
	<?php if($return_status_id == 2) {?>
		<div class="pull-right" style="margin-bottom: 10px;" >
		<a href="javascript:;" onclick="document.getElementById('form_complete_return').submit();" data-toggle="tooltip" title="<?php echo $text_complete_return; ?>" class="btn btn-primary"><i class="fa fa-check-square-o" ></i></a>
		</div>
	<?php } ?>
	
	<form method="post" action="<?php echo $link_complete_return; ?>" id="form_complete_return">
	<input type="hidden" name="input_return_id" value="<?php echo $return_id; ?>" />
	</form>
	
      <?php echo $content_top; ?>
      <table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" colspan="3"><?php echo $text_return_detail; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
           <td class="text-left" style="width: 30%;">
				<b><?php echo $text_return_id; ?></b> #<?php echo $return_id; ?><br />
				<b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?>
			</td>
            <td class="text-left" style="width: 30%;">
				<b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
				<b><?php echo $text_date_ordered; ?></b> <?php echo $date_ordered; ?><br />
				<b><?php echo $column_invoice; ?></b> <?php echo $invoice_no; ?>
			</td>
			<td class="text-left" style="width: 40%;">
				<b><?php echo $column_customer; ?>:</b> <?php echo $name; ?><br />
				<b><?php echo $column_phone; ?>:</b> <?php echo $telephone; ?><br />
				<b><?php echo $text_email; ?></b> <?php echo $email; ?>
			</td>
          </tr>
        </tbody>
      </table>
	  
      <table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" colspan="3"><?php echo $text_product; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 15%;">
				<img src="<?php echo $thumb; ?>" alt="<?php echo $product; ?>" title="<?php echo $product; ?>" class="img-thumbnail"/>
			</td>
            <td class="text-left" style="text-align: left; vertical-align: top;">
				<b><?php echo $column_product; ?>:</b> <a href="<?php echo $href; ?>"><?php echo $product; ?></a><br/>
				<b><?php echo $column_model; ?>:</b> <?php echo $model; ?>  <br/>
				<b><?php echo $column_quantity; ?>:</b> <?php echo $quantity; ?> 
			</td>
			<td class="text-left" style="text-align: left; left; vertical-align: top;">
				<b><?php echo $column_opened; ?>:</b> <?php echo $opened; ?>  <br/>
				<b><?php echo $column_reason; ?>:</b> <?php echo $reason; ?> <br/>
				<?php if($comment){ ?>
					<b><?php echo $column_comment; ?>:</b> <?php echo $comment; ?>  <br/>
				<?php } else {?>
					<b><?php echo $column_comment; ?>:</b> - <br/>
				<?php } ?>
				<b><?php echo $column_action; ?>:</b> <?php echo $action; ?> <br/>
				<form method="post" action="<?php echo $link_confirm_action_changed; ?>" id="form_confirm_action_changed">
				<input type="hidden" name="input_return_id_action" value="<?php echo $return_id; ?>" />
				<?php if ($is_action_changed == 1 && $confirm_action_changed == 0) { ?>
				  <div class="alert alert-danger warning">
				  <i class="fa fa-exclamation-circle"></i> <?php echo $warning_action_changed; ?>
				  <br/> <a href="javascript:;" onclick="document.getElementById('form_confirm_action_changed').submit();"><i class="fa fa-check-circle"></i> <?php echo $text_confirm; ?> </a>
				  </div>
				<?php } ?>
				</form>
			</td>
          </tr>
        </tbody>
      </table>
      <br class="wide" />
	  <h1 class="heading-title"><?php echo $text_discuss; ?></h1>
	  <div class="panel panel-default">
			<div class="panel-collapse collapse in">
				<div class="panel-body">
				<div id="discuss_return">
				
				</div>
				
				<div class="pcAttention" style="display:none;margin: 0 auto;text-align:center"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>
				<?php if(count($return_comments) == 2) { ?>
					<a id="view_more" class="pcViewBtn icon-only"><span ><?php echo $text_view_more; ?></span><i style="margin-left: 5px; font-size: 16px" class="fa fa-comments"></i></a>
				<?php } ?>
				<div class="comment-title"></div>
				<br/>
				<?php if($return_status_id !=3 ){?>
				<div class="comment-form">
					<form class="pcForm" id="form_return_comments" method="post" >
						<input type="hidden" id="discuss_return_id" name="discuss_return_id" value="<?php echo $return_id; ?>">
						<input type="hidden" id="discuss_product_id" name="discuss_product_id" value="<?php echo $product_id; ?>">
						<textarea class="pcText" id="discuss_comment" name="discuss_comment" cols="40" rows="8" style="width: 100%;" placeholder="<?php $column_comment; ?>"></textarea>
					</form>
					<div id="error_discuss" class="text-danger"><?php echo $error_discuss; ?></div>
				</div>
				<br>
				<div class="right-side">
					<a id="pcSubmitBtn" class="button pcSubmitBtn"><span><?php echo $button_submit; ?></span></a>
				</div>
				<?php }?>
				</div>
			</div>
		</div>
	  
      <br class="wide" />
      <?php if ($histories) { ?>
      <h1 class="heading-title"><?php echo $text_history; ?></h1>
      <table class="list table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left bold" style="width: 20%;"><?php echo $column_date_added; ?></td>
            <td class="text-left bold"><?php echo $column_status; ?></td>
			
          </tr>
        </thead>
        <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><i class="fa fa-clock-o" style="padding: 3px; padding: 5px;"></i> <?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
			
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<script type="text/javascript">
$(function(){
	$('.pcAttention').show();
	$('#view_more').hide();
	$('#error_discuss').hide();
	$('#discuss_return').load('<?php echo $list_up_comments; ?>&return_id=' +<?php echo $return_id; ?>,function(){$('.pcAttention').hide();$('#view_more').show();});
	
	$("#view_more").on('click', function(){
		$('.pcAttention').show();
		$('#discuss_return').load('<?php echo $list_return_comments; ?>&return_id=' +<?php echo $return_id; ?>,function(){$('.pcAttention').hide();});
		$('#view_more').hide();
	});
	
	 $(document).ready(function(){
	 <?php if($return_status_id !=3 ){?>
		$("#pcSubmitBtn").click(function(){
			var comment = $("#discuss_comment").val();
			var return_id = $("#discuss_return_id").val();
			var product_id = $("#discuss_product_id").val();
			
			$('.pcAttention').show();
			 $.ajax({
				url: 'index.php?route=account/return/add_comment',
				type: "POST",
				cache: false,
				data: 
				{ 
					discuss_comment: comment, 
					discuss_return_id: return_id, 
					discuss_product_id: product_id
				},
				success : function(html){
					if($("#discuss_comment").val().length < 2){
						$('#error_discuss').show();
						$('.pcAttention').hide();
					}else{
						$('#error_discuss').hide();
						$('.pcAttention').hide();
						loadComments();
						$("#discuss_comment").attr("value", "");
						// setInterval (loadComments, 3000);
					}
				}
			 })
		});
		<?php }?>
		function loadComments(){
			$.ajax({
				cache : false,
				success : function(html){
					$('#discuss_return').load('<?php echo $list_return_comments; ?>&return_id=' +<?php echo $return_id; ?>);
					$('#view_more').hide();
				},
			});
		}
		
	 }); 
});

</script>
<?php echo $footer; ?>