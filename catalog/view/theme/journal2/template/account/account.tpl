<?php echo $header; ?>
<style>
.form-group{
	border-bottom:rgb(245,245,245) 1px solid;
	background:rgb(253,253,253);
}
.form-group .label-right{
	text-align: right;
}
.form-group .label-left{
	text-align: left;
	font-weight:bold;
	width:70% !important;
}
.notification-container{
	width:100%;
	height:30px;
	text-align:right;
	margin-bottom:10px;
}
.notification{
	float:left;
	
	height:30px;
	background:#eaeaea;
	margin:5px;
	display: block;
	position:absolute;
	text-align:center;
	padding-top:5px;
	border-radius: 5px;
}
.order{
	right:60px;
	width:30px;
}
.reward-point{
	right:100px;
	font-weight:bold;
	width:auto;
	padding-left:10px;
	padding-right:10px;
}
.review{
	right:20px;
	width:30px;
}
span.badge{
	position: absolute;
    top: -5px;
    right: -10px;
}
.btn-sm{
	padding:3px 8px;
}

</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	<?php echo $content_top; ?>
	<div class="notification-container">
		<a class="notification review" href="<?php echo $review;?>"><i class="fa fa-thumbs-o-up fa-lg"></i><span class="badge"><?php echo $notification_review; ?></span></a>
		<a class="notification order" href="<?php echo $order;?>"><i class="fa fa-th-list fa-lg"></i><span class="badge"><?php echo $notification_order; ?></span></a>
		<a class="notification reward-point" href="<?php echo $reward;?>"><i class="fa fa-smile-o fa-lg"></i><?php echo $notification_reward; ?></a>
	</div>
	<div class="xl-100">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_name;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $name;?><a style="margin-left:10px;" href="<?php echo $edit;?>"><i class="fa fa-pencil"></i>&nbsp;<?php echo $button_edit;?></a></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_email;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $email;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_phone;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $phone;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_dob;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $dob;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_account_bank;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $account;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_account_name;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $account_name;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_account_number;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $account_number;?></label>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"><?php echo $text_balance;?></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><?php echo $balance;?></label>
				</div>
		</div>
		<?php if($this->customer->getVerified()!=2){ ?>
		<div class="form-group">
			<label class="col-sm-2 control-label label-right"></label>
				<div class="col-sm-10">
					<label class="col-sm-10 control-label label-left"><a href="<?php echo $activate;?>"><?php echo $text_activate;?></a></label>
				</div>
		</div>
		<?php }?>
		
	</div>
	</div>
	<div class="xl-100">
	<div class="panel panel-default xl-50">
  <div class="panel-heading">
    <h2 class="panel-title"><i class="fa fa-th-list"></i> <?php echo $text_list_order;?></h2>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-hover list" style="margin-bottom: 10px;">
      <thead>
        <tr>
          <td class="text-right"><?php echo $text_column_invoice_order;?></td>
          <td><?php echo $text_column_status_order;?></td>
          <td class="text-right"><?php echo $text_column_action_order;?></td>
        </tr>
      </thead>
      <tbody>
		<?php if(!empty($orders)){?>
		<?php foreach($orders as $order_data){?>
        <tr>
          <td class="text-right"><?php echo $order_data['invoice'];?></td>
          <td><?php echo $order_data['status'];?></td>
          <td class="text-right"><a href="<?php echo $order_data['link']; ?>" data-toggle="tooltip" title="<?php echo $text_view;?>" class="btn btn-info btn-sm" data-original-title="<?php echo $text_view;?>"><i class="fa fa-eye"></i></a></td>
        </tr>
		 <?php }?>
		 <?php }else{?>
		 <tr><td colspan="3"><?php echo $text_no_data;?></td></tr>
		 <?php }?>
        </tbody>
    </table>
	<a style="margin-left:10px;" href="<?php echo $order;?>"><?php echo $text_view_more;?></a>
  </div>
</div>
<div class="panel panel-default xl-50">
  <div class="panel-heading">
    <h2 class="panel-title"><i class="fa fa-mail-reply"></i>  <?php echo $text_list_return;?></h2>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-hover list" style="margin-bottom: 10px;">
      <thead>
        <tr>
          <td class="text-right"><?php echo $text_column_invoice_return;?></td>
          <td><?php echo $text_column_product_return;?></td>
		   <td><?php echo $text_column_status_return;?></td>
          <td class="text-right"><?php echo $text_column_action_return;?></td>
        </tr>
      </thead>
      <tbody>
          <?php if(!empty($returns)){?>
		<?php foreach($returns as $return_data){?>
        <tr>
          <td class="text-right"><?php echo $return_data['invoice'];?></td>
		  <td><?php echo $return_data['product'];?></td>
          <td><?php echo $return_data['status'];?></td>
          <td class="text-right"><a href="<?php echo $return_data['link']; ?>" data-toggle="tooltip" title="<?php echo $text_view;?>" class="btn btn-info btn-sm" data-original-title="<?php echo $text_view;?>"><i class="fa fa-eye"></i></a></td>
        </tr>
		 <?php }?>
		 <?php }else{?>
		 <tr><td colspan="4"><?php echo $text_no_data;?></td></tr>
		 <?php }?>
       </tbody>
    </table>
	<a style="margin-left:10px;" href="<?php echo $return;?>"><?php echo $text_view_more;?></a>
  </div>
</div>
</div>
      
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>