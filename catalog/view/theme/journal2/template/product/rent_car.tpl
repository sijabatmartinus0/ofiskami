<?php echo $header; ?>
<style>
#tabs a:hover, #tabs a.selected, #tabs li a:hover, #tabs li.active a {
    color: rgb(255, 255, 255);
    background-color: rgb(70, 119, 210);
	width: 100%;
}
#tabs a, #tabs li a {
	width: 100%;
}
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <div class="container ng-scope">

    <!-- Service Tabs -->
    <div class="row col-sm-12">
        <div class="col-sm-10">
            <img src="<?php echo $logo;?>" style="width:140px">
        </div>
    </div>
    <div class="col-lg-12">
<ul id="tabs" class="nav nav-tabs htabs" style="margin-top: 5px;">
            <li class="active md-tab-delivery" style="width: 50%;"><a href="#md-tab-delivery" data-toggle="tab"><i class="fa fa-car"></i> Daily Rental</a></li>
            <li class="md-tab-pickup" style="width: 50%;"><a href="#md-tab-pickup" data-toggle="tab"><i class="fa fa-plane"></i> Airport Pick Up</a></li>
        </ul>
        
		<div class="tabs-content">
            <div class="tab-pane tab-content active" style="padding:0px 15px 15px 15px;margin-bottom: 10px;" id="md-tab-delivery">
              <div class="content" style="margin-top:20px">
			  <form method="post" enctype="multipart/form-data" class="form-horizontal">
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Pick Up City</label>
				<div class="col-sm-10">
				  <select class="form-control  ng-pristine ng-valid" style="width:50%;margin:0">
                            <option value="">Select City</option>
                            <option value="DKI Jakarta">DKI Jakarta</option>
                            <option value="Bekasi">Bekasi</option>
                            <option value="Bandung">Bandung</option>
                            <option value="Surabaya">Surabaya</option>
                        </select>
						</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Pick Up Location</label>
				<div class="col-sm-10">
				  <input type="text" name="firstname" style="width:50%" placeholder="Pick Up Location" id="input-firstname" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Rental Package</label>
				<div class="col-sm-10">
				  <select class="form-control  ng-pristine ng-valid" style="width:50%;margin:0">
                           <option value="">Select Package</option>
                            <option value="With Driver">With Driver</option>
                            <option value="4 Hours">4 Hours</option>
                        </select>
						</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Start Date & Time</label>
				<div class="col-sm-10">
				  <div class="input-group datetime" style="width:50%">
						<input style="width:100%" type="text" class="form-control inline"  data-date-format="YYYY-MM-DD HH:mm:ss" />
						<span class="input-group-btn" style="width:10%">
                			<button style="width:100%" class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
						</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">End Time</label>
				<div class="col-sm-10">
				  <div class="input-group datetime" style="width:50%">
						<input style="width:100%" type="text" class="form-control inline"  data-date-format="YYYY-MM-DD HH:mm:ss" />
						<span class="input-group-btn" style="width:10%">
                			<button style="width:100%" class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
						</div>
			  </div>
			  </form>
			  </div>
			 
                
            </div>
            <div class="tab-pane tab-content" style="padding:0px 15px 15px 15px;" id="md-tab-pickup">
                <div class="content" style="margin-top:20px">
			  <form method="post" enctype="multipart/form-data" class="form-horizontal">
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Pick Up City</label>
				<div class="col-sm-10">
				  <select class="form-control  ng-pristine ng-valid" style="width:50%;margin:0">
                            <option value="">Select City</option>
                            <option value="DKI Jakarta">DKI Jakarta</option>
                            <option value="Bekasi">Bekasi</option>
                            <option value="Bandung">Bandung</option>
                            <option value="Surabaya">Surabaya</option>
                        </select>
						</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Airline & Flight No.</label>
				<div class="col-sm-10">
					<div class="xl-40">
				  <input type="text" name="firstname" placeholder="Airline" id="input-firstname" class="form-control" />
					</div>
					<div class="xl-10">
				  <input type="text" name="firstname"  placeholder="Flight No." id="input-firstname" class="form-control" />
					</div>
					<div class="xl-50" style="padding:5px 0px 0px 5px;">
					e.g Garuda Indonesia e.g. GA331
					</div>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Destination</label>
				<div class="col-sm-10">
				  <input type="text" name="firstname" style="width:50%" placeholder="Enter location" id="input-firstname" class="form-control" />
				</div>
			  </div>
			  
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-firstname">Arrival Date & Time</label>
				<div class="col-sm-10">
				  <div class="input-group datetime" style="width:50%">
						<input style="width:100%" type="text" class="form-control inline"  data-date-format="YYYY-MM-DD HH:mm:ss" />
						<span class="input-group-btn" style="width:10%">
                			<button style="width:100%" class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
						</div>
						</div>
			  </div>
			  </form>
			  </div>
            </div>
        </div>
		<div class="buttons">
			  <div class="pull-left"><a href="#" class="btn btn-primary button">Book A Car</a></div>
			</div>
		<!--END-->
    </div>
</div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<script type="text/javascript">
$(function() {
	$("body").delegate(".date", "focusin", function(){
		$(this).datetimepicker({pickTime: false});
	});

	$("body").delegate(".datetime", "focusin", function(){
		$(this).datetimepicker({pickTime: true, pickDate: true});
	});

    $("body").delegate(".time", "focusin", function(){
		$(this).datetimepicker({pickDate: false});
	});
});

</script>
<?php echo $footer; ?>