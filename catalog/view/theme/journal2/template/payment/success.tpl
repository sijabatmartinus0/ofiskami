<?php echo $header; ?>
<div id="container" class="container j-container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	
	<div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<center>
			<h1 class="heading-title"><?php echo $heading_title; ?></h1>
			
			<?php echo $text_first; ?> <br/>
			<?php echo $text_second; ?><br/>
			<h2 class="bold"><?php echo $total; ?></h2>
			<hr style="border-bottom: 1px dashed #ccc; margin:15px 0px 20px 0;"/>
			<?php echo $text_third; ?><br/>
			<?php echo $text_fourth; ?>
		</center>		
		
		<br/><br/>
		<?php echo $content_bottom; ?></div>
	</div>
	
</div>

<?php echo $footer; ?>