<style>
.cart-info tbody td{
    border: thin solid  rgb(228, 228, 228);
}
.text-cart{
	margin:5px;
}
.text-right{
	text-align:right !important;
}
.text-left{
	text-align:left !important;
}
.text-normal{
	font-weight:normal !important;
}
.shipping-detail{
	background-color:#ece7e7 !important;
}
</style>
<div class="checkout-content checkout-cart">
    <h2 class="secondary-title"><?php echo $this->journal2->settings->get('one_page_lang_shop_cart', 'Shopping Cart'); ?></h2>
    <div class="checkout-product">
	<div class="table-responsive cart-info" style="max-height: inherit;">
          
	<?php 
		if(isset($cart) && !empty($cart)){
			foreach ($cart as $itemCart) {
			if($itemCart['shipping']['delivery_type']==1){
				$shipping=$itemCart['shipping']['delivery'];
			}else{
				$shipping=$itemCart['shipping']['pickup'];
			}
				
		  ?>
		  <table class="table table-bordered">
            <thead>
			<tr>
				<td class="text-left name" colspan="6" style="padding-left:20px"><?php echo $column_seller; ?>&nbsp;<span class='ms-by-seller'><a href="<?php echo $shipping['seller']['link']; ?>"><?php echo $shipping['seller']['nickname']; ?></a></td>
			</tr>
            </thead>
            <tbody>
			<?php 
				$subtotal=0;
				$quantity=0;
				$weight=0;
				$key=array();
				foreach($itemCart['products'] as $product){
				$subtotal+=$product['total_count'];
				$quantity+=$product['quantity'];
				$weight+=$product['weight_count'];
				array_push($key,$product['key']);
				?>
              <tr>
                <td colspan="3" class="text-left name" ><?php if ($product['thumb']) { ?>
                  <div style="float:left">
				  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?>
				  </div>
				  <div style="float:left">
					  <div class="text-cart">
					  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
					  </div>
					  <?php if (!$product['stock']) { ?>
					  <div class="text-cart">
					  <span class="text-danger">***</span>
					  </div>
					  <?php } ?>
					  <?php if ($product['option']) { ?>
					  <?php foreach ($product['option'] as $option) { ?>
					  <div style="">
					  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
					  </div>
					  <?php } ?>
					  <?php } ?>
					  <?php if ($product['reward']) { ?>
					  <div class="text-cart">
					  <small><?php echo $product['reward']; ?></small>
					  </div>
					  <?php } ?>
					  <?php if ($product['recurring']) { ?>
					  <div class="text-cart">
					  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
					  </div>
					  <?php } ?>
					  <div class="text-cart">
					  <span><?php echo $product['quantity']; ?>&nbsp;item&nbsp;(<?php echo $product['weight']; ?>)&nbsp;x <?php echo $product['price']; ?></span>
						</div>
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_product_price; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $product['price']; ?>
					</div>
					</td>
					<td valign="top"  width="20%" colspan="2" class="text-left price">
					<div class="text-cart">
					<?php echo $column_remark; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $product['information']; ?>
					</div>
					</td>
				</tr>
				
				<?php }?>
                <tr class="shipping-detail">
					<td valign="top" class="text-left price">
						<div class="text-cart">
							<?php echo $column_address; ?>
						</div>
						<?php if(isset($shipping['shipping'])) {?>
						<div class="text-cart text-normal">
							<?php echo $shipping['shipping']; ?>-<?php echo $shipping['shipping_service']; ?>
						</div>
						<div class="text-cart">
							<?php echo $shipping['shipping_address']['detail']; ?>
							<br>
							<?php echo $shipping['shipping_address']['telephone']; ?>
						</div>
						<?php }?>
						<?php if(isset($shipping['branch'])) {?>
						<div class="text-cart text-normal">
							<?php echo $shipping['branch']['name']; ?>-<?php echo $shipping['branch']['company']; ?>
						</div>
						<div class="text-cart">
							<?php echo $shipping['branch']['detail']; ?>
							<br>
							<?php echo $shipping['branch']['telephone']; ?>
						</div>
						<?php }?>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
						<?php echo $column_insurance; ?>
					</div>
					<div class="text-cart text-normal">
				  --
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_total_unit; ?>
					</div>
					<div class="text-cart text-normal">
						<?php echo $quantity; ?>&nbsp;item&nbsp;(<?php echo $this->weight->format($weight,1); ?>)
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_subtotal; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $this->currency->format($subtotal); ?>
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_insurance_charge; ?>
					</div>
					<div class="text-cart">
					--
					</div>
					</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_shipping; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo (isset($shipping['price'])?$shipping['price']:"--"); ?>
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="text-left name">
					</td>
					<td colspan="2" class="text-right total">
						<span class="text-normal"><?php echo $text_per_invoice; ?></span>
						<span><?php echo $this->currency->format((isset($shipping['price_count'])?$shipping['price_count']:0)+$subtotal);?></span>
						</td>
				</tr>
              </table>
			  <br/><br/>
              <?php } ?>
		<?php } ?>
			
             <?php if (isset($vouchers) && !empty($vouchers)) { ?>
			<table class="table table-bordered">
			   <thead>
			<tr>
				<td class="text-left name" colspan="6" style="padding-left:20px"><?php echo $text_voucher;?></td>
			</tr>
            </thead>
            <tbody>
              <?php 
				$total_voucher=0;
			  foreach ($vouchers as $voucher) { 
			  ?>
			  
			  <tr>
                <td colspan="3" class="text-left name" >
                  <?php echo $voucher['description']; ?>
				</td>
					<td valign="top" class="text-left price">
					<div class="text-cart">
					<?php echo $column_product_price; ?>
					</div>
					<div class="text-cart text-normal">
					<?php echo $voucher['amount']; $total_voucher+=$voucher['amount_count']; ?>
					</div>
					</td>
					<td valign="top"  width="20%" colspan="2" class="text-left price">
					<div class="text-cart">
					<?php echo $column_remark; ?>
					</div>
					</td>
				</tr>
			 
              <?php } ?>
			  <tr>
					<td colspan="4" class="text-left name">
					</td>
					<td colspan="2" class="text-right total">
						<span class="text-normal"><?php echo $text_per_invoice; ?></span>
						<span><?php echo $this->currency->format($total_voucher);?></span>
						</td>
				</tr>
			   </tbody>
			   </table>
			   <br/><br/>
			   <?php } ?>
            </tbody>
          </table>
        </div>

		<table class="table table-bordered table-hover">
            <tfoot>
            <?php foreach ($totals as $total) { ?>
                <tr>
                    <td colspan="4" class="text-right"><?php echo $total['title']; ?>:</td>
                    <td class="text-right" style="width:250px"><?php echo $total['text']; ?></td>
                </tr>
            <?php } ?>
            </tfoot>
        </table>
    </div>
    <div id="payment-confirm-button" class="payment-<?php echo Journal2Utils::getProperty($this->session->data, 'payment_method.code'); ?>">
        <h2 class="secondary-title"><?php echo $this->journal2->settings->get('one_page_lang_payment_details', 'Payment Details'); ?></h2>
        <?php echo $payment; ?>
    </div>
</div>

