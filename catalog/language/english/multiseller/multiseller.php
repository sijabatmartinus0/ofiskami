<?php

// **********
// * Global *
// **********
$_['ms_viewinstore'] = 'View in store';
$_['ms_view'] = 'View';
$_['ms_view_modify'] = 'View / Modify';
$_['ms_publish'] = 'Publish';
$_['ms_unpublish'] = 'Unpublish';
$_['ms_edit'] = 'Edit';
$_['ms_comment'] = 'Comment';
$_['ms_clone'] = 'Clone';
$_['ms_relist'] = 'Relist';
$_['ms_rate'] = 'Rate';
$_['ms_download'] = 'Download';
$_['ms_create_product'] = 'Create product';
$_['ms_delete'] = 'Delete';
$_['ms_update'] = 'Update';
$_['ms_type'] = 'Type';
$_['ms_amount'] = 'Amount';
$_['ms_status'] = 'Status';
$_['ms_date_paid'] = 'Date paid';
$_['ms_last_message'] = 'Last message';
$_['ms_description'] = 'Description';
$_['ms_id'] = '#';
$_['ms_by'] = 'by';
$_['ms_action'] = 'Action';
$_['ms_sender'] = 'Sender';
$_['ms_message'] = 'Message';
$_['ms_none'] = 'None';


$_['ms_date_created'] = 'Date created';
$_['ms_date'] = 'Date';

$_['ms_button_submit'] = 'Submit';
$_['ms_button_add_special'] = 'Define a new special price';
$_['ms_button_add_discount'] = 'Define a new quantity discount';
$_['ms_button_submit_request'] = 'Submit request';
$_['ms_button_save'] = 'Save';
$_['ms_button_cancel'] = 'Cancel';
$_['ms_button_select_predefined_avatar'] = 'Select Pre-defined avatar';

$_['ms_button_select_image'] = 'Select image';
$_['ms_button_select_images'] = 'Select images';
$_['ms_button_select_files'] = 'Select files';

$_['ms_transaction_order_created'] = 'Order created';
$_['ms_transaction_order'] = 'Sale: Order Id #%s';
$_['ms_transaction_sale'] = 'Sale: %s';
$_['ms_transaction_refund'] = 'Refund: %s';
$_['ms_transaction_refund_shipping'] = 'Refund shipping';
$_['ms_transaction_listing'] = 'Product listing: %s (%s)';
$_['ms_transaction_signup']      = 'Signup fee at %s';
$_['ms_request_submitted'] = 'Your request is submitted';

$_['ms_totals_line'] = 'Curnently %s sellers and %s products for sale!';

$_['ms_text_welcome'] = '<a href="%s">Login</a> | <a href="%s">Create an account</a> | <a href="%s">Create a seller account</a>.';
$_['ms_button_register_seller'] = 'Register as a seller';
$_['ms_register_seller_account'] = 'Register a seller account';

// Mails

// Seller
$_['ms_mail_greeting'] = "Hello %s,\n\n";
$_['ms_mail_greeting_no_name'] = "Hello,\n\n";
$_['ms_mail_ending'] = "\n\nRegards,\n%s";
$_['ms_mail_message'] = "\n\nMessage:\n%s";

$_['ms_mail_subject_seller_account_created_by_admin'] = 'Seller account created';
$_['ms_mail_seller_account_created_by_admin'] = <<<EOT
Your seller account at %s has been created!

Please click folowing link to change your password.

%s

You can't start adding your product before change your password.
EOT;

$_['ms_mail_subject_seller_account_created'] = 'Seller account created';
$_['ms_mail_seller_account_created'] = <<<EOT
Your seller account at %s has been created!

You can now start adding your products.
EOT;

$_['ms_mail_subject_seller_account_awaiting_moderation'] = 'Seller account awaiting moderation';
$_['ms_mail_seller_account_awaiting_moderation'] = <<<EOT
Your seller account at %s has been created and is now awaiting moderation.

You will receive an email as soon as it is approved.
EOT;

$_['ms_mail_subject_product_awaiting_moderation'] = 'Product awaiting moderation';
$_['ms_mail_product_awaiting_moderation'] = <<<EOT
Your product %s at %s is awaiting moderation.

You will receive an email as soon as it is processed.
EOT;

$_['ms_mail_subject_product_purchased'] = 'New order';
$_['ms_mail_product_purchased'] = <<<EOT
Your product(s) have been purchased from %s.

Customer: %s (%s)

Products:
%s
Total: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
Your product(s) have been purchased from %s.

Customer: %s

Products:
%s
Total: %s
EOT;

$_['ms_mail_subject_seller_contact'] = 'New customer message';
$_['ms_mail_seller_contact'] = <<<EOT
You have received a new customer message!

Name: %s

Email: %s

Product: %s

Message:
%s
EOT;

$_['ms_mail_seller_contact_no_mail'] = <<<EOT
You have received a new customer message!

Name: %s

Product: %s

Message:
%s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
Delivery address:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'Comment: %s';

$_['ms_mail_subject_withdraw_request_submitted'] = 'Payout request submitted';
$_['ms_mail_withdraw_request_submitted'] = <<<EOT
We have received your payout request. You will receive your earnings as soon as it is processed.
EOT;

$_['ms_mail_subject_withdraw_request_completed'] = 'Payout completed';
$_['ms_mail_withdraw_request_completed'] = <<<EOT
Your payout request has been processed. You should now receive your earnings.
EOT;

$_['ms_mail_subject_withdraw_request_declined'] = 'Payout request declined';
$_['ms_mail_withdraw_request_declined'] = <<<EOT
Your payout request has been declined. Your funds have been returned to your balance at %s.
EOT;

$_['ms_mail_subject_transaction_performed'] = 'New transaction';
$_['ms_mail_transaction_performed'] = <<<EOT
New transaction has been added to your account at %s.
EOT;

$_['ms_mail_subject_remind_listing'] = 'Product listing has finished';
$_['ms_mail_seller_remind_listing'] = <<<EOT
Your product's %s listing has finished. Go to your account seller area if you would like to re-list the product.
EOT;

// *********
// * Admin *
// *********
$_['ms_mail_admin_subject_seller_account_created'] = 'New seller account created';
$_['ms_mail_admin_seller_account_created'] = <<<EOT
New seller account at %s has been created!
Seller name: %s (%s)
E-mail: %s
EOT;

$_['ms_mail_admin_subject_seller_account_awaiting_moderation'] = 'New seller account awaiting moderation';
$_['ms_mail_admin_seller_account_awaiting_moderation'] = <<<EOT
New seller account at %s has been created and is now awaiting moderation.
Seller name: %s (%s)
E-mail: %s

You can process it in the Multiseller - Sellers section in back office.
EOT;

$_['ms_mail_admin_subject_product_created'] = 'New product added';
$_['ms_mail_admin_product_created'] = <<<EOT
New product %s has been added to %s.

You can view or edit it in back office.
EOT;

$_['ms_mail_admin_subject_new_product_awaiting_moderation'] = 'New product awaiting moderation';
$_['ms_mail_admin_new_product_awaiting_moderation'] = <<<EOT
New product %s has been added to %s and is awaiting moderation.

You can process it in the Multiseller - Products section in back office.
EOT;

$_['ms_mail_admin_subject_edit_product_awaiting_moderation'] = 'Product edited and awaiting moderation';
$_['ms_mail_admin_edit_product_awaiting_moderation'] = <<<EOT
Product %s at %s has been edited and is awaiting moderation.

You can process it in the Multiseller - Products section in back office.
EOT;

$_['ms_mail_admin_subject_withdraw_request_submitted'] = 'Payout request awaiting moderation';
$_['ms_mail_admin_withdraw_request_submitted'] = <<<EOT
New payout request has been submitted.

You can process it in the Multiseller - Finances section in back office.
EOT;

// Success
$_['ms_success_product_published'] = 'Product published';
$_['ms_success_product_unpublished'] = 'Product unpublished';
$_['ms_success_product_created'] = 'Product created';
$_['ms_success_product_updated'] = 'Product updated';
$_['ms_success_product_deleted'] = 'Product deleted';

// Errors
$_['ms_error_sellerinfo_nickname_empty'] = 'Nickname cannot be empty';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'Nickname can only contain alphanumeric symbols';
$_['ms_error_sellerinfo_nickname_utf8'] = 'Nickname can only contain printable UTF-8 symbols';
$_['ms_error_sellerinfo_nickname_latin'] = 'Nickname can only contain alphanumeric symbols and diacritics';
$_['ms_error_sellerinfo_nickname_length'] = 'Nickname should be between 4 and 50 characters';
$_['ms_error_sellerinfo_nickname_taken'] = 'This nickname is already taken';
$_['ms_error_sellerinfo_company_length'] = 'Company name cannot be longer than 50 characters';
$_['ms_error_sellerinfo_description_length'] = 'Description cannot be longer than 1000 characters';
$_['ms_error_sellerinfo_paypal'] = 'Invalid PayPal address';
$_['ms_error_sellerinfo_terms'] = 'Warning: You must agree to the %s!';
$_['ms_error_file_extension'] = 'Invalid extension';
$_['ms_error_file_type'] = 'Invalid file type';
$_['ms_error_file_size'] = 'File too big';
$_['ms_error_image_too_small'] = 'Image file dimensions are too small. Minimum allowed size is: %s x %s (Width x Height)';
$_['ms_error_image_too_big'] = 'Image file dimensions are too big. Maximum allowed size is: %s x %s (Width x Height)';
$_['ms_error_file_upload_error'] = 'File upload error';
$_['ms_error_form_submit_error'] = 'Error occured when submitting the form. Please contact the store owner for more information.';
$_['ms_error_form_notice'] = 'Please check all form tabs for errors.';
$_['ms_error_product_name_empty'] = 'Product name cannot be empty';
$_['ms_error_product_name_length'] = 'Product name should be between %s and %s characters';
$_['ms_error_product_description_empty'] = 'Product description cannot be empty';
$_['ms_error_product_description_length'] = 'Product description should be between %s and %s characters';
$_['ms_error_product_tags_length'] = 'Line too long';
$_['ms_error_product_price_empty'] = 'Please specify a price for your product';
$_['ms_error_product_price_invalid'] = 'Invalid price';
$_['ms_error_product_price_low'] = 'Price too low';
$_['ms_error_product_price_high'] = 'Price too high';
$_['ms_error_product_category_empty'] = 'Please select a category';
$_['ms_error_product_model_empty'] = 'Product model cannot be empty';
$_['ms_error_product_model_length'] = 'Product model should be between %s and %s characters';
$_['ms_error_product_image_count'] = 'Please upload at least %s image(s) for your product';
$_['ms_error_product_download_count'] = 'Please submit at least %s download(s) for your product';
$_['ms_error_product_image_maximum'] = 'No more than %s image(s) allowed';
$_['ms_error_product_download_maximum'] = 'No more than %s download(s) allowed';
$_['ms_error_product_message_length'] = 'Message cannot be longer than 1000 characters';
$_['ms_error_product_attribute_required'] = 'This attribute is required';
$_['ms_error_product_attribute_long'] = 'This value can not be longer than %s symbols';
$_['ms_error_withdraw_amount'] = 'Invalid amount';
$_['ms_error_withdraw_balance'] = 'Not enough funds on your balance';
$_['ms_error_withdraw_minimum'] = 'Cannot withdraw less than minimum limit';
$_['ms_error_contact_email'] = 'Please specify a valid email address';
$_['ms_error_contact_captcha'] = 'Invalid captcha code';
$_['ms_error_contact_text'] = 'Message cannot be longer than 2000 characters';
$_['ms_error_contact_allfields'] = 'Please fill in all fields';
$_['ms_error_invalid_quantity_discount_priority'] = 'Error in priority field - please enter correct value';
$_['ms_error_invalid_quantity_discount_quantity'] = 'Quantity should be 2 or greater';
$_['ms_error_invalid_quantity_discount_price'] = 'Invalid quantity discount price entered';
$_['ms_error_invalid_quantity_discount_dates'] = 'Date fields for quantity discounts must be filled in';
$_['ms_error_invalid_special_price_priority'] = 'Error in priority field - please enter correct value';
$_['ms_error_invalid_special_price_price'] = 'Invalid special price entered';
$_['ms_error_invalid_special_price_dates'] = 'Date fields for special prices must be filled in';
$_['ms_error_seller_product'] = 'You can\'t add your own product to cart';

// Account - General
$_['ms_account_unread_pm'] = 'You have unread private message';
$_['ms_account_unread_pms'] = 'You have %s unread private messages';
$_['ms_account_register_new'] = 'New Seller';
$_['ms_account_register_seller'] = 'Register seller account';
$_['ms_account_register_seller_note'] = 'Create a seller account and start selling your products in our store!';
$_['ms_account_register_details'] = 'Detail';
$_['ms_account_register_profiles'] = 'Profile';
$_['ms_account_register_payment'] = 'Payment';
$_['ms_account_register_seller_success_heading'] = 'Your Seller Account Has Been Created!';
$_['ms_account_register_seller_success_message']  = '<p>Welcome to %s!</p> <p>Congratulations! Your new seller account has been successfully created!</p> <p>You can now take advantage of seller privileges and start selling your products with us.</p> <p>If you have any problems, <a href="%s">contact us</a>.</p>';
$_['ms_account_register_seller_success_approval'] = '<p>Welcome to %s!</p> <p>Your seller account has been registered and is waiting for approval. You will be notified by email once your account has been activated by the store owner.</p><p>If you have any problems, <a href="%s">contact us</a>.</p>';

$_['ms_seller'] = 'Seller';
$_['ms_seller_forseller'] = 'For seller';
$_['ms_account_dashboard'] = 'Dashboard';
$_['ms_account_seller_account'] = 'Seller Account';
$_['ms_account_customer_account'] = 'Customer Account';
$_['ms_account_sellerinfo'] = 'Seller profile';
$_['ms_account_sellerinfo_new'] = 'New seller account';
$_['ms_account_newproduct'] = 'New product';
$_['ms_account_products'] = 'Products';
$_['ms_account_transactions'] = 'Transactions';
$_['ms_account_orders'] = 'Orders';
$_['ms_account_withdraw'] = 'Request payout';
$_['ms_account_stats'] = 'Statistics';

// Account - New product
$_['ms_account_newproduct_heading'] = 'New Product';
$_['ms_account_newproduct_breadcrumbs'] = 'New Product';
//General Tab
$_['ms_account_product_tab_general'] = 'General';
$_['ms_account_product_tab_specials'] = 'Special prices';
$_['ms_account_product_tab_discounts'] = 'Quantity discounts';
$_['ms_account_product_name_description'] = 'Name & Description';
$_['ms_account_product_name'] = 'Name';
$_['ms_account_product_name_note'] = 'Specify a name for your product';
$_['ms_account_product_description'] = 'Description';
$_['ms_account_product_description_note'] = 'Describe your product';
$_['ms_account_product_meta_description'] = 'Meta Tag Description';
$_['ms_account_product_meta_description_note'] = 'Specify Meta Tag Description for your product';
$_['ms_account_product_meta_keyword'] = 'Meta Tag Keywords';
$_['ms_account_product_meta_keyword_note'] = 'Specify Meta Tag Keywords for your product';
$_['ms_account_product_tags'] = 'Tags';
$_['ms_account_product_tags_note'] = 'Specify tags for your product.';
$_['ms_account_product_price_attributes'] = 'Price & Attributes';
$_['ms_account_product_price'] = 'Price';
$_['ms_account_product_price_note'] = 'Choose a price for your product';
$_['ms_account_product_listing_flat'] = 'Listing fee for this product is <span>%s</span>';
$_['ms_account_product_listing_percent'] = 'Listing fee for this product is based on the product price. Current listing fee: <span>%s</span>.';
$_['ms_account_product_listing_balance'] = 'This amount will be deducted from your seller balance.';
$_['ms_account_product_listing_paypal'] = 'You will be redirected to the PayPal payment page after product submission.';
$_['ms_account_product_listing_itemname'] = 'Product listing fee at %s';
$_['ms_account_product_listing_until'] = 'This product will be listed until %s';
$_['ms_account_product_category'] = 'Category';
$_['ms_account_product_category_note'] = 'Select category for your product';
$_['ms_account_product_enable_shipping'] = 'Enable shipping';
$_['ms_account_product_enable_shipping_note'] = 'Specify whether your product requires shipping';
$_['ms_account_product_quantity'] = 'Quantity';
$_['ms_account_product_quantity_note']    = 'Specify the quantity of your product';
$_['ms_account_product_files'] = 'Files';
$_['ms_account_product_download'] = 'Downloads';
$_['ms_account_product_download_note'] = 'Upload files for your product. Allowed extensions: %s';
$_['ms_account_product_push'] = 'Push updates to previous customers';
$_['ms_account_product_push_note'] = 'Newly added and updated downloads will be made available to previous customers';
$_['ms_account_product_image'] = 'Images';
$_['ms_account_product_image_note'] = 'Select images for your product. First image will be used as a thumbnail. You can change the order of the images by dragging them. Allowed extensions: %s';
$_['ms_account_product_message_reviewer'] = 'Message to the reviewer';
$_['ms_account_product_message'] = 'Message';
$_['ms_account_product_message_note'] = 'Your message to the reviewer';
//Data Tab
$_['ms_account_product_tab_data'] = 'Data';
$_['ms_account_product_model'] = 'Model';
$_['ms_account_product_code'] = 'Product Code';
$_['ms_account_product_sku'] = 'SKU';
$_['ms_account_product_sku_note'] = 'Stock Keeping Unit';
$_['ms_account_product_upc']  = 'UPC';
$_['ms_account_product_upc_note'] = 'Universal Product Code';
$_['ms_account_product_ean'] = 'EAN';
$_['ms_account_product_ean_note'] = 'European Article Number';
$_['ms_account_product_jan'] = 'JAN';
$_['ms_account_product_jan_note'] = 'Japanese Article Number';
$_['ms_account_product_isbn'] = 'ISBN';
$_['ms_account_product_isbn_note'] = 'International Standard Book Number';
$_['ms_account_product_mpn'] = 'MPN';
$_['ms_account_product_mpn_note'] = 'Manufacturer Part Number';
$_['ms_account_product_manufacturer'] = 'Manufacturer';
$_['ms_account_product_manufacturer_note'] = '(Autocomplete)';
$_['ms_account_product_tax_class'] = 'Tax Class';
$_['ms_account_product_date_available'] = 'Date Available';
$_['ms_account_product_stock_status'] = 'Out Of Stock Status';
$_['ms_account_product_stock_status_note'] = 'Status shown when a product is out of stock';
$_['ms_account_product_subtract'] = 'Subtract Stock';

// Options
$_['ms_account_product_tab_options'] = 'Options';
$_['ms_options_add'] = '+ Add option';
$_['ms_options_add_value'] = '+ Add value';
$_['ms_options_required'] = 'Make option required';
$_['ms_options_price_prefix'] = 'Change price prefix';
$_['ms_options_price'] = 'Price...';
$_['ms_options_quantity'] = 'Quantity...';


$_['ms_account_product_manufacturer'] = 'Manufacturer';
$_['ms_account_product_manufacturer_note'] = '(Autocomplete)';
$_['ms_account_product_tax_class'] = 'Tax Class';
$_['ms_account_product_date_available'] = 'Date Available';
$_['ms_account_product_stock_status'] = 'Out Of Stock Status';
$_['ms_account_product_stock_status_note'] = 'Status shown when a product is out of stock';
$_['ms_account_product_subtract'] = 'Subtract Stock';

$_['ms_account_product_priority'] = 'Priority';
$_['ms_account_product_date_start'] = 'Start date';
$_['ms_account_product_date_end'] = 'End date';
$_['ms_account_product_sandbox'] = 'Warning: The payment gateway is in \'Sandbox Mode\'. Your account will not be charged.';



// Account - Edit product
$_['ms_account_editproduct_heading'] = 'Edit Product';
$_['ms_account_editproduct_breadcrumbs'] = 'Edit Product';

// Account - Clone product
$_['ms_account_cloneproduct_heading'] = 'Clone Product';
$_['ms_account_cloneproduct_breadcrumbs'] = 'Clone Product';

// Account - Relist product
$_['ms_account_relist_product_heading'] = 'Relist Product';
$_['ms_account_relist_product_breadcrumbs'] = 'Relist Product';

// Account - Seller
$_['ms_account_sellerinfo_heading'] = 'Seller Profile';
$_['ms_account_sellerinfo_breadcrumbs'] = 'Seller Profile';
$_['ms_account_sellerinfo_nickname'] = 'Nickname';
$_['ms_account_sellerinfo_nickname_note'] = 'Specify your seller nickname.';
$_['ms_account_sellerinfo_phone'] = 'Phone';
$_['ms_account_sellerinfo_phone_note'] = 'Your phone number.';
$_['ms_account_sellerinfo_description'] = 'Description';
$_['ms_account_sellerinfo_description_note'] = 'Describe yourself';
$_['ms_account_sellerinfo_account_address'] = 'Address';
$_['ms_account_sellerinfo_account_address_note'] = 'Your address';
$_['ms_account_sellerinfo_account_name'] = 'Account Name';
$_['ms_account_sellerinfo_account_name_note'] = 'Your Account Name';
$_['ms_account_sellerinfo_account_number'] = 'Account Number';
$_['ms_account_sellerinfo_account_number_note'] = 'Your Account Number';
$_['ms_account_sellerinfo_account_npwp_number'] = 'NPWP Number';
$_['ms_account_sellerinfo_account_npwp_number_note'] = 'Your NPWP Number';
$_['ms_account_sellerinfo_account_npwp_address'] = 'NPWP Address';
$_['ms_account_sellerinfo_account_npwp_address_note'] = 'Your NPWP Address';
$_['ms_account_sellerinfo_account_pod_transfer'] = 'Proof of Deposit Transfer';
$_['ms_account_sellerinfo_account_pod_transfer_note'] = 'Your Proof of Deposit Transfer';
$_['ms_account_sellerinfo_account'] = 'Bank Account';
$_['ms_account_sellerinfo_account_note'] = 'Your Bank Account';
$_['ms_account_sellerinfo_account_select'] = 'Select account';
$_['ms_account_sellerinfo_company'] = 'Company';
$_['ms_account_sellerinfo_company_note'] = 'Your company (optional)';
$_['ms_account_sellerinfo_postcode'] = 'Postcode';
$_['ms_account_sellerinfo_postcode_note'] = 'Your postcode';
$_['ms_account_sellerinfo_logistic'] = 'Logistic';
$_['ms_account_sellerinfo_logistic_note'] = 'Your logistic service';
$_['ms_account_sellerinfo_logistic_select_all'] = 'Select All';
$_['ms_account_sellerinfo_logistic_unselect_all'] = 'Unselect All';
$_['ms_account_sellerinfo_country'] = 'Country';
$_['ms_account_sellerinfo_country_dont_display'] = 'Do not display my country';
$_['ms_account_sellerinfo_country_note'] = 'Select your country.';
$_['ms_account_sellerinfo_zone'] = 'Region / State';
$_['ms_account_sellerinfo_zone_select'] = 'Select region/state';
$_['ms_account_sellerinfo_zone_not_selected'] = 'No region/state selected';
$_['ms_account_sellerinfo_zone_note'] = 'Select your region/state from the list.';
$_['ms_account_sellerinfo_city'] = 'City';
$_['ms_account_sellerinfo_city_select'] = 'Select your city';
$_['ms_account_sellerinfo_city_not_selected'] = 'No city selected';
$_['ms_account_sellerinfo_city_note'] = 'Select your city from the list.';
$_['ms_account_sellerinfo_district'] = 'District';
$_['ms_account_sellerinfo_district_select'] = 'Select district';
$_['ms_account_sellerinfo_district_not_selected'] = 'No district selected';
$_['ms_account_sellerinfo_district_note'] = 'Select your district from the list.';
$_['ms_account_sellerinfo_subdistrict'] = 'Sub District';
$_['ms_account_sellerinfo_subdistrict_select'] = 'Select sub district';
$_['ms_account_sellerinfo_subdistrict_not_selected'] = 'No sub district selected';
$_['ms_account_sellerinfo_subdistrict_note'] = 'Select your sub district from the list.';
$_['ms_account_sellerinfo_avatar'] = 'Avatar';
$_['ms_account_sellerinfo_avatar_note'] = 'Select your avatar';
$_['ms_account_sellerinfo_banner'] = 'Banner';
$_['ms_account_sellerinfo_banner_note'] = 'Upload a banner that will be displayed on your profile page';
$_['ms_account_sellerinfo_paypal'] = 'Paypal';
$_['ms_account_sellerinfo_paypal_note'] = 'Specify your PayPal address';
$_['ms_account_sellerinfo_reviewer_message'] = 'Message to the reviewer';
$_['ms_account_sellerinfo_reviewer_message_note'] = 'Your message to the reviewer';
$_['ms_account_sellerinfo_terms'] = 'Accept terms';
$_['ms_account_sellerinfo_terms_note'] = 'I have read and agree to the <a class="agree" href="%s" alt="%s"><b>%s</b></a>';
$_['ms_account_sellerinfo_fee_flat'] = 'There is a signup fee of <span>%s</span> to become a seller at %s.';
$_['ms_account_sellerinfo_fee_balance'] = 'This amount will be deducted from your initial balance.';
$_['ms_account_sellerinfo_fee_paypal'] = 'You will be redirected to the PayPal payment page after form submission.';
$_['ms_account_sellerinfo_signup_itemname'] = 'Seller account registration at %s';
$_['ms_account_sellerinfo_saved'] = 'Seller account data saved.';

$_['ms_account_status'] = 'Your seller account status is: ';
$_['ms_account_status_tobeapproved'] = '<br />You will be able to use your account as soon as it is approved by the store owner.';
$_['ms_account_status_please_fill_in'] = '<br />Please complete the following form to create a seller account.';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'Active';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'Inactive';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'Disabled';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'Incomplete';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'Deleted';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'Unpaid signup fee';

// Account - Products
$_['ms_account_products_heading'] = 'Your Products';
$_['ms_account_products_breadcrumbs'] = 'Your Products';
$_['ms_account_products_image'] = 'Image';
$_['ms_account_products_product'] = 'Product';
$_['ms_account_products_sales'] = 'Sales';
$_['ms_account_products_earnings'] = 'Earnings';
$_['ms_account_products_status'] = 'Status';
$_['ms_account_products_date'] = 'Date added';
$_['ms_account_products_listing_until'] = 'Listing until';
$_['ms_account_products_action'] = 'Action';
$_['ms_account_products_noproducts'] = 'You don\'t have any products yet!';
$_['ms_account_products_confirmdelete'] = 'Are you sure you want to delete your product?';

$_['ms_not_defined'] = 'Not defined';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'Active';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'Inactive';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'Disabled';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'Deleted';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'Unpaid listing fee';
$_['ms_product_status_' . MsProduct::STATUS_ON_REVIEW] = 'On Review';

// Account - Conversations and Messages
$_['ms_account_conversations'] = 'Conversations';
$_['ms_account_messages'] = 'Messages';

$_['ms_account_conversations_heading'] = 'Your Conversations';
$_['ms_account_conversations_breadcrumbs'] = 'Your Conversations';

$_['ms_account_conversations_status'] = 'Status';
$_['ms_account_conversations_date_created'] = 'Date created';
$_['ms_account_conversations_with'] = 'Conversation with';
$_['ms_account_conversations_title'] = 'Title';

$_['ms_conversation_title_product'] = 'Inquiry about product: %s';
$_['ms_conversation_title'] = 'Inquiry from %s';

$_['ms_account_conversations_read'] = 'Read';
$_['ms_account_conversations_unread'] = 'Unread';

$_['ms_account_messages_heading'] = 'Messages';
$_['ms_account_messages_breadcrumbs'] = 'Messages';

$_['ms_message_text'] = 'Your message';
$_['ms_post_message'] = 'Send message';

$_['ms_customer_does_not_exist'] = 'Customer account deleted';
$_['ms_error_empty_message'] = 'Message cannot be left empty';

$_['ms_mail_subject_private_message'] = 'New private message received';
$_['ms_mail_private_message'] = <<<EOT
You have received a new private message from %s!

%s

%s

You can reply in the messaging area in your account.
EOT;

$_['ms_mail_subject_order_updated'] = 'Your order #%s has been updated by %s';
$_['ms_mail_order_updated'] = <<<EOT
Your order at %s has been updated by %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s

EOT;

$_['ms_mail_subject_seller_vote'] = 'Vote for the seller';
$_['ms_mail_seller_vote_message'] = 'Vote for the seller';

// Account - Transactions
$_['ms_account_transactions_heading'] = 'Your Finances';
$_['ms_account_transactions_breadcrumbs'] = 'Your Finances';
$_['ms_account_transactions_balance'] = 'Your current balance';
$_['ms_account_transactions_earnings'] = 'Your earnings to date';
$_['ms_account_transactions_records'] = 'Balance records';
$_['ms_account_transactions_description'] = 'Description';
$_['ms_account_transactions_amount'] = 'Amount';
$_['ms_account_transactions_notransactions'] = 'You don\'t have any transactions yet!';

// Payments
$_['ms_payment_payments'] = 'Payments';
$_['ms_payment_order'] = 'order #%s';
$_['ms_payment_type_' . MsPayment::TYPE_SIGNUP] = 'Signup fee';
$_['ms_payment_type_' . MsPayment::TYPE_LISTING] = 'Listing fee';
$_['ms_payment_type_' . MsPayment::TYPE_PAYOUT] = 'Manual payout';
$_['ms_payment_type_' . MsPayment::TYPE_PAYOUT_REQUEST] = 'Payout request';
$_['ms_payment_type_' . MsPayment::TYPE_RECURRING] = 'Recurring payment';
$_['ms_payment_type_' . MsPayment::TYPE_SALE] = 'Sale';

$_['ms_payment_status_' . MsPayment::STATUS_UNPAID] = 'Unpaid';
$_['ms_payment_status_' . MsPayment::STATUS_PAID] = 'Paid';

// Account - Orders
$_['ms_account_orders_heading'] = 'Your Orders';
$_['ms_account_orders_breadcrumbs'] = 'Your Orders';
$_['ms_account_orders_id'] = 'Order #';
$_['ms_account_orders_customer'] = 'Customer';
$_['ms_account_orders_products'] = 'Products';
$_['ms_account_orders_history'] = 'History';
$_['ms_account_orders_addresses'] = 'Addresses';
$_['ms_account_orders_total'] = 'Total amount';
$_['ms_account_orders_view'] = 'View order';
$_['ms_account_orders_noorders'] = 'You don\'t have any orders yet!';
$_['ms_account_orders_nohistory'] = 'There is no history for this order yet!';
$_['ms_account_orders_change_status']    = 'Change order status';
$_['ms_account_orders_add_comment']    = 'Add order comment...';

$_['ms_account_order_information'] = 'Order Information';

// Account - Dashboard
$_['ms_account_dashboard_heading'] = 'Seller Dashboard';
$_['ms_account_dashboard_breadcrumbs'] = 'Seller Dashboard';
$_['ms_account_dashboard_orders'] = 'Last orders';
$_['ms_account_dashboard_overview'] = 'Overview';
$_['ms_account_dashboard_seller_group'] = 'Seller group';
$_['ms_account_dashboard_listing'] = 'Listing fee';
$_['ms_account_dashboard_sale'] = 'Sale fee';
$_['ms_account_dashboard_royalty'] = 'Royalty';
$_['ms_account_dashboard_stats'] = 'Stats';
$_['ms_account_dashboard_balance'] = 'Current balance';
$_['ms_account_dashboard_total_sales'] = 'Total sales';
$_['ms_account_dashboard_total_earnings'] = 'Total earnings';
$_['ms_account_dashboard_sales_month'] = 'Sales this month';
$_['ms_account_dashboard_earnings_month'] = 'Earnings this month';
$_['ms_account_dashboard_nav'] = 'Quick navigation';
$_['ms_account_dashboard_nav_profile'] = 'Modify your seller profile';
$_['ms_account_dashboard_nav_product'] = 'Create a new product';
$_['ms_account_dashboard_upload_product'] = 'Upload product';
$_['ms_account_dashboard_nav_products'] = 'Manage your products';
$_['ms_account_dashboard_nav_orders'] = 'View your orders';
$_['ms_account_dashboard_nav_balance'] = 'View your financial records';
$_['ms_account_dashboard_nav_payout'] = 'Request your payout';
//edit by arin
$_['ms_account_dashboard_nav_pending_order'] = 'View Pending Order';
$_['ms_account_dashboard_nav_return_list'] = 'View Return List';

$_['ms_account_dashboard_nav_staff'] = 'User Management';

// Account - Request withdrawal
$_['ms_account_withdraw_heading'] = 'Request Payout';
$_['ms_account_withdraw_breadcrumbs'] = 'Request Payout';
$_['ms_account_withdraw_deposit_fee'] = 'Your current deposit fee:';
$_['ms_account_withdraw_balance'] = 'Your current balance:';
$_['ms_account_withdraw_balance_available'] = 'Available for withdrawal:';
$_['ms_account_withdraw_minimum'] = 'Minimum payout amount:';
$_['ms_account_balance_reserved_formatted'] = '-%s pending withdrawal';
$_['ms_account_balance_waiting_formatted'] = '-%s waiting period';
$_['ms_account_withdraw_description'] = 'Payout request via %s (%s) on %s';
$_['ms_account_withdraw_amount'] = 'Amount:';
$_['ms_account_withdraw_amount_note'] = 'Please specify the payout amount';
$_['ms_account_withdraw_method'] = 'Payment method:';
$_['ms_account_withdraw_method_note'] = 'Please select the payout method';
$_['ms_account_withdraw_method_paypal'] = 'PayPal';
$_['ms_account_withdraw_all'] = 'All earnings currently available';
$_['ms_account_withdraw_minimum_not_reached'] = 'Your total balance is less than the minimum payout amount!';
$_['ms_account_withdraw_no_funds'] = 'No funds to withdraw.';
$_['ms_account_withdraw_no_paypal'] = 'Please <a href="index.php?route=seller/account-profile">specify your PayPal address</a> first!';

// Account - Stats
$_['ms_account_stats_heading'] = 'Statistics';
$_['ms_account_stats_breadcrumbs'] = 'Statistics';
$_['ms_account_stats_tab_summary'] = 'Summary';
$_['ms_account_stats_tab_by_product'] = 'By Product';
$_['ms_account_stats_tab_by_year'] = 'By Year';
$_['ms_account_stats_summary_comment'] = 'Below is a summary of your sales';
$_['ms_account_stats_sales_data'] = 'Sales data';
$_['ms_account_stats_number_of_orders'] = 'Number of orders';
$_['ms_account_stats_total_revenue'] = 'Total revenue';
$_['ms_account_stats_average_order'] = 'Average order';
$_['ms_account_stats_statistics'] = 'Statistics';
$_['ms_account_stats_grand_total'] = 'Grand total sales';
$_['ms_account_stats_product'] = 'Product';
$_['ms_account_stats_sold'] = 'Sold';
$_['ms_account_stats_total'] = 'Total';
$_['ms_account_stats_this_year'] = 'This Year';
$_['ms_account_stats_year_comment'] = '<span id="sales_num">%s</span> Sale(s) for specified period';
$_['ms_account_stats_show_orders'] = 'Show Orders From: ';
$_['ms_account_stats_month'] = 'Month';
$_['ms_account_stats_num_of_orders'] = 'Number of orders';
$_['ms_account_stats_total_r'] = 'Total revenue';
$_['ms_account_stats_average_order'] = 'Average order';
$_['ms_account_stats_today'] = 'Today, ';
$_['ms_account_stats_yesterday'] = 'Yesterday, ';
$_['ms_account_stats_daily_average'] = 'Daily average for ';
$_['ms_account_stats_date_month_format'] = 'F Y';
$_['ms_account_stats_projected_totals'] = 'Projected totals for ';
$_['ms_account_stats_grand_total_sales'] = 'Grand total sales';

// Product page - Seller information
$_['ms_catalog_product_sellerinfo'] = 'Seller information';
$_['ms_catalog_product_contact'] = 'Contact this seller';

$_['ms_footer'] = '<br>MultiMerch Marketplace by <a href="http://multimerch.com/">multimerch.com</a>';

// Catalog - Sellers list
$_['ms_catalog_sellers_heading'] = 'Sellers';
$_['ms_catalog_sellers_country'] = 'Country:';
$_['ms_catalog_sellers_website'] = 'Website:';
$_['ms_catalog_sellers_company'] = 'Company:';
$_['ms_catalog_sellers_totalsales'] = 'Sales:';
$_['ms_catalog_sellers_totalproducts'] = 'Products:';
$_['ms_sort_country_desc'] = 'Country (Z - A)';
$_['ms_sort_country_asc'] = 'Country (A - Z)';
$_['ms_sort_nickname_desc'] = 'Name (Z - A)';
$_['ms_sort_nickname_asc'] = 'Name (A - Z)';

// Catalog - Seller profile page
$_['ms_catalog_sellers'] = 'Sellers';
$_['ms_catalog_sellers_empty'] = 'There are no sellers yet.';
$_['ms_catalog_seller_profile'] = 'View profile';
$_['ms_catalog_seller_profile_heading'] = '%s\'s profile';
$_['ms_catalog_seller_profile_breadcrumbs'] = '%s\'s profile';
$_['ms_catalog_seller_profile_about_seller'] = 'About the seller';
$_['ms_catalog_seller_profile_products'] = 'Some of seller\'s products';
$_['ms_catalog_seller_profile_tab_products'] = 'Products';

$_['ms_catalog_seller_profile_social'] = 'Social profiles';
$_['ms_catalog_seller_profile_country'] = 'Country:';
$_['ms_catalog_seller_profile_zone'] = 'Region/State:';
$_['ms_catalog_seller_profile_website'] = 'Website:';
$_['ms_catalog_seller_profile_company'] = 'Company:';
$_['ms_catalog_seller_profile_totalsales'] = 'Total sales:';
$_['ms_catalog_seller_profile_totalproducts'] = 'Total products:';
$_['ms_rate_success'] = 'Transaction Success from';
$_['ms_transaction'] = 'Transaction';
$_['ms_note_rate_success'] = 'Transactions are successful when orders are completed. While failed transactions might be caused by: insufficient stock, shipiing charges difference, etc.';
$_['ms_catalog_seller_profile_view_products'] = 'View products';
$_['ms_catalog_seller_profile_view'] = 'View all %s\'s products';

// Catalog - Seller's products list
$_['ms_catalog_seller_products_heading'] = '%s\'s products';
$_['ms_catalog_seller_products_breadcrumbs'] = '%s\'s products';
$_['ms_catalog_seller_products_empty'] = 'This seller doesn\'t have any products yet!';

// Catalog - Seller contact dialog
$_['ms_sellercontact_title'] = 'Send a message to seller';
$_['ms_sellercontact_signin'] = 'Please <a href="%s">sign in</a> to contact %s';
$_['ms_sellercontact_sendto'] = 'Send a message to %s';
$_['ms_sellercontact_text'] = 'Message: ';
$_['ms_sellercontact_captcha'] = 'Captcha';
$_['ms_sellercontact_sendmessage'] = 'Send a message to %s';
$_['ms_sellercontact_close'] = 'Close';
$_['ms_sellercontact_send'] = 'Send';
$_['ms_sellercontact_success'] = 'Your message has been successfully sent';

/* Add by M*/
// Product Comments
$_['ms_account_productcomment_breadcrumbs'] = 'Comments';
$_['ms_account_productcomment_heading'] = 'Comments';
$_['ms_account_productcomment_error_comment_short'] = 'The comment body must be at least %s characters long';
$_['ms_account_productcomment_error_comment_long'] = 'The comment body cannot be longer than %s characters';
$_['ms_account_productcomment_text_comment']      = 'Comment';
$_['ms_account_productcomment_text_note']          = '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['ms_account_productcomment_button_continue']          = 'Submit';
$_['ms_account_productcomment_text_more']          = 'View More';
$_['ms_account_productcomment_text_post_comment']          = 'Post Comment';
$_['ms_account_productcomment_text_seller']          = 'Merchant';
$_['ms_account_productcomment_text_customer']          = 'Customer';
$_['pc_error_name'] = 'Please enter a name between %s and %s characters long';
$_['pc_error_email'] = 'Please enter a valid email';
$_['pc_error_comment_short'] = 'The comment body must be at least %s characters long';
$_['pc_error_comment_long'] = 'The comment body cannot be longer than %s characters';
$_['pc_error_captcha'] = 'Verification code does not match the image';
$_['pc_wait']         = 'Please Wait!';
$_['pc_success']      = 'Thank you for your comment.';
$_['pc_no_comments_yet'] = 'No comments added yet';


$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';

// edit by arin
// tab Pending order
$_['tab_new_order']           = 'New Order';
$_['tab_confirm_shipping']    = 'Confirm Shipping';
$_['tab_status_shipping']     = 'Shipping Status';
$_['tab_transaction_list']    = 'Transaction List';
$_['text_confirm_accept']     = 'Are you sure to accept all checked order ?';
$_['text_confirm_decline']    = 'Are you sure to decline order ?';
$_['text_decline_reason']     = 'Decline Reason';

// table shipping data
$_['text_buyer']    		  = 'Bought by ';
$_['text_buy_date']    		  = 'Transaction date: ';
$_['text_deadline_response']  = 'Response deadline: ';
$_['text_deadline_days']  	  = ' days remaining';
$_['text_response']    		  = 'Response';
$_['text_accept']    		  = 'Accept Order';
$_['text_decline']    		  = 'Decline Order';
$_['text_no_data']   		  = "<center><b>There's no new order to be confirmed</b></center>";
$_['text_no_data_pending']    = "<center><b>There's no new order</b></center>";
$_['text_no_data_global']    = "<center><b>There's no data</b></center>";
$_['text_detail']   		  = 'View Detail';
$_['text_delivery_code']   	  = 'Insert Receipt Number';
$_['text_detail_order']   	  = 'Order Detail';
$_['text_order_detail_id']    = 'Order Detail ID:';
$_['text_shipping_service']   = 'Shipping Service';
$_['text_days']   			  = ' day(s)';

//error warning
$_['error_response']   	  	  = 'Choose response first';
$_['error_receipt_number']    = 'Receipt number must be filled';
$_['error_shipping_service']    = 'Please choose shipping service';
$_['error_discuss']   		  = 'The comment body must be at least 3 characters long';

//button
$_['button_response']    	  = 'Response Order';
$_['button_print']    	 	  = 'Print';
$_['button_hide']    	 	  = 'Hide';
$_['button_show']    	 	  = 'Show';
$_['button_show_all']    	  = 'Show All';
$_['button_hide_all']    	  = 'Hide All';
$_['button_accept_all']    	  = 'Accept all orders';
$_['button_confirm_all']      = 'Confirm all';
$_['button_confirm']    	  = 'Confirm';
$_['button_cancel']    	  	  = 'Cancel';
$_['button_save']    	  	  = 'Save';

$_['column_destination']      = 'Destination Address';
$_['column_quantity']         = 'Total Item';
$_['column_shipping_price']   = 'Shipping Price';
$_['column_insurance']    	  = 'Insurance Cost';
$_['column_remarks']    	  = 'Remarks';
$_['column_price']    		  = 'Price';
$_['column_payment_method']   = '<b>Method payment: </b>';
$_['column_total']    		  = 'Total Price Per Receipt Number: ';

//success response
$_['text_success_response']   = 'You have successfully updated response';
$_['text_success_confirm']    = 'You have successfully confirmed an order';
$_['text_success_edit']    	  = 'You have successfully changed receipt number';

//table confirm order
$_['column_buyer']    	  	  = 'Customer';
$_['column_shipping_info']    = 'Shipping Information';
$_['column_delivery']    	  = 'Delivery Agent';
$_['column_action']    	  	  = 'Action';

//order release
$_['text_search_invoice']     = 'Receiver Name / Receipt Number';
$_['text_search_all']     	  = 'Receipt Number / Buyer Name / Receipt Number';
$_['text_choose']    	  	  = 'Choose';
$_['text_deadline']    	  	  = 'Deadline: ';
$_['text_yes']    	  	  	  = 'Yes ';
$_['text_no']    	  	  	  = 'No';
$_['text_change_delivery']    = 'Do you want to change delivery agent?';
$_['text_receipt_number']     = 'Receipt Number';
$_['text_tooltip']     		  = 'Total price which was paid to your store';

//shipping status
$_['text_edit_receipt']       = 'Edit Receipt Number';
$_['text_track']     		  = 'Track';

//return list
$_['text_return_list'] 			= 'Return List';
$_['text_detail_return'] 		= 'Detail Return';
$_['text_info_return'] 			= 'Return Information';
$_['text_email'] 				= 'Email: ';
$_['text_email2'] 				= 'Email';
$_['text_product_info'] 		= 'Product Information';
$_['text_search'] 				= 'Search';
$_['text_accept_return']    	= 'Accept Return';
$_['text_complete_return']    	= 'Return Complete';
$_['success_accept_return']    	= 'You have sucessfully accepted return application';
$_['success_change_action']    	= 'You have sucessfully changed expected action';
$_['warning_user_confirm']    	= "Customer hasn't confirmed action change";
$_['success_user_confirm']    	= "Customer has confirmed action change";
$_['text_discuss']    			= 'Return Application Discussion';
$_['text_change_action']    	= 'Change Action';
$_['text_sla_return']    		= ' day(s) remaining to accept this return application';

// Column
$_['column_return_id']   = 'Return ID';
$_['column_order_id']    = 'Order ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Date Added';
$_['column_customer']    = 'Customer';
$_['column_product']     = 'Product Name';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Quantity';
$_['column_price']       = 'Price';
$_['column_opened']      = 'Opened';
$_['column_comment']     = 'Comment';
$_['column_reason']      = 'Reason';
$_['column_action']      = 'Expected Action';
$_['column_invoice']	 = 'Receipt Number';

$_['text_return_id']     = 'Return ID:';
$_['text_order_id']      = 'Order ID:';
$_['text_date_ordered']  = 'Order Date:';
$_['text_date_added']    = 'Return Date:';
$_['text_history']       = 'Return History';
$_['text_no_comment']    = "There's no comment yet";
$_['text_view_more']     = 'View more';
$_['entry_attribute']    = 'Attribute';
$_['entry_text']     	 = 'Description';

//report
$_['text_report']     	 = 'Report';
$_['text_date_start']    = 'Date Start';
$_['text_date_end']    	 = 'Date End';

//list payments
$_['text_pay_type']    	 	= 'Payment Type';
$_['text_pay_status']    	= 'Payment Status';
$_['text_date_paid']     	= 'Paid Date';
$_['text_date_created']  	= 'Created Date';
$_['text_requester']  	 	= 'Requester';
$_['text_total_price']   	= 'Total Price';
$_['text_subtotal']   	 	= 'Subtotal';
$_['text_total']   	 	= 'Total';
$_['text_date_modified'] 	= 'Modified Date';
$_['text_number'] 		 	= 'No.';
$_['text_transaction_date'] = 'Transaction Date';
$_['text_transaction_code'] = 'Transaction Code';
$_['text_balance'] 			= 'Balance';

$_['text_batch'] 	= 'Batch';
$_['text_batch_select'] 	= 'Select Batch';


// Account - Staff
$_['ms_account_staff_heading'] = 'User Management';
$_['ms_account_staff_breadcrumbs'] = 'User Management';
$_['ms_account_newstaff_breadcrumbs'] = 'Create User';
$_['ms_account_updatestaff_breadcrumbs'] = 'Update User';
$_['ms_create_user'] = 'Create User';
$_['ms_account_staff_name'] = 'Name';
$_['ms_account_staff_email'] = 'Email';
$_['ms_account_staff_status'] = 'Status';
$_['ms_account_staff_date'] = 'Date Added';
$_['ms_account_staff_action'] = 'Action';

$_['ms_account_staff_status_' . MsStaff::STATUS_ENABLED] = 'Enabled';
$_['ms_account_staff_status_' . MsStaff::STATUS_DISABLED] = 'Disabled';

$_['ms_success_staff_enabled'] = 'User enabled';
$_['ms_success_staff_disabled'] = 'User disabled';
$_['ms_success_staff_created'] = 'User created';
$_['ms_success_staff_updated'] = 'User updated';
$_['ms_success_staff_deleted'] = 'User deleted';

$_['ms_account_newstaff_heading'] = 'Create User';
$_['ms_account_updatestaff_heading'] = 'Update User';


$_['ms_account_staff_role'] = 'Roles';
$_['ms_account_staff_roles_unselect_all'] = 'Unselect All';
$_['ms_account_staff_roles_select_all'] = 'Select All';
$_['ms_account_staff_roles_note'] = 'User Roles';

$_['ms_premium'] = 'Premium Seller';
$_['ms_link_request_premium'] = 'Get your premium access now. <a href="%s">Request now</a>';
$_['ms_link_premium'] = 'You have request premium access, your request in progress';

$_['ms_account_noroles_heading'] = 'You can not access this page !';
$_['ms_account_noroles'] = 'Please contact your administrator';

$_['ms_account_withdraw_method_transfer'] = 'Bank Transfer';

$_['ms_account_dashboard_nav_staff'] = 'User Management';

//Report
$_['ms_rep_payment'] = 'Payment Transactions Report';
$_['ms_rep_trans'] = 'Transactions Report';
$_['ms_rep_balance'] = 'Balance Report';
$_['ms_payment_trans'] = 'Payment Transactions';
$_['ms_download_date'] = 'Download Date';
$_['ms_number'] = 'No';
$_['ms_total_amount'] = 'Total Amount';
$_['ms_invoice'] = 'Receipt Number';
$_['ms_customer_name'] = 'Customer Name';
$_['ms_product_sku'] = 'Product SKU';
$_['ms_product_price'] = 'Product Price';
$_['ms_product_total'] = 'Product Total';
$_['ms_warning_date_picker'] = 'Please choose date start and date end';
$_['ms_warning_date_format'] = 'Date should be in YYYY-MM-DD format';


$_['ms_error_sellerinfo_logistic_empty'] = 'Please select logistics';
$_['ms_error_sellerinfo_country_empty'] = 'Please select a country';
$_['ms_error_sellerinfo_city_empty'] = 'Please select a city';
$_['ms_error_sellerinfo_zone_empty'] = 'Please select a zone';
$_['ms_error_sellerinfo_district_empty'] = 'Please select a district';
$_['ms_error_sellerinfo_subdistrict_empty'] = 'Please select a sub district';
$_['ms_error_sellerinfo_postcode_empty'] = 'Postcode cannot be empty';
$_['ms_error_sellerinfo_account_empty'] = 'Please select a account';
$_['ms_error_sellerinfo_account_name_empty'] ='Account Name cannot be empty';
$_['ms_error_sellerinfo_account_number_empty'] = 'Account Number cannot be empty';
$_['ms_error_sellerinfo_npwp_number_empty'] = 'NPWP Number cannot be empty';
$_['ms_error_sellerinfo_npwp_address_empty'] = 'NPWP Address cannot be empty';
$_['ms_error_sellerinfo_address_length'] = 'Address should be longer than 20 characters';

$_['ms_error_product_sku_empty'] = 'SKU cannot be empty';
$_['ms_error_product_sku_taken'] = 'SKU has been taken';
$_['ms_error_product_sku_length'] = 'SKU should be between %s and %s characters';


$_['ms_error_product_code_empty'] = 'Code cannot be empty';
$_['ms_error_length_empty'] = 'Product length cannot be empty';
$_['ms_error_length_invalid'] = 'Invalid product length';
$_['ms_error_width_empty'] = 'Product width cannot be empty';
$_['ms_error_width_invalid'] = 'Invalid product width';
$_['ms_error_height_empty'] = 'Product height cannot be empty';
$_['ms_error_height_invalid'] = 'Invalid product height';
$_['ms_error_weight_empty'] = 'Product weight cannot be empty';
$_['ms_error_weight_invalid'] = 'Invalid product weight';
$_['ms_error_product_date_available_invalid'] = 'Invalid date available';
$_['ms_error_product_price_invalid'] = 'Invalid price';
$_['ms_error_product_quantity_empty'] = 'Please specify a quantity for your product';
$_['ms_error_product_quantity_invalid'] = 'Invalid quantity';
$_['ms_error_product_manufacturer_empty'] = 'Please specify a manufacturer for your product';
$_['ms_error_product_manufacturer_invalid'] = 'Invalid manufacturer';
$_['ms_error_product_tax_class_id_empty'] = 'Please specify a tax class for your product';
$_['ms_error_product_tax_class_id_invalid'] = 'Invalid tax class';


$_['ms_account_dashboard_nav_password'] = 'Change Password';

//print
$_['text_telephone'] 		= 'Telephone:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_delivery_type'] 	= 'Delivery Type:';
$_['text_unit_price'] 		= 'Unit Price';
$_['text_invoice_num']	 	= 'Receipt Number: ';
$_['text_website']	 		= 'Website: ';
$_['text_print']          	= 'Print ';
$_['text_seller_invoice']   = 'SELLER';
$_['text_invoice_info']  	= 'RECEIPT NUMBER INFORMATION';
$_['text_delivery_info']  	= 'DELIVERY INFORMATION';
$_['text_order_info']  		= 'ORDER INFORMATION';
$_['text_voucher_info']  	= 'RECEIVER & VOUCHER INFORMATION';
$_['text_note_invoice']  	= '*)';
$_['text_note_desc']  		= 'Harga di atas sudah termasuk PPN';
$_['column_npwp']			= 'Seller NPWP';
$_['column_date_order']     = 'Date Ordered';
$_['column_shipping']       = 'Shipping Price';
$_['column_receiver']       = 'Sent to';
$_['column_telp']       	= 'Telephone';
$_['column_product_list']   = 'Product List';
$_['column_sku']       		= 'SKU';

$_['ms_transaction_shipping']      = 'Shipping fee';

//account-staff
$_['entry_firstname']		= 'First name';
$_['entry_lastname']		= 'Last name';
$_['entry_email']			= 'Email';
$_['entry_telephone']		= 'Telephone';
$_['entry_dob']				= 'Date of Birth';

$_['error_firstname']		= 'Firstname can not be empty';
$_['error_lastname']		= 'Lastname can not be empty';
$_['error_email']			= 'Email is not valid';
$_['error_telephone']		= 'Telephone can not be empty';
$_['error_dob']				= 'Tanggal lahir tidak valid';

//account-upload-product
// Text
$_['heading_title']        = 'Upload Product';
$_['text_category']        = 'Product Category';
$_['text_seller']          = 'Seller';
$_['text_choose']          = 'Please choose';
$_['text_success_upload']  = 'Your file was successfully uploaded!';
$_['text_success_save']    = "Product saved";

// Entry
$_['entry_upload']         = 'Upload File';
$_['entry_choose_file']    = 'Choose file';
$_['entry_overwrite']      = 'Files that will be overwritten';
$_['entry_progress']       = 'Progress';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify extensions!';
$_['error_temporary']   = 'Warning: There are some temporary files that require deleting. Click the clear button to remove them!';
$_['error_upload']		= 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_category']    = 'Please choose product category';
$_['error_seller']      = 'Please choose seller group';
$_['error_sku']      	= 'Duplicate SKU at row ';
$_['error_agi']      	= 'Can not found attribute group id from the choosen product category';
$_['error_attr_name']   = 'Can not found attribute name at column ';
$_['error_manufacturer']= 'Can not found manufacturer at row ';
$_['error_format']		= 'Invalid template format';
$_['error_numeric']		= 'Invalid input (numeric only) at row ';
$_['error_date']		= 'Invalid date format (use YYYY-MM-DD format) at row ';
$_['error_blank']		= 'You have empty value at row ';
$_['error_image']		= 'File extension of image must be in .jpg, .jpeg, or .png at row ';


//Help
$_['help_upload']     	   = 'Upload .xls or .xlsx file';
$_['help_product']     	   = 'Choose one of product categories';
$_['help_seller']     	   = 'Choose one of the sellers';

//request product
$_['ms_account_request_product']	= 'Product Request';
$_['ms_list_request_product']		= 'List Product Request';
$_['text_kiosk']					= 'Kiosk / Pickup Point';
$_['button_create_request']			= 'Create Request';
$_['text_create_request']			= 'Create Product Request';
$_['text_view_request']				= 'View Product Request';
$_['text_edit_request']				= 'Edit Delivery Order';

$_['column_request']				= 'Request ID';
$_['column_kiosk']					= 'Kiosk';
$_['column_delivery_order']			= 'Delivery Order';

$_['error_delivery_order']			= 'Delivery order required';
$_['error_product_request']			= 'Please complete all of fields';
$_['error_kiosk_id']				= 'Please choose kiosk/pickup point';
$_['error_product_name']			= 'Product name required';
$_['error_product_price']			= 'Product price required';
$_['error_product_quantity']		= 'Product quantity required';
$_['error_minimum']					= 'Quantity must be greater than minimum quantity of product category';

$_['text_success_edit_request']    	= 'You have successfully changed delivery order';
$_['text_success_add_request']    	= 'You have successfully made product request';

//register seller
$_['text_tax_type']    				= 'Tax Type';
$_['text_business_type']    		= 'Business Type';
$_['text_tax_type_pkp']    			= 'PKP';
$_['text_tax_type_non_pkp']    		= 'NON-PKP';
$_['text_tax_type_non_pkp_val']    	= 'NON_PKP';
$_['text_business_type_1']    		= 'INDIVIDUAL';
$_['text_business_type_2']    		= 'BUSINESS';

$_['text_attachment']    			= 'Attachment';
$_['text_npwp']    					= 'NPWP';
$_['text_ktp']    					= 'KTP';
$_['text_book_account']    			= 'Buku Rekening';
$_['text_siup']    					= 'SIUP';
$_['text_tdp']    					= 'TDP';
$_['text_apbu']    					= 'Akta Pendirian Badan Usaha';
$_['text_ktp_direksi']    			= 'KTP Direksi';
$_['text_domisili']    				= 'Surat Keterangan Domisili';

$_['note_npwp']    					= 'Unggah scan NPWP';
$_['note_ktp']    					= 'Unggah scan KTP';
$_['note_book_account']    			= 'Unggah scan buku rekening';
$_['note_siup']    					= 'Unggah scan SIUP';
$_['note_tdp']    					= 'Unggah scan TDP';
$_['note_apbu']    					= 'Unggah scan Akta Pendirian Badan Usaha';
$_['note_ktp_direksi']    			= 'Unggah scan KTP Direksi';
$_['note_domisili']    				= 'Unggah scan Surat Keterangan Domisili';

$_['error_upload_required']    		= 'Upload Required';

//report view detail
$_['column_order_no']			= 'Receipt No.';
$_['column_vendor_name']		= 'Seller Name';
$_['column_vendor_type']		= 'Seller Type';
$_['column_kiosk_id']			= '#ID Kiosk';
$_['column_payment_type']		= 'Payment Type';
$_['column_transaction_date']	= 'Transaction Date';
$_['column_quantity']			= 'Qty';
$_['column_sku']				= 'SKU';
$_['column_price']				= 'Price';
$_['column_total_report']		= 'Total';
$_['column_tax']				= 'Tax (PPN)';
$_['column_commission']			= 'Commission Base';
$_['column_shipping']			= 'Shipping Fee';
$_['column_online_payment']		= 'Online Payment Fee';
$_['column_ofiskita_commission']= 'Commission';
$_['column_tax_ppn']			= 'Tax (PPN for Commission)';
$_['column_vendor_balance']		= 'Balance';
$_['column_batch']		= 'Batch';
$_['error_session_timeout']		= 'Please login first! <a href="%s">Login</a>';
$_['column_total_report']		= 'Total';
$_['text_to']		= 'To';
$_['column_payment_type']		= 'Payment Type';
$_['column_revenue_type']		= 'Revenue Type';
$_['column_business_type']		= 'Business Type';
$_['column_tax_type']			= 'Tax Type';
$_['column_relation_type']		= 'Relation Type';
$_['column_payment_total']		= 'Payment Total';
$_['column_bank']				= 'Bank';

$_['text_method_transfer'] = 'Bank Transfer';
$_['text_description_withdrawal'] = 'Payout request via %s (%s) on %s';

$_['ms_reject_order_description'] = 'Reject Order %s';
?>
