<?php
// Heading
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_password']  = 'New Password';
$_['entry_confirm']   = 'Confirm New Password';
$_['entry_old_pass']  = 'Old Password';

// Error
$_['error_password'] 	= 'Password must be between 4 and 20 characters!';
$_['error_confirm']  	= 'Password confirmation does not match password!';
$_['error_code']  		= 'Invalid code!';
$_['error_old_match']  	= 'The old password does not match';
$_['error_same_pass']  	= 'Please entry a different password';

$_['button_submit']  = 'Submit';