<?php
// Heading
$_['heading_title']        = 'Your order has been placed!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
//$_['text_customer']        = '<p>Your order has been successfully processed!</p><p>You can view your order history by going to the <a href="%s">my account</a> page and by clicking on <a href="%s">history</a>.</p><p>If your purchase has an associated download, you can go to the account <a href="%s">downloads</a> page to view them.</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';
$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';

//edit by arin
$_['text_customer'] = '<center><p>Thank you, you have successfully checkout your order by choosing %s payment method.</p></center>';
$_['text_total_price'] = 'Total price';
$_['text_total_price_paid'] = 'Total price';
// $_['text_customer_transfer']='<center><p>You will be charged the transfer code %s</p></center>';
$_['text_customer_transfer']='<center><p>Please input notes about your transfer using this Order ID: %s</p></center>';
$_['button_confirm_payment'] = 'Confirm Payment';

?>