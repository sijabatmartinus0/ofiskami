<?php
// Heading
$_['heading_title'] = 'Use Coupon Code';

// Text
$_['text_success']  = 'Success: Your coupon discount has been applied!';
$_['text_unset']  = 'Success: Your coupon has been unset!';
// Entry
$_['entry_coupon']  = 'Enter your voucher here';

// Error
$_['error_coupon']	  	= 'Warning: Coupon reached its usage limit!';
$_['error_empty']	   	= 'Warning: Please enter a coupon code!';
$_['error_cart']	   	= 'Warning: Invalid coupon, empty shopping cart!';
$_['error_coupon']	   	= 'Warning: Invalid coupon code!';
$_['error_product']	   	= 'Warning: Invalid coupon for this product!';
$_['error_customer']   	= 'Warning: Invalid coupon for this email!';
$_['error_newsletter'] 	= 'Warning: Invalid coupon for this email!';
$_['error_expired']		= 'Warning: Coupon expired or reached its usage limit!';
$_['error_minimum']   	= 'Warning: Invalid coupon, minimum purchase not achieved!';
