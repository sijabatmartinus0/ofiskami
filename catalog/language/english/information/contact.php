<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_category']  = 'Category';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your message has been successfully sent to ofiskita.com admin!</p>';
$_['text_choose_category']  = '-- Choose Category --';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Message';
$_['entry_captcha']  = 'Enter the code in the box below';

// Email
$_['email_subject']  = 'Message %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Message must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
$_['error_telephone']  = 'Please enter a valid telephone number!';
$_['error_category']  = 'Please choose a category!';


// Category
$_['category_1']	= 'Register & Login';
$_['category_2']	= 'Account';
$_['category_3']	= 'Order / Delivery';
$_['category_4']	= 'Complain';
$_['category_5']	= 'Payment';
$_['category_6']	= 'Reputation System / Review';
$_['category_7']	= 'Return & Withdrawal';
$_['category_8']	= 'Store Setting';
$_['category_9']	= 'Promotion Step';
$_['category_10']	= 'Promotion & Ofiskita Feature';
$_['category_11']	= 'About Ofiskita';
