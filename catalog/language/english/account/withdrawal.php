<?php
$_['heading_title'] = 'Request Withdrawal';
$_['text_account'] = 'Account';
$_['text_heading'] = 'Request Withdrawal';
$_['text_breadcrumbs'] = 'Request Withdrawal';
$_['text_balance'] = 'Your current balance:';
$_['text_balance_available'] = 'Available for withdrawal:';
$_['text_minimum'] = 'Minimum payout amount:';
$_['text_balance_reserved_formatted'] = '-%s pending withdrawal';
$_['text_balance_waiting_formatted'] = '-%s waiting period';
$_['text_description'] = 'Payout request via %s (%s) on %s';
$_['text_amount'] = 'Amount:';
$_['text_amount_note'] = 'Please specify the payout amount';
$_['text_method'] = 'Payment method:';
$_['text_method_note'] = 'Please select the payout method';
$_['text_method_paypal'] = 'PayPal';
$_['text_all'] = 'All earnings currently available';
$_['text_minimum_not_reached'] = 'Your total balance is less than the minimum payout amount!';
$_['text_empty_account'] = 'Please specify your account data. <a href="%s">Edit</a>';
$_['text_no_funds'] = 'No funds to withdraw.';
$_['text_no_paypal'] = 'Please <a href="index.php?route=seller/account-profile">specify your PayPal address</a> first!';
$_['text_method_transfer'] = 'Bank Transfer';
$_['text_success'] = 'Success request withdrawal';

$_['ms_error_withdraw_amount'] = 'Invalid amount';
$_['ms_error_withdraw_minimum'] = 'Cannot withdraw less than minimum limit';
$_['ms_error_withdraw_balance'] = 'Not enough funds on your balance';