<?php
// Heading
$_['heading_title']      = 'Review';

// Text
$_['text_account']       = 'Account';
$_['text_review']       = 'New Review';
$_['text_seller']       = 'Seller';
$_['text_customer']       = 'Customer';
$_['text_no_review']       = 'No Data';
$_['text_display']       = 'Display :';
$_['text_all']       = 'All';
$_['text_unread']       = 'Unread';
$_['text_search']       = 'Invoice Number/Product Name';


$_['entry_review']      = 'Review:';
$_['entry_rating']      = 'Rating';
$_['entry_good']        = 'Good';
$_['entry_bad']         = 'Bad';
$_['entry_accuracy']  = 'Delivery accuracy';

$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_rating_accuracy']    = 'Warning: Please select an accuracy rating!';
$_['error_review']      = 'Warning: You cannot review this product';
$_['text_success']             = 'Thank you for your review.';

$_['button_hide']		= "Hide";
$_['button_show']		= "Show";

