<?php
// Heading
$_['heading_title']      = 'My Account Information';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'Edit Information';
$_['text_your_details']  = 'Your Personal Details';
$_['text_success']       = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telephone';
$_['entry_fax']          = 'Fax';
$_['entry_dob']          = 'Date of Birth';

// Error
$_['error_exists']       = 'Warning: E-Mail address is already registered!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 3 and 32 characters!';
$_['error_custom_field'] = '%s required!';
$_['error_dob']      = 'Wrong Date of Birth format(ex:2015-01-01)';
$_['error_dob_value']      = 'Min Date of Birth '.date("Y-m-d",strtotime('-15 years'));


$_['text_payment_account']  = 'Payment';
$_['entry_account']  = 'Bank Account';
$_['entry_account_name']  = 'Account Name';
$_['entry_account_number']  = 'Account Number';

$_['error_account']    = 'Please select an account';
$_['error_account_name']    = 'Account Name must be between 3 and 32 characters!';
$_['error_account_number']    = 'Account Number cannot be empty';
$_['error_account_number_invalid']    = 'Invalid Account Number';


