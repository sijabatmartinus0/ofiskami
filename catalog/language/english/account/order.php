<?php
// Heading
$_['heading_title']         = 'Order History';
$_['heading_payment']       = 'Payment Confirmation';
$_['heading_success']       = 'Success Confirmation';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Receipt No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success_receive']          = 'Your order has been received';
$_['text_success_complete']          = 'Your order has been completed';
$_['text_success']          = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';
$_['text_receipt']          = ' with a receipt number ';
$_['text_complete_return']  = 'Please wait until the current process of return complete';
$_['text_receiver_name']  	= 'Name:';
$_['text_receiver_email']  	= 'Email:';
$_['text_voucher_data']  	= 'Voucher Data';
$_['text_voucher']  		= 'voucher(s)';

//Waybill
$_['text_modal_waybill']  		= 'Shipment Tracking';
$_['text_waybill_number']  		= 'Waybill';
$_['text_waybill_courier']  		= 'Courier';
$_['text_waybill_service']  		= 'Service';
$_['text_waybill_date']  		= 'Date';
$_['text_waybill_origin']  		= 'Origin';
$_['text_waybill_destination']  		= 'Destination';
$_['text_waybill_shipper']  		= 'Shipper';
$_['text_waybill_receiver']  		= 'Receiver';
$_['text_waybill_not_found']  		= 'Invalid waybill. Waybill that you entered is incorrect or not registered.';


// Column
$_['column_order_id']       = 'Order ID';
$_['column_product']        = 'No. of Products';
$_['column_customer']       = 'Receiver';
$_['column_receiver']       = 'Sent to';
$_['column_telp']       	= 'Telephone';
$_['column_buyer']       	= 'Buyer';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_sku']       		= 'SKU';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date';
$_['column_date_order']     = 'Date Ordered';
$_['column_status']         = 'Order Status';
$_['column_comment']        = 'Comment';
$_['column_shipping']       = 'Shipping Price';
$_['column_product_list']   = 'Product List';
$_['column_buyer']  		= 'Buyer';
$_['column_invoice']  		= 'Receipt';
$_['column_insurance']  	= 'Insurance Cost';
$_['column_remarks']  		= 'Remarks';
$_['column_code']  			= 'Voucher Code';
$_['column_message']  		= 'Message';
$_['column_description']  	= 'Description';
$_['column_voucher']  		= 'Voucher Detail';
$_['column_payment_method'] = 'Payment Method';
$_['column_shipping_method']= 'Shipping Method';

$_['column_name_voucher']	= 'Receiver Name';
$_['column_email_voucher']	= 'Receiver Email';
$_['column_voucher_amount']	= 'Voucher Amount';

// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';

// Button
$_['button_confirm']		= 'Confirm Payment';
$_['button_confirm_done']	= 'Confirmed';
$_['button_receive']		= 'Received Order';
$_['button_complete']		= 'Complete Order';

$_['text_seller']          	= 'Seller:';
$_['text_seller_invoice']   = 'SELLER';
$_['text_print']          	= 'Print ';

$_['text_invoice_info']  	= 'RECEIPT INFORMATION';
$_['text_delivery_info']  	= 'DELIVERY INFORMATION';
$_['text_order_info']  		= 'ORDER INFORMATION';
$_['text_voucher_info']  	= 'RECEIVER & VOUCHER INFORMATION';
$_['text_note_invoice']  	= '*)';
$_['text_note_desc']  		= 'Harga di atas sudah termasuk PPN';

$_['text_total']  		= 'Total';
$_['text_subtotal']  		= 'Sub Total';
?>