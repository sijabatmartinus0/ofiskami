<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';
$_['heading_title_error_code'] = 'Your Link Verification Invalid!';


// Text
$_['text_message']  = '<p style="text-align:center;">Congratulations! Your new account has been successfully created!</p> <p style="text-align:center;"> Your Account : <strong>%s</strong></p> <p style="text-align:center;">You can now take advantage of member privileges to enhance your online shopping experience with us.</p> <p style="text-align:center;">If you have ANY questions about the operation of this online shop, please e-mail the Ofiskita owner.</p> <p style="text-align:center;">A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_message_email']  = '<p>Segera verifikasi akun Anda, klik link berikut jika anda belum menerima email verifikasi <a href="%s">kirim email</a>.</p>';
$_['text_message_phone']  = '<p>Lengkapi keamanan akun Anda dengan melakukan verifikasi nomor HP. <a href="%s">Verifikasi Sekarang</a>.</p>';
// $_['text_message_complete']  = '<p>Selamat! Akun baru Anda sukses dibuat!</p> <p>Anda dapat memulai untuk berbelanja dengan aman di Ofiskita.com <a href="%s">sekarang</a>.</p>';
$_['text_message_complete']  = '<p>Selamat! Akun baru Anda sukses dibuat!</p> <p>Anda dapat memulai untuk berbelanja dengan aman di Ofiskita.com</p>';

$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>You will be notified by e-mail once your account has been activated by the store owner.</p><p>If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account']  = 'Account';
$_['text_progress']  = 'Registration Progress';
$_['text_activate']  = 'Activate';
$_['text_email']  = 'Email Verification has been sent';
$_['text_success_email_verification']  = 'Your email has been verified';
$_['text_error_email_verification']  = 'Invalid verification code, please resend verification mail';
$_['text_message_error_code']  = 'Please resend verification mail';

$_['text_message_verification']  = '%s is your Ofiskita account confirmation code';
$_['text_success_send_verification_code']  = 'Your verification code has been send';
$_['error_empty_registered_number'] 				= "Please enter registered number";

$_['button_continue']  = 'Continue';
$_['button_send']  = 'Send';
$_['button_shop']  = 'Start Shopping';

//edit by arin
$_['text_verification_phone']  	= 'Phone Verification';
$_['text_registered_number']   	= 'Registered Number';
$_['text_ver_code']   			= 'Verification Code';
$_['text_note1']   				= 'Please make sure to enter the right number, we will send verification code to the number.<br/>* SMS verification can be done through GSM network only';
$_['text_note2']   				= "Haven't received verification code yet?";
$_['text_note3']   				= 'User has to provide an active phone number and renew it periodically.';
$_['text_retry_send']   		= 'Resend';

//btn
$_['button_send_verifivation'] 	= 'Send Verification Code';
$_['button_submit'] 			= 'Submit';
$_['button_skip'] 				= 'Skip';

//error
$_['error_is_verified'] 		= "Verification code doesn't match";
$_['error_empty'] 				= "Please enter verification code";

$_['text_edit_account']   		= '* This is wrong phone number. <a href="%s">Edit Account</a>';
$_['text_modal_success']   		= 'SMS Code Verification';
$_['text_modal_success_message']   		= 'Verification code has been send. Please check our SMS in your phone.';
$_['button_ok']   		= 'Ok';

$_['text_resend_email']   		= 'Success resend email verification';

?>

