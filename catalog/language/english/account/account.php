<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Your Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';

$_['text_activate']  = 'Activate your Account';

$_['text_name']       = 'Name';
$_['text_email']       = 'Email';
$_['text_phone']       = 'Phone Number';
$_['text_dob']       = 'Date of Birth';
$_['text_account_bank']       = 'Bank Account';
$_['text_account_name']       = 'Account Name';
$_['text_account_number']       = 'Account Number';
$_['text_balance']       = 'Balance';
$_['text_reward_point']       = ' %s point';

$_['text_list_order'] = '&nbsp;Latest Orders';
$_['text_list_return'] = '&nbsp;Return';

$_['text_column_invoice_order'] = 'Invoice';
$_['text_column_status_order'] = 'Status';
$_['text_column_action_order'] = 'Action';

$_['text_column_invoice_return'] = 'Invoice';
$_['text_column_product_return'] = 'Product';
$_['text_column_status_return'] = 'Status';
$_['text_column_action_return'] = 'Action';

$_['text_view_more'] = 'View More...';
$_['text_view'] = 'View';
$_['text_no_data'] = 'No Data';

$_['button_edit']       = 'Edit';