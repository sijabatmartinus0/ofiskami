<?php
// Heading
$_['heading_title']      = 'Product Returns';

// Text
$_['text_account']       = 'Account';
$_['text_return']        = 'Return Information';
$_['text_return_detail'] = 'Return Details';
$_['text_description']   = 'Please complete the form below to request an RMA number.';
$_['text_order']         = 'Order Information';
$_['text_product']       = 'Product Information &amp; Reason for Return';
$_['text_message']       = '<p>Thank you for submitting your return request. Your request has been sent to the relevant department for processing.</p><p> You will be notified via e-mail as to the status of your request.</p>';
$_['text_return_id']     = 'Return ID:';
$_['text_order_id']      = 'Order ID:';
$_['text_date_ordered']  = 'Order Date:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Date Added:';
$_['text_email']    	 = 'Email:';
$_['text_comment']       = 'Return Comments';
$_['text_history']       = 'Return History';
$_['text_empty']         = 'You have not made any previous returns!';
$_['text_agree']         = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'Return ID';
$_['column_order_id']    = 'Order ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Date Added';
$_['column_customer']    = 'Customer';
$_['column_merchant']    = 'Merchant';
$_['column_product']     = 'Product Name';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Quantity';
$_['column_price']       = 'Price';
$_['column_opened']      = 'Opened';
$_['column_comment']     = 'Comment';
$_['column_reason']      = 'Reason';
$_['column_action']      = 'Action';

// Entry
$_['entry_order_id']     = 'Order ID';
$_['entry_date_ordered'] = 'Order Date';
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telephone';
$_['entry_product']      = 'Product Name';
$_['entry_model']        = 'Product Code';
$_['entry_quantity']     = 'Quantity';
$_['entry_reason']       = 'Reason for Return';
$_['entry_opened']       = 'Product is opened';
$_['entry_fault_detail'] = 'Faulty or other details';
$_['entry_captcha']      = 'Enter the code in the box below';

// Error
$_['text_error']         = 'The returns you requested could not be found!';
$_['error_order_id']     = 'Order ID required!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 3 and 32 characters!';
$_['error_product']      = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']        = 'Product Model must be greater than 3 and less than 64 characters!';
$_['error_reason']       = 'You must select a return product reason!';
$_['error_captcha']      = 'Verification code does not match the image!';
$_['error_agree']        = 'Warning: You must agree to the %s!';
$_['error_quantity']     = 'Minimum quantity is 1';
$_['error_quantity_limit'] = 'Quantity can not exceed total product per order';
$_['error_comment'] 	 = 'Please supply details';

//edit by arin
$_['text_return_action']		= 'Expected return action';
$_['text_upload_return']		= 'Upload product picture';
$_['text_upload']				= 'Upload';
$_['text_invoice']				= 'Invoice Number';
$_['text_complete_return']  	= 'Return Complete';
$_['text_confirm'] 	 			= 'Confirm Action Change';
$_['text_discuss']    			= 'Return Application Discussion';
$_['text_no_data']    			= "There's no comment yet";
$_['text_view_more']     		= 'View more';

$_['success_complete_return']  	= 'Return Process Completed';
$_['success_confirm_action']  	= 'You have confirmed return action change';
$_['warning_action_changed']  	= 'The expected action has been changed, please confirm or discuss with Merchant.';
$_['warning_file_type']  		= 'Only image file types (.jpg, .png, .jpeg) are allowed to be uploaded';
$_['error_return_action']   	= 'Please choose your expected return action';
$_['error_picture_return'] 		= 'Upload product picture';
$_['error_discuss']   		    = 'The comment body must be at least 3 characters long';

$_['column_invoice']	 		= 'Invoice Number';
$_['column_phone']		 		= 'Phone';

$_['button_view']   			= 'View';
$_['button_submit']    			= 'Submit';

?>