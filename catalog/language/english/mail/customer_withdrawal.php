<?php
$_['text_subject'] 				= '%s - Withdrawal Request';
$_['text_customer'] 			= 'Customer: ';
$_['text_withdrawal'] 			= 'Withdrawal: ';
$_['text_date_added'] 			= 'Date Request: ';
$_['text_amount'] 				= 'Amount: ';
$_['text_description'] 			= 'Description: ';
$_['text_withdrawal_link'] 		= 'To view your withdrawal status click on the link below:';
$_['text_withdrawal_status'] 	= 'Your withdrawal request has been sent';
$_['text_withdrawal_status_admin'] 	= 'New withdrawal request';
$_['text_withdrawal_request'] 	= 'Withdrawal Request';
$_['text_footer'] 				= 'Please reply to this email if you have any questions.';