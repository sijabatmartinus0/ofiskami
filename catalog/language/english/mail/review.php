<?php
// Text
$_['text_subject']	= '%s - Product Review';
$_['text_waiting']	= 'You have a new product review waiting.';
$_['text_product']	= 'Product: %s';
$_['text_reviewer']	= 'Reviewer: %s';
$_['text_rating']	= 'Rating: %s';
$_['text_rating_accuracy']	= 'Delivery Accuracy Rating: %s';
$_['text_review']	= 'Review Text:';
$_['text_greeting_customer']	= 'Thank you for your review';
$_['text_greeting_merchant']	= 'You have a new review of your product';