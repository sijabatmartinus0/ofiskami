<?php
// Text
$_['text_new_subject']          = '%s - New Product Comment';
$_['text_reply_subject']        = '%s - New Reply on Product Comment';

$_['text_footer']        		= 'Please reply to this email if you have any questions.';

//Return Discussion
$_['text_new_comment']       	= 'You have a new comment on product %s as decribed bellow.';
$_['text_new_reply']       		= 'You have a new reply comment on product %s as decribed bellow.';
$_['text_link']      			= 'Click the link below to view more detail:';

$_['text_merchant']  			= 'Merchant';
$_['text_customer']  			= 'Customer';
?>