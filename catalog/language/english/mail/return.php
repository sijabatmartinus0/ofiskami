<?php
// Text
$_['text_new_subject']          = '%s - Return %s';
$_['text_new_received']         = 'You have received a return application.';
$_['text_update_return_status'] = 'Your return has been updated to the following status:';
$_['text_new_link']             = 'To view your return detail, click on the link below:';

$_['text_new_return_detail']    = 'Return Details';
$_['text_new_return_id']        = 'Return ID:';
$_['text_new_order_id']         = 'Order ID:';
$_['text_date_added']    		= 'Date Ordered:';
$_['text_date_returned']    	= 'Date Returned:';
$_['text_invoice']    			= 'Invoice:';
$_['text_customer']    			= 'Customer:';
$_['text_telephone']    		= 'Telephone:';
$_['text_email']    			= 'Email:';

$_['text_yes']    				= 'Yes';
$_['text_no']    				= 'No';

$_['text_information']    		= 'Product Information & Return Reason';
$_['text_prod_name']    		= 'Product Name:';
$_['text_prod_model']    		= 'Product Model:';
$_['text_prod_qty']    			= 'Quantity:';
$_['text_prod_open']    		= 'Opened:';
$_['text_ret_reason']    		= 'Return Reason:';
$_['text_ret_comment']    		= 'Return Comment:';
$_['text_ret_action']    		= 'Expected Action:';
$_['text_ret_status']    		= 'Return Status:';

$_['text_update_footer']        = 'Please reply to this email if you have any questions.';

$_['text_merchant_complete']    = 'This following return has been updated to the following status:';
$_['text_merchant_link']        = 'To view return detail click on the link below:';

//Return Discussion
$_['text_new_discussion']       = 'You have a new message on return discussion board as decribed bellow.';
$_['text_discussion_link']      = 'Click the link below to view more detail:';
$_['text_new_subject_discuss']  = '%s - Return Discussion %s';
$_['text_merchant']  			= 'Merchant';
$_['text_customer']  			= 'Customer';

//Return Action Change
$_['text_new_action']          	= '%s - Action Change Return %s';
$_['text_action_change']  		= 'Your expected action of this following return has been changed by merchant to:';

//Return Action Confirm
$_['text_new_confirm']          = '%s - Confirm Action Change Return %s';
$_['text_action_confirm']  		= 'Customer has confirmed expected action change of this following return.';
?>