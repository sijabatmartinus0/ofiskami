<?php
// Text
$_['text_subject']		= '%s - Newsletter';
$_['text_greeting']		= 'Dear %s,';
$_['text_unsubscribe']	= 'You have requested to unsubscribe newsletter. From now on you will be no longer receive any promotion email from us.';
$_['text_subscribe']	= 'You have requested to subscribe newsletter. From now on you will be receive any promotion email from us.';
$_['text_link']			= 'To change your newsletter subscription please follow this link below';

?>
