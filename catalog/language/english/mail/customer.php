<?php
// Text
$_['text_subject']        = '%s - Aktivasi akun Ofiskita Anda!';
$_['text_welcome']        = 'Welcome and thank you for registering at %s!';
$_['text_login']          = 'Your account has now been created and please click the following URL to complete your registration:';
$_['text_approval']       = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_services']       = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_thanks']         = 'Thanks,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has signed up:';
$_['text_website']        = 'Web Site:';
$_['text_customer_group'] = 'Customer Group:';
$_['text_firstname']      = 'First Name:';
$_['text_lastname']       = 'Last Name:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telephone:';

$_['text_title']      = 'Aktivasi Email';
$_['text_header']      = 'Mengaktifkan Akun Ofiskita';
$_['text_message_register']      = 'Terima kasih sudah mendaftar untuk bergabung dengan Ofiskita, marketplace online nomor satu di Indonesia. Untuk menyelesaikan proses pendaftaran, mohon lakukan konfirmasi pendaftaran melalui tombol di bawah ini.';
$_['button_activate']      = 'Aktifkan';
$_['text_message_link_register']      = 'Jika button di atas tidak berfungsi, silahkan copy paste alamat <a style=\'color: #348eda;\' href=\'%s\'>%s</a> ke dalam browser anda.';
$_['text_message_warning']      = 'Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat ofiskita.com bukan alamat lainnya.';
