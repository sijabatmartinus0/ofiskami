<?php
// Text
$_['text_new_subject']          = '%s - Order %s';
$_['text_new_greeting']         = 'Thank you for your interest in %s products. Your order has been received and will be processed once payment has been confirmed.';
$_['text_new_received']         = 'You have received an order.';
$_['text_new_link']             = 'To view your order click on the link below:';
$_['text_new_order_detail']     = 'Order Details';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'Order ID:';
$_['text_new_date_added']       = 'Date Added:';
$_['text_new_order_status']     = 'Order Status:';
$_['text_new_payment_method']   = 'Payment Method:';
$_['text_new_shipping_method']  = 'Shipping Method:';
$_['text_new_email']  			= 'E-mail:';
$_['text_new_telephone']  		= 'Telephone:';
$_['text_new_ip']  				= 'IP Address:';
$_['text_new_payment_address']  = 'Payment Address';
$_['text_new_shipping_address'] = 'Shipping Address';
$_['text_new_products']         = 'Products';
$_['text_new_product']          = 'Product';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Quantity';
$_['text_new_price']            = 'Price';
$_['text_new_order_total']      = 'Order Totals';
$_['text_new_total']            = 'Total';
$_['text_new_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_new_comment']          = 'The comments for your order are:';
$_['text_new_footer']           = 'Please reply to this e-mail if you have any questions.';
$_['text_update_subject']       = '%s - Order Update %s';
$_['text_update_order']         = 'Order ID:';
$_['text_update_order_detail']  = 'Order Detail ID:';
$_['text_update_date_added']    = 'Date Ordered:';
$_['text_update_order_status']  = 'Your order has been updated to the following status:';
$_['text_update_comment']       = 'The comments for your order are:';
$_['text_update_link']          = 'To view your order click on the link below:';
$_['text_update_footer']        = 'Please reply to this email if you have any questions.';

//edit by arin
$_['text_subject']          	= '%s - Order Update %s Order Detail %s';
$_['text_shipping_price']       = 'Shipping Price';
$_['text_receipt_number']       = 'Receipt Number:';
$_['text_decline_reason']       = 'Decline Reason:';

$_['text_merchant_complete']    = 'This following order has been updated to the following status:';
$_['text_merchant_link']        = 'To view order detail click on the link below:';


$_['text_verified_subject']       = 'New Order #%s';
$_['text_complete_subject']       = 'Order Complete #%s';
$_['text_verified_order']         = 'Order ID:';
$_['text_verified_date_added']    = 'Date Ordered:';
$_['text_verified_message']    	  = 'You receive new order, details as follows :';
$_['text_verified_order_status']  = 'New Order Status:';
$_['text_verified_comment']       = 'The order comments:';
$_['text_verified_link']          = 'To view order click on this link';
$_['text_verified_footer']        = 'Please reply to this email if you have any questions.';

$_['text_seller'] = "Seller";
$_['text_shipping_address'] = "Shipping Address";
$_['text_delivery_type'] = "Delivery Type";
$_['text_shipping_price'] ="Shipping Price";
$_['text_total_invoice'] = "Total Receipt Number";
$_['column_product'] ="Product";
$_['column_model'] = "Model";
$_['column_quantity'] = "Quantity";
$_['column_price'] = "Price";
$_['column_total'] = "Total";

$_['text_payment_rejected'] = "Payment Confirmation Rejected";
$_['text_reconfirm'] = "Please resent payment confirmation with valid information.";


$_['text_review'] = "Please give your review about this order by clicking link below";

