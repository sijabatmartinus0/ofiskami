<?php
// Text
$_['text_subject_pickup'] = 'Pengambilan Order';
$_['text_title_pickup'] = 'Pengambilan Order';
$_['text_header_pickup'] = 'Order Anda Telah Selesai';
$_['message_pickup'] = 'Terima kasih sudah menggunakan jasa Ofiskita. Order anda dengan nomor %s telah anda ambil di :';
$_['message_link_pickup'] = 'Anda dapat melihat status order terakhir anda pada link berikut <a style=\'color: #348eda;\' href=\'%s\'>Lihat Order</a>';

$_['text_subject_receive'] = 'Order Tersedia';
$_['text_title_receive'] = 'Order Tersedia';
$_['text_header_receive'] = 'Order Anda Telah Tersedia';
$_['message_receive'] = 'Terima kasih sudah menggunakan jasa Ofiskita. Order anda dengan nomor %s telah tersedia di :';
$_['message_link_receive'] = 'Anda dapat melihat status order terakhir anda pada link berikut <a style=\'color: #348eda;\' href=\'%s\'>Lihat Order</a>';

$_['text_message_warning']      = 'Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat ofiskita.com bukan alamat lainnya.';

?>