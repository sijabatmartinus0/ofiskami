<?php
// Heading
$_['heading_title'] = 'Payment Confirmation';

// Text
$_['text_invoice']   			= 'Receipt Number';
$_['text_total_payment']    	= 'Total Price';
$_['text_date_payment']     	= 'Payment Date';
$_['text_method_payment']   	= 'Payment Method';
$_['text_bank_name']   			= 'Bank';
$_['text_account_name']   		= 'Account Name';
$_['text_account_number']   	= 'Account Number';
$_['text_branch']   			= 'Branch';
$_['text_account_destination']  = 'Account Destination';
$_['text_amount']  				= 'Amount Paid';
$_['text_remarks']  			= 'Remarks';
$_['text_add_bank']  			= 'Tambah Rekening Bank';
$_['text_upload']  				= 'Upload File';
$_['text_empty']  				= '<center>Data not Found</center>';
$_['text_choose']  				= 'Choose file';

// Note
$_['note_branch']  				= '<p>Untuk bank selain BCA diharuskan mengisi Kantor Cabang beserta kota tempat Bank berada</p><p>contoh: Pondok Indah - Jakarta Selatan</p>';
$_['note_amount']  				= '<p>Inputlah sesuai dengan jumlah uang yang Anda transfer.</p><p>Masukkan jumlah pembayaran tanpa tanda titik atau koma.</p><p>Contoh: 157500</p>';
$_['note_product']  			= '- Produk yang sudah dipesan dan dikonfirmasikan pembayarannya tidak dapat dibatalkan.';
$_['note_upload']  				= '- Harap mengunggah bukti pembayaran di halaman Status Pemesanan untuk membantu memudahkan identifikasi dana pembayaran Anda.';
$_['note_optional']  			= ') Optional';

// Button
$_['button_yes']  				= 'Yes';
$_['button_no']  				= 'No';
$_['button_bank']  				= 'Choose Bank';

// Error Notification
$_['error_date_payment']  		= 'Choose payment date';
$_['error_account_id']  		= 'Choose your bank';
$_['error_account_name']  		= 'Account name must be filled';
$_['error_account_number']  	= 'Account number must be filled with numeric character';
$_['error_account_number_int']  = 'Account number must be filled with numeric character';
$_['error_branch']  			= 'Branch must be filled';
$_['error_account_destination'] = 'Choose account destination';
$_['error_amount']  			= 'Total amount must be filled with numeric character and must be same with total price';
$_['error_amount_int']  		= 'Total amount must be filled with numeric character';
$_['error_amount_match']  		= 'The amount must be same with total price';
$_['error_picture_confirmation']= 'Only .pdf .jpg, .jpeg, or .png files are allowed';
$_['error_date_format']			= 'Date payment should be in YYYY-MM-DD format';
$_['error_date_limit']			= 'Date payment may not exceed today';
$_['error_date_order']			= 'Payment date should be greater than order date';
$_['error_payment_method']		= 'Please choose payment method';
$_['error_payment_date']		= 'Payment date must be the same of order date or the day after order date';

$_['acc_destination']			= 'Choose Account Destination';
$_['pay_method']				= 'Choose Payment Method';

// Pop up
$_['column_bank_name']			= '<b>Name</b>';
$_['column_bank_code']			= '<b>Bank Code</b>';
$_['column_search']				= 'Search...';
$_['column_close']				= 'Close';

$_['error_picture_confirmation_size']			= 'Uploaded File not greater than 1MB';
$_['error_picture_confirmation_ext']			= 'Wrong file format';

$_['error_confirm']			= 'You cannot confirm order with Online Payment method';
?>