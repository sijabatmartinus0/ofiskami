<?php
// Heading
$_['heading_title']      = 'Payment Confirmation Success';
$_['heading_success']    = 'Confirmation Success';

// Text
$_['text_first']         = '<p>Thank you, you have successfully confirm your payment using ATM transfer method.</p>';
$_['text_second']        = '<p>Total price you have confirmed:</p>';
$_['text_third']         = '<p>Your payment will be verified by Ofiskita in 24 hours and will be passed directly to the seller.</p>';
$_['text_fourth']         = '<p><a href="%s">Click here</a> for tracking your order</p>';
$_['text_fourth']         = '<p><a href="%s">Click here</a> for tracking your order</p>';
?>