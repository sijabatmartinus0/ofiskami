<?php
// Text
$_['text_title']				= 'Employee Program';
$_['text_instruction']			= 'Employee Program Instructions';
$_['text_description']			= 'Please transfer the total amount to the following bank account.';
$_['text_payment']				= 'Your order will not ship until we receive payment.';