<?php
// Heading 
$_['heading_title']    = 'Akun';

// Text
$_['text_register']    = 'Daftar';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Lupa Password';
$_['text_account']     = 'Akun Saya';
$_['text_edit']        = 'Ubah Akun';
$_['text_password']    = 'Password';
$_['text_address']     = 'Buku Alamat';
$_['text_wishlist']    = 'Daftar Keinginan';
$_['text_order']       = 'Riwayat Pesanan';
$_['text_download']    = 'Unduh';
$_['text_reward']      = 'Poin Hadiah';
$_['text_return']      = 'Pengembalian';
$_['text_transaction'] = 'Transaksi';
$_['text_newsletter']  = 'Newsletter';
$_['text_recurring']   = 'Pembayaran Berulang';
$_['text_seller']   = 'Daftar sebagai Penjual';
$_['text_withdrawal']   = 'Penarikan';
$_['text_review']   = 'Review';
$_['text_my_account']   = 'Akun Saya';
$_['text_register_merchent'] = 'Daftar Merchant';

?>
