<?php
$_['text_subject'] 				= '%s - Request Penarikan Saldo';
$_['text_customer'] 			= 'Customer: ';
$_['text_withdrawal'] 			= 'Penarikan Saldo: ';
$_['text_date_added'] 			= 'Tanggal: ';
$_['text_amount'] 				= 'Jumlah: ';
$_['text_description'] 			= 'Deskripsi: ';
$_['text_withdrawal_link'] 		= 'Untuk melihat request penarikan saldo anda klik pada link di bawah:';
$_['text_withdrawal_status'] 	= 'Request Penarikan anda telah terkirim';
$_['text_withdrawal_status_admin'] 	= 'Request Penarikan baru';
$_['text_withdrawal_request'] 	= 'Request Penarikan Saldo';
$_['text_footer'] 				= 'Silakan balas e-mail ini jika Anda memiliki pertanyaan.';