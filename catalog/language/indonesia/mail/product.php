<?php
// Text
$_['text_new_subject']          = '%s - Komentar Baru Produk';
$_['text_reply_subject']        = '%s - Balasan Baru Untuk Komentar Produk';

$_['text_footer']        		= 'Silakan balas e-mail ini jika Anda memiliki pertanyaan.';

//Return Discussion
$_['text_new_comment']       	= 'Anda mendapat komentar baru untuk produk %s';
$_['text_new_reply']       		= 'Anda mendapat balasan baru untuk komentar produk %s';
$_['text_link']      			= 'Klik link di bawah ini untuk melihat lebih lanjut:';

$_['text_merchant']  			= 'Penjual';
$_['text_customer']  			= 'Pembeli';
?>