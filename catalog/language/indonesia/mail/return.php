<?php
// Text
$_['text_new_subject']          = '%s - Pengembalian %s';
$_['text_new_received']         = 'Anda menerima permohonan pengembalian baru.';
$_['text_update_return_status'] = 'Permohonan pengembalian Anda telah berubah status menjadi:';
$_['text_new_link']             = 'Untuk melihat detail pengembalian Anda, klik link di bawah ini:';

$_['text_new_return_detail']    = 'Detail Pengembalian';
$_['text_new_return_id']        = 'ID Pengembalian:';
$_['text_new_order_id']         = 'ID Pemesanan:';
$_['text_date_added']    		= 'Tanggal Pemesanan:';
$_['text_date_returned']    	= 'Tanggal Pengembalian:';
$_['text_invoice']    			= 'Invoice:';
$_['text_customer']    			= 'Pelanggan:';
$_['text_telephone']    		= 'Telepon:';
$_['text_email']    			= 'Email:';

$_['text_yes']    				= 'Ya';
$_['text_no']    				= 'Tidak';

$_['text_information']    		= 'Informasi Produk & Alasan Pengembalian';
$_['text_prod_name']    		= 'Nama Produk:';
$_['text_prod_model']    		= 'Model:';
$_['text_prod_qty']    			= 'Jumlah:';
$_['text_prod_open']    		= 'Dibuka:';
$_['text_ret_reason']    		= 'Alasan Pengembalian:';
$_['text_ret_comment']    		= 'Komentar:';
$_['text_ret_action']    		= 'Tindakan:';
$_['text_ret_status']    		= 'Status Pengembalian:';

$_['text_update_footer']        = 'Silakan balas e-mail ini jika Anda memiliki pertanyaan.';

$_['text_merchant_complete']    = 'Permohonan pengembalian di bawah ini telah berubah status menjadi:';
$_['text_merchant_link']        = 'Untuk melihat detail pengembalian, klink link di bawah ini:';

//Return Discussion
$_['text_new_discussion']       = 'Anda mendapat pesan baru pada halaman diskusi pengembalian produk.';
$_['text_discussion_link']      = 'Klik link di bawah ini untuk melihat lebih lanjut:';
$_['text_merchant']  			= 'Penjual';
$_['text_customer']  			= 'Pembeli';

//Return Action Change
$_['text_new_action']          	= '%s - Perubahan Tindakan Pengembalian %s';
$_['text_action_change']  		= 'Tindakan untuk permohonan pengembalian Anda telah diganti oleh penjual menjadi:';

//Return Action Confirm
$_['text_new_confirm']          = '%s - Konfirmasi Perubahan Tindakan Pengembalian %s';
$_['text_action_confirm']  		= 'Pelanggan telah mengkonfirmasi perubahan tindakan untuk pengembalian berikut ini.';
?>