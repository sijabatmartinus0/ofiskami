<?php
// Text
$_['text_subject']  = '%s - Aktivasi akun Ofiskita Anda!';
$_['text_welcome']  = 'Selamat datang dan terima kasih untuk mendaftar di %s!';
$_['text_login']    = 'Akun Anda kini telah dibuat dan Anda dapat menyelesaikan proses registrasi dengan mengakses URL berikut:';
$_['text_approval'] = 'Akun Anda harus disetujui sebelum Anda dapat login. Setelah disetujui Anda dapat login dengan menggunakan alamat e-mail dan password Anda dengan mengunjungi website Kami atau pada URL berikut:';
$_['text_services'] = 'Setelah login, Anda akan dapat mengakses layanan lain termasuk meninjau pesanan terakhir, mencetak faktur dan mengubah informasi Akun Anda.';
$_['text_thanks']   = 'Terima kasih,';
$_['text_new_customer']   = 'Pelanggan Baru';
$_['text_signup']         = 'Pelanggan Baru telah mendaftar:';
$_['text_website']        = 'Website:';
$_['text_customer_group'] = 'Kelompok Pelanggan:';
$_['text_firstname']      = 'Nama Depan:';
$_['text_lastname']       = 'Nama Belakang:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telepon:';

$_['text_title']      = 'Aktivasi Email';
$_['text_header']      = 'Mengaktifkan Akun Ofiskita';
$_['text_message_register']      = 'Terima kasih sudah mendaftar untuk bergabung dengan Ofiskita, marketplace online nomor satu di Indonesia. Untuk menyelesaikan proses pendaftaran, mohon lakukan konfirmasi pendaftaran melalui tombol di bawah ini.';
$_['button_activate']      = 'Aktifkan';
$_['text_message_link_register']      = 'Jika button di atas tidak berfungsi, silahkan copy paste alamat <a style=\'color: #348eda;\' href=\'%s\'>%s</a> ke dalam browser anda.';
$_['text_message_warning']      = 'Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat ofiskita.com bukan alamat lainnya.';

?>
