<?php
// Text
$_['text_subject']		= '%s - Newsletter';
$_['text_greeting']		= 'Dear %s,';
$_['text_unsubscribe']	= 'Anda tidak lagi berlangganan newsletter. Mulai saat ini Anda tidak akan menerima email promosi dari kami.';
$_['text_subscribe']	= 'Anda akan berlangganan newsletter. Mulai saat ini Anda akan menerima email promosi dari kami.';
$_['text_link']			= 'Untuk mengubah status berlangganan newsletter silahkan klik link di bawah ini';

?>
