<?php
// Text
$_['text_new_subject']          = '%s - Pesanan %s';
$_['text_new_greeting']         = 'Terima kasih atas minat Anda pada produk %s. Pesanan Anda telah diterima dan akan diproses setelah pembayaran telah dikonfirmasi.';
$_['text_new_received']         = 'Anda telah menerima Pesanan.';
$_['text_new_link']             = 'Untuk melihat pesanan Anda klik pada link di bawah:';
$_['text_new_order_detail']     = 'Rincian Pesanan';
$_['text_new_instruction']      = 'Instruksi';
$_['text_new_order_id']         = 'No. Pesanan:';
$_['text_new_date_added']       = 'Tanggal ditambahkan:';
$_['text_new_order_status']     = 'Status Pesanan:';
$_['text_new_payment_method']   = 'Metode Pembayaran:';
$_['text_new_shipping_method']  = 'Metode Pengiriman:';
$_['text_new_email']  			= 'E-Mail:';
$_['text_new_telephone']  		= 'Telepon:';
$_['text_new_ip']  				= 'IP Address:';
$_['text_new_payment_address']  = 'Alamat Penagihan';
$_['text_new_shipping_address'] = 'Alamat Pengiriman';
$_['text_new_products']         = 'Produk';
$_['text_new_product']          = 'Produk';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Jumlah';
$_['text_new_price']            = 'Harga';
$_['text_new_order_total']      = 'Total Pesanan';	
$_['text_new_total']            = 'Total';	
$_['text_new_download']         = 'Setelah pembayaran Anda telah dikonfirmasi Anda dapat klik pada link di bawah untuk mengakses unduhan produk Anda:';
$_['text_new_comment']          = 'Komentar untuk pesanan Anda:';
$_['text_new_footer']           = 'Silakan balas e-mail ini jika Anda memiliki pertanyaan.';
$_['text_update_subject']       = '%s - Status Pesanan %s';
$_['text_update_order']         = 'No. Pesanan:';
$_['text_update_date_added']    = 'Tanggal Pesanan:';
$_['text_update_order_detail']  = 'Order Detail ID:';
$_['text_update_order_status']  = 'Pesanan Anda telah diperbaharui ke status berikut:';
$_['text_update_comment']       = 'Komentar untuk pesanan Anda:';
$_['text_update_link']          = 'Untuk melihat pesanan anda klik pada link di bawah:';
$_['text_update_footer']        = 'Silakan balas e-mail ini jika Anda memiliki pertanyaan.';

//edit by arin
$_['text_subject']          	= '%s - Order Update %s Order Detail %s';
$_['text_shipping_price']       = 'Biaya Pengiriman';
$_['text_receipt_number']       = 'Nomor Resi:';
$_['text_decline_reason']       = 'Alasan Penolakan:';

$_['text_merchant_complete']    = 'Order beriku ini telah berubah status menjadi:';
$_['text_merchant_link']        = 'Untuk melihat detail order klik pada link di bawah ini:';


$_['text_verified_subject']       = 'Order Baru #%s';
$_['text_complete_subject']       = 'Order Selesai #%s';
$_['text_verified_order']         = 'Order ID:';
$_['text_verified_date_added']    = 'Order Data:';
$_['text_verified_message']    = 'Anda mendapat order, detil order sebagai berikut :';
$_['text_verified_order_status']  = 'Status Order Baru:';
$_['text_verified_comment']       = 'Komentar order:';
$_['text_verified_link']          = 'Klik link berikut untuk melihat detil order';
$_['text_verified_footer']        = 'Silahkan balas email ini jika anda mempunyai pertanyaan.';


$_['text_seller'] = "Pejual";
$_['text_shipping_address'] = "Alamat Pengiriman";
$_['text_delivery_type'] = "Tipe Pengiriman";
$_['text_shipping_price'] ="Biaya Pengiriman";
$_['text_total_invoice'] = "Total No. Struk";
$_['column_product'] ="Produk";
$_['column_model'] = "Model";
$_['column_quantity'] = "Jumlah";
$_['column_price'] = "Harga";
$_['column_total'] = "Total";

$_['text_payment_rejected'] = "Konfirmasi Pembayaran Ditolak";
$_['text_reconfirm'] = "Harap konfirmasi pembayaran sekali lagi dengan informasi yang valid.";

$_['text_review'] = "Untuk memberi review atas pesanan ini silahkan klik tautan di bawah ini";
?>
