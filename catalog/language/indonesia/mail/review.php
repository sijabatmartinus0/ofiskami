<?php
// Text
$_['text_subject']	= '%s - Ulasan Produk';
$_['text_waiting']	= 'Anda memiliki ulasan produk baru yang telah menunggu.';
$_['text_product']	= 'Produk: %s';
$_['text_reviewer']	= 'Penilai: %s';
$_['text_rating']	= 'Penilaian: %s';
$_['text_rating_accuracy']	= 'Penilaian Akurasi Pengiriman: %s';
$_['text_review']	= 'Teks Ulasan:';
$_['text_greeting_customer']	= 'Terima kasih atas penilaian Anda';
$_['text_greeting_merchant']	= 'Anda mendapat penilaian baru atas produk Anda';