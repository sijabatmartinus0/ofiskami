<?php
// Heading
$_['heading_title'] = 'Konfirmasi Pembayaran';

// Label form
$_['text_invoice']   			= 'No. Struk';
$_['text_total_payment']    	= 'Total Tagihan';
$_['text_date_payment']    	 	= 'Tanggal Pembayaran';
$_['text_method_payment']   	= 'Metode Pembayaran';
$_['text_bank_name']   			= 'Nama Bank';
$_['text_account_name']   		= 'Nama Pemilik Rekening';
$_['text_account_number']   	= 'Nomor Rekening';
$_['text_branch']   			= 'Cabang';
$_['text_account_destination']  = 'Rekening Tujuan';
$_['text_amount']  				= 'Jumlah yang sudah dibayar';
$_['text_remarks']  			= 'Keterangan';
$_['text_add_bank']  			= 'Tambah Rekening Bank';
$_['text_upload']  				= 'Unggah Bukti Pembayaran';
$_['text_empty']  				= '<center>Data tidak ditemukan</center>';
$_['text_choose']  				= 'Pilih file';

// Note
$_['note_branch']  				= '<p>Untuk bank selain BCA diharuskan mengisi Kantor Cabang beserta kota tempat Bank berada</p><p>contoh: Pondok Indah - Jakarta Selatan</p>';
$_['note_amount']  				= '<p>Inputlah sesuai dengan jumlah uang yang Anda transfer.</p><p>Masukkan jumlah pembayaran tanpa tanda titik atau koma.</p><p>Contoh: 157500</p>';
$_['note_product']  			= '- Produk yang sudah dipesan dan dikonfirmasikan pembayarannya tidak dapat dibatalkan.';
$_['note_upload']  				= '- Harap mengunggah bukti pembayaran di halaman Status Pemesanan untuk membantu memudahkan identifikasi dana pembayaran Anda.';
$_['note_optional']  			= ') Opsional';

// Button
$_['button_yes']  				= 'Ya';
$_['button_no']  				= 'Tidak';
$_['button_bank']  				= 'Pilih Bank';

// Error Notification
$_['error_date_payment']  		= 'Pilih tanggal pembayaran';
$_['error_account_id']  		= 'Pilih bank';
$_['error_account_name']  		= 'Nama pemilik rekening wajib diisi';
$_['error_account_number']  	= 'Nomor rekening wajib diisi dan harus berupa angka';
$_['error_account_number_int']  = 'Nomor rekening harus berupa angka';
$_['error_branch']  			= 'Cabang wajib diisi';
$_['error_account_destination'] = 'Pilih rekening tujuan';
$_['error_amount']  			= 'Total yang dibayar wajib diisi dengan angka dan harus sama dengan total tagihan';
$_['error_amount_int']  		= 'Total yang dibayar harus berupa angka';
$_['error_amount_match']  		= 'Jumlah pembayaran harus sama dengan total harga';
$_['error_picture_confirmation']= 'Format file yang diperbolehkan hanya .pdf .jpeg, .jpg, atau .png';
$_['error_date_format']			= 'Format tanggal yang diperbolehkan adalah YYYY-MM-DD';
$_['error_date_limit']			= 'Tanggal pembayaran tidak boleh melebihi hari ini';
$_['error_date_order']			= 'Tanggal pembayaran harus lebih besar dari tanggal pemesanan';
$_['error_payment_method']		= 'Pilih metode pembayaran';
$_['error_payment_date']		= 'Tanggal pembayaran harus sama dengan tanggal pemesanan atau sehari setelah pemesanan';

$_['acc_destination']			= 'Pilih Rekening Tujuan';
$_['pay_method']				= 'Pilih Metode Pembayaran';

// Pop up
$_['column_bank_name']			= '<b>Nama</b>';
$_['column_bank_code']			= '<b>Kode Bank</b>';
$_['column_search']				= 'Cari...';
$_['column_close']				= 'Tutup';


$_['error_picture_confirmation_size']			= 'Ukuran file upload tidak boleh lebih dari 1MB';
$_['error_picture_confirmation_ext']			= 'Format file salah';

$_['error_confirm']			= 'Anda tidak dapat melakukan konfirmasi untuk pemesanan dengan Online Payment';
?>