<?php
// Heading
$_['heading_title']      = 'Sukses Konfirmasi Pembayaran';
$_['heading_success']    = 'Konfirmasi Sukses';

// Text
$_['text_first']         = '<p>Terima kasih, Anda telah berhasil melakukan konfirmasi pembayaran dengan menggunakan metode transfer ATM</p>';
$_['text_second']        = '<p>Total yang Anda Konfirmasi</p>';
$_['text_third']         = '<p>Pembayaran Anda akan diverifikasi oleh Ofiskita dalam 1x24 jam dan akan diteruskan kepada penjual.</p>';
$_['text_fourth']         = '<p>Klik <a href="%s">di sini</a> untuk memantau status pemesanan Anda.</p>';
?>