<?php
// Text
$_['text_title']       = 'Transfer Bank';
$_['text_instruction'] = 'Intruksi Transfer Bank';
$_['text_description'] = 'Silahkan transfer total jumlah pada Nomor Rekening Bank berikut.';
//$_['text_payment']     = 'Pesanan Anda tidak akan dikirim sampai Kami menerima pembayaran Anda.';
$_['text_payment']     = 'Harap melakukan konfirmasi pembayaran dalam 1x24 jam. Apabila Anda tidak melakukan konfirmasi dalam batas waktu yang ditentukan, pesanan Anda otomatis akan dibatalkan.';
?>
