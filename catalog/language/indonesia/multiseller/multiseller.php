<?php

// **********
// * Global *
// **********
$_['ms_viewinstore'] = 'Lihat di toko';
$_['ms_view'] = 'Lihat';
$_['ms_view_modify'] = 'Lihat / Ubah';
$_['ms_publish'] = 'Muat';
$_['ms_unpublish'] = 'Batal muat';
$_['ms_edit'] = 'Ubah';
$_['ms_comment'] = 'Komentar';
$_['ms_clone'] = 'Klon';
$_['ms_relist'] = 'Muat ulang';
$_['ms_rate'] = 'Nilai';
$_['ms_download'] = 'Unduh';
$_['ms_create_product'] = 'Buat produk';
$_['ms_delete'] = 'Hapus';
$_['ms_update'] = 'Perbarui';
$_['ms_type'] = 'Tipe';
$_['ms_amount'] = 'Jumlah';
$_['ms_status'] = 'Status';
$_['ms_date_paid'] = 'Tanggal pembayaran';
$_['ms_last_message'] = 'Pesan terakhir';
$_['ms_description'] = 'Deskripsi';
$_['ms_id'] = '#';
$_['ms_by'] = 'oleh';
$_['ms_action'] = 'Aksi';
$_['ms_sender'] = 'Pengirim';
$_['ms_message'] = 'Pesan';
$_['ms_none'] = 'None';


$_['ms_date_created'] = 'Tanggal pembuatan';
$_['ms_date'] = 'Tanggal';

$_['ms_button_submit'] = 'Ajukan';
$_['ms_button_add_special'] = 'Definisikan sebuah harga spesial baru';
$_['ms_button_add_discount'] = 'Definisikan sebuah diskon baru';
$_['ms_button_submit_request'] = 'Ajukan permintaan';
$_['ms_button_save'] = 'Simpan';
$_['ms_button_cancel'] = 'Batal';
$_['ms_button_select_predefined_avatar'] = 'Pilih Pra-definisi avatar';

$_['ms_button_select_image'] = 'Pilih gambar';
$_['ms_button_select_images'] = 'Pilih gambar';
$_['ms_button_select_files'] = 'Pilih berkas';

$_['ms_transaction_order_created'] = 'Pesanan yang telah dibuat';
$_['ms_transaction_order'] = 'Penjualan: Order Id #%s';
$_['ms_transaction_sale'] = 'Penjualan: %s';
$_['ms_transaction_refund'] = 'Pembayaran kembali: %s';
$_['ms_transaction_refund_shipping'] = 'Pembayaran kembali biaya pengiriman';
$_['ms_transaction_listing'] = 'Daftar produk: %s (%s)';
$_['ms_transaction_signup']      = 'Biaya daftar di %s';
$_['ms_transaction_shipping']      = 'Biaya pengiriman';
$_['ms_request_submitted'] = 'Permintaan anda telah diajukan';

$_['ms_totals_line'] = '%s penjual saat ini dan produk %s untuk penjualan!';

$_['ms_text_welcome'] = '<a href="%s">Login</a> | <a href="%s">Buat akun</a> | <a href="%s">Buat akun penjual</a>.';
$_['ms_button_register_seller'] = 'Daftar sebagai penjual';
$_['ms_register_seller_account'] = 'Daftar sebagai akun penjual';

// Mails

// Seller
$_['ms_mail_greeting'] = "Hallo %s,\n\n";
$_['ms_mail_greeting_no_name'] = "Hallo,\n\n";
$_['ms_mail_ending'] = "\n\nSalam,\n%s";
$_['ms_mail_message'] = "\n\nPesan:\n%s";

$_['ms_mail_subject_seller_account_created'] = 'Akun penjual telah dibuat';
$_['ms_mail_seller_account_created'] = <<<EOT
Akun pejual anda pada %s telah dibuat!

Sekarang anda dapat memulai dapat memasukkan produk anda.
EOT;

$_['ms_mail_subject_seller_account_created_by_admin'] = 'Akun penjual telah dibuat';
$_['ms_mail_seller_account_created_by_admin'] = <<<EOT
Akun pejual anda pada %s telah dibuat!

Harap klik tautan berikut untuk mengganti password.

%s

Anda tidak dapat memulai memasukkan produk sebelum anda mengganti password.
EOT;

$_['ms_mail_subject_seller_account_awaiting_moderation'] = 'Akun penjual menunggu proses moderasi';
$_['ms_mail_seller_account_awaiting_moderation'] = <<<EOT
Akun penjual anda pada %s telah dibuat dan sekarang menunggu proses moderasi.

Anda akan mendapatkan email segera setelah disetujui oleh admin.
EOT;

$_['ms_mail_subject_product_awaiting_moderation'] = 'Produk menunggu proses moderasi';
$_['ms_mail_product_awaiting_moderation'] = <<<EOT
Produk anda %s di %s sedang menunggu proses moderasi.

Anda akan menerima email segera setelah diproses oleh admin.
EOT;

$_['ms_mail_subject_product_purchased'] = 'Pesanan Baru';
$_['ms_mail_product_purchased'] = <<<EOT
Produk anda telah dibeli oleh %s.

Pelanggan: %s (%s)

Produk:
%s
Total: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
Produk anda telah dibeli oleh %s.

Pelanggan: %s

Produk:
%s
Total: %s
EOT;

$_['ms_mail_subject_seller_contact'] = 'Pesan baru dari pelanggan';
$_['ms_mail_seller_contact'] = <<<EOT
Anda mendapat pesan baru dari pelanggan!

Nama: %s

Email: %s

Produk: %s

Pesan:
%s
EOT;

$_['ms_mail_seller_contact_no_mail'] = <<<EOT
Anda mendapat pesan baru dari pelanggan!

Nama: %s

Produk: %s

Pesan:
%s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
Alamat pengiriman:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'Komentar: %s';

$_['ms_mail_subject_withdraw_request_submitted'] = 'Permohonan pembayaran telah diajukan';
$_['ms_mail_withdraw_request_submitted'] = <<<EOT
Kami telah menerima permohonan pembayaran anda. Anda akan menerima pendapatan segera setelah proses selesai.
EOT;

$_['ms_mail_subject_withdraw_request_completed'] = 'Pembayaran selesai';
$_['ms_mail_withdraw_request_completed'] = <<<EOT
Permohonan pembayaran anda telah diproses. Anda akan menerima pendapatan segera.
EOT;

$_['ms_mail_subject_withdraw_request_declined'] = 'Permohonan pembayaran dibatalkan';
$_['ms_mail_withdraw_request_declined'] = <<<EOT
Permohonan pembayaran anda telah dibatalkan. Nominal pembayaran anda akan dikembalikan ke saldo anda %s.
EOT;

$_['ms_mail_subject_transaction_performed'] = 'Transaksi baru';
$_['ms_mail_transaction_performed'] = <<<EOT
Transaksi baru telah ditambahkan ke akun anda pada %s.
EOT;

$_['ms_mail_subject_remind_listing'] = 'Daftar produk selesai';
$_['ms_mail_seller_remind_listing'] = <<<EOT
Daftar produk %s Anda telah selesai. Pergi ke daerah penjual account Anda jika Anda ingin daftar ulang produk.
EOT;

// *********
// * Admin *
// *********
$_['ms_mail_admin_subject_seller_account_created'] = 'Akun penjual yang baru telah dibuat';
$_['ms_mail_admin_seller_account_created'] = <<<EOT
Akun penjual baru di %s telah dibuat!
Nama penjual: %s (%s)
E-mail: %s
EOT;

$_['ms_mail_admin_subject_seller_account_awaiting_moderation'] = 'Akun penjual baru menunggu moderasi';
$_['ms_mail_admin_seller_account_awaiting_moderation'] = <<<EOT
Akun penjual baru di %s telah dibuat dan sekarang sedang menunggu proses moderasi.
Nama penjual: %s (%s)
E-mail: %s

Anda dapat memprosesnya di bagian Multiseller - Sellers di back office.
EOT;

$_['ms_mail_admin_subject_product_created'] = 'Produk baru ditambahkan';
$_['ms_mail_admin_product_created'] = <<<EOT
Produk baru %s telah ditambahkan di %s.

Anda dapat melihat atau mengubahnya di back office.
EOT;

$_['ms_mail_admin_subject_new_product_awaiting_moderation'] = 'Produk baru menunggu proses moderasi';
$_['ms_mail_admin_new_product_awaiting_moderation'] = <<<EOT
Produk baru %s telah ditambahkan di %s dan sedang menunggu proses moderasi.

Anda dapat memprosesnya di bagian Multiseller - Products di back office.
EOT;

$_['ms_mail_admin_subject_edit_product_awaiting_moderation'] = 'Produk telah diubah dan menunggu proses moderasi';
$_['ms_mail_admin_edit_product_awaiting_moderation'] = <<<EOT
Produk %s di %s telah diubah dan sedang menunggu proses moderasi.

Anda dapat memprosesnya di bagian Multiseller - Products di back office.
EOT;

$_['ms_mail_admin_subject_withdraw_request_submitted'] = 'Permohonan pembayaran menunggu proses moderasi';
$_['ms_mail_admin_withdraw_request_submitted'] = <<<EOT
Permohonan pembayaran baru telah disampaikan.

Anda dapat memprosesnya di bagian Multiseller - Finances di back office.
EOT;

// Success
$_['ms_success_product_published'] = 'Produk dipublikasikan';
$_['ms_success_product_unpublished'] = 'Produk yang tidak dipublikasikan';
$_['ms_success_product_created'] = 'Produk dibuat';
$_['ms_success_product_updated'] = 'Produk diperbarui';
$_['ms_success_product_deleted'] = 'Produk dihapus';

// Errors
$_['ms_error_sellerinfo_nickname_empty'] = 'Nama panggilan tidak boleh kosong';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'Nama panggilan hanya berisikan simbol alfanumerik';
$_['ms_error_sellerinfo_nickname_utf8'] = 'Nama panggilan hanya berisikan printable UTF-8 symbols';
$_['ms_error_sellerinfo_nickname_latin'] = 'Nama panggilan hanya berisikan simbol alfanumerik dan diakritik';
$_['ms_error_sellerinfo_nickname_length'] = 'Nama panggilan harus berisikan antara 4 sampai 50 karakter';
$_['ms_error_sellerinfo_nickname_taken'] = 'Nama panggilan ini sudah diambil';
$_['ms_error_sellerinfo_company_length'] = 'Nama perusahaan tidak lebih dari 50 karakter';
$_['ms_error_sellerinfo_description_length'] = 'Deskripsi tidak lebih dari 1000 karakter';
$_['ms_error_sellerinfo_paypal'] = 'Alamat Paypal tidak valid';
$_['ms_error_sellerinfo_terms'] = 'Peringatan: Anda harus setuju untuk %s!';
$_['ms_error_file_extension'] = 'Ekstensi tidak valid';
$_['ms_error_file_type'] = 'Tipe berkas tidak valid';
$_['ms_error_file_size'] = 'Ukuran berkas terlalu besar';
$_['ms_error_image_too_small'] = 'Ukuran dimensi berkas gambar terlalu kecil. Ukuran minimum: %s x %s (Width x Height)';
$_['ms_error_image_too_big'] = 'Ukuran dimensi berkas gambar terlalu besar. Ukuran maksimum: %s x %s (Width x Height)';
$_['ms_error_file_upload_error'] = 'Kesalahan unggah berkas';
$_['ms_error_form_submit_error'] = 'Kesalahan terjadi ketika mengirimkan formulir. Silahkan hubungi pemilik toko untuk informasi lebih lanjut.';
$_['ms_error_form_notice'] = 'Silakan periksa semua tab dari bentuk kesalahan.';
$_['ms_error_product_name_empty'] = 'Nama produk tidak boleh kosong';
$_['ms_error_product_name_length'] = 'Nama produk harus berisikan %s sampai %s karakter';
$_['ms_error_product_description_empty'] = 'Deskripsi produk tidak boleh kosong';
$_['ms_error_product_description_length'] = 'Deskripsi produk harus berisikan %s sampai %s karakter';
$_['ms_error_product_tags_length'] = 'Baris terlalu panjang';
$_['ms_error_product_price_empty'] = 'Silahkan tentukan harga untuk produk anda';
$_['ms_error_product_price_invalid'] = 'Harga tidak valid';
$_['ms_error_product_price_low'] = 'Harga terlalu rendah';
$_['ms_error_product_price_high'] = 'Harga terlalu tinggi';
$_['ms_error_product_category_empty'] = 'Silahkan pilih kategori';
$_['ms_error_product_model_empty'] = 'Model produk tidak boleh kosong';
$_['ms_error_product_model_length'] = 'Model produk harus berisikan %s sampai %s karakter';
$_['ms_error_product_code_empty'] = 'Kode produk tidak boleh kosong';
$_['ms_error_product_code_length'] = 'Kode produk harus berisikan %s sampai %s karakter';
$_['ms_error_product_sku_empty'] = 'Nomor SKU produk tidak boleh kosong';
$_['ms_error_product_sku_taken'] = 'Nomor SKU produk sudah diambil';
$_['ms_error_product_sku_length'] = 'Nomor SKU produk harus berisikan %s sampai %s karakter';
$_['ms_error_product_image_count'] = 'Harap unggah setidaknya %s gambar untuk produk anda';
$_['ms_error_product_download_count'] = 'Harap masukkan setidaknya %s unduhan untuk produk anda';
$_['ms_error_product_image_maximum'] = 'Tidak lebih dari %s gambar diijinkan';
$_['ms_error_product_download_maximum'] = 'Tidak lebih dari %s unduhan diijinkan';
$_['ms_error_product_message_length'] = 'Pesan tidak lebih dari 1000 karakter';
$_['ms_error_product_attribute_required'] = 'Atribut ini dibutuhkan';
$_['ms_error_product_attribute_long'] = 'Nilai ini tidak boleh lebih dari %s simbol';
$_['ms_error_withdraw_amount'] = 'Jumlah tidak valid';
$_['ms_error_withdraw_balance'] = 'Saldo anda tidak mencukupi';
$_['ms_error_withdraw_minimum'] = 'Tidak dapat menarik kurang dari batas minimum';
$_['ms_error_contact_email'] = 'Silahkan tentukan alamat email yang valid';
$_['ms_error_contact_captcha'] = 'Captcha code tidak valid';
$_['ms_error_contact_text'] = 'Pesan tidak lebih dari 2000 karakter';
$_['ms_error_contact_allfields'] = 'Silahkan isi semua kolom';
$_['ms_error_invalid_quantity_discount_priority'] = 'Kesalahan di kolom prioritas - Silahkan masukkan nilai yang benar';
$_['ms_error_invalid_quantity_discount_quantity'] = 'Kuantitas harus berisi 2 atau lebih';
$_['ms_error_invalid_quantity_discount_price'] = 'Diskon kuantitas harga yang diisikan tidak valid';
$_['ms_error_invalid_quantity_discount_dates'] = 'Kolom tanggal untuk diskon kuantitas harus diisi';
$_['ms_error_invalid_special_price_priority'] = 'Kesalahan di kolom prioritas - Silahkan masukkan nilai yang benar';
$_['ms_error_invalid_special_price_price'] = 'Harga khusus yang dimasukkan tidak valid';
$_['ms_error_invalid_special_price_dates'] = 'Kolom tanggal untuk harga spesial harus diisi';
$_['ms_error_seller_product'] = 'Anda tidak dapat menambahkan produk anda ke dalam cart';

// Account - General
$_['ms_account_unread_pm'] = 'Anda mendapatkan pesan pribadi yang belum terbaca';
$_['ms_account_unread_pms'] = 'Anda mendapatkan %s pesan pribadi yang belum terbaca';
$_['ms_account_register_new'] = 'Penjual Baru';
$_['ms_account_register_seller'] = 'Daftar akun penjual';
$_['ms_account_register_seller_note'] = 'Buat akun penjual dan mulai menjual produk Anda di toko kami!';
$_['ms_account_register_details'] = 'Detil Anda';
$_['ms_account_register_profiles'] = 'Profil Anda';
$_['ms_account_register_payment'] = 'Detil Pembayaran';
$_['ms_account_register_seller_success_heading'] = 'Akun Penjual Anda Telah Dibuat!';
$_['ms_account_register_seller_success_message']  = '<p>Selamat datang di %s!</p> <p>Selamat! Rekening penjual baru Anda telah berhasil dibuat!</p> <p>Anda sekarang dapat mengambil keuntungan dari hak istimewa penjual dan mulai menjual produk Anda dengan kami.</p> <p>Jika anda memiliki masalah, <a href="%s">hubungi kami</a>.</p>';
$_['ms_account_register_seller_success_approval'] = '<p>Selamat datang di %s!</p> <p>Rekening penjual Anda telah terdaftar dan sedang menunggu persetujuan. Anda akan diberitahu melalui email setelah account Anda telah diaktifkan oleh pemilik toko.</p><p>If you have any problems, <a href="%s">hubungi kami</a>.</p>';

$_['ms_seller'] = 'Penjual';
$_['ms_seller_forseller'] = 'Untuk Penjual';
$_['ms_account_dashboard'] = 'Dashboard';
$_['ms_account_seller_account'] = 'Akun Penjual';
$_['ms_account_customer_account'] = 'Akun Pelanggan';
$_['ms_account_sellerinfo'] = 'Profil Penjual';
$_['ms_account_sellerinfo_new'] = 'Akun baru penjual';
$_['ms_account_newproduct'] = 'Produk baru';
$_['ms_account_products'] = 'Produk';
$_['ms_account_transactions'] = 'Transaksi';
$_['ms_account_orders'] = 'Pesanan';
$_['ms_account_withdraw'] = 'Permohonon pembayaran';
$_['ms_account_stats'] = 'Statistik';

// Account - New product
$_['ms_account_newproduct_heading'] = 'Produk Baru';
$_['ms_account_newproduct_breadcrumbs'] = 'Produk Baru';
//General Tab
$_['ms_account_product_tab_general'] = 'Umum';
$_['ms_account_product_tab_specials'] = 'Harga spesial';
$_['ms_account_product_tab_discounts'] = 'Diskon kuantitas';
$_['ms_account_product_name_description'] = 'Nama & Deskripsi';
$_['ms_account_product_name'] = 'Nama';
$_['ms_account_product_name_note'] = 'Tentukan nama produk anda';
$_['ms_account_product_description'] = 'Deskripsi';
$_['ms_account_product_description_note'] = 'Deskripsikan produk anda';
$_['ms_account_product_meta_description'] = 'Deskripsi Meta Tag';
$_['ms_account_product_meta_description_note'] = 'Tentukan deskripsi meta tag produk anda';
$_['ms_account_product_meta_keyword'] = 'Kata Kunci Meta Tag';
$_['ms_account_product_meta_keyword_note'] = 'Tentukan kata kunci meta tag produk anda';
$_['ms_account_product_tags'] = 'Tags';
$_['ms_account_product_tags_note'] = 'Tentukan tags produk anda.';
$_['ms_account_product_price_attributes'] = 'Harga & Atribut';
$_['ms_account_product_price'] = 'Harga';
$_['ms_account_product_price_note'] = 'Tentukan harga produk anda';
$_['ms_account_product_listing_flat'] = 'Biaya pencantuman produk ini adalah <span>%s</span>';
$_['ms_account_product_listing_percent'] = 'Biaya pencantuman produk ini berdasarkan harga produk. Biaya pencantuman saat ini: <span>%s</span>.';
$_['ms_account_product_listing_balance'] = 'Jumlah ini akan dipotong dari saldo penjual Anda.';
$_['ms_account_product_listing_paypal'] = 'Anda akan diarahkan ke halaman pembayaran PayPal setelah memasukkan produk.';
$_['ms_account_product_listing_itemname'] = 'Biaya pencantuman produk di %s';
$_['ms_account_product_listing_until'] = 'Produk ini akan didaftarkan hingga %s';
$_['ms_account_product_category'] = 'Kategori';
$_['ms_account_product_category_note'] = 'Pilih kategori untuk produk anda';
$_['ms_account_product_enable_shipping'] = 'Aktifkan pengiriman';
$_['ms_account_product_enable_shipping_note'] = 'Tentukan apakah produk Anda membutuhkan pengiriman';
$_['ms_account_product_quantity'] = 'Kuantitas';
$_['ms_account_product_quantity_note']    = 'Tentukan kuantitas produk anda';
$_['ms_account_product_files'] = 'Berkas';
$_['ms_account_product_download'] = 'Unduh';
$_['ms_account_product_download_note'] = 'Unggah berkas untuk produk anda. Ekstensi yang diperbolehkan: %s';
$_['ms_account_product_push'] = 'Memberikan perubahan ke pelanggan sebelumnya';
$_['ms_account_product_push_note'] = 'Yang baru ditambahkan dan unduhan terbaru akan tersedia untuk pelanggan sebelumnya';
$_['ms_account_product_image'] = 'Gambar';
$_['ms_account_product_image_note'] = 'Pilih gambar untuk produk anda. Gambar pertama akan digunakan sebagai gambar awal. Anda dapat merubah urutan gambar dengan menggesernya. Ekstensi yang diperbolehkan: %s';
$_['ms_account_product_message_reviewer'] = 'Pesan untuk peninjau';
$_['ms_account_product_message'] = 'Pesan';
$_['ms_account_product_message_note'] = 'Pesan anda untuk peninjau';
//Data Tab
$_['ms_account_product_tab_data'] = 'Data';
$_['ms_account_product_model'] = 'Model';
$_['ms_account_product_code'] = 'Kode Produk';
$_['ms_account_product_sku'] = 'SKU';
$_['ms_account_product_sku_note'] = 'Stock Keeping Unit';
$_['ms_account_product_upc']  = 'UPC';
$_['ms_account_product_upc_note'] = 'Universal Product Code';
$_['ms_account_product_ean'] = 'EAN';
$_['ms_account_product_ean_note'] = 'European Article Number';
$_['ms_account_product_jan'] = 'JAN';
$_['ms_account_product_jan_note'] = 'Japanese Article Number';
$_['ms_account_product_isbn'] = 'ISBN';
$_['ms_account_product_isbn_note'] = 'International Standard Book Number';
$_['ms_account_product_mpn'] = 'MPN';
$_['ms_account_product_mpn_note'] = 'Manufacturer Part Number';
$_['ms_account_product_manufacturer'] = 'Manufaktur';
$_['ms_account_product_manufacturer_note'] = '(Autocomplete)';
$_['ms_account_product_tax_class'] = 'Kelas Pajak';
$_['ms_account_product_date_available'] = 'Tanggal Tersedia';
$_['ms_account_product_stock_status'] = 'Status Stok Habis';
$_['ms_account_product_stock_status_note'] = 'Status ditampilkan ketika stok produk habis';
$_['ms_account_product_subtract'] = 'Pengurangan Stok';

// Options
$_['ms_account_product_tab_options'] = 'Opsi';
$_['ms_options_add'] = '+ Tambahkan opsi';
$_['ms_options_add_value'] = '+ Tambahkan harga';
$_['ms_options_required'] = 'Buat opsi yang dibutuhkan';
$_['ms_options_price_prefix'] = 'Ubah awalan harga';
$_['ms_options_price'] = 'Harga...';
$_['ms_options_quantity'] = 'Kuantitas...';


$_['ms_account_product_manufacturer'] = 'Manufaktur';
$_['ms_account_product_manufacturer_note'] = '(Autocomplete)';
$_['ms_account_product_tax_class'] = 'Kelas Pajak';
$_['ms_account_product_date_available'] = 'Tanggal Tersedia';
$_['ms_account_product_stock_status'] = 'Status Stok Habis';
$_['ms_account_product_stock_status_note'] = 'Status ditampilkan ketika stok produk habis';
$_['ms_account_product_subtract'] = 'Pengurangan Stok';

$_['ms_account_product_priority'] = 'Prioritas';
$_['ms_account_product_date_start'] = 'Tanggal mulai';
$_['ms_account_product_date_end'] = 'Tanggal selesai';
$_['ms_account_product_sandbox'] = 'Peringatan: Metode pembayaran di \'Sandbox mode \'. Akun Anda tidak akan dikenakan biaya.';



// Account - Edit product
$_['ms_account_editproduct_heading'] = 'Ubah Produk';
$_['ms_account_editproduct_breadcrumbs'] = 'Ubah Produk';

// Account - Clone product
$_['ms_account_cloneproduct_heading'] = 'Klon Produk';
$_['ms_account_cloneproduct_breadcrumbs'] = 'Klon Produk';

// Account - Relist product
$_['ms_account_relist_product_heading'] = 'Daftar Ulang Produk';
$_['ms_account_relist_product_breadcrumbs'] = 'Daftar Ulang Produk';

// Account - Seller
$_['ms_account_sellerinfo_heading'] = 'Profil Penjual';
$_['ms_account_sellerinfo_breadcrumbs'] = 'Profil Penjual';
$_['ms_account_sellerinfo_nickname'] = 'Panggilan';
$_['ms_account_sellerinfo_nickname_note'] = 'Tentukan nama panggilan penjual anda.';
$_['ms_account_sellerinfo_phone'] = 'Telepon';
$_['ms_account_sellerinfo_phone_note'] = 'Nomor telepon anda.';
$_['ms_account_sellerinfo_description'] = 'Deskripsi';
$_['ms_account_sellerinfo_description_note'] = 'Deskripsikan diri anda';
$_['ms_account_sellerinfo_account_address'] = 'Alamat';
$_['ms_account_sellerinfo_account_address_note'] = 'Alamat Anda';
$_['ms_account_sellerinfo_account_name'] = 'Nama Akun';
$_['ms_account_sellerinfo_account_name_note'] = 'Nama Akun Anda';
$_['ms_account_sellerinfo_account_number'] = 'Nomor Akun';
$_['ms_account_sellerinfo_account_number_note'] = 'Nomor Akun Anda';
$_['ms_account_sellerinfo_account_npwp_number'] = 'Nomor NPWP';
$_['ms_account_sellerinfo_account_npwp_number_note'] = 'Nomor NPWP Anda';
$_['ms_account_sellerinfo_account_npwp_address'] = 'Alamat NPWP';
$_['ms_account_sellerinfo_account_npwp_address_note'] = 'Alamat NPWP Anda';
$_['ms_account_sellerinfo_account_pod_transfer'] = 'Bukti Transfer';
$_['ms_account_sellerinfo_account_pod_transfer_note'] = 'Bukti Transfer Anda';
$_['ms_account_sellerinfo_account'] = 'Nama Bank';
$_['ms_account_sellerinfo_account_note'] = 'Nama Bank';
$_['ms_account_sellerinfo_account_select'] = 'Pilih Bank';
$_['ms_account_sellerinfo_company'] = 'Perusahaan';
$_['ms_account_sellerinfo_company_note'] = 'Perusahaaan anda(opsional)';
$_['ms_account_sellerinfo_postcode'] = 'Kode Pos';
$_['ms_account_sellerinfo_postcode_note'] = 'Kode Pos Anda';
$_['ms_account_sellerinfo_logistic'] = 'Logistik';
$_['ms_account_sellerinfo_logistic_note'] = 'Layanan logistik anda';
$_['ms_account_sellerinfo_logistic_select_all'] = 'Pilih Semua';
$_['ms_account_sellerinfo_logistic_unselect_all'] = 'Hapus Semua';
$_['ms_account_sellerinfo_country'] = 'Negara';
$_['ms_account_sellerinfo_country_dont_display'] = 'Jangan tampilkan kenegaraan saya';
$_['ms_account_sellerinfo_country_note'] = 'Pilih Negara Anda.';
$_['ms_account_sellerinfo_zone'] = 'Provinsi';
$_['ms_account_sellerinfo_zone_select'] = 'Pilih Provinsi';
$_['ms_account_sellerinfo_zone_not_selected'] = 'Tidak ada provinsi yang dipilih';
$_['ms_account_sellerinfo_zone_note'] = 'Pilih provinsi anda dari daftar.';
$_['ms_account_sellerinfo_city'] = 'Kota / Kabupaten';
$_['ms_account_sellerinfo_city_select'] = 'Pilih Kota / Kabupaten';
$_['ms_account_sellerinfo_city_not_selected'] = 'Tidak ada kota / kabupaten yang dipilih';
$_['ms_account_sellerinfo_city_note'] = 'Pilih kota / kabupaten anda dari daftar.';
$_['ms_account_sellerinfo_district'] = 'Kecamatan';
$_['ms_account_sellerinfo_district_select'] = 'Pilih Kecamatan';
$_['ms_account_sellerinfo_district_not_selected'] = 'Tidak ada kecamatan yang dipilih';
$_['ms_account_sellerinfo_district_note'] = 'Pilih kecamatan anda dari daftar.';
$_['ms_account_sellerinfo_subdistrict'] = 'Kelurahan';
$_['ms_account_sellerinfo_subdistrict_select'] = 'Pilih Kelurahan';
$_['ms_account_sellerinfo_subdistrict_not_selected'] = 'Tidak ada kelurahan yang dipilih';
$_['ms_account_sellerinfo_subdistrict_note'] = 'Pilih kelurahan anda dari daftar.';
$_['ms_account_sellerinfo_avatar'] = 'Avatar';
$_['ms_account_sellerinfo_avatar_note'] = 'Pilih avatar anda';
$_['ms_account_sellerinfo_banner'] = 'Banner';
$_['ms_account_sellerinfo_banner_note'] = 'Upload spanduk yang akan ditampilkan di halaman profil anda';
$_['ms_account_sellerinfo_paypal'] = 'Paypal';
$_['ms_account_sellerinfo_paypal_note'] = 'Tentukan alamat PayPal anda';
$_['ms_account_sellerinfo_reviewer_message'] = 'Pesan anda ke resensi';
$_['ms_account_sellerinfo_reviewer_message_note'] = 'YPesan anda ke resensi';
$_['ms_account_sellerinfo_terms'] = 'Setujui syarat';
$_['ms_account_sellerinfo_terms_note'] = 'Saya telah membaca dan menyetujui pada <a class="agree" href="%s" alt="%s"><b>%s</b></a>';
$_['ms_account_sellerinfo_fee_flat'] = 'Ada pengungsi signup <span>%s</span> untuk mebjadi penjual pada %s.';
$_['ms_account_sellerinfo_fee_balance'] = 'Nominal ini akan dikurangi sesuai dengan saldo akhir.';
$_['ms_account_sellerinfo_fee_paypal'] = 'Anda akan dilempar ke halaman pembayaran Paypal setelah mengenal form.';
$_['ms_account_sellerinfo_signup_itemname'] = 'Akun penjual yang menangani registrasi akun pada %s';
$_['ms_account_sellerinfo_saved'] = 'Akun penjual telah tersimpan.';

$_['ms_account_status'] = 'Status akun penjual anda saat ini: ';
$_['ms_account_status_tobeapproved'] = '<br />Anda akan dapat menggunakan akun anda segera setelah akun anda disetujui oleh pemilik toko.';
$_['ms_account_status_please_fill_in'] = '<br />Silahkan mengisi semua form untuk membuat akun penjual.';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'Aktif';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'Tidak aktif';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'Tidak diguanakan';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'Belum selesai';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'Dihapus';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'Hutang biaya pendaftaran';

// Account - Products
$_['ms_account_products_heading'] = 'Produk Anda';
$_['ms_account_products_breadcrumbs'] = 'Produk Anda';
$_['ms_account_products_image'] = 'Gambar';
$_['ms_account_products_product'] = 'Produk';
$_['ms_account_products_sales'] = 'Penjualan';
$_['ms_account_products_earnings'] = 'Pendapatan';
$_['ms_account_products_status'] = 'Status';
$_['ms_account_products_date'] = 'Tanggal ditambahkan';
$_['ms_account_products_listing_until'] = 'Daftar hingga';
$_['ms_account_products_action'] = 'Aksi';
$_['ms_account_products_noproducts'] = 'Anda belum mempunyai produk!';
$_['ms_account_products_confirmdelete'] = 'Anda yakin untuk menghapus produk anda?';

$_['ms_not_defined'] = 'Tidak terdefinisi';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'Aktif';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'Tidak aktif';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'Tidak digunakan';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'Terhapus';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'Biaya daftar yang belum terbayar';
$_['ms_product_status_' . MsProduct::STATUS_ON_REVIEW] = 'Proses Review';

// Account - Conversations and Messages
$_['ms_account_conversations'] = 'Percakapan';
$_['ms_account_messages'] = 'Pesan';

$_['ms_account_conversations_heading'] = 'Percakapan Anda';
$_['ms_account_conversations_breadcrumbs'] = 'Percakapan Anda';

$_['ms_account_conversations_status'] = 'Status';
$_['ms_account_conversations_date_created'] = 'Tanggal dibuat';
$_['ms_account_conversations_with'] = 'Percakapan dengan';
$_['ms_account_conversations_title'] = 'Judul';

$_['ms_conversation_title_product'] = 'Pertanyaan tentang produk: %s';
$_['ms_conversation_title'] = 'Pertanyaan dari %s';

$_['ms_account_conversations_read'] = 'Sudah terbaca';
$_['ms_account_conversations_unread'] = 'Belum terbaca';

$_['ms_account_messages_heading'] = 'Pesan';
$_['ms_account_messages_breadcrumbs'] = 'Pesan';

$_['ms_message_text'] = 'Pesan anda';
$_['ms_post_message'] = 'Kirim pesan';

$_['ms_customer_does_not_exist'] = 'Akun pelanggan telah terhapus';
$_['ms_error_empty_message'] = 'Pesan tidak dapat anda kosongkan';

$_['ms_mail_subject_private_message'] = 'Anda menerima pesan pribadi baru';
$_['ms_mail_private_message'] = <<<EOT
Anda mendapat pesan pribadi baru dari %s!

%s

%s

Anda dapat membalas pesan ini di akun anda.
EOT;

$_['ms_mail_subject_order_updated'] = 'Pesanan anda #%s telah diperbarui oleh %s';
$_['ms_mail_order_updated'] = <<<EOT
Pesanan anda pada %s telah diperbarui oleh %s:

Pesanan#: %s

Produk:
%s

Status: %s

Komentar:
%s

EOT;

$_['ms_mail_subject_seller_vote'] = 'Vote untuk penjual';
$_['ms_mail_seller_vote_message'] = 'Vote untuk penjual';

// Account - Transactions
$_['ms_account_transactions_heading'] = 'Keuangan Anda';
$_['ms_account_transactions_breadcrumbs'] = 'Keuangan Anda';
$_['ms_account_transactions_balance'] = 'Saldo anda saat ini';
$_['ms_account_transactions_earnings'] = 'Pendapatan anda sampai saat ini';
$_['ms_account_transactions_records'] = 'Arsip saldo';
$_['ms_account_transactions_description'] = 'Deskripsi';
$_['ms_account_transactions_amount'] = 'Jumlah';
$_['ms_account_transactions_notransactions'] = 'Anda tidak mempunyai transaksi saat ini!';

// Payments
$_['ms_payment_payments'] = 'Pembayaran';
$_['ms_payment_order'] = 'pesanan #%s';
$_['ms_payment_type_' . MsPayment::TYPE_SIGNUP] = 'Biaya masuk';
$_['ms_payment_type_' . MsPayment::TYPE_LISTING] = 'Biaya daftar';
$_['ms_payment_type_' . MsPayment::TYPE_PAYOUT] = 'Pembayaran manual';
$_['ms_payment_type_' . MsPayment::TYPE_PAYOUT_REQUEST] = 'Permintaan pembayaran';
$_['ms_payment_type_' . MsPayment::TYPE_RECURRING] = 'Pembayaran berkala';
$_['ms_payment_type_' . MsPayment::TYPE_SALE] = 'Penjualan';

$_['ms_payment_status_' . MsPayment::STATUS_UNPAID] = 'Belum dibayar';
$_['ms_payment_status_' . MsPayment::STATUS_PAID] = 'Sudah dibayar';

// Account - Orders
$_['ms_account_orders_heading'] = 'Pesanan Anda';
$_['ms_account_orders_breadcrumbs'] = 'Pesanan Anda';
$_['ms_account_orders_id'] = 'Pesanan #';
$_['ms_account_orders_customer'] = 'Pelanggan';
$_['ms_account_orders_products'] = 'Produk';
$_['ms_account_orders_history'] = 'Sejarah';
$_['ms_account_orders_addresses'] = 'Alamat';
$_['ms_account_orders_total'] = 'Jumlah';
$_['ms_account_orders_view'] = 'Lihat pesanan';
$_['ms_account_orders_noorders'] = 'Anda tidak mempunyai pesanan!';
$_['ms_account_orders_nohistory'] = 'Tidak ada sejarah pesanan!';
$_['ms_account_orders_change_status']    = 'Rubah status pesanan';
$_['ms_account_orders_add_comment']    = 'Berikan komentar pesanan...';

$_['ms_account_order_information'] = 'Informasi Pesanan';

// Account - Dashboard
$_['ms_account_dashboard_heading'] = 'Dasbor Penjual';
$_['ms_account_dashboard_breadcrumbs'] = 'Dasbor Penjual';
$_['ms_account_dashboard_orders'] = 'Pesanan Terakhir';
$_['ms_account_dashboard_overview'] = 'Tinjauan';
$_['ms_account_dashboard_seller_group'] = 'Grup penjual';
$_['ms_account_dashboard_listing'] = 'Daftar biaya';
$_['ms_account_dashboard_sale'] = 'Biaya Penjualan';
$_['ms_account_dashboard_royalty'] = 'Honor';
$_['ms_account_dashboard_stats'] = 'Status';
$_['ms_account_dashboard_balance'] = 'Saldo saat ini';
$_['ms_account_dashboard_total_sales'] = 'Total penjualan';
$_['ms_account_dashboard_total_earnings'] = 'Total pendapatan';
$_['ms_account_dashboard_sales_month'] = 'Penjualan bulan ini';
$_['ms_account_dashboard_earnings_month'] = 'Pendapatan bulan ini';
$_['ms_account_dashboard_nav'] = 'Navigasi cepat';
$_['ms_account_dashboard_nav_profile'] = 'Ubah profil penjual';
$_['ms_account_dashboard_nav_product'] = 'Buat produk baru';
$_['ms_account_dashboard_upload_product'] = 'Unggah produk';
$_['ms_account_dashboard_nav_products'] = 'Kelola produk';
$_['ms_account_dashboard_nav_orders'] = 'Lihat pesanan';
$_['ms_account_dashboard_nav_balance'] = 'Lihat catatan keuangan';
$_['ms_account_dashboard_nav_payout'] = 'Permohonan pembayaran';

//edit by arin
$_['ms_account_dashboard_nav_pending_order'] = 'Lihat Pesanan Baru';
$_['ms_account_dashboard_nav_return_list'] = 'Lihat Daftar Pengembalian';

// Account - Request withdrawal
$_['ms_account_withdraw_heading'] = 'Permintaan Pembayaran';
$_['ms_account_withdraw_breadcrumbs'] = 'Permintaan Pembayaran';
$_['ms_account_withdraw_balance'] = 'Saldo anda saat ini:';
$_['ms_account_withdraw_deposit_fee'] = 'Deposit fee anda saat ini:';
$_['ms_account_withdraw_balance_available'] = 'Jumlah penarikan yang tersedia:';
$_['ms_account_withdraw_minimum'] = 'Jumlah minimum pembayaran:';
$_['ms_account_balance_reserved_formatted'] = '-%s penarikan tertunda';
$_['ms_account_balance_waiting_formatted'] = '-%s masa tunggu';
$_['ms_account_withdraw_description'] = 'Permintaan Permintaan menggunakan %s (%s) di %s';
$_['ms_account_withdraw_amount'] = 'Jumlah:';
$_['ms_account_withdraw_amount_note'] = 'Harap menentukan jumlah pembayaran';
$_['ms_account_withdraw_method'] = 'Metode pembayaran:';
$_['ms_account_withdraw_method_note'] = 'Silahkan pilih metode pembayaran';
$_['ms_account_withdraw_method_paypal'] = 'PayPal';
$_['ms_account_withdraw_method_transfer'] = 'Bank Transfer';
$_['ms_account_withdraw_all'] = 'Semua pendapatan yang sekarang tersedia';
$_['ms_account_withdraw_minimum_not_reached'] = 'Total saldo anda kurang dari jumlah pembayaran minimum!';
$_['ms_account_withdraw_no_funds'] = 'Tidak ada nominal uang untuk diambil.';
$_['ms_account_withdraw_no_paypal'] = 'Silahkan <a href="index.php?route=seller/account-profile">tentukan alamat Paypal anda</a> first!';

// Account - Stats
$_['ms_account_stats_heading'] = 'Statistik';
$_['ms_account_stats_breadcrumbs'] = 'Statistik';
$_['ms_account_stats_tab_summary'] = 'Rangkuman';
$_['ms_account_stats_tab_by_product'] = 'Produk';
$_['ms_account_stats_tab_by_year'] = 'Tahun';
$_['ms_account_stats_summary_comment'] = 'Below is a summary of your sales';
$_['ms_account_stats_sales_data'] = 'Data penjualan';
$_['ms_account_stats_number_of_orders'] = 'Jumlah order';
$_['ms_account_stats_total_revenue'] = 'Total pendapatan';
$_['ms_account_stats_average_order'] = 'Rata-rata order';
$_['ms_account_stats_statistics'] = 'Statistik';
$_['ms_account_stats_grand_total'] = 'Total penjualan keseluruhan';
$_['ms_account_stats_product'] = 'Produk';
$_['ms_account_stats_sold'] = 'Terjual';
$_['ms_account_stats_total'] = 'Total';
$_['ms_account_stats_this_year'] = 'Tahun ini';
$_['ms_account_stats_year_comment'] = '<span id="sales_num">%s</span> Penjualan untuk periode tertentu';
$_['ms_account_stats_show_orders'] = 'Tampilkan order dari: ';
$_['ms_account_stats_month'] = 'Bulan';
$_['ms_account_stats_num_of_orders'] = 'Jumlah order';
$_['ms_account_stats_total_r'] = 'Total pendapatan';
$_['ms_account_stats_average_order'] = 'Rata-rata order';
$_['ms_account_stats_today'] = 'Hari ini, ';
$_['ms_account_stats_yesterday'] = 'Kemarin, ';
$_['ms_account_stats_daily_average'] = 'Rata-rata harian untuk ';
$_['ms_account_stats_date_month_format'] = 'F Y';
$_['ms_account_stats_projected_totals'] = 'Total untuk ';
$_['ms_account_stats_grand_total_sales'] = 'Total penjualan keseluruhan';

// Product page - Seller information
$_['ms_catalog_product_sellerinfo'] = 'Informasi penjual';
$_['ms_catalog_product_contact'] = 'Hubungi penjual ini';

$_['ms_footer'] = '<br>MultiMerch Marketplace by <a href="http://multimerch.com/">multimerch.com</a>';

// Catalog - Sellers list
$_['ms_catalog_sellers_heading'] = 'Penjual';
$_['ms_catalog_sellers_country'] = 'Negara:';
$_['ms_catalog_sellers_website'] = 'Halaman web:';
$_['ms_catalog_sellers_company'] = 'Perusahaan:';
$_['ms_catalog_sellers_totalsales'] = 'Penjualan:';
$_['ms_catalog_sellers_totalproducts'] = 'Produk:';
$_['ms_sort_country_desc'] = 'Negara (Z - A)';
$_['ms_sort_country_asc'] = 'Negara (A - Z)';
$_['ms_sort_nickname_desc'] = 'Nama (Z - A)';
$_['ms_sort_nickname_asc'] = 'Nama (A - Z)';

// Catalog - Seller profile page
$_['ms_catalog_sellers'] = 'Sellers';
$_['ms_catalog_sellers_empty'] = 'Tidak ada penjual.';
$_['ms_catalog_seller_profile'] = 'Lihat profil';
$_['ms_catalog_seller_profile_heading'] = 'Profil %s';
$_['ms_catalog_seller_profile_breadcrumbs'] = 'Profil %s';
$_['ms_catalog_seller_profile_about_seller'] = 'Tentang penjual';
$_['ms_catalog_seller_profile_products'] = 'Beberapa produk penjual';
$_['ms_catalog_seller_profile_tab_products'] = 'Produk';

$_['ms_catalog_seller_profile_social'] = 'Profil sosial';
$_['ms_catalog_seller_profile_country'] = 'Negara:';
$_['ms_catalog_seller_profile_zone'] = 'Wilayah/Negara:';
$_['ms_catalog_seller_profile_website'] = 'Halaman web:';
$_['ms_catalog_seller_profile_company'] = 'Perusahaan:';
$_['ms_catalog_seller_profile_totalsales'] = 'Total penjualan:';
$_['ms_catalog_seller_profile_totalproducts'] = 'Total produk:';
$_['ms_rate_success'] = 'Transaksi Sukses dari';
$_['ms_transaction'] = 'Transaksi';
$_['ms_note_rate_success'] = 'Transaksi sukses adalah semua transaksi yang berhasil diselesaikan. Transaksi dapat dianggap gagal karena beberapa hal seperti: stok habis, ongkir tidak sesuai, dll.';
$_['ms_catalog_seller_profile_view_products'] = 'Lihat produk';
$_['ms_catalog_seller_profile_view'] = 'Lihat semua produk %s';

// Catalog - Seller's products list
$_['ms_catalog_seller_products_heading'] = 'Produk %s';
$_['ms_catalog_seller_products_breadcrumbs'] = 'Produk %s';
$_['ms_catalog_seller_products_empty'] = 'Penjual ini tidak mempunyai produk apapun!';

// Catalog - Seller contact dialog
$_['ms_sellercontact_title'] = 'Send a message to seller';
$_['ms_sellercontact_signin'] = 'Silahkan <a href="%s">masuk</a> untuk menghubungi %s';
$_['ms_sellercontact_sendto'] = 'Kirim pesan ke %s';
$_['ms_sellercontact_text'] = 'Pesan: ';
$_['ms_sellercontact_captcha'] = 'Captcha';
$_['ms_sellercontact_sendmessage'] = 'Kirim pesan ke %s';
$_['ms_sellercontact_close'] = 'Tutup';
$_['ms_sellercontact_send'] = 'Kirim';
$_['ms_sellercontact_success'] = 'Pesan anda telah berhasil terkirim';

/* Add by M*/
// Product Comments
$_['ms_account_productcomment_breadcrumbs'] = 'Comments';
$_['ms_account_productcomment_heading'] = 'Comments';
$_['ms_account_productcomment_error_comment_short'] = 'The comment body must be at least %s characters long';
$_['ms_account_productcomment_error_comment_long'] = 'The comment body cannot be longer than %s characters';
$_['ms_account_productcomment_text_comment']      = 'Comment';
$_['ms_account_productcomment_text_note']          = '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['ms_account_productcomment_button_continue']          = 'Submit';
$_['ms_account_productcomment_text_more']          = 'View More';
$_['ms_account_productcomment_text_post_comment']          = 'Post Comment';
$_['ms_account_productcomment_text_seller']          = 'Merchant';
$_['ms_account_productcomment_text_customer']          = 'Customer';
$_['pc_error_name'] = 'Please enter a name between %s and %s characters long';
$_['pc_error_email'] = 'Please enter a valid email';
$_['pc_error_comment_short'] = 'The comment body must be at least %s characters long';
$_['pc_error_comment_long'] = 'The comment body cannot be longer than %s characters';
$_['pc_error_captcha'] = 'Verification code does not match the image';
$_['pc_wait']         = 'Please Wait!';
$_['pc_success']      = 'Thank you for your comment.';
$_['pc_no_comments_yet'] = 'No comments added yet';


$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';

// edit by arin
// tab Pending order
$_['tab_new_order']           = 'Pesanan Baru';
$_['tab_confirm_shipping']    = 'Konfirmasi Pengiriman';
$_['tab_status_shipping']     = 'Status Pengiriman';
$_['tab_transaction_list']    = 'Daftar Transaksi';
$_['text_confirm_accept']     = 'Anda yakin untuk menerima semua pesanan yang dicentang ?';
$_['text_confirm_decline']    = 'Anda yakin untuk menolak pesanan ?';
$_['text_decline_reason']     = 'Alasan Penolakan';

// table shipping data
$_['text_buyer']    		  = 'Pembelian oleh ';
$_['text_buy_date']    		  = 'Tanggal transaksi: ';
$_['text_deadline_response']  = 'Jatuh tempo respon: ';
$_['text_deadline_days']  	  = ' hari lagi';
$_['text_response']    		  = 'Respon';
$_['text_accept']    		  = 'Terima Pesanan';
$_['text_decline']    		  = 'Tolak Pesanan';
$_['button_response']    	  = 'Respon Pesanan';
$_['button_print']    	 	  = 'Print';
$_['button_hide']    	 	  = 'Sembunyikan';
$_['button_show']    	 	  = 'Tampilkan';
$_['button_show_all']    	  = 'Tampilkan semua';
$_['button_hide_all']    	  = 'Sembunyikan semua';
$_['button_accept_all']    	  = 'Terima Pesanan Sekaligus';
$_['button_confirm_all']      = 'Konfirmasi Semua';
$_['button_confirm']    	  = 'Konfirmasi';
$_['button_cancel']    	  	  = 'Batal';
$_['button_save']    	  	  = 'Simpan';

$_['text_no_data']   		  = '<center><b>Tidak ada pesanan baru untuk dikonfirmasi</b></center>';
$_['text_no_data_pending']    = '<center><b>Tidak ada pesanan baru</b></center>';
$_['text_detail']   		  = 'Detail Transaksi';
$_['text_delivery_code']   	  = 'No. Resi Pengiriman';
$_['text_detail_order']   	  = 'Detail Pesanan';
$_['text_order_detail_id']    = 'Order Detail ID:';
$_['text_shipping_service']   = 'Jenis Pengiriman';
$_['text_days']   			  = ' hari';

//error warning
$_['error_response']   	  	  = 'Pilih respon';
$_['error_receipt_number']    = 'Nomor resi wajib diisi';
$_['error_shipping_service']    = 'Silahkan pilih vendor pengiriman';
$_['error_discuss']   		  = 'Panjang komentar minimal sepanjang 3 karakter';

$_['column_destination']      = 'Alamat Tujuan';
$_['column_quantity']         = 'Jumlah Barang';
$_['column_shipping_price']   = 'Ongkos Kirim';
$_['column_insurance']    	  = 'Biaya Asuransi';
$_['column_remarks']    	  = 'Keterangan';
$_['column_price']    		  = 'Harga Barang';
$_['column_payment_method']   = '<b>Pilihan metode pembayaran: </b>';
$_['column_total']    		  = 'Total Harga Per No. Struk: ';

//success response
$_['text_success_response']   = 'Respon telah ditambahkan';
$_['text_success_confirm']    = 'Konfirmasi pesanan berhasil';
$_['text_success_edit']    	  = 'Nomor resi berhasil diubah';

//table confirm order
$_['column_buyer']    	  	  = 'Pembelian oleh';
$_['column_shipping_info']    = 'Pengiriman';
$_['column_delivery']    	  = 'Agen Kurir';
$_['column_action']    	  	  = 'Aksi';

//order release
$_['text_search_invoice']     = 'Nama Penerima / No. Struk';
$_['text_search_all']     	  = 'No. Struk / Buyer Name / Receipt Number';
$_['text_choose']    	  	  = 'Pilih';
$_['text_deadline']    	  	  = 'Jatuh tempo: ';
$_['text_yes']    	  	  	  = 'Ya';
$_['text_no']    	  	  	  = 'Tidak';
$_['text_change_delivery']    = 'Perlu mengganti kurir?';
$_['text_receipt_number']     = 'Nomor Resi';
$_['text_tooltip']     		  = 'Total harga yang dibayarkan ke toko Anda';

//shipping status
$_['text_edit_receipt']       = 'Ubah No. Resi';
$_['text_track']     		  = 'Lacak';

//return list
$_['text_return_list'] 			= 'Daftar Pengembalian Barang';
$_['text_detail_return'] 		= 'Detail Return';
$_['text_info_return'] 			= 'Informasi Return';
$_['text_email'] 				= 'Email: ';
$_['text_email2'] 				= 'Email';
$_['text_product_info'] 		= 'Informasi Produk';
$_['text_search'] 				= 'Cari';
$_['text_accept_return']    	= 'Terima Pengembalian';
$_['text_complete_return']    	= 'Pengembalian Selesai';
$_['success_accept_return']    	= 'Penerimaan permohononan pengembalian sukses';
$_['success_change_action']    	= 'Tindakan pengembalian telah diubah';
$_['warning_user_confirm']    	= "Customer belum mengkonfirmasi perubahan tindakan";
$_['success_user_confirm']    	= "Customer sudah mengkonfirmasi perubahan tindakan";
$_['text_discuss']    			= 'Diskusi Permohononan Pengembalian';
$_['text_change_action']    	= 'Ubah Tindakan';
$_['text_sla_return']    		= ' hari tersisa untuk menerima permohonan pengembalian';

// Column
$_['column_return_id']   = 'No. Pengembalian';
$_['column_order_id']    = 'No. Pesanan';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Tanggal Ditambahkan';
$_['column_customer']    = 'Pelanggan';
$_['column_product']     = 'Nama Produk';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Jumlah';
$_['column_price']       = 'Harga';
$_['column_opened']      = 'Dibuka';
$_['column_comment']     = 'Komentar';
$_['column_reason']      = 'Alasan';
$_['column_action']      = 'Tindakan yang Diharapkan';
$_['column_invoice']	 = 'Nomor Struk';

$_['text_return_id']     = 'No. Pengembalian:';
$_['text_order_id']      = 'No. Pesanan:';
$_['text_date_ordered']  = 'Tanggal Pesanan:';
$_['text_date_added']    = 'Tanggal pengembalian:';
$_['text_history']       = 'Riwayat Pengembalian';
$_['text_no_comment']    = "Belum ada komentar";
$_['text_view_more']     = 'Lihat selengkapnya';
$_['entry_attribute']    = 'Attribute';
$_['entry_text']     	 = 'Deskripsi';

//report
$_['text_report']     	 = 'Laporan';
$_['text_date_start']    = 'Tanggal Mulai';
$_['text_date_end']    	 = 'Tanggal Akhir';

//list payments
$_['text_pay_type']    	 	= 'Tipe Pembayaran';
$_['text_pay_status']    	= 'Status Pembayaran';
$_['text_date_paid']     	= 'Tanggal Pembayaran';
$_['text_date_created']  	= 'Tanggal Pembuatan';
$_['text_requester']  	 	= 'Pemohon';
$_['text_total_price']   	= 'Total Harga';
$_['text_subtotal']   	 	= 'Subtotal';
$_['text_total']   	 	= 'Total';
$_['text_date_modified'] 	= 'Tanggal Perubahan';
$_['text_number'] 		 	= 'No.';
$_['text_transaction_date'] = 'Tanggal Transaksi';
$_['text_transaction_code'] = 'Kode Transaksi';
$_['text_balance'] 			= 'Balance';

$_['text_batch'] 	= 'Batch';
$_['text_batch_select'] 	= 'Pilih Batch';

// Account - Staff
$_['ms_account_staff_heading'] = 'User Management';
$_['ms_account_staff_breadcrumbs'] = 'User Management';
$_['ms_account_newstaff_breadcrumbs'] = 'Create User';
$_['ms_account_updatestaff_breadcrumbs'] = 'Update User';
$_['ms_create_user'] = 'Create User';
$_['ms_account_staff_name'] = 'Name';
$_['ms_account_staff_email'] = 'Email';
$_['ms_account_staff_status'] = 'Status';
$_['ms_account_staff_date'] = 'Date Added';
$_['ms_account_staff_action'] = 'Action';

$_['ms_account_staff_status_' . MsStaff::STATUS_ENABLED] = 'Enabled';
$_['ms_account_staff_status_' . MsStaff::STATUS_DISABLED] = 'Disabled';

$_['ms_success_staff_enabled'] = 'User enabled';
$_['ms_success_staff_disabled'] = 'User disabled';
$_['ms_success_staff_created'] = 'User created';
$_['ms_success_staff_updated'] = 'User updated';
$_['ms_success_staff_deleted'] = 'User deleted';

$_['ms_account_newstaff_heading'] = 'Create User';
$_['ms_account_updatestaff_heading'] = 'Update User';


$_['ms_account_staff_role'] = 'Roles';
$_['ms_account_staff_roles_unselect_all'] = 'Unselect All';
$_['ms_account_staff_roles_select_all'] = 'Select All';
$_['ms_account_staff_roles_note'] = 'User Roles';

$_['ms_premium'] = 'Premium Seller';
$_['ms_link_request_premium'] = 'Dapatkan premium access sekarang. <a href="%s">Request now</a>';
$_['ms_link_premium'] = 'Anda telah megajukan premium access, permintaan anda sedang dalam proses';

$_['ms_account_noroles_heading'] = 'Anda tidak dapat mengakses halaman ini !';
$_['ms_account_noroles'] = 'Harap hubungi administrator.';

$_['ms_account_withdraw_method_transfer'] = 'Bank Transfer';

$_['ms_account_dashboard_nav_staff'] = 'User Management';

//Report
$_['ms_rep_payment'] = 'Laporan Transaksi Pembayaran';
$_['ms_rep_trans'] = 'Laporan Transaksi';
$_['ms_rep_balance'] = 'Laporan Arsip Saldo';
$_['ms_payment_trans'] = 'Transaksi Pembayaran';
$_['ms_download_date'] = 'Tanggal Download';
$_['ms_number'] = 'No';
$_['ms_total_amount'] = 'Jumlah Total';
$_['ms_invoice'] = 'No. Struk';
$_['ms_customer_name'] = 'Nama Pelanggan';
$_['ms_product_sku'] = 'SKU Produk';
$_['ms_product_price'] = 'Harga Produk';
$_['ms_product_total'] = 'Total Produk';

$_['ms_error_product_quantity_empty'] = 'Silahkan tentukan kuantitas untuk produk anda';
$_['ms_error_product_quantity_invalid'] = 'Kuantitas tidak valid';
$_['ms_error_product_date_available_invalid'] = 'Tanggal tersedia tidak valid';
$_['ms_error_length_empty'] = 'Panjang produk tidak boleh kosong';
$_['ms_error_length_invalid'] = 'Panjang produk tidak valid';
$_['ms_error_width_empty'] = 'Lebar produk tidak boleh kosong';
$_['ms_error_width_invalid'] = 'Lebar produk tidak valid';
$_['ms_error_height_empty'] = 'Tinggi produk tidak boleh kosong';
$_['ms_error_height_invalid'] = 'Tinggi produk tidak valid';
$_['ms_error_weight_empty'] = 'Berat produk tidak boleh kosong';
$_['ms_error_weight_invalid'] = 'Berat produk tidak valid';
$_['ms_error_product_manufacturer_empty'] = 'Silahkan tentukan manufaktur produk';
$_['ms_error_product_manufacturer_invalid'] = 'Manufaktur tidak valid';
$_['ms_error_product_tax_class_id_empty'] = 'Silahkan tentukan kelas pajak';
$_['ms_error_product_tax_class_id_invalid'] = 'Kelas pajak tidak valid';


$_['ms_error_product_sku_empty'] = 'SKU tidak boleh kosong';
$_['ms_error_product_code_empty'] = 'Kode produk tidak boleh kosong';
$_['ms_error_sellerinfo_logistic_empty'] = 'Silahkan pilih logistik';
$_['ms_error_sellerinfo_country_empty'] = 'Silahkan pilih negara';
$_['ms_error_sellerinfo_city_empty'] = 'Silahkan pilih kota';
$_['ms_error_sellerinfo_zone_empty'] = 'Silahkan pilih provinsi';
$_['ms_error_sellerinfo_district_empty'] = 'Silahkan pilih kecamatan';
$_['ms_error_sellerinfo_subdistrict_empty'] = 'Silahkan pilih kelurahan';
$_['ms_error_sellerinfo_postcode_empty'] = 'Kode pos tidak boleh kosong';
$_['ms_error_sellerinfo_account_empty'] = 'Silahkan pilih akun bank';
$_['ms_error_sellerinfo_account_name_empty'] ='Nama akun tidak boleh kosong';
$_['ms_error_sellerinfo_account_number_empty'] = 'Nomor akun tidak boleh kosong';
$_['ms_error_sellerinfo_npwp_number_empty'] = 'Nomor NPWP tidak boleh kosong';
$_['ms_error_sellerinfo_npwp_address_empty'] = 'Alamat NPWP tidak boleh kosong';
$_['ms_error_sellerinfo_address_length'] = 'Alamat harus lebih dari 20 karakter';

$_['ms_warning_date_picker'] = 'Pilih tanggal mulai dan tanggal akhir';
$_['ms_warning_date_format'] = 'Format tanggal harus YYYY-MM-DD';
$_['ms_account_dashboard_nav_password'] = 'Ubah Password';

//print
$_['text_telephone'] 		= 'Telepon:';
$_['text_shipping_address'] = 'Alamat Pengiriman';
$_['text_delivery_type'] 	= 'Tipe Pengiriman:';
$_['text_unit_price'] 		= 'Harga per Unit';
$_['text_invoice_num']	 	= 'No. Struk: ';
$_['text_website']	 		= 'Website: ';
$_['text_print']          	= 'Cetak ';
$_['text_seller_invoice']    = 'PENJUAL';
$_['text_invoice_info']  		= 'INFORMASI TAGIHAN';
$_['text_delivery_info']  		= 'INFORMASI PENGIRIMAN';
$_['text_order_info']  			= 'INFORMASI PESANAN';
$_['text_voucher_info']  		= 'INFORMASI PENERIMA & VOUCHER';
$_['text_note_invoice']  		= '*)';
$_['text_note_desc']  			= 'Harga di atas sudah termasuk PPN';
$_['column_npwp']			= 'NPWP Penjual';
$_['column_date_order']     = 'Tanggal Pesanan';
$_['column_shipping']       = 'Ongkos Kirim';
$_['column_receiver']       = 'Ditujukan kepada';
$_['column_telp']       	= 'No. Telepon';
$_['column_product_list']   = 'Daftar Produk';
$_['column_sku']       		= 'SKU';

//account-staff
$_['entry_firstname']		= 'Nama awal';
$_['entry_lastname']		= 'Nama akhir';
$_['entry_email']			= 'Email';
$_['entry_telephone']		= 'Telepon';
$_['entry_dob']				= 'Tanggal Lahir';

$_['error_firstname']		= 'Nama awal wajib diisi';
$_['error_lastname']		= 'Nama akhir wajib diisi';
$_['error_email']			= 'Email tidak valid';
$_['error_telephone']		= 'Telepon wajib diisi';
$_['error_dob']				= 'Tanggal lahir tidak valid';

//account-upload-product
// Text
$_['heading_title']        = 'Unggah Produk';
$_['text_category']        = 'Kategori Produk';
$_['text_seller']          = 'Penjual';
$_['text_choose']          = 'Silahkan pilih';
$_['text_success_upload']  = 'File anda berhasil diunggah!';
$_['text_success_save']    = "Produk berhasil disimpan";

// Entry
$_['entry_upload']         = 'Unggah File';
$_['entry_choose_file']    = 'Pilih file';
$_['entry_overwrite']      = 'File yang akan diganti';
$_['entry_progress']       = 'Proses';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify extensions!';
$_['error_temporary']   = 'Warning: There are some temporary files that require deleting. Click the clear button to remove them!';
$_['error_upload']		= 'Unggah terlebih dahulu';
$_['error_filename']    = 'Nama file harus diantara 3 dan 128 karakter';
$_['error_filetype']    = 'Tipe file tidak sesuai';
$_['error_category']    = 'Harap pilih kategori produk';
$_['error_seller']      = 'Harap pilih penjual';
$_['error_sku']      	= 'Duplikasi SKU pada baris ke ';
$_['error_agi']      	= 'Atribut tidak tersedia atas kategori produk yang dipilih';
$_['error_attr_name']   = 'Nama atribut tidak sesuai pada kolom ';
$_['error_manufacturer']= 'Manufaktur tidak ditemukan pada baris ke ';
$_['error_format']		= 'Format template tidak sesuai';
$_['error_numeric']		= 'Inputan tidak sesuai (harus berupa angka) pada baris ke ';
$_['error_date']		= 'Format tanggal tidak sesuai (gunakan format YYYY-MM-DD ) pada baris ke ';
$_['error_blank']		= 'Terdapat kolom yang kosong pada baris ke ';
$_['error_image']		= 'Ekstensi file yang diperbolehkan .jpg, .jpeg, atau .png pada baris ke ';

//Help
$_['help_upload']     	   = 'Unggah file .xls atau .xlsx';
$_['help_product']     	   = 'Pilih kategori produk';
$_['help_seller']     	   = 'Pilih penjual';

//request product
$_['ms_account_request_product']	= 'Ajukan Produk';
$_['ms_list_request_product']		= 'Daftar Pengajuan Produk';
$_['text_kiosk']					= 'Kios / Pickup Point';
$_['button_create_request']			= 'Buat Pengajuan';
$_['text_create_request']			= 'Buat Pengajuan Produk';
$_['text_view_request']				= 'Detail Pengajuan Produk';
$_['text_edit_request']				= 'Ubah Nomor Pengiriman';

$_['column_request']				= 'ID Pengajuan';
$_['column_kiosk']					= 'Kios';
$_['column_delivery_order']			= 'Nomor Pengiriman';

$_['error_delivery_order']			= 'Nomor pengiriman wajib diisi';
$_['error_product_request']			= 'Semua field wajib diisi';
$_['error_kiosk_id']				= 'Pilih kios/pickup point';
$_['error_product_name']			= 'Produk wajib diisi';
$_['error_product_price']			= 'Harga wajib diisi';
$_['error_product_quantity']		= 'Jumlah wajib diisi';
$_['error_minimum']					= 'Jumlah harus lebih besar dari jumlah minimal kategori produk';

$_['text_success_edit_request']    	= 'Nomor pengiriman berhasil diubah';
$_['text_success_add_request']    	= 'Pengajuan produk berhasil';

//register seller
$_['text_tax_type']    				= 'Tipe Pajak';
$_['text_business_type']    		= 'Tipe Bisnis';
$_['text_tax_type_pkp']    			= 'PKP';
$_['text_tax_type_non_pkp']    		= 'NON-PKP';
$_['text_tax_type_non_pkp_val']    	= 'NON_PKP';
$_['text_business_type_1']    		= 'INDIVIDUAL';
$_['text_business_type_2']    		= 'BUSINESS';

$_['text_attachment']    			= 'Lampiran';
$_['text_npwp']    					= 'NPWP';
$_['text_ktp']    					= 'KTP';
$_['text_book_account']    			= 'Buku Rekening';
$_['text_siup']    					= 'SIUP';
$_['text_tdp']    					= 'TDP';
$_['text_apbu']    					= 'Akta Pendirian Badan Usaha';
$_['text_ktp_direksi']    			= 'KTP Direksi';
$_['text_domisili']    				= 'Surat Keterangan Domisili';

$_['note_npwp']    					= 'Unggah scan NPWP';
$_['note_ktp']    					= 'Unggah scan KTP';
$_['note_book_account']    			= 'Unggah scan buku rekening';
$_['note_siup']    					= 'Unggah scan SIUP';
$_['note_tdp']    					= 'Unggah scan TDP';
$_['note_apbu']    					= 'Unggah scan Akta Pendirian Badan Usaha';
$_['note_ktp_direksi']    			= 'Unggah scan KTP Direksi';
$_['note_domisili']    				= 'Unggah scan Surat Keterangan Domisili';

$_['error_upload_required']    		= 'Unggah file yang diperlukan';

//report view detail
$_['column_order_no']			= 'Receipt No.';
$_['column_vendor_name']		= 'Seller Name';
$_['column_vendor_type']		= 'Seller Type';
$_['column_kiosk_id']			= '#ID Kiosk';
$_['column_payment_type']		= 'Payment Type';
$_['column_transaction_date']	= 'Transaction Date';
$_['column_quantity']			= 'Qty';
$_['column_sku']				= 'SKU';
$_['column_price']				= 'Price';
$_['column_total_report']		= 'Total';
$_['column_tax']				= 'Tax (PPN)';
$_['column_commission']			= 'Commission Base';
$_['column_shipping']			= 'Shipping Fee';
$_['column_online_payment']		= 'Online Payment Fee';
$_['column_ofiskita_commission']= 'Commission';
$_['column_tax_ppn']			= 'Tax (PPN for Commission)';
$_['column_vendor_balance']		= 'Balance';
$_['column_batch']		= 'Batch';
$_['error_session_timeout']		= 'Silahkan login dahulu! <a href="%s">Login</a>';
$_['column_total_report']		= 'Total';
$_['text_to']		= 'Sampai';
$_['column_payment_type']		= 'Payment Type';
$_['column_revenue_type']		= 'Revenue Type';
$_['column_business_type']		= 'Business Type';
$_['column_tax_type']			= 'Tax Type';
$_['column_relation_type']		= 'Relation Type';
$_['column_payment_total']		= 'Payment Total';
$_['column_bank']				= 'Bank';

$_['text_method_transfer'] = 'Bank Transfer';
$_['text_description_withdrawal'] = 'Penarikan menggunakan %s (%s) di %s';

$_['ms_reject_order_description'] = 'Reject Order %s';
?>
