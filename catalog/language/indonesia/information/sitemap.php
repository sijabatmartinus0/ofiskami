<?php
// Heading
$_['heading_title']    = 'Peta Situs';
 
// Text
$_['text_special']     = 'Penawaran Istimewa';
$_['text_account']     = 'Akun Saya';
$_['text_edit']        = 'Informasi Akun';
$_['text_password']    = 'Password';
$_['text_address']     = 'Buku Alamat';
$_['text_history']     = 'Riwayat Pesanan';
$_['text_download']    = 'Unduh';
$_['text_cart']        = 'Cart';
$_['text_checkout']    = 'Checkout';
$_['text_search']      = 'Cari';
$_['text_information'] = 'Informasi';
$_['text_contact']     = 'Hubungi Kami';
?>
