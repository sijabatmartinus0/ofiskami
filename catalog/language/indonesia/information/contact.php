<?php
// Heading
$_['heading_title']  = 'Hubungi Kami';

// Text 
$_['text_location']  = 'Lokasi Kami';
$_['text_store']     = 'Toko Kami';
$_['text_contact']   = 'Form Kontak';
$_['text_address']   = 'Alamat';
$_['text_telephone'] = 'Telepon';
$_['text_fax']       = 'Fax';
$_['text_category']  = 'Kategori';
$_['text_open']      = 'Waktu Pembukaan';
$_['text_comment']   = 'Komentar';
$_['text_success']   = '<p>Pesan Anda berhasil dikirim ke admin ofiskita.com!</p>';
$_['text_choose_category']  = '-- Pilih Kategori --';

// Entry Fields
$_['entry_name']     = 'Nama Anda';
$_['entry_email']    = 'Alamat E-Mail';
$_['entry_enquiry']  = 'Pesan';
$_['entry_captcha']  = 'Masukkan kode di kotak berikut';

// Email
$_['email_subject']  = 'Pesan %s';

// Errors
$_['error_name']     = 'Nama harus terdiri dari 3 sampai 32 karakter!';
$_['error_email']    = 'Alamat E-Mail tidak valid!';
$_['error_enquiry']  = 'Pesan harus terdiri dari 10 sampai 3000 karakter!';
$_['error_captcha']  = 'Kode verifikasi tidak sesuai dengan gambar!';
$_['error_telephone']  = 'Harap masukkan nomor telepon yang valid!';
$_['error_category']  = 'Harap pilih kategori!';

// Category
$_['category_1']	= 'Daftar & Login';
$_['category_2']	= 'Akun';
$_['category_3']	= 'Pemesanan / Pengiriman';
$_['category_4']	= 'Komplain';
$_['category_5']	= 'Pembayaran';
$_['category_6']	= 'Sistem Reputasi / Ulasan';
$_['category_7']	= 'Pengembalian & Penarikan Dana';
$_['category_8']	= 'Pengaturan Toko';
$_['category_9']	= 'Cara Promosi';
$_['category_10']	= 'Promo & Fitur Ofiskita';
$_['category_11']	= 'Tentang Ofiskita';
?>
