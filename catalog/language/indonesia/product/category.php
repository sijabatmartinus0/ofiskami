<?php
// Text
$_['text_refine']       = 'Perbaiki Pencarian';
$_['text_product']      = 'Produk';
$_['text_error']        = 'Kategori tidak ditemukan!';
$_['text_empty']        = 'Tidak ada produk dalam kategori ini.';
$_['text_quantity']     = 'Jumlah:';
$_['text_manufacturer'] = 'Merk:';
$_['text_model']        = 'Kode Produk:'; 
$_['text_points']       = 'Poin Hadiah:'; 
$_['text_price']        = 'Harga:'; 
$_['text_tax']          = 'Diluar Pajak:'; 
$_['text_compare']      = 'Perbandingan Produk (%s)'; 
$_['text_sort']         = 'Sortir berdasarkan:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Nama (A - Z)';
$_['text_name_desc']    = 'Nama (Z - A)';
$_['text_sold_asc']    = 'Terjual';
$_['text_price_asc']    = 'Harga (Rendah &gt; Tinggi)';
$_['text_price_desc']   = 'Harga (Tinggi &gt; Rendah)';
$_['text_rating_asc']   = 'Penilaian (Terendah)';
$_['text_rating_desc']  = 'Penilaian';
$_['text_discount']     = 'Discount';
$_['text_discount_asc'] = 'Discount (Rendah &gt; Tinggi)';
$_['text_discount_desc']= 'Discount (Tinggi &gt; Rendah)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Tampilan:';
$_['text_discount_terbaik'] = 'Diskon Terbaik';
$_['text_modal_cart']                = 'Beli Produk';
$_['button_modal_cart']            = 'Beli Produk Ini';
?>