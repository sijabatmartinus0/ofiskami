<?php
// Heading 
$_['heading_title']         = 'Riwayat Pesanan';
$_['heading_payment']       = 'Konfirmasi Pembayaran';
$_['heading_success']       = 'Konfirmasi Sukses';

// Text
$_['text_account']          = 'Akun';
$_['text_order']            = 'Informasi Pesanan';
$_['text_order_detail']     = 'Detail Pesanan';
$_['text_invoice_no']       = 'No. Pesanan:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Tanggal:';
$_['text_shipping_address'] = 'Alamat Pengiriman';
$_['text_shipping_method']  = 'Metode Pengiriman:';
$_['text_payment_address']  = 'Alamat Pembayaran';
$_['text_payment_method']   = 'Metode Pembayaran:';
$_['text_comment']          = 'Komentar Pesanan';
$_['text_history']          = 'Sejarah Pesanan';
$_['text_success_receive']          = 'Order telah anda terima';
$_['text_success_complete']          = 'Order anda telah selesai';
$_['text_success']          = 'Sukses: Anda telah menambahkan <a href="%s">%s</a> ke dalam <a href="%s">Cart</a> Anda!';
$_['text_empty']            = 'Anda belum melakukan pesanan sebelumnya!';
$_['text_error']            = 'Pesanan yang anda minta tidak dapat ditemukan!';
$_['text_receipt']          = ' dengan nomor resi ';
$_['text_seller']          = 'Penjual:';
$_['text_seller_invoice']    = 'PENJUAL';
$_['text_print']          = 'Cetak ';
$_['text_complete_return']  = 'Harap tunggu sampai proses pengembalian selesai';
$_['text_receiver_name']  	= 'Nama:';
$_['text_receiver_email']  	= 'Email:';
$_['text_voucher_data']  	= 'Data Voucher';
$_['text_voucher']  		= 'voucher';

//Waybill
$_['text_modal_waybill']  		= 'Lacak Kiriman';
$_['text_waybill_number']  		= 'Waybill';
$_['text_waybill_courier']  	= 'Kurir';
$_['text_waybill_service']  	= 'Layanan';
$_['text_waybill_date']  		= 'Tanggal Pengiriman';
$_['text_waybill_origin']  		= 'Asal';
$_['text_waybill_destination']  = 'Tujuan';
$_['text_waybill_shipper']  	= 'Pengirim';
$_['text_waybill_receiver']  	= 'Penerima';
$_['text_waybill_not_found']  	= 'Invalid waybill. Resi yang Anda masukkan salah atau belum terdaftar.';
$_['text_invoice_info']  		= 'INFORMASI TAGIHAN';
$_['text_delivery_info']  		= 'INFORMASI PENGIRIMAN';
$_['text_order_info']  			= 'INFORMASI PESANAN';
$_['text_voucher_info']  		= 'INFORMASI PENERIMA & VOUCHER';
$_['text_note_invoice']  		= '*)';
$_['text_note_desc']  			= 'Harga di atas sudah termasuk PPN';

// Column
$_['column_order_id']       = 'No. Pesanan';
$_['column_product']        = 'No. Produk';
$_['column_customer']       = 'Penerima';
$_['column_receiver']       = 'Ditujukan kepada';
$_['column_telp']       	= 'No. Telepon';
$_['column_buyer']       	= 'Pembeli';
$_['column_name']           = 'Nama Produk';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Jumlah';
$_['column_sku']       		= 'SKU';
$_['column_price']          = 'Harga';
$_['column_total']          = 'Total';
$_['column_action']         = 'Tindakan';
$_['column_date_added']     = 'Tanggal';
$_['column_date_order']     = 'Tanggal Pesanan';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Komentar';
$_['column_shipping']       = 'Ongkos Kirim';
$_['column_product_list']   = 'Daftar Produk';
$_['column_buyer']  		= 'Pembeli';
$_['column_invoice']  		= 'No. Pesanan';
$_['column_insurance']  	= 'Biaya Asuransi';
$_['column_remarks']  		= 'Keterangan';
$_['column_code']  			= 'Kode Voucher';
$_['column_message']  		= 'Pesan';
$_['column_description']  	= 'Deskripsi';
$_['column_voucher']  		= 'Detail Voucher';
$_['column_payment_method'] = 'Metode Pembayaran';
$_['column_shipping_method']= 'Metode Pengiriman';

$_['column_name_voucher']	= 'Nama Penerima';
$_['column_email_voucher']	= 'Email Penerima';
$_['column_voucher_amount']	= 'Nominal Voucher';

// Error
$_['error_reorder']         = '%s saat ini tidak tersedia untuk pemesanan ulang.';

// Button
$_['button_confirm']		= 'Konfirmasi Pembayaran';
$_['button_confirm_done']	= 'Telah Dikonfirmasi';
$_['button_receive']	= 'Received Order';
$_['button_complete']	= 'Complete Order';

$_['text_total']  		= 'Total';
$_['text_subtotal']  		= 'Sub Total';
?>