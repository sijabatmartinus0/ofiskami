<?php
// Heading 
$_['heading_title']      = 'Pengembalian Produk';

// Text
$_['text_account']       = 'Akun';
$_['text_return']        = 'Informasi Pengembalian';
$_['text_return_detail'] = 'Detail Pengembalian';
$_['text_description']   = '<p>Mohon dilengkapi form berikut ini untuk meminta no. RMA.</p>';
$_['text_order']         = 'Informasi Pesanan';
$_['text_product']       = 'Informasi Produk &amp; Alasan Pengembalian';
$_['text_message']       = '<p>Terima kasih atas pengajuan permintaan pengembalian Anda. Permintaan Anda telah dikirim ke bagian yang terkait untuk diproses.</p><p> Anda akan diberitahukan melalui e-mail mengenai status permintaan Anda.</p>';
$_['text_return_id']     = 'No. Pengembalian:';
$_['text_order_id']      = 'No. Pesanan:';
$_['text_date_ordered']  = 'Tanggal Pesanan:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Tanggal ditambahkan:';
$_['text_email']    	 = 'Email:';
$_['text_comment']       = 'Komentar Pengembalian';
$_['text_history']       = 'Riwayat Pengembalian';
$_['text_empty']         = 'Anda belum pernah melakukan pengembalian sebelumnya!';
$_['text_agree']         = 'Saya telah membaca dan setuju untuk <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'No. Pengembalian';
$_['column_order_id']    = 'No. Pesanan';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Tanggal Ditambahkan';
$_['column_customer']    = 'Pelanggan';
$_['column_merchant']    = 'Penjual';
$_['column_product']     = 'Nama Produk';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Jumlah';
$_['column_price']       = 'Harga';
$_['column_opened']      = 'Dibuka';
$_['column_comment']     = 'Komentar';
$_['column_reason']      = 'Alasan';
$_['column_action']      = 'Tindakan';

// Entry
$_['entry_order_id']     = 'No. Pesanan';
$_['entry_date_ordered'] = 'Tanggal Pesanan';
$_['entry_firstname']    = 'Nama Depan';
$_['entry_lastname']     = 'Nama Belakang';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telepon';
$_['entry_product']      = 'Nama Produk';
$_['entry_model']        = 'Kode Produk';
$_['entry_quantity']     = 'Jumlah';
$_['entry_reason']       = 'Alasan Pengembalian:';
$_['entry_opened']       = 'Produk dibuka:';
$_['entry_fault_detail'] = 'Kesalahan atau detail lainnya:';
$_['error_quantity']     = 'Jumlah minimal adalah 1';
$_['error_quantity_limit'] = 'Jumlah tidak boleh melebihi jumlah produk per order';
$_['error_comment'] 	 = 'Mohon isi keterangan';

// Error
$_['text_error']         = 'Pengembalian yang Anda minta tidak ditemukan!';
$_['error_order_id']     = 'No. Pesanan diperlukan!';
$_['error_firstname']    = 'Nama Depan harus terdiri dari 1 sampai 32 karakter!';
$_['error_lastname']     = 'Nama Belakang harus terdiri dari 1 sampai 32 karakter!';
$_['error_email']        = 'Alamat E-Mail tidak valid!';
$_['error_telephone']    = 'Telepon harus terdiri dari 3 sampai 32 karakter!';
$_['error_product']      = 'Nama Produk harus terdiri dari 3 dan kurang dari 255 karakter!';
$_['error_model']        = 'Model Produk harus lebih dari 3 dan kurang dari 64 karakter!';
$_['error_reason']       = 'Anda harus memilih alasan pengembalian produk!';
$_['error_captcha']      = 'Kode verifikasi tidak sesuai dengan gambar!';
$_['error_agree']        = 'Error: Anda harus menyetujui %s!';

//edit by arin
$_['text_return_action']		= 'Pengembalian yang diharapkan';
$_['text_upload_return']		= 'Unggah gambar produk';
$_['text_upload']				= 'Unggah';
$_['text_invoice']				= 'Nomor Invoice';
$_['text_complete_return'] 	 	= 'Pengembalian Selesai';
$_['text_confirm'] 	 			= 'Konfirmasi Perubahan Tindakan';
$_['text_discuss']    			= 'Diskusi Permohononan Pengembalian';
$_['text_no_data']    			= "Belum ada komentar";
$_['text_view_more']     		= 'Lihat selengkapnya';

$_['success_complete_return']  	= 'Proses Pengembalian Telah Selesai';
$_['success_confirm_action']  	= 'Konfirmasi perubahan tindakan berhasil';
$_['warning_action_changed']  	= 'Tindakan pengembalian telah diganti, harap konfirmasi atau diskusikan dengan Merchant.';
$_['warning_file_type']  		= 'Tipe file yang diperbolehkan untuk diupload hanya .jpg, .png, atau .jpeg';
$_['error_return_action']   	= 'Pilih metode pengembalian yang diharapkan';
$_['error_picture_return']  	= 'Unggah gambar produk';
$_['error_discuss']   		    = 'Panjang komentar minimal sepanjang 3 karakter';

$_['column_invoice']			= 'Nomor Invoice';
$_['column_phone']		 		= 'Telepon';

$_['button_view']   			= 'Lihat';
$_['button_submit']    			= 'Kirim';

?>