<?php
// Heading
$_['heading_title'] = 'Akun Anda Berhasil Dibuat!';
$_['heading_title_error_code'] = 'Link Verifikasi Anda Tidak Valid!';

// Text
$_['text_message']  = '<p style="text-align:center;">Selamat! Akun baru Anda sukses dibuat!</p> <br><p style="text-align:center;"> Akun Anda : <strong>%s</strong></p><br> <p style="text-align:center;">Anda bisa mendapatkan keuntungan sebagai anggota untuk memperoleh pengalaman berbelanja online bersama Kami.</p> <p style="text-align:center;">Jika Anda memiliki pertanyaan apapun mengenai cara kerja toko online ini, mohon untuk mengirim email kepada admin Ofiskita.</p> <p style="text-align:center;">Sebuah konfirmasi telah dikirim ke alamat email yang telah disediakan. Jika Anda tidak menerimanya dalam jangka waktu satu jam, mohon untuk <a href="%s">menghubungi kami</a>.</p>';
$_['text_message_email']  = '<p>Segera verifikasi akun Anda, klik link berikut jika anda belum menerima email verifikasi <a href="%s">kirim email</a>.</p>';
$_['text_message_phone']  = '<p>Lengkapi keamanan akun Anda dengan melakukan verifikasi nomor HP. <a href="%s">Verifikasi Sekarang</a>.</p>';
// $_['text_message_complete']  = '<p>Selamat! Akun baru Anda sukses dibuat!</p> <p>Anda dapat memulai untuk berbelanja dengan aman di Ofiskita.com <a href="%s">sekarang</a>.</p>';
$_['text_message_complete']  = '<p>Selamat! Akun baru Anda sukses dibuat!</p> <p>Anda dapat memulai untuk berbelanja dengan aman di Ofiskita.com</p>';

$_['text_approval'] = '<p>Terima kasih untuk pendaftaran dengan %s!</p><p>Anda akan diberitabukan melalui email setelah Akun Anda diaktivasi oleh pemilik toko.</p><p>Jika Anda tidak menerimanya dalam jangka waktu satu jam, mohon untuk <a href="%s">menghubungi pemilik toko</a>.</p>';
$_['text_account']  = 'Akun';
$_['text_progress']  = 'Progres Registrasi';
$_['text_activate']  = 'Aktivasi';
$_['text_email']  = 'Email verifikasi telah dikirim';
$_['text_success_email_verification']  = 'Email anda telah diverifikasi';
$_['text_error_email_verification']  = 'Kode verifikasi salah, Mohon kirim ulang email verifikasi';
$_['text_message_error_code']  = '<p style="text-align:center;">Harap kirim ulang email verifikasi<p>';

$_['text_message_verification']  = '%s ini adalah kode konfirmasi akun Ofiskita anda';
$_['text_success_send_verification_code']  = 'kode verifikasi anda telah dikirim';
$_['error_empty_registered_number'] 				= "Harap masukkan nomor yang telah terdaftar";

$_['button_continue']  = 'Lanjutkan';
$_['button_send']  = 'Kirim';
$_['button_shop']  = 'Mulai Belanja';

//edit by arin
$_['text_verification_phone']  	= 'Verifikasi Nomor Handphone';
$_['text_registered_number']   	= 'Nomor Terdaftar';
$_['text_ver_code']   			= 'Kode Verifikasi';
$_['text_note1']   				= '* Pastikan nomor yang Anda daftarkan benar, kami akan mengirim SMS kode verfikasi ke nomor tersebut.<br/>* Verifikasi SMS hanya dapat dilakukan dengan menggunakan operator GSM.';
$_['text_note2']   				= 'Tidak menerima SMS?';
$_['text_note3']   				= '<p>Pengguna diharuskan memasukkan nomor telepon yang bisa dihubungi dan memperbaruinya secara berkala.</p>';
$_['text_retry_send']   		= 'Kirim ulang';


$_['button_send_verifivation'] 	= 'Kirim Kode Verifikasi';
$_['button_submit'] 			= 'Kirim';
$_['button_skip'] 				= 'Lewati';

//error
$_['error_is_verified'] 		= "Kode verifikasi tidak cocok";
$_['error_empty'] 				= "Harap masukkan kode verifikasi";

$_['text_edit_account']   		= 'Nomor telepon ini salah. <a href="%s" style="color:blue">Ubah akun</a>';
$_['text_modal_success']   		= 'Verifikasi Kode SMS';
$_['text_modal_success_message']   		= 'Kode verifikasi telah sukses dikirim. Silahkan periksa SMS dari kami pada ponsel Anda.';
$_['button_ok']   		= 'Ok';


$_['text_resend_email']   		= 'Berhasil mengirim ulang email verifikasi.';

?>