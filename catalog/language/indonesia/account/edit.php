<?php
// Heading 
$_['heading_title']     = 'Informasi Akun Saya';

// Text
$_['text_account']      = 'Akun';
$_['text_edit']         = 'Ubah Informasi';
$_['text_your_details'] = 'Data Pribadi Anda';
$_['text_success']      = 'Sukses: Akun Anda telah sukses diperbaharui.';

// Entry
$_['entry_firstname']  = 'Nama Depan';
$_['entry_lastname']   = 'Nama Belakang';
$_['entry_email']      = 'E-Mail';
$_['entry_telephone']  = 'Telepon';
$_['entry_fax']        = 'Fax';

// Error
$_['error_exists']     = 'Error: ALamat E-Mail sudah terdaftar!';
$_['error_firstname']  = 'Nama Awal harus terdiri dari 1 sampai 32 karakter!';
$_['error_lastname']   = 'Nama Akhir harus terdiri dari 1 sampai 32 karakter!';
$_['error_email']      = 'Alamat E-Mail tidak valid!';
$_['error_telephone']  = 'Telephone harus terdiri dari 3 sampai 32 karakter!';
$_['error_custom_field'] = '%s wajib!';
$_['error_dob']      = 'Format Tanggal Lahir salah (Contoh:2015-01-01)';
$_['error_dob_value']      = 'Minimum Tanggal Lahir '.date("Y-m-d",strtotime('-15 years'));


$_['text_payment_account']  = 'Detil Pembayaran';
$_['entry_account']  = 'Nama Bank';
$_['entry_account_name']  = 'Nama Akun';
$_['entry_account_number']  = 'Nomor Akun';

$_['error_account']    = 'Silahkan pilih akun bank';
$_['error_account_name']    = 'Nama akun harus terdiri dari 1 sampai 32 karakter!';
$_['error_account_number']    = 'Nomor akun tidak boleh kosong';
$_['error_account_number_invalid']    = 'Nomor akun salah';
?>