<?php
// Heading 
$_['heading_title']      = 'Akun Saya';

// Text
$_['text_account']       = 'Akun';
$_['text_my_account']    = 'Akun Saya';
$_['text_my_orders']     = 'Pesanan Saya';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Ubah informasi Akun Saya';
$_['text_password']      = 'Ubah password Saya';
$_['text_address']       = 'Modifikasi isi buku alamat Anda';
$_['text_wishlist']      = 'Modifikasi daftar keinginan Anda';
$_['text_order']         = 'Lihat riwayat pesanan Anda';
$_['text_download']      = 'Unduh';
$_['text_reward']        = 'Poin Hadiah Anda'; 
$_['text_return']        = 'Lihat permintaan pengembalian Anda'; 
$_['text_transaction']   = 'Transaksi Anda'; 
$_['text_newsletter']    = 'Mendaftar / Berhenti berlangganan Newsletter';
$_['text_recurring']     = 'Pembayaran Berulang';
$_['text_transactions']  = 'Transaksi';

$_['text_activate']  = 'Aktivasi Akun Saya';


$_['text_name']       = 'Nama';
$_['text_email']       = 'Email';
$_['text_phone']       = 'No. Telepon';
$_['text_dob']       = 'Tanggal Lahir';
$_['text_account_bank']       = 'Nama Bank';
$_['text_account_name']       = 'Nama Akun';
$_['text_account_number']       = 'Nomor Akun';
$_['text_balance']       = 'Saldo';
$_['text_reward_point']       = ' %s poin';
$_['text_list_order'] = '&nbsp;Order Terakhir';
$_['text_list_return'] = '&nbsp;Pengembalian';

$_['text_column_invoice_order'] = 'Invoice';
$_['text_column_status_order'] = 'Status';
$_['text_column_action_order'] = 'Action';

$_['text_column_invoice_return'] = 'Invoice';
$_['text_column_product_return'] = 'Produk';
$_['text_column_status_return'] = 'Status';
$_['text_column_action_return'] = 'Action';

$_['text_view_more'] = 'View More...';
$_['text_view'] = 'View';
$_['text_no_data'] = 'Tidak ada data';


$_['button_edit']       = 'Ubah';
?>