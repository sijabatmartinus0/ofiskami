<?php
// Heading
$_['heading_title']      = 'Review';

// Text
$_['text_account']       = 'Akun';
$_['text_review']       = 'Review Baru';
$_['text_seller']       = 'Penjual';
$_['text_customer']       = 'Pelanggan';
$_['text_no_review']       = 'Tidak ada data review';
$_['text_display']       = 'Tamplikan :';
$_['text_all']       = 'Semua';
$_['text_unread']       = 'Belum Dibaca';
$_['text_search']       = 'Invoice/Nama Produk';

$_['entry_review']      = 'Ulasan Anda:';
$_['entry_rating']      = 'Penilaian';
$_['entry_good']        = 'Bagus';
$_['entry_bad']         = 'Jelek';
$_['entry_accuracy']  = 'Akurasi Pengiriman';

$_['error_text']        = 'Error: Teks Ulasan harus terdiri dari 25 sampai 1000 karakter!';
$_['error_rating']      = 'Error: Silahkan pilih penilaian ulasan!';
$_['error_rating_accuracy']  = 'Error: Silahkan pilih penilaian ulasan akurasi pengiriman!';
$_['error_review']      = 'Error: Anda tidak dapat melakukan review produk ini';

$_['text_success']      = 'Terima kasih atas ulasan Anda.';

$_['button_hide']		= "Hide";
$_['button_show']		= "Show";