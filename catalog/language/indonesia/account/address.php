<?php
// Heading 
$_['heading_title']     = 'Buku Alamat';

// Text
$_['text_account']      = 'Akun';
$_['text_address_book'] = 'Isi Buku Alamat';
$_['text_edit_address'] = 'Ubah Alamat';
$_['text_add_address']    = 'Tambah Alamat';
$_['text_add']             = 'Alamat Anda telah berhasil ditambahkan';
$_['text_edit']            = 'Alamat Anda telah berhasil diperbaharui';
$_['text_delete']          = 'Alamat Anda telah berhasil dihapus';
$_['text_empty']           = 'Anda tidak memiliki alamat di Akun Anda.';

// Entry
$_['entry_firstname']   = 'Nama Depan';
$_['entry_lastname']    = 'Nama Belakang';
$_['entry_alias']       = 'Simpan Alamat sebagai';
$_['entry_company']     = 'Perusahaan';
$_['entry_address_1']   = 'Alamat 1';
$_['entry_address_2']   = 'Alamat 2';
$_['entry_postcode']       = 'Kode Pos';
$_['entry_subdistrict']      = 'Kelurahan';
$_['entry_district']      = 'Kecamatan';
$_['entry_city']           = 'Kota';
$_['entry_country']        = 'Negara';
$_['entry_zone']           = 'Propinsi';
$_['entry_default']     = 'Alamat Utama';

// Error
$_['error_delete']      = 'Error: Anda harus memiliki setidaknya satu alamat!';
$_['error_default']     = 'Error: Anda tidak dapat menghapus alamat utama Anda!';
$_['error_firstname']   = 'Nama Awal harus terdiri dari 1 sampai 32 karakter!';
$_['error_lastname']    = 'Nama Akhir harus terdiri dari 1 sampai 32 karakter!';
$_['error_alias']    = 'Nama Alamat harus terdiri dari 1 sampai 32 karakter!';
$_['error_vat']         = 'Jumlah PPN tidak valid!';
$_['error_address_1']   = 'Alamat harus terdiri dari 3 sampai 128 karakter!';
$_['error_subdistrict']      = 'Silahkan pilih Kelurahan!';
$_['error_district']      = 'Silahkan pilih Kecamatan!';
$_['error_city']           = 'Silahkan pilih Kota!';
$_['error_postcode']       = 'Kode post harus terdiri dari 2 sampai 10 karakter!';
$_['error_country']        = 'Silahkan pilih Negara!';
$_['error_zone']           = 'Silahkan pilih Propinsi!';
$_['error_custom_field']   = '%s wajib!';
$_['error_delete_cart']   = 'Error: Anda harus menghapus cart terlebih dahulu!';
?>