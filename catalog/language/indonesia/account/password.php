<?php
// Heading 
$_['heading_title']  = 'Ganti Password';

// Text
$_['text_account']   = 'Akun';
$_['text_password']  = 'Password Anda';
$_['text_success']   = 'Sukses: Password anda telah sukses diperbaharui.';

// Entry
$_['entry_password']  = 'Password Baru';
$_['entry_confirm']   = 'Konfirmasi Password';
$_['entry_old_pass']  = 'Password Lama';

// Error
$_['error_password'] 	= 'Password harus terdiri dari 4 sampai 20 karakter!';
$_['error_confirm'] 	= 'Konfirmasi Password tidak sama dengan password!';
$_['error_code'] 		= 'Kode tidak valid!';
$_['error_old_match']  	= 'Password lama tidak sesuai';

$_['button_submit']  = 'Submit';
?>