<?php
// Heading 
$_['heading_title']        = 'Pendaftaran Merchant';

// Text
$_['text_account']         = 'Akun';
$_['text_register']        = 'FORM REGISTRASI MERCHANT';
$_['text_your_details']    = 'Data Merchant Anda';
$_['text_your_address']    = 'Alamat Anda';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Password Anda';
$_['text_personal'] = 'Personal';
$_['text_corporate'] = 'Corporate';
$_['text_agree']           = 'Saya telah membaca dan menyetujui <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';
// $_['text_agree']           = 'Dengan menekan Daftar Akun, saya mengkonfirmasi telah menyetujui <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>, serta <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_nama'] = 'Nama';
$_['entry_badan_usaha']      = 'Badan Usaha';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telepon';
$_['entry_kategori_produk'] = 'Kategori Produk';
$_['entry_website_toko'] = 'Website Toko';
$_['entry_npwp'] = 'Memiliki NPWP';
// Error
$_['error_exists']         = 'Error: Alamat E-Mail sudah terdaftar!';
$_['error_nama']         = 'Nama harus terdiri dari 1 sampai 32 karakter!';
$_['error_badan_usaha']      = 'Badan Usaha harus terdiri dari 1 sampai 32 karakter!';
$_['error_email']          = 'Alamat E-Mail tidak valid!';
$_['error_telephone']      = 'Telepon harus terdiri dari 3 sampai 32 karakter!';
$_['error_kategori_produk']      = 'Kategori Produk harus terdiri dari 3 sampai 32 karakter!';
$_['error_website_toko']      = 'Website toko harus terdiri dari 3 sampai 32 karakter!';
?>