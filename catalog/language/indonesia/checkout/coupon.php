<?php
// Heading
$_['heading_title'] = 'Gunakan Kode Kupon';

// Text
$_['text_success']  = 'Sukses: Voucher Diskon Anda telah diaplikasikan!';
$_['text_unset']  = 'Sukses: Voucher telah dikosongkan!';
// Entry
$_['entry_coupon']  = 'Masukkan Voucher Anda disini';

// Error
$_['error_coupon']  = 'Error: Kupon tidak valid, kupon telah mencapai batas penggunaan!';
$_['error_empty']   = 'Error: Silahkan masukkan kode kupon!';
$_['error_cart']	   	= 'Error: Kupon tidak valid, keranjang belanja kosong!';
$_['error_coupon']	   	= 'Error: Kode kupon tidak sesuai!';
$_['error_product']	   	= 'Error: Kupon tidak valid, produk tidak sesuai!';
$_['error_customer']   	= 'Error: Kupon tidak valid, email tidak sesuai!';
$_['error_newsletter'] 	= 'Error: Kupon tidak valid, email tidak sesuai!';
$_['error_expired']		= 'Error: Kupon tidak valid, Kupon tidak sesuai atau telah melewati batas pemakaian!';
$_['error_minimum']   	= 'Error: Kupon tidak valid, minimum pembelanjaan belum tercapai!';
