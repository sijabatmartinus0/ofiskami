<?php
// Heading
$_['heading_title'] = 'Pesanan Anda Telah Diproses!';

// Text
$_['text_basket']   = 'Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Sukses';
//$_['text_customer'] = '<p>Pesanan Anda telah berhasil diproses!</p><p>Anda dapat melihat riwayat pesanan Anda dengan beralih ke halaman <a href="%s">Akun Saya </a> dan dengan mengklik <a href = "%s">Riwayat Pesanan</a>.</p>Jika pembelian Anda memiliki unduhan yang terkait, Anda dapat melanjutkan ke halaman <a href="%s">Unduh</a> untuk melihatnya.</p><p>Silahkan kirimkan pertanyaan Anda ke <a href="%s">pemilik toko</a>.</p><p>Terima kasih untuk berbelanja dengan Kami secara online.!</p>';
$_['text_guest']    = '<p>Pesanan anda telah berhasil diproses!</p><p>Terima kasih untuk berbelanja dengan Kami secara online.!</p>';

//edit by arin
$_['text_customer'] = '<center><p>Terima kasih, Anda telah berhasil melakukan checkout pemesanan dengan memilih pembayaran %s</p></center>';
// $_['text_customer_transfer']='<center><p>Anda dikenakan biaya untuk kode transfer sebesar %s</p></center>';
$_['text_customer_transfer']='<center><p>Harap masukkan berita transfer Anda dengan Kode Pemesanan berikut: %s</p></center>';
$_['text_total_price'] = 'Jumlah yang harus dibayar';
$_['text_total_price_paid'] = 'Jumlah yang telah dibayar';

$_['button_confirm_payment'] = 'Konfirmasi Pembayaran';
?>