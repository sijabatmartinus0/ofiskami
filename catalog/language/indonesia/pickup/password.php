<?php
// Heading 
$_['heading_title']  = 'Ganti Password';
$_['pp_heading_nav'] = 'Menu Navigasi Cepat';
$_['pp_account_dashboard_nav'] = 'Dashboard';
$_['pp_account_history_nav'] = 'Riwayat';
$_['pp_change_password'] = 'Ubah Password';
// Text
$_['text_account']   = 'Akun';
$_['text_password']  = 'Password Anda';
$_['text_success']   = 'Sukses: Password anda telah sukses diperbaharui.';

// Entry
$_['entry_password']  = 'Password Baru';
$_['entry_confirm']   = 'Konfirmasi Password';
$_['entry_old_pass']  = 'Password Lama';

// Error
$_['error_password'] 	= 'Password harus terdiri dari 4 sampai 20 karakter!';
$_['error_confirm'] 	= 'Konfirmasi Password tidak sama dengan password!';
$_['error_code'] 		= 'Kode tidak valid!';
$_['error_old_match']  	= 'Password lama tidak sesuai';
$_['error_same_pass']  	= 'Gunakan password yang berbeda jika Anda ingin mengubah password';

$_['button_submit']  = 'Submit';
?>