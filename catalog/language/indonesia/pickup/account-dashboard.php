<?php
// Text
$_['heading_title'] = 'Dashboard Pick Up Point';
$_['pp_heading_nav'] = 'Menu Navigasi Cepat';
$_['pp_account_dashboard_nav'] = 'Dashboard';
$_['pp_account_history_nav'] = 'Riwayat';
$_['pp_change_password'] = 'Ubah Password';
$_['text_success'] = 'Anda telah mengubah Pemesanan Pick Up';
$_['text_no_item'] = 'Tidak ada data';
$_['text_search'] = 'Cari';
$_['text_md_edit_order'] = 'Ubah Pesanan';
$_['button_receive'] = 'Goods Receive';
$_['button_pickup'] = 'Pick Up';
$_['label_column_view'] = 'Lihat';
$_['label_column_action'] = 'Aksi';
$_['label_column_status'] = 'Status';
$_['label_column_telephone'] = 'No. Telepon';
$_['label_column_recipient'] = 'Penerima';
$_['label_column_customer'] = 'Pelanggan';
$_['label_column_order'] = 'Pesanan #';

$_['text_order_detail'] = 'Detail Pesanan';
$_['text_invoice_no'] = 'No. Invoice';
$_['text_order_id'] = 'No. Pesanan';
$_['text_date_added'] = 'Tanggal Pesanan';
$_['text_payment_method'] = 'Metode Pembayaran';

$_['text_order_pickup'] = 'Pelanggan';
$_['text_order_name'] = 'Nama';
$_['text_order_email'] = 'Email';
$_['text_order_phone'] = 'Telepon';
$_['text_order_product'] = 'Daftar Produk';
$_['text_order_product_name'] = 'Produk';
$_['text_order_product_model'] = 'Model';
$_['text_order_product_option'] = 'Pilihan';
?>