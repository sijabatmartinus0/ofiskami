<?php
// Text
$_['heading_title'] = 'Pick Up Point History';
$_['pp_heading_nav'] = 'Quick Menu';
$_['pp_account_dashboard_nav'] = 'Dashboard';
$_['pp_account_history_nav'] = 'History';
$_['pp_change_password'] = 'Ubah Password';
$_['text_success'] = 'You have been updated Pick Up Order';
$_['text_no_item'] = 'No Item';
$_['text_search'] = 'Search';
$_['text_md_edit_order'] = 'Update Order';
$_['button_view'] = 'View';
$_['label_column_view'] = 'View';
$_['label_column_action'] = 'Action';
$_['label_column_status'] = 'Status';
$_['label_column_telephone'] = 'Phone No.';
$_['label_column_recipient'] = 'Recipient';
$_['label_column_customer'] = 'Customer';
$_['label_column_order'] = 'Order #';

$_['text_order_detail'] = 'Order Detail';
$_['text_invoice_no'] = 'Invoice No.';
$_['text_order_id'] = 'Order No.';
$_['text_date_added'] = 'Order Date';
$_['text_payment_method'] = 'Payment Method';

$_['text_order_pickup'] = 'Customer';
$_['text_order_name'] = 'Name';
$_['text_order_email'] = 'Email';
$_['text_order_phone'] = 'Phone';
$_['text_order_product'] = 'Product List';
$_['text_order_product_name'] = 'Product';
$_['text_order_product_model'] = 'Model';
$_['text_order_product_option'] = 'Option';


?>