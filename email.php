<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Aktivasi Email</title>
<style>
/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
</style>
</head>

<body bgcolor="#f6f6f6" style='font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;'>

<!-- body -->
<table class="body-wrap" style="padding: 20px;width: 100%;" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" style=' clear: both !important;display: block !important;Margin: 0 auto !important;max-width: 600px !important;border: 1px solid #f0f0f0;padding: 20px;' bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content" style='display: block;margin: 0 auto;max-width: 600px;border-bottom:1px solid #cecece;'>
      <table>
        <tr>
          <td>
			<h2>Mengaktifkan Akun Ofiskita</h2>
            <p>Hi Rizki,</p>
            <p>Terima kasih sudah mendaftar untuk bergabung dengan Ofiskita, marketplace online nomor satu di Indonesia. Untuk menyelesaikan proses pendaftaran, mohon lakukan konfirmasi pendaftaran melalui tombol di bawah ini.</p>
            <!-- button -->
            <table class="btn-primary" style='Margin-bottom: 10px;width: 100% !important;' cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style='background-color: #348eda;font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px;text-align: center;vertical-align: top; '>
                  <a style='background-color: #348eda;border: solid 1px #348eda;border-width: 10px 20px;display: inline-block;color: #ffffff;cursor: pointer;font-weight: bold;line-height: 2;text-decoration: none;' href="https://github.com/leemunroe/html-email-template">Aktifkan</a>
                </td>
              </tr>
            </table>
            <!-- /button -->
            <p>Jika button di atas tidak berfungsi, silahkan copy paste alamat <a style="color: #348eda;" href="#">link</a> ke dalam browser anda.</p>
		  </td>
        </tr>
      </table>
      </div>
	  <div class="content-info" style='display: block;margin: 10px auto;max-width: 600px;border-bottom:1px solid #cecece;'>
      <table>
        <tr>
          <td>
			<table class="info" style="width: 100% !important;" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" style='background-color: #e14169; font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; text-align: center;vertical-align: top;color:#fff;'>
                  <p>Hati-hati terhadap pihak yang mengaku dari Ofiskita, membagikan voucher belanja atau meminta data pribadi maupun channel lainnya. Untuk semua email dengan link dari Ofiskita pastikan alamat URL di browser sudah di alamat <a href="#">ofiskita.com</a> bukan alamat lainnya.</p>
                </td>
              </tr>
            </table>
			<p></p>
		  </td>
        </tr>
      </table>
      </div>
	  
	  <div class="content-copyright" style='display: block;margin: 10px auto;max-width: 600px;font-size:10px !important;'>
      <table>
        <tr>
          <td>
			2015@Ofiskita.com
		  </td>
        </tr>
      </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap" style="clear: both !important;width: 100%;">
  <tr>
    <td></td>
    <td class="container">
	
      <!-- content -->
      <div class="content">
        <table>
          <tr>
            <td align="center">
              <p style="color: #666666;font-size: 12px;">Email ini mengganggu anda? <a style="color: #999999;" href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.
              </p>
            </td>
          </tr>
        </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /footer -->

</body>
</html>