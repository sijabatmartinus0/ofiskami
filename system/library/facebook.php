<?php
require_once(DIR_LIBRARY.'login/facebook/autoload.php');
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class Facebook {
	private $helper;
	public function __construct($registry) {
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->url = $registry->get('url');
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->response = $registry->get('response');
		// init app with app id and secret
		FacebookSession::setDefaultApplication( '438566336351793','39ea76e3d14bdf1cbe0d5c90f81d7544' );
		// login helper with redirect_uri
		$this->helper = new FacebookRedirectLoginHelper($this->url->link('account/login/facebook', '', 'SSL'));
	}

	public function login() {
		try {
		  $session = $this->helper->getSessionFromRedirect();
		} catch( FacebookRequestException $ex ) {
		  // When Facebook returns an error
		} catch( Exception $ex ) {
		  // When validation fails or other local issues
		}
		if ( isset( $session ) ) {
			$request = new FacebookRequest( $session, 'GET', '/me' );
			$response = $request->execute();
			// get response
			$graphObject = $response->getGraphObject();
			$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
			$fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
			$femail = $graphObject->getProperty('email');    // To Get Facebook email ID
			$customer=$this->getCustomer($femail);

			if($customer){
				if((int)$customer['login_type']==2){
					$this->customer->login($femail, '', true);
					
					unset($this->session->data['guest']);

					// Default Shipping Address
					$this->load->model('account/address');

					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
					}

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
					);

					$this->model_account_activity->addActivity('login', $activity_data);

					if (isset($this->request->post['redirect'])){
						$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
					} else {
						$this->response->redirect($this->url->link('account/account', '', 'SSL'));
					}
					
					$this->response->redirect($this->url->link('account/account', '', 'SSL'));
					
				}else{
					$this->response->redirect($this->url->link('account/login', '', 'SSL'));
				}
			}else{
				$this->session->data['facebook']['id']=$fbid;
				$this->session->data['facebook']['name']=$fbfullname;
				$this->session->data['facebook']['email']=$femail;
				unset($this->session->data['twitter']);
				unset($this->session->data['google']);
				$this->response->redirect($this->url->link('account/register', '', 'SSL'));
			}
		} else {
			$loginUrl = $this->helper->getLoginUrl(array('email'));
			$this->response->redirect($loginUrl);
		}
	}
	
	private function getCustomer($email){
		$sql = "SELECT * FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) ."'";
		
		$res = $this->db->query($sql);
		if($res->num_rows > 0){
			return $res->row;
		}else{
			return false;
		}
	}
}