<?php
class Cart {
	private $config;
	private $db;
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');
		$this->rajaongkir = $registry->get('rajaongkir');

		if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
			$this->session->data['cart'] = array();
		}else{
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");
			if ($customer_query->num_rows>0 && $customer_query->row['cart'] && is_string($customer_query->row['cart'])) {
				$cart = unserialize($customer_query->row['cart']);
				unset($this->session->data['cart']);
				$this->session->data['cart'] = array();
				foreach ($cart as $key => $value) {
					if (!array_key_exists($key, $this->session->data['cart'])) {
						$this->session->data['cart'][$key] = $value;
					} else {
						$this->session->data['cart'][$key] += $value;
					}
				}
			}else{
				$this->session->data['cart'] = array();
			}
		}
	}

	public function getProducts() {
		
		if (!$this->data) {
			foreach ($this->session->data['cart'] as $key => $quantity) {
				$product = unserialize(base64_decode($key));
				$product_id = $product['product_id'];

				$stock = true;

				// Options
				if (!empty($product['option'])) {
					$options = $product['option'];
				} else {
					$options = array();
				}

				// Profile
				if (!empty($product['recurring_id'])) {
					$recurring_id = $product['recurring_id'];
				} else {
					$recurring_id = 0;
				}
				
				if(!isset($product['shipping'])){
					$product['shipping']=array();
				}

				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");
				/*echo "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'";die();
*/
				if ($product_query->num_rows) {
					$option_price = 0;
					$option_points = 0;
					$option_weight = 0;

					$option_data = array();

					foreach ($options as $product_option_id => $value) {
						$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

						if ($option_query->num_rows) {
							if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}

									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}

									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
										$stock = false;
									}

									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $value,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);
								}
							} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
								foreach ($value as $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}

										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}

										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}

										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
											$stock = false;
										}

										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $product_option_value_id,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'value'                   => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);
									}
								}
							} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => '',
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => '',
									'name'                    => $option_query->row['name'],
									'value'                   => $value,
									'type'                    => $option_query->row['type'],
									'quantity'                => '',
									'subtract'                => '',
									'price'                   => '',
									'price_prefix'            => '',
									'points'                  => '',
									'points_prefix'           => '',
									'weight'                  => '',
									'weight_prefix'           => ''
								);
							}
						}
					}

					$price = $product_query->row['price'];
					$price_normal = $product_query->row['price'];

					// Product Discounts
					$discount_quantity = 0;

					foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
						$product_2 = (array)unserialize(base64_decode($key_2));

						if ($product_2['product_id'] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}

					$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

					if ($product_discount_query->num_rows) {
						$price = $product_discount_query->row['price'];
					}

					// Product Specials
					$product_special_query = $this->db->query("SELECT coalesce(sum(price)) as price, sum(discount) as discount FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC");

					if ($product_special_query->num_rows) {
						if($product_special_query->row['discount'] > 0 && $product_special_query->row['price'] > 0){
                          $price = $product_special_query->row['price']-$product_special_query->row['discount'];
                        }
                        else if ($product_special_query->row['discount'] > 0){                    
                          $price = $product_query->row['price']-$product_special_query->row['discount'];
                        }
                        else if($product_special_query->row['price'] > 0){                    
                          $price = $product_special_query->row['price'];
                        }
                        else {
                          $price = $product_info['price'];   
                        }
                        			/*var_dump($product_query->row['price']);
                                    var_dump($product_special_query->row['price']);
                                    var_dump($product_special_query->row['discount']);
                                    var_dump($price);die();*/
					}

					// Reward Points
					$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

					if ($product_reward_query->num_rows) {
						$reward = $product_reward_query->row['points'];
					} else {
						$reward = 0;
					}

					// Downloads
					$download_data = array();

					$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					foreach ($download_query->rows as $download) {
						$download_data[] = array(
							'download_id' => $download['download_id'],
							'name'        => $download['name'],
							'filename'    => $download['filename'],
							'mask'        => $download['mask']
						);
					}

					// Stock
					if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
						$stock = false;
					}

					$recurring_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` `p` JOIN `" . DB_PREFIX . "product_recurring` `pp` ON `pp`.`recurring_id` = `p`.`recurring_id` AND `pp`.`product_id` = " . (int)$product_query->row['product_id'] . " JOIN `" . DB_PREFIX . "recurring_description` `pd` ON `pd`.`recurring_id` = `p`.`recurring_id` AND `pd`.`language_id` = " . (int)$this->config->get('config_language_id') . " WHERE `pp`.`recurring_id` = " . (int)$recurring_id . " AND `status` = 1 AND `pp`.`customer_group_id` = " . (int)$this->config->get('config_customer_group_id'));

					if ($recurring_query->num_rows) {
						$recurring = array(
							'recurring_id'    => $recurring_id,
							'name'            => $recurring_query->row['name'],
							'frequency'       => $recurring_query->row['frequency'],
							'price'           => $recurring_query->row['price'],
							'cycle'           => $recurring_query->row['cycle'],
							'duration'        => $recurring_query->row['duration'],
							'trial'           => $recurring_query->row['trial_status'],
							'trial_frequency' => $recurring_query->row['trial_frequency'],
							'trial_price'     => $recurring_query->row['trial_price'],
							'trial_cycle'     => $recurring_query->row['trial_cycle'],
							'trial_duration'  => $recurring_query->row['trial_duration']
						);
					} else {
						$recurring = false;
					}

					$this->data[$key] = array(
						'key'             => $key,
						'product_id'      => $product_query->row['product_id'],
						'name'            => $product_query->row['name'],
						'model'           => $product_query->row['model'],
						'shipping'        => $product['shipping'],
						'image'           => $product_query->row['image'],
						'option'          => $option_data,
						'download'        => $download_data,
						'quantity'        => $quantity,
						'minimum'         => $product_query->row['minimum'],
						'maximum'         => $product_query->row['maximum'],
						'subtract'        => $product_query->row['subtract'],
						'stock'           => $stock,
						'price'           => ($price + $option_price),
						'total'           => ($price + $option_price) * $quantity,
						'price_normal'           => ($price_normal + $option_price),
						'total_normal'           => ($price_normal + $option_price) * $quantity,
						'reward'          => $reward * $quantity,
						'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
						'tax_class_id'    => $product_query->row['tax_class_id'],
						'weight'          => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id' => $product_query->row['weight_class_id'],
						'length'          => $product_query->row['length'],
						'width'           => $product_query->row['width'],
						'height'          => $product_query->row['height'],
						'length_class_id' => $product_query->row['length_class_id'],
						'recurring'       => $recurring
					);
				} else {
					$this->remove($key);
				}
			}
		}

		return $this->data;
	}

	public function getRecurringProducts() {
		$recurring_products = array();

		foreach ($this->getProducts() as $key => $value) {
			if ($value['recurring']) {
				$recurring_products[$key] = $value;
			}
		}

		return $recurring_products;
	}

	public function add($product_id, $qty = 0, $option = array(), $recurring_id = 0, $shipping = array()) {
		
		$this->data = array();

		$product['product_id'] = (int)$product_id;

		if ($option) {
			$product['option'] = $option;
		}

		if ($recurring_id) {
			$product['recurring_id'] = (int)$recurring_id;
		}
		
		if($shipping){
			$product['shipping'] = $shipping;
		}

		//UPDATE GROUPING
		foreach ($this->session->data['cart'] as $key => $quantity) {
			$productGroup = unserialize(base64_decode($key));
			if(isset($product['shipping']['delivery']) && isset($productGroup['shipping']['delivery'])){
				if($productGroup['shipping']['delivery']['shipping_address']==$product['shipping']['delivery']['shipping_address'] && $productGroup['shipping']['delivery']['seller_id']==$product['shipping']['delivery']['seller_id'] && $productGroup['shipping']['delivery']['shipping_service_id']==$product['shipping']['delivery']['shipping_service_id']){
					$productResult=$productGroup;
					$productResult['shipping']['delivery']['price']=$product['shipping']['delivery']['price'];
		
					//UNSET
					$key = base64_encode(serialize($productGroup));
					unset($this->session->data['cart'][$key]);
					//SET
					$key = base64_encode(serialize($productResult));
					if ((int)$quantity && ((int)$quantity > 0)) {
						if (!isset($this->session->data['cart'][$key])) {
							$this->session->data['cart'][$key] = (int)$quantity;
						} else {
							$this->session->data['cart'][$key] += (int)$quantity;
						}
					}
				}
			}
		}
		
		$key = base64_encode(serialize($product));

		if ((int)$qty && ((int)$qty > 0)) {
			if (!isset($this->session->data['cart'][$key])) {
				$this->session->data['cart'][$key] = (int)$qty;
			} else {
				$this->session->data['cart'][$key] += (int)$qty;
			}
		}
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "' WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");
	}

	public function updateShipping($keys, $shipping= array()){
		foreach ($keys as $key) {
			//GET QUANTITY
			$quantity=$this->session->data['cart'][$key];
			$product=unserialize(base64_decode($key));
			//UNSET
			unset($this->session->data['cart'][$key]);
			$newProduct = unserialize(base64_decode($key));
			$newProduct['shipping']=$shipping;
			$newProduct['shipping']['information']=$product['shipping']['information'];
			
			$newKey = base64_encode(serialize($newProduct));
			
			if ((int)$quantity && ((int)$quantity > 0)) {
				if (!isset($this->session->data['cart'][$newKey])) {
					$this->session->data['cart'][$newKey] = (int)$quantity;
				} else {
					$this->session->data['cart'][$newKey] += (int)$quantity;
				}
			}
		}
		
		//UPDATE GROUPING
		foreach ($this->session->data['cart'] as $key => $quantity) {
			$productGroup = unserialize(base64_decode($key));
			if(isset($shipping['delivery']) && isset($productGroup['shipping']['delivery'])){
				if($productGroup['shipping']['delivery']['shipping_address']==$shipping['delivery']['shipping_address'] && $productGroup['shipping']['delivery']['seller_id']==$shipping['delivery']['seller_id'] && $productGroup['shipping']['delivery']['shipping_service_id']==$shipping['delivery']['shipping_service_id']){
					//echo "CARTOLD".json_encode($this->session->data['cart']);
					if($productGroup['shipping']['delivery']['price']!=$shipping['delivery']['price']){
						$productResult=$productGroup;
						$productResult['shipping']['delivery']['price']=$shipping['delivery']['price'];
			
						//UNSET
						unset($this->session->data['cart'][$key]);
						//SET
						$newKey = base64_encode(serialize($productResult));
						//echo $quantity."AAAAAAAAAAAA".json_encode($productGroup)."CART".json_encode($this->session->data['cart']);
						//echo $quantity."AAAAAAAAAAAANNNNN".json_encode($productResult);
						if ((int)$quantity && ((int)$quantity > 0)) {
							if (!isset($this->session->data['cart'][$newKey])) {
								//echo "BBBBBBBBBBBBB".json_encode($productGroup);
								$this->session->data['cart'][$newKey] = (int)$quantity;
							} else {
								//echo "CCCCCCCCCC".json_encode($productGroup);
								$this->session->data['cart'][$newKey] += (int)$quantity;
							}
						}
					}
				}
			}
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "' WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");

	}
	
	public function update($key, $qty, $shipping = array()) {
		$this->data = array();
		//REMOVE OLD CART
		unset($this->session->data['cart'][$key]);
		
		$newProduct = unserialize(base64_decode($key));
		$newProduct['shipping']=$shipping;
		
		$key = base64_encode(serialize($newProduct));
		
		$this->session->data['cart'][$key] = (int)$qty;
		
		//UPDATE GROUPING
		foreach ($this->session->data['cart'] as $key => $quantity) {
			$productGroup = unserialize(base64_decode($key));
			if(isset($shipping['delivery']) && isset($productGroup['shipping']['delivery'])){
				if($productGroup['shipping']['delivery']['shipping_address']==$shipping['delivery']['shipping_address'] && $productGroup['shipping']['delivery']['seller_id']==$shipping['delivery']['seller_id'] && $productGroup['shipping']['delivery']['shipping_service_id']==$shipping['delivery']['shipping_service_id']){
					$productResult=$productGroup;
					$productResult['shipping']['delivery']['price']=$shipping['delivery']['price'];
		
					//UNSET
					$key = base64_encode(serialize($productGroup));
					unset($this->session->data['cart'][$key]);
					//SET
					$key = base64_encode(serialize($productResult));
					$this->session->data['cart'][$key] = (int)$quantity;
					
				}
			}
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "' WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");

	}

	public function remove($key) {
		$this->data = array();
		$response=true;
		if(is_array($key)){
			//CHECK GROUP
			$delivery_shipping_address="";
			$delivery_seller_id="";
			$delivery_shipping_service_id="";
			$pickup_seller_id="";
			$pickup_pp_branch="";
			
			$count=0;
			$err=0;
			foreach($key as $x){
				$product = unserialize(base64_decode($x));
				if($count>0){
					if(isset($product['shipping']['delivery'])){
						if($product['shipping']['delivery']['shipping_address']!=$delivery_shipping_address || $product['shipping']['delivery']['seller_id']!=$delivery_seller_id || $product['shipping']['delivery']['shipping_service_id']!=$delivery_shipping_service_id){
							$err++;
							break;
						}
					}
					if(isset($product['shipping']['pickup'])){
						if($product['shipping']['pickup']['seller_id']!=$pickup_seller_id || $product['shipping']['pickup']['branch']!=$pickup_pp_branch){
							$err++;
							break;
						}
					}
				}else{
					if(isset($product['shipping']['delivery'])){
						$delivery_shipping_address=$product['shipping']['delivery']['shipping_address'];
						$delivery_seller_id=$product['shipping']['delivery']['seller_id'];
						$delivery_shipping_service_id=$product['shipping']['delivery']['shipping_service_id'];
					}
					if(isset($product['shipping']['pickup'])){
						$pickup_seller_id=$product['shipping']['pickup']['seller_id'];
						$pickup_pp_branch=$product['shipping']['pickup']['branch'];
					}
				}
				$count++;
			}
			if($err==0){
				foreach($key as $x){
					unset($this->session->data['cart'][$x]);
				}
			}
		}else{
			$product = unserialize(base64_decode($key));
			unset($this->session->data['cart'][$key]);
			$err=0;
			foreach ($this->session->data['cart'] as $key => $quantity) {
				$productGroup = unserialize(base64_decode($key));
				if(isset($product['shipping']['delivery']) && isset($productGroup['shipping']['delivery'])){
					if($productGroup['shipping']['delivery']['shipping_address']==$product['shipping']['delivery']['shipping_address'] && $productGroup['shipping']['delivery']['seller_id']==$product['shipping']['delivery']['seller_id'] && $productGroup['shipping']['delivery']['shipping_service_id']==$product['shipping']['delivery']['shipping_service_id']){
						$err++;
						break;
					}
				}
			}
			//Update Cart
			if($err>0){
				if(isset($product['shipping']['delivery'])){
					$DESTINATION=$product['shipping']['delivery']['destination_ro'];		
					$ORIGIN=$product['shipping']['delivery']['origin_ro'];
					$SELLER_ID=$product['shipping']['delivery']['seller_id'];
					$ADDRESS_ID=$product['shipping']['delivery']['shipping_address'];
					$SERVICE_ID=$product['shipping']['delivery']['shipping_service_id'];
					$SHIPPING_ID=$product['shipping']['delivery']['shipping_id'];
					$WEIGHT=$this->getWeightShipping(
					array(
						'seller_id'=>$SELLER_ID,
						'shipping_address'=>$ADDRESS_ID,
						'shipping_service_id'=>$SERVICE_ID
						)
					);
					$shipping_code=$this->getShippingCode($SHIPPING_ID);
						
					$logisticRO=$this->rajaongkir->getServiceRajaOngkir($ORIGIN,$DESTINATION,$WEIGHT,$shipping_code);
					
					if(!empty($logisticRO)){
						$shipping_service=$this->getShippingService($SERVICE_ID);
						$PRICE=$this->getPriceShippingRO($logisticRO,$shipping_code,$shipping_service);
						//SET NEW SHIPPING
						
						foreach ($this->session->data['cart'] as $key => $quantity) {
							$productGroup = unserialize(base64_decode($key));
							if(isset($productGroup['shipping']['delivery'])){
								if($productGroup['shipping']['delivery']['shipping_address']==$ADDRESS_ID && $productGroup['shipping']['delivery']['seller_id']==$SELLER_ID && $productGroup['shipping']['delivery']['shipping_service_id']==$SERVICE_ID){
									$productResult=$productGroup;
									$productResult['shipping']['delivery']['price']=$PRICE;
									//UNSET
									$key = base64_encode(serialize($productGroup));
									unset($this->session->data['cart'][$key]);
									//SET
									$key = base64_encode(serialize($productResult));
									if ((int)$quantity && ((int)$quantity > 0)) {
										if (!isset($this->session->data['cart'][$key])) {
											$this->session->data['cart'][$key] = (int)$quantity;
										} else {
											$this->session->data['cart'][$key] += (int)$quantity;
										}
									}
								}
							}
						}
					}else{
						$response=false;
					}
				}
			}
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "' WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");

		return $response;
	}
	public function removeVoucher($key) {
		$this->data = array();

		unset($this->session->data['vouchers'][$key]);
	}

	public function clear() {
		$this->data = array();

		$this->session->data['cart'] = array();
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "' WHERE customer_id = '" . (isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0') . "'");
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}
	
	public function getWeightShipping($data = array()) {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				if(isset($product['shipping']['delivery'])){
					if($data['shipping_address']==$product['shipping']['delivery']['shipping_address'] && $data['seller_id']==$product['shipping']['delivery']['seller_id'] && $data['shipping_service_id']==$product['shipping']['delivery']['shipping_service_id']){
						$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
					}
				}
			}
		}
		return $weight;
	}
	
	public function getWeightShippingOthers($data = array(),$key = array()){
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				if(isset($product['shipping']['delivery'])){
					if($data['shipping_address']==$product['shipping']['delivery']['shipping_address'] && $data['seller_id']==$product['shipping']['delivery']['seller_id'] && $data['shipping_service_id']==$product['shipping']['delivery']['shipping_service_id'] && !in_array($product['key'], $key)){
						$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
					}
				}
			}
		}
		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->session->data['cart']);
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}
		
		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}
	
	public function getShipping() {
		$shipping = array();

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$product['shipping']['product_id']=$product['product_id'];
				$shipping[] = $product['shipping'];
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}
	
	private function getShippingCode($shipping_id){
		$result="";
		$sql="SELECT code FROM " . DB_PREFIX . "shipping WHERE shipping_id=".(int)$shipping_id;
		$query = $this->db->query($sql);

		if(isset($query->row['code'])){
			$result=$query->row['code'];
		}
		return $result;
	}
	
	private function getShippingService($shipping_service_id){
		$result="";
		$sql="SELECT name FROM " . DB_PREFIX . "shipping_service WHERE shipping_service_id=".(int)$shipping_service_id;
		$query = $this->db->query($sql);

		if(isset($query->row['name'])){
			$result=$query->row['name'];
		}
		return $result;
	}
	
	private function getPriceShippingRO($logisticRO,$shipping_code,$shipping_service){
		$price=0;
		$comparedLogisticRO=array();
		foreach($logisticRO as $itemRO){
			foreach($itemRO['costs'] as $costRO){
				$temp=array();
				if(isset($costRO['service'])){
					if($itemRO['code']==$shipping_code && $costRO['service']==$shipping_service){
						if($costRO['cost'][0]['value']){
							$price=$costRO['cost'][0]['value'];
							break;
						}
					}
				}
			}
		}
		return $price;
		//return 0;
	}
	
	public function getCartAddressAvailable($address_id){
		$count = 0;

		foreach ($this->getProducts() as $product) {
			if(isset($product['shipping']['delivery'])){
				if((int)$product['shipping']['delivery']['shipping_address']==(int)$address_id){
					$count++;
				}
			}
		}

		return $count;
	}
	
}
