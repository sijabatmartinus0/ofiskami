<?php
class Compare {
	private $total;
	public function __construct($registry) {
		$this->session = $registry->get('session');
	}
	public function getTotal() {
	
		if(isset($this->session->data['compare'])){
			$total=0;
			foreach ($this->session->data['compare'] as $value){
				$total+=count($value);
			}
			$this->total=$total;
		}else{
			$this->total=0;
		}
		return $this->total;
	}
}