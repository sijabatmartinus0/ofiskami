<?php class ReCaptcha {
	public function __construct() {
	
	}
	public function verify($CAPTCHA){
		$result=false;
		$SECRET = "6LfjVh4TAAAAAP7TzOwrNahPnZc_e_rGhyDfW9xx";
		try{
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => 
"https://www.google.com/recaptcha/api/siteverify?secret=".$SECRET."&response=".$CAPTCHA."&ip".$_SERVER['REMOTE_ADDR'],
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_USERAGENT => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16",
			  CURLOPT_TIMEOUT => 30,
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);			
			$data=json_decode($response, true);
			//echo $response;
			if ($data['success']) {
			  $result=true;
		    }
		}catch(Exception $e){
			$result=false;
		}
		return $result;
	}
}
