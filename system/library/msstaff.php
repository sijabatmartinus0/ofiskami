<?php
class MsStaff extends Model {
	const STATUS_DELETED = 2;
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;
	
	const PASSWORD_DEFAULT = 'Passw0rd';
	
	private $errors;
	
	public function getStaffs($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';

		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`oms.date_created`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}
		
		// todo validate order parameters
		$sql = "SELECT
					oms.seller_id as 'seller_id',
					oc.firstname as 'name',
					oc.email as 'email',
					oc.status as 'status',
					oms.date_created as 'date_created'
				FROM " . DB_PREFIX . "ms_seller oms
				LEFT JOIN " . DB_PREFIX . "customer oc 
					ON oc.customer_id=oms.seller_id
				WHERE 1 = 1 AND seller_id<>".(int)$data['seller_group']

				. (isset($data['seller_group']) ? " AND oms.seller_group =  " .  (int)$data['seller_group'] : '')
				. (isset($data['status']) ? " AND oc.status IN  (" .  $this->db->escape(implode(',', $data['status'])) . ")" : '')
				
				. $wFilters
				
				. $hFilters
				
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];

		return $res->rows;
	}
	
	public function staffOneGroupBySeller($staff_id, $seller_id){
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "ms_seller`
				WHERE seller_group = " . (int)$seller_id . " 
				AND seller_id = " . (int)$staff_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['total'];			
	}
	
	public function changeStatus($staff_id, $status) {
		$sql = "UPDATE " . DB_PREFIX . "customer
				SET	status =  " .  (int)$status . "
				WHERE customer_id = " . (int)$staff_id;
		
		$res = $this->db->query($sql);
	}
	
	public function deleteUser($staff_id){
		
	}
	
	public function getRoles(){
		$sql = "SELECT
					*
				FROM " . DB_PREFIX . "ms_roles omr
				WHERE 1 = 1 AND omr.status=1 ";

		$res = $this->db->query($sql);
		
		return $res->rows;
	}
	
	public function getStaff($staff_id){
		$sql = "SELECT 	
				oms.seller_id,
				oc.firstname,
				oc.lastname,
				oms.nickname,
				oc.email,
				oc.telephone,
				oc.dob,
				oms.roles
				FROM `" . DB_PREFIX . "customer` oc
				LEFT JOIN `" . DB_PREFIX . "ms_seller` oms
					ON oms.seller_id = oc.customer_id
				WHERE oc.customer_id = " . (int)$staff_id;
		$res = $this->db->query($sql);

		if (!$res->num_rows) return FALSE;

		return $res->row;
	}
	
	public function createUser($data){
		
		$avatar = isset($data['avatar_name']) ? $this->MsLoader->MsFile->moveImage($data['avatar_name']) : '';
		$banner = isset($data['banner_name']) ? $this->MsLoader->MsFile->moveImage($data['banner_name']) : '';

		if (isset($data['commission']))
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		
		$sql = "INSERT INTO " . DB_PREFIX . "ms_seller
				SET seller_id = " . (int)$data['seller_id'] . ",
					seller_status = " . (isset($data['status']) ? (int)$data['status'] : self::STATUS_INACTIVE) . ",
					seller_approved = " . (isset($data['approved']) ? (int)$data['approved'] : 0) . ",
					seller_group = " .  (isset($data['seller_group']) ? (int)$data['seller_group'] : $this->config->get('msconf_default_seller_group_id'))  .  ",
					nickname = '" . $this->db->escape($data['nickname']) . "',
					description = '" . $this->db->escape(isset($data['description']) ? $data['description'] : '') . "',
					company = '" . $this->db->escape(isset($data['company']) ? $data['company'] : '') . "',
					dob = '" . $this->db->escape($data['dob']) . "',
					country_id = " . (isset($data['country']) ? (int)$data['country'] : 0) . ",
					zone_id = " . (isset($data['zone']) ? (int)$data['zone'] : 0) . ",
					city_id = " . (isset($data['city']) ? (int)$data['city']: 0) . ",
					district_id = " . (isset($data['district']) ? (int)$data['district']: 0) . ",
					subdistrict_id = " . (isset($data['subdistrict']) ? (int)$data['subdistrict']: 0) . ",
					postcode = '" . (isset($data['postcode']) ? $this->db->escape($data['postcode']): 0) . "',
					account_id = " . (isset($data['account']) ? (int)$data['account']: 0) . ",
					account_name = '" . (isset($data['account_name']) ?  $this->db->escape($data['account_name']): '') . "',
					account_number = '" .(isset($data['account_number']) ?  $this->db->escape($data['account_number']): '') . "',
					address = '" . (isset($data['address']) ? $this->db->escape($data['address']): '') . "',
					npwp_number = '" . (isset($data['npwp_number']) ? $this->db->escape($data['npwp_number']): '') . "',
					npwp_address = '" . (isset($data['npwp_address']) ? $this->db->escape($data['npwp_address']): '') . "',
					telephone = '" . $this->db->escape($data['telephone']) . "',
					commission_id = " . (isset($commission_id) ? $commission_id : 'NULL') . ",
					product_validation = " . (isset($data['product_validation']) ? (int)$data['product_validation'] : 0) . ",
					paypal = '" . $this->db->escape(isset($data['paypal']) ? $data['paypal'] : '') . "',
					avatar = '" . $this->db->escape($avatar) . "',
					banner = '" . $this->db->escape($banner) . "',
					seller_type = '" . $this->db->escape(isset($data['seller_type']) ? $data['seller_type'] : '') . "',
					premium_status= '" . $this->db->escape(isset($data['premium_status']) ? $data['premium_status'] : '') . "',
					roles = '" . $this->db->escape(isset($data['roles']) ? serialize($data['roles']) : '') . "',
					date_created = NOW()";

		$this->db->query($sql);
		$seller_id = $this->db->getLastId();
	}
	
	public function editUser($data){
		
		$avatar = isset($data['avatar_name']) ? $this->MsLoader->MsFile->moveImage($data['avatar_name']) : '';
		$banner = isset($data['banner_name']) ? $this->MsLoader->MsFile->moveImage($data['banner_name']) : '';

		if (isset($data['commission']))
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET seller_status = " . (isset($data['status']) ? (int)$data['status'] : self::STATUS_INACTIVE) . ",
					seller_approved = " . (isset($data['approved']) ? (int)$data['approved'] : 0) . ",
					seller_group = " .  (isset($data['seller_group']) ? (int)$data['seller_group'] : $this->config->get('msconf_default_seller_group_id'))  .  ",
					nickname = '" . $this->db->escape($data['nickname']) . "',
					description = '" . $this->db->escape(isset($data['description']) ? $data['description'] : '') . "',
					company = '" . $this->db->escape(isset($data['company']) ? $data['company'] : '') . "',
					dob = '" . $this->db->escape($data['dob']) . "',
					country_id = " . (isset($data['country']) ? (int)$data['country'] : 0) . ",
					zone_id = " . (isset($data['zone']) ? (int)$data['zone'] : 0) . ",
					city_id = " . (isset($data['city']) ? (int)$data['city']: 0) . ",
					district_id = " . (isset($data['district']) ? (int)$data['district']: 0) . ",
					subdistrict_id = " . (isset($data['subdistrict']) ? (int)$data['subdistrict']: 0) . ",
					postcode = '" . (isset($data['postcode']) ? $this->db->escape($data['postcode']): 0) . "',
					account_id = " . (isset($data['account']) ? (int)$data['account']: 0) . ",
					account_name = '" . (isset($data['account_name']) ?  $this->db->escape($data['account_name']): '') . "',
					account_number = '" .(isset($data['account_number']) ?  $this->db->escape($data['account_number']): '') . "',
					address = '" . (isset($data['address']) ? $this->db->escape($data['address']): '') . "',
					npwp_number = '" . (isset($data['npwp_number']) ? $this->db->escape($data['npwp_number']): '') . "',
					npwp_address = '" . (isset($data['npwp_address']) ? $this->db->escape($data['npwp_address']): '') . "',
					telephone = '" . $this->db->escape($data['telephone']) . "',
					commission_id = " . (isset($commission_id) ? $commission_id : 'NULL') . ",
					product_validation = " . (isset($data['product_validation']) ? (int)$data['product_validation'] : 0) . ",
					paypal = '" . $this->db->escape(isset($data['paypal']) ? $data['paypal'] : '') . "',
					avatar = '" . $this->db->escape($avatar) . "',
					banner = '" . $this->db->escape($banner) . "',
					seller_type = '" . $this->db->escape(isset($data['seller_type']) ? $data['seller_type'] : '') . "',
					premium_status= '" . $this->db->escape(isset($data['premium_status']) ? $data['premium_status'] : '') . "',
					roles = '" . $this->db->escape(isset($data['roles']) ? serialize($data['roles']) : '') . "'
					 WHERE seller_id='".(int)$data['seller_id']."'";

		$this->db->query($sql);
		$seller_id = $this->db->getLastId();
	}
	
	public function addCustomerUser($data) {
		$this->event->trigger('pre.customer.add', $data);

		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', dob = '" . $this->db->escape($data['dob']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? serialize($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', hash = '" . $this->db->escape($data['hash']) . "', code = '" . $this->db->escape($data['code']) . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->event->trigger('post.customer.add', $customer_id);

		return $customer_id;
	}

	public function editCustomerUser($data) {
		$this->event->trigger('pre.customer.edit', $data);

		$customer_id = $data['customer_id'];

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', dob = '" . $this->db->escape($data['dob']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->event->trigger('post.customer.edit', $customer_id);
	}
	
}
?>