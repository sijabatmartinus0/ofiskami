<?php
require_once DIR_LIBRARY . '/websocket/Client.php';
use WebSocket\Client;

class WebSocket {
	public function __construct($registry) {
		$this->db = $registry->get('db');
	}

	public function send($data=array()){
		$log = new Log("mail.log");
		try{
			$email='ws.ofiskita@ofiskita.com';
			$password='1fe809c085bcfcfc18b0930b65ce182d8990b7c6';
			$option = array(
				"headers"=>array(
					"Authorization"=>"Basic ".base64_encode($email.":".$password)
				)
			);
			$data['clientId']=$email;
			$data['token']="1fe809c085bcfcfc18b0930b65ce182d8990b7c6";
			$client = new Client("ws://localhost:8888/ws",$option);
			$client->send(json_encode($data));
		
			$response = json_decode($client->receive());
			
			$client_id = "";
			$counter=0;
			foreach($response->data as $client){
				$client_id.=($counter!=0)?',':'';
				$client_id.="\"".$client."\"";
				$counter++;
			}
			
				$sql = "SELECT email
						FROM " . DB_PREFIX . "customer c, " . DB_PREFIX . "pp_user pu
						WHERE c.customer_id=pu.pp_user_id AND pu.is_client_server=1 "
						. (!empty($response->data) ? " AND c.email not in (" . $client_id .")" : '');
				$res = $this->db->query($sql);
				$clients = $res->rows;
				foreach($clients as $client){
					$sql = "INSERT INTO " . DB_PREFIX . "ws_command
							SET message = '" . json_encode($data) . "',
							client_id = '" . $client['email'] . "'" ;
					$this->db->query($sql);
				}
				$log->write("Websocket Success");
		}catch(Exception $e){
			$log->write("Websocket Exception");
			$sql = "SELECT email
						FROM " . DB_PREFIX . "customer c, " . DB_PREFIX . "pp_user pu
						WHERE c.customer_id=pu.pp_user_id AND pu.is_client_server=1 "
						. (!empty($response->data) ? " AND c.email not in (" . $client_id .")" : '');
				$res = $this->db->query($sql);
				$clients = $res->rows;
				foreach($clients as $client){
					$sql = "INSERT INTO " . DB_PREFIX . "ws_command
							SET message = '" . json_encode($data) . "',
							client_id = '" . $client['email'] . "'" ;
					$this->db->query($sql);
				}
		}
	}
}