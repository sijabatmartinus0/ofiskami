<?php
class Payment {

	const METHOD_BALANCE = 1;
	const METHOD_PAYPAL = 2;
	const METHOD_PAYPAL_ADAPTIVE = 3;
	const METHOD_TRANSFER = 4;
	
	
	const TYPE_PAYOUT = 3;
	const TYPE_PAYOUT_REQUEST = 4;
	
		
	const STATUS_UNPAID = 1;
	const STATUS_PAID = 2;
	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->config = $registry->get('config');
		$this->user = $registry->get('user');
		$this->currency = $registry->get('currency');
	}

	public function createPayment($data) {
		$sql = "INSERT INTO `" . DB_PREFIX . "payment`
				SET customer_id = " . (int)$data['customer_id'] . ",
					user_id = " . (isset($data['user_id']) ? (int)$data['user_id'] : 'NULL') . ",
					product_id = " . (isset($data['product_id']) ? (int)$data['product_id'] : 'NULL') . ",
					order_id = " . (isset($data['order_id']) ? (int)$data['order_id'] : 'NULL') . ",
					order_detail_id = " . (isset($data['order_detail_id']) ? (int)$data['order_detail_id'] : 'NULL') . ",
					return_id = " . (isset($data['return_id']) ? (int)$data['return_id'] : 'NULL') . ",
					payment_type = " . (int)$data['payment_type'] . ",
					payment_status = " . (int)$data['payment_status'] . ",
					payment_method = " . (int)$data['payment_method'] . ",
					payment_data = '" . (isset($data['payment_data']) ? $this->db->escape($data['payment_data']) : '') . "',
					amount = ". (float)$this->uniformDecimalPoint($data['amount']) . ",
					currency_id = " . (int)$data['currency_id'] . ",
					currency_code = '" . $this->db->escape($data['currency_code']) . "',
					description = '" . (isset($data['description']) ? $this->db->escape($data['description']) : '') . "',
					date_created = NOW()";

		$this->db->query($sql);
		return $this->db->getLastId();
	}
	
	public function updatePayment($payment_id, $data) {
		$sql = "UPDATE `" . DB_PREFIX . "payment`
				SET payment_id = payment_id"
					. (isset($data['customer_id']) ? ", customer_id = " . (int)$data['customer_id'] : '')
					. (isset($data['product_id']) ? ", product_id = " . (int)$data['product_id'] : '')
					. (isset($data['order_id']) ? ", order_id = " . (int)$data['order_id'] : '')
					. (isset($data['order_detail_id']) ? ", order_detail_id = " . (int)$data['order_detail_id'] : '')
					. (isset($data['return_id']) ? ", return_id = " . (int)$data['return_id'] : '')					
					. (isset($data['payment_type']) ? ", payment_type = " . (int)$data['payment_type'] : '')
					. (isset($data['payment_status']) ? ", payment_status = " . (int)$data['payment_status'] : '')
					. (isset($data['payment_method']) ? ", payment_method = " . (int)$data['payment_method'] : '')
					. (isset($data['payment_data']) ? ", payment_data = '" . $this->db->escape($data['payment_data']) . "'" : '')
					. (isset($data['amount']) ? ", amount = " . (float)$this->uniformDecimalPoint($data['amount']) : '')
					. (isset($data['currency_id']) ? ", currency_id = " . (int)$data['currency_id'] : '')
					. (isset($data['currency_code']) ? ", currency_code = " . $this->db->escape($data['currency_code']) : '')
					. (isset($data['description']) ? ", description = '" . $this->db->escape($data['description']) . "'" : '')
					. (isset($data['date_created']) ? ", date_created = NOW()" : '')
					. (isset($data['date_paid']) && !is_null($data['date_paid']) ? ", date_paid = '" . $this->db->escape($data['date_paid']) . "'" : ", date_paid = NULL") . "
				WHERE payment_id = " . (int)$payment_id;

		return $this->db->query($sql);
	}
	
	public function getPayments($data = array(), $sort = array()) {
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}
		if(isset($sort['filters_table'])) {
			if (isset($sort['filters_table']['filter_date_start']) && isset($sort['filters_table']['filter_date_end']) && $sort['filters_table']['filter_date_start']!='' && $sort['filters_table']['filter_date_end']!='') {
				$filters .= " AND DATE(mpay.date_created) BETWEEN '".$sort['filters_table']['filter_date_start']."' AND '".$sort['filters_table']['filter_date_end']."'";
			}

			if (isset($sort['filters_table']['filter_status'])) {
				if($sort['filters_table']['filter_status']!='' && $sort['filters_table']['filter_status']!=0){
					$filters .= " AND payment_status = '" . (int)$sort['filters_table']['filter_status'] . "'";
				}
			}
		}
		

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					payment_id,
					payment_type,
					payment_status,
					payment_method,
					payment_data,
					amount,
					currency_code,
					mpay.date_created as 'mpay.date_created',
					mpay.date_paid as 'mpay.date_paid',
					mpay.description as 'mpay.description',
					ms.customer_id as 'customer_id',
					ms.firstname,
					product_id,
					order_id
				FROM `" . DB_PREFIX . "payment` mpay
				LEFT JOIN `" . DB_PREFIX . "customer` ms
					USING (customer_id)
				WHERE 1 = 1 "
				. (isset($data['payment_id']) ? " AND payment_id =  " .  (int)$data['payment_id'] : '')
				. (isset($data['customer_id']) ? " AND customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['product_id']) ? " AND product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['order_detail_id']) ? " AND order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['return_id']) ? " AND return_id =  " .  (int)$data['return_id'] : '')
				. (isset($data['currency_id']) ? " AND currency_id =  " .  (int)$data['currency_id'] : '')
				. (isset($data['payment_type']) ? " AND payment_type IN  (" .  $this->db->escape(implode(',', $data['payment_type'])) . ")" : '')
				. (isset($data['payment_method']) ? " AND payment_method IN  (" .  $this->db->escape(implode(',', $data['payment_method'])) . ")" : '')
				. (isset($data['payment_status']) ? " AND payment_status IN  (" .  $this->db->escape(implode(',', $data['payment_status'])) . ")" : '')

				. $filters

				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
				
		$res = $this->db->query($sql);
		
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];
		
		return ($res->num_rows == 1 && isset($data['single']) ? $res->row : $res->rows);
	}
	
	public function getTotalPayments($data){
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "payment`
				WHERE 1 = 1 "
				. (isset($data['payment_id']) ? " AND payment_id =  " .  (int)$data['payment_id'] : '')
				. (isset($data['customer_id']) ? " AND customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['product_id']) ? " AND product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['order_detail_id']) ? " AND order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['return_id']) ? " AND return_id =  " .  (int)$data['return_id'] : '')
				. (isset($data['currency_id']) ? " AND currency_id =  " .  (int)$data['currency_id'] : '')
				. (isset($data['payment_type']) ? " AND payment_type IN  (" .  $this->db->escape(implode(',', $data['payment_type'])) . ")" : '')
				. (isset($data['payment_method']) ? " AND payment_method IN  (" .  $this->db->escape(implode(',', $data['payment_method'])) . ")" : '')
				. (isset($data['payment_status']) ? " AND payment_status IN  (" .  $this->db->escape(implode(',', $data['payment_status'])) . ")" : '');
				
		$res = $this->db->query($sql);
		return $res->row['total'];		
	}
	
	public function getTotalAmount($data) {
		$sql = "SELECT SUM(amount) as 'total'
				FROM `" . DB_PREFIX . "payment`
				WHERE 1 = 1 "
				. (isset($data['customer_id']) ? " AND customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['payment_type']) ? " AND payment_type IN  (" .  $this->db->escape(implode(',', $data['payment_type'])) . ")" : '')
				. (isset($data['payment_status']) ? " AND payment_status IN  (" .  $this->db->escape(implode(',', $data['payment_status'])) . ")" : '');
				
		$res = $this->db->query($sql);

		return $res->row['total'];		
	}	
	
	public function deletePayment($payment_id) {
		$sql = "DELETE FROM `" . DB_PREFIX . "payment`
				WHERE payment_id = " . (int)$payment_id;
		
		$this->db->query($sql);
	}
	
	private function uniformDecimalPoint($number) {
		return (float)(str_replace(',', '', str_replace('.', '.', $number)));
	}
	
	public function exportPayout($data = array(), $sort = array()) {
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}
		if(isset($sort['filters_table'])) {
			if (isset($sort['filters_table']['filter_date_start']) && isset($sort['filters_table']['filter_date_end']) && $sort['filters_table']['filter_date_start']!='' && $sort['filters_table']['filter_date_end']!='') {
				$filters .= " AND DATE(mpay.date_created) BETWEEN '".$sort['filters_table']['filter_date_start']."' AND '".$sort['filters_table']['filter_date_end']."'";
			}

			if (isset($sort['filters_table']['filter_status'])) {
				if($sort['filters_table']['filter_status']!='' && $sort['filters_table']['filter_status']!=0){
					$filters .= " AND payment_status = '" . (int)$sort['filters_table']['filter_status'] . "'";
				}
			}
		}
		

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					payment_id,
					payment_type,
					payment_status,
					payment_method,
					payment_data,
					amount,
					currency_code,
					mpay.date_created as 'mpay.date_created',
					mpay.date_paid as 'mpay.date_paid',
					mpay.description as 'mpay.description',
					ms.customer_id as 'customer_id',
					ms.firstname,
					product_id,
					order_id
				FROM `" . DB_PREFIX . "payment` mpay
				LEFT JOIN `" . DB_PREFIX . "customer` ms
					USING (customer_id)
				WHERE 1 = 1 "
				. (isset($data['payment_id']) ? " AND payment_id =  " .  (int)$data['payment_id'] : '')
				. (isset($data['customer_id']) ? " AND customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['product_id']) ? " AND product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['return_id']) ? " AND return_id =  " .  (int)$data['return_id'] : '')
				. (isset($data['order_detail_id']) ? " AND order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['currency_id']) ? " AND currency_id =  " .  (int)$data['currency_id'] : '')
				. (isset($data['payment_type']) ? " AND payment_type IN  (" .  $this->db->escape(implode(',', $data['payment_type'])) . ")" : '')
				. (isset($data['payment_method']) ? " AND payment_method IN  (" .  $this->db->escape(implode(',', $data['payment_method'])) . ")" : '')
				. (isset($data['payment_status']) ? " AND payment_status IN  (" .  $this->db->escape(implode(',', $data['payment_status'])) . ")" : '')

				. $filters;
				
		$res = $this->db->query($sql);
		
		//EXPORT EXCEL
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$sheet = $objPHPExcel->getActiveSheet();
		
		if($sort['filters_table']['filter_date_start']==null && $sort['filters_table']['filter_date_end'] == null && $sort['filters_table']['filter_date_start']=='' && $sort['filters_table']['filter_date_end'] == ''){
			$range_date = 'All';
		}else{
			$date_start = date('d/m/Y', strtotime($sort['filters_table']['filter_date_start']));
			$date_end = date('d/m/Y', strtotime($sort['filters_table']['filter_date_end']));
			$range_date = $date_start . ' - ' . $date_end;
		}
		
		if(isset($sort['filters_table'])) {
			if (isset($sort['filters_table']['filter_date_start']) && isset($sort['filters_table']['filter_date_end']) && $sort['filters_table']['filter_date_start']!='' && $sort['filters_table']['filter_date_end']!='') {
				$date_start = date('d/m/Y', strtotime($sort['filters_table']['filter_date_start']));
				$date_end = date('d/m/Y', strtotime($sort['filters_table']['filter_date_end']));
				$range_date = $date_start . ' - ' . $date_end;
			}else{
				$range_date = 'All';
			}

			if (isset($sort['filters_table']['filter_status'])) {
				switch ($sort['filters_table']['filter_status']) {
					case Payment::STATUS_UNPAID:
						$payment_status='Unpaid';
						break;
						
					case Payment::STATUS_PAID:
						$payment_status='Paid';
						break;
						
					default: 
						$payment_status='All';
				}
			}
		}
		
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator($this->user->getUserName())
									 ->setLastModifiedBy($this->user->getUserName())
									 ->setTitle("Payout Report")
									 ->setSubject("Payout")
									 ->setDescription("Payout")
									 ->setKeywords("office 2007 openxml php");

									 
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('B1:H1')
					->setCellValue('B1', 'Payout Report')
					->mergeCells('B2:H2')
					->setCellValue('B2', 'Created Date: '.$range_date)
					->mergeCells('B3:H3')
					->setCellValue('B3', 'Payment Status: '.$payment_status)
					->mergeCells('B4:H4')
					->setCellValue('B4', 'Download Date: '.date("d/m/Y"))
					->setCellValue('A6', 'No')
					->setCellValue('B6', 'Customer')
					->setCellValue('C6', 'Type')
					->setCellValue('D6', 'Amount')
					->setCellValue('E6', 'Description')
					->setCellValue('F6', 'Status')
					->setCellValue('G6', 'Date Created')
					->setCellValue('H6', 'Date Paid');
		
		//header style
		$headerStyleArray = array(
			'font' => array(
				'bold' => true,
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => '4C82C5',
				)
			),
		);
		
		//title style
		$titleStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FFFFFF',
				)
			),
		);
		
		$centerStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$leftStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$rightStyleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		//bold style
		$titleFont = array(
			'font' => array(
				'bold' => true,
				'size' => 14
			)
		);
		$boldFont = array(
			'font' => array(
				'bold' => true
			)
		);
		
		$sheet->getStyle('A6:H6')->applyFromArray($headerStyleArray);
		$sheet->getStyle('B1')->applyFromArray($titleFont);
		$sheet->getStyle('A1:H3')->applyFromArray($titleStyleArray);
		
		//data from db
		$reports = $res->rows;
		
		$row = 7;
		$no = 1;
		$temp = 0;
		
		foreach($reports as $report){
			switch ($report['payment_type']) {
					case Payment::TYPE_PAYOUT:
					case Payment::TYPE_PAYOUT_REQUEST:
						$payment_type='Payout';
						break;
						
					default: 
						$payment_type='Generic';
			}
			
			switch ($report['payment_status']) {
					case Payment::STATUS_UNPAID:
						$payment_status='Unpaid';
						break;
						
					case Payment::STATUS_PAID:
						$payment_status='Paid';
						break;
						
					default: 
						$payment_status='Generic';
			}
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, $no)
						->setCellValue('B'.$row, $report['firstname'])
						->setCellValue('C'.$row, $payment_type)
						->setCellValue('D'.$row, $this->currency->format($report['amount'], $this->currency->getCode(), $this->currency->getValue()))
						->setCellValue('E'.$row, $report['mpay.description'])
						->setCellValue('F'.$row, $payment_status)
						->setCellValue('G'.$row, date('d/m/Y', strtotime($report['mpay.date_created'])))
						->setCellValue('H'.$row, date('d/m/Y', strtotime($report['mpay.date_paid'])));
			$row++;
			$no++;
		}
		$last_row = $row - 1;
		
		//set column width
		$sheet->getColumnDimension('A')->setWidth(8);
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getColumnDimension('D')->setWidth(20);
		$sheet->getColumnDimension('E')->setWidth(70);
		$sheet->getColumnDimension('F')->setWidth(20);
		$sheet->getColumnDimension('G')->setWidth(16);
		$sheet->getColumnDimension('H')->setWidth(16);
		
		//body style 
		$sheet->getStyle('C7:D'.$last_row)->getAlignment()->setWrapText(true);
		$sheet->getStyle('A7:A'.$last_row)->applyFromArray($centerStyleArray);	//column no, invoice style
		$sheet->getStyle('B7:B'.$last_row)->applyFromArray($leftStyleArray);	//column no, invoice style
		$sheet->getStyle('C7:C'.$last_row)->applyFromArray($leftStyleArray);	//column name & product style
		$sheet->getStyle('D7:D'.$last_row)->applyFromArray($rightStyleArray);	//column name & product style
		$sheet->getStyle('E7:F'.$last_row)->applyFromArray($leftStyleArray);	//column sku, qty style
		$sheet->getStyle('G7:J'.$last_row)->applyFromArray($leftStyleArray);	//column total style
		
		//set column color
		$sheet->getStyle('A7:H'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
		
		//set border line
		$border_style = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('argb' => 'A3A3A3'),
								),
							),
						);
		
		$border_thick = array(
							'borders' => array(
								'bottom' => array(
									'style' => PHPExcel_Style_Border::BORDER_THICK,
									'color' => array('argb' => '8C8C8C'),
								),
							),
						);
		
		$sheet->getStyle("A6:H".$last_row)->applyFromArray($border_style);
		$sheet->getStyle('A3:H3')->applyFromArray($border_thick);
		
		// Rename worksheet
		$sheet->setTitle('Payout Report');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set logo header
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Ofiskita');
		$objDrawing->setDescription('Ofiskita');
		$objDrawing->setPath(DIR_IMAGE . 'header_report.png');
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWidthAndHeight(500,148);
		$objDrawing->setWorksheet($sheet);
		

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Payout Report.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
		
		
	}
	
}