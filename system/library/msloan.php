<?php
class MsLoan extends Model {
	const MS_BALANCE_TYPE_SALE = 1;
	
	public function addPayment($seller_id, $amount, $batch_id) {
		$sql = "SELECT *
				FROM " . DB_PREFIX . "ms_loan
				WHERE is_finish=1 AND seller_id = " . (int)$seller_id . "
				ORDER BY date_created ASC";
		$res = $this->db->query($sql);

		$loans = $res->rows;
		$saldo=$amount; 
		foreach($loans as $loan){
			
			$sql = "SELECT IFNULL(SUM(payment),0) as prepayment
				FROM " . DB_PREFIX . "ms_loan_detail
				WHERE loan_id = " . (int)$loan['loan_id'];
			$query = $this->db->query($sql);
			
			$prepayment = $query->row['prepayment'];
			
			if(((int)$saldo - ((int)$loan['amount']-(int)$prepayment))>=0){
				$saldo = (int)$saldo - ((int)$loan['amount']-(int)$prepayment);
				$paid = ((int)$loan['amount']-(int)$prepayment);
			}else{
				$paid = $saldo;
				$saldo = 0;
			}
			
			$sql_paid=false;
			
			if(((int)$prepayment+$paid)==(int)$loan['amount']){
				$sql_paid=true;
			}
			
			$sql = "INSERT INTO `" . DB_PREFIX . "ms_loan_detail`
				SET loan_id = " . (int)$loan['loan_id'] . ", 
					paid_batch_id = '" . $this->db->escape($batch_id) . "',
					payment = " . (int)$paid . ",
					prepayment = " . (int)$prepayment . ",
					date_created=NOW()";
				

			$this->db->query($sql);
			
			if($sql_paid==true){
				$sql = "UPDATE `" . DB_PREFIX . "ms_loan`
				SET date_modified=NOW(), is_finish=0 WHERE loan_id = " . (int)$loan['loan_id'];
				
				$this->db->query($sql);
			}
			
			if($saldo==0){
				break;
			}
		}
	}
	
	/*
	public function finishLoan($seller_id) {
		$sql = "SELECT *
				FROM " . DB_PREFIX . "ms_loan
				WHERE is_finish=1 AND seller_id = " . (int)$seller_id . "
				ORDER BY date_created ASC";
		$res = $this->db->query($sql);

		$loans = $res->rows;
		foreach($loans as $loan){
			$sql = "UPDATE `" . DB_PREFIX . "ms_loan`
				SET paid = amount , date_modified=NOW(), is_finish=0 WHERE loan_id = " . (int)$loan['loan_id'];
		}
	}*/
	
	public function calcLoan($seller_id, $batch_id){
		
		$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		//Get Loans
		$sql = "SELECT *
				FROM " . DB_PREFIX . "ms_loan
				WHERE is_finish=1 AND seller_id = " . (int)$seller_id . "
				ORDER BY date_created ASC";
		$res 		= $this->db->query($sql);
		$loans 		= $res->rows;
		$totalLoans	= 0;
		foreach($loans as $loan){
			$sql = "SELECT IFNULL(SUM(payment),0) as prepayment
				FROM " . DB_PREFIX . "ms_loan_detail
				WHERE loan_id = " . (int)$loan['loan_id'];
			$query = $this->db->query($sql);
			
			$prepayment = $query->row['prepayment'];
			
			$totalLoans+=((int)$loan['amount']-(int)$prepayment);
		}
		
		//Get OrderComplete
		$sql = "SELECT *
				FROM " . DB_PREFIX . "summary_report
				WHERE seller_id = " . (int)$seller_id . "
				 AND batch_id = '" . $batch_id . "'";
		$res 		= $this->db->query($sql);
		$orders 	= $res->rows;
		$totalOrder	= 0;
		$totalCommission=0;
		$order_detail_id	= 0;
		foreach($orders as $order){
			/*if($order_detail_id!=(int)$order['order_detail_id']){
				$order_detail_id=$order['order_detail_id'];
				$totalOrder+=(int)$order['shipping_fee'];
			}*/
			$totalCommission+=(int)$order['store_commission']+(int)$order['online_payment'];
			$totalOrder+=(int)$order['vendor_balance'];
		}
		
		//Paid Tax
			/*$this->MsLoader->MsBalance->addBalanceEntry(
				(int)$seller_id,
				array(
					'balance_type' => MsBalance::MS_BALANCE_TYPE_TAX,
					'amount' => floor(MsOrderData::PKP_TAX * $totalCommission)*-1,
					'description' => "Commission Tax Batch ".$batch_id
				)
			);*/
		//floor(MsOrderData::PKP_TAX * $totalCommission)
		$this->db->query("UPDATE " . DB_PREFIX . "summary_report SET 
					store_commission_tax='0' 
					WHERE seller_id = " . (int)$seller_id . "
					AND batch_id = '" . $batch_id . "'");
			
		//Get Balance
		$totalBalance 	= $this->MsLoader->MsBalance->getSellerBalance($seller_id) - $totalOrder;
		
		$minimumDeposit = 500000;//(int)$this->config->get('msconf_seller_fault_max') * (int)$this->config->get('msconf_seller_fault_amount');
		
		//Calculate
		$saldoNett		= ($totalOrder + $totalBalance) - $totalLoans;
		
		if($saldoNett<=0){
			$paidAmount = ($totalOrder + $totalBalance);
			$this->addPayment($seller_id, $paidAmount, $batch_id);
			//Paid Loan
			$this->MsLoader->MsBalance->addBalanceEntry(
				(int)$seller_id,
				array(
					'balance_type' => MsBalance::MS_BALANCE_TYPE_FAULT,
					'amount' => $paidAmount*-1,
					'description' => "Debt Payments Batch ".$batch_id
				)
			);
			
			//Create Payment
			$paymentId=$this->MsLoader->MsPayment->createPayment(array(
				'user_id' => $seller_id,
				'seller_id' => $seller_id,
				'batch_id' => $batch_id,
				'payment_type' => MsPayment::TYPE_PAYOUT_REQUEST,
				'payment_status' => MsPayment::STATUS_UNPAID,
				'payment_method' => MsPayment::METHOD_TRANSFER,
				'payment_data' => $seller['ms.account_name']."(".$seller['ms.account']."-".$seller['ms.account_number'].")",
				'amount' => 0,
				'currency_id' => $this->currency->getId($this->config->get('config_currency')),
				'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
				'description' => 'Payout request via Bank Transfer ('.$seller['ms.account_name'].') on '.date('d/m/Y')
				)
			);
			
			$this->MsLoader->MsPayment->updatePayment($paymentId, array(
				'payment_status' => MsPayment::STATUS_PAID,
				'date_paid' => date( 'Y-m-d H:i:s')
			));
			
			$payment = $this->MsLoader->MsPayment->getPayments(array('payment_id' => $paymentId, 'single' => 1));
			
			$this->MsLoader->MsBalance->addBalanceEntry($payment['seller_id'], array(
				'withdrawal_id' => $paymentId,
				'balance_type' => MsBalance::MS_BALANCE_TYPE_WITHDRAWAL,
				'amount' => 0,
				'batch_id' => $batch_id,
				'description' => $payment['mpay.description']
			));
		}
		
		if($saldoNett > 0 && $saldoNett <= $minimumDeposit){
			$paidAmount = $totalLoans;
			$this->addPayment($seller_id, $paidAmount, $batch_id);
			//Paid Loan
			$this->MsLoader->MsBalance->addBalanceEntry(
				(int)$seller_id,
				array(
					'balance_type' => MsBalance::MS_BALANCE_TYPE_FAULT,
					'amount' => $paidAmount*-1,
					'description' => "Debt Payments Batch ".$batch_id
				)
			);
			
			//Create Payment
			$paymentId=$this->MsLoader->MsPayment->createPayment(array(
				'user_id' => $seller_id,
				'seller_id' => $seller_id,
				'batch_id' => $batch_id,
				'payment_type' => MsPayment::TYPE_PAYOUT_REQUEST,
				'payment_status' => MsPayment::STATUS_UNPAID,
				'payment_method' => MsPayment::METHOD_TRANSFER,
				'payment_data' => $seller['ms.account_name']."(".$seller['ms.account']."-".$seller['ms.account_number'].")",
				'amount' => 0,
				'currency_id' => $this->currency->getId($this->config->get('config_currency')),
				'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
				'description' => 'Payout request via Bank Transfer ('.$seller['ms.account_name'].') on '.date('d/m/Y')
				)
			);
			
			$this->MsLoader->MsPayment->updatePayment($paymentId, array(
				'payment_status' => MsPayment::STATUS_PAID,
				'date_paid' => date( 'Y-m-d H:i:s')
			));
			
			$payment = $this->MsLoader->MsPayment->getPayments(array('payment_id' => $paymentId, 'single' => 1));
			
			$this->MsLoader->MsBalance->addBalanceEntry($payment['seller_id'], array(
				'withdrawal_id' => $paymentId,
				'balance_type' => MsBalance::MS_BALANCE_TYPE_WITHDRAWAL,
				'amount' => 0,
				'batch_id' => $batch_id,
				'description' => $payment['mpay.description']
			));
		}
		
		if($saldoNett > $minimumDeposit){
			$paidAmount = $totalLoans;
			$this->addPayment($seller_id, $paidAmount, $batch_id);
			//Paid Loan
			$this->MsLoader->MsBalance->addBalanceEntry(
				(int)$seller_id,
				array(
					'balance_type' => MsBalance::MS_BALANCE_TYPE_FAULT,
					'amount' => $paidAmount*-1,
					'description' => "Debt Payments Batch ".$batch_id
				)
			);
			
			//Create Payment
			$paymentId=$this->MsLoader->MsPayment->createPayment(array(
				'user_id' => $seller_id,
				'seller_id' => $seller_id,
				'batch_id' => $batch_id,
				'payment_type' => MsPayment::TYPE_PAYOUT_REQUEST,
				'payment_status' => MsPayment::STATUS_UNPAID,
				'payment_method' => MsPayment::METHOD_TRANSFER,
				'payment_data' => $seller['ms.account_name']."(".$seller['ms.account']."-".$seller['ms.account_number'].")",
				'amount' => $totalOrder-$totalLoans,
				'currency_id' => $this->currency->getId($this->config->get('config_currency')),
				'currency_code' => $this->currency->getCode($this->config->get('config_currency')),
				'description' => 'Payout request via Bank Transfer ('.$seller['ms.account_name'].') on '.date('d/m/Y')
				)
			);
			
			$this->MsLoader->MsPayment->updatePayment($paymentId, array(
				'payment_status' => MsPayment::STATUS_PAID,
				'date_paid' => date( 'Y-m-d H:i:s')
			));
			
			$payment = $this->MsLoader->MsPayment->getPayments(array('payment_id' => $paymentId, 'single' => 1));
			
			$this->MsLoader->MsBalance->addBalanceEntry($payment['seller_id'], array(
				'withdrawal_id' => $paymentId,
				'balance_type' => MsBalance::MS_BALANCE_TYPE_WITHDRAWAL,
				'amount' => (-1*($totalOrder-$totalLoans)),
				'batch_id' => $batch_id,
				'description' => $payment['mpay.description']
			));
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "transaction_batch SET date_close=NOW(), is_closed='0' WHERE batch_id='". $batch_id ."'");
	}
	
	public function getSellerLoans($data=array(), $sort=array()) {
		
		$sql = "SELECT *
				FROM " . DB_PREFIX . "ms_loan
				WHERE 1 = 1 AND is_finish = 1 "
				. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['order_detail_id']) ? " AND order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '')
				. (isset($data['batch_id']) ? " AND batch_id =  '" .  $data['batch_id'] ."'" : '')
				. ((isset($data['date_start']) && isset($data['date_end']) && $data['date_start']!='' && $data['date_end']!='') ? " AND date_created BETWEEN  '" .  $data['date_start'] ."' AND '". $data['date_end'] ."'": '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
				
		$res = $this->db->query($sql);

		return $res->rows;
	}
	
	public function addLoan($data) {
		$sql = "INSERT INTO `" . DB_PREFIX . "ms_loan`
				SET seller_id = " . (int)$data['seller_id'] . ",
					user_id = " . (isset($data['user_id']) ? (int)$data['user_id'] : 'NULL') . ",
					order_id = " . (isset($data['order_id']) ? (int)$data['order_id'] : 'NULL') . ",
					order_detail_id = " . (isset($data['order_detail_id']) ? (int)$data['order_detail_id'] : 'NULL') . ",
					batch_id = '" . $this->db->escape($data['batch_id']) . "',
					amount = " . (int)$data['amount'] . ",
					description = '" . (isset($data['description']) ? $this->db->escape($data['description']) : '') . "',
					date_created = NOW()";

		$this->db->query($sql);
		return $this->db->getLastId();
	}

	
	
}
?>
