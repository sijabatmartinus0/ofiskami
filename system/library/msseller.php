<?php
final class MsSeller extends Model {
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_DISABLED = 3;
	const STATUS_DELETED = 4;
	const STATUS_UNPAID = 5;
	const STATUS_INCOMPLETE = 6;
	
	const PREMIUM_STATUS_DEFAULT = 0;
	const PREMIUM_STATUS_REQUEST = 1;
	const PREMIUM_STATUS_APPROVE = 2;
		
	const SELLER_TYPE_USER = 0;
	const SELLER_TYPE_ADMIN = 1;
	
	const MS_SELLER_VALIDATION_NONE = 1;
	const MS_SELLER_VALIDATION_ACTIVATION = 2;
	const MS_SELLER_VALIDATION_APPROVAL = 3;

	private $isSeller = FALSE;
	private $seller_id;	
	private $seller_group_id;	
	private $nickname;
	private $description;
	private $company;
	private $country_id;
	private $zone_id;
	private $city_id;
	private $district_id;
	private $subdistrict_id;
	private $postcode;
	private $avatar;
	private $seller_status;
	private $paypal;
	private $seller_type;
	private $premium_status;
	private $roles;
	private $account_id;
	private $account;
	private $account_name;
	private $account_number;
	private $address;
	private $npwp_number;
	private $npwp_address;
	private $fault;
	
	
  	public function __construct($registry) {
  		parent::__construct($registry);
  		
  		//$this->log->write('creating seller object: ' . $this->session->data['customer_id']);
		if (isset($this->session->data['customer_id'])) {
			//TODO 
			//$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$this->session->data['customer_id'] . "' AND seller_status = '1'");
			$seller_query = $this->db->query("SELECT mss.*,a.name as account FROM " . DB_PREFIX . "ms_seller mss LEFT JOIN " . DB_PREFIX . "account a on a.account_id=mss.account_id WHERE mss.seller_id = '" . (int)$this->session->data['customer_id'] . "'");			
			
			if ($seller_query->num_rows) {
				$this->isSeller = TRUE;
				$this->nickname = $seller_query->row['nickname'];
				$this->description = $seller_query->row['description'];
				$this->company = $seller_query->row['company'];
				$this->country_id = $seller_query->row['country_id'];
				$this->zone_id = $seller_query->row['zone_id'];
				$this->city_id = $seller_query->row['city_id'];
				$this->district_id = $seller_query->row['district_id'];
				$this->subdistrict_id = $seller_query->row['subdistrict_id'];
				$this->postcode = $seller_query->row['postcode'];
				$this->avatar = $seller_query->row['avatar'];
				$this->seller_status = $seller_query->row['seller_status'];
				$this->paypal = $seller_query->row['paypal'];
				$this->account_id = $seller_query->row['account_id'];
				$this->account = $seller_query->row['account'];
				$this->account_name = $seller_query->row['account_name'];
				$this->account_number = $seller_query->row['account_number'];
				$this->address = $seller_query->row['address'];
				$this->npwp_number = $seller_query->row['npwp_number'];
				$this->npwp_address = $seller_query->row['npwp_address'];
				$this->seller_type = $seller_query->row['seller_type'];
				$this->premium_status = $seller_query->row['premium_status'];
				$this->roles = unserialize($seller_query->row['roles']);
				
				if((int)$seller_query->row['seller_group']!=1){
					$this->seller_id=$seller_query->row['seller_group'];
				}else{
					$this->seller_id=$seller_query->row['seller_id'];
				}
				
				$fault_query = $this->db->query("SELECT fault FROM " . DB_PREFIX . "ms_seller_fault WHERE seller_id = '" . (int)$this->seller_id . "'");
				
				if ($seller_query->num_rows) {
					$this->fault=(int)$fault_query->row['fault'];
				}else{
					$this->fault=0;
				}
			}
  		}
	}

	private function _dupeSlug($slug) {
			$similarity_query = $this->db->query("SELECT * FROM ". DB_PREFIX . "url_alias WHERE keyword LIKE '" . $this->db->escape($slug) . "%'");
			return ($similarity_query->num_rows > 0) ? $slug . $similarity_query->num_rows : $slug;
	}

  	public function isCustomerSeller($customer_id) {
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "ms_seller`
				WHERE seller_id = " . (int)$customer_id;
		
		$res = $this->db->query($sql);
		
		if ($res->row['total'] == 0)
			return FALSE;
		else
			return TRUE;	  		
  	}
  	
	public function getSellerName($seller_id) {
		$sql = "SELECT firstname as 'firstname'
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['firstname'];
	}	
	
	public function getSellerEmail($seller_id) {
		$sql = "SELECT email as 'email' 
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['email'];
	}
		
	public function createSeller($data) {
		$avatar = isset($data['avatar_name']) ? $this->MsLoader->MsFile->moveImage($data['avatar_name']) : '';
		$banner = isset($data['banner_name']) ? $this->MsLoader->MsFile->moveImage($data['banner_name']) : '';

		if (isset($data['commission']))
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		
		$sql = "INSERT INTO " . DB_PREFIX . "ms_seller
				SET seller_id = " . (int)$data['seller_id'] . ",
					seller_status = " . (isset($data['status']) ? (int)$data['status'] : self::STATUS_INACTIVE) . ",
					seller_approved = " . (isset($data['approved']) ? (int)$data['approved'] : 0) . ",
					seller_group = " .  (isset($data['seller_group']) ? (int)$data['seller_group'] : $this->config->get('msconf_default_seller_group_id'))  .  ",
					nickname = '" . $this->db->escape($data['nickname']) . "',
					description = '" . $this->db->escape(isset($data['description']) ? $data['description'] : '') . "',
					company = '" . $this->db->escape(isset($data['company']) ? $data['company'] : '') . "',
					dob = '" . $this->db->escape($data['dob']) . "',
					country_id = " . (isset($data['country']) ? (int)$data['country'] : 0) . ",
					zone_id = " . (isset($data['zone']) ? (int)$data['zone'] : 0) . ",
					city_id = " . (isset($data['city']) ? (int)$data['city']: 0) . ",
					district_id = " . (isset($data['district']) ? (int)$data['district']: 0) . ",
					subdistrict_id = " . (isset($data['subdistrict']) ? (int)$data['subdistrict']: 0) . ",
					postcode = '" . (isset($data['postcode']) ? $this->db->escape($data['postcode']): 0) . "',
					account_id = " . (isset($data['account']) ? (int)$data['account']: 0) . ",
					account_name = '" . (isset($data['account_name']) ?  $this->db->escape($data['account_name']): '') . "',
					account_number = '" .(isset($data['account_number']) ?  $this->db->escape($data['account_number']): '') . "',
					address = '" . (isset($data['address']) ? $this->db->escape($data['address']): '') . "',
					npwp_number = '" . (isset($data['npwp_number']) ? $this->db->escape($data['npwp_number']): '') . "',
					npwp_address = '" . (isset($data['npwp_address']) ? $this->db->escape($data['npwp_address']): '') . "',
					telephone = '" . $this->db->escape($data['telephone']) . "',
					commission_id = " . (isset($commission_id) ? $commission_id : 'NULL') . ",
					product_validation = " . (isset($data['product_validation']) ? (int)$data['product_validation'] : 0) . ",
					paypal = '" . $this->db->escape(isset($data['paypal']) ? $data['paypal'] : '') . "',
					avatar = '" . $this->db->escape($avatar) . "',
					banner = '" . $this->db->escape($banner) . "',
					date_created = NOW(),
					tax_type = '".$this->db->escape($data['tax_type'])."',
					business_type = '".$this->db->escape($data['business_type'])."'";

		$this->db->query($sql);
		$seller_id = $this->db->getLastId();
		
		/*insert attachment*/
		if(isset($data['npwp_individu_files'])){
			$npwp_individu = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_individu_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='1', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($npwp_individu)."'");
		}
		
		if(isset($data['ktp_files'])){
			$ktp = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='2', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($ktp)."'");
		}
		
		if(isset($data['tabungan_individu_files'])){
			$tabungan_individu = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_individu_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='3', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tabungan_individu)."'");
		}
		
		if(isset($data['npwp_bisnis_files'])){
			$npwp_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_bisnis_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='4', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($npwp_bisnis)."'");
		}
		
		if(isset($data['siup_files'])){
			$siup = $this->MsLoader->MsFile->moveImageAttachment($data['siup_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='5', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($siup)."'");
		}
		
		if(isset($data['tdp_files'])){
			$tdp = $this->MsLoader->MsFile->moveImageAttachment($data['tdp_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='6', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tdp)."'");
		}
		
		if(isset($data['apbu_files'])){
			$apbu = $this->MsLoader->MsFile->moveImageAttachment($data['apbu_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='7', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($apbu)."'");
		}
		
		if(isset($data['ktp_direksi_files'])){
			$ktp_direksi = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_direksi_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='8', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($ktp_direksi)."'");
		}
		
		if(isset($data['domisili_files'])){
			$domisili = $this->MsLoader->MsFile->moveImageAttachment($data['domisili_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='9', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($domisili)."'");
		}
		
		if(isset($data['tabungan_bisnis_files'])){
			$tabungan_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_bisnis_files']);
			$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='10', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tabungan_bisnis)."'");
		}
		
		$sql = "INSERT INTO " . DB_PREFIX . "ms_seller_fault
				SET seller_id = " . (int)$data['seller_id'] . ",
				fault = '" . (int)$this->config->get('msconf_seller_fault_max') . "'";
		$this->db->query($sql);
		
		$this->MsLoader->MsPayment->createPayment(array(
			'seller_id'=>$data['seller_id'],
			// 'user_id'=>$this->user->getId(),
			// 'user_id'=>(($this->user->getId() != null) ? $this->user->getId() : '0') ,
			'user_id'=> '0' ,
			'payment_type'=>(int)MsPayment::TYPE_DEPOSIT_FEE,
			'payment_status'=>(int)MsPayment::STATUS_PAID,
			'payment_method'=>(int)MsPayment::METHOD_BALANCE,
			'amount'=>(float)($this->config->get('msconf_seller_fault_max')*$this->config->get('msconf_seller_fault_amount')),
			'currency_id'=>$this->currency->getId(),
			'currency_code'=>$this->currency->getCode()
		));
		
		$this->MsLoader->MsBalance->addBalanceEntry(
			$data['seller_id'],
				array(
					'balance_type' => MsBalance::MS_BALANCE_TYPE_SALE,
					'amount' => (float)($this->config->get('msconf_seller_fault_max')*$this->config->get('msconf_seller_fault_amount')),
					'description' => 'Deposit Fee'
				)
		);
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_logistic WHERE seller_id=" . (int)$seller_id. "");
		
		if (isset($data['logistic'])) {
			foreach ($data['logistic'] as $logistic_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "ms_seller_logistic SET logistic_id = '" . (int)$logistic_id . "', seller_id = '" . (int)$seller_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'seller_id=" . (int)$seller_id . "', keyword = '" . $this->db->escape($this->_dupeSlug($data['keyword'])) . "'");
		}
	}
	
	public function nicknameTaken($nickname) {
		$sql = "SELECT nickname
				FROM `" . DB_PREFIX . "ms_seller` p
				WHERE p.nickname = '" . $this->db->escape($nickname) . "'";
		
		$res = $this->db->query($sql);

		return $res->num_rows;
	}
	
	public function nicknameTakenUpdate($nickname,$staff_id) {
		$sql = "SELECT nickname
				FROM `" . DB_PREFIX . "ms_seller` p
				WHERE p.nickname = '" . $this->db->escape($nickname) . "' AND p.seller_id<>'".(int)$staff_id."'";
		
		$res = $this->db->query($sql);

		return $res->num_rows;
	}
	
	public function editSeller($data) {
		$seller_id = (int)$data['seller_id'];

		$old_avatar = $this->getSellerAvatar($seller_id);
		
		if (!isset($data['avatar_name']) || ($old_avatar['avatar'] != $data['avatar_name'])) {
			$this->MsLoader->MsFile->deleteImage($old_avatar['avatar']);
		}
		
		if (isset($data['avatar_name'])) {
			if ($old_avatar['avatar'] != $data['avatar_name']) {			
				$avatar = $this->MsLoader->MsFile->moveImage($data['avatar_name']);
			} else {
				$avatar = $old_avatar['avatar'];
			}
		} else {
			$avatar = '';
		}

		$old_banner = $this->getSellerBanner($seller_id);

		if (!isset($data['banner_name']) || ($old_banner['banner'] != $data['banner_name'])) {
			$this->MsLoader->MsFile->deleteImage($old_banner['banner']);
		}

		if (isset($data['banner_name'])) {
			if ($old_banner['banner'] != $data['banner_name']) {
				$banner = $this->MsLoader->MsFile->moveImage($data['banner_name']);
			} else {
				$banner = $old_banner['banner'];
			}
		} else {
			$banner = '';
		}

		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET description = '" . $this->db->escape($data['description']) . "',
					company = '" . $this->db->escape($data['company']) . "',
					nickname = '" . $this->db->escape($data['nickname']) . "',
					country_id = " . (int)$data['country'] . ",
					zone_id = " . (int)$data['zone'] . ",
					city_id = " . (int)$data['city'] . ",
					district_id = " . (int)$data['district'] . ",
					subdistrict_id = " . (int)$data['subdistrict'] . ",
					postcode = '" . $this->db->escape($data['postcode']) . "',
					account_id = " . (int)$data['account'] . ",
					account_name = '" . $this->db->escape($data['account_name']) . "',
					account_number = '" . $this->db->escape($data['account_number']) . "',
					address = '" . $this->db->escape($data['address']) . "',
					business_type = '" . $this->db->escape($data['business_type']) . "',
					tax_type = '" . $this->db->escape($data['tax_type']) . "',
					npwp_number = '" . $this->db->escape($data['npwp_number']) . "',
					npwp_address = '" . $this->db->escape($data['npwp_address']) . "',"
					. (isset($data['status']) ? "seller_status=  " .  (int)$data['status'] . "," : '')
					. (isset($data['approved']) ? "seller_approved=  " .  (int)$data['approved'] . "," : '')
					. (isset($data['paypal']) ? "paypal = '" . $this->db->escape($data['paypal']) . "'," : '')
					." banner = '" . $this->db->escape($banner) . "',
					avatar = '" . $this->db->escape($avatar) . "'
				WHERE seller_id = " . (int)$seller_id;
		
		$this->db->query($sql);
		
		/*insert attachment*/
		if(isset($data['npwp_individu_files'])){
			$old_attachment = $this->getAttachment($seller_id, 1);
			if($old_attachment == ""){
				$npwp_individu = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_individu_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='1', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($npwp_individu)."'");
			}else if($old_attachment != "" && $old_attachment != $data['npwp_individu_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$npwp_individu = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_individu_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($npwp_individu)."' WHERE attachment_id='1' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['ktp_files'])){
			$old_attachment = $this->getAttachment($seller_id, 2);
			if($old_attachment == ""){
				$ktp = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='2', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($ktp)."'");
			}else if($old_attachment != "" && $old_attachment != $data['ktp_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$ktp = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($ktp)."' WHERE  attachment_id='2' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['tabungan_individu_files'])){
			$old_attachment = $this->getAttachment($seller_id, 3);
			if($old_attachment == ""){
				$tabungan_individu = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_individu_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='3', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tabungan_individu)."'");
			}else if($old_attachment != "" && $old_attachment != $data['tabungan_individu_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$tabungan_individu = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_individu_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($tabungan_individu)."' WHERE attachment_id='3' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['npwp_bisnis_files'])){
			$old_attachment = $this->getAttachment($seller_id, 4);
			if($old_attachment == ""){
				$npwp_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_bisnis_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='4', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($npwp_bisnis)."'");
			}else if($old_attachment != "" && $old_attachment != $data['npwp_bisnis_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$npwp_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['npwp_bisnis_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($npwp_bisnis)."' WHERE attachment_id='4' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['siup_files'])){
			$old_attachment = $this->getAttachment($seller_id, 5);
			if($old_attachment == ""){
				$siup = $this->MsLoader->MsFile->moveImageAttachment($data['siup_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='5', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($siup)."'");
			}else if($old_attachment != "" && $old_attachment != $data['siup_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$siup = $this->MsLoader->MsFile->moveImageAttachment($data['siup_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($siup)."' WHERE attachment_id='5' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['tdp_files'])){
			$old_attachment = $this->getAttachment($seller_id, 6);
			if($old_attachment == ""){
				$tdp = $this->MsLoader->MsFile->moveImageAttachment($data['tdp_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='6', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tdp)."'");
			}else if($old_attachment != "" && $old_attachment != $data['tdp_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$tdp = $this->MsLoader->MsFile->moveImageAttachment($data['tdp_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($tdp)."' WHERE  attachment_id='6' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['apbu_files'])){
			$old_attachment = $this->getAttachment($seller_id, 7);
			if($old_attachment == ""){
				$apbu = $this->MsLoader->MsFile->moveImageAttachment($data['apbu_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='7', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($apbu)."'");
			}else if($old_attachment != "" && $old_attachment != $data['apbu_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$apbu = $this->MsLoader->MsFile->moveImageAttachment($data['apbu_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($apbu)."' WHERE attachment_id='7' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['ktp_direksi_files'])){
			$old_attachment = $this->getAttachment($seller_id, 8);
			if($old_attachment == ""){
				$ktp_direksi = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_direksi_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='8', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($ktp_direksi)."'");
			}else if($old_attachment != "" && $old_attachment != $data['ktp_direksi_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$ktp_direksi = $this->MsLoader->MsFile->moveImageAttachment($data['ktp_direksi_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($ktp_direksi)."' WHERE attachment_id='8' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['domisili_files'])){
			$old_attachment = $this->getAttachment($seller_id, 9);
			if($old_attachment == ""){
				$domisili = $this->MsLoader->MsFile->moveImageAttachment($data['domisili_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='9', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($domisili)."'");
			}else if($old_attachment != "" && $old_attachment != $data['domisili_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$domisili = $this->MsLoader->MsFile->moveImageAttachment($data['domisili_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET  file='".$this->db->escape($domisili)."' WHERE attachment_id='9' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		if(isset($data['tabungan_bisnis_files'])){
			$old_attachment = $this->getAttachment($seller_id, 10);
			if($old_attachment == ""){
				$tabungan_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_bisnis_files']);
				$sql = $this->db->query("INSERT INTO `" . DB_PREFIX . "ms_seller_attachment` SET attachment_id='10', seller_id='".(int)$this->db->escape($seller_id)."', file='".$this->db->escape($tabungan_bisnis)."'");
			}else if($old_attachment != "" && $old_attachment != $data['tabungan_bisnis_files']){
				$this->MsLoader->MsFile->deleteImage($old_attachment);
				$tabungan_bisnis = $this->MsLoader->MsFile->moveImageAttachment($data['tabungan_bisnis_files']);
				$sql = $this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_attachment` SET file='".$this->db->escape($tabungan_bisnis)."' WHERE attachment_id='10' AND seller_id='".(int)$this->db->escape($seller_id)."'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_logistic WHERE seller_id=" . (int)$seller_id. "");
		
		if (isset($data['logistic'])) {
			foreach ($data['logistic'] as $logistic_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "ms_seller_logistic SET logistic_id = '" . (int)$logistic_id . "', seller_id = '" . (int)$seller_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'seller_id=" . (int)$seller_id. "'");
		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'seller_id=" . (int)$seller_id . "', keyword = '" . $this->db->escape($this->_dupeSlug($data['keyword'])) . "'");
		}
	}		
		
	public function getSellerAvatar($seller_id) {
		$query = $this->db->query("SELECT avatar as avatar FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");
		
		return $query->row;
	}

	public function getSellerBanner($seller_id) {
		$query = $this->db->query("SELECT banner as banner FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");

		return $query->row;
	}
	
	public function getSellerId() {
  		return $this->seller_id;
  	}
	
  	public function getNickname() {
  		return $this->nickname;
  	}

  	public function getCompany() {
  		return $this->company;
  	}
  	
  	public function getCountryId() {
  		return $this->country_id;
  	}
	public function getZoneId() {
  		return $this->zone_id;
  	}
	
	public function getCityId() {
  		return $this->city_id;
  	}
	
	public function getDistrictId() {
  		return $this->district_id;
  	}
	
	public function getSubdistrictId() {
  		return $this->subdistrict_id;
  	}
	
	public function getPostcode() {
  		return $this->postcode;
  	}
	
	public function getAccountId() {
  		return $this->account_id;
  	}
	
	public function getAccountName() {
  		return $this->account_name;
  	}
	
	public function getAccountNumber() {
  		return $this->account_number;
  	}
	
	public function getAddress() {
  		return $this->address;
  	}
	
	public function getNPWPNumber() {
  		return $this->npwp_number;
  	}
	
	public function getNPWPAddress() {
  		return $this->npwp_address;
  	}

  	public function getDescription() {
  		return $this->description;
  	}
  	
  	public function getStatus() {
  		return $this->seller_status;
  	}
	
	public function getSellerType() {
  		return $this->seller_type;
  	}
	
	public function getPremiumStatus() {
  		return $this->premium_status;
  	}
	
	public function getRoles() {
  		return $this->roles;
  	}

  	public function getPaypal() {
  		return $this->paypal;
  	}
	
	public function getFault() {
  		return $this->fault;
  	}
  	
  	public function isSeller() {
  		return $this->isSeller;
  	}
  	
	public function getSalesForSeller($seller_id) {
		$sql = "SELECT IFNULL(SUM(number_sold),0) as total
				FROM `" . DB_PREFIX . "ms_product`
				WHERE seller_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['total'];
	}
	
	public function getSalt($seller_id) {
		$sql = "SELECT salt
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['salt'];		
	}
	
	
	public function adminEditSeller($data) {
		$seller_id = (int)$data['seller_id'];

		// commissions
		if (!$data['commission_id']) {
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		} else {
			$commission_id = $this->MsLoader->MsCommission->editCommission($data['commission_id'], $data['commission']);
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'seller_id=" . (int)$seller_id. "'");
		
		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'seller_id=" . (int)$seller_id . "', keyword = '" . $this->db->escape($this->_dupeSlug($data['keyword'])) . "'");
		}

		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET description = '" . $this->db->escape($data['description']) . "',
					company = '" . $this->db->escape($data['company']) . "',
					seller_status = '" .  (int)$data['status'] .  "',
					country_id = " . (int)$data['country'] . ",
					zone_id = " . (int)$data['zone'] . ",
					city_id = " . (int)$data['city'] . ",
					district_id = " . (int)$data['district'] . ",
					subdistrict_id = " . (int)$data['subdistrict'] . ",
					postcode = '" . $this->db->escape($data['postcode']) . "',
					account_id = " . (int)$data['account'] . ",
					account_name = '" . $this->db->escape($data['account_name']) . "',
					account_number = '" . $this->db->escape($data['account_number']) . "',
					address = '" . $this->db->escape($data['address']) . "',
					npwp_number = '" . $this->db->escape($data['npwp_number']) . "',
					npwp_address = '" . $this->db->escape($data['npwp_address']) . "',
					seller_approved = '" .  (int)$data['approved'] .  "',
					product_validation = '" .  (int)$data['product_validation'] .  "',
					commission_id = " . (!is_null($commission_id) ? (int)$commission_id : 'NULL' ) . ",
					seller_group = '" .  (int)$data['seller_group'] .  "'
				WHERE seller_id = " . (int)$seller_id;
		
		$this->db->query($sql);	
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_logistic WHERE seller_id=" . (int)$seller_id. "");
		
		if (isset($data['logistic'])) {
			foreach ($data['logistic'] as $logistic_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "ms_seller_logistic SET logistic_id = '" . (int)$logistic_id . "', seller_id = '" . (int)$seller_id . "'");
			}
		}
	}
	
	/********************************************************/
	
	
	public function getTotalSellers($data = array()) {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . DB_PREFIX . "ms_seller ms
			WHERE 1 = 1 "
			. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '');

		$res = $this->db->query($sql);

		return $res->row['total'];
	}
	
	public function getTotalSellerNonStaff($data = array()) {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . DB_PREFIX . "ms_seller ms
			WHERE 1 = 1 AND (ms.seller_type=1 OR ( ms.seller_type=0 AND ms.seller_group=1 )) "
			. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '');

		$res = $this->db->query($sql);

		return $res->row['total'];
	}
	
	public function getAttachment($seller_id, $attachment_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller_attachment WHERE seller_id='".(int)$seller_id."' AND attachment_id='".(int)$attachment_id."'");
		
		if($query->num_rows){
			return $query->row['file'];
		}else{
			return "";
		}
	}
	
	public function getAttachmentMaster(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_attachment WHERE status='1'");
		
		return $query->num_rows;
	}
	
	public function getSeller($seller_id, $data = array()) {
		$sql = "SELECT	CONCAT(c.firstname, ' ', c.lastname) as name,
						c.email as 'c.email',
						c.change_password as 'c.change_password',
						ms.seller_id as 'seller_id',
						ms.nickname as 'ms.nickname',
						ms.company as 'ms.company',
						ms.website as 'ms.website',
						ms.paypal as 'ms.paypal',
						ms.seller_status as 'ms.seller_status',
						ms.seller_approved as 'ms.seller_approved',
						ms.date_created as 'ms.date_created',
						ms.product_validation as 'ms.product_validation',
						ms.avatar as 'ms.avatar',
						ms.banner as 'banner',
						ms.country_id as 'ms.country_id',
						ms.zone_id as 'ms.zone_id',
						ms.city_id as 'ms.city_id',
						ms.district_id as 'ms.district_id',
						ms.subdistrict_id as 'ms.subdistrict_id',
						ms.postcode as 'ms.postcode',
						ms.telephone as 'ms.telephone',
						ms.seller_type as 'seller_type',
						ms.premium_status as 'premium_status',
						ms.account_id as 'ms.account_id',
						oa.name as 'ms.account',
						ms.account_name as 'ms.account_name',
						ms.account_number as 'ms.account_number',
						ms.address as 'ms.address',
						ms.npwp_number as 'ms.npwp_number',
						ms.npwp_address as 'ms.npwp_address',
						ms.description as 'ms.description',
						ms.commission_id as 'ms.commission_id',
						ms.seller_group as 'ms.seller_group',
						ms.business_type as 'ms.business_type',
						ms.tax_type as 'ms.tax_type',
						IFNULL(SUM(mp.number_sold), 0) as 'total_sales',
						(SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE `query` = 'seller_id=" . (int)$seller_id . "' LIMIT 1) AS keyword
				FROM `" . DB_PREFIX . "customer` c
				INNER JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_product` mp
					ON (c.customer_id = mp.seller_id)
				LEFT JOIN `" . DB_PREFIX . "account` oa
					ON (oa.account_id = ms.account_id)
				WHERE ms.seller_id = " .  (int)$seller_id
				. (isset($data['product_id']) ? " AND mp.product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '')
				. " GROUP BY ms.seller_id
				LIMIT 1";
				
		$res = $this->db->query($sql);

		if (!isset($res->row['seller_id']) || !$res->row['seller_id'])
			return FALSE;
		else
			return $res->row;
	}	
	
	public function getSellers($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`c.name`" => 1, "total_sales" => 1, "`ms.date_created`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}
		
		$sql = "SELECT
					SQL_CALC_FOUND_ROWS"
					// additional columns
					. (isset($cols['total_products']) ? "
						(SELECT COUNT(*) FROM " . DB_PREFIX . "product p
						LEFT JOIN " . DB_PREFIX . "ms_product mp USING (product_id)
						LEFT JOIN " . DB_PREFIX . "ms_seller USING (seller_id)
						WHERE seller_id = ms.seller_id) as total_products,
					" : "")

					. (isset($cols['total_earnings']) ? "
						(SELECT COALESCE(SUM(amount),0)
							- (SELECT COALESCE(ABS(SUM(amount)),0)
								FROM `" . DB_PREFIX . "ms_balance`
								WHERE seller_id = ms.seller_id
								AND balance_type = ". MsBalance::MS_BALANCE_TYPE_REFUND
						. ") as total
						FROM `" . DB_PREFIX . "ms_balance`
						WHERE seller_id = ms.seller_id
						AND balance_type = ". MsBalance::MS_BALANCE_TYPE_SALE . ") as total_earnings,
					" : "")
					
					. (isset($cols['current_balance']) ? "
						(SELECT COALESCE(
							(SELECT balance FROM " . DB_PREFIX . "ms_balance
								WHERE seller_id = ms.seller_id  
								ORDER BY balance_id DESC
								LIMIT 1
							),
							0
						)) as current_balance,
					" : "")	
					
					// default columns
					." CONCAT(c.firstname, ' ', c.lastname) as 'c.name',
					c.email as 'c.email',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					ms.company as 'ms.company',
					ms.website as 'ms.website',
					ms.seller_status as 'ms.seller_status',
					ms.seller_approved as 'ms.seller_approved',
					ms.date_created as 'ms.date_created',
					ms.avatar as 'ms.avatar',
					ms.banner as 'banner',
					ms.country_id as 'ms.country_id',
					ms.zone_id as 'ms.zone_id',
					ms.description as 'ms.description',
					ms.paypal as 'ms.paypal',
					ms.seller_type as 'seller_type',
					ms.premium_status as 'premium_status',
					IFNULL(SUM(mp.number_sold), 0) as 'total_sales',
					(CASE WHEN omsf.fault<=". (int)$this->config->get('msconf_seller_fault_min') ." THEN 1 ELSE 0 END) AS 'fault_status'
				FROM `" . DB_PREFIX . "customer` c
				INNER JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_product` mp
					ON (c.customer_id = mp.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller_fault` omsf
					ON (ms.seller_id = omsf.seller_id)
				WHERE 1 = 1 AND ms.seller_status != 0 AND (ms.seller_type=1 OR ( ms.seller_type=0 AND ms.seller_group=1 ))"
				. (isset($data['seller_id']) ? " AND ms.seller_id =  " .  (int)$data['seller_id'] : '')
				. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '')
				
				. $wFilters
				
				. " GROUP BY ms.seller_id HAVING 1 = 1 "
				
				. $hFilters
				
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];
		
		return $res->rows;
	}
	
	public function getCustomers($sort = array()) {
		$sql = "SELECT  CONCAT(c.firstname, ' ', c.lastname) as 'c.name',
						c.email as 'c.email',
						c.customer_id as 'c.customer_id',
						ms.seller_id as 'seller_id'
				FROM `" . DB_PREFIX . "customer` c
				LEFT JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				WHERE ms.seller_id IS NULL"
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
    			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		
		return $res->rows;
	}
	
	public function getTotalEarnings($seller_id, $data = array()) {
		// note: update getSellers() if updating this
		$sql = "SELECT COALESCE(SUM(amount),0)
					   - (SELECT COALESCE(ABS(SUM(amount)),0)
						  FROM `" . DB_PREFIX . "ms_balance`
					 	  WHERE seller_id = " . (int)$seller_id . "
						  AND balance_type = ". MsBalance::MS_BALANCE_TYPE_REFUND
						  . (isset($data['period_start']) ? " AND DATEDIFF(date_created, '{$data['period_start']}') >= 0" : "")
				. ") as total
				FROM `" . DB_PREFIX . "ms_balance`
				WHERE seller_id = " . (int)$seller_id . "
				AND balance_type IN (". MsBalance::MS_BALANCE_TYPE_SALE.",". MsBalance::MS_BALANCE_TYPE_SHIPPING.")"
				. (isset($data['period_start']) ? " AND DATEDIFF(date_created, '{$data['period_start']}') >= 0" : "");

		$res = $this->db->query($sql);
		return $res->row['total'];
	}
	
	public function getLogistics($seller_id) {
		$result = array();
		$sql = "SELECT logistic_id
				FROM `" . DB_PREFIX . "ms_seller_logistic`
				WHERE seller_id = " . (int)$seller_id . "";
		$res = $this->db->query($sql);
		if($res->num_rows > 0){
			foreach($res->rows as $row){
				$result[]=$row['logistic_id'];
			}
		}
		return $result;
	}
	
	public function changeStatus($seller_id, $seller_status) {
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_status =  " .  (int)$seller_status . "
				WHERE seller_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
	}
	
	public function changeApproval($seller_id, $approved) {
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET	approved =  " .  (int)$approved . "
				WHERE seller_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
	}
	
	public function deleteSeller($seller_id) {
		$products = $this->MsLoader->MsProduct->getProducts(array('seller_id' => $seller_id));

		foreach ($products as $product) {
			$this->MsLoader->MsProduct->changeStatus($product['product_id'], MsProduct::STATUS_DELETED);
		}
	
		$this->db->query("UPDATE " . DB_PREFIX . "ms_seller SET seller_status = 0 WHERE seller_id = '" . (int)$seller_id . "'");;
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_balance WHERE seller_id = '" . (int)$seller_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_payment WHERE seller_id = '" . (int)$seller_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` = 'seller_id=".(int)$seller_id."'");
	}
	
	public function isNewSeller($code){
		$sql = "SELECT * FROM " . DB_PREFIX . "customer WHERE change_password = '" . $this->db->escape($code) ."'";
		
		$res = $this->db->query($sql);
		if($res->num_rows > 0){
			return $res->row;
		}else{
			return false;
		}
	}

	public function isActiveTemp($id) {

        $sql = "SELECT seller_id as 'seller'
				FROM `" . DB_PREFIX . "ms_product`
				WHERE product_id = " . (int) $id;
        $res = $this->db->query($sql);
        $res->row['seller'];

        $sql = "SELECT * FROM `" . DB_PREFIX . "ms_seller`
				WHERE seller_id = " . $res->row['seller'] . " start_date_close <= now() and end_date_close >=now() AND status_close = 1";
        $f = $this->db->query($sql);
        
        if ($f->row) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	public function updatePassword($data = array()){
		$sql = "UPDATE " . DB_PREFIX . "customer
				SET	change_password =  '0'
				WHERE change_password = '" . $this->db->escape($data['code']) ."' AND customer_id=". (int)$data['customer_id'];
		
		$this->db->query($sql);
		
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_status =  1
				WHERE seller_id=". (int)$data['customer_id'];
		
		$this->db->query($sql);
	}
	
	public function grantPremiumSeller($seller_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_group=1 AND seller_type=0 AND premium_status=1 AND seller_id = '" . (int)$seller_id ."'";
		
		$res = $this->db->query($sql);

		if($res->num_rows > 0){
			$seller=$res->row;
			$this->load->model('localisation/language');
			$languages = $this->model_localisation_language->getLanguages();
			
			// commissions
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "ms_seller_group 
					SET seller_group_id = '".(int)$seller_id."',
					    commission_id = '1',
						product_period = '0',
						product_quantity = '0'
			");
			
			foreach($languages as $language){
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "ms_seller_group_description 
						SET seller_group_id = '" . (int)$seller_id . "', 
							language_id = '" . (int)$language['language_id'] . "', 
							name = '" . $this->db->escape($seller['nickname']) . "', 
							description = '" . $this->db->escape($seller['nickname']) . " Group'");
			}
			
			$this->db->query("UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_group =  '".(int)$seller_id."',
				seller_type =  1,
				premium_status =  2
				WHERE seller_id=". (int)$seller_id);
		
		}
	}
	
	public function requestPremiumSeller($seller_id){
		$this->db->query("UPDATE " . DB_PREFIX . "ms_seller
				SET	premium_status =  1
				WHERE seller_type=0 AND premium_status=0 AND seller_id=". (int)$seller_id);
	}
	
	public function checkRole($route){
		$result=true;
		$routes=explode("/",$route);
		if(sizeof($routes)>=2){
			$route=$routes[0]."/".$routes[1];
			$sql = "SELECT
						*
					FROM " . DB_PREFIX . "ms_roles omr
					WHERE 1 = 1 AND omr.role='".$this->db->escape($route)."' ";

			$res = $this->db->query($sql);
			
			if($res->num_rows>0){
				$role=$res->row;
				if(in_array((int)$role['role_id'],$this->roles)){
					$result=false;
				}
			}
		}
		
		return $result;
	}
	
	public function getSellerFaultReachLimit(){
		$sql = "SELECT count(1) as total
				FROM `" . DB_PREFIX . "ms_seller_fault` omsf,`" . DB_PREFIX . "ms_seller` oms
				WHERE omsf.seller_id=oms.seller_id AND oms.seller_status=1 AND omsf.fault <= " . (int)$this->config->get('msconf_seller_fault_min') . "";
		$res = $this->db->query($sql);
		return $res->row['total'];
	}
	
	public function processFault($seller_id){
		$this->db->query("UPDATE " . DB_PREFIX . "ms_seller_fault
				SET	fault =  fault-1
				WHERE seller_id=". (int)$seller_id);
	}
	
	public function processNonCooperation($seller_id){
		$this->db->query("UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_status =  ".MsSeller::STATUS_DISABLED."
				WHERE seller_id=". (int)$seller_id);
		
		$this->db->query("UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_status =  ".MsSeller::STATUS_DISABLED."
				WHERE seller_group=". (int)$seller_id);
				
		$this->db->query("UPDATE " . DB_PREFIX . "customer
				SET	status =  0
				WHERE customer_id=". (int)$seller_id);
				
		$this->db->query("UPDATE " . DB_PREFIX . "customer
				SET	status =  0
				WHERE customer_id IN (SELECT seller_id FROM " . DB_PREFIX . "ms_seller WHERE seller_group=". (int)$seller_id.")");
		
		$this->db->query("UPDATE " . DB_PREFIX . "ms_product
				SET	product_status =  0
				WHERE seller_id=". (int)$seller_id);
				
		$this->db->query("UPDATE " . DB_PREFIX . "product
				SET	status =  0
				WHERE product_id IN (SELECT product_id FROM " . DB_PREFIX . "ms_product WHERE seller_id=". (int)$seller_id.")");
	}
}

?>
