<?php
class Balance {

	const BALANCE_TYPE_SALE = 1;
	const BALANCE_TYPE_REFUND_PRODUCT = 2;
	const BALANCE_TYPE_WITHDRAWAL = 3;
	const BALANCE_TYPE_REFUND_SHIPPING = 8;
	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->config = $registry->get('config');
		$this->currency = $registry->get('currency');
	}
	
	public function getCustomerBalanceEntries($customer_id, $sort) {
		$sql = "SELECT *
				FROM " . DB_PREFIX . "balance
				WHERE customer_id = " . (int)$customer_id . "
				ORDER BY {$sort['order_by']} {$sort['order_way']}"
				. ($sort['limit'] ? " LIMIT ".(int)(($sort['page'] - 1) * $sort['limit']).', '.(int)($sort['limit']) : '');
		$res = $this->db->query($sql);

		return $res->rows;
	}
	
	public function getTotalCustomerBalanceEntries($customer_id) {
		$sql = "SELECT COUNT(*) as 'total'
				FROM " . DB_PREFIX . "balance
				WHERE customer_id = " . (int)$customer_id;
				
		$res = $this->db->query($sql);

		return $res->row['total'];
	}

	public function getBalanceEntries($data = array(), $sort = array()) {
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		// todo fix other getBalanceEntries calls
		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					*,
					mb.description as 'mb.description',
					mb.date_created as 'mb.date_created'
				FROM " . DB_PREFIX . "balance mb
				INNER JOIN " . DB_PREFIX . "customer ms
					ON (mb.customer_id = ms.customer_id)
				WHERE 1 = 1"
				. (isset($data['order_id']) ? " AND mb.order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['order_detail_id']) ? " AND mb.order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['return_id']) ? " AND mb.return_id =  " .  (int)$data['return_id'] : '')
				. (isset($data['product_id']) ? " AND mb.product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['customer_id']) ? " AND mb.customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['withdrawal_id']) ? " AND mb.withdrawal_id =  " .  (int)$data['withdrawal_id'] : '')
				. (isset($data['balance_type']) ? " AND mb.balance_type =  " .  (int)$data['balance_type'] : '')
				. $filters
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
		$res = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];

		return $res->rows;
	}
	
	public function getBalanceEntry($data) {
		$sql = "SELECT *
				FROM " . DB_PREFIX . "balance mb
				WHERE 1 = 1 "
				. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['order_detail_id']) ? " AND order_detail_id =  " .  (int)$data['order_detail_id'] : '')
				. (isset($data['return_id']) ? " AND return_id =  " .  (int)$data['return_id'] : '')
				. (isset($data['product_id']) ? " AND product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['customer_id']) ? " AND customer_id =  " .  (int)$data['customer_id'] : '')
				. (isset($data['withdrawal_id']) ? " AND withdrawal_id =  " .  (int)$data['withdrawal_id'] : '')
				. (isset($data['balance_type']) ? " AND balance_type =  " .  (int)$data['balance_type'] : '')
				. " LIMIT 1";
				
		$res = $this->db->query($sql);
		
		if ($res->num_rows)
			return $res->row;
		else
			return FALSE;
	}	
	
	public function getTotalBalanceEntries() {
		$sql = "SELECT COUNT(*) as 'total'
				FROM " . DB_PREFIX . "balance";
				
		$res = $this->db->query($sql);

		return $res->row['total'];
	}

	public function getTotalBalanceAmount($data = array()) {
		$sql = "SELECT COALESCE(
					(SELECT SUM(balance) FROM " . DB_PREFIX . "balance
					 WHERE balance_id IN (
						SELECT MAX(balance_id) FROM " . DB_PREFIX . "balance
						LEFT JOIN " . DB_PREFIX . "customer
							USING(customer_id)"
						. (isset($data['status']) ? " WHERE status IN  (" .  $this->db->escape(implode(',', $data['status'])) . ")" : '') .
						" GROUP BY customer_id
					)),
					0
				) as total";
		$res = $this->db->query($sql);

		return $res->row['total'];
	}


	public function getCustomerBalance($customer_id) {
		// note: update getSellers() if updating this
		$sql = "SELECT COALESCE(
					(SELECT balance FROM " . DB_PREFIX . "balance
						WHERE customer_id = " . (int)$customer_id . " 
						ORDER BY balance_id DESC
						LIMIT 1
					),
					0
				) as balance";
		$res = $this->db->query($sql);

		return $res->row['balance'];
	}

	public function getAvailableCustomerFunds($customer_id) {
		return max(0, $this->getCustomerBalance($customer_id) - $this->getReservedCustomerFunds($customer_id) - $this->getWaitingCustomerFunds($customer_id));
	}

	public function getReservedCustomerFunds($customer_id) {
		$sql = "SELECT SUM(amount) as total
				FROM " . DB_PREFIX . "payment
				WHERE customer_id = " . (int)$customer_id . "
				AND payment_type IN (" . (int)Payment::TYPE_PAYOUT . "," . (int)Payment::TYPE_PAYOUT_REQUEST . ")
				AND payment_status = " . (int)Payment::STATUS_UNPAID;
		
		$res = $this->db->query($sql);

		return $res->row['total'];
	}
	
	public function getWaitingCustomerFunds($customer_id) {
		$sql = "SELECT SUM(amount) as total
				FROM " . DB_PREFIX . "balance
				WHERE customer_id = " . (int)$customer_id . " 
				AND balance_type = " . (int)Balance::BALANCE_TYPE_SALE ."
				AND DATEDIFF(NOW(), date_created) < " . (int)$this->config->get('msconf_withdrawal_waiting_period');
		$res = $this->db->query($sql);
		
		$pending_diff = $this->getCustomerBalance($customer_id) - $this->getReservedCustomerFunds($customer_id);
		
		if ($pending_diff >= $res->row['total'])
			return $res->row['total'];
		else
			return $pending_diff;
	}	
	
	public function addBalanceEntry($customer_id, $data) {
		$sql = "INSERT INTO " . DB_PREFIX . "balance
				SET customer_id = " . (int)$customer_id . ",
					order_id = " . (isset($data['order_id']) ? (int)$data['order_id'] : 'NULL') . ",
					order_detail_id = " . (isset($data['order_detail_id']) ? (int)$data['order_detail_id'] : 'NULL') . ",
					return_id = " . (isset($data['return_id']) ? (int)$data['return_id'] : 'NULL') . ",
					product_id = " . (isset($data['product_id']) ? (int)$data['product_id'] : 'NULL') . ",
					withdrawal_id = " . (isset($data['withdrawal_id']) ? (int)$data['withdrawal_id'] : 'NULL') . ",
					balance_type = " . (int)$data['balance_type'] . ",
					amount = ". (float)$this->currency->format($data['amount'], $this->config->get('config_currency'), '', FALSE) . ",
					balance = amount + (
						SELECT balance FROM (
							SELECT COALESCE(
								(SELECT balance FROM " . DB_PREFIX . "balance
						  			WHERE customer_id = " . (int)$customer_id . "
						  			ORDER BY balance_id DESC LIMIT 1),
								0
							) as balance
						) as tmpTable
					),
					description = '" . $this->db->escape($data['description']) . "',
					date_created = NOW()";

		$this->db->query($sql);
		
		$balance_id = $this->db->getLastId();
		return $balance_id;
	}
	
}