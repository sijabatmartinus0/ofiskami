<?php
class RajaOngkir {
	public function __construct() {
	
	}

	public function getServiceRajaOngkir($ORIGIN,$DESTINATION,$WEIGHT, $COURIER='jne'){
		$result=array();
		if((int)$WEIGHT<1){
			$WEIGHT=1;
		}
		
		$WEIGHT=ceil($WEIGHT);
		
		try{
			$curl = curl_init();
			//echo "origin=".$ORIGIN."&destination=".$DESTINATION."&weight=".$WEIGHT;
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
			  //CURLOPT_HTTPPROXYTUNNEL =>0,
			  //CURLOPT_PROXY, 'proxyagit.astragraphia.local:8000',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "originType=city&origin=".$ORIGIN."&destinationType=city&destination=".$DESTINATION."&weight=".$WEIGHT."&courier=".$COURIER,
			  CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 302a3e35ce54ca4d1427251aeb785e51"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			//echo $response;
			curl_close($curl);
			$data=json_decode($response, true);
			//echo $response;
			if(isset($data['rajaongkir']['results'])){
				$result=$data['rajaongkir']['results'];
			}
		}catch(Exception $e){
			$result=array();
		}
		
		return $result;
		
	}
	
	public function checkWaybillRajaOngkir($WAYBILL, $COURIER='jne'){
		$result=array();
		try{
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://pro.rajaongkir.com/api/waybill",
			  //CURLOPT_HTTPPROXYTUNNEL =>0,
			  //CURLOPT_PROXY, 'proxyagit.astragraphia.local:8000',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "waybill=".$WAYBILL."&courier=".$COURIER,
			  CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 302a3e35ce54ca4d1427251aeb785e51"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			//echo $response;
			curl_close($curl);
			$data=json_decode($response, true);
			//echo $response;
			if(isset($data['rajaongkir']['result'])){
				$result=$data['rajaongkir']['result'];
			}
		}catch(Exception $e){
			$result=array();
		}
		
		return $result;
		
	}
}
