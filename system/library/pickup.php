<?php
class Pickup {
	private $pp_user_id;
	private $pp_branch_id;
	private $isPickupPoint = FALSE; 

	public function __construct($registry) {
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');

		if (isset($this->session->data['customer_id'])) {
			$pickup_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pp_user WHERE pp_user_id = '" . (int)$this->session->data['customer_id'] . "' AND status = '1'");

			if ($pickup_query->num_rows) {
				$this->pp_user_id = $pickup_query->row['pp_user_id'];
				$this->pp_branch_id = $pickup_query->row['pp_branch_id'];
				$this->isPickupPoint = TRUE;
			}
		}
	}

	public function getId() {
		return $this->pp_user_id;
	}

	public function getBranchId() {
		return $this->pp_branch_id;
	}

	public function isPickupPoint() {
		return $this->isPickupPoint;
	}
	
	public function isPickupPointUser($pp_user_id){
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "pp_user`
				WHERE pp_user_id = " . (int)$pp_user_id;
		
		$res = $this->db->query($sql);
		
		if ($res->row['total'] == 0)
			return FALSE;
		else
			return TRUE;
	}
}