<?php
// Heading
$_['heading_title']            = 'Shopping Cart';

// Text
$_['text_success']             = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_remove']              = 'Success: You have modified your shopping cart!';
$_['text_login']               = 'Attention: You must <a href="%s">login</a> or <a href="%s">create an account</a> to view prices!';
$_['text_items']               = '%s item(s) - %s';
$_['text_points']              = 'Reward Points: %s';
$_['text_next']                = 'What would you like to do next?';
$_['text_next_choice']         = 'Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.';
$_['text_empty']               = 'Your shopping cart is empty!';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';
$_['text_trial']               = '%s every %s %s for %s payments then ';
$_['text_recurring']           = '%s every %s %s';
$_['text_length']              = ' for %s payments';
$_['text_until_cancelled']     = 'until cancelled';
$_['text_recurring_item']      = 'Recurring Item';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_no_access']      = 'You can not access this page';
$_['text_voucher']      = 'Gift Voucher';



// Column
$_['column_image']             = 'Images';
$_['column_name']              = 'Product Name';
$_['column_model']             = 'Model';
$_['column_quantity']          = 'Quantity';
$_['column_price']             = 'Unit Price';
$_['column_total']             = 'Total';

//Edit by arin
$_['column_seller']			   = 'Buy from shop:';
$_['column_product_price']	   = 'Product Price';
$_['column_remark']	   		   = 'Remark';
$_['column_address']	   	   = 'Shipping Destination';
$_['column_insurance']	   	   = 'Insurance';
$_['column_total_unit']	   	   = 'Total Items';
$_['column_subtotal']	   	   = 'Subtotal';
$_['column_insurance_charge']  = 'Insurance Cost';
$_['column_shipping']	   	   = 'Shipping Cost';
$_['text_delete_all']		   = 'Delete All';
$_['text_per_invoice']		   = 'Total per Invoice';
$_['text_insurance_yes']	   = 'Yes';
$_['text_insurance_no']	       = 'No';

// Error

	$_['text_items'] = '<i class="fa fa-shopping-cart"></i><span class="count">%s</span> <span class="total contrast_font mobile_hide">%s</span>';
			
$_['error_stock']              = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum']            = 'Minimum order amount for %s is %s!';
$_['error_maximum']            = 'Maximum order amount for %s is %s!';
$_['error_required']           = '%s required!';
$_['error_product']            = 'Warning: There are no products in your cart!';
$_['error_recurring_required'] = 'Please select a payment recurring!';		
			
$_['text_md_cart_product_name'] = 'Product Name';
$_['text_md_cart_product_amount'] = 'Amount';
$_['text_md_cart_product_price'] = 'Price';
$_['text_md_cart_product_information'] = 'Information';
$_['text_md_cart_nologin_message'] = 'Please login first!';
$_['text_md_cart_notfound_message'] = 'Product not found!';
$_['button_md_cart_pick_up_point'] = 'Pick Up';
$_['button_md_cart_logistic'] = 'Delivery';
$_['button_md_cart_add_address'] = 'Add New Address';
$_['button_md_cart_cancel_address'] = 'Back';

$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_alias']       = 'Save The Address as';
$_['entry_company']        = 'Company';
$_['entry_address_1']      = 'Address 1';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_subdistrict']           = 'Sub District';
$_['entry_district']           = 'District';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Region / State';

$_['text_md_cart_search_pp_country'] = 'Country';
$_['text_md_cart_search_pp_zone'] = 'Zone';
$_['text_md_cart_search_pp_city'] = 'City';
$_['text_md_cart_search_pp_branch'] = 'Pickup Point';
$_['text_md_cart_info_pp_name'] = 'Pickup Point';
$_['text_md_cart_info_pp_address'] = 'Address';
$_['text_md_cart_info_pp_company'] = 'Company';
$_['text_md_cart_info_pp_telephone'] = 'Telephone';

$_['text_md_cart_info_shipping_name'] = 'Courier';
$_['text_md_cart_info_shipping_service_name'] = 'Service';
$_['text_md_cart_info_shipping_service_insurance'] = 'Insurance';
$_['text_md_cart_info_shipping_price'] = 'Price';
$_['text_md_cart_info_total'] = 'Subtotal';

$_['error_warning'] = "Sorry, We can not process your request";

$_['text_md_cart_pickup_person_status'] = 'Pick Up by Other Person';
$_['text_md_cart_pickup_person_name'] = 'Name';
$_['text_md_cart_pickup_person_email'] = 'Email';
$_['text_md_cart_pickup_person_phone'] = 'Phone';

$_['text_md_edit_address'] = 'Edit Shipping & Address';
$_['text_md_edit_cart'] = 'Edit Cart';

$_['error_amount'] = 'Stock Not Available';
$_['error_amount_0'] = 'Amount must greater than 0 or minimum checkout';
$_['error_delivery_type'] = 'Please Choose Delivery Type';
$_['error_pickup_branch'] = 'Please Choose Pick Up Point Branch';
$_['error_pickup_person_phone'] = 'Please fill Pick Up Person Phone';
$_['error_pickup_person_phone_valid'] = 'Please fill a Valid Pick Up Person Phone';
$_['error_pickup_person_email'] = "Pick Up Person Email Doesn't Appear to Be Valid";
$_['error_pickup_person_name'] = 'Please fill Pick Up Person Name';
$_['error_shipping_address'] = 'Please Choose Shipping Address';
$_['error_insurance'] = 'Please Choose Shipping Insurance';
$_['error_shipping_service_id'] = 'Please Choose Shipping Service';
$_['error_shipping_id'] = 'Please Choose Shipping Agent';
$_['error_add_firstname'] = 'Please fill First Name';
$_['error_add_lastname'] = 'Please fill Last Name';
$_['error_add_alias'] = 'Please fill Address Name';
$_['error_add_address_1'] = 'Please fill Address';
$_['error_add_country_id'] = 'Please Choose Country';
$_['error_add_zone_id'] = 'Please Choose Zone';
$_['error_add_city_id'] = 'Please Choose City';
$_['error_add_district_id'] = 'Please Choose District';
$_['error_add_subdistrict_id'] = 'Please Choose Sub District';
$_['error_add_postcode'] = 'Please fill Postcode';
$_['error_api'] = 'Error Shipping API';

$_['text_md_cart_activation_message'] = 'Please activate your email first!';
$_['button_activation'] = 'Activate';

$_['text_reload']      = 'Reload';

$_['button_ok']      = 'Ok';
$_['text_md_remove']      = 'Remove Product';
$_['text_remove_product_confirmation']      = 'Are you sure to remove this product from cart ?';
$_['text_md_remove_all']      = 'Remove All Products';
$_['text_remove_all_product_confirmation']      = 'Are you sure to remove this products from cart ?';

$_['text_error_log_not_match']      = 'Error, Shipping Service not match!';
$_['text_error_log_seller']      = 'Error, Seller/Product not found!';
$_['text_error_log_product']      = 'Error, Product not found!';
$_['text_md_cart_empty_product']      = 'Product has been removed or changed from cart!';
