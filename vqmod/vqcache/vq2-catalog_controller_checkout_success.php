<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');
		$data['method']='';
		
		$this->load->model('account/activity');
		$this->load->model('checkout/order');
		
		$data['text_total_price'] = $this->language->get('text_total_price');
		
		if (isset($this->session->data['order_id'])) {
			
			$order=$this->model_checkout_order->getOrder($this->session->data['order_id']);
			
			if($order){
				if((int)$order['order_status_id']==1 || ((int)$order['order_status_id']==3 && $order['payment_code']=='sgopayment') || ((int)$order['order_status_id']==3 && $order['payment_code']=='free_checkout') || ((int)$order['order_status_id']==2 && $order['payment_code']=='employee_program')){
					if($order['payment_code']=='bank_transfer' || $order['payment_code']=='employee_program' || $order['payment_code']=='free_checkout'){
						$data['text_total_price'] = $this->language->get('text_total_price');
					}else if($order['payment_code']=='sgopayment' || $order['payment_code']=='vapayment'){
						$data['text_total_price'] = $this->language->get('text_total_price_paid');
					}
					$this->cart->clear();
					$data['method']=$order['payment_method'];
					if($order['payment_code']=='bank_transfer'){
						$transfer_code=$this->model_checkout_order->getTransferCode($this->session->data['order_id']);
						$data['total']=$this->currency->format($order['total']);
						$data['code']=$this->currency->format($transfer_code);
						// $data['text_message_transfer'] = sprintf($this->language->get('text_customer_transfer'), $data['code']);
						$data['text_message_transfer'] = sprintf($this->language->get('text_customer_transfer'), $this->session->data['order_id']);
					}else{
						$data['total']=$this->currency->format($order['total']);
					}
					
					if ($this->customer->isLogged()) {
						$activity_data = array(
							'customer_id' => $this->customer->getId(),
							'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
							'order_id'    => $this->session->data['order_id']
						);

						$this->model_account_activity->addActivity('order_account', $activity_data);
					} else {
						$activity_data = array(
							'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
							'order_id' => $this->session->data['order_id']
						);

						$this->model_account_activity->addActivity('order_guest', $activity_data);
					}

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
					unset($this->session->data['guest']);

				unset($this->session->data['delivery_date']);
				unset($this->session->data['survey']);
			
					unset($this->session->data['comment']);
					unset($this->session->data['order_id']);
					unset($this->session->data['second_order_id']);
					unset($this->session->data['coupon']);
					unset($this->session->data['reward']);
					unset($this->session->data['voucher']);
					unset($this->session->data['vouchers']);
					unset($this->session->data['totals']);
				}
			}
		}else{
			$this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $data['method']);
			$data['is_logged_in'] = true;
		} 

		$data['button_continue'] = $this->language->get('button_continue');
		//edit by arin
		$data['button_confirm_payment'] = $this->language->get('button_confirm_payment');
		
		$data['continue'] = $this->url->link('account/order');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}
	
	private function isLoggedIn() {
        return $this->customer->isLogged();
    }
}