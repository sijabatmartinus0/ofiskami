<?php
// Heading  
$_['heading_title']   = 'Cart';

// Text
$_['text_success']    		= 'Sukses: Anda telah menambahkan <a href="%s">%s</a> ke <a href="%s">cart</a> Anda!';
$_['text_remove']              = 'Sukses: Anda telah memodifikasi cart Anda!';
$_['text_login']      		= 'Perhatian: Anda harus <a href="%s">login</a> atau <a href="%s">buat Akun</a> untuk melihat harga!';
$_['text_items']      		= '%s item - %s';
$_['text_points']     		= 'Poin Hadiah: %s';
$_['text_next']                = 'Apa yang akan Anda lakukan selanjutnya?';
$_['text_next_choice']         = 'Pilih jika Anda mempunyai kode diskon atau poin hadiah yang ingin Anda gunakan atau Anda ingin memperkirakan ongkos kirim.';
$_['text_empty']               = 'Cart Anda masih kosong!';
$_['text_day']                 = 'hari';
$_['text_week']                = 'minggu';
$_['text_semi_month']          = 'setengah bulan';
$_['text_month']               = 'bulan';
$_['text_year']                = 'tahun';
$_['text_trial']               = '%s setiap %s %s untuk %s pembayaran lalu ';
$_['text_recurring']           = '%s setiap %s %s';
$_['text_length']              = ' untuk %s pembayaran';
$_['text_until_cancelled']     = 'sampai dibatalkan';
$_['text_recurring_item']      = 'Barang Berulang';
$_['text_payment_recurring']   = 'Profil Pembayaran';
$_['text_trial_description']   = '%s setiap %d %s(s) untuk %d pembayaran lalu';
$_['text_payment_description'] = '%s setiap %d %s(s) untuk %d pembayaran';
$_['text_payment_cancel']      = '%s setiap %d %s(s) sampai dibatalkan';
$_['text_no_access']      = 'Anda tidak dapat mengakses halaman ini';
$_['text_voucher']      = 'Gift Voucher';

// Column
$_['column_image']             = 'Gambar';
$_['column_name']              = 'Nama Produk';
$_['column_model']             = 'Model';
$_['column_quantity']          = 'Jumlah';
$_['column_price']             = 'Harga Satuan';
$_['column_total']             = 'Total';

//Edit by arin
$_['column_seller']			   = 'Pembelian dari toko:';
$_['column_product_price']	   = 'Harga Barang';
$_['column_remark']	   		   = 'Keterangan';
$_['column_address']	   	   = 'Alamat Tujuan';
$_['column_insurance']	   	   = 'Asuransi';
$_['column_total_unit']	   	   = 'Total Barang';
$_['column_subtotal']	   	   = 'Subtotal';
$_['column_insurance_charge']  = 'Biaya Asuransi';
$_['column_shipping']	   	   = 'Ongkos Kirim';
$_['text_delete_all']		   = 'Hapus Semua';
$_['text_per_invoice']		   = 'Total per Invoice';
$_['text_insurance_yes']	   = 'Ya';
$_['text_insurance_no']	       = 'Tidak';

// Error

	$_['text_items'] = '<i class="fa fa-shopping-cart"></i><span class="count">%s</span> <span class="total contrast_font mobile_hide">%s</span>';
			
$_['error_stock']              = 'Produk dengan tanda *** tidak tersedia dalam jumlah yang diinginkan atau stok kosong!';
$_['error_minimum']            = 'Jumlah pesanan minimum untuk %s adalah %s!';
$_['error_maximum']            = 'Jumlah pesanan maximum untuk %s adalah %s!';	
$_['error_required']           = '%s wajib!';
$_['error_product']            = 'Error: Tidak ada produk dalam cart Anda!';
$_['error_recurring_required'] = 'Silahkan pilih pembayaran berulang!';

$_['text_md_cart_product_name'] = 'Nama Produk';
$_['text_md_cart_product_amount'] = 'Jumlah';
$_['text_md_cart_product_price'] = 'Harga';
$_['text_md_cart_product_information'] = 'Informasi';
$_['text_md_cart_nologin_message'] = 'Silahkan login dahulu!';
$_['text_md_cart_notfound_message'] = 'Produk tidak ditemukan!';
$_['button_md_cart_pick_up_point'] = 'Ambil';
$_['button_md_cart_logistic'] = 'Kirim';
$_['button_md_cart_add_address'] = 'Tambah Alamat Baru';
$_['button_md_cart_cancel_address'] = 'Kembali';

$_['entry_firstname']      = 'Nama Depan';
$_['entry_lastname']       = 'Nama Belakang';
$_['entry_alias']       = 'Simpan Alamat sebagai';
$_['entry_company']        = 'Perusahaan';
$_['entry_address_1']      = 'Alamat 1';
$_['entry_address_2']      = 'Alamat 2';
$_['entry_postcode']       = 'Kode Pos';
$_['entry_subdistrict']      = 'Kelurahan';
$_['entry_district']      = 'Kecamatan';
$_['entry_city']           = 'Kota';
$_['entry_country']        = 'Negara';
$_['entry_zone']           = 'Propinsi';

$_['text_md_cart_search_pp_country'] = 'Negara';
$_['text_md_cart_search_pp_zone'] = 'Propinsi';
$_['text_md_cart_search_pp_city'] = 'Kota';
$_['text_md_cart_search_pp_branch'] = 'Pickup Point';
$_['text_md_cart_info_pp_name'] = 'Pickup Point';
$_['text_md_cart_info_pp_address'] = 'Alamat';
$_['text_md_cart_info_pp_company'] = 'Perusahaan';
$_['text_md_cart_info_pp_telephone'] = 'Telepon';

$_['error_amount'] = 'Stock Not Available';
$_['error_amount_0'] = 'Amount must greater than 0 or minimum checkout';
$_['error_delivery_type'] = 'Please Choose Delivery Type';
$_['error_pickup_branch'] = 'Please Choose Pick Up Point Branch';
$_['error_pickup_person_phone'] = 'Please fill Pick Up Person Phone';
$_['error_pickup_person_phone_valid'] = 'Please fill a Valid Pick Up Person Phone';
$_['error_pickup_person_email'] = 'Alamat Email Tidak Valid';
$_['error_pickup_person_name'] = 'Please fill Pick Up Person Name';
$_['error_shipping_address'] = 'Please Choose Shipping Address';
$_['error_insurance'] = 'Please Choose Shipping Insurance';
$_['error_shipping_service_id'] = 'Please Choose Shipping Service';
$_['error_shipping_id'] = 'Please Choose Shipping Agent';
$_['error_add_firstname'] = 'Please fill First Name';
$_['error_add_lastname'] = 'Please fill Last Name';
$_['error_add_alias'] = 'Please fill Address Name';
$_['error_add_address_1'] = 'Please fill Address';
$_['error_add_country_id'] = 'Please Choose Country';
$_['error_add_zone_id'] = 'Please Choose Zone';
$_['error_add_city_id'] = 'Please Choose City';
$_['error_add_district_id'] = 'Please Choose District';
$_['error_add_subdistrict_id'] = 'Please Choose Sub District';
$_['error_add_postcode'] = 'Please fill Postcode';

$_['text_md_cart_info_shipping_name'] = 'Kurir Pengiriman';
$_['text_md_cart_info_shipping_service_name'] = 'Paket Pengiriman';
$_['text_md_cart_info_shipping_service_insurance'] = 'Biaya Asuransi';
$_['text_md_cart_info_shipping_price'] = 'Ongkos Kirim';
$_['text_md_cart_info_total'] = 'Subtotal';

$_['text_md_cart_pickup_person_status'] = 'Diambil oleh Orang Lain';
$_['text_md_cart_pickup_person_name'] = 'Nama';
$_['text_md_cart_pickup_person_email'] = 'Email';
$_['text_md_cart_pickup_person_phone'] = 'Telepon';

$_['text_md_edit_address'] = 'Ubah Pengiriman & Alamat';
$_['text_md_edit_cart'] = 'Ubah Cart';

$_['error_warning'] = 'Maaf, Kami tidak dapat memproses permintaan anda';
$_['error_api'] = 'Error Shipping API';

$_['text_md_cart_activation_message'] = 'Silahkan aktivasi email anda dahulu!';
$_['button_activation'] = 'Aktivasi';


$_['text_reload']      = 'Ulangi';

$_['button_ok']      = 'Ok';
$_['text_md_remove']      = 'Hapus Produk';
$_['text_remove_product_confirmation']      = 'Anda yakin menghapus produk ini dari cart ?';
$_['text_md_remove_all']      = 'Hapus Semua Produk';
$_['text_remove_all_product_confirmation']      = 'Anda yakin menghapus produk-produk ini dari cart ?';

$_['text_error_log_not_match']      = 'Error, Data jasa pengiriman tidak sesuai!';
$_['text_error_log_seller']      = 'Error, Data Penjual/Produk tidak ditemukan!';
$_['text_error_log_product']      = 'Error, Data Produk tidak ditemukan!';
$_['text_md_cart_empty_product']      = 'Produk telah dihapus atau diubah dari cart!';
?>