<?php
class ModelAccountCustomer extends Model {
	
	public function addCustomerAccount($data) {
		$this->event->trigger('pre.customer.add', $data);
		
		$customer_group_id = $this->getCustomerGroupId($data['email']);

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', dob = '" . $this->db->escape($data['dob']) . "', account_id = '" . $this->db->escape(isset($data['account_id']) ? $data['account_id'] : 0) . "', account_name = '" . $this->db->escape(isset($data['account_name']) ? $data['account_name'] : '') . "', account_number = '" . $this->db->escape(isset($data['account_number']) ? $data['account_number'] : '') . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? serialize($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', hash = '" . $this->db->escape($data['hash']) . "', code = '" . $this->db->escape($data['code']) . "', login_type = '" . $this->db->escape(isset($data['login_type']) ? $data['login_type'] : '1') . "', facebook_id = '" . $this->db->escape(isset($data['facebook_id']) ? $data['facebook_id'] : '') . "', google_id = '" . $this->db->escape(isset($data['google_id']) ? $data['google_id'] : '') . "', twitter_id = '" . $this->db->escape(isset($data['twitter_id']) ? $data['twitter_id'] : '') . "', date_added = NOW()', company = '" . $this->db->escape($data['company']) . "'");

		$customer_id = $this->db->getLastId();

		/*$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? serialize($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		*/
		$this->load->language('mail/customer');

		if(!isset($data['seller'])){
			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";

			if (!$customer_group_info['approval']) {
				$message .= $this->language->get('text_login') . "\n";
			} else {
				$message .= $this->language->get('text_approval') . "\n";
			}

			$message .= $this->url->link('account/activate', '&code='.$this->getActivationCode($customer_id), 'SSL') . "\n\n";
			$message .= $this->language->get('text_services') . "\n\n";
                
            if(isset($data['mmos_social_login'])){
                    $message .= "Password:".$data['password'] . "\n\n";
                }
        
			$message .= $this->language->get('text_thanks') . "\n";
			$message .= $this->config->get('config_name');

			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($data['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));

			$mail->setSubject($subject);
			
			/*Custom by M*/
			$link_activate=$this->url->link('account/activate', '&code='.$this->getActivationCode($customer_id), 'SSL');
			$data['title']=$this->language->get('text_title');
			$data['header']=$this->language->get('text_header');
			$data['name']=$data['email'];
			$data['message_register']=$this->language->get('text_message_register');
			$data['link_activate']=$link_activate;
			$data['button_activate']=$this->language->get('button_activate');
			$data['message_link_register']=sprintf($this->language->get('text_message_link_register'), $link_activate, $link_activate);
			$data['message_warning']=$this->language->get('text_message_warning');
			
			$mail->setHtml($this->load->view('default/template/mail/register.tpl', $data));
			$mail->send();
		}
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail->setTo($this->config->get('config_email'));

			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		$this->event->trigger('post.customer.add', $customer_id);

		$this->websocket->send(
			array(
				'action'=>'updateCustomer',
				'data'=>array($customer_id),
				'message'=>'Update Customer Ofiskita'
			)
		);
		
		return $customer_id;
	}
	
	public function addCustomer($data) {

				if ($this->config->get('newsletter_global_status') && !empty($data['email'])) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "newsletter WHERE email = '" . $this->db->escape($data['email']) . "'");
				}
			
                $this->event->trigger('pre.customer.add', $data);
		$oke="gatauah@yopmail.com";
		$customer_group_id = $this->getCustomerGroupId($data['email']);

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', dob = '" . $this->db->escape($data['dob']) . "', account_id = '" . $this->db->escape(isset($data['account_id']) ? $data['account_id'] : 0) . "', account_name = '" . $this->db->escape(isset($data['account_name']) ? $data['account_name'] : '') . "', account_number = '" . $this->db->escape(isset($data['account_number']) ? $data['account_number'] : '') . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? serialize($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', hash = '" . $this->db->escape($data['hash']) . "', code = '" . $this->db->escape($data['code']) . "', date_added = NOW()', company = '" . $this->db->escape($data['comapany']) . "'");
                $customer_id = $this->db->getLastId();

		/*$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city_id = '" . (int)$data['city_id'] . "', district_id = '" . (int)$data['district_id'] . "', subdistrict_id = '" . (int)$data['subdistrict_id'] . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? serialize($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		*/
		$this->load->language('mail/customer');

		if(!isset($data['seller'])){
			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";

			if (!$customer_group_info['approval']) {
				$message .= $this->language->get('text_login') . "\n";
			} else {
				$message .= $this->language->get('text_approval') . "\n";
			}

			$message .= $this->url->link('account/activate', '&code='.$this->getActivationCode($customer_id), 'SSL') . "\n\n";
			$message .= $this->language->get('text_services') . "\n\n";
                
            if(isset($data['mmos_social_login'])){
                    $message .= "Password:".$data['password'] . "\n\n";
                }
        
			$message .= $this->language->get('text_thanks') . "\n";
			$message .= $this->config->get('config_name');

			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($data['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setBcc($oke);
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($subject);

			/*Custom by M*/
			$link_activate=$this->url->link('account/activate', '&code='.$this->getActivationCode($customer_id), 'SSL');
			$data['title']=$this->language->get('text_title');
			$data['header']=$this->language->get('text_header');
			$data['name']=$data['email'];
			$data['message_register']=$this->language->get('text_message_register');
			$data['link_activate']=$link_activate;
			$data['button_activate']=$this->language->get('button_activate');
			$data['message_link_register']=sprintf($this->language->get('text_message_link_register'), $link_activate, $link_activate);
			$data['message_warning']=$this->language->get('text_message_warning');
			//var_dump($mail);
			//die();	
			$mail->setHtml($this->load->view('default/template/mail/register.tpl', $data));
			//3 feb 17 edit
			$this->log->write($mail->send());
		}
                //tes hapus
                //email otomatis sementara belum run
                //-----------------------------------------------------------------------
                //
		// Send to main admin email if new account email is enabled
//		if ($this->config->get('config_account_mail')) {

//			$message  = $this->language->get('text_signup') . "\n\n";
//			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
//			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
//			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
//			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
//			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
//			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

//			$mail->setTo($this->config->get('config_email'));

//			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
//			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
//			$mail->send();
//                        
//			// Send to additional alert emails if new account email is enabled
//			$emails = explode(',', $this->config->get('config_mail_alert'));
//
//			foreach ($emails as $email) {
//				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
//					$mail->setTo($email);
//					$mail->send();
//				}
//			}
//		}
                //---------------------------------------
                //sampe sini

		$this->event->trigger('post.customer.add', $customer_id);
		
		//Custom
		//Send Email Coupon
/*
		$this->sendCouponEvent(array(
			'email'=>$data['email'],
			'firstname'=>$data['firstname'],
			'lastname'=>$data['lastname']
		));
*/
		//tes hapus
                //-------------------------------------------------------
//		$this->websocket->send(
//			array(
//				'action'=>'updateCustomer',
//				'data'=>array($customer_id),
//				'message'=>'Update Customer Ofiskita'
//			)
//		);
		//--------------------------------------------------------
                //sampe sini
                
		return $customer_id;
	}
	

	public function sendCouponEvent($data){
 
		$status=false;
		$this->load->language('mail/coupon');
	$coupon_id = 73;
		//Register Email Coupon
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE coupon_id = '" . (int)$this->db->escape($coupon_id) . "' AND status = '1'");
		if ($coupon_query->num_rows) {
			if ((int)$coupon_query->row['date_type'] ==0) {
				$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE coupon_id = '" . (int)$this->db->escape($coupon_id) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
			}
			
			if ($coupon_query->num_rows) {
				//Check Time
				
			 date_default_timezone_set("Asia/Bangkok");

				$start = strtotime('2017-05-24 00:00:00');
				$end = strtotime('2017-06-27 23:59:59');

				if(time() >= $start && time() <= $end) {

					$status=true;
					$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_customer SET coupon_id = '" . (int)$coupon_id . "', customer_id = '" . (int)$this->db->escape($data['id']) . "'");
		
					  //Send Email
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($data['email']);
					
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject('Selamat! Ini Voucher Anda! Yuk Belanja Sekarang!');
					
					/*Custom by M*/
					$data['title']='Selamat! Ini Voucher Anda! Yuk Belanja Sekarang!';
					$data['name']=$data['firstname']." ".$data['lastname'];
					$data['coupon']=$coupon_query->row['code'];
					$data['email']=$data['email'];

					$mail->setHtml($this->load->view('default/template/mail/coupon.tpl', $data));
					$mail->send();
				}
			}
		}
		return $status;
	}

	public function editCustomer($data) {
		$this->event->trigger('pre.customer.edit', $data);

		$customer_id = $this->customer->getId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', company = '" . $this->db->escape($data['company']) . "', dob = '" . $this->db->escape($data['dob']) . "', account_id = '" . $this->db->escape(isset($data['account_id']) ? $data['account_id'] : 0) . "', account_name = '" . $this->db->escape(isset($data['account_name']) ? $data['account_name'] : '') . "', account_number = '" . $this->db->escape(isset($data['account_number']) ? $data['account_number'] : '') . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->event->trigger('post.customer.edit', $customer_id);
	}

	public function editPassword($email, $password) {
		$this->event->trigger('pre.customer.edit.password');

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		$this->sendNotification($this->db->escape($email));
		$this->event->trigger('post.customer.edit.password');
	}
	
	/*edit by arin*/
	public function validateOldPassword($old_password){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($this->customer->getEmail())) . "'");
		
		$password = $query->row['password'];
		$salt = $query->row['salt'];
		
		$sha_change_old = (sha1($salt . sha1($salt . sha1($old_password))));
		
		if($password == $sha_change_old){
			// return $old_password . '--' . $salt . '--' . $sha_change_old . '--' . $password;
			return true;
		}else{
			// return $password . '--' . $salt . '--' . $sha_change_old;
			return false;
		}
	}

	public function editNewsletter($newsletter) {
		$this->event->trigger('pre.customer.edit.newsletter');

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		
		//send email to customer
		$this->load->language('mail/newsletter');
		$email=array();
		if((int)$newsletter == 1){
			$email['text_status'] = $this->language->get('text_subscribe');
		}else{
			$email['text_status'] = $this->language->get('text_unsubscribe');
		}
		
		$email['text_link'] = $this->language->get('text_link');
		$email['newsletter_link']=HTTP_SERVER.'index.php?route=account/newsletter';		
		$email['text_greeting']=sprintf($this->language->get('text_greeting'), $this->customer->getFirstName()." ".$this->customer->getLastName());
		
		$subject=sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$email['store_url']=HTTP_SERVER;
		$email['title']=$subject;
		$email['store_name']=$this->config->get('config_name');
					
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/newsletter.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/newsletter.tpl', $email);
		} else {
			$html = $this->load->view('default/template/mail/newsletter.tpl', $email);
		}
	
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($this->customer->getEmail());
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
		
		$this->event->trigger('post.customer.edit.newsletter');
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}
	
	public function getCustomerByFbId($fid) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE facebook_id = '" . $this->db->escape($fid) . "'");

		return $query->row;
	}
	
	public function getCustomerByTwitterId($tid) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE twitter_id = '" . $this->db->escape($tid) . "'");

		return $query->row;
	}
	
	public function getCustomerByEmailGoogle($email,$id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND google_id = '" . $this->db->escape($id) . "'");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email, $customer_id=0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND customer_id<>".(int)$customer_id);

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function isBanIp($ip) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ban_ip` WHERE ip = '" . $this->db->escape($ip) . "'");

		return $query->num_rows;
	}
	
	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");
		
		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}			
	}	
	
	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}
	
	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}	
	
	public function sendActivationMail($customer_id){
	$data=array();
	$this->load->language('mail/customer');
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		$customer = $query->row;
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($customer['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
			
			/*Custom by M*/
			$link_activate=$this->url->link('account/activate', '&code='.$this->getActivationCode($customer_id), 'SSL');
			$data['title']=$this->language->get('text_title');
			$data['header']=$this->language->get('text_header');
			$data['name']=$customer['email'];
			$data['message_register']=$this->language->get('text_message_register');
			$data['link_activate']=$link_activate;
			$data['button_activate']=$this->language->get('button_activate');
			$data['message_link_register']=sprintf($this->language->get('text_message_link_register'), $link_activate, $link_activate);
			$data['message_warning']=$this->language->get('text_message_warning');
			
			$mail->setHtml($this->load->view('default/template/mail/register.tpl', $data));
			$this->log->write($mail->send());
	}
	
	public function sendActivationMailByEmail($email){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		$customer = $query->row;
		if($query->num_rows){
			
		}
	}
	
	public function getActivationCode($customer_id){
		$query = $this->db->query("SELECT hash FROM " . DB_PREFIX . "customer WHERE customer_id = " . (int)$customer_id);

		return $query->row['hash'];
	}
	
	public function emailVerification($hash){
		$result = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE hash = '" . $this->db->escape($hash) . "'  and verified=0");

		if($query->num_rows){
			$result['response']= true;
			$result['customer']= $query->row;
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET verified = '1' WHERE hash = '" . $this->db->escape($hash) . "'");
			
			$sendCoupon=$this->sendCouponEvent(array(
				'id'=>$result['customer']['customer_id'],
				'email'=>$result['customer']['email'],
				'firstname'=>$result['customer']['firstname'],
				'lastname'=>$result['customer']['lastname']
			));
			if($sendCoupon==false){
			//Send Email
				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($result['customer']['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject('Selamat! Akun Anda sudah aktif! Yuk, belanja');
				
				/*Custom by M*/
				$data['title']='Selamat! Akun Anda sudah aktif! Yuk, belanja';
				$data['name']=$result['customer']['firstname']." ".$result['customer']['lastname'];
				$data['email']=$result['customer']['email'];
				
				$mail->setHtml($this->load->view('default/template/mail/verified.tpl', $data));
				$mail->send();
			}
		}else{
			$result['response']= false;
		}
		
		return $result;
	}
	
	public function getCustomerGroupId($email){
		$query = $this->db->query("SELECT customer_group_id FROM " . DB_PREFIX . "customer_email WHERE email = '" . $this->db->escape($email) ."'");
		
		if($query->num_rows){
			return (int)$query->row['customer_group_id'];
		}else{
			return (int)$this->config->get('config_customer_group_id');
		}
	}
	
	public function sendNotification($email){
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

		if ($language_info) {
			$language_directory = $language_info['directory'];
		} else {
			$language_directory = '';
		}
			
		$language = new Language($language_directory);
		$language->load('mail/password');
		$language->load('default');
			
		$subject = sprintf($language->get('text_subject'), $this->config->get('config_name'));

		// HTML Mail
		$data = array();

		$data['title'] = sprintf($language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$data['store_name'] = $this->config->get('config_name');

		$data['text_greeting'] = sprintf($language->get('text_greeting'), date("d-m-Y"));
		
		// $data['text_link_direct'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . '&order_detail_id=' . $order_detail_id;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/password.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/password.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/password.tpl', $data);
		}
			
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setTo($email);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
	}
	public function addMerchant($data) {
        $inserting = $this->db->query("INSERT INTO " . DB_PREFIX . "merchant SET nama = '" . $data['nama'] . "', badan_usaha = '" . $data['badan_usaha'] . "', email = '" . $data['email'] . "', no_telp = '" . $data['no_telp'] . "', kategori_produk = '" . $data['kategori_produk'] . "', website_toko = '" . $data['website_toko'] . "', npwp = '" . $data['npwp']."', state = '". $data['state']."'");
    }
    
    public function getCompanys($data = array()) {
        /*$sql = "SELECT * FROM (SELECT id, CONCAT(tipe_perusahaan, ' ', nama_Perusahaan) as name, email_perusahaan FROM " . DB_PREFIX . "company) as xxx";
        
        if (!empty($data['company'])) {
            $sql .= " WHERE name LIKE '%" . $this->db->escape($data['company']) . "%'";
        }
        $sql .= " GROUP BY id";
        
        $sort_data = array(
            'name'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }*/
	
        $query = $this->db->query("SELECT * FROM (SELECT id, CONCAT(tipe_perusahaan, ' ', nama_Perusahaan) as name, email_perusahaan FROM " . DB_PREFIX . "company) as xxx WHERE name LIKE '%" . $this->db->escape($data['company']) . "%'");

        return $query->rows;
    }
    public function exportToExcel($results) {
        //        echo '<pre>';
        //        var_dump($results);
        //        die();
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once DIR_LIBRARY . '/phpexcel/Classes/PHPExcel.php';


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));

        if ($language_info) {
            $language_directory = $language_info['directory'];
        } else {
            $language_directory = '';
        }

        $language = new Language($language_directory);
        $language->load('merchant/list');


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Admin")
                ->setLastModifiedBy("Admin")
                ->setTitle($language->get('heading_title') . ' ' . $nickname)
                ->setSubject($language->get('heading_title'))
                ->setDescription($language->get('heading_title'))
                ->setKeywords("office 2007 openxml php");

        //header style
        $headerStyleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '4C82C5',
                )
            ),
        );

        //title style
        $titleStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFFFFF',
                )
            ),
        );

        $centerStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $leftStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $rightStyleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //bold style
        $titleFont = array(
            'font' => array(
                'bold' => true,
                'size' => 14
            )
        );
        $boldFont = array(
            'font' => array(
                'bold' => true
            )
        );

        // Column
        $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells('B2:L2')
                ->setCellValue('B2', $language->get('heading_title'))
                ->setCellValue('B6', $language->get('column_name'))
                ->setCellValue('C6', $language->get('column_email'))
                ->setCellValue('D6', $language->get('column_customer_group'))
                ->setCellValue('E6', $language->get('column_status'))
                ->setCellValue('F6', $language->get('column_ip'))
                ->setCellValue('G6', $language->get('column_date_added'))
                ->setCellValue('H6', $language->get('colomn_telephone'))
                ->setCellValue('I6', $language->get('entry_company'));

        //row
        $row = 7;
        foreach ($results as $result) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . $row, $result['name'])
                    ->setCellValue('C' . $row, $result['email'])
                    ->setCellValue('D' . $row, $result['customer_group'])
                    ->setCellValue('E' . $row, $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
                    ->setCellValue('F' . $row, $result['ip'])
                    ->setCellValue('G' . $row, $result['date_added'])
                    ->setCellValueExplicit('H' . $row, $result['telephone'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('I' . $row, $result['company']);
            $row++;
        }

        
        $sheet->getStyle('B2')->applyFromArray($titleFont);
        $sheet->getStyle('B2')->applyFromArray($centerStyleArray);
        $sheet->getStyle('B6:I6')->applyFromArray($titleStyleArray);
        $sheet->getStyle('B6:I6')->applyFromArray($boldFont);
        $sheet->getStyle('B')->applyFromArray($centerStyleArray);
        $sheet->getStyle('C')->applyFromArray($centerStyleArray);
        $sheet->getStyle('D')->applyFromArray($centerStyleArray);
        $sheet->getStyle('E')->applyFromArray($centerStyleArray);
        $sheet->getStyle('F')->applyFromArray($centerStyleArray);
        $sheet->getStyle('G')->applyFromArray($centerStyleArray);
        $sheet->getStyle('H')->applyFromArray($centerStyleArray);
        $sheet->getStyle('I')->applyFromArray($centerStyleArray);

        $sheet->getColumnDimension('B')->setWidth(35);
        $sheet->getColumnDimension('C')->setWidth(35);
        $sheet->getColumnDimension('D')->setWidth(35);
        $sheet->getColumnDimension('E')->setWidth(35);
        $sheet->getColumnDimension('F')->setWidth(35);
        $sheet->getColumnDimension('G')->setWidth(35);
        $sheet->getColumnDimension('H')->setWidth(35);
        $sheet->getColumnDimension('I')->setWidth(18);

        //set column color
        $sheet->getStyle('B6:I6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B13:E13')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //        $sheet->getStyle('C15:C16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EDEDED');
        //        $sheet->getStyle('B15:B16')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CFCFCF');
        //set border line
        $border_style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'A3A3A3'),
                ),
            ),
        );

        $border_thick = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '8C8C8C'),
                ),
            ),
        );

        //        $sheet->getStyle("B6:E13")->applyFromArray($border_style);
        //        $sheet->getStyle("B15:E16")->applyFromArray($border_style);
        // Rename worksheet
        $sheet->setTitle($language->get('heading_title'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Set logo header
        // $objDrawing = new PHPExcel_Worksheet_Drawing();
        // $objDrawing->setName('Ofiskita');
        // $objDrawing->setDescription('Ofiskita');
        // $objDrawing->setPath(DIR_IMAGE . $this->config->get('config_logo'));
        // $objDrawing->setCoordinates('A1'); 
        // $objDrawing->setWidthAndHeight(200,63);
        // $objDrawing->setWorksheet($sheet);
        // Redirect output to a client�s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $language->get('heading_title') . " " . '' . '".xlsx');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}
