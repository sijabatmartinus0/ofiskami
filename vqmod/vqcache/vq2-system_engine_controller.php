<?php
abstract class Controller {
	protected $registry;

	public function __construct($registry) {

			$GLOBALS['controller_name'] = get_class ($this);

			
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}